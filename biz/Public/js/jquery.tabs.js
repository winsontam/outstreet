//DEMO CODE:

//<div id="tabs">
//	<ul>
//	  <li><a href="#t-1">Content 1</a></li>
//	  <li><a href="#t-2">Content 2</a></li>
//	  <li><a href="#t-3">Content 3</a></li>
//	</ul>
//	<div id="t-1">
//	  Content 1
//	</div>
//	<div id="t-2">
//	  Content 2
//	</div>
//	<div id="t-3">
//	  Content 3
//	</div>
//</div>

//jQuery('#tabs ul').showTabs();

jQuery.fn.showTabs = function (selected) {
    var sel = selected || 1;

    return this.each(function () {
        var ul    = jQuery(this);
        var ipl    = 'a[href^=#]';

        // Go through all the in-page links in the ul
        // and hide all but the selected's contents
        ul.find(ipl).each(function (i) {
            var link = jQuery(this);

            if ((i + 1) === sel) {
                link.addClass('selected');
            }
            else {
                jQuery(link.attr('href')).hide();
            }
        });

        // When clicking the UL (or anything within)
        ul.click(function (e) {
            var clicked    = jQuery(e.target);
            var link    = false;

            if (clicked.is(ipl)) {
                link = clicked;
            }
            else {
                var parent = clicked.parents(ipl);

                if (parent.length) {
                    link = parent;
                }
            }

            if (link) {
                var selected = ul.find('a.selected');

                if (selected.length) {
                    jQuery(selected.removeClass('selected').attr('href')).hide();
                }

                jQuery(link.addClass('selected').attr('href')).show();

                return false;
            }
        });
    });
};
