function initialselshop(target){
	selshopbox = $("#selshoptrigger"+target).fancybox({
		'scrolling'			:	'auto',
		'centerOnScroll'	:	true,
		'width'				:	800,
		'height'			:	400,
		'autoDimensions'	:	false,
		'onStart'			:	function(){
									this.href = __OT_APP__+'/index.php?m=base&a=selshop&ids='+$('#selshoptxt'+target).val()+'&errids='+$('#errorshoptxt'+target).val()+'&target='+target;
								}
	});	
}
function set_shop(target){
	var result = new Array();
	var num = 0;
	$(".selshop").each(function(){
		if($(this).attr('ref')=='true'){
			result[num] = $(this).val();
			num ++;
		}
	});
	$('#selshoptxt'+target).val(result.join(','));
	if(num==0){
		$('#selshopbutton'+target).val('選擇商戶');
	}else{
		$('#selshopbutton'+target).val('已選擇'+num+'個商戶');
	}
}
function set_selshop_ids(target,value){
	$('#selshoptxt'+target).val(value);
}
function get_selshop_ids(target){
	return $('#selshoptxt'+target).val();
}
function get_selshop_error(target){
	return $('#errorshoptxt'+target).val();
}
function set_selshop_error(target,value){
	$('#errorshoptxt'+target).val(value);
}