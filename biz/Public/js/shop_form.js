/*Initiation*/
var map = null;
var geocoder = null;

$("#address").keypress(function(e) {
  if (e.which == 13) {
    return false;
  }
});
/*End initiation*/

/*Validation*/
function getformValidateCriteria(formName, path,thick){

	if (typeof(description_len) == "undefined") { description_len = 20; }
	if (typeof(keyword_num) == "undefined") { keyword_num = 3; }

	var formValidateCriteria = new Array();
	formValidateCriteria = {
		event: "blur",
		rules: {
			name: { required: true },
			cate_ids: { required: true },
			main_location_id: { required: true },
			sub_location_id: { required: true },
			address: { required: true, minlength: 2 },
			telephone: { minlength: 8, number: true },
			sub_telephone: { minlength: 8, number: true },
			description:{ maxlength: description_len },
			eng_description:{ maxlength: description_len },
			parking:{ maxlength: 100 },
			email: { email: true },
			website:{ url: true },
			worktime_warning: { required: true }
		},
		messages: {
			name: "商戶名稱是必須",
			cate_ids: "所屬類別必須選擇至少一個",
			main_location_id: "所屬地區是必須",
			sub_location_id: "所屬地區是必須",
			address: { required: "地址是必須", minlength: "地址不能少於2個字符" },
			telephone:{ minlength: "電話號碼必須是8個字符", number: "電話號碼必需為數字" },
			sub_telephone:{ minlength: "電話號碼必須是8個字符", number: "電話號碼必需為數字" },
			description: { maxlength: "商戶介紹不能大於"+description_len+"個字符" },
			eng_description: { maxlength: "商戶介紹不能大於"+description_len+"個字符" },
			parking: { maxlength: "泊車資料不能大於100個字符" },
			email: "電郵地址必須正確",
			website: "網址地址必須正確",
			worktime_warning: "營業時間必須正確"
		},
		submitHandler: function(frm){
			var isError = false;

			$('#keyword_warning').css('color','#999');
			$('#eng_keyword_warning').css('color','#999');
			
			var keywordsNum = 0;
			var keywords = $(frm).find('[name=keyword]').val().split(',');
			for (var i=0; i<keywords.length; i++){
				if (keywords[i]) { keywordsNum++; }
			}
			
			var engKeywordsNum = 0;
			var engKeywords = $(frm).find('[name=eng_keyword]').val().split(',');
			for (var i=0; i<engKeywords.length; i++){
				if (engKeywords[i]) { engKeywordsNum++; }
			}
			
			if(keywordsNum > keyword_num){
				validator.showErrors( { 'keyword_warning': '請輸入不多於'+keyword_num+'個關鍵字' } ); isError = true;
			}else if(engKeywordsNum > keyword_num){
				validator.showErrors( { 'eng_keyword_warning': '請輸入不多於'+keyword_num+'個關鍵字' } ); isError = true;
			}

			var con1 = $('#is_renovation').attr('checked');
			var con2 = $('#is_closed').attr('checked');
			var con3 = $('#is_moved').attr('checked');						
			if( (con1 && con2 && con3)||(con1 && con2)||(con2 && con3)||(con1 && con3) ){
				validator.showErrors( { 'status_warning': '特殊狀態只可選擇其中一項' } ); isError = true;
			}
			
			if ( !isError ) {
				$( formName ).ajaxSubmit( {
					success: function ( response, status ) {
						var json = $.parseJSON( response );
						if ( json.success ) {
							alert('更改成功, 店鋪簡介需要經過審核才能更新，請耐心等候1-2個工作天!');
							location.reload();
						}
						else {
							alert('錯誤出現');
						}
					}
				} );
			}
			return false;
		}
	};
	return formValidateCriteria;	
}

function validateWorkTime(){			
	weekinput= 0;
	warning  = 0;
	for(i=0; i<8; i++){
		if(!$('#work_dayoff_'+i).attr('checked')) {
			if ( $('#work_start_time_'+i).val() > 0 && $('#work_start_time_'+i).val() < 2400
			&& $('#work_end_time_'+i).val() > 0 && $('#work_end_time_'+i).val() < 2400
			&& $('#work_start_time_'+i).val().length == 4 && $('#work_end_time_'+i).val().length == 4 ) {weekinput++; } else {
				warning++;
			}
		}
	}
	if (warning > 0){
		return '';
	}else{
		return '1';	
	}
}
/*End valiatation*/

/*EventHandler*/
function resetFormHandler(){
	$('#mallname').attr({readonly:false});
	$('#mallname').css('background-color','#FFFFFF');
	$('#mallname').val('');
	$('#address').val('');
	$('#googlemap_lat').val('');
	$('#googlemap_lng').val('');
	$('#mall').attr('checked',false).trigger('click').attr('checked',false);
	$('#reset').css('display','none');
}

function addressBlurEventHandler(){
	//if ($('#address').val() != '{$shopInfo.address}'){
		if ($('#main_location_id').val()=='' || $('#sub_location_id').val()==''){	
			alert('未輸入區域');
			return;
		}

		//$('#addressError').text('正在搜索您輸入的街道'+$('#main_location_name').val() + ' ' + $('#sub_location_name').val() + ' ' + $('#address').val()+'是否存在...');
		//$('#addressError').css('background-color','#66f');
		geocoder.getLatLng(
			$('#main_location_id option:selected').text() + ' ' + $('#sub_location_id option:selected').text() + ' ' + $('#address').val(),
			function(point) {
				if (!point) {
					$('#addressError').text('無法找到您輸入的街道地址');
					$('#addressError').css('color','#ff0000');
					//$('#addressError').css('background-color','#f66');
					//$('#address').focus();
				} else {
					$('#addressError').text('');
					//$('#addressError').text('您輸入的街道地址位於東經'+point.lat()+'度,北緯'+point.lng()+'度');
					//$('#addressError').css('background-color','#6f6');
					$('#googlemap_lat').val(point.lat());
					$('#googlemap_lng').val(point.lng());
					map.clearOverlays();
					map.setCenter(point, 17);
					var marker = new GMarker(point, {draggable: true});
					GEvent.addListener(marker, "dragstart", function() {
						map.closeInfoWindow();
					});
					GEvent.addListener(marker, "dragend", function() {
						var mlatlng = marker.getLatLng();
						$('#googlemap_lat').val(mlatlng.lat());
						$('#googlemap_lng').val(mlatlng.lng());
					});
					map.addOverlay(marker);
					//marker.openInfoWindowHtml($('#sub_location_name').val() + ' ' + $('#address').val());
				}
			}
		);
	//}	
}

function mainLocationChangeEventHandler(rootPath){
	//設置相應隱藏域的值		
	$('#main_location_name').val($('#main_location_id').val());
	//ajax取商圈
	$('#sub_location_id').empty();
	$.get(rootPath,function( response ){
		var json = $.parseJSON( response );
		for (idx in json.data){
			if (idx == 0){
				$('#sub_location_name').val(json.data[idx].name);
			}
			$('#sub_location_id').append('<option value="' + json.data[idx].id + '">' + json.data[idx].name +'</option>');
		}
	});	
}

function mallClickEventHandler(){
	if($('#mall').attr('checked')==true){
		$('#address').attr({readonly:true});
		$('#main_location_id_hidden').val($('#main_location_id').val());	
		$('#sub_location_id_hidden').val($('#sub_location_id').val());	
		$('#main_location_id').attr({disabled:true});	
		$('#sub_location_id').attr({disabled:true});			
		$('#address').css('background-color','#ebebe4');
		$('#mallname').css('background-color','#FFFFFF');		
		$('#googlemap_lat').val('');
		$('#googlemap_lng').val('');
		$('#bymallselect').css("display","");
		$('#mall_id').val('0');
		$('#mallname').val('');
		$('#address').val('');
	}else{
		$('#address').attr({readonly:false});
		$('#main_location_id').attr({disabled:false});	
		$('#sub_location_id').attr({disabled:false});	
		$('#address').css('background-color','#FFFFFF');
		$('#bymallselect').css("display","none");
		$('#malllist').hide();
		$('#mall_id').val('0');
		$('#mallname').val('');
		$('#address').val('');
		$('#googlemap_lat').val('');
		$('#googlemap_lng').val('');
		$('#address').trigger('blur');
	}	
}
/*End EventHandler*/

/*General function*/
function changeWorkTime(i) {
	if ($('#work_dayoff_'+i).attr('checked')) {
		$('#work_start_time_'+i).val('');
		$('#work_end_time_'+i).val('');
		$('#time_input_'+i).hide(); 
		$('#time_input_'+i).hide(); 
	} else {
		$('#time_input_'+i).show();
		$('#time_input_'+i).show();
	}
}

function setcates(cates){
	$('#cate_ids').val('');
	$('#cate_names').val('');
	var _ids = $('#cate_ids');
	var _names = $('#cate_names');	
	var temp_name='';
	for(idx in cates){
		if (_ids.val() != ''){
			_ids.val(_ids.val()+ ',')
		}
		if (_names.val() != ''){
			_names.val(_names.val()+ ',')
		}
		if(temp_name!=''){
			temp_name = temp_name + ',';
		}
		var id_ = cates[idx].value.split('||')[0];
		var name_ = cates[idx].value.split('||')[1];
		_ids.val(_ids.val() + id_);
		temp_name = temp_name + name_;
		_names.val(_names.val() + name_);
	}
	temp_name=shopuniquecates(temp_name);
	_names.val(temp_name);
}

function shopuniquecates(target){
	var ele = target.split(',');
	ele = shopunique(ele);	
	target='';
	for(var i=0;i<ele.length;i++){
		if(target!=''){
			target=target+',';
		}
		target=target+ele[i];
	}
	return target;
}

function shopunique(arrayName){
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ ){  
	for(var j=0; j<newArray.length;j++ ){
		if(newArray[j]==arrayName[i]) 
			continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}

function getcates(){
	var arr = $('#cate_ids').val().split(',');
	for(idx in arr){
		if (arr[idx] != ''){
			$('#cate_id_' + arr[idx]).attr('checked', true);
		}
	}
}

function buildGooglemap(googlemap_lat,googlemap_lng){
	map = new GMap2(document.getElementById("gmap"));
	geocoder = new GClientGeocoder();

	map.addControl(new GLargeMapControl());
	if(googlemap_lat!=null && googlemap_lng!=null){
		var latlng = new GLatLng(googlemap_lat,googlemap_lng);
		map.setCenter(latlng, 17);
		var marker = new GMarker(latlng, {draggable: true});
		map.addOverlay(marker);
		//marker.openInfoWindowHtml($('#sub_location_name').val() + ' ' + $('#address').val());
		GEvent.addListener(marker, "dragstart", function() {
		  	map.closeInfoWindow();
		});
		GEvent.addListener(marker, "dragend", function() {
			var mlatlng = marker.getLatLng();
			$('#googlemap_lat').val(mlatlng.lat());
			$('#googlemap_lng').val(mlatlng.lng());							
		});	
	}
}

function fillSelectedMall(root){
	var mainLocationId = $('#main_location_id').val();
	var subLocationId = $('#sub_location_id').val();
	if (mainLocationId == '' || subLocationId == ''){
		alert('請選擇商戶區域及所屬地區');
		$('#bymallselect').css("display","none");
		//$('#main_location_id').focus();
		$('#main_location_id').select();
		$('#mall').attr('checked', false);
		$('#main_location_id').attr({disabled:false});	
		$('#sub_location_id').attr({disabled:false});
		return;
	}
	var uri = root +'?m=shop&a=getMall&main_location_id='+mainLocationId+'&sub_location_id='+subLocationId+'&key=' + encodeURI($('#mallname').val());

	$.get(uri, function(response){
		var json = $.parseJSON( response );
		$('#malllist').empty();	
		$('#malllist').show();
		$('#nomalltip').hide();
		if (json.data != null){
			for (idx in json.data){				
				var li;

				if (json.data[idx].name != ''){
					li = $('<li id="mall'+json.data[idx].shop_id+'">' + json.data[idx].name + '(' + json.data[idx].sub_location.name + json.data[idx].address + json.data[idx].rooms + ')</li>');
					$('#mallname').val(json.data[idx].name);
					$('#address').val(json.data[idx].address);
					$('#parent_shop_id').val(json.data[idx].shop_id);
					$('#googlemap_lat').val(json.data[idx].googlemap_lat);
					$('#googlemap_lng').val(json.data[idx].googlemap_lng);
				}else{					
					li = $('<li id="mall'+json.data[idx].shop_id+'">' + json.data[idx].eng_name + '(' + json.data[idx].sub_location.name + json.data[idx].address + json.data[idx].rooms+ ')</li>');
					$('#mallname').val(json.data[idx].eng_name);
					$('#address').val(json.data[idx].address);
					$('#parent_shop_id').val(json.data[idx].shop_id);
					$('#googlemap_lat').val(json.data[idx].googlemap_lat);
					$('#googlemap_lng').val(json.data[idx].googlemap_lng);
				}
				li.data('mall', json.data[idx]);
				li.bind('click', function(){
					$('#mallname').val($(this).data('mall').name);
					$('#address').val($(this).data('mall').address);
					$('#parent_shop_id').val($(this).data('mall').shop_id);
					$('#googlemap_lat').val($(this).data('mall').googlemap_lat);
					$('#googlemap_lng').val($(this).data('mall').googlemap_lng);
					$('#mallname').attr({readonly:true});
					$('#mallname').css('background-color','#ebebe4');
					$('#reset').css({display:''});
					//$('#address').trigger('blur');
					var point = new GLatLng($('#googlemap_lat').val(), $('#googlemap_lng').val());
					map.clearOverlays();
					map.setCenter(point, 17);
					var marker = new GMarker(point, {draggable: true});
					GEvent.addListener(marker, "dragstart", function() {
						map.closeInfoWindow();
					});
					GEvent.addListener(marker, "dragend", function() {
						var mlatlng = marker.getLatLng();
						$('#googlemap_lat').val(mlatlng.lat());
						$('#googlemap_lng').val(mlatlng.lng());
					});
					map.addOverlay(marker);
					$(this).parents('#malllist').hide();
				});				
				$('#malllist').append(li);
			}
		}else{
			$('#nomalltip').show();			
		}
	});
}

function checkExistMall(root,mall_id){
	$().ready( function() {
		var uri = root+'?m=shop&a=getMall&mall_id='+ mall_id;
		$.get(uri, function(response){
			var json = $.parseJSON( response );
			if (json.data[0] != null){
				$('#mall').attr('checked',true).attr('checked',true);
				$('#address').val(json.data[0].address);
				$('#parent_shop_id').val(json.data[0].shop_id);
				$('#googlemap_lat').val(json.data[0].googlemap_lat);
				$('#googlemap_lng').val(json.data[0].googlemap_lng);
				$('#address').attr({readonly:true});
				$('#main_location_id').attr({disabled:true});	
				$('#sub_location_id').attr({disabled:true});			
				$('#address').css('background-color','#ebebe4');
				$('#mallname').css('background-color','#FFFFFF');
				$('#bymallselect').css("display","");
				$('#mallname').val(json.data[0].name);
			}
		});
	} );
		
}
/*End general function*/
