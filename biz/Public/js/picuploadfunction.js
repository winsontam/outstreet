function initialuploadelement(){
	$('Div').each(function(index) {
		if($(this).attr('ref')=='upload'){
			uploadpic($(this).attr('ref_id'));
		}
	});	
}
function deletepic(eleid){
	if(confirm('圖片刪除將無法復原, 確認刪除?')){		
		var file_path=$('#hidden_file_path'+eleid).val();		
		$.get(__OT_APP__+'?m=upload&a=delete&dest='+dest+'&file_path='+encodeURIComponent(file_path)+'&itemid='+itemid, function(response) {
			var json = $.parseJSON( response );			
			if(json.success){
				alert('成功刪除圖片');
				$('#hidden_file_path'+eleid).remove();
				$('#files'+eleid).find('img').fadeOut('slow', function() {
    				$('#files'+eleid).html('');
  				});				
				$('#deletepic'+eleid).remove();	
				$('#pic_box'+eleid).append('<div id="upload'+eleid+'" ref="upload" ref_id="'+eleid+'" class="upload">上載</div>');
				uploadpic(eleid);
			}else{
				alert('無法刪除圖片');
			}
		});
	}
}
function uploadpic(eleid){
	var btnUpload=$('#upload'+eleid);
	new AjaxUpload(btnUpload, {
		action: __OT_APP__+'?m=upload&a=upload',
		name: '#upload'+eleid,
		responseType: 'JSON',
		data:{
            timestamp: timestamp,
            dest: dest,
			itemid: itemid,
			name: '#upload'+eleid
        },
		onSubmit: function(file, ext){
			if(confirm('確認要上載圖片?')){			
				if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
					  // check for valid file extension				
					$('#status'+eleid).text('Only JPG, PNG or GIF files are allowed');				
					return false;
				}				
				$('#upload'+eleid).hide();
				$('#status'+eleid).text('Uploading...');				
			}else{
				return false;
			}
		},
		onComplete: function(file, json){			
			$('#status'+eleid).text('');
			if(json.status==0){				
				$('#files'+eleid).hide();
				$('#files'+eleid).html('<img src="'+json.oustreet_dir+json.file+'" class="image"/>');
				$('#files'+eleid).fadeIn('slow');
				$('#upload'+eleid).remove();
				$('#pic_box'+eleid).append('<div id="deletepic'+eleid+'" class="deletepic" onclick="deletepic('+eleid+');">刪除</div>');
				if (dest == 'brand')
					{ $('#pic_box'+eleid).append('<input id="hidden_file_path'+eleid+'" type="text" class="hidden_file_path" value="brand/'+json.file+'" />'); }
				else
					{ $('#pic_box'+eleid).append('<input id="hidden_file_path'+eleid+'" type="text" class="hidden_file_path" value="'+json.file+'" />'); }
			} else{				
				alert(json.message);
				$('#upload'+eleid).fadeIn();
			}
		}
	});
}