datepicker_zhTW = {
	closeText: "完成",
	currentText: "今天",
	nextText: "下頁",
	prevText: "上頁",
	monthNames: ["一月", "二月", "三月", "四月", "五月", "六月",
	"七月", "八月", "九月", "十月", "十一月", "十二月"],
	//monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul","Ags", "Sep", "Okt", "Nov", "Des"],
	dayNames: ["日", "一", "二", "三", "四",
	"五", "六"],
	dayNamesShort: ["日","一", "二", "三", "四", "五", "六"],
	dayNamesMin: ["日","一", "二", "三", "四", "五", "六"],
	dateFormat: 'yy-mm-dd',
	firstDay: 1,
	isRTL: false,
	showButtonPanel: true
 };