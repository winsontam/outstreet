<?php

class IndexAction extends BaseAction
{
	public function index()
	{
		$this->display();	
	}

	public function do_login()
	{
		$email = @$_REQUEST[ 'email' ];
		$password = @$_REQUEST[ 'password' ];

		$success = false;
		if ( $email && $password && $owner = D( 'OwnerFields' )->where( array('email'=> $email, 'is_disabled'=> 0))->find() )
		{
			if ( strtolower( $owner[ 'password' ] ) == strtolower( md5( $password ) ) )
			{
				D( 'OwnerFields' )->where( array( 'email' => $email ) )->setField( 'last_login', time() );

				$_SESSION[ 'owner_id' ] = $owner[ 'id' ];
				$_SESSION[ 'owner' ] = $owner;
				$success = true;
				write_log( 'general', $owner['id'], 'login' );
			}
		}
		
		echo json_encode( array( 'success' => $success ) );
	}

	public function do_logout()
	{
		write_log( 'general', $_SESSION[ 'owner_id' ], 'logout' );
		unset( $_SESSION[ 'owner_id' ], $_SESSION[ 'owner' ] );
		$this->redirect( 'index/index' ); 
	}

	public function forgetpw()
	{
		$this->display(); 
	}

	public function do_forgetpw()
	{
		$email = @$_REQUEST[ 'email' ];

		$success = false;
		if ( $email && $owner = D( 'OwnerFields' )->where( array('email'=> $email, 'is_disabled'=> 0))->find() )
		{
			$password = mt_rand(100000,999999);
			D( 'OwnerFields' )->where( array('email'=> $email))->setField('password', md5($password));

			$caption = '重設你OutStreet商戶資訊管理系統的登入密碼';
			$this->assign('password', $password);
    		$body = $this->fetch('email_forgetpw');
			$this->smtp_mail_log($email,$caption,$body,'',2,3);
			
			$success = true;
		}
		
		echo json_encode( array( 'success' => $success ) );
	}
}

?>