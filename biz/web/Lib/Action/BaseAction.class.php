<?php

class BaseAction extends Action
{
	var $shop_count;

	public function _initialize()
	{
		$non_login_sessions = array( 'index' => null, 'tmp' => null );

		$is_non_login_session = false;
		foreach ( $non_login_sessions AS $key => $val )
		{
			if ( strtolower( $key ) == strtolower( MODULE_NAME ) )
			{
				if ( !$val || strtolower( $val ) == strtolower( ACTION_NAME ) )
					{ $is_non_login_session = true; }
			}
		}
		
		if ( !$is_non_login_session && !$_SESSION[ 'owner_id' ] ) {
			$this->redirect( 'index/index' ); 
			exit();
		}
				
		$owner_id=$_SESSION['owner_id'];
		$is_brand=D('ShopBrand')->where('owner_id='.$owner_id)->getField('id');
		if($is_brand){	//Case A, Brand Owner
			$this->assign('brand_status',0);
			$_SESSION['brand_status']=0;
		}else{
			$shoplist=D('ShopOwnerMap')->table('shop_owner_map a')->field('b.id as id,b.brand_id as brand_id')->join('shop_fields b on a.shop_id = b.id')->where('a.owner_id='.$owner_id)->select();
			if($shoplist[0]['brand_id']==0){// Case C, no brand shop owner
				$this->assign('brand_status',2);
				$_SESSION['brand_status']=2;
				$this->assign('id',$shoplist[0]['id']);
			}else{
				if(sizeof($shoplist)==1){ // Case B, brand shop owner (Special Case for owning only 1 shop)
					$this->assign('brand_status',2);
					$_SESSION['brand_status']=2;
					$this->assign('id',$shoplist[0]['id']);
				}else{   // Case B, brand shop owner
					$_SESSION['brand_status']=1;
					$this->assign('brand_status',1);
				}
			}
		}		
	}

	public function selshop(){
		$owner_id=$_SESSION['owner_id'];
		
		$where=array();
		$brand_id=D('ShopBrand')->where('owner_id='.$owner_id)->getField('id');
		if($brand_id){
			$shop_count=D('ShopFields')->where('brand_id='.$brand_id)->count();
			$where['brand_id']=$brand_id;
		}else{
			$shop_count=D('ShopOwnerMap')->where('owner_id='.$owner_id)->count();
			$where['c.owner_id']=$owner_id;
		}
		
		$where['a.is_audit']=1;
		$shopList=D('Shop')
		->table('shop_fields a')
		->field('a.id as id,description,a.name as shop_name,b.name as sub_location_name, a.address, a.rooms')
		->join('location_sub b on b.id=a.sub_location_id')
		->join('shop_owner_map c on a.id=c.shop_id')		
		->where($where)
		->select();
		
		$ids = array();
		$errids = array();
		$ids = explode(',',$_REQUEST['ids']);		
		$errids = explode(',',$_REQUEST['errids']);
		$target = $_REQUEST['target'];

		$this->assign('ids', $ids);
		$this->assign('errids', $errids);
		$this->assign('target', $target);
		$this->assign('shop_count',$shop_count);
		$this->assign('shopList',$shopList);
		$this->display();
	}
	
	public function assign_shop(){
		$type = $_REQUEST['type'];
		$shop_ids = explode(',',$_REQUEST['ids']);
		$item_id = $_REQUEST['item_id'];		
		$return = array();
		switch($type){
			case 'pdf':
				$rights = check_right($type,$shop_ids,$item_id);
				if($rights['status']==0){
					M('shop_pdf_map')->where('pdf_id = '.$item_id)->delete();
					foreach($shop_ids as $key=>$shop_id){
						if($shop_id){
							$data = array();
							$data['shop_id'] = $shop_id;
							$data['pdf_id'] = $item_id;
							M('shop_pdf_map')->add($data);
						}
					}
				}
				break;
				case 'news':
				$rights = check_right($type,$shop_ids,$item_id);
				if($right['status']==0){
					M('shop_news_map')->where(array('news_id'=>$item_id))->delete();						
						foreach($shop_ids as $key=>$shop_id){
							if($shop_id && !in_array($shop_id,$rights['err_ids'])){								
								$data = array();
								$data['shop_id'] = $shop_id;
								$data['news_id'] = $item_id;
								M('shop_news_map')->add($data);
							}
						}
				}
				break;
			default:
				$rights = array('status'=>0);
		}
		echo json_encode($rights);
	}

	public function smtp_mail_log($sendto_mail, $subject="", $body="", $sendto_name="",$type, $source){

		vendor("phpmailer.class#phpmailer");
		$mail = new PHPMailer();
		$mail->IsSMTP(); // set mailer to use SMTP	
		$mail->SMTPAuth = true; // turn on SMTP authentication
		$mail->Host = C('SMTP_SERVER'); // specify main and backup server
		//$mail->SMTPSecure = "ssl";
		$mail->Port = 25;
		$mail->Username = C('SMTP_USERNAME'); // SMTP username
		$mail->Password = C('SMTP_PASSWORD'); // SMTP password
		$mail->From = C('SMTP_FROM');
		$mail->FromName = C('SMTP_FROMNAME');
		$mail->AddAddress($sendto_mail, $sendto_name);
		$mail->CharSet = 'UTF-8';
		$mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
		$mail->Body = $body;
		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
		$mail->WordWrap = 50;
		$mail->MsgHTML($body);
		$mail->IsHTML(true);
		if(!$mail->Send())
		{
			echo "Message could not be sent. <p>";
			echo "Mailer Error: " . $mail->ErrorInfo;
			exit;
		}
		else
		{
			//Start Save Email Log
			$memid = 0;
			$userDao = D('User');
			$where = array();
			$where['email'] = $sendto_mail;
			$userInfo = $userDao->where($where)->find();
			if(!empty($userInfo)){
				$memid = $userInfo['id'];
			}	
			
			//Start Insert to DB
			$emailLogDao = D('EmailLog');
			$emailLogDao->create();
			$data = array();
			$data['datetime'] = time();			
			$data['mem_id'] = $memid;
			$data['email_address'] = $sendto_mail;
			$data['type'] = $type;
			$data['source'] = $source;
			$emailLogDao->add($data);
			//End Insert to DB
				
			//End Save Email Log
		}
	}

}

?>