<?php

class ReviewAction extends BaseAction {

	public function index() {
		$owner_id=$_SESSION['owner_id'];
		
		$where=array();
		$is_brand=D('ShopBrand')->where('owner_id='.$owner_id)->getField('id');
		if($is_brand){
			$shop_count=D('ShopFields')->where('brand_id='.$is_brand)->count();
			$where['b.brand_id']=$is_brand;
		}else{
			$shop_count=D('ShopOwnerMap')->where('owner_id='.$owner_id)->count();
			$where['c.owner_id']=$owner_id;
		}

		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'a.create_time';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';

		$where['a.is_audit']=1;
		$where['a.shop_id']=array('neq',0);

		$count = D('ReviewFields')
		->table('review_fields a')
		->join('shop_fields b on a.shop_id=b.id')
		->join('shop_owner_map c on b.id=c.shop_id')
		->where($where)
		->count('distinct a.id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$reviewList=D('ReviewFields')
		->table('review_fields a')
		->field('a.id,a.title,a.create_time,a.description,b.name as shop_name,d.nick_name,e.name as sub_location_name,b.id as shop_id,a.user_id,f.rate_1')
		->join('shop_fields b on a.shop_id=b.id')
		->join('shop_owner_map c on b.id=c.shop_id')
		->join('user_fields d on a.user_id = d.id')
		->join('location_sub e on b.sub_location_id=e.id')
		->join('review_rate f on f.review_id = a.id')
		->where($where)
		->order($sort . ' ' . $sortorder)
		->limit($limit)
		->select();
		
		$this->assign('OUTSTREET_DIR',C('OUTSTREET_DIR'));
		$this->assign('shop_count',$shop_count);
		$this->assign('review_count',$count);
		$this->assign('reviewList',$reviewList);
		$this->assign('multipage',$multipage);
		$this->display();
	}

}

?>