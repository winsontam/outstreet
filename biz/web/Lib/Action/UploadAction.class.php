<?php
class UploadAction extends BaseAction{	
	public function upload(){
		$owner_id=$_SESSION['owner_id'];
		$timestamp = $_POST['timestamp'];
		$dest = $_POST['dest'];
		$itemid=$_REQUEST['itemid'];
		$name=$_REQUEST['name'];
		$uploadedfile = $_FILES[$name]['tmp_name'];
		list($width,$height)=getimagesize($uploadedfile);
		switch(strtoupper($dest)){
			case 'SHOP':
				$uploaddir = C('OUTSTREET_ROOT').C('SHOP_UPFILE_PATH');
				$display_path=C('OUTSTREET_DIR').C('SHOP_UPFILE_PATH');
				$field = D('ShopPicture');
			break;			
			case 'EVENT':	
				$uploaddir = C('OUTSTREET_ROOT').C('EVENT_UPFILE_PATH').'/';
				$display_path=C('OUTSTREET_DIR').C('EVENT_UPFILE_PATH').'/';				
			break;
			case 'NEWS':
				$uploaddir = C('OUTSTREET_ROOT').C('SHOPNEWS_UPFILE_PATH').'/';
				$display_path=C('OUTSTREET_DIR').C('SHOPNEWS_UPFILE_PATH').'/';
				$field = D('ShopnewsPicture');
				if($width>580 || $height>435){
					$return['message']='請上載圖片尺寸小於 580 * 435';
					$return['status']=1;
					echo json_encode($return);
					exit();
				}
			break;
			case 'BRAND':
				$uploaddir = C('OUTSTREET_ROOT').C('SHOP_UPFILE_PATH').'brand/';
				$display_path=C('OUTSTREET_DIR').C('SHOP_UPFILE_PATH').'brand/';
				$field = D('ShopBrand');
				$type = "direct";
				if($width!=200 && $height !=150){
					$return['message']='請上載圖片尺寸為200 * 150';
					$return['status']=1;
					echo json_encode($return);
					exit();
				}
			break;
			default:
				$uploaddir = C('OUTSTREET_ROOT').C('SHOP_UPFILE_PATH');
			break;		
		}
		$uploaddir = './'.$uploaddir;
		$file = $uploaddir . basename($_FILES['uploadfile']['name']);

		import('ORG.Net.UploadFile');
		$upload = new UploadFile();
		$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));
		$upload->saveRule = 'time';
		$upload->thumb = true;
		$upload->thumbMaxWidth = 120;
		$upload->thumbMaxHeight = 120;
		$upload->uploadReplace = true;
		$upload->savePath = $uploaddir;
		if($upload->upload()) {
			$upinfo = $upload->getUploadFileInfo();			
			switch(strtoupper($dest)){
				case 'BRAND':
					$data['file_path']="brand/".$upinfo[0]['savename'];
					$data['update_time']=time();
					$success=$field->where('id='.$itemid)->save($data);
				break;
				case 'NEWS':
					$data['file_path']=$upinfo[0]['savename'];
					$data['shop_news_id']=$itemid;
					$data['create_time']=time();
					$data['user_id']=$owner_id;
					$data['picstamp']=$timestamp;
					$success=$field->add($data);
				break;
				case 'SHOP':					
					$data['file_path']=$upinfo[0]['savename'];
					$data['create_time']=time();
					$data['shop_id']=$itemid;
					$data['owner_id']=$owner_id;
					$success=$field->add($data);
				break;
			}
			$upfilepath = $upinfo[0]['savename'];
			if(!$success){
				$return['status']=1;
				$return['message']=$upfileerr = $upload->getErrorMsg();				
			}else{
				$return['status']=0;
				$return['file']=$upfilepath;
				$return['oustreet_dir']=$display_path;
			}
		}else{
			$return['status']=1;
			$return['message']=$upfileerr = $upload->getErrorMsg();
		}
		echo json_encode($return);			
	}
	public function delete(){
		$dest = $_REQUEST['dest'];
		$itemid = $_REQUEST['itemid'];
		$file_path = $_REQUEST['file_path'];
		$where=array();
		switch(strtoupper($dest)){
			case 'SHOP':
				$uploaddir = C('OUTSTREET_ROOT').C('SHOP_UPFILE_PATH');
				$where['file_path']=$file_path;
			break;			
			case 'EVENT':	
				$uploaddir = C('OUTSTREET_ROOT').C('EVENT_UPFILE_PATH').'/';					
			break;
			case 'NEWS':
				$uploaddir = C('OUTSTREET_ROOT').C('SHOPNEWS_UPFILE_PATH').'/';
				$field = D('ShopnewsPicture');
				$where['shop_news_id']=$itemid;				
			break;
			case 'BRAND':
				$uploaddir = C('OUTSTREET_ROOT').C('SHOP_UPFILE_PATH');
				$field = D('ShopBrand');
				$type = "direct";
				$where['file_path']=$file_path;
			break;
			default:
				$uploaddir = C('OUTSTREET_ROOT').C('SHOP_UPFILE_PATH');
			break;		
		}
		@unlink($uploaddir.$file_path);
		@unlink($uploaddir.'thumb_'.$file_path);
		switch(strtoupper($dest)){
			case 'BRAND':
				@unlink($uploaddir.str_replace('brand/','brand/thumb_',$file_path));
				$data['file_path']='';
				$data['update_time']=time();
				$success=$field->where($where)->save($data);
				$return['success']=$success;
			break;
			case 'SHOP':				
				$success=M('ShopPicture')->where($where)->delete();
				$return['success']=$success;
			break;
			default:				
				$success=$field->where($where)->delete();
				$return['success']=$success;
			break;
		}
		echo json_encode($return);
	}
}
?>