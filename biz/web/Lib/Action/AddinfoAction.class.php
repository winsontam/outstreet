<?php
class AddInfoAction extends BaseAction {
	public function index()
	{
		$owner_id=$_SESSION['owner_id'];
		$simpleshop_id =0;
		$where=array();		
		$is_brand=D('ShopBrand')->where('owner_id='.$owner_id)->getField('id');
		if($is_brand){
			//echo 'is_brand';
			$shop_count=D('ShopFields')->where('brand_id='.$is_brand)->count();
			$where['a.brand_id']=$is_brand;
		}else{
			$shoplist=D('ShopOwnerMap')->table('shop_owner_map a')->field('b.id as id,b.brand_id as brand_id')->join('shop_fields b on a.shop_id = b.id')->where('a.owner_id='.$owner_id)->select();
			if($shoplist[0]['brand_id']==0){	//if no brand, set owner_id 
				$where['a.owner_id'] = $owner_id;
				$simpleshop_id= $shoplist[0]['id'];
			}else{	//else set brand id
				$where['a.brand_id'] = $shoplist[0]['brand_id'];
				if(sizeof($shoplist)==1){
					$simpleshop_id= $shoplist[0]['id'];
				}
			}
		}
		$this->assign('simpleshop_id',$simpleshop_id);
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'a.create_time';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';
		$where['a.is_audit'] = array('neq',-1);
		
		$count = D('ShopPdf')
		->table('shop_pdf a')
		->join('shop_pdf_map b on a.id=b.pdf_id')	
		->join('shop_fields c on c.id=b.shop_id')
		->join('shop_owner_map d on c.id=d.shop_id')
		->group('a.id')
		->where($where)
		->count('distinct a.id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;	
		
		$shoppdfDao=D('ShopPdf');
		$pdfList=$shoppdfDao
		->table('shop_pdf a')
		->field('a.*')
		->join('shop_pdf_map b on a.id=b.pdf_id')	
		->join('shop_fields c on c.id=b.shop_id')
		->join('shop_owner_map d on c.id=d.shop_id')
		->group('a.id')		
		->where($where)
		->order($sort . ' ' . $sortorder)
		->limit($limit)
		->select();
		
		foreach($pdfList as &$pdf){
			$assignlist = M('shop_pdf_map')->where('pdf_id='.$pdf['id'])->select();
			$pdf['assigned'] =0;
			$pdf['operation_right']=check_enable($pdf['id'],$owner_id,'pdf');			
			foreach($assignlist as $assign_rec){
				$pdf['assigned'] ++;
				$pdf['assigned_ids'][] =$assign_rec['shop_id'];
			}
		}
		unset($pdf);
		$this->assign('multipage',$multipage);
		$this->assign('pdfList',$pdfList);
		$this->assign('pdf_count',$count);
		$this->display();
	}
	public function add(){
		$this->display();		
	}
	public function do_add()
	{
		$owner_id=$_SESSION['owner_id'];	
		$data = array();
		$where=array();		
		$brand_id=D('ShopBrand')->where('owner_id='.$owner_id)->getField('id');
		
		$return=array();
		$return['status'] = 0;
		
		if($brand_id){		//if it's a brand owner, write brand id
			$data['brand_id'] = $brand_id;
		}else{	//else
			$shoplist=D('ShopOwnerMap')->table('shop_owner_map a')->field('b.id as id,b.brand_id as brand_id')->join('shop_fields b on a.shop_id = b.id')->where('a.owner_id='.$owner_id)->select();
			if($shoplist[0]['brand_id']==0){	//if no brand, set owner_id 
				$data['owner_id'] = $owner_id;
			}else{	//else set brand id
				$data['brand_id'] = $shoplist[0]['brand_id'];
			}
		}
		
		if($_FILES['file']['type']!=''){
			if(strtolower($_FILES['file']['type'])!='application/pdf'){
					$return['status'] = 1;	//wrong
			}else{
				if (is_uploaded_file($_FILES['file']['tmp_name'])){
					$filename = time().rand(1000,9999).'.pdf';
					$data['create_time']=time();
					$data['is_audit']='0';
					$data['ori_file_name']=$_FILES['file']['name'];
					$data['file_path']=$filename;
					$data['description'] = htmlspecialchars(stripslashes($_REQUEST[ 'description' ]));
					$data['start_date'] = $_REQUEST[ 'time_start' ];
					$data['end_date'] = $_REQUEST[ 'time_end' ];					
					$id = M( 'shop_pdf' )->add( $data );
					move_uploaded_file($_FILES['file']['tmp_name'], C('OUTSTREET_ROOT').'/'.C('SHOP_PDF_PATH').$filename);
					write_log( 'pdf', $id, 'add' );
					$return['status'] = 0;	//normal return;
				}else{
					$return['status'] = 2;	//unknown error
				}
			}
		}else{
			$return['status'] = 1;
		}
		echo json_encode($return);
    }
	public function edit(){
		$id = $_REQUEST['id'];
		$pdfInfo = M('shop_pdf')->where('id='.$id)->find();
		$this->assign('pdfInfo',$pdfInfo);
		$this->display();		
	}
	public function do_edit()
	{
		$id = $_REQUEST[ 'id' ];
		$return=array();
		$return['status'] = 0;		

		if($_REQUEST['newupload']==1){
			if($_FILES['file']['type']!=''){
				if(strtolower($_FILES['file']['type'])!='application/pdf'){
						$return['status'] = 1;	//wrong
				}else{
					if (is_uploaded_file($_FILES['file']['tmp_name'])){
						$filename = time().rand(1000,9999).'.pdf';
						$data['update_time']=time();
						$data['is_audit']='0';
						$data['ori_file_name']=$_FILES['file']['name'];
						$data['file_path']=$filename;
						$data['description'] = htmlspecialchars(stripslashes($_REQUEST[ 'description' ]));
						$data['start_date'] = $_REQUEST[ 'time_start' ];
						$data['end_date'] = $_REQUEST[ 'time_end' ];
						M( 'shop_pdf' )->where( array( 'id' => $id ) )->save( $data );
						write_log( 'pdf', $id, 'edit' );
						move_uploaded_file($_FILES['file']['tmp_name'], C('OUTSTREET_ROOT').'/'.C('SHOP_PDF_PATH').$filename);
						$return['status'] = 0;	//normal return;
					}else{
						$return['status'] = 2;	//unknown error
					}
				}
			}
		}else{
			$data['description'] = htmlspecialchars(stripslashes($_REQUEST[ 'description' ]));
			$data['start_date'] = $_REQUEST[ 'time_start' ];
			$data['end_date'] = $_REQUEST[ 'time_end' ];
			M( 'shop_pdf' )->where( array( 'id' => $id ) )->save( $data );
			write_log( 'pdf', $id, 'edit' );
		}
		echo json_encode($return);
    }
	public function downloadpdf(){
		$filename = $_REQUEST['filename'];
		header('Content-type: application/pdf;');
		$u_agent = $_SERVER['HTTP_USER_AGENT'];
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		readfile(C('OUTSTREET_DIR').C('SHOP_PDF_PATH').$filename);
	}
	public function detail(){
		$pdfInfo = M('shop_pdf')->where('id='.$_REQUEST['id'])->find();
		$pdfInfo['shops'] = M('shop_pdf_map')->table('shop_pdf_map a')->field('b.id, b.name, b.address,b.rooms, c.name as sub_location_name')->join('shop_fields b on a.shop_id = b.id')->join('location_sub c on b.sub_location_id = c.id')->where('a.pdf_id ='.$_REQUEST['id'])->select();
		$this->assign('pdfInfo',$pdfInfo);		
		$this->display();
	}
	public function audit(){
		//Check assign shop limit
		$return = array();
		$id = $_REQUEST['id'];
		$ids = $_REQUEST['ids'];	
		$shop_ids = explode(',',$ids);
		$type = $_REQUEST['type'];
		$return['type'] = $type;
		if($type==0){ //audit
			if($ids ==''){
				$return['status'] = 2;
			}else{
				$rights = check_right('pdf',$shop_ids,$id);
				if($rights['status']==0){
					$data = array();
					$data['is_audit'] = 1;
					$data['update_time'] = time();
					$return['status'] = M('shop_pdf')->where('id='.$_REQUEST['id'])->save($data)?0:1;
					write_log( 'pdf', $id, 'audit' );
					//check mapping
					$count = M('shop_pdf_map')->where('pdf_id='.$_REQUEST['id'])->count();
					if($count==0){
						foreach($shop_ids as $shop_id){
							$data = array();
							$data['pdf_id'] = $_REQUEST['id'];
							$data['shop_id'] = $shop_id;
							M('shop_pdf_map')->add($data);
						}
					}
				}else{
					$return = $rights;
				}
			}
		}else{//unaudit
			$data = array();
			$data['is_audit'] = 0;
			$data['update_time'] = time();
			$return['status'] = M('shop_pdf')->where('id='.$_REQUEST['id'])->save($data)?0:1;
			write_log( 'pdf', $id, 'unaudit' );
		}
		echo json_encode($return);
	}
	public function delete(){
		$id = $_REQUEST['id'];
		$data = array();
		$data['is_audit'] = -1;
		$data['update_time'] = time();		
		$status = M('shop_pdf')->where('id='.$id)->save($data);
		write_log( 'pdf', $id, 'delete' );
		echo $status;
	}

}

?>