﻿<?php

class ShopnewsAction extends BaseAction
{
	public function index()
	{
		$owner_id=$_SESSION['owner_id'];	
		
		$where=array();
		$where['a.is_audit']=array('neq','-1');
		$is_brand=D('ShopBrand')->where('owner_id='.$owner_id)->getField('id');		
		if($is_brand){
			//echo 'is_brand_owner<br>';
			$shop_count=D('ShopFields')->where('brand_id='.$is_brand)->count();
			$where['a.brand_id']=$is_brand;
		}else{			
			$shop_count=D('ShopOwnerMap')->where('owner_id='.$owner_id)->count();			
			$shopIds=D('ShopOwnerMap')->where('owner_id='.$owner_id)->field('shop_id')->select();
			foreach($shopIds as $key=>$val){
				$shopinbrand=D('ShopFields')->where('id='.$val['shop_id'])->getField('brand_id');
				if($shopinbrand){
					//echo 'is_shop_in_brand_owner<br>';
					$where['a.brand_id']=$shopinbrand;
				}else{
					//echo 'is_shop_owner<br>';
					$where['a.owner_id']=$owner_id;
					if(count($shopIds)==1){
						$simpleshop_id=$shopIds[0]['shop_id'];
					}
				}
			}
		}		
		$this->assign('simpleshop_id',$simpleshop_id);
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'a.create_time';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';
		
		$count = D('ShopNews')
		->table('shop_news a')
		->join('shop_news_map b on a.id=b.news_id')	
		->join('shop_fields c on c.id=b.shop_id')
		->join('shop_owner_map d on c.id=d.shop_id')		
		->where($where)
		->group('a.id')
		->count('distinct a.id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;	
		
		$shopnewsDao=D('ShopNews');
		$newsList=$shopnewsDao
		->table('shop_news a')
		->field('a.*')
		->join('shop_news_map b on a.id=b.news_id')	
		->join('shop_fields c on c.id=b.shop_id')
		->join('shop_owner_map d on c.id=d.shop_id')		
		->where($where)
		->group('a.id')
		->order($sort . ' ' . $sortorder)
		->limit($limit)
		->select();
		foreach($newsList as $key=>$val){
			$assignList=D('ShopNewsMap')->where('news_id='.$val['id'])->select();
			$newsList[$key]['assigned']=count($assignList);
			$newsList[$key]['assigned_ids']='';
			$newsList[$key]['operation_right']=check_enable($val['id'],$owner_id,'news');			
			foreach($assignList as $key2=>$val2){
				$newsList[$key]['assigned_ids'].=$val2['shop_id'].',';
			}
			$newsList[$key]['assigned']=count($assignList);			
		}
		$this->assign('multipage',$multipage);
		$this->assign('newsList',$newsList);
		$this->assign('news_count',$count);
		//echo 'owner id:'.$owner_id;
		$this->display();
	}
	public function action(){
		$news_id=$_REQUEST['news_id'];
		$action=$_REQUEST['action'];
		$newsInfo=D('ShopNews')->where('id='.$news_id)->find();
		$shop_ids=$_REQUEST['shop_ids'];
		switch ($action){
			case 'edit':
			$this->assign('dest','news');
			$this->assign('picstamp',$newsInfo['picstamp']);
			$this->assign('newsInfo',$newsInfo);
			$this->display('edit');
			break;
			case 'add':
			$this->assign('dest','news');
			$this->assign('picstamp',time().rand(1000,9999));
			$this->display('edit');
			break;
			case 'preview':	
			$newsInfo['picture']=C('OUTSTREET_DIR').C('SHOPNEWS_UPFILE_PATH').D('ShopnewsPicture')->where('shop_news_id='.$news_id)->getfield('file_path');
			$assignedList = D('ShopNewsMap')
			->field('b.id,CONCAT(b.name,"(",c.name,"分店",")") as name,b.address')
			->table('shop_news_map a')
			->join('shop_fields b on a.shop_id=b.id')
			->join('location_sub c on b.sub_location_id=c.id')
			->where('a.news_id='.$news_id)
			->select();
			$this->assign('assignedList',$assignedList);
			$this->assign('newsInfo',$newsInfo);			
			$this->display('preview');
			break;
			case 'delete':
				$dao=D('ShopNews');
				$news_id=$_REQUEST['news_id'];
				$data=array();
				$data['is_audit']='-1';
				$data['update_time']=time();
				$success=$dao->where('id='.$news_id)->save($data);
				if($success){
					write_log( 'news', $news_id, 'delete' );
					$return['success']=$success;
				}else{
					$return['success']=0;
				}
				echo json_encode($return);				
			break;
			case 'submit_add':	
				$dao=D('ShopNews');
				$data=array();
				$data['title']=htmlspecialchars(stripslashes($_REQUEST['title']));
				$data['description']=htmlspecialchars(stripslashes($_REQUEST['description']));
				$data['time_start']=$_REQUEST['time_start'];
				$data['time_end']=$_REQUEST['time_end'];
				$data['picstamp']=$_REQUEST['picstamp'];
				$data['create_time']=time();
				$owner_id=$_SESSION['owner_id'];
				$is_brand=D('ShopBrand')->where('owner_id='.$owner_id)->getField('id');
				if($is_brand){
					$data['brand_id']=$is_brand;
				}else{
					$shopIds=D('ShopOwnerMap')->where('owner_id='.$owner_id)->field('shop_id')->select();
					foreach($shopIds as $key=>$val){
						$shopinbrand=D('ShopFields')->where('id='.$val['shop_id'])->getField('brand_id');
						if($shopinbrand){					
							$data['brand_id']=$shopinbrand;
						}else{
							$data['owner_id']=$owner_id;
						}
					}
				}
				$success=$dao->add($data);
				$data=array();
				$data['shop_news_id']=$success;
				D('ShopnewsPicture')->where('picstamp='.$_REQUEST['picstamp'])->save($data);
				if($success){
					write_log( 'news', $success, 'add' );
					$return['success']=$success;
					$return['message']=1;
				}else{
					$return['success']=0;
				}
				echo json_encode($return);
			break;
			case 'submit_edit':
				$dao=D('ShopNews');
				$news_id=$_REQUEST['news_id'];
				$data=array();
				
				$data['title']=htmlspecialchars(stripslashes($_REQUEST['title']));
				$data['description']=htmlspecialchars(stripslashes($_REQUEST['description']));				
				$data['time_start']=$_REQUEST['time_start'];
				$data['time_end']=$_REQUEST['time_end'];				
				$data['update_time']=time();
				$success=$dao->where('id='.$news_id)->save($data);
				if($success){
					$return['success']=$success;
					$return['message']=2;
					write_log( 'news', $news_id, 'edit' );
				}else{
					$return['success']=0;
				}
				echo json_encode($return);
			break;
			case 'audit':
				$return = array();			
				$shop_ids = explode(',',$_REQUEST['shop_ids']);				
				if($_REQUEST['shop_ids']==''){
					$return['status']=2;					
				}else{
					$audit=$_REQUEST['audit'];				
					if($audit==0){
						$rights = check_right('news',$shop_ids,$news_id);
						if($rights['status']==0){
							$data = array();
							$data['is_audit'] = 1;
							$data['update_time'] = time();
							$return['status'] = M('shop_news')->where('id='.$news_id)->save($data)?0:1;
							write_log( 'news', $news_id, 'audit' );
							foreach($shop_ids as $key=>$val){
								if($val){
									$where=array();
									$where['shop_id']=$val;
									$where['news_id']=$news_id;
									$check_map = D('ShopNewsMap')->where($where)->count();
									if($check_map==0){
										$data=array();
										$data['shop_id']=$val;
										$data['news_id']=$news_id;
										D('ShopNewsMap')->add($data);
									}
								}
							}
						}else{
							$return = $rights;
						}
					}else{//unaudit					
						$data = array();
						$data['is_audit'] = 0;
						$data['update_time'] = time();
						$return['status'] = M('shop_news')->where('id='.$news_id)->save($data)?0:1;
						write_log( 'news', $news_id, 'unaudit' );
					}
				}
				echo json_encode($return);				
			break;
		}		
	}
}
?>