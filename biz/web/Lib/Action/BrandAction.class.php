<?php

class BrandAction extends BaseAction
{
	protected function filter_vals( $array, $vals, $default = array() )
	{
		$vals = array_unique( array_intersect( $vals, $array ) );
		return ( count( $vals ) ) ? $vals : $default;
	}

	public function index()
	{
		$owner_id=$_SESSION['owner_id'];		
		$brandInfo=D('ShopBrand')->where('owner_id='.$owner_id)->find();
		
		$this->assign('brandInfo',$brandInfo);
		$this->assign('outstreet_dir',C('OUTSTREET_DIR'));
		$this->assign('shop_upload_path',C('SHOP_UPFILE_PATH'));
		$this->assign('picstamp', time() . rand(1000,9999));
		$this->assign('dest',"brand");
		$this->display();
	}
	public function do_edit()
	{
		$owner_id=$_SESSION['owner_id'];
		$shopbrandDao=D('ShopBrand');	
		$data['description']=htmlspecialchars(stripslashes($_REQUEST[ 'description' ]));
		$data['eng_description']=$_REQUEST['eng_description'];
		$data['email']=$_REQUEST['email'];
		if($_REQUEST['file_path']){
			$data['file_path']="brand/".$_REQUEST['file_path'];
		}		
		$brand_id=$_REQUEST['id'];
		
		$lastBrandInfo=$shopbrandDao->where('id='.$brand_id)->find();
	
		$isChanged = false;
		foreach( $data AS $key=>$val )
			{ if ( $val != $lastBrandInfo[$key] ) { $isChanged = $key; } }
	
		$data['update_time']=time();
		$success = $shopbrandDao->where('id='.$brand_id)->save($data);

		$shop_brand = serialize($shopbrandDao->where(array('id'=>$brand_id))->find());
		if ($isChanged)
			{ D('OmsBrandLog')->add(array('owner_id'=>$owner_id,'create_time'=>time(),'shop_brand'=>$shop_brand)); }

		if($success){
			write_log( 'brand', $brand_id, 'edit' );
			echo json_encode(array('success'=>1));
		}else{
			echo json_encode(array('success'=>0));
		}
	}	
}
?>