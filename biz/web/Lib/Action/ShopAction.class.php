<?php

include COMMON_PATH.'shopform.php';

class ShopAction extends BaseAction {

	public function listall()
	{
		if ($this->shop_count==1)
			{ header( 'location:' . __APP__ . '?m=shop&a=edit&id=' . D('ShopOwnerMap')->where(array('owner_id'=>$_SESSION[ 'owner_id' ]))->getField('id') ); }

		$owner_id = $_SESSION['owner_id'];

		$brand_id = D( 'ShopBrand' )->where( 'owner_id='.$owner_id )->getField( 'id' );

		
		$where = array();
		$where[ 'a.is_audit' ] = 1;

		if ( $brand_id )
			{ $where[ 'a.brand_id' ] = $brand_id; }
		else
			{ $where['b.owner_id'] = $owner_id; }

		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'a.create_time';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';


		$count = D( 'ShopFields' )->table( 'shop_fields a' )->join( 'shop_owner_map b on a.id = b.shop_id' )
			->where( $where )->count( 'distinct a.id' );

		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$list = D( 'ShopFields' )->field( 'a.*,c.name as mainloc, d.name as subloc' )->table( 'shop_fields a' )
			->join( 'shop_owner_map b on a.id = b.shop_id' )
			->join('location_main c on c.id=a.main_location_id')
			->join('location_sub d on d.id=a.sub_location_id')
			->where( $where )
			->order( array( $sort => $sortorder ) )
			->limit( $limit )->rows( 'id' );

		$this->assign('count',$count);
		$this->assign('list',$list);
		$this->assign('multipage',$multipage);
		$this->display();
	}

	public function edit()
	{
		$owner_id = $_SESSION['owner_id'];
		$id = $_REQUEST['id'];

		$shopInfo = D( 'ShopFields' )->where(array('id'=>$id))->find();
		$shopParent = D( 'ShopFields' )->where(array('parent_shop_id'=>$shopInfo['parent_shop_id']))->find();
		$shopTime = D( 'ShopTime' )->where(array('shop_id'=>$id))->rows('recurring_week');
		$shopCateIds = D( 'ShopCateMap' )->where(array('shop_id'=>$id))->rows(null,'cate_id');
		$shopCateNames = D( 'ShopCateMap' )->table('shop_cate_map a')->join('shop_cate b ON a.cate_id=b.id')->where(array('a.shop_id'=>$id))->rows(null,'name');

		$main_location_list = D( 'LocationMain' )->rows( 'id' );

		$this->assign('shopInfo',$shopInfo);
		$this->assign('shopParent',$shopParent);
		$this->assign('shopTime',$shopTime);
		$this->assign('shopCateNames',implode(',', $shopCateNames));
		$this->assign('shopCateIds',implode(',', $shopCateIds));
		$this->assign('main_location_list',$main_location_list);
		$this->assign('id',$id);
		$this->display();
	}

	public function do_edit()
	{
		$owner_id = $_SESSION['owner_id'];
		$id = $_REQUEST['shop_id'];
		$cate_ids = $_REQUEST['cate_ids'];
		$cate_ids = explode(',',$cate_ids);
		$loop = true;
		$loopIds = $cate_ids;
		$childrenIds = array();
		$parentIds = array();
		while($loop)
		{
			$cate = M('ShopCate')->where(array('parent_id'=>array('in',$loopIds)))->select();
			foreach ($cate AS $val) { $loopIds[] = $val['id']; $parentIds[] = $val['parent_id']; }
			if (count($loopIds)) { $childrenIds = array_merge($childrenIds,$loopIds); $loopIds = array(); }
			else { $loop = false; }
		}
		$cate_ids = array_diff($childrenIds,$parentIds);

		$success = true;

		$shopInfo = array();
		Import('ORG.Util.Input');
		$shopInfo['parent_shop_id'] = $_REQUEST['parent_shop_id'];
		$shopInfo['main_location_id'] = M('LocationSub')->where(array('id'=>$_REQUEST['sub_location_id']))->getField('main_location_id');
		$shopInfo['sub_location_id'] = $_REQUEST['sub_location_id'];
		$shopInfo['name'] = $_REQUEST['name'];
		$shopInfo['eng_name'] = Input::getVar($_REQUEST['eng_name']);
		$shopInfo['address'] = $_REQUEST['address'];
		$shopInfo['rooms'] = $_REQUEST['rooms'];
		$shopInfo['eng_address'] = Input::getVar($_REQUEST['eng_address']);
		$shopInfo['keyword'] = $_REQUEST['keyword'];
		$shopInfo['parking'] = $_REQUEST['parking'];
		//$shopInfo['description'] = $_REQUEST['description'];
		$shopInfo['telephone'] = (int) $_REQUEST['telephone'];
		$shopInfo['sub_telephone'] = (int) $_REQUEST['sub_telephone'];
		$shopInfo['email'] = $_REQUEST['email'];
		$shopInfo['googlemap_lng'] = $_REQUEST['googlemap_lng'];
		$shopInfo['googlemap_lat'] = $_REQUEST['googlemap_lat'];
		$shopInfo['eng_rooms'] = $_REQUEST['eng_rooms'];
		$shopInfo['eng_description'] = $_REQUEST['eng_description'];
		$shopInfo['eng_parking'] = $_REQUEST['eng_parking'];
		$shopInfo['eng_keyword'] = $_REQUEST['eng_keyword'];

		if($_REQUEST['is_closed']){ $shopInfo['shop_status']= '2'; }
		else if($_REQUEST['is_renovation']){ $shopInfo['shop_status']= '3'; }
		else if($_REQUEST['is_moved']){ $shopInfo['shop_status']= '1'; }
		else{ $shopInfo['shop_status']= '0'; }

		$lastShopInfo = D( 'ShopFields' )->where(array('id'=>$id))->find();

		$isChanged = false;
		foreach( $shopInfo AS $key=>$val )
			{ if ( $val != $lastShopInfo[$key] ) { $isChanged = true; } }

		$lastShopTime = D('ShopTime')->where( array( 'shop_id' => $id ) )->rows('recurring_week');
		D('ShopTime')->where( array( 'shop_id' => $id ) )->delete();

		$inputedDays = 0;
		$changedDays = 0;
		for( $i=0; $i<8; $i++ )
		{
			if(!@$_REQUEST['work_dayoff_'.$i])
			{
				if ($_REQUEST['work_start_time_'.$i] != $lastShopTime[$i+1]['start_time'] || $_REQUEST['work_end_time_'.$i] != $lastShopTime[$i+1]['end_time'])
					{ $changedDays++; }
				$inputedDays++;
			}
		}

		if ( $inputedDays != count($lastShopTime) || $changedDays )
			{ $isChanged = true; }

		for( $i=0; $i<8; $i++ )
		{
			if(!@$_REQUEST['work_dayoff_'.$i])
			{
				if ( @$_REQUEST['work_start_time_'.$i] && @$_REQUEST['work_end_time_'.$i] ) {
					$has_work_time = 1;
					D('ShopTime')->data( array(
						'shop_id' => $id,
						'start_time' => $_REQUEST['work_start_time_'.$i],
						'end_time' => $_REQUEST['work_end_time_'.$i],
						'recurring_week' => $i+1 ) )->add();
				}
			}
		}
		$shopInfo['work_time'] = $has_work_time;

		$last_cate_ids = D('ShopCateMap')->where( array( 'shop_id' => $id ) )->rows(null,'cate_id');
		$last_cate_ids = array_unique($this->mapShopCateRef($last_cate_ids));
		$cate_ids = array_unique($this->mapShopCateRef($cate_ids));


		D('ShopCateMap')->where( array( 'shop_id' => $id ) )->delete();

		foreach ( $cate_ids as $cate_id ) {
			D('ShopCateMap')->add( array( 'cate_id'=>$cate_id, 'shop_id'=>$id ) );
		}

		if ( array_diff( array_merge( $last_cate_ids, $cate_ids ), array_intersect( $last_cate_ids, $cate_ids ) ) )
			{ $isChanged = true; }

		$shopInfo['update_time'] = time();

		D('ShopFields')->where(array('id'=>$id))->save( $shopInfo );

		$shop_fields = serialize(D('ShopFields')->where(array('id'=>$id))->find());
		$shop_time = serialize(D('ShopTime')->where(array('shop_id'=>$id))->select());
		$shop_cate_map = serialize(D('ShopCateMap')->where(array('shop_id'=>$id))->select());
		if ($isChanged)
			{ D('OmsShopLog')->add(array('owner_id'=>$owner_id,'create_time'=>time(),'shop_fields'=>$shop_fields,'shop_time'=>$shop_time,'shop_cate_map'=>$shop_cate_map)); }

		write_log( 'shop', $id, 'edit' );


		if ($_REQUEST['new_description']!=$_REQUEST['old_description'])
		{
			$caption = '#' . $id . ' ' . $_REQUEST['name'] . ' 更改了簡介';
			$body = '
				<b>#' . $id . ' ' . $_REQUEST['name'] . ' 更改了簡介</b>
				<br/><br/>
				<b>新的簡介:</b>
				<br/>
				' . $_REQUEST['new_description'] . '
				<br/><br/>
				<b>舊的簡介:</b>
				<br/>
				' . $_REQUEST['old_description'] . '
			';
			$this->smtp_mail_log('info@outstreet.com.hk',$caption,$body,'',2,3);
		}


		echo json_encode( array( 'success' => $success ) );
	}

	public function upload()
	{
		$id = $_REQUEST['id'];

		$this->assign('id',$id);
		$this->assign('picstamp',$picstamp);
		$this->display();
	}

	public function do_upload()
	{
		$id = $_REQUEST['id'];
		$file_path = $_REQUEST['file_path'];

		M('ShopPicture')->add( array( 'shop_id'=>$id, 'file_path'=>$file_path ) );
	}	

	private function mapShopCateRef($shopcate){
		//mapping for shopcate reference id
		foreach($shopcate AS $key=>$cateId){
			$referenceId = D('ShopCate')->where("id=$cateId")->getfield('reference_id');
			if($referenceId!=0){
				$shopcate[$key] = $referenceId;		
			}
		}
		return array_unique($shopcate);
	}

	public function selshopcate(){
		//獲取Shop分類主目錄列表 壓入模板
		$cate = M('ShopCate')->getField('id,name');
		$level_1 = M('ShopCate')->where(array('level'=>1))->getField('id,parent_id');
		$level_2 = M('ShopCate')->where(array('level'=>2))->getField('id,parent_id');
		$level_3 = M('ShopCate')->where(array('level'=>3))->getField('id,parent_id');

		$cateList = array();
		foreach ( $level_1 AS $level_1_id => $level_1_parent_id )
		{
			if ($cate[ $level_1_id ])
				$cateList[ $level_1_id ][ 'name' ] = $cate[ $level_1_id ];
		}
		foreach ( $level_2 AS $level_2_id => $level_2_parent_id )
		{
			if ($cateList[$level_2_parent_id][ 'name' ] && $cate[ $level_2_id ])
				$cateList[$level_2_parent_id]['item'][ $level_2_id ][ 'name' ] = $cate[ $level_2_id ];
		}
		foreach ( $level_3 AS $level_3_id => $level_3_parent_id )
		{
			if ($cateList[$level_2[$level_3_parent_id]][ 'name' ] && $cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['name'] && $cate[ $level_3_id ])
				$cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['item'][ $level_3_id ][ 'name' ] = $cate[ $level_3_id ];
		}
		
		//Shop Cate Ref List
		$shopCateList = D('ShopCate')->field('id,reference_id')->select();
		$refList = array();
		foreach($shopCateList as $key=>$shopCate){
			$cateId = $shopCate['id'];
			$refId = $shopCate['reference_id'];
			if($refId!=0){
				$refList[$cateId][] = $refId;
				$refList[$refId][] = $cateId;
			}
		}
		foreach($refList as $key=>$ref){
			foreach($ref as $cateId){
				foreach($refList[$cateId] as $elem){
					$refList[$key][] = $elem;	
				}
			}
			$refList[$key] = array_unique($refList[$key]);
		}

		$refArray = D('ShopCate')->where('reference_id != 0')->rows(null,'id');

		$this->assign('refList', $refList);
		$this->assign('refArray', $refArray);
		$this->assign('cateList', $cateList);
		$this->display();
	}

	public function getMall(){
		$key=$_GET['key'];
		$mall_id = $_GET['mall_id'];
		$mainLocationId = $_GET['main_location_id'];
		$subLocationId = $_GET['sub_location_id'];
		$mainLocationDao = D('LocationMain');
		$subLocationDao = D('LocationSub');	
		$shopCateDao = D('ShopCate');
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');
		$cate_id = "175";
		
		if (!empty($key)){
			$where['name'] = array('like', '%'.$key.'%');
		}
		if (!empty($mainLocationId)){
			$where['main_location_id'] = $mainLocationId;
		}
		if (!empty($subLocationId)){
			$where['sub_location_id'] = $subLocationId;
		}
		if (!empty($mall_id)){
			$where['shop_fields.id'] = $mall_id;
		}

		$mallList = array();
		if (count($where))
		{
			$where['cate_id'] = $cate_id;
			$where['is_audit']="1";
			$mallList = D( 'ShopFields' )->join('shop_cate_map on shop_cate_map.shop_id = shop_fields.id')->where($where)->limit(20)->order('shop_fields.id asc')->select();		
			foreach($mallList as $key=>$mall){
				$mainLoc = $mainLocationDao->find($mall['main_location_id']);
				$subLoc = $subLocationDao->find($mall['sub_location_id']);
				$mallList[$key]['main_location']['name']=$mainLoc['name'];
				$mallList[$key]['sub_location']['name']=$subLoc['name'];
			}
		}
		$this->ajaxReturn($mallList, 'get', 1);
	}

	public function locationSub(){
		$mainLocationId = $_GET['mainlocationid'];

		$locationSubDao = D('LocationSub');
		 
		$where = array();
		$where['main_location_id'] = $mainLocationId;
		$SubLocationList = $locationSubDao->where($where)->select();
		 
		$this->ajaxReturn($SubLocationList);
	}
}

?>