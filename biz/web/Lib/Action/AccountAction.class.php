<?php

class AccountAction extends BaseAction
{
    public function edit()
	{
		$id = $_SESSION[ 'owner_id' ];

		$owner = D( 'OwnerFields' )->where( array( 'id' => $id ) )->find();

		$this->assign( 'owner', $owner );
		$this->display();
    }

    public function do_edit()
	{
		$id = $_SESSION[ 'owner_id' ];
		$data['o_company_name'] = $_REQUEST[ 'o_company_name' ];
		$data['o_contact_person'] = $_REQUEST[ 'o_contact_person' ];
		$data['o_telephone'] = $_REQUEST[ 'o_telephone' ];
		$data['o_position'] = $_REQUEST[ 'o_position' ];

		$success = true;

		D( 'OwnerFields' )->where( array( 'id' => $id ) )->save( $data );

		$owner = D( 'OwnerFields' )->where( array( 'id' => $id ) )->find();
		$_SESSION[ 'owner_id' ] = $owner[ 'id' ];
		$_SESSION[ 'owner' ] = $owner;

		echo json_encode( array( 'success' => $success ) );
    }

    public function change_password()
	{
		$this->display();
    }

    public function do_change_password()
	{
		$id = $_SESSION[ 'owner_id' ];
		$old_password = $_REQUEST[ 'old_password' ];
		$new_password = $_REQUEST[ 'new_password' ];

		$success = true;

		if ( !D( 'OwnerFields' )->where( array( 'id' => $id, 'password' => md5($old_password) ) )->count() ) { $success = false; }

		if ( $success ) { D( 'OwnerFields' )->where( array( 'id' => $id ) )->save( array( 'password' => md5($new_password) ) ); }

		$owner = D( 'OwnerFields' )->where( array( 'id' => $id ) )->find();
		$_SESSION[ 'owner_id' ] = $owner[ 'id' ];
		$_SESSION[ 'owner' ] = $owner;

		echo json_encode( array( 'success' => $success ) );
    }

}

?>