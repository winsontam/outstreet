<?php
class ShopFieldsModel extends BaseModel{

	protected $_link = array(

		'location_sub'=>array(
			'mapping_type'=>BELONGS_TO,
			'mapping_name'=>'sub_location',
			'foreign_key'=>'sub_location_id',
		),
		'location_main'=>array(
			'mapping_type'=>BELONGS_TO,
			'mapping_name'=>'main_location',
			'foreign_key'=>'main_location_id',
		),
		
	);
}
?>