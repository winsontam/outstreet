<?php
function check_right($type,$shop_ids,$item_id){
	$return = array();	
	$return['status'] = 0;
	$owner_id=$_SESSION['owner_id'];
	$brand_id=D('ShopBrand')->where('owner_id='.$owner_id)->getField('id');
	switch($type){
		case 'news':
			$newsInfo = M('shop_news')->where('id='.$item_id)->find();
			$time_start = $newsInfo['time_start'];
			$time_end = $newsInfo['time_end'];
			$where=array();
			$where['_string'] = '("'.$time_start.'" BETWEEN a.time_start AND a.time_end AND "'.$time_start.'" != a.time_end)
  									OR 
  								("'.$time_end.'" BETWEEN a.time_start AND a.time_end AND "'.$time_end.'" != a.time_start)
 									OR
  								("'.$time_start.'" <= a.time_start AND "'.$time_end.'" >= a.time_end )';
			$where['a.is_audit'] = 1;
			if($brand_id){
				$where['a.brand_id']=$brand_id;
			}else{
				$where['d.owner_id']=$owner_id;
			}
			foreach($shop_ids as $key=>$shop_id){
				if($shop_id){
					$shop_level = M('shop_fields')->where('id='.$shop_id)->getField('shop_level');
					$shop_num = getShopLevelSetting('news_num',$shop_level);					
					$where['b.shop_id'] = $shop_id;
					$count = D('ShopNews')
					->table('shop_news a')
					->join('shop_news_map b on a.id=b.news_id')
					->join('shop_fields c on c.id=b.shop_id')
					->join('shop_owner_map d on c.id=d.shop_id')
					->where($where)
					->count();
					if($count >= $shop_num){
						$return['status'] = 1;
						$return['err_ids'][] = $shop_id;
					}
				}
			}
		break;
		case 'pdf':
			$pdfInfo = M('shop_pdf')->where('id='.$item_id)->find();
			$time_start = $pdfInfo['start_date'];
			$time_end = $pdfInfo['end_date'];
			$where=array();
			
			$where['_string'] = '("'.$time_start.'" BETWEEN a.start_date AND a.end_date AND "'.$time_start.'" != a.end_date)
  									OR 
  								("'.$time_end.'" BETWEEN a.start_date AND a.end_date AND "'.$time_end.'" != a.start_date)
 									OR
  								("'.$time_start.'" <= a.start_date AND "'.$time_end.'" >= a.end_date )';
			$where['a.id'] = array('neq',$item_id);
			$where['a.is_audit'] = 1;			
			if($brand_id){
				$where['a.brand_id']=$brand_id;
			}else{
				$where['d.owner_id']=$owner_id;
			}
			
			foreach($shop_ids as $key=>$shop_id){
				if($shop_id){
					$shop_level = M('shop_fields')->where('id='.$shop_id)->getField('shop_level');
					$shop_num = getShopLevelSetting('pdf_num',$shop_level);
					$where['b.shop_id'] = $shop_id;
					if($brand_id){
						$count = D('ShopPdf')
						->table('shop_pdf a')
						->join('shop_pdf_map b on a.id=b.pdf_id')
						->join('shop_fields c on c.id=b.shop_id')
						->where($where)
						->count();
					}else{
						$count = D('ShopPdf')
						->table('shop_pdf a')
						->join('shop_pdf_map b on a.id=b.pdf_id')
						->join('shop_fields c on c.id=b.shop_id')
						->join('shop_owner_map d on c.id=d.shop_id')						
						->where($where)
						->count();
					}
					if($count >= $shop_num){
						$return['status'] = 1;
						$return['err_ids'][] = $shop_id;
					}
				}
			}
		break;
		case 'default':
			$return['status'] = 0;
	}
	return $return;
}

function check_enable($item_id,$owner_id,$type){
	switch($type){
		case 'news':
			$dbo=D('ShopNewsMap');
			$field_name = 'news_id';
		break;
		case 'pdf':
			$dbo=D('ShopPdfMap');
			$field_name = 'pdf_id';
		break;
	}
	$temp=$dbo->where($field_name.'='.$item_id)->field('shop_id')->select();
	$shop_ids=array();
	foreach($temp as $key=>$val){
		$shop_ids[$key]=$val['shop_id'];
	}
	$is_brand=D('ShopBrand')->where('owner_id='.$owner_id)->getField('id');
	if($is_brand){
		$temp=D('ShopFields')->where('brand_id='.$is_brand)->field('id as shop_id')->select();
	}else{
		$temp=D('ShopOwnerMap')->where('owner_id='.$owner_id)->field('shop_id')->select();
	}
	foreach($temp as $key=>$val){
		$owner_shop_ids[$key]=$val['shop_id'];
	}
	foreach($shop_ids as $key=>$val){		
		if(!in_array($val,$owner_shop_ids)){
			return 0;
		}
	}
	return 1;
}

function generateSelShop($itemid,$num=0,$txt=''){
	$result = '';
	$result .= '<a class="selshoptrigger" id="selshoptrigger'.$itemid.'">';
	$result .= '<input id="selshopbutton'.$itemid.'" type="button" class="btn02_xlong" value="';
	if($num==0){
		$result .= '選擇分店';
	}else{
		$result .= '已選擇'.$num.'個分店';
	}
	$result .= '" />';
	$result .= '<input type="hidden" name="selshoptxt'.$itemid.'" id="selshoptxt'.$itemid.'" value="'.$txt.'" />';
	$result .= '<input type="hidden" name="errorshoptxt'.$itemid.'" id="errorshoptxt'.$itemid.'" value="" />';
	$result .= '</a>';
	$result .= '<script>';
	$result .= '$(document).ready(function() {';
	$result .= '	initialselshop('.$itemid.');';
	$result .= '});</script>';
	echo $result;
}

function formatShopName($chi_name, $eng_name) {
	if($chi_name) {
		$string .= $chi_name.' ';
	}
	
	if(($eng_name != $chi_name) && ($eng_name)) {
		$string .= $eng_name;
	}

	return $string;	
}

function formatTelephone($telephone, $type, $startSalt = '',$endSalt = '',$status = false) {
	if($telephone == '' || $telephone  == 0) {
		if($type == 1) {
			$string = '暫無';
		}
	} else {
		if($type == 2) {
			$string = '&nbsp;&nbsp;|&nbsp;&nbsp;';
		}
		$string .= substr($telephone,0,4).' '.substr($telephone,4,4);
	}
	if(isset($string)&& $status){
		return '<script>print_ecode("'.ecode($startSalt,$endSalt, $string).'");</script>';
	}else{
		return $string;
	}
}

function formatInfo($word, $length){
	$word = strip_tags($word,'');
	
	if(mb_strlen($word,'UTF-8')>=$length){
		$word =	mb_substr($word,0,$length-4,'UTF-8').'...';
	}

	return $word;
}

function formatCategory($cateArray, $length, $doURL = true){
	$strlen = 0;
	$x = $i = 0;
	$keys = array();
	foreach( $cateArray as $val ){
		$strlen += mb_strlen( $val['name'], 'UTF-8' );
		$i++;
		if ( $strlen > $length && $i != 1 ) {
			$string .= '...';
			break;
		}
		
		$id = $val['reference_id']?$val['reference_id']:$val['id'];

		if (!in_Array($id,$keys)){
			if($doURL) {
				$string .= "<a href='__ROOT__/shop/lists/shop_cate_ids/".$id."'>".$val['name']."</a>&nbsp;";
			} else {
				$string .= ($x ? ',':'').$val['name']; $x++;
			}
		}
		$keys[] = $id;
	}
	
	return $string;
}

function formatAddr($shopArray, $type, $length, $startSalt='',$endSalt='',$status=false){
	$string = '<a href="__URL__/lists/mainlocationid/'.$shopArray['main_location_id'].'">';
	if($status){
		$string .= '<script>print_ecode("'.ecode($startSalt,$endSalt,$shopArray['main_location']['name']).'");</script>';
	}else{
		$string .= $shopArray['main_location']['name'];		
	}

	$string .= '</a>&nbsp;';
    $string .= '<a href="__URL__/lists/sublocationid/'.$shopArray['sub_location_id'].'">';

	if($status){
		$string .= '<script>print_ecode("'.ecode($startSalt,$endSalt,$shopArray['sub_location']['name']).'");</script>';		
	}else{
		$string .= $shopArray['sub_location']['name'];		
	}
	$string .= '</a>&nbsp;';

	if($type == 'shop') {
		$shopDao = D('Shop');
		if ($shopArray['parent_shop_id'] != 0) {
			$parentShopInfo = $shopDao->find($shopArray['parent_shop_id']);
			if($status){
				$string .= '<script>print_ecode("'.ecode($startSalt,$endSalt,$shopArray['address'].'&nbsp;').'");</script>';				
			}else{
				$string .= $shopArray['address'].'&nbsp;';	
			}
			$string .= '<a href="__URL__/view/id/'.$shopArray['parent_shop_id'].'">';
			$string .= '<script>print_ecode("'.ecode($startSalt,$endSalt,$parentShopInfo['name']).'");</script>';
			$string .= '</a>&nbsp;';
			if($status){
				$wholeAddress = '<script>print_ecode("'.ecode($startSalt,$endSalt,$shopArray['rooms']).'");</script>';				
			}else{
				$wholeAddress = $shopArray['rooms'];				
			}
		} else {
			if($status){
				$wholeAddress = '<script>print_ecode("'.ecode($startSalt,$endSalt,$shopArray['address'].' '.$shopArray['rooms']).'");</script>';
			}else{
				$wholeAddress = $shopArray['address'].' '.$shopArray['rooms'];
			}
		}
	}

	$string .= $wholeAddress;
	return $string;
}

function formatShopCategoryId($cate) {
	$id = $cate['reference_id']?$cate['reference_id']:$cate['id'];
	return $id;
}

function urlShortForm($url, $length){
	if(mb_strlen($url,'UTF-8')>=$length){
		$tempurl=mb_substr($url,0,$length-4,'UTF-8');
		$tempurl=$tempurl . " ... ";
		$url=$tempurl.mb_substr($url,-5,5,'UTF-8');
		return $url;
	}else{
		return $url;
	}
}

function chDate($number){
	switch ($number){
		case "1":
			$str = "d/m/Y";
		break;
		case "2":
			$str = "Y年m月d日";
		break;
		case "3":
			$str = "d/m";
		break;
	}
	return $str;
}

function week2ch($number) {
    $number = intval($number);
    $array  = array('日','一','二','三','四','五','六');
    $str = $array[$number];
    return $str;
}

function timediff($begin_time, $end_time){
	if($begin_time < $end_time){
		$starttime = $begin_time;
		$endtime = $end_time;
	}
	else{
		$starttime = $end_time;
		$endtime = $begin_time;
	}

	$timediff = $endtime - $starttime;
	$days = intval($timediff/86400);
	$remain = $timediff%86400;
	$hours = intval($remain/3600);
	$remain = $remain%3600;
	$mins = intval($remain/60);
	$secs = $remain%60;

	$res = array("day"=>$days,"hour"=>$hours,"min"=>$mins,"sec"=>$secs);
	return $res;
}

function hasSymbol($str){
	if(preg_match('/[_]/',$str)){
		return true;	
	}else{
		$str = trim($str);
		$strTemp = '';
		mb_regex_encoding( 'UTF-8' );
		mb_ereg_search_init( $str, '\w+' );
		while ( ( $match = mb_ereg_search_regs() ) ){
			$strTemp.=$match[0];
		}
		return (strlen($strTemp) == strlen($str))?false:true;
	}
}

function formatInfoUTF8($str,$length) { 
	$split=1;
	$curLen = 0;
	$isShorten=false;	
	$array = array(); 
	for ( $i=0; $i < strlen( $str ); ){ 
		$value = ord($str[$i]); 
		if($value > 127){ 
			if($value >= 192 && $value <= 223) 
				$split=2; 
			elseif($value >= 224 && $value <= 239) 
				$split=3; 
			elseif($value >= 240 && $value <= 247) 
				$split=4; 
		}else{ 
			$split=1; 
		}
		if($split==1){
			$curLen++;
		}else{
			$curLen+=2;
		}
		$key = NULL; 
		for ( $j = 0; $j < $split; $j++, $i++ ) { 
			if(ord($str[$i])!=13 && ord($str[$i])!=10){	//carriage return & line feed
				$key .= $str[$i];
			}
		} 
		if($curLen<$length-3){
			array_push( $array, $key ); 
		}else{
			$isShorten=true;
		}
	}
	if($isShorten){
		return implode('',$array).'...';
	}else{
		return implode('',$array);
	}
}

function getTimediffWord($timediff) {
	switch($timediff){
		case 'today':
			$word = '今天';
		break;
		case 'tomorrow':
			$word = '明天';
		break;
		case 'week':
			$word = '本週';
		break;
		case 'weekend':
			$word = '本週末';
		break;
		case 'month':
			$word = '本月';
		break;
	}
	return $word;
}

function getShopLevelSetting( $name, $level = 0 ) {
	$settings = array(
		'description_len' => array(
			0 => 20,
			1 => 50,
			2 => 100
		),
		'upload_movie' => array(
			0 => false,
			1 => false,
			2 => true
		),
		'pdf_num' => array(
			0 => 0,
			1 => 3,
			2 => 3
		),
		'add_website' => array(
			0 => false,
			1 => true,
			2 => true
		),
		'photo_num' => array(
			0 => 1,
			1 => 3,
			2 => 5
		),
		'news_num' => array(
			0 => 1,
			1 => 3,
			2 => 5
		),
		'keyword_num' => array(
			0 => 3,
			1 => 5,
			2 => 10
		),
		'notify_review' => array(
			0 => false,
			1 => true,
			2 => true
		),
	);

	return $settings[$name][$level];
}

function msubstr($str, $start=0, $length, $charset="utf-8", $suffix=true) {
	if(function_exists("mb_substr"))
	return mb_substr($str, $start, $length, $charset);
	elseif(function_exists('iconv_substr')) {
		return iconv_substr($str,$start,$length,$charset);
	}
	$re['utf-8']   = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
	$re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
	$re['gbk']	  = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
	$re['big5']	  = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
	preg_match_all($re[$charset], $str, $match);
	$slice = join("",array_slice($match[0], $start, $length));
	if($suffix) return $slice."…";
	return $slice;
}

function generatePicBox($type,$id){
	$picurl=C('OUTSTREET_DIR');
	switch ($type){
		case 'brand':
			$photo_num=1;
			$pic=D('ShopBrand')->where('id='.$id)->select();
			$picurl.=C('SHOP_UPFILE_PATH');
		break;
		case 'shop':
			$photo_num=getShopLevelSetting('photo_num',D('ShopFields')->where('id='.$id)->getField('shop_level'));
			$pic=D('ShopPicture')->where('shop_id='.$id)->select();
			$picurl.=C('SHOP_UPFILE_PATH').'thumb_';
		break;
		case 'news':
			$photo_num=1;
			$pic=D('ShopnewsPicture')->where('shop_news_id='.$id)->select();
			$picurl.=C('SHOPNEWS_UPFILE_PATH').'thumb_';
		break;
	}
	for($i=0;$i<$photo_num;$i++){
		$result.='<div id="pic_box'.$i.'" class="pic_box">';
		if($pic[$i]['file_path']){
			$result.='<div class="files" id="files'.$i.'" ><center><img src="'.$picurl.''.$pic[$i]['file_path'].'" onload="DrawImage(this,180,180)" align="absmiddle"/></center></div>';
			if ($type=='shop')
			{
				if ($pic[$i]['owner_id'])
					{ $result.='<div id="deletepic'.$i.'" class="deletepic" onclick="deletepic('.$i.');">刪除</div>'; }
				else
					{ $result.='<center>相片由會員提供<br/>如需更改，煩請<a href="mailto:comments@outstreet.com.hk">聯絡我們</a>。</center>'; }
			} else {
				$result.='<div id="deletepic'.$i.'" class="deletepic" onclick="deletepic('.$i.');">刪除</div>';
			}

			$result.='<span id="status'.$i.'" ></span>';
			$result.='<input id="hidden_file_path'.$i.'" class="hidden_file_path" type="text" value="'.$pic[$i]['file_path'].'" />';
			if ($type=='shop' && !$pic[$i]['owner_id']) 
				{ $photo_num++; }
		}else{
			$result.='<div class="files" id="files'.$i.'" ></div>';
			$result.='<span id="status'.$i.'" ></span>';
			$result.='<div id="upload'.$i.'" ref="upload" ref_id="'.$i.'" class="upload">上載</div>';			
		}
		$result.='</div>';
	}
	return $result;
}

function write_log( $item_name, $item_id, $item_action )
{
	$owner_id = $_SESSION['owner_id'];
	$data = array();
	$data['owner_id'] = $owner_id;
	$data['create_time'] = time();
	$data['item_name'] = $item_name;
	$data['item_id'] = $item_id;
	$data['item_action'] = $item_action;
	M('OmsLog')->add( $data );
}

function getHelp($id){
	$helps[0] = '選擇的類別越切合您業務的性質越容易被搜尋到。';	//所屬類別
	$helps[1] = '正確輸入地址，可讓客戶更容易找到您的分店。<br>
				例：新界葵芳興芳路223號新都會廣場3樓399A6鋪。<br>
				<ul><li>所屬地區：新界　葵芳</li>
				<li>所屬商埸：新都會廣埸</li>
				<li>街道：興芳路223號</li>
				<li>詳細地址： 3樓399A6鋪</li>';	//商戶地址
	$helps[2] = '簡單而清晰的分店介紹。<br>
				e.g, 售賣歐洲品牌手袋，價格合理';	//分店介紹
	$helps[3] = '提供分店鄰近的泊車資料。如果有免費泊車，更容易吸引駕駛者。';	//泊車資料
	$helps[4] = '試從客戶出發，選擇最能代表您的業務的關鍵字。<br>
				例：美甲店- 整指甲,Gel 甲,水晶甲';	//關鍵字
	$helps[5] = '簡單而清晰的品牌介紹。<br>e.g, 售賣歐洲品牌手袋，價格合理';	//商戶介紹
	$helps[6] = '提供您的分店實照圖，可讓客戶了解分店的環境，增加到臨消費的信心。';	//相片管理
	$helps[8] = '上載清晰的品牌商標，增加品牌曝光率。';	//品牌相片
	echo '<div>'.$helps[$id].'</div>';
}

?>