var uni = document, txt = "", partner_pm = " ", partner_ctxt = " ", maxcount = 3;
var j = 6, r = 0, bi = function( a ) {
	return uni.getElementById( a )
};
function getkeywords( key ) {
	var match = window.location.href.match( key );
	if ( match == undefined ) {
		return""
	} else {
		return match ? match[1] : null
	}
}

var unikeyword = ( typeof zKeyword !== 'undefined' ) ? zKeyword : "";

//For mobile use
window.mobilecheck = function() {
	var check = false;
	( function( a ) {
		if ( /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|pad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test( a ) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test( a.substr( 0, 4 ) ) )
			check = true
	} )( navigator.userAgent || navigator.vendor || window.opera );
	return check;
}

function gaClick() {
	ga( 'tracker1.send', 'event', 'YSM', window.mobilecheck() ? 'click_mobile' : 'click_desktop', unikeyword + ' - ' + location.href );
}

//Check is mobile
if ( window.mobilecheck() ) {
	partner_pm = "uni_hk_outstreet_mobile_pm",
			partner_ctxt = "uni_hk_outstreet_mobile_ctxt";
	//Check mobile is search page
	if ( location.href.match( '/search' ) ) {
		//get mobile version unikeyword
		 if ( typeof zKeyword !== 'undefined' ) {
			var unikeyword = zKeyword;
		} else if ( $( '#category_id option:selected' ).text() != "選擇類別" ) {
			var unikeyword = $( '#category_id option:selected' ).text();
		} else if ( $( '.input' ).val() != null ) {
			var unikeyword = $( '.input' ).val().trim();
		} else {
			var unikeyword = "";
		}
		maxcount = 2;
	} else {
		maxcount = 1;
		 if ( typeof zKeyword !== 'undefined' ) {
			var unikeyword = zKeyword;
		} else if ( $( '.content div a' ).text() != " " ) {
			var unikeyword = $( '.content div a' ).text();
		} else {
			var unikeyword = "";
		}
	}

} else {
	partner_pm = "uni_hk_outstreet_pm",
			partner_ctxt = "uni_hk_outstreet_ctxt";
}


function ljs( e, t ) {
	if ( e ) {
		backfill = 1;
		tag = "https://js-apac-ss.ysm.yahoo.com/d/search/p/standard/js/hk/flat/mpd/rlb/?Keywords=" + encodeURIComponent( e ) + "&Partner=" + partner_pm + "&keywordCharEnc=utf8&outputCharEnc=utf8&mkt=hk&cb=" + ( "" + Math.random() ).substring( 2, 6 ) + "&maxCount=" + maxcount + "&urlFilters=uni_hk&serveUrl=" + encodeURIComponent( location.href )
	} else {
		backfill = 0;
		tag = "https://hk-cm.ysm.yahoo.com/js_flat_1_0/?config=20840581828&source=" + partner_ctxt + "&ctxtUrl=" + encodeURIComponent( location.href ) + "&ctxtId=entertainment&ctxtCat=hk_t1_default_entertainment&maxCount=" + maxcount + "&mkt=hk&cb=" + ( "" + Math.random() ).substring( 2, 6 )
	}
	var n = uni.createElement( "script" ), r = false, i = uni.getElementsByTagName( "head" )[0];
	n.src = tag;
	n.onload = n.onreadystatechange = function() {
		if ( !r && ( !this.readyState || this.readyState == "loaded" || this.readyState == "complete" ) ) {
			r = true;
			t();
			n.onload = n.onreadystatechange = null;
			i.removeChild( n )
		}
	};
	i.appendChild( n )
}
ljs( unikeyword, function() {
	var zl = zSr.length, e = 6;
	if ( e < zl ) {
		write( zSr, e )
	}
	if ( zl == 6 && backfill ) {
		ljs( "", function() {
			write( zSr, e )
		} )
	}
} );
function write( zSr, e ) {
	var zl = zSr.length;
	if ( typeof ( ysmright ) !== "undefined" ) {
		var advCount = 0, txt = "";
		if ( e >= zl ) {
			var e = 6;
			r++
		}
		if ( e < zl ) {
			txt += '<div style="margin-top:5px">';
			while ( e < zl && advCount < 3 ) {
				var d = zSr[e++];
				var u1 = zSr[e++];
				var c = zSr[e++];
				var t = zSr[e++];
				var s = zSr[e++];
				var u2 = zSr[e++];
				txt += '<div style="width:100%;cursor:pointer;" onclick="window.open(\'' + c + '\')">'
						+ '<table style="border-bottom: 1px solid #cccccc;width:300px;">'
						+ '<tbody>'
						+ '<th style="text-align:left;">' + t + '</th>'
						+ '<tr>'
						+ '<td>' + d + '<br>'
						+ '<a style="font-size:9pt;color:#228b22;">' + s + '</a>'
						+ '</td>'
						+ '</tr>'
						+ '</tbody>'
						+ '</table>';
				advCount++
			}
			txt += '</div>'
					+ '<div style="text-decoration:none;color:#cccccc;font-size:8pt;text-align:right;cursor:pointer;" onclick="window.open(\'http://hk.searchmarketing.yahoo.com/?o=HK0025\')">ads by yahoo</div>'
					+ '</div>';
			bi( "ysm_right" ).innerHTML = txt
		}
	}
	if ( typeof ( ysmdisplay ) !== "undefined" ) {
		for ( var i = 1; i <= ysmdisplay; i++ ) {
			if ( e < zl ) {
				var txt = "";
				var d = zSr[e++];
				var u1 = zSr[e++];
				var c = zSr[e++];
				var t = zSr[e++];
				var s = zSr[e++];
				var u2 = zSr[e++];
				txt += '<table width="100%" cellspacing="0" cellpadding="0">'
						+ '	<tr>'
						+ '		<td style="width: 35px; cursor: pointer;" onclick="gaClick();window.open(\'' + c + '\')"></td>'
						+ '		<td style="cursor: pointer;" onclick="gaClick();window.open(\'' + c + '\')">'
						+ '			<table>'
						+ '				<tr>'
						+ '					<td>'
						+ '						<div class="ot_d_name">'
						+ '							<span class="ot_d_merchant_chiname">'
						+ '								<a href="javascript:;" class="ot_t_main_link">' + t + '</a>'
						+ '							</span>'
						+ '						</div>'
						+ '						<div class="ot_d_type">贊助網站</div>'
						+ '						<div class="ot_d_address">' + s + '</div>'
						+ '						<div class="ot_d_guides_result_summary">' + d + '</div>'
						+ '					</td>'
						+ '				</tr>'
						+ '			</table>'
						+ '		</td>'
						+ '		<td style="width: 75px; cursor: pointer;" onclick="gaClick();window.open(\'' + c + '\')"></td>'
						+ '	</tr>'
						+ '	<tr>'
						+ '		<td style="cursor: pointer;" onclick="gaClick();window.open(\'' + c + '\')"></td>'
						+ '		<td style="cursor: pointer;" onclick="gaClick();window.open(\'' + c + '\')"></td>'
						+ '		<td>'
						+ '			<a href="javascript:;" style="color: #aaaaaa; font-size: 11px; text-decoration: none;" onclick="window.open(\'http://hk.searchmarketing.yahoo.com/?o=HK0025\')">'
						+ '				ads by yahoo'
						+ '			</a>'
						+ '		</td>'
						+ '	</tr>'
						+ '</table>'
			}
			bi( "ysm_display" + i ).innerHTML = txt
		}
	}

	if ( typeof ( ysmmobile_v ) !== "undefined" ) {
		if ( e >= zl ) {
			var e = 6;
			r++
		}
		if ( e < zl ) {
			var txt = " ";
			var d = zSr[e++];
			var u1 = zSr[e++];
			var c = zSr[e++];
			var t = zSr[e++];
			var s = zSr[e++];
			var u2 = zSr[e++];
			txt += '<div style="width:382px;height:86px;padding:6px 4px;position:relative;display:inline-block;vertical-align:middle;" onclick="window.open(\'' + c + "')\">";
			txt += '<div style="float:left;width:130px;overflow:hidden;margin-right:5px;"><img src="http://ysm.unimhk.com/thumbnail.php?l=' + s + '" style="width:130px;height:86px;"></div>';
			txt += '<div><a style="color:#00609a;font-size:11px;text-decoration:none;">' + t + '</a><br><a style="color:#666666;font-size:9pt;font-family:Tahoma;">' + d + '</a><br><a style="color:#888;font-size:8pt;text-decoration:none;">' + s + "</a></div>";
			txt += "</div>";
			txt += '<div><a style="text-decoration:none;color:#888;font-size:8pt;text-align:right;cursor:pointer;position: absolute;right: 0px;bottom: 0px;}" onclick="window.open(\'http://hk.searchmarketing.yahoo.com/?o=HK0025\')"><span>ads by yahoo</span></a></div>';
			bi( "ysm_mobile_v" ).innerHTML = txt
		}
	}

	if ( typeof ( mobysmdisplay ) !== "undefined" ) {
		for ( var i = 1; i <= mobysmdisplay; i++ ) {
			var txt = "";
			if ( e < zl ) {
				var d = zSr[e++];
				var u1 = zSr[e++];
				var c = zSr[e++];
				var t = zSr[e++];
				var s = zSr[e++];
				var u2 = zSr[e++];


				txt = '<table width="100%" cellspacing="0" cellpadding="0">'
						+ '	<tr>'
						+ '		<td>'
						+ '			<table width="100%" cellspacing="0" cellpadding="0" style="cursor: pointer;" onclick="gaClick();window.open(\'' + c + '\')">'
						+ '				<tr>'
						+ '					<td colspan="2"><b><a href="javascript:;" class="title">' + t + '</a></b></td>'
						+ '				</tr>'
						+ '				<tr>'
						+ '					<td class="label">類別 :</td>'
						+ '					<td>贊助網站</td>'
						+ '				</tr>'
						+ '				<tr>'
						+ '					<td class="label">網址 :</td>'
						+ '					<td>' + s + '</td>'
						+ '				</tr>'
						+ '				<tr>'
						+ '					<td class="label">簡介 :</td>'
						+ '					<td>' + d + '</td>'
						+ '				</tr>'
						+ '			</table>'
						+ '		</td>'
						+ '	</tr>'
						+ '	<tr>'
						+ '		<td align="right">'
						+ '			<a href="javascript:;" style="color: #aaaaaa; font-size: 11px; text-decoration: none;" onclick="window.open(\'http://hk.searchmarketing.yahoo.com/?o=HK0025\')">'
						+ '				ads by yahoo'
						+ '			</a>'
						+ '		</td>'
						+ '	</tr>'
						+ '</table>'
						+ '<br/>'

			}
			bi( "mobysm_display" + i ).innerHTML = txt
		}
	}
}
;