function setPic(eleid,picpath,desc,picid,path,url,from,picstamp,itemid){
	if (typeof(picid) != 'undefined' && picid!=''){
		var jPhoto = $('#photoAt'+eleid);
		var temp = '<a id="upfileAt'+eleid+'" class="thickbox" rel="upfiles" title="'+desc+'" href="'+path+'/'+picpath+'"><img src="'+path+'/'+picpath+'" width="120" height="120" /></a>';
		jPhoto.empty();
		jPhoto.append(temp);
		jPhoto.append('<br /><span rel="upfiles">' + desc + '</span>');
		tb_init('#upfileAt' + eleid);
		var jPhotoEdit = $('#peditAt'+eleid);
		jPhotoEdit.empty();
		temp = '<a href="'+url+'/deletePic/id/'+picid+'/eleid/'+eleid+'/from/'+from+'/picstamp/'+ picstamp +'/itemid/'+ itemid +'" target="deleteResult">刪除</a>';
		jPhotoEdit.append(temp);
	}else{
		reInitPic(eleid,from,picstamp,itemid,url);
	}
}

function reInitPic(eleid,from,picstamp,itemid,url){
	var jPhoto = $('#photoAt'+eleid);
	jPhoto.empty();
	var jPhotoEdit = $('#peditAt'+eleid);
	jPhotoEdit.empty();
	temp = '<a id="uplink'+eleid+'" href="'+url+'/uploadPic/eleid/'+eleid+'/picstamp/'+picstamp+'/from/'+from;
	if(itemid!=''){
		temp +='/itemid/'+itemid;
	}
	temp +='?height=200&width=430&modal=false" class="thickbox">上載</a>';
	jPhotoEdit.append(temp);
	tb_init('#uplink'+eleid);
}
