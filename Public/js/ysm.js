( function () {

	return;

    var token = 'token' + new Date().getTime();

    $.ajax( {
        url: '/tracking/code/token/' + token,
        cache: true,
        jsonp: false,
        jsonpCallback: token,
        dataType: 'jsonp',
        success: function ( response ) {

            var mobile = response.mobile;
            var code = response.code;

            function load( callback )
            {
                var loaded = false;
                var script = document.createElement( 'script' );
                var head = document.getElementsByTagName( 'head' )[0];

                script.src = 'https://s.yimg.com/uv/dm/scripts/syndication.js';
                script.onload = script.onreadystatechange = function () {
                    if ( !loaded && ( !this.readyState || this.readyState == 'loaded' || this.readyState == 'complete' ) ) {
                        loaded = true;
                        if ( callback ) {
                            callback();
                        }
                        script.onload = script.onreadystatechange = null;
                        head.removeChild( script );
                    }
                };
                head.appendChild( script );
            }


            $( function () {

                var keyword = ( typeof zKeyword !== 'undefined' ) ? zKeyword : null;

                load( function () {

                    addCSSRule( document.styleSheets[0], '#ypa-head iframe', 'width: 100%' );

                    if ( mobile ) {
                        window.ypaAds.insertMultiAd( {
                            ypaAdConfig: '00000032b',
                            ypaAdTypeTag: '',
                            ypaPubParams: {
                                query: keyword
                            },
                            ypaAdSlotInfo: [ {
                                    ypaAdSlotId: 'head',
                                    ypaAdDivId: 'ypa-head',
                                    ypaAdWidth: $( '<div id="ypa-head"></div>' )
                                            .prependTo( '#results' )
                                            .width()
                                            .toString(),
                                    ypaAdHeight: '1'
                                } ] } );
                    } else {
                        $( '<li id="ypa-head1" class="ot_d_alt_row"></li>' )
                                .insertBefore( '#ot_d_guides_result_box .ot_d_content ul li:nth-child(1)' );
                        $( '<li id="ypa-head2" class="ot_d_alt_row"></li>' )
                                .insertBefore( '#ot_d_guides_result_box .ot_d_content ul li:nth-child(6)' );
                        $( '<li id="ypa-head3" class="ot_d_alt_row"></li>' )
                                .insertBefore( '#ot_d_guides_result_box .ot_d_content ul li:nth-child(12)' );

                        window.ypaAds.insertMultiAd( {
                            ypaAdConfig: '0000002d2',
                            ypaAdTypeTag: '',
                            ypaPubParams: { query: keyword },
                            ypaAdSlotInfo: [ {
                                    ypaAdSlotId: 'listing',
                                    ypaAdDivId: 'ypa-head1',
                                    ypaAdWidth: $( '#ypa-head1' )
                                            .width()
                                            .toString(),
                                    ypaAdHeight: '1'
                                }, {
                                    ypaAdSlotId: 'listing',
                                    ypaAdDivId: 'ypa-head2',
                                    ypaAdWidth: $( '#ypa-head2' )
                                            .width()
                                            .toString(),
                                    ypaAdHeight: '1'
                                }, {
                                    ypaAdSlotId: 'listing',
                                    ypaAdDivId: 'ypa-head3',
                                    ypaAdWidth: $( '#ypa-head3' )
                                            .width()
                                            .toString(),
                                    ypaAdHeight: '1'
                                } ] } );
                    }


                    if ( keyword ) {
                        if ( mobile ) {
                            $( '<script' + '></script' + '>' ).attr( 'src', 'http://m.outstreet.com.hk/tracking/view/code/' + code ).appendTo( 'head' );
                            ga( 'tracker1.send', 'event', 'YSM', 'view_mobile', keyword + ' - ' + location.href );
                        } else {
                            $( '<script' + '></script' + '>' ).attr( 'src', 'http://outstreet.com.hk/tracking/view/code/' + code ).appendTo( 'head' );
                            ga( 'tracker1.send', 'event', 'YSM', 'view_desktop', keyword + ' - ' + location.href );
                        }
                    }

                } );

            } );

        }
    } );

    function addCSSRule( sheet, selector, rules, index ) {
        if ( "insertRule" in sheet ) {
            sheet.insertRule( selector + "{" + rules + "}", index );
        }
        else if ( "addRule" in sheet ) {
            sheet.addRule( selector, rules, index );
        }
    }

} )();