function jphoto_cycle(){
	$('#jposter_content_set').cycle({
		fx: 'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...		
		pause:1,
		timeout:8000		
	});
	$("#jposter_frame").mouseover(function(){
		$('.jposter_next').show();
		$('.jposter_prev').show();      	
    }).mouseout(function(){
		$('.jposter_next').hide();
		$('.jposter_prev').hide();		
    });
	$('.jposter_next').click(function(){
		$('#jposter_content_set').cycle('next');				
	});
	$('.jposter_prev').click(function(){
		$('#jposter_content_set').cycle('prev');
	});	
}