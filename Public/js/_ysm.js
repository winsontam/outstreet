( function() {

	var token = 'token' + new Date().getTime();

	$.ajax( {
		url: '/tracking/code/token/' + token,
		cache: true,
		jsonp: false,
		jsonpCallback: token,
		dataType: 'jsonp',
		success: function( response ) {

			var mobile = response.mobile;
			var code = response.code;

			var partner_pm = 'uni_hk_outstreet_pm';
			var partner_ctxt = 'uni_hk_outstreet_ctxt';
			var maxcount = 3;

			if ( mobile ) {
				partner_pm = 'uni_hk_outstreet_mobile_pm';
				partner_ctxt = 'uni_hk_outstreet_mobile_ctxt';
				maxcount = 2;
                return;
			}

			function load( keyword, callback )
			{
				var src = 'https://hk-cm.ysm.yahoo.com/js_flat_1_0/?'
						+ 'config=20840581828'
						+ '&source=' + partner_ctxt
						+ '&ctxtUrl=' + encodeURIComponent( location.href )
						+ '&ctxtId=entertainment'
						+ '&ctxtCat=hk_t1_default_entertainment'
						+ '&maxCount=' + maxcount
						+ '&mkt=hk'
						+ '&cb=' + String( Math.random() ).substring( 2, 6 );

				if ( keyword ) {
					src = 'https://js-apac-ss.ysm.yahoo.com/d/search/p/standard/js/hk/flat/mpd/rlb/?'
							+ 'Keywords=' + encodeURIComponent( keyword )
							+ '&Partner=' + partner_pm
							+ '&keywordCharEnc=utf8'
							+ '&outputCharEnc=utf8'
							+ '&mkt=hk'
							+ '&cb=' + String( Math.random() ).substring( 2, 6 )
							+ '&maxCount=' + maxcount
							+ '&urlFilters=uni_hk'
							+ '&serveUrl=' + encodeURIComponent( location.href );
				}

				var loaded = false;
				var script = document.createElement( 'script' );
				var head = document.getElementsByTagName( 'head' )[0];

				script.src = src;
				script.onload = script.onreadystatechange = function() {
					if ( !loaded && ( !this.readyState || this.readyState == 'loaded' || this.readyState == 'complete' ) ) {
						loaded = true;
						if ( callback ) {
							if ( zSr ) {
								var listings = [ ];
								for ( var i = 6; i < zSr.length; i = i + 6 ) {
									listings.push( {
										link: zSr[ i + 2 ],
										title: zSr[ i + 3 ],
										description: zSr[ i + 0 ],
										url: zSr[ i + 4 ]
									} );
								}
								if ( keyword && !listings.length ) {
									load( null, callback );
								} else {
									callback( keyword, listings );
								}
							}
						}
						script.onload = script.onreadystatechange = null;
						head.removeChild( script );
					}
				};
				head.appendChild( script );
			}


			$( function() {

				var keyword = ( typeof zKeyword !== 'undefined' ) ? zKeyword : null;

				load( keyword, function( keyword, listings ) {

					$.each( listings, function( index, listing ) {

						var id = '#ysmdisplay' + ( index + 1 );

						if ( $( id ).length ) {
							var tpl = '<table width="100%" cellspacing="0" cellpadding="0">'
									+ '	<tr>'
									+ '		<td style="width: 35px; cursor: pointer;" class="ysmclick"></td>'
									+ '		<td style="cursor: pointer;" class="ysmclick">'
									+ '			<table>'
									+ '				<tr>'
									+ '					<td>'
									+ '						<div class="ot_d_name">'
									+ '							<span class="ot_d_merchant_chiname">'
									+ '								<a href="javascript:;" class="ot_t_main_link">' + listing.title + '</a>'
									+ '							</span>'
									+ '						</div>'
									+ '						<div class="ot_d_type">贊助網站</div>'
									+ '						<div class="ot_d_address">' + listing.url + '</div>'
									+ '						<div class="ot_d_guides_result_summary">' + listing.description + '</div>'
									+ '					</td>'
									+ '				</tr>'
									+ '			</table>'
									+ '		</td>'
									+ '		<td style="width: 75px; cursor: pointer;" class="ysmclick"></td>'
									+ '	</tr>'
									+ '	<tr>'
									+ '		<td style="cursor: pointer;" class="ysmclick"></td>'
									+ '		<td style="cursor: pointer;" class="ysmclick"></td>'
									+ '		<td>'
									+ '			<a href="javascript:;" style="color: #aaaaaa; font-size: 11px; text-decoration: none;" onclick="window.open(\'http://hk.searchmarketing.yahoo.com/?o=HK0025\')">'
									+ '				ads by yahoo'
									+ '			</a>'
									+ '		</td>'
									+ '	</tr>'
									+ '</table>';

							if ( typeof ysmmobile !== 'undefined' ) {
								tpl = '<table width="100%" cellspacing="0" cellpadding="0">'
										+ '	<tr>'
										+ '		<td>'
										+ '			<table width="100%" cellspacing="0" cellpadding="0" style="cursor: pointer;" class="ysmclick">'
										+ '				<tr>'
										+ '					<td colspan="2"><b><a href="javascript:;" class="title">' + listing.title + '</a></b></td>'
										+ '				</tr>'
										+ '				<tr>'
										+ '					<td class="label">類別 :</td>'
										+ '					<td>贊助網站</td>'
										+ '				</tr>'
										+ '				<tr>'
										+ '					<td class="label">網址 :</td>'
										+ '					<td>' + listing.url + '</td>'
										+ '				</tr>'
										+ '				<tr>'
										+ '					<td class="label">簡介 :</td>'
										+ '					<td>' + listing.description + '</td>'
										+ '				</tr>'
										+ '			</table>'
										+ '		</td>'
										+ '	</tr>'
										+ '	<tr>'
										+ '		<td align="right">'
										+ '			<a href="javascript:;" style="color: #aaaaaa; font-size: 11px; text-decoration: none;" onclick="window.open(\'http://hk.searchmarketing.yahoo.com/?o=HK0025\')">'
										+ '				ads by yahoo'
										+ '			</a>'
										+ '		</td>'
										+ '	</tr>'
										+ '</table>'
										+ '<br/>';
							}

							$( tpl ).appendTo( id ).find( '.ysmclick' ).click( function() {

								window.open( listing.link );

								if ( keyword ) {
									if ( mobile ) {
										$( '<script' + '></script' + '>' ).attr( 'src', 'http://m.outstreet.com.hk/tracking/click/code/' + code ).appendTo( 'head' );
										ga( 'tracker1.send', 'event', 'YSM', 'click_mobile', keyword + ' - ' + listing.title + ' - ' + location.href );
									} else {
										$( '<script' + '></script' + '>' ).attr( 'src', 'http://outstreet.com.hk/tracking/click/code/' + code ).appendTo( 'head' );
										ga( 'tracker1.send', 'event', 'YSM', 'click_desktop', keyword + ' - ' + listing.title + ' - ' + location.href );
									}
								}

							} );
						}

					} );

					if ( keyword ) {
						if ( mobile ) {
							$( '<script' + '></script' + '>' ).attr( 'src', 'http://m.outstreet.com.hk/tracking/view/code/' + code ).appendTo( 'head' );
							ga( 'tracker1.send', 'event', 'YSM', 'view_mobile', keyword + ' - ' + location.href );
						} else {
							$( '<script' + '></script' + '>' ).attr( 'src', 'http://outstreet.com.hk/tracking/view/code/' + code ).appendTo( 'head' );
							ga( 'tracker1.send', 'event', 'YSM', 'view_desktop', keyword + ' - ' + location.href );
						}
					}

				} );

			} );

		}
	} );

} )();