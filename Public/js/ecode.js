var Ecode = {
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	_ssalt: "",
	_esalt: "",
	set_salt: function(ssalt,esalt){
		this._ssalt = ssalt;
		this._esalt = esalt;
	},
	decode : function (input) {
		if(input==""){
			return "";
		}
		var i = 0;	
		var c = c1 = c2 = 0;
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var output = "";
		var string = "";
		var re = new RegExp("^("+this._ssalt+")(.*?)("+this._esalt+")$","");
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
		input = input.replace(re,"$2");		
		while (i < input.length) {
			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));
			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;
			output = output + String.fromCharCode(chr1);
			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}
		}
		var i = 0;
		while ( i < output.length ) {
			c = output.charCodeAt(i);
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = output.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = output.charCodeAt(i+1);
				c3 = output.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
		}
		return string;
	}
}

function print_ecode(input){
	document.write(Ecode.decode(input));
}