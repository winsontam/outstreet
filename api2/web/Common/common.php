<?php
function getShopDetail($input,$type){
	switch($type){
		case 1:
			$getValue='description';
		break;
		case 2:
			$getValue='website';
		break;
		case 3:
			$getValue='email';
		break;
		case 4:
			$getValue='pictures';
		break;
	}

	if(is_array($input)){
		$shopInfo=$input;
	}else{
		$shop_id=$input;
		$shopDao=D('Shop');
		$shopInfo=$shopDao->relation(true)->find($shop_id);
	}

	if($shopInfo[$getValue] != ''){
		return $shopInfo[$getValue];
	}else{
		if($shopInfo['brand_id'] > 0){
			$brandInfo=D('Brand')->find($shopInfo['brand_id']);
			if($getValue=='pictures' && !empty($brandInfo['file_path'])){
				$brandInfo['pictures'][0]['file_path']=$brandInfo['file_path'];
			}
			return $brandInfo[$getValue];
		}else{
			return;
		}
	}
}

function generateShopWorkTime($work_time, $working_hour) {
	if($work_time != '' and $work_time != 0){
		
		$hour = array();
		foreach ($working_hour AS $val)
		{
			$hour[$val['recurring_week']] = $val;
		}
		$working_hour = $hour;

		ksort($working_hour);
		
		$whresult = array();
		$search_array = array();
		foreach($working_hour AS $key => $val)
		{
			if ( $key != 8 )
			{
				$whresult[$val['start_time'].'/'.$val['end_time']][] = $val['recurring_week'];
				$search_array[] = $val['recurring_week'];
			}
		}

		$j = 0;
		for($i = 1; $i < 8; $i++) {
			if (!in_array($i, $search_array)) {
				$dayoffday[$j] = $i;
				$j++;
			}
		}
		
		$no = 0;

		foreach ($whresult as $key => $value) {
			$rowA[$no] = $key;
			$prev = 0;
			$prevResult = 0;
			$prevAndResult = 0;
			foreach ($value as $key02 => $value02) {
				$now = $value02 - $prev;
				
				if($prevResult == 0) {
					if($value02 < 8) {
						$rowB[$no] .= '星期'.$value02;
					} else {
						$rowB[$no] .= $value02;	
					}
				} else {
					if($now == 1) {
						if($prevAndResult == 0) {
							$rowB[$no] .= '至'.$value02;
						} else {
							$rowB[$no] = substr($rowB[$no],0,strlen($rowB[$no])-1).$value02;
						}
						$prevAndResult++;
					} else {
						$rowB[$no] .= '及'.$value02;
					}
				}

				$prevResult++;
				$prev = $value02;
			}
			$no++;
		}
			
		// Start Day Off //
		if(count($dayoffday) > 0) {
			$prevResult = 0;
			$prevAndResult = 0;
			$rowA[$no] = '休息';

			foreach ($dayoffday as $key => $value) {
				if($prevResult == 0) {
					$prev = $value;	
				}
				
				$now = $value - $prev;
				
				if($prevResult == 0) {
					if($value < 8) {
						$rowB[$no] .= '星期'.$value;
					} else {
						$rowB[$no] .= $value;	
					}
				} else {
					if($now == 1) {
						if($prevAndResult == 0) {
							$rowB[$no] .= '至'.$value;
						} else {
							$rowB[$no] = substr($rowB[$no],0,strlen($rowB[$no])-1).$value;
						}
						$prevAndResult++;
					} else {
						$rowB[$no] .= '及'.$value;
					}
				}

				$prevResult++;
				$prev = $value;
			}
		}
		// End Day Off //
	
		// Start special days and holidays
		foreach ($special_working_hour as $hours){
			$idx++;
			$special_range_start[$idx] = $hours[start_time];
			$special_range_end[$idx] = $hours[end_time];
			if ($hours[recurring_week] > 0)
				$special_range_out[$idx] .= $day[$hours[recurring_week]];
			else{
				$special_range_out[$idx] .= date("m月d日",strtotime($hours[start_day]));
				if ($hours[start_day] != $hours[end_day])
					$special_range_out[$idx] .= "至" . date("m月d日",strtotime($hours[end_day]));
			}
		}
		// End special days and holidays

		$output = '';
		for ($i=0; $i<count($rowA); $i++){ //loop through regular week hours for output
			$day = array(1 => '一',2 => '二',3 => '三',4 => '四',5 => '五',6 => '六',7 => '日', 8=> '公眾假期');
		
			for($j = 1; $j < 9; $j++) {
				$rowB[$i] = str_replace($j,$day[$j],$rowB[$i]);
			}
			$output .= $rowB[$i];
			if($rowA[$i] == '休息') {
				$output .= '休息';
			} else {
				$output .= ' '.substr($rowA[$i],0,2) . ":". substr($rowA[$i],2,2) . " - " . substr($rowA[$i],5,2) . ":". substr($rowA[$i],7,2);	
			}
			$output .= "\n";
		}	
		$output .= '公眾假期';
		if ($working_hour[7]['start_time']&&$working_hour[7]['end_time'])
		{
			$output .= ' '.substr($working_hour[7]['start_time'],0,2) . ":". substr($working_hour[7]['start_time'],2,2) . " - " . substr($working_hour[7]['end_time'],0,2) . ":". substr($working_hour[7]['end_time'],2,2);	
		}
		else
		{
			$output .= '休息';	
		}
		$output .= "\n";
		
		for ($i=1; $i<=$idx; $i++){ //loop through special hours for output
			$output .= '';
			$output .= $special_range_out[$i];
			$output .= substr($special_range_start[$i],0,2) . ":". substr($special_range_start[$i],2) . " - " . substr($special_range_end[$i],0,2) . ":". substr($special_range_end[$i],2);
			$output .= "\n";
		}		
	} else {
		$output = '暫無';
	}
	
	return $output;
}
function week2ch($number) {
    $number = intval($number);
    $array  = array('日','一','二','三','四','五','六');
    $str = $array[$number];
    return $str;
}
function chDate($number){
	switch ($number){
		case "1":
			$str = "d/m/Y";
		break;
		case "2":
			$str = "Y年m月d日";
		break;
		case "3":
			$str = "d/m";
		break;
	}
	return $str;
}
function formatInfoUTF8($str,$length) { 
	$split=1;
	$curLen = 0;
	$isShorten=false;	
	$array = array(); 
	for ( $i=0; $i < strlen( $str ); ){ 
		$value = ord($str[$i]); 
		if($value > 127){ 
			if($value >= 192 && $value <= 223) 
				$split=2; 
			elseif($value >= 224 && $value <= 239) 
				$split=3; 
			elseif($value >= 240 && $value <= 247) 
				$split=4; 
		}else{ 
			$split=1; 
		}
		if($split==1){
			$curLen++;
		}else{
			$curLen+=2;
		}
		$key = NULL; 
		for ( $j = 0; $j < $split; $j++, $i++ ) { 
			if(ord($str[$i])!=13 && ord($str[$i])!=10){	//carriage return & line feed
				$key .= $str[$i];
			}
		} 
		if($curLen<$length-3){
			array_push( $array, $key ); 
		}else{
			$isShorten=true;
		}
	}
	if($isShorten){
		return implode('',$array).'...';
	}else{
		return implode('',$array);
	}
}
function rtnPromotionType($type) {
	switch($type) {
		case 1:
			$string = '大減價';
		break;
		case 2:
			$string = '商戶優惠';
		break;
		case 3:
			$string = '信用卡優惠';
		break;
	}
	return $string;
}
function formatTelephone($telephone, $type, $startSalt = '',$endSalt = '',$status = false) {
	if($telephone == '' || $telephone  == 0) {
		if($type == 1) {
			$string = '';
		}
	} else {
		if($type == 2) {
			$string = '&nbsp;&nbsp;|&nbsp;&nbsp;';
		}
		$string .= substr($telephone,0,4).' '.substr($telephone,4,4);
	}
	return $string;	
}
function check_utf8_encode($string){
	if(C('UTF8_ENCODE')){
		$string=utf8_encode($string);
	}
	return $string;
}
function utf8_encode_array($array){	
	if(is_array($array)){
		foreach($array as $key=>$val){
			$array[$key] = utf8_encode_array($val);
		}
		return $array;
	}else{
		return check_utf8_encode($array);
    }
}
function rtn_array_string($array){
	if(!$array) {
		$array = array();	
	}	
	return $array;
}
function formatCategory($cateArray, $length, $doURL = true){
	$strlen = 0;
	$x = $i = 0;
	$keys = array();
	foreach( $cateArray as $val ){
		$strlen += mb_strlen( $val['name'], 'UTF-8' );
		$i++;
		if ( $strlen > $length && $i != 1 ) {
			$string .= '...';
			break;
		}
		
		$id = $val['reference_id']?$val['reference_id']:$val['id'];

		if (!in_Array($id,$keys)){
			if($doURL) {
				$string .= "<a href='__ROOT__/shop/lists/shop_cate_ids/".$id."'>".$val['name']."</a>&nbsp;";
			} else {
				$string .= ($x ? ',':'').$val['name']; $x++;
			}
		}
		$keys[] = $id;
	}
	
	return $string;
}
function html2txt($document){
	$text = preg_replace('/\s+/',"\n",strip_tags($document));
	
	return strip_tags($document);
} 
function rtnPromotionImg($promotionInfo) {
	$promotionInfo['pictures'] = D('PromotionPicture')->field('file_path')->where('promotion_id='.$promotionInfo['promotion_id'])->select();
	if(!empty($promotionInfo['pictures'])){
		foreach($promotionInfo['pictures'] as &$picture){
			$picture = C('OUTSTREET_DIR').C('PROMOTION_UPFILE_PATH').$picture['file_path'];
		}
		unset($picture);
	}else{
		$string = C('OUTSTREET_DIR').'/Public/images/promotion/';
		if ($promotionInfo['type1']==1) {
			$string .= 'promotion_def_bigsale.jpg';
		} elseif($promotionInfo['type1']==2) {
			$string .= 'promotion_def_coupon.jpg';
		}
		$promotionInfo['pictures'] = array($string);
	}
	return $promotionInfo['pictures'];
}
?>