<?php
import( "Think.Core.Model.RelationModel" );

class AppFeaturedModel extends RelationModel
{

    protected $tableName = 'app_featured';

    protected $_link     = array(
        'shops'     => array(
            'mapping_type'   => HAS_MANY,
            'class_name'     => 'app_featured_shop',
            'mapping_fields' => 'shop_id',
            'mapping_name'   => 'shops',
            'foreign_key'    => 'featured_id',
        ),
    );

}
