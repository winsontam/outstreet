<?php

class NewsAction extends BaseAction
{

    public function listing()
    {
        $page = (int) $_REQUEST['page'] ? (int) $_REQUEST['page'] : 1;

        // $dbo
        $dbo = D('AppNews');

        // $where
        $where                   = array();
        $where['lang']         = $this->lang;
        $where['published_at'] = array('lt', date("Y-m-d H:i:s"));


        $total_number = $count        = $dbo
            ->where($where)
            ->count('DISTINCT id');

        import('ORG.Util.Page');
        $pagination = new Page($count, 100);

        $total_pages = ceil($total_number / $pagination->listRows);

        $rows = $dbo
            ->where($where)
            ->limit($pagination->firstRow.','.$pagination->listRows)
            ->relation(true)
            ->order('published_at DESC')
            ->select();

        $listings = array();

        $i = 0;
        foreach ($rows AS $row) {

            if ($i === 2) {
                /* $listings[] = array(
                  'type'		 => 'ads',
                  'ads_html'	 => '<a href="http://hk.yahoo.com" target="_blank">Go to yahoo! (Ads Test)</a>',
                  ); */
            } elseif ($page == 1 && in_array($i, array(1, 5, 9))) {
                $listings[] = array(
                    'type' => 'hotmob',
                );
            }

            $listing               = array();
            $listing['type']     = 'news';
            $listing['ads_html'] = '';
            $listing['news_id']  = $row['id'];
            $listing['title']    = $row['title'];
            $listing['content']  = $row['content'];
            $listing['link']     = $row['link'];

            $listing['small_photo_links'] = array();
            foreach ($row['photos'] AS $photo) {
                $listing['small_photo_links'][] = C("OUTSTREET_DIR").'/Public/uploadimages/app_news/small_'.$photo['file'];
            }

            $listing['photo_links'] = array();
            foreach ($row['photos'] AS $photo) {
                $listing['photo_links'][] = C("OUTSTREET_DIR").'/Public/uploadimages/app_news/large_'.$photo['file'];
            }

            $listing['published_at'] = date('j/n', strtotime($row['published_at']));

            $listing['next_type'] = 'detail';
            $listing['next_api']  = C('API_DIR').'?m=news&a=detail&news_id='.$row['id'].'&lang='.$this->lang;

            $listings[] = $listing;

            $i++;
        }

        $more_api = '';

        if ($page < $total_pages) {
            $more_api_params = array('m' => 'news', 'a' => 'listing');


            $more_api_params['page'] = $page + 1;
            $more_api_params['lang'] = $this->lang;
            $more_api                  = C('API_DIR').'?'.http_build_query($more_api_params);
        }

        $this->output(compact('total_number', 'total_pages', 'more_api', 'listings'));
    }

    public function detail()
    {
        $news_id = (int) $_REQUEST['news_id'];

        // $dbo
        $dbo = D('AppNews');

        $where       = array();
        $where['id'] = $news_id;

        $row = $dbo
            ->where($where)
            ->relation(true)
            ->find();

        $listing = array();

        if ($row) {
            $listing['news_id']  = $row['id'];
            $listing['title']    = $row['title'];
            $listing['content']  = $row['content'];
            $listing['link']     = $row['link'];

            $listing['small_photo_links'] = array();
            foreach ($row['photos'] AS $photo) {
                $listing['small_photo_links'][] = C("OUTSTREET_DIR").'/Public/uploadimages/app_news/small_'.$photo['file'];
            }

            $listing['photo_links'] = array();
            foreach ($row['photos'] AS $photo) {
                $listing['photo_links'][] = C("OUTSTREET_DIR").'/Public/uploadimages/app_news/large_'.$photo['file'];
            }

            $listing['published_at'] = date('j/n', strtotime($row['published_at']));
        }
        
        $this->output($listing);
    }
}