<?php

class OptionAction extends BaseAction
{

    public function category()
    {
        $categories = $this->getCategoriesByParentId();

        $this->output( $categories );
    }

    public function location()
    {
        $locations = array();

        $mainRows = D( 'LocationMain' )->select();

        foreach ( $mainRows AS $mainRow )
        {
            $location                       = array();
            $location[ 'main_location_id' ] = $mainRow[ 'id' ];

            if ( $this->lang == 'en' )
            {
                $location[ 'name' ] = $mainRow[ 'english_name' ];
            }
            else
            {
                $location[ 'name' ] = $mainRow[ 'name' ];
            }

            $location[ 'sub_locations' ] = array();

            $subRows = D( 'LocationSub' )
                ->where( array(
                    'main_location_id' => $mainRow[ 'id' ],
                    '_string'          => 'app_orders > 0',
                ) )
                ->order( 'app_orders DESC' )
                ->select();

            if ( $this->lang == 'en' )
            {
                foreach ( $subRows AS $subRow )
                {
                    $location[ 'sub_locations' ][] = array(
                        'sub_location_id' => $subRow[ 'id' ],
                        'name'            => $subRow[ 'english_name' ],
                    );
                }
            }
            else
            {
                foreach ( $subRows AS $subRow )
                {
                    $location[ 'sub_locations' ][] = array(
                        'sub_location_id' => $subRow[ 'id' ],
                        'name'            => $subRow[ 'name' ],
                    );
                }
            }

            $locations[] = $location;
        }

        $this->output( $locations );
    }

    public function getCategoriesByParentId( $parent_id = 0 )
    {
        $categories = array();

        $rows = D( 'AppCategory' )
            ->where( array( 'parent_id' => $parent_id, 'disabled' => 0 ) )
			->order( 'sort DESC, english_name ASC' )
            ->select();

        if ( $this->lang == 'en' )
        {
            foreach ( $rows AS $row )
            {
                $category                     = array();
                $category[ 'category_id' ]    = $row[ 'id' ];
                $category[ 'name' ]           = $row[ 'english_name' ];
                $category[ 'sub_categories' ] = $this->getCategoriesByParentId( $row[ 'id' ] );
                $categories[]                 = $category;
            }
        }
        else
        {
            foreach ( $rows AS $row )
            {
                $category                     = array();
                $category[ 'category_id' ]    = $row[ 'id' ];
                $category[ 'name' ]           = $row[ 'name' ];
                $category[ 'sub_categories' ] = $this->getCategoriesByParentId( $row[ 'id' ] );
                $categories[]                 = $category;
            }
        }

        return $categories;
    }
}
