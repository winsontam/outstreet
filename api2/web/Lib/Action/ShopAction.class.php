<?php

class ShopAction extends BaseAction
{

    public function category()
    {
        $category_id = (int) $_REQUEST['category_id'];
        $parent_id   = is_numeric($category_id) ? $category_id : 0;

        $dbo = D('AppCategory');

        $rows = $dbo
            ->where(array(
            'parent_id' => $parent_id,
            'disabled'  => 0,
        ));

        $rows = $rows->order('sort DESC, english_name ASC');
        $rows = $rows->select();

        /*
          if ( $parent_id )
          {
          array_unshift( $rows, $dbo
          ->where( array(
          'id'		 => $parent_id,
          'disabled'	 => 0,
          ) )
          ->find() );
          }
         */

        $categories = array();

        foreach ($rows AS $row) {
            $category                  = array();
            $category['category_id']   = $row['id'];
            $category['name']          = $row['name'];
            $category['english_name']  = $row['english_name'];
            $category['photo_link']    = $row['cover_file'] ? C('API_DIR').'/Public/cover/'.$row['cover_file'] : '';
            $category['cover_version'] = $row['cover_version'];
            $category['icon_link']     = $row['icon_file'] ? C('API_DIR').'/Public/icon/'.$row['icon_file'] : '';
            $category['icon_version']  = $row['icon_version'];

            if ($row['id'] == $parent_id) {
                $end = true;
            } else {
                $end = !D('AppCategory')->where(array('parent_id' => $row['reference_id'] ? $row['reference_id'] : $row['id']))->count('id');
            }

            if ($end) {
                $category['next_type'] = 'listing';
                $category['next_api']  = C('API_DIR').'?m=shop&a=listing&category_id='.$row['id'].'&lang='.$this->lang;
            } else {
                $category['next_type'] = 'category';
                $category['next_api']  = C('API_DIR').'?m=shop&a=category&category_id='.$row['id'].'&lang='.$this->lang;
            }

            $categories[] = $category;
        }

        if ($category_id) {
            $parentId = D('AppCategory')
                ->where(array('id' => $category_id))
                ->getField('parent_id');
        }

        $this->output(compact('parentId', 'categories'));
    }

    public function listing()
    {
        $query            = (string) $_REQUEST['query'];
        $query_type       = (string) $_REQUEST['query_type'];
        $category_id      = (int) $_REQUEST['category_id'];
        $category_ids     = (array) $_REQUEST['category_ids'];
        $main_location_id = (int) $_REQUEST['main_location_id'];
        $sub_location_id  = (int) $_REQUEST['sub_location_id'];
        $sub_location_ids = (array) $_REQUEST['sub_location_ids'];
        $latitude         = (float) $_REQUEST['latitude'];
        $longitude        = (float) $_REQUEST['longitude'];
        $distance         = (int) $_REQUEST['distance'];
        $page             = (int) $_REQUEST['page'] ? (int) $_REQUEST['page'] : 1;
        $sort             = is_numeric($_REQUEST['sort']) ? (int) $_REQUEST['sort'] : 1;

        if ($category_id) {
            $category_ids[] = $category_id;
        }


        $category_ids = $this->getRelatedCategoryIds($category_ids);

        if ($sub_location_id) {
            $sub_location_ids[] = $sub_location_id;
        }

        $location_ids = array();

        if ($main_location_id || $sub_location_ids) {
            $distance = 0;

            if ($main_location_id) {
                $location_ids = array_merge($location_ids, $this->getLocationIdsByMainLocationId($main_location_id));
            } elseif ($sub_location_ids) {
                $location_ids = array_merge($location_ids, $sub_location_ids);
            }
        }

        // $dbo
        $dbo = D('AppShop');

        // $where
        $where = array(
            'hidden' => 0,
        );

        // search
        $scoreQuerys = array();

        if ($query) {
            $tmp   = $query;
            $chars = array();

            if (preg_match_all('/\w+/', $tmp, $matches)) {
                foreach ($matches[0] AS $match) {
                    $chars['single'][] = $match;
                    $tmp               = str_replace($match, '', $tmp);
                }
            }

            mb_regex_encoding('UTF-8');
            mb_ereg_search_init($tmp, '\w+');

            while (( $matches = mb_ereg_search_regs())) {
                $chars['multiple'][] = $matches[0];
            }

            if ($query_type == 'name') {
                $searchFields = array(
                    'CONCAT( name, " - ", english_name )' => 10,
                );
            } elseif ($query_type == 'address') {
                $searchFields = array(
                    'address' => 10,
                );
            } else {
                $searchFields = array(
                    'CONCAT( name, " - ", english_name )' => 10,
                    'description'                         => 8,
                    'address'                             => 6
                );
            }

            $searchConditions = array();

            foreach ($chars AS $group) {
                foreach ($group AS $type => $char) {
                    if ($type == 'single') {
                        $searchConditions[] = ' LIKE "%'.$char.'%"';
                    } elseif ($type == 'multiple') {
                        if (( $length = preg_match_all('/./u', $char, $matches) ) > 1) {
                            $searchConditions[] = ' REGEXP "('.implode('|', $matches[0]).'){'.round($length * 0.8).','.$length.'}"';
                        } else {
                            $searchConditions[] = ' LIKE "%'.$char.'%"';
                        }
                    }

                    foreach ($searchFields AS $field => $score) {
                        $scoreQuerys[] = '( ( '.$field.' LIKE "%'.$char.'%" ) * '.$score.')'
                            .' + ( ( '.$field.' LIKE "%'.$char.'" ) * '.$score.')'
                            .' + ( ( '.$field.' LIKE "'.$char.'%" ) * '.$score.')';
                    }
                }
            }

            $searchQuerys = array();

            foreach ($searchConditions AS $condition) {
                $searchQuerys[] = 'CONCAT( '.implode(', " - " ,', array_keys($searchFields)).' )'.$condition;
            }

            $where['_string'] = $where['_string'] ? $where['_string'].' AND ' : '';
            $where['_string'] .= '('.implode(' AND ', $searchQuerys).')';
        }

        if ($location_ids) {
            $where['location_id'] = array('IN', $location_ids);
        }

        if ($category_ids) {
            $where['_string'] = $where['_string'] ? $where['_string'].' AND ' : '';
            $where['_string'] .= 'EXISTS ( SELECT app_shop_category.id FROM app_shop_category WHERE app_shop.id = app_shop_category.shop_id AND app_shop_category.category_id IN ( "'.implode('","',
                    $category_ids).'" ) )';
        }

        // latitude longitude search
        if ($latitude && $longitude && floor($latitude) == 22) {
            $distanceFormula = '111.1111'
                .' * DEGREES(ACOS(COS(RADIANS('.$latitude.'))'
                .' * COS(RADIANS(latitude))'
                .' * COS(RADIANS('.$longitude.' - longitude)) + SIN(RADIANS('.$latitude.'))'
                .' * SIN(RADIANS(latitude))))';
            $distanceField   = $distanceFormula.' AS distance';

            if ($distance) {
                $where['_string'] = $where['_string'] ? $where['_string'].' AND ' : '';
                $where['_string'] .= $distanceFormula.' * 1000 <= '.$distance;
            }
        } else {
            $distanceField = '0 AS distance';
        }

        if ($scoreQuerys) {
            $scoreQuerys = implode('+', $scoreQuerys);
            $scoreField  = '( '.$scoreQuerys.' ) AS scores';
        } {
            $scoreField = '0 AS scores';
        }

        if ($sort) {
            $order = 'app_shop.id DESC';
        } else {
            /* if ( $scoreQuerys )
              {
              $order = 'scores DESC';
              }
              else */if ($latitude && $longitude && floor($latitude) == 22) {
                $order = 'distance ASC';
            } else {
                $order = 'app_shop.id DESC';
            }
        }

        $total_number = $count        = $dbo
            ->where($where)
            ->count('DISTINCT id');

        import('ORG.Util.Page');
        $pagination = new Page($count, 100);

        $total_pages = ceil($total_number / $pagination->listRows);

        $rows = $dbo
            ->field('app_shop.*,'.$scoreField.','.$distanceField)
            ->where($where)
            ->limit($pagination->firstRow.','.$pagination->listRows)
            ->relation(true)
            ->order($order)
            ->select();

        $listings = array();

        $i = 0;
        foreach ($rows AS $row) {
            if ($i === 2) {
                /* $listings[] = array(
                  'type'		 => 'ads',
                  'ads_html'	 => '<html><body style="background-color: #000000;"></body></html>',
                  ); */
            } elseif ($page == 1 && in_array($i, array(1, 5, 9))) {
                $listings[] = array(
                    'type' => 'hotmob',
                );
            }

            $listing             = array();
            $listing['type']     = 'shop';
            $listing['ads_html'] = '';
            $listing['shop_id']  = $row['id'];

            $names          = array();
            $description    = '';
            $category_names = array();
            $categories     = array();
            $location_name  = '';
            $address        = '';

            if ($this->lang == 'en') {
                if ($row['english_name']) {
                    $names[] = $row['english_name'];
                }

                if ($row['name'] && $row['name'] != $row['english_name']) {
                    $names[] = $row['name'];
                }

                $description = $row['english_description'];

                foreach ($row['categories'] AS $category) {
                    $category_names[]            = $category['english_name'];
                    $categories[$category['id']] = $category['english_name'];
                }

                $location_name = $row['location']['english_name'];
                $address       = $row['english_address'];
            } else {
                if ($row['name']) {
                    $names[] = $row['name'];
                }

                if ($row['english_name'] && $row['name'] != $row['english_name']) {
                    $names[] = $row['english_name'];
                }

                $description = $row['description'];

                foreach ($row['categories'] AS $category) {
                    $category_names[]            = $category['name'];
                    $categories[$category['id']] = $category['name'];
                }

                $location_name = $row['location']['name'];
                $address       = $row['address'];
            }

            $listing['name']           = trim(implode(' - ', $names));
            $listing['description']    = trim($description);
            $listing['category_names'] = implode(', ', $category_names);
            $listing['categories']     = $categories;

            $listing['location_id']   = $row['location']['id'];
            $listing['location_name'] = $location_name;
            $listing['address']       = trim($address);

            $listing['latitude']  = $row['latitude'];
            $listing['longitude'] = $row['longitude'];
            if ($row['distance'] >= 1) {
                $listing['distance'] = number_format($row['distance'], 2).' km';
            } else {
                if ($row['distance'] >= 0.1) {
                    $listing['distance'] = number_format($row['distance'] * 1000, 0).' m';
                } else {
                    $listing['distance'] = '< 100 m';
                }
            }

            $listing['photo_link'] = '';
            foreach ($row['photos'] AS $photo) {
                $listing['photo_link'] = C("OUTSTREET_DIR").'/Public/uploadimages/app_shop/small_'.$photo['file'];
                break;
            }

            $listing['premium'] = $row['premium'];

            $listing['next_type'] = 'detail';
            $listing['next_api']  = C('API_DIR').'?m=shop&a=detail&shop_id='.$row['id'].'&lang='.$this->lang;

            $listings[] = $listing;

            $i++;
        }

        $category_name = '';

        if ($category_id) {
            $category = D('AppCategory')->where(array('id' => $category_id))->find();
            if ($this->lang == 'en') {
                $category_name = $category['english_name'];
            } else {
                $category_name = $category['name'];
            }
        }

        $more_api = '';

        if ($page < $total_pages) {
            $more_api_params = array('m' => 'shop', 'a' => 'listing');

            foreach (array('query', 'category_id', 'category_ids', 'main_location_id', 'sub_location_ids', 'latitude', 'longitude', 'distance', 'sort') AS $name) {
                if (isset($_REQUEST[$name])) {
                    $more_api_params[$name] = $_REQUEST[$name];
                }
            }

            $more_api_params['page'] = $page + 1;
            $more_api_params['lang'] = $this->lang;
            $more_api                = C('API_DIR').'?'.http_build_query($more_api_params);
        }

        $this->output(compact('category_id', 'category_name', 'total_number', 'total_pages', 'more_api', 'listings'));
    }

    public function detail()
    {
        $shop_id = (int) $_REQUEST['shop_id'];

        // $dbo
        $dbo = D('AppShop');

        $where       = array();
        $where['id'] = $shop_id;

        $row = $dbo
            ->where($where)
            ->relation(true)
            ->find();

        $listing = array();

        if ($row) {
            $listing['shop_id'] = $row['id'];

            $names                       = array();
            $description                 = '';
            $category_names              = array();
            $categories                  = array();
            $low_priority_category_names = array();
            $location_name               = '';
            $address                     = '';

            if ($this->lang == 'en') {
                if ($row['english_name']) {
                    $names[] = $row['english_name'];
                }

                if ($row['name'] && $row['name'] != $row['english_name']) {
                    $names[] = $row['name'];
                }

                $description = $row['english_description'];

                foreach ($row['categories'] AS $category) {
                    if ($category['sort'] >= 0) {
                        $category_names[] = $category['english_name'];
                    } else {
                        $low_priority_category_names[] = $category['english_name'];
                    }
                    $categories[$category['id']] = $category['english_name'];
                }

                $location_name = $row['location']['english_name'];
                $address       = $row['english_address'];
            } else {
                if ($row['name']) {
                    $names[] = $row['name'];
                }

                if ($row['english_name'] && $row['name'] != $row['english_name']) {
                    $names[] = $row['english_name'];
                }

                $description = $row['description'];

                $category_ids = array();
                foreach ($row['categories'] AS $category) {
                    if ($category['sort'] >= 0) {
                        $category_names[] = $category['name'];
                    } else {
                        $low_priority_category_names[] = $category['name'];
                    }
                    $categories[$category['id']] = $category['name'];
                }

                $location_name = $row['location']['name'];
                $address       = $row['address'];
            }

            $listing['name']           = trim(implode(' - ', $names));
            $listing['description']    = trim($description);
            $listing['category_names'] = trim(implode(', ', $category_names)."\n\n".implode(', ', $low_priority_category_names));
            $listing['categories']     = $categories;

            $listing['location_id']   = $row['location']['id'];
            $listing['location_name'] = $location_name;
            $listing['address']       = trim($address);

            $listing['latitude']      = $row['latitude'];
            $listing['longitude']     = $row['longitude'];
            $listing['telephone']     = $row['telephone'];
            $listing['website']       = $row['website'];
            $listing['opening_hours'] = trim($row['opening_hours']);

            $listing['premium'] = $row['premium'];

            $listing['photo_links'] = array();
            foreach ($row['photos'] AS $photo) {
                $listing['photo_links'][] = C("OUTSTREET_DIR").'/Public/uploadimages/app_shop/large_'.$photo['file'];
            }
        }

        $this->output($listing);
    }

    public function featured()
    {
        $featured_dbo = D('AppFeatured');
        $shop_dbo     = D('AppShop');

        $where                 = array();
        $where['lang']         = $this->lang;
        $where['hidden']       = 0;
        $where['published_at'] = array('elt', date('Y-m-d H:i:s'));

        $rows = $featured_dbo
            ->field('id,category_id,title,description,button')
            ->where($where)
            ->order('published_at DESC')
            ->relation(true)
            ->select();

        $all_featured = array();

        foreach ($rows AS $row) {
            $featured                = array();
            $featured['category_id'] = $row['category_id'];
            $featured['title']       = $row['title'];
            $featured['description'] = $row['description'];
            $featured['button']      = $row['button'];

            if ($row['shops']) {
                $shop_ids = array();

                foreach ($row['shops'] AS $shop) {
                    $shop_ids[] = $shop['shop_id'];
                }

                $rows = $shop_dbo
                    ->where(array('id' => array('IN', $shop_ids)))
                    ->relation(true)
                    ->select();

                foreach ($rows AS $row) {

                    $names          = array();
                    $description    = '';
                    $category_names = array();
                    $categories     = array();
                    $location_name  = '';
                    $address        = '';

                    if ($this->lang == 'en') {
                        if ($row['english_name']) {
                            $names[] = $row['english_name'];
                        }

                        if ($row['name'] && $row['name'] != $row['english_name']) {
                            $names[] = $row['name'];
                        }

                        $description = $row['english_description'];

                        foreach ($row['categories'] AS $category) {
                            $category_names[]            = $category['english_name'];
                            $categories[$category['id']] = $category['english_name'];
                        }

                        $location_name = $row['location']['english_name'];
                        $address       = $row['english_address'];
                    } else {
                        if ($row['name']) {
                            $names[] = $row['name'];
                        }

                        if ($row['english_name'] && $row['name'] != $row['english_name']) {
                            $names[] = $row['english_name'];
                        }

                        $description = $row['description'];

                        foreach ($row['categories'] AS $category) {
                            $category_names[]            = $category['name'];
                            $categories[$category['id']] = $category['name'];
                        }

                        $location_name = $row['location']['name'];
                        $address       = $row['address'];
                    }

                    $shop['shop_id']     = $row['id'];
                    $shop['name']        = trim(implode(' - ', $names));
                    $shop['description'] = trim($description);

                    $shop['category_names'] = implode(', ', $category_names);
                    $shop['categories']     = $categories;

                    $shop['location_id'] = $row['location']['id'];
                    $shop['location_name']   = $location_name;
                    $shop['address']         = trim($address);

                    $shop['photo_link'] = '';
                    foreach ($row['photos'] AS $photo) {
                        $shop['photo_link'] = C("OUTSTREET_DIR").'/Public/uploadimages/app_shop/small_'.$photo['file'];
                        break;
                    }

                    $featured['listings'][] = $shop;
                }
            }

            $all_featured[] = $featured;
        }

        $this->output($all_featured);
    }

    protected function getRelatedCategoryIds($ids)
    {
        $dao = D('AppCategory');

        $search_ids = array();
        $rows       = $dao->where(array('id' => array('IN', $ids), 'disabled' => 0))->select();
        foreach ($rows AS $row) {
            $search_ids[] = $row['reference_id'] ? $row['reference_id'] : $row['id'];
        }

        $related_ids = $search_ids;

        while (count($search_ids)) {
            $rows = $dao->where(array('parent_id' => array('IN', $search_ids), 'disabled' => 0))->select();

            $search_ids = array();

            foreach ($rows AS $row) {
                $related_ids[] = $row['reference_id'] ? $row['reference_id'] : $row['id'];
                $search_ids[]  = $row['id'];
            }
        }

        return $related_ids;
    }

    protected function getLocationIdsByMainLocationId($id)
    {
        $rows = D('LocationSub')->where(array('main_location_id' => $id))->select();

        $location_ids = array();

        foreach ($rows AS $row) {
            $location_ids[] = $row['id'];
        }

        return $location_ids;
    }
}