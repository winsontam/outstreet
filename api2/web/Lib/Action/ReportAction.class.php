<?php

class ReportAction extends BaseAction
{

	public function shop()
	{
		$shop_id		 = (int) $_REQUEST[ 'shop_id' ];
		$email			 = $_REQUEST[ 'email' ];
		$last_name		 = $_REQUEST[ 'last_name' ];
		$first_name		 = $_REQUEST[ 'first_name' ];
		$is_closed		 = $_REQUEST[ 'is_closed' ];
		$address		 = $_REQUEST[ 'address' ];
		$opening_hours	 = $_REQUEST[ 'opening_hours' ];
		$website		 = $_REQUEST[ 'website' ];

		$to		 = 'nobody@example.com';
		$subject = 'Report (shop)';
		$message = 'Shop Id: ' . $shop_id . "\n";
		$message .= 'Email: ' . $email . "\n";
		$message .= 'Last Name: ' . $last_name . "\n";
		$message .= 'First Name: ' . $first_name . "\n";

		if ( $is_closed )
		{
			$message .= 'Closed: Yes' . "\n";
		}

		if ( $address )
		{
			$message .= 'Address: ' . $address . "\n";
		}

		if ( $opening_hours )
		{
			$message .= 'Opening Hours: ' . $opening_hours . "\n";
		}

		if ( $website )
		{
			$message .= 'Website: ' . $website . "\n";
		}
	}

}
