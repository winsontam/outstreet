<?php

class BaseAction extends Action
{

    public $lang = 'zh';

    public function _initialize()
    {
        if ( !empty( $_REQUEST[ 'lang' ] ) )
        {
            switch ( $_REQUEST[ 'lang' ] )
            {
                case 'en':
                    $this->lang = $_REQUEST[ 'lang' ];
                    break;
            }
        }

        if ( $_SERVER[ 'REMOTE_ADDR' ] == '210.3.88.146' )
        {
            //$this->lang = 'en';
        }
    }

    public function output( $json )
    {
        if ( $_REQUEST[ 'dev' ] )
        {
            echo '<pre>';
            print_r( $json );
            echo '</pre>';
        }
        else
        {
            //echo urldecode( json_encode( $this->array_map_recursive( $json, 'urlencode' ) ) );
            echo json_encode( $json );
        }
    }

    public function array_map_recursive( $arr, $func )
    {
        $ret = array();

        foreach ( $arr as $key => $val )
        {
            if ( is_array( $val ) )
            {
                $ret[ $key ] = $this->array_map_recursive( $val, $func );
            }
            else
            {
                $ret[ $key ] = call_user_func( $func, $val );
            }
        }

        return $ret;
    }
}
