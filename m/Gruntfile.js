module.exports = function(grunt){

	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
		uglify: {
            build: {
                files: {
                    'main.min.js': ['Public/js/main.js']
                }
            }
        },
		watch: {
            js: {
                files: ['Public/js/main.js'],
                tasks: ['uglify']
            }
        },
    });

    grunt.registerTask('default', []);

};