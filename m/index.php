<?php

header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
define( 'THINK_PATH', './ThinkPHP' );
define( 'APP_NAME', 'web' );
define( 'APP_PATH', 'web/' );
require(THINK_PATH . '/ThinkPHP.php');
$App = new App();
$App->run();

function rrmdir( $dir )
{
	if ( is_dir( $dir ) )
	{
		$objects = scandir( $dir );
		foreach ( $objects as $object )
		{
			if ( $object != "." && $object != ".." )
			{
				if ( is_dir( $dir . "/" . $object ) )
					rrmdir( $dir . "/" . $object );
				else
					unlink( $dir . "/" . $object );
			}
		}
		rmdir( $dir );
	}
}

if ( isset( $_REQUEST[ 'clear' ] ) )
{
	rrmdir( APP_PATH . 'Runtime' );
}

?>