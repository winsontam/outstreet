
var gulp = require( 'gulp' );
var clean = require( 'gulp-clean' );
var concat = require( 'gulp-concat' );
var uglify = require( 'gulp-uglify' );
var jsObfuscator = require( 'gulp-js-obfuscator' );
var watch = require( 'gulp-watch' );
var gutil = require( 'gulp-util' );
var fs = require( 'fs' );
var path = require( 'path' );


gulp.task( 'vendor-js', function () {

    return gulp
            .src( './vendor/**/*.js' )
            .pipe( concat( 'vendor.js' ) )
            .pipe( uglify() )
            .pipe( gulp.dest( '../' ) );

} );

gulp.task( 'app-js', function () {

    return gulp
            .src( './app/**/*.js' )
            .pipe( uglify() )
            .pipe( jsObfuscator() )
            .pipe( gulp.dest( '../' ) );

} );

gulp.task( 'watch', function () {

    gulp.watch( './vendor/**/*.js', [ 'vendor-js' ] );
    gulp.watch( './app/**/*.js', [ 'app-js' ] );

} );

gulp.task( 'default', [
    'vendor-js',
    'app-js',
    'watch'
] );
