(function ($) {

    var iframe = $('<iframe/>')
        .attr('sandbox', 'allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-top-navigation')
        .attr('src', 'http://m.outstreet.com.hk/index/news')
        .css('width', $(window).width() + 'px')
        .css('height', $(window).height() + 'px')
        .css('marginBottom', $(window).height() + 'px')
        .appendTo('body');

})(jQuery);
