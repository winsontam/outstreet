<?php
	function pagenation($totalpage,$page){
		if ( $page > 1 ){
			echo '<a href="__APP__?'.http_build_query(array_merge($_REQUEST,array('page'=>$page-1))).'">&lt;上一頁</a>';
		}
		if ( $page < $totalpage ){
			echo '<a href="__APP__?'.http_build_query(array_merge($_REQUEST,array('page'=>$page+1))).'">下一頁&gt;</a>';
		}
	}
	function generateShopWebsite($website){
		if($website!='--' && $website!=''){
			echo '<a href="'.$website.'">'.$website.'</a>';
		}else{
			echo '--';
		}
	}
	function generateShopTelephone($telephone){
		if($telephone!='--' && $telephone!=''){
			echo '<a title="Call" href="tel:'.$telephone.'">'.$telephone.'</a>';
		}else{
			echo '--';
		}
	}
	function getsubcateids($category_ids,$type='shop'){
		switch($type){
			case 'shop':
				$shopCateDao=D('ShopCate');
				$category_ids = $shopCateDao->field('IF(reference_id>0,reference_id,id) as category_id')->where(array('id'=>array('in',$category_ids)))->rows(null,'category_id');
				$search_cate_ids = $category_ids;
				$loopids = array();
				$loopids = $category_ids;
				$loop = true;
				while($loop) {
					$shopCateList = $shopCateDao->where(array('type'=>array('neq','website'),'parent_id'=>array('in',$loopids)))->select();
					$cate = array();
					foreach( $shopCateList AS $val ) {
						$cate[] = $val['id'];
						$search_cate_ids[] = $val['reference_id']?$val['reference_id']:$val['id'];
					}
					if ( count($cate) ) { $loopids = $cate; }
					else { $loop = false; }
				}
				return $search_cate_ids;
			break;
			case 'event':
				$eventCateDao=D('EventCate');
				$category_ids = $eventCateDao->field('IF(reference_id>0,reference_id,id) as category_id')->where(array('id'=>array('in',$category_ids)))->rows(null,'category_id');
				$search_cate_ids = $category_ids;
				$loopids = array();
				$loopids = $category_ids;
				$loop = true;
				while($loop) {
					$eventCateList = $eventCateDao->where(array('parent_id'=>array('in',$loopids)))->select();
					$cate = array();
					foreach( $eventCateList AS $val ) {
						$cate[] = $val['id'];
						$search_cate_ids[] = $val['reference_id']?$val['reference_id']:$val['id'];
					}
					if ( count($cate) ) { $loopids = $cate; }
					else { $loop = false; }
				}
				return $search_cate_ids;
			break;
		}
	}

	function formatTelephone($telephone, $type, $startSalt = '',$endSalt = '',$status = false) {
		if($telephone == '' || $telephone  == 0) {
			if($type == 1) {
				$string = '';
			}
		} else {
			if($type == 2) {
				$string = '&nbsp;&nbsp;|&nbsp;&nbsp;';
			}
			$string .= substr($telephone,0,4).' '.substr($telephone,4,4);
		}
		return $string;
	}
	function getTopShopCate($category_id){
		$cateInfo=D('ShopCate')->where('id='.$category_id)->find();
		if($cateInfo['level']!='1'){
			$id=getTopShopCate($cateInfo['parent_id']);
		}else{
			$id=$cateInfo['id'];
		}
		return $id;
	}
	function getmainlocationList(){
		$main_location_list=D('LocationMain')->select();
		return $main_location_list;
	}
	function getsublocationList($main_location_id){
		$sub_location_list=D('LocationSub')->where('main_location_id='.$main_location_id)->select();
		return $sub_location_list;
	}
	function generate_categoryList($category_id,$type='shop'){
		switch($type){
			case 'shop':
			$dbo=D('ShopCate');
			$type=array('neq','iphone');
			break;
			case 'event':
			$dbo=D('EventCate');
			$type=array('neq','mobile');
			break;
		}
		if(empty($category_id)){
			$categoryList=$dbo->where(array('parent_id'=>0,'type'=>$type))->select();
		}
		else
		{
			$category_id = $dbo->field('IF(reference_id>0,reference_id,id) AS category_id')->where(array('id'=>$category_id))->find();
			$category_id = $category_id['category_id'];
			$request_cate_Info=$dbo->where(array('id'=>$category_id))->find();

			$categoryList=$dbo->where(array('parent_id'=>$category_id))->select();
			//$this->assign('request_cate_Info',$request_cate_Info);
			$result[1]=$request_cate_Info;
		}
		//$this->assign('categoryList',$categoryList);
		$result[0]=$categoryList;
		return $result;
	}
	function generateShopWorkTime($work_time, $working_hour) {
	if($work_time != '' and $work_time != 0){

		$hour = array();
		foreach ($working_hour AS $val)
		{
			$hour[$val['recurring_week']] = $val;
		}
		$working_hour = $hour;

		ksort($working_hour);

		$whresult = array();
		$search_array = array();
		foreach($working_hour AS $key => $val)
		{
			if ( $key != 8 )
			{
				$whresult[$val['start_time'].'/'.$val['end_time']][] = $val['recurring_week'];
				$search_array[] = $val['recurring_week'];
			}
		}

		$j = 0;
		for($i = 1; $i < 8; $i++) {
			if (!in_array($i, $search_array)) {
				$dayoffday[$j] = $i;
				$j++;
			}
		}

		$no = 0;

		foreach ($whresult as $key => $value) {
			$rowA[$no] = $key;
			$prev = 0;
			$prevResult = 0;
			$prevAndResult = 0;
			foreach ($value as $key02 => $value02) {
				$now = $value02 - $prev;

				if($prevResult == 0) {
					if($value02 < 8) {
						$rowB[$no] .= '星期'.$value02;
					} else {
						$rowB[$no] .= $value02;
					}
				} else {
					if($now == 1) {
						if($prevAndResult == 0) {
							$rowB[$no] .= '至'.$value02;
						} else {
							$rowB[$no] = substr($rowB[$no],0,strlen($rowB[$no])-1).$value02;
						}
						$prevAndResult++;
					} else {
						$rowB[$no] .= '及'.$value02;
					}
				}

				$prevResult++;
				$prev = $value02;
			}
			$no++;
		}

		// Start Day Off //
		if(count($dayoffday) > 0) {
			$prevResult = 0;
			$prevAndResult = 0;
			$rowA[$no] = '休息';

			foreach ($dayoffday as $key => $value) {
				if($prevResult == 0) {
					$prev = $value;
				}

				$now = $value - $prev;

				if($prevResult == 0) {
					if($value < 8) {
						$rowB[$no] .= '星期'.$value;
					} else {
						$rowB[$no] .= $value;
					}
				} else {
					if($now == 1) {
						if($prevAndResult == 0) {
							$rowB[$no] .= '至'.$value;
						} else {
							$rowB[$no] = substr($rowB[$no],0,strlen($rowB[$no])-1).$value;
						}
						$prevAndResult++;
					} else {
						$rowB[$no] .= '及'.$value;
					}
				}

				$prevResult++;
				$prev = $value;
			}
		}
		// End Day Off //

		// Start special days and holidays
		foreach ($special_working_hour as $hours){
			$idx++;
			$special_range_start[$idx] = $hours[start_time];
			$special_range_end[$idx] = $hours[end_time];
			if ($hours[recurring_week] > 0)
				$special_range_out[$idx] .= $day[$hours[recurring_week]];
			else{
				$special_range_out[$idx] .= date("m月d日",strtotime($hours[start_day]));
				if ($hours[start_day] != $hours[end_day])
					$special_range_out[$idx] .= "至" . date("m月d日",strtotime($hours[end_day]));
			}
		}
		// End special days and holidays

		$output = '';
		for ($i=0; $i<count($rowA); $i++){ //loop through regular week hours for output
			$day = array(1 => '一',2 => '二',3 => '三',4 => '四',5 => '五',6 => '六',7 => '日', 8=> '公眾假期');

			for($j = 1; $j < 9; $j++) {
				$rowB[$i] = str_replace($j,$day[$j],$rowB[$i]);
			}
			$output .= $rowB[$i];
			if($rowA[$i] == '休息') {
				$output .= '休息';
			} else {
				$output .= substr($rowA[$i],0,2) . ":". substr($rowA[$i],2,2) . " - " . substr($rowA[$i],5,2) . ":". substr($rowA[$i],7,2);
			}
			$output .= "<br>";
		}
		$output .= '公眾假期';
		if ($working_hour[7]['start_time']&&$working_hour[7]['end_time'])
		{
			$output .= substr($working_hour[7]['start_time'],0,2) . ":". substr($working_hour[7]['start_time'],2,2) . " - " . substr($working_hour[7]['end_time'],0,2) . ":". substr($working_hour[7]['end_time'],2,2);
		}
		else
		{
			$output .= '休息';
		}
		$output .= "<br>";

		for ($i=1; $i<=$idx; $i++){ //loop through special hours for output
			$output .= '';
			$output .= $special_range_out[$i];
			$output .= substr($special_range_start[$i],0,2) . ":". substr($special_range_start[$i],2) . " - " . substr($special_range_end[$i],0,2) . ":". substr($special_range_end[$i],2);
			$output .= "<br>";
		}
	} else {
		$output = '--';
	}

	return $output;
	}
	function week2ch($number) {
		$number = intval($number);
		$array  = array('日','一','二','三','四','五','六');
		$str = $array[$number];
		return $str;
	}
	function chDate($number){
		switch ($number){
			case "1":
				$str = "d/m/Y";
			break;
			case "2":
				$str = "Y年m月d日";
			break;
			case "3":
				$str = "d/m";
			break;
		}
		return $str;
	}
	function formatInfoUTF8($str,$length) {
	$split=1;
	$curLen = 0;
	$isShorten=false;
	$array = array();
	for ( $i=0; $i < strlen( $str ); ){
		$value = ord($str[$i]);
		if($value > 127){
			if($value >= 192 && $value <= 223)
				$split=2;
			elseif($value >= 224 && $value <= 239)
				$split=3;
			elseif($value >= 240 && $value <= 247)
				$split=4;
		}else{
			$split=1;
		}
		if($split==1){
			$curLen++;
		}else{
			$curLen+=2;
		}
		$key = NULL;
		for ( $j = 0; $j < $split; $j++, $i++ ) {
			if(ord($str[$i])!=13 && ord($str[$i])!=10){	//carriage return & line feed
				$key .= $str[$i];
			}
		}
		if($curLen<$length-3){
			array_push( $array, $key );
		}else{
			$isShorten=true;
		}
	}
	if($isShorten){
		return implode('',$array).'...';
	}else{
		return implode('',$array);
	}
}

function formatShopCateLink($cate) {
	$links = array(
		'__ROOT__/shop/search/category_id/' . $cate[ 'id' ],
		'__ROOT__/?m=shop&a=search&category_id=' . $cate[ 'id' ]
	);
	return $links[ array_rand( $links ) ];
}

?>