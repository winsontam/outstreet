<?php

class EventAction extends BaseAction
{

	public function search()
	{
		$category_ids = (array) $_REQUEST[ 'category_id' ];
		if ( $_REQUEST[ 'category_id' ] )
		{
			$category_ids = (array) $_REQUEST[ 'category_id' ];
		}
		else
		{
			$category_ids = (array) $_REQUEST[ 'category_ids' ];
		}
		$category_id = !empty( $category_ids ) ? $category_ids[ 0 ] : 0;
		$this->assign( 'category_id', $category_id );

		$generate_result = generate_categoryList( $category_id, 'event' );
		if ( !empty( $generate_result[ 1 ] ) )
		{
			$this->assign( 'request_cate_Info', $generate_result[ 1 ] );
		}
		if ( !empty( $generate_result[ 0 ] ) )
		{
			$this->assign( 'categoryList', $generate_result[ 0 ] );
		}

		/* core */
		$shop_id = (int) $_REQUEST[ 'shop_id' ];
		$query = (string) $_REQUEST[ 'query' ];
		$creater_name = $_REQUEST[ 'creater_name' ];
		$telephone = $_REQUEST[ 'telephone' ];
		$main_location_id = (int) $_REQUEST[ 'main_location_id' ];
		$sub_location_ids = (array) $_REQUEST[ 'sub_location_ids' ];
		if ( $_REQUEST[ 'location_id' ] )
		{
			$this->assign( 'location_id', $location_id );
			$location = explode( '_', $_REQUEST[ 'location_id' ] );
			if ( $location[ 0 ] == 'main' )
			{
				$main_location_id = $location[ 1 ];
			}
			else
			{
				$sub_location_ids[] = $location[ 1 ];
			}
		}
		$this->assign( 'main_location_id', $main_location_id );
		$this->assign( 'sub_location_ids', $sub_location_ids );
		$googlemap_lat = (float) $_COOKIE[ 'googlemap_lat' ];
		$googlemap_lng = (float) $_COOKIE[ 'googlemap_lng' ];
		$date_range = $_REQUEST[ 'date_range' ];
		$timediff = $_REQUEST[ 'timediff' ];
		$parent_event_id = (int) $_REQUEST[ 'parent_event_id' ];
		$page = (int) $_REQUEST[ 'page' ];


		$main_location_list = getmainlocationList();
		$this->assign( 'main_location_list', $main_location_list );
		for ( $i = 0; $i < 4; $i++ )
		{
			$sub_location_list[ $i ] = getsublocationList( $i + 1 );
		}
		$category_ids = getsubcateids( $category_ids, 'event' );
		$where = array();
		$where[ 'a.is_audit' ] = 1;
		$countQuerys = array();
		if ( $query )
		{
			$terms = $query;
			$allterms = array();
			if ( preg_match_all( '/\w+/', $terms, $m ) )
			{
				foreach ( $m[ 0 ] AS $t )
				{
					$terms = str_replace( $t, '', $terms );
					$allterms[ $t ] = 'e';
				}
			}
			$mterms = array();
			mb_regex_encoding( 'UTF-8' );
			mb_ereg_search_init( $terms, '\w+' );
			while ( ( $m = mb_ereg_search_regs() ) )
			{
				$allterms[ $m[ 0 ] ] = 'm';
			}

			$searchcols = array(
				'CONCAT(`name`,":",`english_name`)'	 => 10,
				'`creater_name`'					 => 8
			);
			$likecolumn = '[likecols]';
			$orQuerys = array();

			foreach ( $allterms AS $term => $type )
			{
				if ( $type == 'e' )
				{
					//$orQuerys[] = $likecolumn . ' REGEXP "[[:<:]]' . $term . '.{0,3}[[:>:]]"';
					$orQuerys[] = $likecolumn . ' REGEXP "' . $term . '"';
				}
				elseif ( $type == 'm' )
				{
					if ( ( $len = preg_match_all( '/./u', $term, $t ) ) > 1 )
					{
						$orQuerys[] = $likecolumn . ' REGEXP "(' . implode( '|', $t[ 0 ] ) . '){' . round( $len * 0.8 ) . ',' . ($len) . '}"';
					}
					else
					{
						$orQuerys[] = $likecolumn . ' LIKE "%' . $term . '%"';
					}
				}
				foreach ( $searchcols AS $col => $score )
				{
					$countQuerys[] = '( ( ' . $col . ' REGEXP "' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "[[:<:]]' . $term . '[[:>:]]" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "^' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "' . $term . '$" ) * ' . $score . ')'
						. ' + ( IF( length(' . $col . ') = length("' . $term . '"), 1, 0 ) * ' . $score . ')';
				}
			}

			$where[ '_string' ] = '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' OR ', $orQuerys ) ) . ')';
			$field = '*,(' . implode( ' + ', $countQuerys ) . ') AS scores';
			$order = 'scores DESC, end_time DESC';
		}
		else
		{
			$field = '*, IF(end_time+86400<NOW(),0,end_time) AS scores';
			$order = 'scores ASC, end_time DESC';
		}
		$countQuerys[] = '( IF( length(telephone), 0, 0 ) )' . ' + ( IF( d.id>0, 0, 0 ) )';
		$countField = '((' . implode( ' + ', $countQuerys ) . ') + ( IF(c.review_count is null,0,c.review_count) )) AS scores';

		if ( $main_location_id )
		{
			$where[ 'a.sub_location_id' ] = array( 'exp', ' IN (SELECT id FROM location_sub WHERE main_location_id = "' . $main_location_id . '" ) ' );
		}
		if ( $sub_location_ids )
		{
			$where[ 'a.sub_location_id' ] = array( 'in', $sub_location_ids );
		}
		if ( $category_ids )
		{
			$where[ 'a.cate_id' ] = array( 'in', $category_ids );
		}
		if ( $parent_event_id )
		{
			$where[ 'parent_event_id' ] = $parent_event_id;
		};
		if ( !empty( $creater_name ) )
			$where[ 'a.creater_name' ] = array( 'like', '%' . $creater_name . '%' );
		if ( !empty( $telephone ) )
			$where[ 'a.telephone' ] = array( 'like', '%' . $telephone . '%' );

		/* user GPS distance search
		  if (!$main_location_id && !count($sub_location_ids) && $googlemap_lat && $googlemap_lng)
		  {
		  if ($where[ '_string' ]) { $where[ '_string' ] .= ' AND '; }
		  else { $where[ '_string' ] = ''; }
		  $where[ '_string' ] .= '( 6371 * acos( cos( radians('.$googlemap_lat.') ) * cos( radians( googlemap_lat ) ) * cos( radians( googlemap_lng ) - radians('.$googlemap_lng.') ) + sin( radians('.$googlemap_lat.') ) * sin( radians( googlemap_lat ) ) ) ) <= 1';
		  }
		  user GPS distance search */

		if ( $shop_id )
		{
			$where[ 'a.shop_id' ] = $shop_id;
		}

		/* timediff */
		if ( $timediff )
		{
			$times = $this->getdatebyword( $timediff );
			if ( $times[ 0 ] )
			{
				$start_date = $times[ 0 ];
				$where[ 'end_time' ] = array( 'egt', $start_date );
			}
			if ( $times[ 1 ] )
			{
				$end_date = $times[ 1 ];
				$where[ 'start_time' ] = array( 'elt', $end_date );
			}
		}
		elseif ( $start_date || $end_date )
		{
			if ( is_int( $start_date ) )
			{
				$where[ 'end_time' ] = array( 'egt', $start_date );
			}
			if ( is_int( $end_date ) )
			{
				$where[ 'start_time' ] = array( 'elt', $end_date );
			}
		}
		/* timediff */

		/* input page */
		$result_per_page = C( 'RESULT_PER_PAGE' );
		$count = D( 'EventFields' );
		$count = $count->table( 'event_fields a' )
				->join( 'statistics_event c ON a.id=c.event_id' )
				->join( 'event_picture d ON a.id = d.event_id' )
				->where( $where )->count( 'distinct a.id' );
		$totalpage = ceil( $count / $result_per_page );
		if ( $page == '' )
		{
			$page = 1;
		}
		/* input page */
		/* join address into concat */
		/* join address into concat */
		$category_name = D( 'EventCate' )->where( 'id=' . $category_id )->getField( 'name' );
		$results = D( 'EventFields' )->field( 'a.creater_name, a.cate_id,a.id as event_id,if(a.name!=a.english_name,CONCAT(a.name," ",a.english_name),a.name) as name,main_location_id,sub_location_id,address,shop_id,rooms, a.googlemap_lat, a.googlemap_lng, if(d.file_path<>"",CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'EVENT_UPFILE_PATH' ) . 'thumb_",d.file_path),"") AS picture_url,a.end_time,a.start_time,a.details,' . $countField );
		$results = $results->table( 'event_fields a' )
				->join( 'statistics_event c ON a.id=c.event_id' )
				->join( 'event_picture d ON a.id = d.event_id' )
				->where( $where )->group( 'event_id' )->limit( $result_per_page )->page( $page )->order( $order )->select();
		/* Handle results shop_cates display */
		/* end core */
		foreach ( $results as $key => $val )
		{
			$results[ $key ][ 'main_location_name' ] = D( 'LocationMain' )->where( 'id=' . $results[ $key ][ 'main_location_id' ] )->getField( 'name' );
			$results[ $key ][ 'sub_location_name' ] = D( 'LocationSub' )->where( 'id=' . $results[ $key ][ 'sub_location_id' ] )->getField( 'name' );
			if ( $results[ $key ][ 'shop_id' ] != '0' )
			{
				$results[ $key ][ 'address' ] = D( 'ShopFields' )->where( 'id=' . $results[ $key ][ 'shop_id' ] )->getField( 'name' ) . " " . $results[ $key ][ 'rooms' ];
			}
			$results[ $key ][ 'address' ] = $results[ $key ][ 'main_location_name' ] . " " . $results[ $key ][ 'sub_location_name' ] . " " . $results[ $key ][ 'address' ];
			$results[ $key ][ 'category_names' ] = D( 'EventCate' )->where( 'id=' . $val[ 'cate_id' ] )->getField( 'name' );
			$results[ $key ][ 'date_range' ] = date( chDate( 1 ), $val[ 'start_time' ] ) . "(" . week2ch( date( "w", $val[ 'start_time' ] ) ) . ") - " . date( chDate( 1 ), $val[ 'end_time' ] ) . "(" . week2ch( date( "w", $val[ 'end_time' ] ) ) . ")";
			if ( time() > $val[ 'end_time' ] )
			{
				$results[ $key ][ 'name' ].="(已完結)";
			}
			if ( $results[ $key ][ 'details' ] == '' )
			{
				$results[ $key ][ 'details' ] = '暫無';
			}
			else
			{
				$results[ $key ][ 'details' ] = formatInfoUTF8( $results[ $key ][ 'details' ], 30 );
			}
		}
		$this->assign( 'page_url', preg_replace( '/\/page\/\d+/is', '', $_SERVER[ 'REQUEST_URI' ] ) );
		$this->assign( 'results', $results );
		$this->assign( 'result_per_page', $result_per_page );
		$this->assign( 'totalpage', $totalpage );
		$this->assign( 'creater_name', $creater_name );
		$this->assign( 'telephone', $telephone );
		$this->assign( 'timediff', $timediff );
		$this->assign( 'page', $page );
		$this->assign( 'sub_location_list', $sub_location_list );
		$this->assign( 'eventList', $results );
		$this->assign( 'result_count_current_page', count( $result ) );
		$this->assign( 'result_count', $count );
		$this->display();
	}

	public function searchform()
	{
		$category_id = (int) $_REQUEST[ 'category_id' ];
		if ( $category_id )
		{
			$request_cateinfo = D( 'EventCate' )->where( 'id=' . $category_id )->find();
			$level = $request_cateinfo[ 'level' ];
			$parent_id = $request_cateinfo[ 'parent_id' ];
			$cateList[ $level + 1 ] = array_pop( generate_categoryList( $category_id, 'event' ) );
			if ( empty( $cateList[ $level + 1 ] ) )
			{
				unset( $cateList[ $level + 1 ] );
			}
			$selected_cate[ $level ] = $category_id;
			for ( $i = $level; $i >= 1; $i-- )
			{
				$selected_cate[ $i - 1 ] = $parent_id;
				$cateList[ $i ] = array_pop( generate_categoryList( $parent_id, 'event' ) );
				$parent_id = D( 'EventCate' )->where( 'id=' . $parent_id )->getField( 'parent_id' );
			}
			$nextLevel = $level + 1;
			$this->assign( $category_id );
		}
		else
		{
			$cateList[ 1 ] = array_pop( generate_categoryList( 0, 'event' ) );
		}
		$this->assign( 'telephone', $_REQUEST[ 'telephone' ] );
		$this->assign( 'timediff', $_REQUEST[ 'timediff' ] );
		$this->assign( 'creater_name', $_REQUEST[ 'creater_name' ] );
		$this->assign( 'query', $_REQUEST[ 'query' ] );
		$this->assign( 'selected_cate', $selected_cate );
		$this->assign( 'cateList', $cateList );
		$this->assign( 'category_id', $category_id );

		if ( $_REQUEST[ 'location_id' ] )
		{
			$this->assign( 'location_id', $location_id );
			$location = explode( '_', $_REQUEST[ 'location_id' ] );
			if ( $location[ 0 ] == 'main' )
			{
				$main_location_id = $location[ 1 ];
			}
			else
			{
				$sub_location_ids[] = $location[ 1 ];
			}
		}
		$main_location_list = getmainlocationList();
		$this->assign( 'main_location_list', $main_location_list );
		for ( $i = 0; $i < 4; $i++ )
		{
			$sub_location_list[ $i ] = getsublocationList( $i + 1 );
		}
		$this->assign( 'sub_location_list', $sub_location_list );
		$this->assign( 'main_location_id', $main_location_id );
		$this->assign( 'sub_location_ids', $sub_location_ids );
		$this->display();
	}

	public function view()
	{

		$event_id = $_REQUEST[ 'event_id' ];
		$map = $_REQUEST[ 'map' ];
		$eventInfo = D( 'EventFields' )->where( 'id=' . $event_id )->find();
		if ( $eventInfo[ 'english_name' ] != '' && $eventInfo[ 'english_name' ] != $eventInfo[ 'name' ] )
		{
			$eventInfo[ 'name' ].=' ' . $eventInfo[ 'english_name' ];
		}
		if ( $eventInfo[ 'end_time' ] < time() )
		{
			$eventInfo[ 'name' ].="(已完結)";
		}
		$eventInfo[ 'event_id' ] = $event_id;
		$eventInfo[ 'main_location_name' ] = D( 'LocationMain' )->where( 'id=' . $eventInfo[ 'main_location_id' ] )->getField( 'name' );
		$eventInfo[ 'sub_location_name' ] = D( 'LocationSub' )->where( 'id=' . $eventInfo[ 'sub_location_id' ] )->getField( 'name' );
		if ( $eventInfo[ 'shop_id' ] != '0' )
		{
			$eventInfo[ 'address' ] = D( 'ShopFields' )->where( 'id=' . $eventInfo[ 'shop_id' ] )->getField( 'name' ) . " " . $eventInfo[ 'rooms' ];
		}
		$eventInfo[ 'address' ] = $eventInfo[ 'main_location_name' ] . " " . $eventInfo[ 'sub_location_name' ] . " " . $eventInfo[ 'address' ];

		/* ticket */
		$eventInfo[ 'ticket_price' ] = $eventInfo[ 'price' ] ? $eventInfo[ 'price' ] : '';
		$eventInfo[ 'ticket_date' ] = $eventInfo[ 'ticket_startdate' ] ? date( chDate( 1 ), $eventInfo[ 'ticket_startdate' ] ) . "(" . week2ch( date( "w", $eventInfo[ 'ticket_startdate' ] ) ) . ")" : '';
		$eventInfo[ 'ticket_website' ] = $eventInfo[ 'ticket_website' ] ? $eventInfo[ 'ticket_website' ] : '';
		$eventInfo[ 'ticket_phone' ] = $eventInfo[ 'ticket_phone' ] ? formatTelephone( $eventInfo[ 'ticket_phone' ], 1, $startSalt, $endSalt, true ) : '';
		/* ticket */
		$eventInfo[ 'telephone' ] = $eventInfo[ 'telephone' ] ? formatTelephone( $eventInfo[ 'telephone' ], 1, $startSalt, $endSalt, true ) : '';

		$eventInfo[ 'description' ] = $eventInfo[ 'details' ];
		$eventInfo[ 'date_range' ] = date( chDate( 1 ), $eventInfo[ 'start_time' ] ) . "(" . week2ch( date( "w", $eventInfo[ 'start_time' ] ) ) . ") - " . date( chDate( 1 ), $eventInfo[ 'end_time' ] ) . "(" . week2ch( date( "w", $eventInfo[ 'end_time' ] ) ) . ")";
		$eventInfo[ 'address' ] = $eventInfo[ 'address' ] . " " . $eventInfo[ 'rooms' ];
		$eventInfo[ 'review_count' ] = D( 'StatisticsEvent' )->where( 'event_id=' . $event_id )->getField( 'review_count' );

		$eventInfo[ 'category_names' ] = D( 'EventCate' )->where( 'id=' . $eventInfo[ 'cate_id' ] )->getField( 'name' );
		$eventInfo[ 'category_id' ] = $eventInfo[ 'cate_id' ];
		$eventInfo[ 'is_sub_event' ] = $eventInfo[ 'parent_event_id' ] ? 1 : 0;
		$eventInfo[ 'is_main_event' ] = D( 'EventFields' )->where( 'parent_event_id=' . $event_id )->count() ? 1 : 0;

		if ( $eventInfo[ 'is_main_event' ] )
		{
			$eventInfo[ 'time_more' ] = '';
			$sub_event = D( 'EventFields' )->where( array( 'parent_event_id' => $event_id, 'is_audit' => 1 ) )->select();
			foreach ( $sub_event as $key => $val )
			{
				if ( $key != 0 )
				{
					$eventInfo[ 'time_more' ].="\n\r";
				}
				$eventInfo[ 'time_more' ].= $sub_event[ $key ][ 'name' ] . " " . date( chDate( 1 ), $sub_event[ $key ][ 'start_time' ] ) . "(" . week2ch( date( "w", $sub_event[ $key ][ 'start_time' ] ) ) . ") - " . date( chDate( 1 ), $sub_event[ $key ][ 'end_time' ] ) . "(" . week2ch( date( "w", $sub_event[ $key ][ 'end_time' ] ) ) . ")";
			}
		}
		$eventInfo[ 'picture_count' ] = D( 'EventPicture' )->where( 'event_id=' . $event_id )->count() + D( 'ReviewFields' )->table( 'review_fields a' )
				->join( 'review_pictures b on a.id=b.review_id' )
				->where( array( 'a.event_id' => $event_id ) )
				->count();
		$getPictures = D( 'EventPicture' )->field( 'file_path' )->where( 'event_id=' . $event_id )->select();
		foreach ( $getPictures as $key => $val )
		{
			$eventInfo[ 'pictures' ][ $key ] = C( 'OUTSTREET_DIR' ) . C( 'Event_UPFILE_PATH' ) . $val[ 'file_path' ];
		}
		$eventInfo[ 'reviews' ] = D( 'ReviewFields' )
			->field( 'a.id as review_id,a.title,a.description,c.nick_name as user_name, if(c.face_path,CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'USERFACE_UPFILE_PATH' ) . 'face",c.face_path),CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'USERDEFAULTFACE_UPFILE_PATH' ) . '",c.face_path)) AS user_picture_url,FROM_UNIXTIME(a.create_time,"%d/%m") as create_time,FROM_UNIXTIME(a.create_time,"%w") as week_time,d.review_count as user_review_count' )
			->table( 'review_fields a' )
			->join( 'user_fields c ON a.user_id=c.id' )
			->join( 'statistics_user d ON c.id=d.user_id' )
			->where( array( 'a.event_id' => $event_id, 'a.is_audit' => '1' ) )
			->select();
		foreach ( $eventInfo[ 'reviews' ] as $key => $val )
		{
			$eventInfo[ 'reviews' ][ $key ][ 'week_time' ] = week2ch( $eventInfo[ 'reviews' ][ $key ][ 'week_time' ] );
			$eventInfo[ 'reviews' ][ $key ][ 'description' ] = preg_replace( "/<br(.*?)\/+?>/", "\n", $eventInfo[ 'reviews' ][ $key ][ 'description' ] );
			$eventInfo[ 'reviews' ][ $key ][ 'description' ] = strip_tags( $eventInfo[ 'reviews' ][ $key ][ 'description' ] );
			$eventInfo[ 'reviews' ][ $key ][ 'create_time' ].="(" . $eventInfo[ 'reviews' ][ $key ][ 'week_time' ] . ")";
		}
		$this->assign( 'result_count', count( $eventInfo[ 'reviews' ] ) );
		$this->assign( 'eventInfo', $eventInfo );
		$this->display( $map ? 'map' : 'view' );
	}

	public function photo()
	{
		$page = (int) $_REQUEST[ 'page' ];
		$result_per_page = C( 'RESULT_PER_PAGE' );

		$event_id = $_REQUEST[ 'event_id' ];

		$eventInfo = $this->getbasicinfo( $event_id );
		$sql = "(select file_path,'event' as type from event_picture where event_id=" . $event_id . ") UNION (select b.file_path,'review' as type from review_fields a,review_pictures b where a.event_id = " . $event_id . " and b.review_id=a.id)";
		$count = count( D( 'EventFields' )->query( $sql ) );
		$totalpage = ceil( $count / $result_per_page );
		if ( $page == '' )
		{
			$page = 1;
		}
		if ( $page > $totalpage )
		{
			$page = $totalpage;
		}
		$limit = (($result_per_page * $page) - $result_per_page) . "," . $result_per_page;
		$sql = "(select file_path,'event' as type from event_picture where event_id=" . $event_id . ") UNION (select b.file_path,'review' as type from review_fields a,review_pictures b where a.event_id = " . $event_id . " and b.review_id=a.id) limit " . $limit . "";
		$getPictures = D( 'EventFields' )->page( $page )->limit( $result_per_page )->query( $sql );
		foreach ( $getPictures as $key => $val )
		{
			if ( $val[ 'type' ] == 'event' )
			{
				$eventInfo[ 'pictures' ][ $key ][ 'thumb' ] = C( 'OUTSTREET_DIR' ) . C( 'EVENT_UPFILE_PATH' ) . 'thumb_' . $val[ 'file_path' ];
				$eventInfo[ 'pictures' ][ $key ][ 'ori' ] = C( 'OUTSTREET_DIR' ) . C( 'EVENT_UPFILE_PATH' ) . $val[ 'file_path' ];
			}
			else
			{
				$eventInfo[ 'pictures' ][ $key ][ 'thumb' ] = C( 'OUTSTREET_DIR' ) . C( 'EVENT_REVIEW_UPFILE_PATH' ) . 'thumb_' . $val[ 'file_path' ];
				$eventInfo[ 'pictures' ][ $key ][ 'ori' ] = C( 'OUTSTREET_DIR' ) . C( 'EVENT_REVIEW_UPFILE_PATH' ) . $val[ 'file_path' ];
			}
		}
		$this->assign( 'page_url', preg_replace( '/\/page\/\d+/is', '', $_SERVER[ 'REQUEST_URI' ] ) );
		$this->assign( 'result_per_page', $result_per_page );
		$this->assign( 'page', $page );
		$this->assign( 'totalpage', $totalpage );
		$this->assign( 'result_count', $count );
		$this->assign( 'eventInfo', $eventInfo );
		$this->display();
	}

	public function subevent()
	{
		$page = (int) $_REQUEST[ 'page' ];
		$event_id = $_REQUEST[ 'event_id' ];
		$eventInfo = $this->getbasicinfo( $event_id );

		$result_per_page = C( 'RESULT_PER_PAGE' );
		$count = D( 'EventFields' )
			->where( array( 'parent_event_id' => $event_id, 'is_audit' => '1' ) )
			->count( 'distinct id' );
		$totalpage = ceil( $count / $result_per_page );
		if ( $page == '' )
		{
			$page = 1;
		}
		if ( $page > $totalpage )
		{
			$page = $totalpage;
		}
		/* input page */
		$eventList = D( 'EventFields' )
			->where( array( 'parent_event_id' => $event_id, 'is_audit' => '1' ) )
			->field( 'id as event_id' )
			->limit( $result_per_page )->page( $page )
			->select();
		foreach ( $eventList as $key => $val )
		{
			$eventList[ $key ] = $this->getbasicinfo( $val[ 'event_id' ] );
		}
		$this->assign( 'page_url', preg_replace( '/\/page\/\d+/is', '', $_SERVER[ 'REQUEST_URI' ] ) );
		$this->assign( 'result_per_page', $result_per_page );
		$this->assign( 'page', $page );
		$this->assign( 'totalpage', $totalpage );
		$this->assign( 'result_count', $count );
		$this->assign( 'eventList', $eventList );
		$this->assign( 'eventInfo', $eventInfo );
		$this->display();
	}

	private function getbasicinfo( $id )
	{
		$dbo = D( 'EventFields' );
		$result = $dbo->where( 'id=' . $id )->find();
		$result[ 'event_id' ] = $result[ 'id' ];
		if ( $result[ 'shop_id' ] )
		{
			$result[ 'main_location_id' ] = D( 'ShopFields' )->where( 'id=' . $result[ 'shop_id' ] )->getField( 'main_location_id' );
			$result[ 'sub_location_id' ] = D( 'ShopFields' )->where( 'id=' . $result[ 'shop_id' ] )->getField( 'sub_location_id' );
			$result[ 'shop_name' ] = D( 'ShopFields' )->where( 'id=' . $result[ 'shop_id' ] )->getField( 'name' );
		}
		$result[ 'main_location_name' ] = D( 'LocationMain' )->where( 'id=' . $result[ 'main_location_id' ] )->getField( 'name' );
		$result[ 'sub_location_name' ] = D( 'LocationSub' )->where( 'id=' . $result[ 'sub_location_id' ] )->getField( 'name' );
		$result[ 'ori_address' ] = $result[ 'address' ];
		$result[ 'address' ] = '';
		if ( $result[ 'main_location_name' ] )
		{
			$result[ 'address' ] = $result[ 'main_location_name' ] . " ";
		}
		if ( $result[ 'sub_location_name' ] )
		{
			$result[ 'address' ] = $result[ 'address' ] . $result[ 'sub_location_name' ] . " ";
		}
		if ( $result[ 'shop_id' ] )
		{
			$result[ 'address' ] = $result[ 'address' ] . $result[ 'shop_name' ] . " ";
		}
		if ( $result[ 'ori_address' ] )
		{
			$result[ 'address' ] = $result[ 'address' ] . $result[ 'ori_address' ] . " ";
		}
		if ( $result[ 'rooms' ] )
		{
			$result[ 'address' ] = $result[ 'address' ] . $result[ 'rooms' ];
		}
		if ( $result[ 'telephone' ] == '0' )
		{
			$result[ 'telephone' ] = '--';
		}
		$result[ 'cate_name' ] = D( 'EventCate' )->where( 'id=' . $result[ 'cate_id' ] )->getField( 'name' );
		return $result;
	}

	private function getdatebyword( $timediff )
	{
		$firsttime = 0;
		$lasttime = 0;
		switch ( $timediff )
		{
			case 'today':
				$firsttime = strtotime( date( 'Y-m-d 0:0:0' ) );
				$lasttime = strtotime( date( 'Y-m-d 23:59:59' ) );
				break;
			case 'tomorrow':
				$firsttime = strtotime( date( 'Y-m-d 0:0:0' ) ) + 60 * 60 * 24;
				$lasttime = strtotime( date( 'Y-m-d 23:59:59' ) ) + 60 * 60 * 24;
				break;
			case 'week':
				$firsttime = strtotime( date( 'Y-m-d 0:0:0' ) ) - date( 'w' ) * 24 * 60 * 60;
				$lasttime = strtotime( date( 'Y-m-d 23:59:59' ) ) + (6 - date( 'w' )) * 24 * 60 * 60;
				break;
			case 'weekend':
				if ( date( 'N' ) < 5 )
				{
					$weekendstart = 5 - date( 'N' );
					$firsttime = mktime( 0, 0, 0, date( 'n' ), date( 'j' ) + $weekendstart, date( 'Y' ) );
				}
				else
				{
					$firsttime = strtotime( date( 'Y-m-d 0:0:0' ) );
				}
				$weekendend = 7 - date( 'N' );
				$lasttime = mktime( 23, 59, 59, date( 'n' ), date( 'j' ) + $weekendend, date( 'Y' ) );
				break;
			case 'month':
				$firsttime = strtotime( date( 'Y-m-d 0:0:0' ) );
				$lasttime = strtotime( date( 'Y-m-30 23:59:59' ) );
				break;
		}
		return array( $firsttime, $lasttime );
	}

}
