<?php
require_once COMMON_PATH . 'Mobile_Detect.php';
require_once COMMON_PATH . 'JavaScriptPacker.php';

class TrackingAction extends Action
{

    public $rate = 0.15;

    public $site = 'desktop';

    public $mobile = false;

    public function __construct()
    {
        parent::__construct();

        D( 'Tracking' )->where( 'created < "' . date( 'Y-m-d H:i:s', time() - 86400 * 7 ) . '"' )->delete();

        switch ( $_SERVER[ 'SERVER_NAME' ] )
        {
            case 'm.outstreet.com.hk':
            case 'mdev.outstreet.com.hk':
                $this->rate = 0.15;
                $this->site = 'mobile';
                break;
        }

        $detect = new Mobile_Detect();

        if ( $detect->isMobile() || $detect->isTablet() )
        {
            $this->mobile = true;
        }

        $this->assign( 'mobile', $this->mobile );
    }

    public function code()
    {
        $code = md5( uniqid( time(), true ) );

        $step = '0';

        if ( !preg_match( '/bot/i', $_SERVER[ 'HTTP_USER_AGENT' ] ) )
        {
            if ( $this->site == 'mobile' )
            {
                if ( $this->mobile )
                {
                    $step = 1;
                }
            }
            else
            {
                if ( !$this->mobile )
                {
                    if ( !in_array( rtrim( $_SERVER[ 'HTTP_REFERER' ], '/' ), array( 'http://outstreet.com.hk', 'http://www.outstreet.com.hk', 'http://outstreet.com.hk/index.php', 'http://www.outstreet.com.hk/index.php' ) ) )
                    {
                        $step = 1;
                    }
                }
            }
        }

        D( 'Tracking' )->add( array(
            'code'      => $code,
            'step'      => $step,
            'ip'        => $_SERVER[ 'REMOTE_ADDR' ],
            'pure'      => D( 'Tracking' )->where( 'step IN ( "6", "16" ) AND ip = "' . $_SERVER[ 'REMOTE_ADDR' ] . '" AND created >= "' . date( 'Y-m-d H:i:s', time() - 86400 ) . '"' )->count() ? '0' : '1',
            'device'    => $this->site,
            'useragent' => $_SERVER[ 'HTTP_USER_AGENT' ],
            'referer'   => $_SERVER[ 'HTTP_REFERER' ],
            'created'   => date( 'Y-m-d H:i:s' )
        ) );

        $json = json_encode( array( 'code' => $code, 'mobile' => $this->mobile, 'ip' => $_SERVER[ 'REMOTE_ADDR' ] ) );

        if ( $_GET[ 'token' ] )
        {
            echo $_GET[ 'token' ] . '(' . $json . ');';
        }
        else
        {
            echo $json;
        }
    }

    public function land()
    {
        if ( $_POST[ 'code' ] )
        {
            $tracking = D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ], 'step' => '1' ) )->find();

            if ( $tracking )
            {
                D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ] ) )->save( array( 'step' => '2' ) );

                $this->assign( 'content', $this->content() );
            }
        }

        $this->display( 'index' );
    }

    public function lead()
    {
        if ( $_POST[ 'code' ] )
        {
            $tracking = D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ], 'step' => '2' ) )->find();

            if ( $tracking )
            {
                D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ] ) )->save( array( 'step' => '3' ) );

                $total      = D( 'Tracking' )->where( 'step IN ( "5", "6", "15", "16" ) AND device = "' . $this->site . '" AND DATE( created ) = "' . date( 'Y-m-d', time() ) . '"' )->count();
                $transacted = D( 'Tracking' )->where( 'step IN ( "6", "16" ) AND device = "' . $this->site . '" AND DATE( created ) = "' . date( 'Y-m-d' ) . '"' )->count();
                $rate       = ( $total && $transacted ) ? $transacted / $total : 0;

                $rand = mt_rand( 1, 100 );

                $transact = ( $tracking[ 'pure' ] && $rate <= $this->rate && $rand ) ? '1' : '0';
                D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ] ) )->save( array( 'transact' => $transact, 'rate' => $rate ) );

                $link = $this->getLink();

                $this->assign( 'link', $link );
                $this->assign( 'content', $this->content() );
            }
        }

        $this->display( 'index' );
    }

    public function page()
    {
        if ( $_POST[ 'code' ] )
        {
            $tracking = D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ], 'step' => '3' ) )->find();

            if ( $tracking )
            {
                D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ] ) )->save( array( 'step' => '4' ) );

                $title = $this->getTitle();

                $this->assign( 'title', $title );
                $this->assign( 'content', $this->content() );
            }
        }

        $this->display( 'index' );
    }

    public function convert()
    {
        echo '<div>';

        if ( $_POST[ 'code' ] )
        {
            $tracking = D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ], 'step' => '4' ) )->find();

            if ( $tracking )
            {
                D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ] ) )->save( array( 'step' => '5' ) );

                echo $this->content();
            }
        }

        echo '</div>';
    }

    public function verify()
    {
        echo '<div>';

        if ( $_POST[ 'code' ] )
        {
            $tracking = D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ], 'step' => '5' ) )->find();

            if ( $tracking )
            {
                if ( $tracking[ 'transact' ] )
                {
                    if ( !D( 'Tracking' )->where( 'step IN ( "6", "16" ) AND ip = "' . $_SERVER[ 'REMOTE_ADDR' ] . '" AND created >= "' . date( 'Y-m-d H:i:s', time() - 86400 ) . '"' )->count() )
                    {
                        echo $this->content();
                    }
                }
            }
        }

        echo '</div>';
    }

    public function transact()
    {
        if ( $_GET[ 'code' ] )
        {
            $tracking = D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ], 'step' => '5' ) )->find();

            if ( $tracking )
            {
                if ( $tracking[ 'transact' ] )
                {
                    D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ] ) )->save( array( 'step' => '6' ) );
                }
            }
        }

        echo 'void(0);';
    }

    public function view()
    {
        if ( $_GET[ 'code' ] )
        {
            $tracking = D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ], 'step' => '1' ) )->find();

            if ( $tracking )
            {
                D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ] ) )->save( array( 'step' => '15' ) );
            }
        }

        echo 'void(0);';
    }

    public function click()
    {
        if ( $_GET[ 'code' ] )
        {
            $tracking = D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ], 'step' => '15' ) )->find();

            if ( $tracking )
            {
                D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ] ) )->save( array( 'step' => '16' ) );
            }
        }

        echo 'void(0);';
    }

    public function js()
    {
        if ( $_SERVER[ 'REMOTE_ADDR' ] == '218.255.188.102' )
        {
            echo 'console.log(' . json_encode( $_SERVER ) . ');';
        }
        else
        {
            echo 'void(0);';
        }
    }

    public function redirect()
    {
        $this->assign( 'content', $this->content() );
        $this->display( 'index' );
    }

    public function content()
    {
        $content = $this->fetch();

        return preg_replace_callback( '#<script>(.+?)</script>#is', function ( $matches ) {
            $script = $this->script( $matches[ 1 ] );

            for ( $total = mt_rand( 5, 10 ), $i = 1; $i <= $total; $i++ )
            {
                $script = $this->script( $script );
            }

            return '<script>' . $script . '</script>';
        }, $content );
    }

    public function script( $script )
    {
        $packer = new JavaScriptPacker( $script, 'Normal', false, false );

        return 'e(d("' . base64_encode( $packer->pack() ) . '"));';
    }

    public function getLink()
    {
        if ( rand( 0, 1 ) )
        {
            $cate = D( 'ShopCate' )->where( array( 'type' => 'all' ) )->order( 'RAND()' )->find();
            return '__ROOT__/shop/search/category_id/' . $cate[ 'id' ];
        }
        else
        {
            $shop = D( 'ShopFields' )->where( array( 'is_audit' => 1 ) )->order( 'RAND()' )->find();
            return '__ROOT__/shop/view/shop_id/' . $shop[ 'id' ];
        }
    }

    public function getTitle()
    {
        if ( !empty( $_REQUEST[ 'category_id' ] ) )
        {
            $cate = D( 'ShopCate' )->where( array( 'id' => $_REQUEST[ 'category_id' ] ) )->find();

            if ( $cate )
            {
                return $cate[ 'name' ] . ' ' . $cate[ 'english_name' ];
            }
        }

        if ( !empty( $_REQUEST[ 'shop_id' ] ) )
        {
            $shop = D( 'ShopFields' )->where( array( 'id' => $_REQUEST[ 'shop_id' ] ) )->find();

            if ( $shop )
            {
                $name = $shop[ 'name' ];

                if ( $shop[ 'english_name' ] )
                {
                    $name .= $shop[ 'english_name' ];
                }

                return $name;
            }
        }

        return 'OutStreet 出街 - Mobile';
    }
}
