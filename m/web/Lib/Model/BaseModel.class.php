<?php

import("Think.Core.Model.RelationModel");

class BaseModel extends RelationModel{	
	public function rows( $key = null , $item = null , $isMultiItem = false ) {
		$array = $this->select();
		$rows = array();
		if ( $key && $item ) {
			if ( $isMultiItem ) { foreach ( $array AS $row ) { $rows[ $row[ $key ] ][] = $row[ $item ]; } } 
			else { foreach ( $array AS $row ) { $rows[ $row[ $key ] ] = $row[ $item ]; } } 
		}
		elseif ( $key ) {
			if ( $isMultiItem ) { foreach ( $array AS $row ) { $rows[ $row[ $key ] ][] = $row; } } 
			else { foreach ( $array AS $row ) { $rows[ $row[ $key ] ] = $row; } } 
		}
		elseif ( $item ) {
			foreach ( $array AS $row ) { $rows[] = $row[ $item ]; }
		}
		else {
			foreach ( $array AS $row ) { $rows[] = $row; }
		}
		return $rows;
	}
}
?>