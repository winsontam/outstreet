<?php /*
<!DOCTYPE html>
<html lang="zh">
    <head>
        <title>OutStreet 出街 - 全港最新最多元化的出街指南</title>
    </head>
	<script type="text/javascript">

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-43363581-1', 'outstreet.com.hk');
		ga('send', 'pageview');

		// Google Analytics
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-15793058-1']);
		_gaq.push(['_trackPageview']);
		_gaq.push(['t2._setAccount', 'UA-19879263-4']);
		_gaq.push(['t2._trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
    <body style="text-align: center; background-color: #5491D2;">
		<img src="Public/maintain.jpg" style="border: 0px;"/>
	</body>
</html>
 */ ?>
<?php

  define( "WEB_ROOT", '/dev_site' );
  define( 'THINK_PATH', './ThinkPHP' );
  define( 'APP_NAME', 'web' );
  define( 'APP_PATH', 'web/' );
  require(THINK_PATH . '/ThinkPHP.php');
  $App = new App();
  $App->run();

?>