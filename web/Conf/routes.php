<?php
return array(
	'guide@'=>array(
		array('/^$/','shop','index',''),
		array('/^.+p\/(\d+)$/','shop','index','p')
	),
	'shopregister@'=>array(
		array('/^$/','user','owner_login','')
	),
	'president_massage@'=>array(
		array('/^$/','shop','view',null,'id=40940')
	),
	'purplestar@'=>array(
		array('/^$/','shop','view',null,'id=43185')
	),
    'ownerguide@'=>array(
        array('/^$/','user','owner_login','')
    )
);
?>
