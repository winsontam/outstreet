﻿<?php

if (!defined('THINK_PATH')) exit();

return array(
			 
	'OUTSTREET_ROOT'=>'../www',
	'OUTSTREET_DIR'=>'http://www.outstreet.com.hk',
	'FB_SHARE_URL'=>'http://www.outstreet.com.hk',

	'DB_TYPE'=>'mysql', //数据库类型
	'DB_HOST'=>'mysql5.s224.sureserver.com', //服务器地址
	'DB_NAME'=>'outstreet_db', //数据库名
	'DB_USER'=>'dbuser001', //用户名
	'DB_PWD'=>'a9c84a04', //密码
	'DB_PORT'=>3307, //端口
	'DB_PREFIX'=>'', //数据库表前缀
	'URL_MODE'=>2,	//URLREWRITE模式
	'TEMPLATE_SUFFIX'=>'.html',	//模板文件後綴
	'URL_HTML_SUFFIX'=>'.html',	//偽靜態後綴
	'URL_CASE_INSENSITIVE'=>true,	//URL是否區分大小寫
	'PAGE_ROLLPAGE'=>5,
	'PAGE_LISTROWS'=>10,	//每頁數量
	'APP_DEBUG'=>false,	//DEBUG模式是否開啟
	'TMPL_ACTION_SUCCESS'=>'Public:message', //正確信息提示頁面
	'TMPL_ACTION_ERROR'=>'Public:message',	//錯誤信息提示頁面
	'TMPL_CACHE_TIME' => 1,
	'INDEX_SHOP_CATES'=>'1,16,6,17,10,3,2,5,15,9,14,8,4,12,13,7,11',
	//Subcate order
	'SUB_CATE_ORDER_1'=>'18,23,24,30,21,28,29,27,20,25,22,26,19,31,32,353',
	'SUB_CATE_ORDER_16'=>'146,135,136,140,141,137,138,139,145,143,144,352,142',
	'SUB_CATE_ORDER_6'=>'68,67,66,60,58,64,59,61,62,63,65',
	'SUB_CATE_ORDER_17'=>'151,150,147,157,148,153,154,156,155,152,149',
	'SUB_CATE_ORDER_10'=>'95,102,96,99,98,97,100,101',
	'SUB_CATE_ORDER_3'=>'41,42,40,39,44,43',
	'SUB_CATE_ORDER_2'=>'38,37,33,36,35,34',
	'SUB_CATE_ORDER_5'=>'54,53,51,57,55,52,56,365',
	'SUB_CATE_ORDER_15'=>'134,133,130,131,132,129,128',
	'SUB_CATE_ORDER_9'=>'92,93,91,90,89,94,88',
	'SUB_CATE_ORDER_14'=>'125,126,124,127',
	'SUB_CATE_ORDER_8'=>'54,53,51,57,55,52,56,365',
	//End subcate order
	'INDEX_EVENT_CATES'=>'1,2,3,4,5,6,7,8,9,10,11,12',
	'EMAIL_ENCRYPT_KEY'=>'91#25D1cC2G/*$9**/$$vkl:{rfgtv8}4vqBT62^gg7r',

	'TOKEN_ON'=>false,
	'URL_ROUTER_ON'=>true, //URL路由開啟

//@.ORG.Simple用到的配置 STAR
	'USER_AUTH_ON'=>true,	//是否開啟身份認證
	'USER_AUTH_MODEL'=>'User',
	'USER_AUTH_GATEWAY'=>'/User/login/',	//身份認證的登錄頁面
	'REQUIRE_AUTH_ACTION'=>'add,edit,mypanel,changePsd,changeEmail,addStepOne,getOwner,submitMessage,joinShop2Favorite',	//需要認證的操作  啟用該項 其他操作均無需認證
//'NOT_AUTH_ACTION'=>'index,view,login,register',	//無需認證的操作   和需要認證的操作 啟用一項即可
//@.ORG.Simple用到的配置 END

//common.php的smtp_mail函數的配置 START
	'SMTP_SERVER'=>'mbox.outstreet.com.hk',
	'SMTP_USERNAME'=>'admin@outstreet.com.hk',
	'SMTP_PASSWORD'=>'outstreet',
	'SMTP_FROM'=>'admin@outstreet.com.hk',
	'SMTP_FROMNAME'=>'OutStreet',
//common.php的smtp_mail函數的配置 END

//ShopAction.class.php中用到的參數	START
	'SHOP_PHOTO_COUNT'=>8,		//創建商鋪時所能添加的圖片數量
	'EVENT_PHOTO_COUNT'=>6,		//創建節目時所能添加的圖片數量
	'REVIEW_PHOTO_COUNT'=>6,		//評論時所能添加的圖片數量
//ShopAction.class.php中用到的參數	END

//ShopAction.class.php的uploadPic中用到的參數 START	
	'REVIEW_UPFILE_PATH'=> '/Public/uploadimages/review/',
	'SHOP_UPFILE_PATH'=> '/Public/uploadimages/shop/',
	'EVENT_UPFILE_PATH'=> '/Public/uploadimages/event/',
	'EVENT_REVIEW_UPFILE_PATH'=> '/Public/uploadimages/event_review/',
	'REQUEST_SHOP_UPFILE_PATH'=> '/Public/uploadimages/request/shop/',
	'REQUEST_EVENT_UPFILE_PATH'=> '/Public/uploadimages/request/event/',	
	'PROMO_UPFILE_PATH'=> '/Public/uploadimages/promo/',
	'USERFACE_UPFILE_PATH'=> '/Public/uploadimages/userface/',
	'OTPROMO_UPFILE_PATH'=> '/Public/uploadimages/outstreetpromo/',
	'POSTER_HTML_UPFILE_PATH'=> '/Public/uploadimages/outstreetpromo/poster_html/',
	'PROMOTION_UPFILE_PATH'=> '/Public/uploadimages/promotion/',
	'PROMOTION_REVIEW_UPFILE_PATH'=> '/Public/uploadimages/promotion_review/',
	'SHOP_PDF_PATH'=> '/Public/upload/pdf/',
	'SHOPNEWS_UPFILE_PATH'=> '/Public/uploadimages/shop_news/',
//關於POSTER的變數 END

//ForumAction.class.php的upfile用到的參數
	'FORUM_UPFILE_PATH'=> '/Public/uploadimages/forum/',
//end

//SA模塊用到的配置 START
	'DB_BACKUP_PATH'=> 'backup/',		//備份文件建立的路徑
//SA模塊用到的配置 END

	'UPFILE_ALLOW_IMGEXT'=>'jpg,jpeg,gif,png',
	'UPFILE_ALLOW_PDFEXT'=>'pdf',
	'COOKIE_EXPIRE'=> 3600 * 24,	//COOKIE有效期

//EventAction.class.php的getShopByKeyword中用到的參數eventAdvShop
	'EVENTADVSHOP'=>array('76','77','78','79','80','81','82','83','84'),

//Security Log
	'TIME_VISIT_TOLERATE'=>2,	//in second
	'COUNT_VISIT_TOLERATE'=>3,	//in time
	'TIME_RANGE_TOLERATE'=>60,	//in second (For Shop Request)
	'COUNT_RANGE_TOLERATE'=>8,	//in time

	'PATH_SALT'=>'k08DJ08idw0z'

);

?>