<?php

function rtnPromotionImg($promotionInfo, $className, $width='', $height='') {
	if (!empty($promotionInfo['pictures'])) {
		foreach($promotionInfo['pictures'] as $pic){
			if($pic['is_main']==1){
				$string .= '<img src="'.C('OUTSTREET_DIR').C('PROMOTION_UPFILE_PATH').'/'.'thumb_'.$pic['file_path'];
			}
		}
		if($string==''){$string .= '<img src="'.C('OUTSTREET_DIR').C('PROMOTION_UPFILE_PATH').'/'.'thumb_'.$promotionInfo['pictures'][0]['file_path'];}
	} else {
        $string .= '<img src="__PUBLIC__/images/promotion/';
		if ($promotionInfo['type1']==1) {
			$string .= 'promotion_def_bigsale.jpg';
		} elseif($promotionInfo['type1']==2) {
			$string .= 'promotion_def_coupon.jpg';
		}
	}

	if($width && $height) {
		$string .= '" width="'.$width.'" height="'.$height.'"';
	}

	$string .= '" class="'.$className.'">';
	return $string;
}

function rtnPromotionType($type) {
	switch($type) {
		case 1:
			$string = '大減價';
		break;
		case 2:
			$string = '商戶優惠';
		break;
		case 3:
			$string = '信用卡優惠';
		break;
	}
	return $string;
}

function checkVisitFrequecy(){
	$logDao = D('Log');
	$UtilitylogDao = M('utility_log_ip');
	$ip = getIp();
	$log_time = time();

	import ( '@.ORG.SimpleAcl' );
	$authInfo = SimpleAcl::getLoginInfo();
	$user_id = isset($authInfo['id'])?$authInfo['id']:0;
	$fromtime = $log_time-C('TIME_VISIT_TOLERATE');
	$fromtimerange = $log_time-C('TIME_RANGE_TOLERATE');

	$where = array();
	$whereRange = array();
	$whereUtility = array();

	$where['ip'] = $ip;
	$where['create_time'] = array('gt', $fromtime);

	$whereRange['ip'] = $ip;
	$whereRange['create_time'] = array('gt', $fromtimerange);
	$whereRange['page'] = array('like', '%shop_provide_info%');

	$whereUtility['log_time'] = array('gt', $fromtimerange);
	$whereUtility['ip'] = $ip;
	$whereUtility['status'] = 2;

	$exceptionDao = M('utility_exception');
	$count_except = $exceptionDao->where('"'.$ip.'" like concat(ip,"%")')->count();
	$count_log = $logDao->where($where)->count();
	$count_range_log = $logDao->where($whereRange)->count();
	$count_utility_log = $UtilitylogDao->where($whereUtility)->count();

	if($count_except==0){//Not is exception list
		if($count_log>C('COUNT_VISIT_TOLERATE') || ($count_range_log>C('COUNT_RANGE_TOLERATE')&& $count_utility_log==0)){
			$m = new Model();
			$m->execute('insert into log_fields(ip,page,create_time,redirect,user_id) values("'.$ip.'","'.$_SERVER['REQUEST_URI'].'",'.time().',1,'.$user_id.')');
			//redirect to valiate page
			addToBlockList($ip, $log_time);
			header('Location: '.__ROOT__.'/Utility/accessValidate');
			exit;
		}
	}
}

function isIpBlocked(){
	$ip = getIp();
	$logDao = M('utility_log_ip');
	$where = array();
	$where['ip'] = $ip;
	$where['status'] =1;
	$count = $logDao->where($where)->count();
	return $count>0?true:false;
}

function addToBlockList($ip, $log_time){
	$logDao = M('utility_log_ip');
	$data = array();
	$data['ip'] = $ip;
	$data['log_time'] = $log_time;
	$data['status'] = 1;
	$data['create_time'] = time();
	$logDao->add($data);
}

function dropFromBlockList(){
	$ip = getIp();
	$logDao = M('utility_log_ip');
	$where = array();
	$data = array();
	$where['ip'] = $ip;
	$data['status'] = 2;
	$logDao->where($where)->save($data);
}

function getIp(){
	if (!empty($_SERVER['HTTP_CLIENT_IP'])){
    	$ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
    	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
   		$ip=$_SERVER['REMOTE_ADDR'];
    }
	return $ip;
}

function getStartSalt(){
	return base64_encode(date('YmdH:i'));
}

function getEndSalt(){
	return base64_encode(date('H:iYmd'));
}

function getHash($id){
	return md5(C('PATH_SALT').$id);
}

function generateUploadPicModule($type,$picstamp,$picNum=0){
	$outout = '';
	$from = '';
	$itemStr='';
	$width = '430';
	$height = '200';
	$picNum = $picNum ? $picNum : getAuthPicNum($type);

	switch($type){
		case 0:	//shop
			$from='shop';
		break;
		case 1:	//shop request
			$from='request';
		break;
		case 2:	//shop review
			$from='review';
		break;
		case 3:	//event review
			$from='event_review';
		break;
		case 4:	//promotion review
			$from='promotion_review';
		break;
		case 5:	//Evemt
			$from='event';
		break;
		case 6:	//Owner Shop News
			$from='shopnews';
		break;
		default://shop
			$from='shop';
		break;
	}

	$output .= '<iframe id="deleteResult" name="deleteResult" style="display:none;"></iframe>';
	for ($item = 0;$item < $picNum;$item++ ){
		$output .= '<iframe name="iframeAt'.$item.'" id="iframeAt'.$item.'" src="__ROOT__/Uploadimage/uploadResult" style="display:none"></iframe>';
		$output .= '<div class="photoContainer">';
		$output .= '<div class="photo" id="photoAt'.$item.'"></div>';
		$output .= '<div class="action" id="peditAt'.$item.'"><a href="__ROOT__/Uploadimage/uploadPic/eleid/'.$item.'/picstamp/'.$picstamp.'/from/'.$from.'/'.$itemStr.'?height='.$height.'&width='.$width.'&modal=false" class="thickbox">上載</a></div>';
		$output .= '</div>';
	}
	return $output;
}

function ecode($startSalt,$endSalt,$str){
 	return $startSalt.base64_encode($str).$endSalt;
}

function fbBtn($item_id, $type, $option, $nick_name, $review_id=0) {
	$string = '<div class="ot_facebook_set">';
	switch($type) {
		case 'poster':
			$share_url = C('FB_SHARE_URL').'/'.$type.'/view/posterid/'.$item_id.'/uname/'.$nick_name.'/r/'.md5(time()).'/';
			$like_url = C('FB_SHARE_URL').'/'.$type.'/view/posterid/'.$item_id.'/like/1/';
		break;
		default:
			$share_url = C('FB_SHARE_URL').'/'.$type.'/view/id/'.$item_id.'/uname/'.$nick_name.'/r/'.md5(time()).'/';
			$like_url = C('FB_SHARE_URL').'/'.$type.'/view/id/'.$item_id.'/like/1/';
		break;
	}

	if($review_id) {
		$share_url .= 'reviewid/'.$review_id.'/';
		$like_url  .= 'reviewid/'.$review_id.'/';
	}

	$like_url = 'https://www.facebook.com/outstreet';
	switch($option) {
		case 1:
			$string .= '<div class="fbshare"><a href="//www.facebook.com/sharer.php?u='.urlencode($share_url).'" target="_blank">分享至facebook</a></div>';
		break;
		case 2:
			$string .= '<div><iframe src="//www.facebook.com/plugins/like.php?href='.urlencode($like_url). '&send=false&layout=button_count&width=300&show_faces=true&action=like&colorscheme=light&font&height=30&appId=207058589321787" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:350px; height:30px;" allowTransparency="true"></iframe></div>';
		break;
		case 3:
			$string .= '<div class="fbshare"><a href="//www.facebook.com/sharer.php?u='.urlencode($share_url).'" target="_blank">分享至facebook</a></div>';
			$string .= '<div><iframe src="//www.facebook.com/plugins/like.php?href='.urlencode($like_url). '&send=false&layout=button_count&width=300&show_faces=true&action=like&colorscheme=light&font&height=30&appId=207058589321787" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:350px; height:30px;" allowTransparency="true"></iframe></div>';
		break;
	}

	$string .= '</div>';
	return $string;
}

function formatShopName($chi_name, $eng_name) {
	if($chi_name) {
		$string .= $chi_name.' ';
	}

	if(($eng_name != $chi_name) && ($eng_name)) {
		$string .= $eng_name;
	}

	return $string;
}

function formatTelephone($telephone, $type, $startSalt = '',$endSalt = '',$status = false) {
	if($telephone == '' || $telephone  == 0) {
		if($type == 1) {
			$string = '暫無';
		}
	} else {
		if($type == 2) {
			$string = '&nbsp;&nbsp;|&nbsp;&nbsp;';
		}
		$string .= substr($telephone,0,4).' '.substr($telephone,4,4);
	}
	if(isset($string)&& $status){
		return '<script>print_ecode("'.ecode($startSalt,$endSalt, $string).'");</script>';
	}else{
		return $string;
	}
}

function formatInfo($word, $length){
	$word = strip_tags($word,'');

	if(mb_strlen($word,'UTF-8')>=$length){
		$word =	mb_substr($word,0,$length-4,'UTF-8').'...';
	}

	return $word;
}

function formatCategory($cateArray, $length, $doURL = true){
	$strlen = 0;
	$x = $i = 0;
	$keys = array();
	foreach( $cateArray as $val ){
		$strlen += mb_strlen( $val['name'], 'UTF-8' );
		$i++;
		if ( $strlen > $length && $i != 1 ) {
			$string .= '...';
			break;
		}

		$id = $val['reference_id']?$val['reference_id']:$val['id'];

		if (!in_Array($id,$keys)){
			if($doURL) {
				$string .= "<a href='".formatShopCateLink($val) ."'>".$val['name']."</a>&nbsp;";
			} else {
				$string .= ($x ? ',':'').$val['name']; $x++;
			}
		}
		$keys[] = $id;
	}

	return $string;
}

function formatShopCateLink($cate) {
	$id = formatShopCategoryId($cate);
	$name = $cate['name'];
	$english_name = $cate['english_name'];

	return '__ROOT__/shop/lists/id/'.$id.'/c/'. skipSpecialChar($name);
}

function formatEventCateLink($cate) {
	$id = formatEventCategoryId($cate);
	$name = $cate['name'];
	$english_name = $cate['english_name'];

	return '__ROOT__/event/lists/cateid/'.$id.'/c/'. skipSpecialChar($name);
}

function formatAddr($shopArray, $type, $length, $startSalt='',$endSalt='',$status=false){
	$string = '<a href="__URL__/lists/mainlocationid/'.$shopArray['main_location_id'].'">';
	if($status){
		$string .= '<script>print_ecode("'.ecode($startSalt,$endSalt,$shopArray['main_location']['name']).'");</script>';
	}else{
		$string .= $shopArray['main_location']['name'];
	}

	$string .= '</a>&nbsp;';
    $string .= '<a href="__URL__/lists/sublocationid/'.$shopArray['sub_location_id'].'">';

	if($status){
		$string .= '<script>print_ecode("'.ecode($startSalt,$endSalt,$shopArray['sub_location']['name']).'");</script>';
	}else{
		$string .= $shopArray['sub_location']['name'];
	}
	$string .= '</a>&nbsp;';

	if($type == 'shop') {
		$shopDao = D('Shop');
		if ($shopArray['parent_shop_id'] != 0) {
			$parentShopInfo = $shopDao->find($shopArray['parent_shop_id']);
			if($status){
				$string .= '<script>print_ecode("'.ecode($startSalt,$endSalt,$shopArray['address'].'&nbsp;').'");</script>';
			}else{
				$string .= $shopArray['address'].'&nbsp;';
			}
			$string .= '<a href="__URL__/view/id/'.$shopArray['parent_shop_id'].'">';
			$string .= '<script>print_ecode("'.ecode($startSalt,$endSalt,$parentShopInfo['name']).'");</script>';
			$string .= '</a>&nbsp;';
			if($status){
				$wholeAddress = '<script>print_ecode("'.ecode($startSalt,$endSalt,$shopArray['rooms']).'");</script>';
			}else{
				$wholeAddress = $shopArray['rooms'];
			}
		} else {
			if($status){
				$wholeAddress = '<script>print_ecode("'.ecode($startSalt,$endSalt,$shopArray['address'].' '.$shopArray['rooms']).'");</script>';
			}else{
				$wholeAddress = $shopArray['address'].' '.$shopArray['rooms'];
			}
		}
	}

	$string .= $wholeAddress;
	//$string .= formatInfo($wholeAddress,25); (length : not use yet)
	return $string;
}

function formatEventCategoryId($cate) {
	$id = $cate['reference_id']?$cate['reference_id']:$cate['id'];
	return $id;
}

function formatShopCategoryId($cate) {
	$id = $cate['reference_id']?$cate['reference_id']:$cate['id'];
	return $id;
}

function urlShortForm($url, $length){
	if(mb_strlen($url,'UTF-8')>=$length){
		$tempurl=mb_substr($url,0,$length-4,'UTF-8');
		$tempurl=$tempurl . " ... ";
		$url=$tempurl.mb_substr($url,-5,5,'UTF-8');
		return $url;
	}else{
		return $url;
	}
}

function chDate($number){
	switch ($number){
		case "1":
			$str = "d/m/Y";
		break;
		case "2":
			$str = "Y年m月d日";
		break;
		case "3":
			$str = "d/m";
		break;
	}
	return $str;
}

function getUserIdByName($nick_name){
	$userDao = D('User');
	$where = array();
	$where['nick_name'] = $nick_name;
	$userInfo = $userDao->where($where)->find();
	if(!empty($userInfo)){
		return $userInfo['id'];
	}
}

function getFace($userInfo){
	if (!empty($userInfo['face_path'])){
		return C('OUTSTREET_DIR').C('USERFACE_UPFILE_PATH').'face'.$userInfo['face_path'];
	}

	if ($userInfo['user_type'] == 1){
		return __PUBLIC__ . '/images/face/1.gif';
	}else{
		return __PUBLIC__ . '/images/face/0.gif';
	}
}

function getFirstPicture($modelInfo, $type, $thumb = null){
	if (!empty($modelInfo['pictures'])){
		if ($type == 'review'){
			if(isset($thumb)){
				return C('OUTSTREET_DIR').C('REVIEW_UPFILE_PATH').$modelInfo['pictures'][0]['shop_id'].'/'.'thumb_'.$modelInfo['pictures'][0]['file_path'];
			}else{
				return C('OUTSTREET_DIR').C('REVIEW_UPFILE_PATH').$modelInfo['pictures'][0]['shop_id'].'/'.$modelInfo['pictures'][0]['file_path'];
			}
		}else if($type == 'shop'){
			if(isset($thumb)){
				if(preg_match('/^brand/',$modelInfo['pictures'][0]['file_path'],$matches)){
					return C('OUTSTREET_DIR').C('SHOP_UPFILE_PATH').$modelInfo['pictures'][0]['file_path'];
				}else{
					return C('OUTSTREET_DIR').C('SHOP_UPFILE_PATH').'thumb_'.$modelInfo['pictures'][0]['file_path'];
				}
			}else{
				return C('OUTSTREET_DIR').C('SHOP_UPFILE_PATH').$modelInfo['pictures'][0]['file_path'];
			}
		}else if($type == 'event'){
			if(isset($thumb)){
				return C('OUTSTREET_DIR').C('EVENT_UPFILE_PATH').'thumb_'.$modelInfo['pictures'][0]['file_path'];
			}else{
				return C('OUTSTREET_DIR').C('EVENT_UPFILE_PATH').$modelInfo['pictures'][0]['file_path'];
			}
		}
	}else if($type=='shop' && empty($modelInfo['pictures'])){
		$modelInfo['pictures']=getShopDetail($modelInfo,4);
		if(!empty($modelInfo['pictures'])){
			return C('OUTSTREET_DIR').C('SHOP_UPFILE_PATH').$modelInfo['pictures'][0]['file_path'];
		}else{
			if(isset($thumb)){
				return __PUBLIC__ . '/images/thumb_nopic.jpg';
			}else{
				return __PUBLIC__ . '/images/nopic.jpg';
			}
		}
	}else{
		if(isset($thumb)){
			return __PUBLIC__ . '/images/thumb_nopic.jpg';
		}else{
			return __PUBLIC__ . '/images/nopic.jpg';
		}
	}
}

function msubstr($str, $start=0, $length, $charset="utf-8", $suffix=true) {
	if(function_exists("mb_substr"))
	return mb_substr($str, $start, $length, $charset);
	elseif(function_exists('iconv_substr')) {
		return iconv_substr($str,$start,$length,$charset);
	}
	$re['utf-8']   = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
	$re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
	$re['gbk']	  = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
	$re['big5']	  = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
	preg_match_all($re[$charset], $str, $match);
	$slice = join("",array_slice($match[0], $start, $length));
	if($suffix) return $slice."…";
	return $slice;
}

function obcopy($ob){
	$serialized_contents = serialize($ob);
	return unserialize($serialized_contents);
}

function week2ch($number) {
    $number = intval($number);
    $array  = array('日','一','二','三','四','五','六');
    $str = $array[$number];
    return $str;
}

function timediff($begin_time, $end_time){
	if($begin_time < $end_time){
		$starttime = $begin_time;
		$endtime = $end_time;
	}else{
		$starttime = $end_time;
		$endtime = $begin_time;
	}

	$timediff = $endtime - $starttime;
	$days = intval($timediff/86400);
	$remain = $timediff%86400;
	$hours = intval($remain/3600);
	$remain = $remain%3600;
	$mins = intval($remain/60);
	$secs = $remain%60;

	$res = array("day"=>$days,"hour"=>$hours,"min"=>$mins,"sec"=>$secs);
	return $res;
}

function generateUserStatistics($userId){
	$userDao = D('User');
	$statisticsUserDao = D('StatisticsUser');

	$userInfo = $userDao->find($userId);

	if(!empty($userInfo)){
		$statisticsUserDao->where('user_id='.$userId)->delete();

		$shopDao = D('Shop');
		$reviewDao = D('Review');
		$eventReviewDao = D('EventReview');
		$promoReviewDao = D('PromoReview');
		$eventDao = D('Event');
		$userShopFavoriteDao = D('UserShopFavorite');
		$userUserFavoriteDao = D('UserUserFavorite');
		$userPromotionFavoriteDao = D('UserPromotionFavorite');
		$userMessageDao = D('UserMessage');
		$shopPictureDao = D('ShopPicture');
		$reviewPictureDao = D('ReviewPicture');
		$reviewCommendDao = D('ReviewCommend');

		$where = array();
		$where['user_id'] = $userInfo['id'];

		$data = array();
		$data['user_id'] = $userInfo['id'];
		$data['nick_name'] = $userInfo['nick_name'];

		$where['is_audit'] = 1;
		$data['review_count'] = $reviewDao->where($where)->count();
		$data['review_count'] += $eventReviewDao->where($where)->count();
		$data['review_count'] += $promoReviewDao->where($where)->count();

		unset($where['is_audit']);

		$data['event_count'] = $eventDao->where($where)->count();
		$data['favorite_shop_count'] = $userShopFavoriteDao->where($where)->count();
		$data['message_count'] = $userMessageDao->where($where)->count();
		$data['submit_message_count'] = $userMessageDao->where('submit_user_id=' . $userInfo['id'])->count();
		$data['picture_count'] = $reviewPictureDao->where($where)->count();

		$data['favorite_user_count'] = $userUserFavoriteDao->where('user_id=' . $userInfo['id'])->count();
		$data['favorite_offer_count'] = $userPromotionFavoriteDao->where('user_id=' . $userInfo['id'])->count();
		$data['favorite_me_count'] = $userUserFavoriteDao->where('favorite_user_id=' . $userInfo['id'])->count();
		$data['commend_review_count'] = $reviewCommendDao->where("user_id='" . $userInfo['id'] . "'")->count();
		$success=$statisticsUserDao->add($data);
	}
}

function generateMainLocationStatistics($locationId){
	$locationMainDao = D('LocationMain');
	$shopDao = D('Shop');
	$eventDao = D('Event');

	$where = array();
	$where['main_location_id'] = $locationId;
	$where['is_audit'] = 1;

	$locationMainDao->where("id=".$locationId)->setField('shop_count',$shopDao->where($where)->count());
	$locationMainDao->where("id=".$locationId)->setField('event_count',$eventDao->where($where)->count());
}

function generateSubLocationStatistics($locationId){
	$locationSubDao = D('LocationSub');
	$shopDao = D('Shop');
	$eventDao = D('Event');

	$where = array();
	$where['sub_location_id'] = $locationId;
	$where['is_audit'] = 1;

	$locationSubDao->where("id=".$locationId)->setField('shop_count',$shopDao->where($where)->count());
	$locationSubDao->where("id=".$locationId)->setField('event_count',$eventDao->where($where)->count());
}

function generateShopCateStatByShopID($shopId, $action){
	$ShopCateDao		= D('ShopCate');
	$ShopCateMapDao		= D('ShopCateMap');
	$shopCateMapList	= $ShopCateMapDao->where("shop_id =$shopId")->select();
	$cateIdArray = array();

	foreach($shopCateMapList as $val) {
		$cateIdArray[] = $val['cate_id'];
		$tmpCateIdArray = generateParentCateIDListByCateID($val['cate_id']);

		foreach($tmpCateIdArray as $tmpVal) {
			$cateIdArray[] = $tmpVal;
		}
	}

	$cateIdArray = array_unique($cateIdArray);

	foreach($cateIdArray as $val) {
		switch($action) {
			case 'add':
				$ShopCateDao->query("update `shop_cate` set shop_count = shop_count + 1 where id = '".$val."'");
			break;
			case 'delete':
				$ShopCateDao->query("update `shop_cate` set shop_count = shop_count - 1 where id = '".$val."'");
			break;
		}
	}

	return true;
}

function generateParentCateIDListByCateID($cateId) {
	$shopCateDao = D('ShopCate');
	$cateIdArray = array();
	$shopCateList	= $shopCateDao->where("id =$cateId")->select();
	$level 			= $shopCateList[0]['level'];
	$parentCateId   = $shopCateList[0]['parent_id'];

	while($level != 1) {
		$cateIdArray[]  = $parentCateId;
		$shopCateList	= $shopCateDao->where("id =$parentCateId")->select();
		$level = $shopCateList[0]['level'];
		$parentCateId = $shopCateList[0]['parent_id'];
	}

	return $cateIdArray;
}

function generateAllShopCateStatistics(){
	$ShopCateDao = D('ShopCate');
	$Model = new Model();
	$category = array();
	$selfCateCount_arr = array();
	$uniSonCount_arr = array();
	$accuShopCount_arr = array();
	$shopCateParentIdMap = array();
	$shopCateTree = $ShopCateDao ->select();
	$shopCateParentIdMap = $ShopCateDao -> field('id, parent_id') ->select();
	$result = $Model -> query("select parent_id,shop_id from shop_cate_map left join shop_cate on shop_cate_map.cate_id = shop_cate.id left join shop_fields on shop_cate_map.shop_id = shop_fields.id
where is_Audit = 1");
	$result2 = $Model->query("select cate_id, count(*) as shop_count from shop_cate_map left join shop_fields on shop_cate_map.shop_id=shop_fields.id where is_Audit=1 group by cate_id");

	foreach($result2 as $elem){
		$cateId = $elem['cate_id'];
		$shop_count = $elem['shop_count'];
		$selfCateCount_arr["$cateId"] = $shop_count;
	}
	foreach($result as $elem){
		$parent_id = $elem['parent_id'];
		$shop_id = $elem['shop_id'];
		if(!$category["$parent_id"]){
			$category["$parent_id"] = array();
		}
		array_push($category["$parent_id"], $shop_id) ;
	}
	foreach($category as $key =>$elem){
		$elem = array_unique($elem);	//find unique shop
		$uniSonCount_arr["$key"] = count($elem);
	}
	calAllShopLeaf($shopCateParentIdMap,$shopCateTree,$accuShopCount_arr,$selfCateCount_arr,$uniSonCount_arr);
}

function calAllShopLeaf($shopCateParentIdMap,$shopCateTree,$accuShopCount_arr,$selfCateCount_arr,$uniSonCount_arr){
	//process all leaf
	if(count($shopCateTree)==0){
		$ShopCateDao = D("ShopCate");
		foreach($accuShopCount_arr as $key=>$elem){
			$ShopCateDao->find($key);
			$ShopCateDao->shop_count = $elem['shop_count'];
			$ShopCateDao->save();
		}
		return;
	}
	foreach($shopCateTree as $key=>$elem){
		$cate_id = $elem['id'];
		if(isShopLeaf($cate_id,$shopCateTree)){
			$sum = 0;
			//add self level's shop
			$sum += $selfCateCount_arr[$cate_id];
			//add all son's accUniqueShop
			$sum += getSonShopCount($cate_id, $uniSonCount_arr,$shopCateParentIdMap,$selfCateCount_arr);
			//put into accShopCount
			$accuShopCount_arr["$cate_id"] = array('shop_count'=>$sum);
			//unset leaf
			unset($shopCateTree["$key"]);
		}
	}
	calAllShopLeaf($shopCateParentIdMap,$shopCateTree,$accuShopCount_arr,$selfCateCount_arr,$uniSonCount_arr);
}

function getSonShopCount($cate_id, $uniSonCount_arr,$shopCateParentIdMap,$selfCateCount_arr){
	$sum = 0;
	$sum+=$uniSonCount_arr[$cate_id];
	foreach($shopCateParentIdMap as $elem){
		if($elem['parent_id']==$cate_id){
			$id = $elem['id'];
			$sum += getSonShopCount($id, $uniSonCount_arr,$shopCateParentIdMap,$selfCateCount_arr);
		}
	}
	return $sum;
}

function isShopLeaf($cateId, $shopCateTree){
	$leaf = true;
	foreach($shopCateTree as $elem){
		if($elem['parent_id']==$cateId)	{
			$leaf = false;
		}
	}
	return $leaf;
}

function generateShopCateStatistics($cateIds, $action){
	$updateList = array();
	//db
	$ShopCateDao = D('ShopCate');
	$shopCateList = $ShopCateDao->select();
	$model = new Model();

	//prepare list of cate for update
	$parentIds = array();
	$updateList = getParentList($cateIds,$updateList,$shopCateList,$parentIds);
	foreach($updateList as $elem){
		if($action=='add'){
			$ShopCateDao->query("update `shop_cate` set shop_count = shop_count + 1 where id = '".$elem."'");
		} else if($action == 'delete'){
			$ShopCateDao->query("update `shop_cate` set shop_count = shop_count - 1 where id = '".$elem."'");
		}
	}
}

function getParentList($cateIds,$updateList,$shopCateList,$parentIds){
	foreach($cateIds as $elem){
		array_push($updateList,$elem);
	}
	foreach($shopCateList as $elem){
		if(in_array($elem['id'],$cateIds) && $elem['parent_id']!=0){
			if(!in_array($elem['parent_id'],$parentIds)){
				array_push($parentIds,$elem['parent_id']);
				$updateList = getParentList((array)$elem['parent_id'],$updateList,$shopCateList,$parentIds);
			}
		}
	}
	return $updateList;
}

function generateShopWorkTime($work_time, $working_hour) {
	if($work_time != '' and $work_time != 0){
		$hour = array();
		foreach ($working_hour AS $val)
		{
			$hour[$val['recurring_week']] = $val;
		}
		$working_hour = $hour;

		ksort($working_hour);

		$whresult = array();
		$search_array = array();
		foreach($working_hour AS $key => $val)
		{
			if ( $key != 8 )
			{
				$whresult[$val['start_time'].'/'.$val['end_time']][] = $val['recurring_week'];
				$search_array[] = $val['recurring_week'];
			}
		}

		$j = 0;
		for($i = 1; $i < 8; $i++) {
			if (!in_array($i, $search_array)) {
				$dayoffday[$j] = $i;
				$j++;
			}
		}

		$no = 0;

		foreach ($whresult as $key => $value) {
			$rowA[$no] = $key;
			$prev = 0;
			$prevResult = 0;
			$prevAndResult = 0;
			foreach ($value as $key02 => $value02) {
				$now = $value02 - $prev;
				if($prevResult == 0) {
					if($value02 < 8) {
						$rowB[$no] .= '星期'.$value02;
					} else {
						$rowB[$no] .= $value02;
					}
				} else {
					if($now == 1) {
						if($prevAndResult == 0) {
							$rowB[$no] .= '至'.$value02;
						} else {
							$rowB[$no] = substr($rowB[$no],0,strlen($rowB[$no])-1).$value02;
						}
						$prevAndResult++;
					} else {
						$rowB[$no] .= '及'.$value02;
					}
				}

				$prevResult++;
				$prev = $value02;
			}
			$no++;
		}

		// Start Day Off //
		if(count($dayoffday) > 0) {
			$prevResult = 0;
			$prevAndResult = 0;
			$rowA[$no] = '休息';

			foreach ($dayoffday as $key => $value) {
				if($prevResult == 0) {
					$prev = $value;
				}

				$now = $value - $prev;

				if($prevResult == 0) {
					if($value < 8) {
						$rowB[$no] .= '星期'.$value;
					} else {
						$rowB[$no] .= $value;
					}
				} else {
					if($now == 1) {
						if($prevAndResult == 0) {
							$rowB[$no] .= '至'.$value;
						} else {
							$rowB[$no] = substr($rowB[$no],0,strlen($rowB[$no])-1).$value;
						}
						$prevAndResult++;
					} else {
						$rowB[$no] .= '及'.$value;
					}
				}

				$prevResult++;
				$prev = $value;
			}
		}
		// End Day Off //

		// Start special days and holidays
		foreach ($special_working_hour as $hours){
			$idx++;
			$special_range_start[$idx] = $hours[start_time];
			$special_range_end[$idx] = $hours[end_time];
			if ($hours[recurring_week] > 0)
				$special_range_out[$idx] .= $day[$hours[recurring_week]];
			else{
				$special_range_out[$idx] .= date("m月d日",strtotime($hours[start_day]));
				if ($hours[start_day] != $hours[end_day])
					$special_range_out[$idx] .= "至" . date("m月d日",strtotime($hours[end_day]));
			}
		}
		// End special days and holidays

		$output = '<table>';
		for ($i=0; $i<count($rowA); $i++){ //loop through regular week hours for output
			$day = array(1 => '一',2 => '二',3 => '三',4 => '四',5 => '五',6 => '六',7 => '日', 8=> '公眾假期');

			for($j = 1; $j < 9; $j++) {
				$rowB[$i] = str_replace($j,$day[$j],$rowB[$i]);
			}

			$output .= '<tr>';
			$output .= '<td style="vertical-align:baseline">'.$rowB[$i].'</td>';
			if($rowA[$i] == '休息') {
				$output .= '<td style="vertical-align:baseline">&nbsp;&nbsp;&nbsp;休息</td>';
			} else {
				$output .= '<td style="vertical-align:baseline">&nbsp;&nbsp;&nbsp;'.substr($rowA[$i],0,2) . ":". substr($rowA[$i],2,2) . " - " . substr($rowA[$i],5,2) . ":". substr($rowA[$i],7,2).'</td>';
			}
			$output .= '</tr>';
		}

		$output .= '<tr>';
		$output .= '<td style="vertical-align:baseline">公眾假期</td>';
		if ($working_hour[8]['start_time']&&$working_hour[8]['end_time'])
		{
			$output .= '<td style="vertical-align:baseline">&nbsp;&nbsp;&nbsp;'.substr($working_hour[8]['start_time'],0,2) . ":". substr($working_hour[8]['start_time'],2,2) . " - " . substr($working_hour[8]['end_time'],0,2) . ":". substr($working_hour[8]['end_time'],2,2).'</td>';
		} else {
			$output .= '<td style="vertical-align:baseline">休息</td>';
		}
		$output .= '</tr>';

		for ($i=1; $i<=$idx; $i++){ //loop through special hours for output
			$output .= '<tr>';
			$output .= '<td style="vertical-align:baseline">'.$special_range_out[$i].'</td>';
			$output .= '<td style="vertical-align:baseline">&nbsp;&nbsp;&nbsp;'.substr($special_range_start[$i],0,2) . ":". substr($special_range_start[$i],2) . " - " . substr($special_range_end[$i],0,2) . ":". substr($special_range_end[$i],2).'</td>';
			$output .= "</tr>";
		}
		$output .= "</table>";
	} else {
		$output = '暫無';
	}

	return $output;
}

/*!
 * ubb2html support for php
 * @requires xhEditor
 *
 * @author Yanis.Wang<yanis.wang@gmail.com>
 * @site http://xheditor.com/
 * @licence LGPL(http://www.opensource.org/licenses/lgpl-license.php)
 *
 * @Version: 0.9.6 build 100221
 */
function ubb2html($sUBB)
{
	$sHtml=$sUBB;

	global $emotPath,$cnum,$arrcode,$bUbb2htmlFunctionInit;$cnum=0;$arrcode=array();
	$emotPath='../xheditor_emot/';//表情根路径

	if(!$bUbb2htmlFunctionInit){
	function saveCodeArea($match)
	{
		global $cnum,$arrcode;
		$cnum++;$arrcode[$cnum]=$match[0];
		return "[\tubbcodeplace_".$cnum."\t]";
	}}
	$sHtml=preg_replace_callback('/\[code\s*(?:=\s*((?:(?!")[\s\S])+?)(?:"[\s\S]*?)?)?\]([\s\S]*?)\[\/code\]/i','saveCodeArea',$sHtml);

	$sHtml=preg_replace("/&/",'&',$sHtml);
	$sHtml=preg_replace("/</",'&lt;',$sHtml);
	$sHtml=preg_replace("/>/",'&gt;',$sHtml);
	$sHtml=preg_replace("/\r?\n/",'<br />',$sHtml);

	$sHtml=preg_replace("/\[(\/?)(b|u|i|s|sup|sub)\]/i",'<$1$2>',$sHtml);
	$sHtml=preg_replace('/\[color\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]/i','<span style="color:$1;">',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getSizeName($match)
	{
		$arrSize=array('8pt','10pt','12pt','14pt','18pt','24pt','36pt');
		return '<span style="font-size:'.$arrSize[$match[1]-1].';">';
	}}
	$sHtml=preg_replace_callback("/\[size\s*=\s*(\d+?)\s*\]/i",'getSizeName',$sHtml);
	$sHtml=preg_replace('/\[font\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]/i','<span style="font-family:$1;">',$sHtml);
	$sHtml=preg_replace('/\[back\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]/i','<span style="background-color:$1;">',$sHtml);
	$sHtml=preg_replace("/\[\/(color|size|font|back)\]/i",'</span>',$sHtml);

	for($i=0;$i<3;$i++)$sHtml=preg_replace('/\[align\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\](((?!\[align(?:\s+[^\]]+)?\])[\s\S])*?)\[\/align\]/','<p align="$1">$2</p>',$sHtml);
	$sHtml=preg_replace('/\[img\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/img\]/i','<img src="$1" />',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getImg($match)
	{
		$p1=$match[1];$p2=$match[2];$p3=$match[3];$src=$match[4];
		$a=$p3?$p3:(!is_numeric($p1)?$p1:'');
		return '<img src="'.$src.'"'.(is_numeric($p1)?' width="'.$p1.'"':'').(is_numeric($p2)?' height="'.$p2.'"':'').($a?' align="'.$a.'"':'').' />';
	}}
	$sHtml=preg_replace_callback('/\[img\s*=(?:\s*(\d*%?)\s*,\s*(\d*%?)\s*)?(?:,?\s*(\w+))?\s*\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/img\]/i','getImg',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getEmot($match)
	{
		global $emotPath;
		$arr=split(',',$match[1]);
		if(!$arr[1]){$arr[1]=$arr[0];$arr[0]='default';}
		$path=$emotPath.$arr[0].'/'.$arr[1].'.gif';
		return '<img src="'.$path.'" />';
	}}
	$sHtml=preg_replace_callback('/\[emot\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\/\]/i','getEmot',$sHtml);
	$sHtml=preg_replace('/\[url\]\s*(((?!")[\s\S])*?)(?:"[\s\S]*?)?\s*\[\/url\]/i','<a href="$1">$1</a>',$sHtml);
	$sHtml=preg_replace('/\[url\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]\s*([\s\S]*?)\s*\[\/url\]/i','<a href="$1">$2</a>',$sHtml);
	$sHtml=preg_replace('/\[email\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/email\]/i','<a href="mailto:$1">$1</a>',$sHtml);
	$sHtml=preg_replace('/\[email\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]\s*([\s\S]+?)\s*\[\/email\]/i','<a href="mailto:$1">$2</a>',$sHtml);
	$sHtml=preg_replace("/\[quote\]([\s\S]*?)\[\/quote\]/i",'<blockquote>$1</blockquote>',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getFlash($match)
	{
		$w=$match[1];$h=$match[2];$url=$match[3];
		if(!$w)$w=550;if(!$h)$h=400;
		return '<embed type="application/x-shockwave-flash" src="'.$url.'" wmode="opaque" quality="high" bgcolor="#ffffff" menu="false" play="true" loop="true" width="'.$w.'" height="'.$h.'" />';
	}}
	$sHtml=preg_replace_callback('/\[flash\s*(?:=\s*(\d+)\s*,\s*(\d+)\s*)?\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/flash\]/i','getFlash',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getMedia($match)
	{
		$w=$match[1];$h=$match[2];$play=$match[3];$url=$match[4];
		if(!$w)$w=550;if(!$h)$h=400;
		return '<embed type="application/x-mplayer2" src="'.$url.'" enablecontextmenu="false" autostart="'.($play=='1'?'true':'false').'" width="'.$w.'" height="'.$h.'" />';
	}}
	$sHtml=preg_replace_callback('/\[media\s*(?:=\s*(\d+)\s*,\s*(\d+)\s*(?:,\s*(\d+)\s*)?)?\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/media\]/i','getMedia',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getTable($match)
	{
		return '<table'.(isset($match[1])?' width="'.$match[1].'"':'').(isset($match[2])?' bgcolor="'.$match[2].'"':'').'>';
	}}
	$sHtml=preg_replace_callback('/\[table\s*(?:=(\d{1,4}%?)\s*(?:,\s*([^\]"]+)(?:"[^\]]*?)?)?)?\s*\]/i','getTable',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getTR($match){return '<tr'.(isset($match[1])?' bgcolor="'.$match[1].'"':'').'>';}}
	$sHtml=preg_replace_callback('/\[tr\s*(?:=(\s*[^\]"]+))?(?:"[^\]]*?)?\s*\]/i','getTR',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getTD($match){
		$col=isset($match[1])?$match[1]:0;$row=isset($match[2])?$match[2]:0;$w=isset($match[3])?$match[3]:null;
		return '<td'.($col>1?' colspan="'.$col.'"':'').($row>1?' rowspan="'.$row.'"':'').($w?' width="'.$w.'"':'').'>';
	}}
	$sHtml=preg_replace_callback("/\[td\s*(?:=\s*(\d{1,2})\s*,\s*(\d{1,2})\s*(?:,\s*(\d{1,4}%?))?)?\s*\]/i",'getTD',$sHtml);
	$sHtml=preg_replace("/\[\/(table|tr|td)\]/i",'</$1>',$sHtml);
	$sHtml=preg_replace("/\[\*\]([^\[]+)/i",'<li>$1</li>',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getUL($match)
	{
		$str='<ul';
		if(isset($match[1]))$str.=' type="'.$match[1].'"';
		return $str.'>';
	}}
	$sHtml=preg_replace_callback('/\[list\s*(?:=\s*([^\]"]+))?(?:"[^\]]*?)?\s*\]/i','getUL',$sHtml);
	$sHtml=preg_replace("/\[\/list\]/i",'</ul>',$sHtml);

	for($i=1;$i<=$cnum;$i++)$sHtml=str_replace("[\tubbcodeplace_".$i."\t]", $arrcode[$i],$sHtml);

	if(!$bUbb2htmlFunctionInit){
	function fixText($match)
	{
		$text=$match[2];
		$text=preg_replace("/\t/",'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$text);
		$text=preg_replace("/  /",' &nbsp;',$text);
		return $match[1].$text;
	}}
	$sHtml=preg_replace_callback('/(^|<\/?\w+(?:\s+[^>]*?)?>)([^<$]+)/i','fixText',$sHtml);

	$bUbb2htmlFunctionInit=true;

	return $sHtml;
}

function hasSymbol($str){
	if(preg_match('/[_]/',$str)){
		return true;
	}else{
		$str = trim($str);
		$strTemp = '';
		mb_regex_encoding( 'UTF-8' );
		mb_ereg_search_init( $str, '\w+' );
		while ( ( $match = mb_ereg_search_regs() ) ){
			$strTemp.=$match[0];
		}
		return (strlen($strTemp) == strlen($str))?false:true;
	}
}

function formatInfoUTF8($str,$length) {
	$split=1;
	$curLen = 0;
	$isShorten=false;
	$array = array();
	for ( $i=0; $i < strlen( $str ); ){
		$value = ord($str[$i]);
		if($value > 127){
			if($value >= 192 && $value <= 223)
				$split=2;
			elseif($value >= 224 && $value <= 239)
				$split=3;
			elseif($value >= 240 && $value <= 247)
				$split=4;
		}else{
			$split=1;
		}
		if($split==1){
			$curLen++;
		}else{
			$curLen+=2;
		}
		$key = NULL;
		for ( $j = 0; $j < $split; $j++, $i++ ) {
			if(ord($str[$i])!=13 && ord($str[$i])!=10){	//carriage return & line feed
				$key .= $str[$i];
			}
		}
		if($curLen<$length-3){
			array_push( $array, $key );
		}else{
			$isShorten=true;
		}
	}
	if($isShorten){
		return implode('',$array).'...';
	}else{
		return implode('',$array);
	}
}

function getTimediffWord($timediff) {
	switch($timediff){
		case 'today':
			$word = '今天';
		break;
		case 'tomorrow':
			$word = '明天';
		break;
		case 'week':
			$word = '本週';
		break;
		case 'weekend':
			$word = '本週末';
		break;
		case 'month':
			$word = '本月';
		break;
	}
	return $word;
}

function getShopLevelSetting( $name, $level = 0 )
{
	$settings = array(
		'description_len' => array(
			''=> 20,
			0 => 20,
			1 => 50,
			2 => 100
		),
		'upload_movie' => array(
			''=> false,
			0 => false,
			1 => false,
			2 => true
		),
		'pdf_num' => array(
			''=> 0,
			0 => 0,
			1 => 3,
			2 => 3
		),
		'add_website' => array(
			''=> false,
			0 => false,
			1 => true,
			2 => true
		),
		'photo_num' => array(
			''=> 1,
			0 => 1,
			1 => 3,
			2 => 5
		),
		'news_num' => array(
			''=> 1,
			0 => 1,
			1 => 3,
			2 => 5
		),
		'keyword_num' => array(
			''=> 3,
			0 => 3,
			1 => 5,
			2 => 10
		),
		'notify_review' => array(
			''=> false,
			0 => false,
			1 => true,
			2 => true
		),
	);

	return $settings[$name][$level];
}

function getOwnShopsWhere($user_id){
	$id = $user_id;
	$brand = D('Brand');
	$userOwnMap = D('ShopOwnerMap');
	$shop = D('Shop');

	$where = array();
	$where['user_id'] = $id;
	$where['brand_id'] = 0;
	$maplist = $userOwnMap->where($where)->select();
	$shopIds = array();
	foreach($maplist as $map){
		$shopIds[] = $map['shop_id'];
	}

	$where = array();
	$where['owner_id'] = $id;
	$maplist = $brand->where($where)->select();
	$brandIds = array();
	foreach($maplist as $map){
		$brandIds[] = $map['id'];
	}

	unset($where['user_id']);
	unset($where['shop_id']);
	unset($where['brand_id']);
	unset($where['owner_id']);

	if($shopIds){
		$where['id'] = array('in',$shopIds);
	}elseif($brandIds){
		$where['brand_id'] = array('in',$brandIds);
	}else{
		$where['1']	 = 0; //no shop
	}
	return $where;
}

function getShopDetail($input,$type){
	switch($type){
		case 1:
			$getValue='description';
		break;
		case 2:
			$getValue='website';
		break;
		case 3:
			$getValue='email';
		break;
		case 4:
			$getValue='pictures';
		break;
	}

	if(is_array($input)){
		$shopInfo=$input;
	}else{
		$shop_id=$input;
		$shopDao=D('Shop');
		$shopInfo=$shopDao->relation(true)->find($shop_id);
	}

	if($shopInfo[$getValue] != ''){
		return $shopInfo[$getValue];
	}else{
		if($shopInfo['brand_id'] > 0){
			$brandInfo=D('Brand')->find($shopInfo['brand_id']);
			if($getValue=='pictures' && !empty($brandInfo['file_path'])){
				$brandInfo['pictures'][0]['file_path']=$brandInfo['file_path'];
			}
			return $brandInfo[$getValue];
		}else{
			return;
		}
	}
}

function getsubcateids($category_ids) {
	$shopCateDao=D('ShopCate');
	$search_cate_ids = $category_ids;
	$loopids = array();
	$loopids = $category_ids;
	$loop = true;

	while($loop) {
		$shopCateList = $shopCateDao->where(array('type'=>array('neq','iphone'),'parent_id'=>array('in',$loopids)))->select();
		$cate = array();
		foreach( $shopCateList AS $val ) {
			$cate[] = $val['id'];
			$search_cate_ids[] = $val['reference_id']?$val['reference_id']:$val['id'];
		}
		if ( count($cate) ) { $loopids = $cate; }
		else { $loop = false; }
	}
	return $search_cate_ids;
}

function skipSpecialChar($value) {
	return urlencode(preg_replace('/[\!\@\#\$\%\^\&\*\(\)\_\+\-\=\[\]\\\;\'\,\.\/\`\{\}\|\:\"\<\>\?\~\s]+/is','-',$value));
}

?>