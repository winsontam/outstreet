<?php
import("Think.Core.Model.RelationModel");
class StatisticsEventModel extends RelationModel{
	protected $tableName = 'statistics_event';

	protected $_link = array(
		'event'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'event_fields',
			'foreign_key'=>'event_id',
			'mapping_name'=>'event',
	),
	);

}


