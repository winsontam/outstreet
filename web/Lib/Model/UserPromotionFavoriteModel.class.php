<?php
import("Think.Core.Model.RelationModel");
class UserPromotionFavoriteModel extends RelationModel{
	protected $tableName = 'user_offer_favorite';

	protected $_auto = array(
	array('create_time','time',self::MODEL_INSERT,'function'),
	);
	
	protected $_link = array(		
		'offer'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'offer_fields',
			'foreign_key'=>'offer_id',
			'mapping_name'=>'offer',
		),
	);

	protected $fields = array(
		'id',  
		'create_time',    
		'user_id',
		'user_name',
		'offer_id',
		'offer_name',
		'_pk'=>'id',
		'_autoinc'=>true
	);

}
