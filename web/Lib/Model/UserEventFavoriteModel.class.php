<?php
import("Think.Core.Model.RelationModel");
class UserEventFavoriteModel extends RelationModel{
	protected $tableName = 'user_event_favorite';

	protected $_auto = array(
	array('create_time','time',self::MODEL_INSERT,'function'),
	);
	
	protected $_link = array(
		'event'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'event_fields',
			'foreign_key'=>'event_id',
			'mapping_name'=>'event',
		),
	);

	protected $fields = array(
		'id',  
		'create_time',    
		'user_id',
		'user_name',
		'event_id',
		'event_name',
		'_pk'=>'id',
		'_autoinc'=>true
	);

}
