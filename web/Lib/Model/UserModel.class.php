<?php
import("Think.Core.Model.RelationModel");
class UserModel extends RelationModel{
	protected $tableName = 'user_fields';
	


	protected $_validate  =  array(
		array('email','require','請填寫您的電郵','add'),
		array('email','email','電郵格式错误！',2),
		array('email','','該電郵已經被注冊',0,'unique','add'),
		array('nick_name','','昵稱已經存在',0,'unique','add'),
		array('password','require','密碼不能為空','add'),
	);
	
	protected $_link = array(
		'statistics'=>array(
			'mapping_type'=>HAS_ONE,
			'class_name'=>'statistics_user',
			'foreign_key'=>'user_id',
			'mapping_name'=>'statistics',
			'mapping_order'=>'create_time desc'
		),
		'myFavoriteUsers'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'user_user_favorite',
			'foreign_key'=>'user_id',
			'mapping_name'=>'myFavoriteUsers',
			'mapping_order'=>'create_time desc',
			'mapping_limit'=>'6'
		),
		'favoriteMeUsers'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'user_user_favorite',
			'foreign_key'=>'favorite_user_id',
			'mapping_name'=>'favoriteMeUsers',
			'mapping_order'=>'create_time desc',
			'mapping_limit'=>'6'
		)
	);

	protected $_auto = array(
		array('create_time','time',self::MODEL_INSERT,'function'),
		array('password','md5',self::MODEL_INSERT,'function'),
	);

	protected $fields = array(
		'id',  
		'create_time',    
		'update_time',    
		'nick_name',    
		'user_type',    
		'first_name',    
		'last_name',    
		'password',    
		'email',    
		'sex',    
		'birthday',    
		'income',    
		'education',    
		'school',    
		'telephone',    
		'address',    
		'desc',
		'likes',
		'true_name',
		'business',
		'professional',
		'face_path',
		'last_login',
		'fb_userid',
		'is_email_verify',
		'o_company_name',
		'o_position',
		'admin_shop_tmp_id',
		'_pk'=>'id',
		'_autoinc'=>true
	);
}


