<?php
import("Think.Core.Model.RelationModel");
class UserUserFavoriteModel extends RelationModel{
	protected $tableName = 'user_user_favorite';

	protected $_auto = array(
		array('create_time','time',self::MODEL_INSERT,'function'),
	);

	protected $_link = array(
		'user'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'user_fields',
			'foreign_key'=>'user_id',
			'mapping_name'=>'user',
		),
		'favorite_user'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'user_fields',
			'foreign_key'=>'favorite_user_id',
			'mapping_name'=>'favorite_user',
		),
	);


	protected $fields = array(
		'id',  
		'create_time',    
		'user_id',
		'user_name',
		'favorite_user_id',
		'favorite_user_name',
		'_pk'=>'id',
		'_autoinc'=>true
	);

}
