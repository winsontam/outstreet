<?php
import("Think.Core.Model.RelationModel");

class ShopModel extends RelationModel{
	protected $tableName = 'shop_fields';

	protected $_link = array(
		'pictures'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'shop_picture',
			'foreign_key'=>'shop_id',
			'mapping_name'=>'pictures',
			'mapping_order'=>'create_time desc'
		),
		'sub_cates'=>array(
			'mapping_type'=>MANY_TO_MANY,
			'class_name'=>'shop_cate',
			'mapping_name'=>'sub_cates',
			'foreign_key'=>'shop_id',
			'relation_foreign_key'=>'cate_id',
			'relation_table'=>'shop_cate_map',
		),
		'pdf'=>array(
			'mapping_type'=>MANY_TO_MANY,
			'class_name'=>'shop_pdf',
			'mapping_name'=>'pdf',
			'foreign_key'=>'shop_id',
			'relation_foreign_key'=>'pdf_id',
			'relation_table'=>'shop_pdf_map',
		),
		'news'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'shop_news',
			'foreign_key'=>'shop_id',
			'mapping_name'=>'news',
		),
		'statistics'=>array(
			'mapping_type'=>HAS_ONE,
			'class_name'=>'statistics_shop',
			'mapping_name'=>'statistics',
			'foreign_key'=>'shop_id',
		),
		'sub_location'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'location_sub',
			'mapping_name'=>'sub_location',
			'foreign_key'=>'sub_location_id',
		),
		'main_location'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'location_main',
			'mapping_name'=>'main_location',
			'foreign_key'=>'main_location_id',
		),
	);

	protected $_validate  =  array(
		array('name','require','商戶名稱是必須的','add'),
	  	array('address','require','地址必須填寫','add')
	);

	protected $_auto = array(
		array('create_time','time',self::MODEL_INSERT,'function'),
	);

	protected $fields = array(
		'id',
		'parent_shop_id',
		'create_time',
		'update_time',
		'brand_id',
		'name',
		'eng_name',
		'work_time',
		'main_location_id',
		'sub_location_id',
		'address',
		'rooms',
		'googlemap_lat',
		'googlemap_lng',
		'eng_address',
		'eng_rooms',
		'price_range',
		'parking',
		'description',
		'eng_parking',
		'eng_description',
		'telephone',
		'sub_telephone',
		'website',
		'email',
		'keyword',
		'eng_keyword',
		'picstamp',
		'is_audit',
		'is_closed',
		'is_renovation',
		'_pk'=>'id',
		'_autoinc'=>true
	);
}

