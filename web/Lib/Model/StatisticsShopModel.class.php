<?php
import("Think.Core.Model.RelationModel");
class StatisticsShopModel extends RelationModel{
	protected $tableName = 'statistics_shop';

	protected $_link = array(
		'shop'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'shop_fields',
			'foreign_key'=>'shop_id',
			'mapping_name'=>'shop',
		),
	);

}


