<?php
class ForumOptionModel extends Model{
	protected $tableName = 'forum_options';


	protected $fields = array(
		'id',
		'thread_id',
		'name',
		'vote_count'
		);
}
