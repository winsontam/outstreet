<?php
require_once COMMON_PATH . 'Mobile_Detect.php';

class BaseAction extends Action
{

    public $cache_filename = '';

    public $fb_info        = array();

    public function __construct()
    {
        parent::__construct();

        $this->checkMobile();
        $this->checkCache();

        //include common files
        include COMMON_PATH . 'auth.php';
        if ( strtoupper( MODULE_NAME ) == 'SHOP' )
        {
            include COMMON_PATH . 'shopform.php';
        }

        //Redirect to valiate page for suspected user
        //if ( isIpBlocked() == true && strtoupper( MODULE_NAME ) != 'UTILITY' )
        //{
        //header('Location: '.__ROOT__.'/Utility/accessValidate');
        //}

        $hotKeywords = M( 'keyword' )->order( 'id asc' )->select();
        $this->assign( 'hotKeywords', $hotKeywords );

        $this->assign( 'startSalt', getStartSalt() );
        $this->assign( 'endSalt', getEndSalt() );
    }

    public function checkMobile()
    {
        if ( !$_COOKIE[ 'is_skip_check_mobile' ] )
        {
            $detect = new Mobile_Detect();

            if ( $detect->isMobile() || $detect->isTablet() )
            {
                $url = 'http://m.outstreet.com.hk';

                if ( strtolower( MODULE_NAME ) == 'shop' )
                {
                    switch ( strtolower( ACTION_NAME ) )
                    {
                        case "lists":
                            $url .= '/shop/search';
                            if ( $_REQUEST[ 'mainlocationid' ] )
                            {
                                $url .= '/location_id/main_' . $_REQUEST[ 'mainlocationid' ];
                            }
                            elseif ( $_REQUEST[ 'sublocationid' ] )
                            {
                                $url .= '/location_id/sub_' . $_REQUEST[ 'sublocationid' ];
                            }
                            if ( $_REQUEST[ 'id' ] )
                            {
                                $url .= '/category_id/' . $_REQUEST[ 'id' ];
                            }
                            break;
                        case "view":
                            $url .= '/shop/view';
                            if ( $_REQUEST[ 'id' ] )
                            {
                                $url .= '/shop_id/' . $_REQUEST[ 'id' ];
                            }
                            break;
                    }
                }
                else if ( strtolower( MODULE_NAME ) == 'event' )
                {
                    switch ( strtolower( ACTION_NAME ) )
                    {
                        case "lists":
                            $url .= '/event/search';
                            if ( $_REQUEST[ 'mainlocationid' ] )
                            {
                                $url .= '/location_id/main_' . $_REQUEST[ 'mainlocationid' ];
                            }
                            elseif ( $_REQUEST[ 'sublocationid' ] )
                            {
                                $url .= '/location_id/sub_' . $_REQUEST[ 'sublocationid' ];
                            }
                            if ( $_REQUEST[ 'cateid' ] )
                            {
                                $url .= '/category_id/' . $_REQUEST[ 'cateid' ];
                            }
                            break;
                        case "view":
                            $url .= '/event/view';
                            if ( $_REQUEST[ 'id' ] )
                            {
                                $url .= '/event_id/' . $_REQUEST[ 'id' ];
                            }
                            break;
                    }
                }

                if ( strtolower( MODULE_NAME ) != 'download' )
                {
                    header( 'location:' . $url );
                }
            }
        }
    }

    public function checkCache()
    {
        $this->cache_filename = '/tmp/cache/ujm' . preg_replace( '/[^a-zA-Z0-9]/', '_', urlencode( getenv( 'REQUEST_URI' ) . '?' . getenv( 'QUERY_STRING' ) ) );

        if ( getenv( 'SERVER_NAME' ) != 'dev.outstreet.com.hk' )
        {
            if ( !$_POST )
            {
                if ( !is_dir( '/tmp/cache' ) )
                {
                    mkdir( '/tmp/cache' );
                }
                else
                {
                    if ( file_exists( $this->cache_filename ) )
                    {
                        if ( time() - filemtime( $this->cache_filename ) <= 24 * 60 * 60 * 30 )
                        {
                            exit( base64_decode( file_get_contents( $this->cache_filename ) ) );
                        }
                    }
                }
            }
        }
    }

    public function view_loc()
    {
        $type                        = $_REQUEST[ 'type' ];
        $locationSubDao              = D( 'LocationSub' );
        $mainlocationid              = $_GET[ 'mainlocationid' ];
        $sublocationid               = $_GET[ 'sublocationid' ];
        $where                       = array();
        $where[ 'main_location_id' ] = $_REQUEST[ 'id' ];
        $locationSubList             = $locationSubDao->where( $where )->order( 'orders, id' )->select();

        if ( isset( $sublocationid ) )
        {
            $where          = array();
            $where[ 'id' ]  = $sublocationid;
            $curSubLocation = $locationSubDao->where( $where )->find();
            $this->assign( 'curSubLocation', $curSubLocation );
        }
        $this->assign( 'locationSubList', $locationSubList );
        $this->assign( 'type', $type );
        $this->display();
    }

    public function selshopcate()
    {
        //獲取Shop分類主目錄列表 壓入模板
        $cate    = M( 'ShopCate' )->where( array( 'type' => array( 'neq', 'iphone' ) ) )->getField( 'id,name' );
        $level_1 = M( 'ShopCate' )->where( array( 'level' => 1 ) )->getField( 'id,parent_id' );
        $level_2 = M( 'ShopCate' )->where( array( 'level' => 2 ) )->getField( 'id,parent_id' );
        $level_3 = M( 'ShopCate' )->where( array( 'level' => 3 ) )->getField( 'id,parent_id' );

        $cateList = array();
        foreach ( $level_1 AS $level_1_id => $level_1_parent_id )
        {
            if ( $cate[ $level_1_id ] )
                $cateList[ $level_1_id ][ 'name' ] = $cate[ $level_1_id ];
        }
        foreach ( $level_2 AS $level_2_id => $level_2_parent_id )
        {
            if ( $cateList[ $level_2_parent_id ][ 'name' ] && $cate[ $level_2_id ] )
                $cateList[ $level_2_parent_id ][ 'item' ][ $level_2_id ][ 'name' ] = $cate[ $level_2_id ];
        }
        foreach ( $level_3 AS $level_3_id => $level_3_parent_id )
        {
            if ( $cateList[ $level_2[ $level_3_parent_id ] ][ 'name' ] && $cateList[ $level_2[ $level_3_parent_id ] ][ 'item' ][ $level_3_parent_id ][ 'name' ] && $cate[ $level_3_id ] )
                $cateList[ $level_2[ $level_3_parent_id ] ][ 'item' ][ $level_3_parent_id ][ 'item' ][ $level_3_id ][ 'name' ] = $cate[ $level_3_id ];
        }

        //Shop Cate Ref List
        $shopCateList = D( 'ShopCate' )->field( 'id,reference_id' )->select();
        $refList      = array();
        foreach ( $shopCateList as $key => $shopCate )
        {
            $cateId = $shopCate[ 'id' ];
            $refId  = $shopCate[ 'reference_id' ];
            if ( $refId != 0 )
            {
                $refList[ $cateId ][] = $refId;
                $refList[ $refId ][]  = $cateId;
            }
        }
        foreach ( $refList as $key => $ref )
        {
            foreach ( $ref as $cateId )
            {
                foreach ( $refList[ $cateId ] as $elem )
                {
                    $refList[ $key ][] = $elem;
                }
            }
            $refList[ $key ] = array_unique( $refList[ $key ] );
        }

        $this->assign( 'refList', $refList );
        $this->assign( 'cateList', $cateList );
        $this->display();
    }

    public function seleventcate()
    {
        //獲取Event分類主目錄列表 壓入模板
        $cate    = M( 'EventCate' )->where( array( 'type' => array( 'neq', 'mobile' ) ) )->getField( 'id,name' );
        $level_1 = M( 'EventCate' )->where( array( 'level' => 1 ) )->getField( 'id,parent_id' );
        $level_2 = M( 'EventCate' )->where( array( 'level' => 2 ) )->getField( 'id,parent_id' );
        $level_3 = M( 'EventCate' )->where( array( 'level' => 3 ) )->getField( 'id,parent_id' );

        $cateList = array();
        foreach ( $level_1 AS $level_1_id => $level_1_parent_id )
        {
            if ( $cate[ $level_1_id ] )
                $cateList[ $level_1_id ][ 'name' ] = $cate[ $level_1_id ];
        }
        foreach ( $level_2 AS $level_2_id => $level_2_parent_id )
        {
            if ( $cateList[ $level_2_parent_id ][ 'name' ] && $cate[ $level_2_id ] )
                $cateList[ $level_2_parent_id ][ 'item' ][ $level_2_id ][ 'name' ] = $cate[ $level_2_id ];
        }
        foreach ( $level_3 AS $level_3_id => $level_3_parent_id )
        {
            if ( $cateList[ $level_2[ $level_3_parent_id ] ][ 'name' ] && $cateList[ $level_2[ $level_3_parent_id ] ][ 'item' ][ $level_3_parent_id ][ 'name' ] && $cate[ $level_3_id ] )
                $cateList[ $level_2[ $level_3_parent_id ] ][ 'item' ][ $level_3_parent_id ][ 'item' ][ $level_3_id ][ 'name' ] = $cate[ $level_3_id ];
        }

        //Event Cate Ref List
        $eventCateList = D( 'EventCate' )->field( 'id,reference_id' )->select();
        $refList       = array();
        foreach ( $eventCateList as $key => $eventCate )
        {
            $cateId = $eventCate[ 'id' ];
            $refId  = $eventCate[ 'reference_id' ];
            if ( $refId != 0 )
            {
                $refList[ $cateId ][] = $refId;
                $refList[ $refId ][]  = $cateId;
            }
        }

        foreach ( $refList as $key => $ref )
        {
            foreach ( $ref as $cateId )
            {
                foreach ( $refList[ $cateId ] as $elem )
                {
                    $refList[ $key ][] = $elem;
                }
            }
            $refList[ $key ] = array_unique( $refList[ $key ] );
        }

        $this->assign( 'refList', $refList );
        $this->assign( 'cateList', $cateList );
        $this->display();
    }

    public function _initialize()
    {
        import( '@.ORG.SimpleAcl' );

        try {

            $this->assign( 'fb_popup_register', 'false' );

            include COMMON_PATH . 'facebook/src/facebook.php';

            FB::instance( array( 'appId' => '159558357412421', 'secret' => '8cc5aa4c80af155824aa50c93de8539b', 'cookie' => true ) );

            if ( $this->fb_info = FB::instance()->api( '/me' ) )
            {
                if ( D( 'User' )->where( array( 'email' => strtolower( $this->fb_info[ 'email' ] ) ) )->count() )
                {
                    D( 'User' )->where( array( 'email' => strtolower( $this->fb_info[ 'email' ] ) ) )->save( array( 'facebook_id' => $this->fb_info[ 'id' ] ) );
                }
                else
                {
                    D( 'User' )->add( array(
                        'nick_name'   => $this->fb_info[ 'name' ],
                        'first_name'  => $this->fb_info[ 'first_name' ],
                        'last_name'   => $this->fb_info[ 'last_name' ],
                        'email'       => $this->fb_info[ 'email' ],
                        'facebook_id' => $this->fb_info[ 'id' ],
                    ) );
                }

                $userInfo = D( 'User' )->where( array( 'facebook_id' => $this->fb_info[ 'id' ] ) )->find();
                if ( count( $userInfo ) )
                {
                    $authInfo                  = $userInfo;
                    $authInfo[ 'is_facebook' ] = true;

                    SimpleAcl::saveLogin( $authInfo );
                }

                if ( $_REQUEST[ 'debug' ] )
                {
                    try {
                        print_r( FB::instance()->api( '/oauth/access_token', array(
                                'client_id'         => FB::instance()->getAppId(),
                                'client_secret'     => FB::instance()->getAppSecret(),
                                'fb_exchange_token' => FB::instance()->getAccessToken(),
                                'grant_type'        => 'fb_exchange_token',
                        ) ) );
                    } catch ( FacebookApiException $e ) {
                        echo '<pre>' . $e->getMessage() . '</pre>';
                    }
                }

                if ( $_SERVER[ 'HTTP_USER_AGENT' ] != 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)' )
                {
                    if ( $_REQUEST[ 'id' ] )
                    {
                        if ( strtoupper( ACTION_NAME ) == 'VIEW' )
                        {
                            try {
                                if ( strtoupper( MODULE_NAME ) == 'SHOP' )
                                {
                                    FB::instance()->api( '/me/news.reads', 'post', array( 'article' => C( 'OUTSTREET_DIR' ) . '/shop/view/id/' . $_REQUEST[ 'id' ] ) );
                                }
                                elseif ( strtoupper( MODULE_NAME ) == 'EVENT' )
                                {
                                    FB::instance()->api( '/me/news.reads', 'post', array( 'article' => C( 'OUTSTREET_DIR' ) . '/event/view/id/' . $_REQUEST[ 'id' ] ) );
                                }
                                elseif ( strtoupper( MODULE_NAME ) == 'PROMITION' )
                                {
                                    FB::instance()->api( '/me/news.reads', 'post', array( 'article' => C( 'OUTSTREET_DIR' ) . '/promotion/view/id/' . $_REQUEST[ 'id' ] ) );
                                }
                            } catch ( FacebookApiException $e ) {
                                if ( $_REQUEST[ 'debug' ] )
                                {
                                    echo '<pre>' . $e->getMessage() . '</pre>';
                                }
                            }
                        }
                    }
                }
            }
            elseif ( ( $authInfo = SimpleAcl::getLoginInfo() ) && @$authInfo[ 'is_facebook' ] )
            {
                SimpleAcl::clearLogin();
            }
        } catch ( Exception $e ) {

        }

        if ( SimpleAcl::checkLogin() )
            $this->assign( 'authInfo', SimpleAcl::getLoginInfo() );

        $this->assign( 'page', $_REQUEST[ C( 'VAR_PAGE' ) ] );

        //$searchLogDao = D('SearchLog');
        //$searchList = $searchLogDao->query('SELECT create_time,keywords,count(id) as search_count,results FROM __TABLE__ where results!=0 group by keywords order by search_count desc,results desc limit 10');
        //$this->assign('searchList',$searchList);
        //Start Customize Title, Description and Keywords for SEO
        $module              = strtoupper( MODULE_NAME );
        $action              = strtoupper( ACTION_NAME );
        $slogan              = 'OutStreet 出街 - 全港最新最多元化的出街指南';
        $sub_slogan          = '| ' . $slogan;
        $default_keywords    = '香港購物,出街,逛街,美食';
        $default_description = 'OutStreet 出街';

        switch ( $module )
        {
            case 'SHOP':
                switch ( $action )
                {
                    case 'LISTS':
                        $cate_id           = $_REQUEST[ 'id' ];
                        $cate_name         = D( 'ShopCate' )->where( 'id=' . $cate_id )->getField( 'name' );
                        $cate_english_name = D( 'ShopCate' )->where( 'id=' . $cate_id )->getField( 'english_name' );

                        $title .= $cate_name ? $cate_name : '出街指南';
                        $title .= ' ';
                        $title .= $cate_english_name ? $cate_english_name : 'Shopping Guide';

                        //$title .= $sub_slogan;

                        $keywords    = $default_keywords;
                        $description = $default_description;
                        break 2;
                    case 'VIEW':
                        //Start Title
                        if ( $_REQUEST[ 'id' ] )
                        {
                            $name         = D( 'Shop' )->where( 'id=' . $_REQUEST[ 'id' ] )->getField( 'name' );
                            $english_name = D( 'Shop' )->where( 'id=' . $_REQUEST[ 'id' ] )->getField( 'eng_name' );

                            if ( $name )
                            {
                                $title .= $name;
                                if ( $name != $english_name )
                                {
                                    $title .= ' ' . $english_name . ' ';
                                }
                            }
                        }

                        //$title .= $sub_slogan;
                        //End Title
                        //Start Keywords
                        if ( $_REQUEST[ 'id' ] )
                        {
                            $keyword = D( 'Shop' )->where( 'id=' . $_REQUEST[ 'id' ] )->getField( 'keyword' );
                            if ( $keyword )
                            {
                                $keywords = $keyword . ' ';
                            }

                            $sublocationname = D( 'Shop' )->relation( true )->where( 'id=' . $_REQUEST[ 'id' ] )->select();
                            $keywords .= $sublocationname[ 0 ][ 'sub_location' ][ 'name' ] . ' ';
                        }

                        $shopCateMapDao = D( 'ShopCateMap' );
                        $cateids        = $shopCateMapDao->where( 'shop_cate_map.shop_id=' . $_REQUEST[ 'id' ] )->select();
                        foreach ( $cateids as $vo )
                        {
                            if ( !empty( $vo[ 'cate_id' ] ) )
                            {
                                $keywords .= D( 'ShopCate' )->where( 'shop_cate.id=' . $vo[ 'cate_id' ] )->getField( 'name' ) . ' ';
                            }
                        }
                        //End Keywords
                        //Start Description
                        if ( !$_REQUEST[ 'id' ] )
                        {
                            $shopDescription = D( 'Shop' )->where( 'id=' . $_REQUEST[ 'id' ] )->getField( 'description' );
                            if ( !empty( $shopDescription ) )
                            {
                                $description = $shopDescription;
                            }
                            else
                            {
                                $description = $default_description;
                            }
                        }
                        else
                        {
                            $description = $default_description;
                        }
                        //End Description
                        break 2;
                    case 'SEARCH':
                        //Start Collect Cates
                        $i       = 0;
                        $cateids = explode( ',', $_REQUEST[ 'shop_cate_ids' ] );

                        foreach ( $cateids as $cateid )
                        {
                            if ( !empty( $cateid ) and $i < 4 )
                            {
                                $cate_name[ $i ] = D( 'ShopCate' )->where( 'id=' . $cateid )->getField( 'name' );
                                $i++;
                            }
                        }

                        //Start Collect Sublocation
                        $i              = 0;
                        $sublocationids = explode( ',', $_REQUEST[ 'sub_location_ids' ] );

                        foreach ( $sublocationids as $sublocationid )
                        {
                            if ( !empty( $sublocationid ) and $i < 4 )
                            {
                                $sub_location_name[ $i ] = D( 'LocationSub' )->where( 'id=' . $sublocationid )->getField( 'name' );
                                $i++;
                            }
                        }

                        //Start Title
                        $searchterms = $_REQUEST[ 'searchterms' ];
                        if ( $searchterms == '' or ( count( $sub_location_name ) > 0) or ( count( $cate_name ) > 0) )
                        {
                            if ( $searchterms != '' )
                            {
                                $title .= $searchterms . ' ';
                            }

                            for ( $i = 0; $i < 2; $i++ )
                            {
                                if ( !empty( $sub_location_name[ $i ] ) )
                                {
                                    $title .= $sub_location_name[ $i ] . ' ';
                                }
                            }

                            for ( $i = 0; $i < 2; $i++ )
                            {
                                if ( !empty( $cate_name[ $i ] ) )
                                {
                                    $title .= $cate_name[ $i ] . ' ';
                                }
                            }

                            //$title .= $sub_slogan;
                        }
                        else
                        {
                            $title = '出街指南 ' . $sub_slogan;
                        }
                        //End Title
                        //Start Keywords
                        if ( (count( $sub_location_name ) > 0) or ( count( $cate_name ) > 0) )
                        {
                            for ( $i = 0; $i < 4; $i++ )
                            {
                                if ( !empty( $sub_location_name[ $i ] ) )
                                {
                                    $keywords .= $sub_location_name[ $i ] . ' ';
                                }
                            }
                            for ( $i = 0; $i < 4; $i++ )
                            {
                                if ( !empty( $cate_name[ $i ] ) )
                                {
                                    $keywords .= $cate_name[ $i ] . ' ';
                                }
                            }
                        }
                        else
                        {
                            $keywords = $default_keywords;
                        }
                        //End Keywords
                        //Start Description
                        if ( (count( $sub_location_name ) > 0) or ( count( $cate_name ) > 0) )
                        {
                            for ( $i = 0; $i < 4; $i++ )
                            {
                                if ( !empty( $sub_location_name[ $i ] ) )
                                {
                                    $description .= $sub_location_name[ $i ] . ' ';
                                }
                            }
                            for ( $i = 0; $i < 4; $i++ )
                            {
                                if ( !empty( $cate_name[ $i ] ) )
                                {
                                    $description .= $cate_name[ $i ] . ' ';
                                }
                            }
                        }
                        else
                        {
                            $description = $default_description;
                        }
                        //End Description
                        break 2;
                    default:
                        $title       = '出街指南 ' . $sub_slogan;
                        $keywords    = $default_keywords;
                        $description = $default_description;
                        break 2;
                }
            case 'EVENT':
                switch ( $action )
                {
                    case 'LISTS':
                        $cate_id          = $_REQUEST[ 'cateid' ];
                        $main_location_id = $_REQUEST[ 'mainlocationid' ];
                        $sub_location_id  = $_REQUEST[ 'sublocationid' ];
                        if ( $cate_id == '' && $main_location_id == '' && $sub_location_id == '' )
                        {
                            $title = '節目';
                            //$title .= $sub_slogan;
                        }
                        else
                        {
                            $cate_name = D( 'EventCate' )->where( 'id=' . $cate_id )->getField( 'name' );
                            if ( $cate_name )
                            {
                                $title .= $cate_name . ' ';
                            }
                            //$title .= $sub_slogan;
                        }
                        $keywords    = $default_keywords;
                        $description = $default_description;
                        break 2;
                    case 'VIEW':
                        $id          = $_REQUEST[ 'id' ];
                        if ( $id == '' )
                        {
                            continue;
                        }
                        $name         = D( 'Event' )->where( 'id=' . $_REQUEST[ 'id' ] )->getField( 'name' );
                        $english_name = D( 'Event' )->where( 'id=' . $_REQUEST[ 'id' ] )->getField( 'english_name' );
                        if ( $name )
                        {
                            $title .= $name . ' ';
                        }
                        if ( $english_name && ($name != $english_name) )
                        {
                            $title .= $english_name . ' ';
                        }
                        //$title.= $sub_slogan;
                        $keywords    = $default_keywords;
                        $description = $default_description;
                        break 2;
                    case 'SEARCH':
                        $timediff    = $_REQUEST[ 'timediff' ];
                        $searchterms = $_REQUEST[ 'searchterms' ];
                        if ( $searchterms )
                        {
                            $title = $searchterms;
                            //$title .= $sub_slogan;
                        }
                        elseif ( $timediff )
                        {
                            switch ( strtoupper( $timediff ) )
                            {
                                case 'TODAY': $title .= '今天節目';
                                    break;
                                case 'TOMORROW': $title .= '明天節目';
                                    break;
                                case 'WEEK': $title .= '本週節目 ';
                                    break;
                                case 'WEEKEND': $title .= '本週末節目';
                                    break;
                                case 'MONTH': $title .= '本月節目';
                                    break;
                            }
                            //$title .= $sub_slogan;
                        }
                        else
                        {
                            $title = '節目';
                            //$title .= $sub_slogan;
                        }
                        $keywords    = $default_keywords;
                        $description = $default_description;
                        break 2;
                    default:
                        $title       = '節目';
                        //$title .= $sub_slogan;
                        $keywords    = $default_keywords;
                        $description = $default_description;
                        break 2;
                }
            case 'PROMOTION':
                switch ( $action )
                {
                    case 'VIEW':
                        $id   = $_REQUEST[ 'id' ];
                        $name = D( 'Promotion' )->where( 'id=' . $_REQUEST[ 'id' ] )->getField( 'name' );
                        if ( $name )
                        {
                            $title .= $name . ' ';
                        }
                        break;
                    default:
                        $title = '優惠';
                        break;
                }
                //$title .= $sub_slogan;
                $keywords    = $default_keywords;
                $description = $default_description;
                break;
            default:
                $title       = $slogan;
                $keywords    = $default_keywords;
                $description = $default_description;
        }
        $this->assign( 'title', $title );
        $this->assign( 'keywords', $keywords );
        $this->assign( 'description', $description );
        //End Customize Title, Description and Keywords for SEO

        $todaywea = M( 'weatherFields' )->where( array( 'date' => array( 'egt', date( 'Y-m-d' ) ) ) )->limit( 1 )->find();
        $tmrwea   = M( 'weatherFields' )->where( array( 'date' => array( 'gt', date( 'Y-m-d' ) ) ) )->limit( 1 )->find();
        $this->assign( "todaywea", $todaywea );
        $this->assign( "tmrwea", $tmrwea );

        $_POST    = array_map( 'htmlspecialchars', $_POST );
        $_REQUEST = array_map( 'htmlspecialchars', $_REQUEST );
        $_GET     = array_map( 'htmlspecialchars', $_GET );
    }

    public function successBox( $msg )
    {
        if ( !$this->get( 'waitSecond' ) )
        {
            $this->assign( 'waitSecond', 3 );
        }

        $this->assign( 'box', 1 );
        $this->success( $msg );
    }

    public function errorBox( $msg )
    {
        if ( !$this->get( 'waitSecond' ) )
        {
            $this->assign( 'waitSecond', 3 );
        }

        $this->assign( 'box', 1 );
        $this->error( $msg );
    }

    public function sellocation()
    {
        //獲取Shop分類主目錄列表 壓入模板
        $locationMainDao  = D( 'LocationMain' );
        $locationSubDao   = D( 'LocationSub' );
        $locationMainList = $locationMainDao->select();

        foreach ( $locationMainList as &$locationMain )
        {
            $where                             = array();
            $where[ 'main_location_id' ]       = $locationMain[ 'id' ];
            $locationMain[ 'locationSubList' ] = $locationSubDao->where( $where )->order( 'orders, id' )->select();
        }

        $this->assign( 'locationMainList', $locationMainList );
        $this->display();
    }

    /**
     * 对字符或者数组加逗号连接, 用来
     *
     * @param string/array $arr 可以传入数字或者字串
     * @return string 这样的格式: '1','2','3'
     */
    public function implode( $arr )
    {
        return implode( ",", ( array ) $arr );
    }
    /*     * ******* Action ******** */
    /* 1. unsubscribe */

    public function user_action_log( $mem_id, $action )
    {
        $useractionlogDao     = D( 'UserActionLog' );
        $data[ 'mem_id' ]     = $mem_id;
        $data[ 'action' ]     = $action;
        $data[ 'session_id' ] = session_id();
        $data[ 'ip' ]         = $_SERVER[ 'REMOTE_ADDR' ];
        $useractionlogDao->create( $data );
        $success              = $useractionlogDao->add();
        if ( $success )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /*     * ******* Type ******** */
    /* 2. Frontend */
    /*     * ******* Source ******** */
    /* 1. Request Shop Owner */
    /* 2. Member Validation */
    /* 3. Member Resend Password */

    public function smtp_mail_log( $sendto_mail, $subject = "", $body = "", $sendto_name = "", $type, $source )
    {

        vendor( "phpmailer.class#phpmailer" );
        $mail           = new PHPMailer();
        $mail->IsSMTP(); // set mailer to use SMTP
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->Host     = C( 'SMTP_SERVER' ); // specify main and backup server
        //$mail->SMTPSecure = "ssl";
        $mail->Port     = 25;
        $mail->Username = C( 'SMTP_USERNAME' ); // SMTP username
        $mail->Password = C( 'SMTP_PASSWORD' ); // SMTP password
        $mail->From     = C( 'SMTP_FROM' );
        $mail->FromName = C( 'SMTP_FROMNAME' );
        $mail->AddAddress( $sendto_mail, $sendto_name );
        $mail->CharSet  = 'utf8';
        $mail->Subject  = $subject;
        $mail->Body     = $body;
        $mail->AltBody  = "To view the message, please use an HTML compatible email viewer!";
        $mail->WordWrap = 50;
        $mail->MsgHTML( $body );
        $mail->IsHTML( true );
        if ( !$mail->Send() )
        {
            echo "Message could not be sent. <p>";
            echo "Mailer Error: " . $mail->ErrorInfo;
            exit;
        }
        else
        {
            //Start Save Email Log
            $memid            = 0;
            $userDao          = D( 'User' );
            $where            = array();
            $where[ 'email' ] = $sendto_mail;
            $userInfo         = $userDao->where( $where )->find();
            if ( !empty( $userInfo ) )
            {
                $memid = $userInfo[ 'id' ];
            }

            //Start Insert to DB
            $emailLogDao             = D( 'EmailLog' );
            $emailLogDao->create();
            $data                    = array();
            $data[ 'datetime' ]      = time();
            $data[ 'mem_id' ]        = $memid;
            $data[ 'email_address' ] = $sendto_mail;
            $data[ 'type' ]          = $type;
            $data[ 'source' ]        = $source;
            $emailLogDao->add( $data );
            //End Insert to DB
            //End Save Email Log
        }
    }

    public function mall()
    {
        $parent_shop_id = $_REQUEST[ 'parentshopid' ];

        //mall info
        $where               = array();
        $where[ 'id' ]       = $parent_shop_id;
        $where[ 'is_audit' ] = 1;
        $mall                = D( 'Shop' )->relation( true )->where( $where )->find();

        //shop info
        $where                     = array();
        $where[ 'parent_shop_id' ] = $parent_shop_id;
        $where[ 'is_audit' ]       = 1;
        $shopList                  = D( 'Shop' )->relation( true )->where( $where )->order( 'name' )->select();

        //cate info
        $shopcateList = array();

        //Level 2 cates
        foreach ( $shopList as $shop )
        {
            foreach ( $shop[ 'sub_cates' ] as $cates )
            {
                if ( $cates[ 'parent_id' ] != 0 )
                {
                    $shopcateList[] = $cates[ 'parent_id' ];
                }
            }
        }
        $where            = array();
        $where[ 'id' ]    = array( 'in', implode( ',', ( array ) $shopcateList ) );
        $where[ 'level' ] = 2;
        $cateList         = D( 'ShopCate' )->where( $where )->select();
        $this->assign( 'mall', $mall );
        $this->assign( 'cateList', $cateList );
        $this->display();
    }

    public function mallbox()
    {
        $parent_shop_id            = $_REQUEST[ 'parentshopid' ];
        $cateid                    = $_REQUEST[ 'cateid' ];
        //shop info
        $where                     = array();
        $where[ 'parent_shop_id' ] = $parent_shop_id;
        $where[ 'is_audit' ]       = 1;
        if ( $cateid != 0 && $cateid != '' )
        { //cate selected
            $shopcateList    = array();
            $shopcateMapList = D( 'ShopCateMap' )->join( 'shop_cate on shop_cate.id = shop_cate_map.cate_id' )->where( 'shop_cate.parent_id =' . $cateid )->select();
            foreach ( $shopcateMapList as $shop )
            {
                $shopcateList[] = $shop[ 'shop_id' ];
            }
            $str           = implode( ',', ( array ) $shopcateList );
            $where[ 'id' ] = array( 'in', $str );
        }
        $count     = D( 'Shop' )->relation( true )->where( $where )->count();
        import( 'ORG.Util.Page' );
        $p         = new Page( $count );
        $multipage = $p->show();
        $limit     = $p->firstRow . ',' . $p->listRows;
        $shopList  = D( 'Shop' )->relation( true )->where( $where )->limit( $limit )->order( 'shop_status, name' )->select();
        $this->assign( 'multipage', $multipage );
        $this->assign( 'shopList', $shopList );
        $this->display();
    }

    public function brand()
    {
        $brandid = $_REQUEST[ 'brandid' ];

        //mall info
        $where         = array();
        $where[ 'id' ] = $brandid;
        $brand         = D( 'Brand' )->where( $where )->find();

        //shop info
        $where                = array();
        $where[ 'brand_id' ]  = $brandid;
        $where[ 'is_audit' ]  = 1;
        $shopList             = D( 'Shop' )->relation( true )->where( $where )->select();
        $brand[ 'sub_cates' ] = $shopList[ 0 ][ 'sub_cates' ];
        $location_mainList    = array();
        foreach ( $shopList as $shop )
        {
            $location_mainList[ $shop[ 'main_location' ][ 'id' ] ] = $shop[ 'main_location' ];
        }
        asort( $location_mainList );
        $this->assign( 'location_mainList', $location_mainList );
        $this->assign( 'brand', $brand );
        $this->display();
    }

    public function brandbox()
    {
        $brandid = $_REQUEST[ 'brandid' ];

        //shop info
        $where               = array();
        $where[ 'brand_id' ] = $brandid;
        $where[ 'is_audit' ] = 1;

        $sel_loc = $_REQUEST[ 'sel_loc' ];
        if ( $sel_loc != 0 && $sel_loc != '' )
        { //cate selected
            $where[ 'main_location_id' ] = $sel_loc;
        }
        $count     = D( 'Shop' )->relation( true )->where( $where )->count();
        import( 'ORG.Util.Page' );
        $p         = new Page( $count );
        $multipage = $p->show();
        $limit     = $p->firstRow . ',' . $p->listRows;
        $shopList  = D( 'Shop' )->relation( true )->where( $where )->limit( $limit )->order( 'shop_status, sub_location_id' )->select();
        $this->assign( 'multipage', $multipage );
        $this->assign( 'shopList', $shopList );
        $this->display();
    }

    public function getRealIpAddr()
    {
        if ( !empty( $_SERVER[ 'HTTP_CLIENT_IP' ] ) )
        {
            $ip = $_SERVER[ 'HTTP_CLIENT_IP' ];
        }
        elseif ( !empty( $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] ) )
        {
            $ip = $_SERVER[ 'HTTP_X_FORWARDED_FOR' ];
        }
        else
        {
            $ip = $_SERVER[ 'REMOTE_ADDR' ];
        }

        return $ip;
    }
}
