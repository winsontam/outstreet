<?php

class UserAction extends BaseAction{

	public function unsubscribe(){
		$action=$_REQUEST['action'];
		$user_id=$_REQUEST['id'];
		$userDao=D('User');
		if($action==''){
			$key= C('EMAIL_ENCRYPT_KEY');
			
			if(md5($_REQUEST['id'].$key)==$_REQUEST['hash']){				
				$userInfo=$userDao->find($user_id);
				$this->assign('userInfo',$userInfo);
				$this->display();
			}else{			
				$this->error('取消訂閱失敗, 請聯絡我們');
			}
		}elseif($action==1){
			$data['acc_edm']='0';
			$userDao->where('id='.$user_id)->save($data);
			$success=$this->user_action_log($user_id,1);
			if($success){									
				echo json_encode(array('result'=>1));
			}else{
				echo json_encode(array('result'=>0));
			}
		}
		
	}

    public function index(){
    	$userDao = D('User');
		$shopDao = D('Shop');
		$eventDao = D('Event');
		$promotionDao = D('Promotion');
    	$userStatisticsDao = D('StatisticsUser');
		$reviewDao = D('Review');

    	$userlist = $userStatisticsDao->relation(true)->order('review_count desc,favorite_me_count desc,picture_count desc')->limit(14)->select();
    	$this->assign('userlist',$userlist);
    	$this->assign('userCount',$userDao->where('is_email_verify=1')->count());

		foreach($userlist as $idx => $value){
			$shopSql = "select id, create_time, title, user_id, is_audit, 'shop' as for_type, shop_id as for_id from review_fields where shop_id<>0";
			//$eventSql = "union select id, create_time, title, user_id, is_audit, 'event' as for_type, event_id as for_id from review_fields where event_id<>0";
			$promotionSql = "union select id, create_time, title, user_id, is_audit, 'promotion' as for_type, promotion_id as for_id from review_fields where promotion_id<>0";

			$where = 'where user_id="'.$value['user_id'].'" and is_audit = 1';

			$selectSql = "select * from ($shopSql $eventSql $promotionSql) as temp $where order by create_time desc limit 1";

			$model = new Model();
			$userReview[$value['user_id']] = $model->query($selectSql);
			
			foreach($userReview[$value['user_id']] as &$review){
				$review['user'] = $userDao->find($review['user_id']);
				
				switch($review['for_type']){
					case 'shop':
						$review['shop'] = $shopDao->relation(true)->find($review['for_id']);
					break;
					case 'event':
						$review['event'] = $eventDao->relation(true)->find($review['for_id']);
					break;
					case 'promotion':
						$review['promotion'] = $promotionDao->relation(true)->find($review['for_id']);
					break;
				}
			}
		}

		$this->assign('userReview',$userReview);

		$where = array();
		$where['is_email_verify'] = 1;
    	$newuserlist = $userDao->where($where)->relation(true)->limit(5)->order('id desc')->select();
    	$this->assign('newUserList',$newuserlist);

		$where = array();
		$where['is_audit'] = 1;
		$reviewList = $reviewDao->where($where)->relation(true)->order('id desc')->limit(5)->select();
		$this->assign('reviewList', $reviewList);

    	$threadDao = D('ForumThread');
    	$threadList = $threadDao->order('post_count desc')->limit(5)->select();
    	$this->assign('threadList', $threadList);
    	$this->display();
    }
	
	public function sellocation(){
		//獲取Shop分類主目錄列表 壓入模板
		$type=$_REQUEST['type'];
		//echo $type;
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');
		$locationMainList = $locationMainDao->select();
		 
		foreach($locationMainList as &$locationMain){
			$where = array();
			$where['main_location_id'] = $locationMain['id'];
			$locationMain['locationSubList'] = $locationSubDao->where($where)->select();
		}
		 
		$this->assign('type',$type); 
		$this->assign('locationMainList', $locationMainList);
		$this->display();
	}
	
	public function seleventcate(){
		//獲取Event分類主目錄列表 壓入模板
		$cate = M('EventCate')->where(array('type'=>array('neq','mobile')))->getField('id,name');
		$level_1 = M('EventCate')->where(array('level'=>1))->getField('id,parent_id');
		$level_2 = M('EventCate')->where(array('level'=>2))->getField('id,parent_id');

		$cateList = array();
		foreach ( $level_1 AS $level_1_id => $level_1_parent_id )
		{
			if ($cate[ $level_1_id ])
				$cateList[ $level_1_id ][ 'name' ] = $cate[ $level_1_id ];
		}
		foreach ( $level_2 AS $level_2_id => $level_2_parent_id )
		{
			if ($cateList[$level_2_parent_id][ 'name' ] && $cate[ $level_2_id ])
				$cateList[$level_2_parent_id]['item'][ $level_2_id ][ 'name' ] = $cate[ $level_2_id ];
		}
		
		//Event Cate Ref List
		$eventCateList = D('EventCate')->field('id,reference_id')->select();
		$refList = array();
		foreach($eventCateList as $key=>$eventCate){
			$cateId = $eventCate['id'];
			$refId = $eventCate['reference_id'];
			if($refId!=0){
				$refList[$cateId][] = $refId;
				$refList[$refId][] = $cateId;
			}
		}
		foreach($refList as $key=>$ref){
			foreach($ref as $cateId){
				foreach($refList[$cateId] as $elem){
					$refList[$key][] = $elem;	
				}
			}
			$refList[$key] = array_unique($refList[$key]);
		}
			
		$this->assign('refList', $refList);
		$this->assign('cateList', $cateList);
		$this->display();
	}
    
	public function lists(){
		$userDao = D('User');
		$where['is_email_verify'] = 1;
		//設置分頁代碼
		$count = $userDao->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
			
		//獲取數據列表
		$userList = $userDao->relation(true)->where($where)->limit($limit)->order('create_time desc')->select();
	
		//數據壓入模板
		$this->assign('multipage', $multipage);
		$this->assign('userList', $userList);
		$this->display();
  	}
    
    private function _userStampEncode($id,$createtime){
    	return base64_encode(rand(1000,9999) . $createtime . $id);
    }
    
    private function _userStampDecode($userstamp){
    	$userstamp = base64_decode($userstamp);
    	
    	if ($len = strlen($userstamp) < 15){
    		return false;
    	}else{
    		$where = array();
    		$where['create_time'] = substr($userstamp, 4, 10);
    		$where['id'] = substr($userstamp, 14);
    		return $where;
    	}
    }
    
    private function sendValidateEmail($userid){
    	$userDao = D('User');
    	$userInfo = $userDao->find($userid);
    	
		$caption = '多謝註冊成為OutStreet會員！請確認你的電郵。';
		$this->assign('nickname', $userInfo['nick_name']);
		$this->assign('caption', '多謝註冊成為OutStreet會員！請確認你的電郵。');
		$this->assign('userstamp', $this->_userStampEncode($userid, $userInfo['create_time']));
		$this->assign('domain', $_SERVER['SERVER_NAME']);
		$body = $this->fetch('email_validate');
		
		if($userInfo['email'] != '') {
			$this->smtp_mail_log($userInfo['email'],$caption,$body,'',2,2);
		}
    }
    
    public function view(){
    	$id = $_GET['id'];
    	$userDao = D('User');
		$shop = D('Shop');
		$where = getOwnShopsWhere($id);
		$shopCount .= $shop->where($where)->count();
		if($shopCount == ''){
			$shopCount = 0;	
		}

    	$userInfo = $userDao->relation(true)->find($id);
	    if ($userInfo){
			$authInfo =  SimpleAcl::getLoginInfo();
			if($userInfo['id']==$authInfo['id']){//self panel
				if($authInfo['user_type']==0){
					$isOwner=false;	
				}else{
					$isOwner=true;			
				}
			}else{
					$isOwner=false;
			}
			if(!$isOwner){
				foreach($userInfo['favoriteMeUsers'] as &$fans){
					$fans['detail'] = $userDao->find($fans['user_id']);
				}
				$shopPictureDao = D('ShopPicture');
				$where = array();
				$where['user_id'] = $userInfo['id'];
				$shopPictureList = $shopPictureDao->where($where)->limit(5)->order('create_time desc')->select();
				$this->assign('shopPictureList',$shopPictureList);
			}else{
				
			}
			$this->assign('userInfo',$userInfo);			
			$this->assign('isOwner',$isOwner);
			$this->assign('shopCount',$shopCount);
			
	    	$this->display('view');
	    }else{
	    	$this->error('請確認您的鏈接來自本網站');
	    }
    }

    public function viewComments(){
		$userId = $_GET['id'];
    	$where = array();
    	$where['user_id'] = $userId;
		$where['is_audit'] = 1;
     	
    	$userMessageDao = D('UserMessage');
		$count = $userMessageDao->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->showAjax();
    	$this->assign('multipage',$multipage);
		$limit = $p->firstRow.','.$p->listRows;
    	$messageList = $userMessageDao->relation(true)->where($where)->limit($limit)->order('create_time desc')->select();
    	$this->assign('messageList',$messageList);
    	
    	$userDao = D('User');
    	$this->assign('userInfo', $userDao->find($userId));
    	$this->display('view-comments');
    }

    public function viewPhotos(){
    	$userId = $_GET['id'];
		$photoList = array();
		$reviewPictureDao = D('ReviewPicture');

		$count = $reviewPictureDao->join('review_fields on review_pictures.review_id = review_fields.id')->where(array('review_fields.user_id'=>$userId,'review_fields.is_audit'=>1))->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->showAjax();
    	$this->assign('multipage',$multipage);
		$limit = $p->firstRow.','.$p->listRows;

		$reviewPictureList = $reviewPictureDao->field('review_fields.shop_id as shop_id,review_fields.event_id as event_id,review_fields.promotion_id as promotion_id,review_pictures.description as description,review_pictures.file_path as file_path')->join('review_fields on review_pictures.review_id = review_fields.id')->where(array('review_fields.user_id'=>$userId,'review_fields.is_audit'=>1))->select();

		$i = 0;
		foreach($reviewPictureList as &$pictures){
			if($pictures['shop_id'] > 0) {
				$photoList[$i]['type'] = 'shop';
			} else if($pictures['event_id'] > 0) {
				$photoList[$i]['type'] = 'event';
			} else if($pictures['promotion_id'] > 0) {
				$photoList[$i]['type'] = 'promotion';
			}
			
			$photoList[$i]['description'] = $pictures['description'];
			$photoList[$i]['file_path'] = $pictures['file_path'];
			$i++;
		}

    	$this->assign('photoList',$photoList);
    	$this->display('view-photos');
    }

    public function viewReviews(){
	   	$userId = $_GET['id'];
    	$reviewDao = D('Review');
    	$count = $reviewDao->where("user_id = $userId and is_audit = 1")->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->showAjax();
		$limit = $p->firstRow.','.$p->listRows;

    	$reviewList = $reviewDao->relation(true)->where("user_id = $userId and is_audit = 1")->limit($limit)->order('create_time desc')->select();
		foreach($reviewList as $key=>$review){
			if($review['shop_id']>0){
				$reviewList[$key]['belongs'] = D('Shop')->where('id='.$review['shop_id'])->find();
			}elseif($review['event_id']>0){
				$reviewList[$key]['belongs'] = D('Event')->where('id='.$review['event_id'])->find();				
			}elseif($review['promotion_id']>0){
				$reviewList[$key]['belongs'] = D('Promotion')->where('id='.$review['promotion_id'])->find();			
			}
		}
    	$this->assign('reviewList',$reviewList);
    	$this->assign('multipage',$multipage);
    	$this->display('view-reviews');
    }

    public function viewStores(){
	   	$userId = $_GET['id'];
    	
    	$where = array();
    	$where['user_id'] = $userId;
    	
	  	$userShopFavorite = D('UserShopFavorite');
	  	
		$count = $userShopFavorite->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->showAjax();
		$limit = $p->firstRow.','.$p->listRows;
	  	
    	$favoriteList = $userShopFavorite->relation(true)->where($where)->limit($limit)->order('create_time desc')->select();
		
		foreach($favoriteList as &$shop){
			$shop['shop']['website']=getShopDetail($shop['shop'],2);
			// Temp for list image (20100621 by tammy)
			$shopDao = D('Shop');
			$brandDao = D('Brand');
			$sqlImg = "select file_path from shop_picture where shop_id = ".$shop['shop_id'];
			
			$shopImg = $shopDao->query($sqlImg);
			
			if (!empty($shopImg[0]['file_path'])){
				$shop['shop']['pictures'][0]['file_path'] = $shopImg[0]['file_path'];
			} else {
				$brandInfo = $brandDao->where('id='.$shop['shop']['brand_id'])->select();
				
				if (!empty($brandInfo[0]['file_path'])){
					$shop['shop']['pictures'][0]['file_path'] = $brandInfo[0]['file_path'];
				}
			}
			// Temp for list image (20100621 by tammy)
			
			//working hour logic
			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shop['id']);
			$working_hour = $shopTimeDao->where($where)->order('start_time,end_time,recurring_week')->select();
			
			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shop['id'],"recurring_week "=>0);
			$special_working_hour = $shopTimeDao->where($where)->order('start_day,end_day')->select();
			
			$output = generateShopWorkTime($shop['shop']['work_time'],$working_hour,$special_working_hour);
			
			$shop['shop']['work_time'] = $output;
			//end working hour logic
		}
		
		$this->assign('multipage',$multipage);
    	$this->assign('favoriteList',$favoriteList);
    	$this->display('view-stores');
    }

    public function viewOwnShopAddnews(){
		$shopnewsDao		= D('ShopNews');
		$shopnewsPictureDao	= D('ShopNewsPicture');
		$type				= $_REQUEST['type'];
		$shop_id			= $_REQUEST['shop_id'];
		$row_id				= $_REQUEST['id'];
		switch($type){
			case 'submitadd':
				$data['shop_id']	= $_REQUEST['shop_id'];
				$data['title']		= $_REQUEST['title'];
				$data['picstamp']	= $_REQUEST['picstamp'];
				$data['description']= $_REQUEST['description'];
				$data['time_start'] = $_REQUEST['time_start'];
				$data['time_end']   = $_REQUEST['time_end'];
				$data['create_time']= time();
				
				if($_REQUEST['time_start'] == '' || $_REQUEST['time_end'] == '') {
					$return['success']=0;
					$return['msg']=4;
				} else if($_REQUEST['time_start'] > $_REQUEST['time_end']) {
					$return['success']=0;
					$return['msg']=5;
				} else {
					$row_id = $shopnewsDao->add($data);
					
					if($row_id){
						/*********** Start Add News ID in shop news pic ***********/
						$picData['shop_news_id']	= $row_id;
						$shopnewsPictureDao->where('picstamp='.$_REQUEST['picstamp'])->save($picData);
						/*********** End Add News ID in shop news pic ***********/
						$return['success']=1;
						$return['msg']=1;
					}else{
						$return['success']=0;
						$return['msg']=0;
					}
				}
				echo json_encode($return);
			break;
			case 'submitedit':
				$data['shop_id']	= $_REQUEST['shop_id'];
				$data['title']		= $_REQUEST['title'];
				$data['picstamp']	= $_REQUEST['picstamp'];
				$data['description']= $_REQUEST['description'];
				$data['time_start'] = $_REQUEST['time_start'];
				$data['time_end']   = $_REQUEST['time_end'];
				$id					= $_REQUEST['row_id'];
				$data['update_time']= time();

				if($_REQUEST['time_start'] == '' || $_REQUEST['time_end'] == '') {
					$return['success']=0;
					$return['msg']=4;
				} else if($_REQUEST['time_start'] > $_REQUEST['time_end']) {
					$return['success']=0;
					$return['msg']=5;
				} else {
					if($shopnewsDao->where('id='.$id)->save($data)){
						/*********** Start Add News ID in shop news pic ***********/
						$picData['shop_news_id']	= $id;
						$shopnewsPictureDao->where('picstamp='.$_REQUEST['picstamp'])->save($picData);
						/*********** End Add News ID in shop news pic ***********/
						$return['success']=1;
						$return['msg']=2;
					}else{
						$return['success']=0;
						$return['msg']=3;
					}
				}
				echo json_encode($return);
			break;
			case 'add':
				$this->assign('shop_id',$shop_id);
				$this->assign('type',$type);
				$this->assign('picstamp', time().rand(1000,9999));
				$this->display();
			break;
			case 'edit':
				$newsInfo=$shopnewsDao->find($row_id);
				$newsInfo['picture']=$shopnewsPictureDao->where('picstamp='.$newsInfo['picstamp'])->select();
				$this->assign('newsInfo',$newsInfo);
				$this->assign('row_id',$row_id);
				$this->assign('shop_id',$shop_id);
				$this->assign('path',C('OUTSTREET_DIR').C('SHOPNEWS_UPFILE_PATH'));
				$this->assign('picstamp', $newsInfo['picstamp']);
				$this->assign('type',$type);
				$this->display();
			break;
		}
	}

    public function viewFavoriteUsers(){
	   	$userId = $_GET['id'];
    	
    	$where = array();
    	$where['user_id'] = $userId;
    	
	  	$userUserFavorite = D('UserUserFavorite');
	  	$count = $userUserFavorite->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->showAjax();
		$limit = $p->firstRow.','.$p->listRows;
	  	$userList = $userUserFavorite->relation(true)->where($where)->limit($limit)->order('create_time desc')->select();

    	$this->assign('multipage',$multipage);
    	$this->assign('userList',$userList);
    	$this->display('view-favoriteusers');
    }

	public function countnews(){
		$shopNewsDao=D('ShopNews');
		$shop_id=$_REQUEST['shop_id'];
		$newslist=$shopNewsDao->field('*, FROM_UNIXTIME(create_time,"%Y-%m-%d") as newcreatetime')->where('shop_id='.$shop_id)->select();
		echo json_encode(array('newslist'=>$newslist));
	}

	public function countpdf(){
		$shoppdfDao=D('ShopPdf');
		$shop_id=$_REQUEST['shop_id'];
		$current_pdf_num=$shoppdfDao->where('shop_id='.$shop_id)->select();
		echo json_encode(array('current_pdf_num'=>$current_pdf_num,'audit_pdf_num'=>$audit_pdf_num,'max_pdf'=>$max_pdf));
	}

	public function auditpdfrecord(){
		$shoppdfDao=D('ShopPdf');
		$shopDao=D('Shop');
		$row_id=$_REQUEST['id'];
		$isaudit=$_REQUEST['isaudit'];
		$shop_id=$shoppdfDao->where('id='.$row_id)->getField('shop_id');
		$shop_level=$shopDao->where('id='.$shop_id)->getField('shop_level');
		$max_pdf=getShopLevelSetting('pdf_num',$shop_level);
		if($isaudit==1){
			$where=array();
			$where['is_audit']=1;
			$where['shop_id']=$shop_id;
			$auditedpdfnumber=$shoppdfDao->where($where)->count();
			if($auditedpdfnumber+1>$max_pdf){
				$return['success']=0;
				$return['msg']=0;
			}else{
				$data['is_audit']=$isaudit;
				if($shoppdfDao->where('id='.$row_id)->save($data)){
					$return['success']=1;
				}
			}
		}else{
			$data['is_audit']=$isaudit;
			if($shoppdfDao->where('id='.$row_id)->save($data)){
				$return['success']=1;
			}
		}
		echo json_encode($return);
	}

	public function deletepdfrecord(){
		$shoppdfDao=D('ShopPdf');
		$shop_id=$_REQUEST['shop_id'];
		$row_id=$_REQUEST['id'];
		$filename="doc_".$shop_id."_".$row_id.".pdf";
		if(unlink(C('OUTSTREET_ROOT').'/'.C('SHOP_PDF_PATH').'/'.$filename)){
			$shoppdfDao->where('id='.$row_id)->delete();
			$return['success']=1;
		}else{
			$return['success']=0;
		}
		echo json_encode($return);
	}

	public function auditnewsrecord(){
		$shopnewsDao	= D('ShopNews');
		$shopDao		= D('Shop');
		$row_id			= $_REQUEST['id'];
		$isaudit		= $_REQUEST['isaudit'];
		$shop_id		= $shopnewsDao->where('id='.$row_id)->getField('shop_id');
		$shop_level		= $shopDao->where('id='.$shop_id)->getField('shop_level');
		$max_news		= getShopLevelSetting('news_num',$shop_level);

		if($isaudit==1){
			$where['shop_id']=$shop_id;
			$where['is_audit']=1;
			$auditednewsnumber=$shopnewsDao->where($where)->count();
			if($auditednewsnumber+1>$max_news){
				$return['success']=0;
				$return['msg']=0;
			}else{
				$data['is_audit']=$isaudit;
				if($shopnewsDao->where('id='.$row_id)->save($data)){
					$return['success']=1;
				}else{
					$return['success']=0;
					$return['msg']=1;
				}
			}
		}else{
			$data['is_audit']=$isaudit;
			if($shopnewsDao->where('id='.$row_id)->save($data)){
				$return['success']=1;
			}else{
				$return['success']=0;
				$return['msg']=1;
			}
		}
		echo json_encode($return);
	}

	public function deletenewsrecord(){
		$row_id=$_REQUEST['id'];
		$shop_id=$_REQUEST['shop_id'];
		$shopnewsDao=D('ShopNews');
		$shopnewsPictureDao=D('ShopNewsPicture');
		$picstamp=$shopnewsDao->where('id='.$row_id)->getField('picstamp');
		$picture=$shopnewsPictureDao->where('picstamp='.$picstamp)->find();
		if($picture!=''){
			unlink(C('OUTSTREET_ROOT').'/'.C('SHOPNEWS_UPFILE_PATH').'/'.$picture['file_path']);
			unlink(C('OUTSTREET_ROOT').'/'.C('SHOPNEWS_UPFILE_PATH').'/thumb_'.$picture['file_path']);
		}
		$shopnewsPictureDao->delete($picture['id']);
		if($shopnewsDao->delete($row_id)){
			$return['success']=1;
		}else{
			$return['success']=0;
		}
		echo json_encode($return);
	}

	public function viewOwnAdditionalData(){
		$userId = $_GET['user_id'];
		$shopId = $_GET['shop_id'];
		$userDao = D('User');
		$shopDao=D('Shop');	
    	$where = array();
    	$where['id'] = $userId;
		$userInfo = $userDao->where($where)->find();
		$shop_id=$shopId;
		$shop_level=$shopDao->where('id='.$shop_id)->getField('shop_level');
		$pdf_num=getShopLevelSetting('pdf_num',$shop_level);
		$news_num=getShopLevelSetting('news_num',$shop_level);

		//check shop level downgrade problem
		$shopNewsDao=D('ShopNews');
		$audit_news_num=$shopNewsDao->where(array('shop_id'=>$shop_id,'is_audit'=>1))->count();
		$shop_level=$shopDao->where('id='.$shop_id)->getField('shop_level');
		$max_news=getShopLevelSetting('news_num',$shop_level);
		if($audit_news_num>$max_news){
			$shopNewsDao->query('update shop_news set is_audit = 0 where shop_id ='.$shop_id);
		}
		$shoppdfDao=D('ShopPdf');
		$current_pdf_num=$shoppdfDao->where('shop_id='.$shop_id)->select();
		$audit_pdf_num=$shoppdfDao->where(array('shop_id'=>$shop_id,'is_audit'=>1))->count();
		$shop_level=$shopDao->where('id='.$shop_id)->getField('shop_level');
		$max_pdf=getShopLevelSetting('pdf_num',$shop_level);
		if($audit_pdf_num>$max_pdf){
			$shoppdfDao->query('update shop_pdf set is_audit = 0 where shop_id ='.$shop_id);
		}

		$this->assign('news_num',$news_num);
		$this->assign('pdf_num',$pdf_num);
		$this->assign('userInfo',$userInfo);
		$this->display('viewOwnAdditionalData');
	}

	public function viewOwnShop(){
	   	$userId = $_GET['id'];
		$userOwnShop = D('Shop');
		$where = getOwnShopsWhere($userId);
		$count = $userOwnShop->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->showAjax();
		$limit = $p->firstRow.','.$p->listRows;
		$ownShopList = $userOwnShop->relation(true)->where($where)->limit($limit)->order('create_time desc')->select();		

		foreach($ownShopList as &$shop){
			//Temp for list image (20100621 by tammy)
			$shopDao = D('Shop');
			$brandDao = D('Brand');
			//$sqlImg = "select file_path from shop_picture where shop_id = ".$shop['shop_id'];
			$sqlImg = "select file_path from shop_picture where shop_id = ".$shop['id'];
			
			$shopImg = $shopDao->query($sqlImg);
			
			if (!empty($shopImg[0]['file_path'])){
				//$shop['shop']['pictures'][0]['file_path'] = $shopImg[0]['file_path'];
				$shop['pictures'][0]['file_path'] = $shopImg[0]['file_path'];				
			} else {
				//$brandInfo = $brandDao->where('id='.$shop['shop']['brand_id'])->select();
				$brandInfo = $brandDao->where('id='.$shop['brand_id'])->select();				
				
				if (!empty($brandInfo[0]['file_path'])){
					//$shop['shop']['pictures'][0]['file_path'] = $brandInfo[0]['file_path'];
					$shop['pictures'][0]['file_path'] = $brandInfo[0]['file_path'];					
				}
			}
			// Temp for list image (20100621 by tammy)
			
			//working hour logic
			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shop['id']);
			$working_hour = $shopTimeDao->where($where)->order('start_time,end_time,recurring_week')->select();
			
			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shop['id'],"recurring_week "=>0);
			$special_working_hour = $shopTimeDao->where($where)->order('start_day,end_day')->select();
			
			//$output = generateShopWorkTime($shop['shop']['work_time'],$working_hour,$special_working_hour);
			$output = generateShopWorkTime($shop['work_time'],$working_hour,$special_working_hour);			
			
			//$shop['shop']['work_time'] = $output;
			$shop['work_time'] = $output;
			//end working hour logic
		}

		$this->assign('multipage',$multipage);
    	//$this->assign('favoriteList',$favoriteList);
    	$this->assign('ownShopList',$ownShopList);
    	$this->display('viewOwnShop');
    }

    //登錄
    public function login(){
    	C('SHOW_PAGE_TRACE',false);
    	
    	$userDao = D('User');
		import ( '@.ORG.SimpleAcl' );
    	
    	if(SimpleAcl::getLoginInfo()){
    		$this->redirect('mypanel');
    	}
    	
    	if ($_POST['submit']){
    		
       		Import('Think.Util.Cookie');

			$type = $_REQUEST['type'];	

			$where = array();
			$where['email'] = $_POST['email'];
			$where['password'] = md5($_POST['password']);
			if($type==1){
				$where['user_type']	=1;
			}

			if ($userInfo = SimpleAcl::authenticate($where)){
				if($userInfo['is_disabled']==0){
					if ($userInfo['is_email_verify'] == 1){
							
						$authInfo = array();
						$authInfo['id'] = $userInfo['id'];
						if($userInfo['user_type']==1){
							$authInfo['nick_name'] = '店主'.$userInfo['o_contact_person'];
						}else{
							$authInfo['nick_name'] = $userInfo['nick_name'];						
						}
						
						$authInfo['email'] = $userInfo['email'];
						$authInfo['sex'] = $userInfo['sex'];
						$authInfo['face_path'] = $userInfo['face_path'];
						$authInfo['levels'] = $userInfo['levels'];
						$authInfo['user_type'] = $userInfo['user_type'];
						
						SimpleAcl::saveLogin($authInfo);
						
						$userDao->where('id='.$userInfo['id'])->setField('last_login',time());
	
						if($userInfo['user_type']==0){
							if (!empty($_SESSION['NOT_AUTH_ACTION'])){
								$this->assign('jumpUrl', $_SESSION['url']);
								$_SESSION['NOT_AUTH_ACTION'] = '';
							}else{
								$this->assign('jumpUrl', $_SESSION['url']);
							}
						}else{
							$this->assign('jumpUrl','__ROOT__/User/mypanel');	
						}
						
						if (isset($_REQUEST['box'])){
							$this->successBox('登錄成功');
						}else{
							$this->success('登錄成功');
						}
				
					}else{
						//尚未進行email認證
						$_SESSION['userid'] = $authInfo['id'];
						$this->redirect('changeValidateEmail');
					}
				}else{
					if (isset($_REQUEST['box'])){
						$this->errorBox('賬號尚未啟用');
					}else{
						$this->error('賬號尚未啟用');
					}
				}
			}else{
				if (isset($_REQUEST['box'])){
					$this->errorBox('登錄失敗，請檢查您的賬號密碼');
				}else{
					$this->error('登錄失敗，請檢查您的賬號密碼');
				}
			}
    	}else{
    		
  			if (!empty($_SERVER['HTTP_REFERER'])){
    			//Cookie::set('NOT_AUTH_ACTION',$_SERVER['HTTP_REFERER']);
  			}
    		if(isset($_GET['box'])){
				$this->display('box-login');
    		}else{
				$this->display();
    		}
    	}
    }
    
	public function owner_login(){
		C('SHOW_PAGE_TRACE',false);
		import ( '@.ORG.SimpleAcl' );
		$authInfo =  SimpleAcl::getLoginInfo();		
		if($authInfo['user_type']==1){
			$this->redirect('mypanel');
		}else{
			$this->display();
		}
	}

	//登出
    public function logout(){
		import ( '@.ORG.SimpleAcl' );
		SimpleAcl::clearLogin();

		$this->assign('jumpUrl','__ROOT__/');
		$this->success('您已經成功退出');
    }
    
	//注冊
    public function register(){

    	$authInfo = $this->get('authInfo');
    	if(!empty($authInfo)){
    		$this->error("您已經登錄");
    		return;
    	}
    	
    	$userDao = D('User');
		$submit = $_REQUEST['submit'];
		$mail=$_POST['email'];
		$nickname=$_POST['nick_name'];
		$getmail=$userDao->where('email="'.$mail.'"')->find();
		$getnickname=$userDao->where('nick_name="'.$nickname.'"')->find();

    	if ($submit == 'step2'){
			$user_id = $_REQUEST['uid'];
			if(!$user_id){
				$mainLocationDao = D('LocationMain');
				$mainLocationList = $mainLocationDao->select();
				$this->assign('mainLocationList', $mainLocationList);
				if ($_SESSION['verify'] != md5($_POST['verify'])){
					$this->error('驗證碼不正確');
				}
	
				$RestrictedNames = M('UserRestrictedNames')->where( "'".strtolower($_POST['nick_name'])."' LIKE CONCAT('%',keyword,'%')" )->select();
				if ( $RestrictedNames )
					{ $this->error('昵稱不能用保留字, 如'.$RestrictedNames[0]['keyword']); }
				if(empty($getmail) && empty($getnickname)){
					$userInfo = $userDao->create();
				}else{
					if(!empty($getmail)){
						$userInfo = $getmail;
					}else{
						$userInfo = $getnickname;
					}
				}
			}else{
				$userInfo = $userDao->where('id='.$user_id)->find();	
			}

    		if ($userInfo){
    			if (!empty($userInfo['birthday'])){
	    			$userInfo['birthday'] = strtotime($userInfo['birthday']);
    			}else{
    				$userInfo['birthday'] = 0;
    			}
    			if (empty($userInfo['income'])){
    				$userInfo['income'] = 0;
    			}
				if ($newid = $userDao->add($userInfo)){											
					$inputDao = D('Input');
					$where = array();
					$where['caption'] = '教育程度';
					$this->assign('educationList', $inputDao->where($where)->select());
					$where['caption'] = '家庭每月收入';
					$this->assign('incomeList', $inputDao->where($where)->select());
					$where['caption'] = '現職';
					$this->assign('professionalList', $inputDao->where($where)->select());
					$where['caption'] = '大學';
					$this->assign('schoolList', $inputDao->where($where)->select());
					//$this->sendValidateEmail($newid);						
					$this->assign('newid', $newid);
					$this->display('reg-step2');
				}elseif($newid=$userInfo['id']){
					$inputDao = D('Input');
					$where = array();
					$where['caption'] = '教育程度';
					$this->assign('educationList', $inputDao->where($where)->select());
					$where['caption'] = '家庭每月收入';
					$this->assign('incomeList', $inputDao->where($where)->select());
					$where['caption'] = '現職';
					$this->assign('professionalList', $inputDao->where($where)->select());
					$where['caption'] = '大學';
					$this->assign('schoolList', $inputDao->where($where)->select());
					$this->assign('newid', $newid);
					$this->display('reg-step2');
				}else{	    			
					$this->error($userDao->getError());
				}
    		}else{
    			$this->error('系統錯誤，請聯繫管理員。');
    		}
    		
    	}elseif($submit == 'done'){
    		$newid = $_REQUEST['newid'];
			$uid = $_REQUEST['uid'];
    		
    		$data = array();
			if($uid){
				$data['id'] = $uid;
			}else{
				$data['id'] = $newid;
			}
			Import('ORG.Util.Input');
			if(isset($_POST['sex'])){
	    		$data['sex'] = $_POST['sex'];
			}
			if(isset($_POST['birthday'])){
	    		$data['birthday'] = strtotime($_POST['birthday']);
			}
			if(isset($_POST['income'])){
	    		$data['income'] = $_POST['income'];
			}
			if(isset($_POST['education'])){
	    		$data['education'] = $_POST['education'];
			}
			if(isset($_POST['school'])){
	    		$data['school'] = $_POST['school'];
			}
			if(isset($_POST['professional'])){
	    		$data['professional'] = $_POST['professional'];
			}
			if(isset($_POST['telephone'])){
	    		$data['telephone'] = $_POST['telephone'];
			}
			if(isset($_POST['address'])){
	    		$data['address'] = $_POST['address'];
			}
			if(isset($_POST['likelist'])){
	    		$data['likelist'] = $_POST['likelist'];
			}
			if(isset($_POST['desc'])){
	    		$data['desc'] = Input::getVar($_POST['desc']);
			}
			if(isset($_POST['address_sub_location_ids'])){
	    		$data['address_sub_location_ids'] = Input::getVar($_POST['address_sub_location_ids']);
			}
			if(isset($_POST['outstreet_sub_location_ids'])){
	    		$data['outstreet_sub_location_ids'] = Input::getVar($_POST['outstreet_sub_location_ids']);
			}
			if(isset($_POST['cate_ids'])){
	    		$data['cate_ids'] = Input::getVar($_POST['cate_ids']);
			}
			if(isset($_POST['event_ids'])){
	    		$data['event_ids'] = Input::getVar($_POST['event_ids']);
			}
    		$data['update_time'] = time();			
    		$userDao->save($data);
			if($uid){
				$this->assign('jumpUrl', '__ROOT__/user/login');
				$this->success('注冊成功，請登錄並更改您的帳號密碼！');
			}else{
				$this->assign('jumpUrl', '__ROOT__/user/login');
				$this->success('注冊成功，您可立即登錄OutStreet！');
			}
    	}else{
			$this->display();    		
    	}
    }
    
	//我的控制面板
    public function mypanel(){
    	$authInfo = SimpleAcl::getLoginInfo();
    	$this->redirect('user/view/id/' . $authInfo['id']);
    }

    //編輯個人信息
    public function edit(){
    	$userDao = D('User');
    	$this->assign('box',1);
    	if (isset($_POST['submit'])){
    		
    		Import('@.ORG.SimpleAcl');
    		$userInfo = SimpleAcl::getLoginInfo();

    		$data = array();
    		$data['id'] = $userInfo['id'];

    		if (!empty($_FILES)){
				import('ORG.Net.UploadFile');
				$upload = new UploadFile();
				$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));
				$upload->savePath = C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH') ;
				$upload->saveRule = 'time';
		     	$upload->thumb = true;
		      	$upload->thumbRemoveOrigin = true;
		     	$upload->thumbPrefix = 'face';
		     	$upload->thumbMaxWidth = 120;
		      	$upload->thumbMaxHeight = 120;
		      	$upload->uploadReplace = true;
		      	if($upload->upload()) {
		      		$upinfo = $upload->getUploadFileInfo();
		      		$upfilepath = $upinfo[0]['savename'];
		      		$data['face_path'] = $upfilepath;
					unlink(C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH') . $userInfo['face_path']);
					unlink(C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH') . 'face' . $userInfo['face_path']);
		      	}
    		}
    		
			Import('ORG.Util.Input');

			if(isset($_POST['sex'])){
	    		$data['sex'] = $_POST['sex'];
			}
			if(isset($_POST['nick_name'])){
	    		$data['nick_name'] = $_POST['nick_name'];
			}
			if(isset($_POST['birthday'])){
	    		$data['birthday'] = strtotime($_POST['birthday']);
			}
			if(isset($_POST['income'])){
	    		$data['income'] = $_POST['income'];
			}
			if(isset($_POST['education'])){
	    		$data['education'] = $_POST['education'];
			}
			if(isset($_POST['school'])){
	    		$data['school'] = $_POST['school'];
			}
			if(isset($_POST['professional'])){
	    		$data['professional'] = $_POST['professional'];
			}
			if(isset($_POST['telephone'])){
	    		$data['telephone'] = $_POST['telephone'];
			}
			if(isset($_POST['address'])){
	    		$data['address'] = $_POST['address'];
			}
			if(isset($_POST['likelist'])){
	    		$data['likelist'] = $_POST['likelist'];
			}
			if(isset($_POST['desc'])){
	    		$data['desc'] = Input::getVar($_POST['desc']);
			}
    		$data['update_time'] = time();

    		if ($userDao->save($data)){
				$userInfo = $userDao->find($userInfo['id']);

				$authInfo = array();
				$authInfo['id'] = $userInfo['id'];
				$authInfo['nick_name'] = $userInfo['nick_name'];
				$authInfo['email'] = $userInfo['email'];
				$authInfo['sex'] = $userInfo['sex'];
				$authInfo['face_path'] = $userInfo['face_path'];
	
				SimpleAcl::saveLogin($authInfo);    		

				$this->assign('jumpUrl', '__URL__/mypanel');
	    		$this->success('更新資料成功');
    		}else{
    			$this->error($userDao->getDbError());
    		}
    		
    	}else{
    		$userDao = D('User');
    		$authInfo = SimpleAcl::getLoginInfo();
    		$this->assign('userInfo', $userDao->find($authInfo['id']));

			$inputDao = D('Input');
			$where = array();
			$where['caption'] = '教育程度';
			$this->assign('educationList', $inputDao->where($where)->select());
			$where['caption'] = '家庭每月收入';
			$this->assign('incomeList', $inputDao->where($where)->select());
			$where['caption'] = '現職';
			$this->assign('professionalList', $inputDao->where($where)->select());
			$where['caption'] = '大學';
			$this->assign('schoolList', $inputDao->where($where)->select());

			$this->display();
    	}
    }

	//編輯店主信息
    public function editOwner(){
    	$userDao = D('User');
    	$this->assign('box',1);
    	if (isset($_POST['submit'])){
    		Import('@.ORG.SimpleAcl');
    		$userInfo = SimpleAcl::getLoginInfo();
    		$data = array();
    		$data['id'] = $userInfo['id'];
			if(isset($_POST['contact_person'])){
	    		$data['o_contact_person'] = $_POST['contact_person'];
			}
			if(isset($_POST['telephone'])){
	    		$data['telephone'] = $_POST['telephone'];
			}
			if(isset($_POST['position'])){
	    		$data['o_position'] = $_POST['position'];
			}
			if(isset($_POST['company_name'])){
	    		$data['o_company_name'] = $_POST['company_name'];
			}
    		$data['update_time'] = time();
    		if ($userDao->save($data)){

				$userInfo = $userDao->find($userInfo['id']);				
				$authInfo = array();
				$authInfo['id'] = $userInfo['id'];
				if($userInfo['user_type']==1){
					$authInfo['nick_name'] = '店主'.$userInfo['o_contact_person'];
				}else{
					$authInfo['nick_name'] = $userInfo['nick_name'];						
				}
				$authInfo['email'] = $userInfo['email'];
				$authInfo['sex'] = $userInfo['sex'];
				$authInfo['face_path'] = $userInfo['face_path'];
				$authInfo['levels'] = $userInfo['levels'];
				$authInfo['user_type'] = $userInfo['user_type'];

				SimpleAcl::saveLogin($authInfo);

				$this->assign('jumpUrl', '__URL__/mypanel');
	    		$this->success('更新資料成功');
    		}else{
    			$this->error($userDao->getDbError());
    		}

    	}else{
    		$userDao = D('User');
    		$authInfo = SimpleAcl::getLoginInfo();
    		$this->assign('userInfo', $userDao->find($authInfo['id']));
			$this->display();
    	}
    }
    
    //修改郵箱地址
    public function changeEmail(){
    	$this->assign('box',1);
    	if (isset($_POST['submit'])){
    		
    		$email = $_POST['email'];
    		
    		$userDao = D('User');
			import ( '@.ORG.SimpleAcl' );
			$userInfo = SimpleAcl::getLoginInfo();
			
			$data = array();
			$data['email'] = $email;
			$data['is_email_verify'] = '0';
			
			$userDao->where('id='.$userInfo['id'])->save($data);
    		
    		$_SESSION['userid'] = $userInfo['id'];
    		$this->redirect('resendValidateEmail');
    	}else{
	    	$this->display();
    	}
    }
    
    //修改密碼
    public function changePsd(){
    	$this->assign('box',1);
  		$userDao = D('User');
    	
    	if (isset($_POST['submit'])){
    		Import('@.ORG.SimpleAcl');
    		$userInfo = SimpleAcl::getLoginInfo();

			$where = array();
			$where['id'] = $userInfo['id'];
			$where['password'] = md5($_POST['originalPsd']);

			if ($userDao->where($where)->find()){
	    		$data = array();
	    		$data['id'] = $userInfo['id'];
	    		$data['password'] = md5($_POST['password']);
	
	    		$userDao->save($data);
					
				SimpleAcl::saveLogin($userDao->find($userInfo['id']));
				$this->assign('jumpUrl', '__ROOT__/user/mypanel');
	    		$this->success('修改密碼成功');
			}else{			
	    		$this->error('原始密碼錯誤');					
			}
    	}else{
			$this->display();
    	}
    }
    

    //忘記密碼
    public function forgetPsd(){

    	$email = $_POST['email'];
    	$verify = $_POST['verify'];
    	
    	if (isset($_POST['submit'])){
    		if (md5($verify) != $_SESSION['verify']){
    			$this->error('驗證碼錯誤');
    		}else{
    			$userDao = D('User');
    			$where = array();
    			$where['email'] = $email;
    			
    			if ($userInfo = $userDao->where($where)->find()){
					$caption = '重設你OutStreet帳戶密碼';
    				$this->assign('nickname', $userInfo['nick_name']);
					$this->assign('caption', '重設你OutStreet帳戶密碼');
					$this->assign('userstamp', $this->_userStampEncode($userInfo['id'], $userInfo['create_time']));
					$this->assign('domain', $_SERVER['SERVER_NAME']);
    				$body = $this->fetch('email_forgetPsd');
    				
					if($userInfo['email'] != '') {
						$this->smtp_mail_log($userInfo['email'],$caption,$body,'',2,3);
					}

					$this->assign('jumpUrl', '__ROOT__');
    				$this->success('已經將密碼發送到您的郵箱，請注意查收!');
    			}else{
    				$this->error('該郵箱未注冊');
    			}
    		}
    	}else{
    		$this->display();
    	}
    	
    }
    
    //重設密碼 唯一來源:用戶EMIAL
    public function doFindPsd(){
	  	$userstamp = $_GET['userstamp'];
	  	
	  	if (isset($userstamp)){
	  		$where = $this->_userStampDecode($userstamp);
	  		if ($where){
		  		$userDao = D('User');
		  		$userInfo = $userDao->where($where)->find();
		  		if ($userInfo){
		  			if (isset($_POST['submit'])){
		  				$userDao->where($where)->setField('password', md5($_POST['password']));
						$this->assign('jumpUrl', '__URL__/login');
		  				$this->success('您的密碼已成功修改，請登錄');
		  			}else{
		  				$this->assign('userstamp', $userstamp);
			  			$this->assign('email',$userInfo['email']);	  				
		  				$this->display();
		  			}
		  		}else{
			  		$this->error('請勿嘗試非法外部提交。');
		  		}
	  		}else{
		  		$this->error('系統錯誤，請聯繫管理員。');
  		}
	  	}else{
	  		$this->error('認證碼格式不正確，請聯繫管理員。');
	  	}
    }
        
    
    //進行用戶身份認證  
    //唯一來路: 用戶EMAIL
	public function doValidateEmail(){
	  	$userstamp = $_GET['userstamp'];
	  	
	  	if (isset($userstamp)){
	  		$where = $this->_userStampDecode($userstamp);
	  		if ($where){
		  		$userDao = D('User');
		  		$user = $userDao->where($where)->find();
		  		$userDao->where($where)->setField('is_email_verify', 1);

				$data = array();
				$data['user_id'] = $user['id'];
				$data['type'] = 1;
				$data['score'] = 30;
				$data['isValid'] = 1;
				$data['create_time'] = time();
				M('UserScoreLog')->add($data);

		  		$this->assign('jumpUrl', '__URL__/mypanel');
		  		$this->success('恭喜您，您的Email已經通過認證');
	  		}else{
		  		$this->error('系統錯誤，請聯繫管理員。');
  			}
	  	}else{
	  		$this->error('認證碼格式不正確，請聯繫管理員。');
	  	}
	}
	  
	//修改驗證郵箱  
	//唯一來路：Login頁面(記錄userid到session) 然後重定向到該頁
	public function changeValidateEmail(){
		$userid = $_SESSION['userid'];
	  
	  	if (isset($userid)){
			$userDao = D('User');
		  
			if (isset($_POST['submit'])){
			 	$email = $_POST['email'];
			  	$userDao->where("id=$userid")->setField('email',$email);
			  	$this->redirect('resendValidateEmail');
		  	}else{
				$userInfo = $userDao->find($userid);
			  	$this->assign('email', $userInfo['email']);
			  	$this->display();
		  	}
		  
	  	}else{
		  	$this->assign('jumpUrl', '__ROOT__');
		  	$this->error('系統異常');
	  	}
	}
	  
	public function resendEmail(){
	  	$email = $_POST['email'];
    	$verify = $_POST['verify'];
    	
    	if (isset($_POST['submit'])){

    		if (md5($verify) != $_SESSION['verify']){
    			$this->error('驗證碼錯誤');
    		}else{
    			$userDao = D('User');
    			$where = array();
    			$where['email'] = $email;
    			
    			if ($userInfo = $userDao->where($where)->find()){
    				if($userInfo['is_email_verify']){
    					$this->error('該郵箱已注冊');
    				}
    				else{
    					$this->sendValidateEmail($userInfo['id']);
    					$this->assign('waitSecond',5);
					  	$this->assign('jumpUrl','login');
					  	$this->success('已經重新發送到您的電郵中，請儘快確認！');
    				}
    			}else{
    				$this->error('該郵箱未注冊');
    			}
    		}
    		
    	}else{
    		$this->display();
    	}
	}
	  
	//重新發送驗證碼  
	//來路：1. Login頁面(記錄userid到session) -> changeValidateEmail頁面:重新發送驗證函
	//來路：2. changeEmail頁面(記錄userid到session)
	public function resendValidateEmail(){
	  
	  $userid = $_SESSION['userid'];
	  
	  if (isset($userid)){
		  $this->sendValidateEmail($userid);
		  unset($_SESSION);
		  session_destroy();
		  
		  $this->assign('waitSecond',5);
		  $this->assign('jumpUrl','login');
		  $this->success('已經重新發送到您的電郵中，請儘快確認！');
	  }else{
		  $this->error('請勿嘗試從外部非法提交！');
	  }
	}
	  
    //驗證碼
	public function verify(){
		$type	 =	 isset($_GET['type'])?$_GET['type']:'gif';
		import("ORG.Util.Image");
		Image::buildImageVerify(4,1,$type);
	}
	
	public function uploadPic(){
		C('SHOW_PAGE_TRACE',false);
		$this->display();
	}
	
	public function uploadResult(){
		C('SHOW_PAGE_TRACE',false);

		if (isset($_REQUEST['btnupload'])){

			$eleid = $_REQUEST['eleid'];
			$userid = $_REQUEST['userid'];
			$description = $_REQUEST['description'];

			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));

			$upload->savePath = C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH') ;

			$upload->saveRule = 'time';
			$upload->thumb = true;
			$upload->thumbMaxWidth = 120;
			$upload->thumbMaxHeight = 120;
			$upload->uploadReplace = true;
	 		$upload->thumbPrefix = 'face';
	   
			if($upload->upload()) {
				$upinfo = $upload->getUploadFileInfo();
				$upfilepath = $upinfo[0]['savename'];

				$dao = D('User');
				$dao->where('id='.$userid)->setField('face_path', $upfilepath);

				$this->assign('eleid', $eleid);
				$this->assign('upfilepath', $upfilepath);
				$this->assign('description', $description);
				$this->assign('picid', $picid);
				$this->display('uploadResult');
			}else{
				$upfileerr = $upload->getErrorMsg();
				$upfilepath = '';
				echo '<script text="text/javascript">';
				echo 'parent.setPic("'.$eleid.'", "'.$upfilepath.'", "'.$upfileerr.'");';
				echo 'alert("'.$upfileerr.'");';
				echo '</script>';
			}
		}else{
			echo 'none';
		}
	}

	public function deletePic(){
		$authInfo = $this->get('authInfo'); 
		unlink(C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH') . 'face' . $authInfo['face_path']);
		$this->display();
	}

	public function revalid(){
		$userid = $_REQUEST['userid'];
		if(is_numeric($userid)){
			$this->sendValidateEmail($userid);
			echo 'ok';
		}
	}

}

