<?php
class AjaxAction extends BaseAction{

	//要求$_GET傳遞MainCateId
	//輸出指定MainCateId下的所有子分類
	public function downloadpdf(){
		$id=$_REQUEST['id'];
		$shoppdfDao=D('ShopPdf');
		$pdf=$shoppdfDao->where('id='.$id)->find();
		header('Content-type: application/pdf;');
		$u_agent = $_SERVER['HTTP_USER_AGENT'];
		$filename=$pdf['file_path'];
		if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)){
			header('Content-Disposition: attachment; filename="'.$filename.'"');
   		}else{
			header('Content-Disposition: attachment; filename="'.$pdf['ori_file_name'].'"');
		}
		readfile(C('OUTSTREET_DIR').C('SHOP_PDF_PATH').$filename);
	}

	public function uploadshoppdf(){
		$type=$_REQUEST['type'];
		$shoppdfDao=D('ShopPdf');
		$userDao=D('User');
		$description = $_REQUEST['description'];
		$shop_id = $_REQUEST['shopid'];
		if($type=='edit'){
			$row_id=$_REQUEST['row_id'];
			if($_FILES['file']['type']!=''){
				if(strtolower($_FILES['file']['type'])!='application/pdf'){
					$return['success'] = 0;
					$return['msg']=2;
				}else{
					if (is_uploaded_file($_FILES['file']['tmp_name'])){
						$data['update_time']=time();
						$data['is_audit']='0';
						$data['ori_file_name']=$_FILES['file']['name'];
						$data['description']=$_REQUEST['description'];
						$shoppdfDao->where('id='.$row_id)->save($data);
						move_uploaded_file($_FILES['file']['tmp_name'], C('OUTSTREET_ROOT').'/'.C('SHOP_PDF_PATH').'/doc_'.$shop_id.'_'.$row_id.'.pdf');
						$return['success'] = 1;
						$return['msg']=1;
					}else{
						$return['success'] = 0;
						$return['msg']=0;
					}
				}
			}else{
				$data['update_time']=time();
				$data['is_audit']='0';
				$data['description']=$_REQUEST['description'];
				$shoppdfDao->where('id='.$row_id)->save($data);
				$return['success'] = 1;
				$return['msg']=1;
			}
		}else{
			if(strtolower($_FILES['file']['type'])!='application/pdf'){
				$return['success'] = 0;
				$return['msg']=2;
			}else{
				if (is_uploaded_file($_FILES['file']['tmp_name'])) {
					$data['create_time']=time();
					$data['shop_id']=$shop_id;
					$data['ori_file_name']=$_FILES['file']['name'];
					$data['description']=$_REQUEST['description'];
					$recordid = $shoppdfDao->add($data);
					move_uploaded_file($_FILES['file']['tmp_name'], C('OUTSTREET_ROOT').'/'.C('SHOP_PDF_PATH').'/doc_'.$shop_id.'_'.$recordid.'.pdf');
					$return['success'] = 1;
					$return['msg']=1;
				}else{
					$return['success'] = 0;
					$return['msg']=0;
				}
			}
		}
		echo json_encode($return);
	}

	public function shopcatedetail(){
		$shopCateDao = D('ShopCate');
		$cate_id = $_GET['cate_id'];
		$cate_detail=$shopCateDao->find($cate_id);
		if($cate_detail['level']==1){
			$result[0]=$cate_id;
			$result[2]=1;
			$this->ajaxReturn($result);	
		}
		if($cate_detail['level']==3){
			$lv2id=$shopCateDao->where('id='.$cate_id)->getField('parent_id');			
		}else{
			$lv2id=$cate_id;
		}
		$lv1id=$shopCateDao->where('id='.$lv2id)->getField('parent_id');
		$result[0]=$lv1id;
		$result[1]=$lv2id;
		$this->ajaxReturn($result);
	}

	public function shopCateSub(){
		$cate_id = $_GET['cate_id'];

		$shopCateDao = D('ShopCate');

		$where = array();
		$where['parent_id'] = $cate_id;
		$shopCateList = $shopCateDao->where($where)->select();
		 
		$this->ajaxReturn($shopCateList);
	}

	public function eventCateSub(){
		$mainid = $_GET['mainid'];

		$eventCateDao = D('EventCate');
		 
		$where = array();
		$where['parent_id'] = $mainid;
		$eventCateSubList = $eventCateDao->where($where)->select();
		 
		$this->ajaxReturn($eventCateSubList);
	}

	//要求$_GET傳遞MainLocationId
	//輸出指定MainLocationId下的所有地區
	public function locationSub(){
		$mainLocationId = $_GET['mainlocationid'];

		$locationSubDao = D('LocationSub');
		 
		$where = array();
		$where['main_location_id'] = $mainLocationId;
		$SubLocationList = $locationSubDao->where($where)->select();
		 
		$this->ajaxReturn($SubLocationList);
	}
	
	public function existEmail(){
		$email = $_GET['email'];
		$userDao = D('User');
		 
		if ($userDao->where("email='$email'")->count()){
			echo 'false';
		}else{
			echo 'true';
		}
	}

	public function existNickName(){
		$nickname = $_GET['nick_name'];
		$userDao = D('User');
		
		$names = new Model();
		
		$num = $names->query("SELECT COUNT(*) FROM user_restricted_names WHERE keyword LIKE '$nickname'");
		 
		if ($num['0']['COUNT(*)'] > 0 || $userDao->where("nick_name='$nickname'")->count()){
			echo 'false';
		}else{
			echo 'true';
		}
	}

	public function user_login(){
    	$userDao = D('User');
		import ( '@.ORG.SimpleAcl' );
		Import('Think.Util.Cookie');
		$where = array();
		$where['email'] = $_POST['email'];
		$where['password'] = md5($_POST['password']);
		if ($userInfo = SimpleAcl::authenticate($where)){
			if($userInfo['user_type']!=1){//Disallow owner
				if ($userInfo['is_email_verify'] == 1){	
					$authInfo = array();
					$authInfo['id'] = $userInfo['id'];
					$authInfo['nick_name'] = $userInfo['nick_name'];						
					$authInfo['email'] = $userInfo['email'];
					$authInfo['sex'] = $userInfo['sex'];
					$authInfo['face_path'] = $userInfo['face_path'];
					$authInfo['levels'] = $userInfo['levels'];
					$authInfo['user_type'] = $userInfo['user_type'];
					SimpleAcl::saveLogin($authInfo);
					$userDao->where('id='.$userInfo['id'])->setField('last_login',time());
					$this->ajaxReturn(null,0);
				}
			}else{
					$this->ajaxReturn(null,2);
			}
		}
		$this->ajaxReturn(null,1);
	}

	public function joinEvent2Favorite(){
		$eventid = $_GET['eventid'];
		import ( '@.ORG.SimpleAcl' );
		$authInfo = SimpleAcl::getLoginInfo();

		if ($authInfo){
			$userEventFavoriteDao = D('UserEventFavorite');
			 
			$where = array();
			$where['event_id'] = $eventid;
			$where['user_id'] = $authInfo['id'];
			if ($userEventFavoriteDao->where($where)->limit(1)->select()){

				unset($where['user_id']);
				$ct = $userEventFavoriteDao->where($where)->count();
				$this->ajaxReturn($ct, '您已收藏過該活動');
			}else{

				$eventDao = D('Event');
				$eventInfo = $eventDao->find($eventid);

				$data = array();
				$data['user_id'] = $authInfo['id'];
				$data['user_name'] = $authInfo['nick_name'];
				$data['event_id'] = $eventInfo['id'];
				$data['event_name'] = $eventInfo['name'];
				$data['create_time'] = time();
				if($userEventFavoriteDao->add($data)){
					
					unset($where['user_id']);
					$ct = $userEventFavoriteDao->where($where)->count();
					
					$this->ajaxReturn($ct, '已成功加入您的活動收藏夾');

					generateUserStatistics($authInfo['id']);
					
				}else{
					$this->ajaxReturn(null, '系統異常,請聯繫管理員');
				}
			}
		}else{
			$this->ajaxReturn(null, '您尚未登錄或登錄已經過期，請刷新頁面重新登錄后再收藏該活動!');			
		}
	}

	public function joinPromotion2Favorite(){
		$promotionid = $_GET['promotionid'];
		import ( '@.ORG.SimpleAcl' );
		$authInfo = SimpleAcl::getLoginInfo();
		if ($authInfo){
			$userPromotionFavoriteDao = D('UserPromotionFavorite');
			$where = array();
			$where['offer_id'] = $promotionid;
			$where['user_id'] = $authInfo['id'];
			if ($userPromotionFavoriteDao->where($where)->limit(1)->select()){
				$this->ajaxReturn(0, '您已收藏過該優惠');
			}else{
				$promotionDao = D('Offer');
				$promotionInfo = $promotionDao->find($promotionid);
				$data = array();
				$data['user_id'] = $authInfo['id'];
				$data['user_name'] = $authInfo['nick_name'];
				$data['offer_id'] = $promotionInfo['id'];
				$data['offer_name'] = $promotionInfo['name'];
				$data['create_time'] = time();				
				if($userPromotionFavoriteDao->add($data)){
					generateUserStatistics($authInfo['id']);					
					$this->ajaxReturn(1, '已成功加入您的優惠收藏夾');
				}else{
					$this->ajaxReturn(0, '系統異常,請聯繫管理員');
				}
			}
		}else{
			$this->ajaxReturn(null, '您尚未登錄或登錄已經過期，請刷新頁面重新登錄后再收藏該優惠!');			
		}
	}

	public function joinShop2Favorite(){
		$shopid = $_GET['shopid'];
		import ( '@.ORG.SimpleAcl' );
		$authInfo = SimpleAcl::getLoginInfo();

		if ($authInfo){
			$userShopFavoriteDao = D('UserShopFavorite');
			 
			$where = array();
			$where['shop_id'] = $shopid;
			$where['user_id'] = $authInfo['id'];
			if ($userShopFavoriteDao->where($where)->limit(1)->select()){
				$this->ajaxReturn(null, '您已收藏過該商戶');
			}else{

				$shopDao = D('Shop');
				$shopInfo = $shopDao->find($shopid);

				$data = array();
				$data['user_id'] = $authInfo['id'];
				$data['user_name'] = $authInfo['nick_name'];
				$data['shop_id'] = $shopInfo['id'];
				$data['shop_name'] = $shopInfo['name'];
				$data['create_time'] = time();
				if($userShopFavoriteDao->add($data)){
					generateUserStatistics($authInfo['id']);
					$this->ajaxReturn(null, '已成功加入您的商戶收藏夾');
				}else{
					$this->ajaxReturn(null, '系統異常,請聯繫管理員');
				}
			}
		}else{
			$this->ajaxReturn(null, '您尚未登錄或登錄已經過期，請刷新頁面重新登錄后再收藏該商戶!');			
		}
	}
	
	public function joinUser2Favorite(){
		$userid = $_GET['userid'];

		import ( '@.ORG.SimpleAcl' );
		$authInfo = SimpleAcl::getLoginInfo();

		if ($authInfo){
			$userUserFavoriteDao = D('UserUserFavorite');
			 
			$where = array();
			$where['user_id'] = $authInfo['id'];
			$where['favorite_user_id'] = $userid;
			if ($userUserFavoriteDao->where($where)->limit(1)->select()){
				$this->ajaxReturn(null, '您已收藏過該评家');
			}else{
				$userDao = D('User');
				$userInfo = $userDao->find($userid);

				$data = array();
				$data['favorite_user_id'] = $userInfo['id'];
				$data['favorite_user_name'] = $userInfo['nick_name'];
				$data['user_id'] = $authInfo['id'];
				$data['user_name'] = $authInfo['nick_name'];
				$data['create_time'] = time();
				if($userUserFavoriteDao->add($data)){
					generateUserStatistics($authInfo['id']);
					generateUserStatistics($userid);
					$this->ajaxReturn(null, '已成功加入您的心水評家');
				}else{
					$this->ajaxReturn(null, '系統異常,請聯繫管理員');
				}
			}
		}else{
			$this->ajaxReturn(null, '您尚未登錄或登錄已經過期，請刷新頁面重新登錄后添加心水評家!');			
		}
	}

	public function favoriteList(){
		$userid = $_GET['userid'];
		$shopid = $_GET['shopid'];

		$where = array();
		if (is_numeric($userid)){
			$where['user_id'] = $userid;
		}
		if(is_numeric($shopid)){
			$where['shop_id'] = $shopid;
		}
		$userShopFavorite = D('UserShopFavorite');

		$count = $userShopFavorite->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		$favoriteList = $userShopFavorite->where($where)->limit($limit)->order('create_time desc')->select();
		if ($favoriteList){
			$this->ajaxReturn($favoriteList, $multipage, 1);
		}else{
			$this->ajaxReturn(null, '沒有收藏的商戶', 0);
		}
	}

	public function reviewList(){
		$userid = $_GET['userid'];
		$shopid = $_GET['shopid'];

		$where = array();
		if (is_numeric($userid)){
			$where['user_id'] = $userid;
		}
		if(is_numeric($shopid)){
			$where['shop_id'] = $shopid;
		}
		$reviewDao = D('Review');

		$count = $reviewDao->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		$reviewList = $reviewDao->where($where)->limit($limit)->order('create_time desc')->select();
		if ($reviewList){
			import('ORG.Util.Input');
			foreach($reviewList as &$review){
				$review['description'] = Input::deleteHtmlTags($review['description'], 0, 30);
				$review['description'] = msubstr($review['description'], 0, 30);
			}
			$this->ajaxReturn($reviewList, $multipage, 1);
		}else{
			$this->ajaxReturn(null, '沒有發表過評介', 0);
		}
	}

	public function shopList(){
		$userid = $_GET['userid'];
		$ownerid = $_GET['ownerid'];

		$where = array();
		if (is_numeric($userid)){
			$where['user_id'] = $userid;
			$where['owner_id'] = array('neq', $userid);
		}
		if(is_numeric($ownerid)){
			$where['owner_id'] = $ownerid;
		}
		$shopDao = D('Shop');

		$count = $shopDao->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		$shopList = $shopDao->where($where)->limit($limit)->order('create_time desc')->select();
		if ($shopList){
			$this->ajaxReturn($shopList, $multipage, 1);
		}else{
			$this->ajaxReturn(null, '沒有商戶', 0);
		}
	}

	public function messageList(){
		$userid = $_GET['userid'];
		$submituserid = $_GET['submituserid'];

		$where = array();
		$where['is_audit'] = 1;
		if (is_numeric($userid)){
			$where['user_id'] = $userid;
		}
		if(is_numeric($submituserid)){
			$where['submit_user_id'] = $submituserid;
		}
		$messageDao = D('UserMessage');

		$count = $messageDao->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		$messageList = $messageDao->where($where)->limit($limit)->order('create_time desc')->select();
		 
		if ($messageList){
			foreach($messageList as &$message){
				$message['createtime'] = date('Y-m-d H:i:s',$message['create_time']);
			}

			$this->ajaxReturn($messageList, $multipage, 1);
		}else{
			$this->ajaxReturn(null, '暫時沒有留言', 0);
		}
	}

	public function getShopsByMap(){
		$southWest = $_GET['southWest'];
		$northEast = $_GET['northEast'];
		$cate_id = $_GET['cate_id'];
		$keyword = $_GET['keyword'];
		$southWestArr = explode(',',$southWest);
		 
		$southWestLat = $southWestArr[0];
		$southWestLng = $southWestArr[1];

		$northEastArr = explode(',',$northEast);
		$northEastLat = $northEastArr[0];
		$northEastLng = $northEastArr[1];

		$where = array('is_audit'=>1);
		$where['googlemap_lat'] = array('between',array($southWestLat,$northEastLat));
		$where['googlemap_lng'] = array('between',array($southWestLng,$northEastLng));

		if ($cate_id) {
			$cate_ids = array();
			$cate_ids[] = $cate_id;
			$ids = $cate_ids;
			$havingChild = true;

			while ($havingChild) {
				$shopcate = M('ShopCate')->where(array('parent_id'=>array('in',$ids)))->select();
				if ( count($shopcate) ) {
					$ids = array();
					foreach ( $shopcate AS $val ) { $ids[] = $val['id']; }
					$cate_ids = array_merge($cate_ids, $ids);
				}
				else { $havingChild = false; }
			}

			if (count($cate_ids)) { $where['cate_id'] = array('in',$cate_ids); }
		}

		if (!empty($keyword)){
			$likewhere = array();
			$likewhere['keyword'] = array('like', "%$keyword%");
			$likewhere['name'] = array('like', "%$keyword%");
			$likewhere['address'] = array('like', "%$keyword%");
			$likewhere['_logic'] = 'or';
			$where['_complex'] = $likewhere;
		}
		 
		$shopDao = D('Shop');
		$mainLocationDao = D('LocationMain');
		$subLocationDao = D('LocationSub');	
		
		$count = $shopDao->join( 'shop_cate_map on shop_fields.id = shop_cate_map.shop_id' )->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		 
		$shopPageList = $shopDao->join( 'shop_cate_map on shop_fields.id = shop_cate_map.shop_id' )->limit(10)->where($where)->select();
		$shopList = $shopDao->join( 'shop_cate_map on shop_fields.id = shop_cate_map.shop_id' )->limit(100)->where($where)->select();
		
		foreach($shopList as $key=>$shop){
			$mainLoc = $mainLocationDao->find($shop['main_location_id']);
			$subLoc = $subLocationDao->find($shop['sub_location_id']);
			if(empty($mainLoc)) $mainLoc ='';
			if(empty($subLoc)) $subLoc ='';			
			$shopList[$key]['main_location_name']=$mainLoc['name'];
			$shopList[$key]['sub_location_name']=$subLoc['name'];		
		}

		$data = array();
		$data['shopList'] = $shopList;
		$data['shopPageList'] = $shopPageList;
		$data['multipage'] = $multipage;
		
		$this->ajaxReturn($data,'',count($shopList));
	}

	public function getEventsByMap(){
		$southWest = $_GET['southWest'];
		$northEast = $_GET['northEast'];
		$main_event_cate_id = $_GET['main_event_cate_id'];
		$sub_event_cate_id = $_GET['sub_event_cate_id'];
		$keyword = $_GET['keyword'];
		 
		$southWestArr = explode(',',$southWest);
		 
		$southWestLat = $southWestArr[0];
		$southWestLng = $southWestArr[1];
		 
		$northEastArr = explode(',',$northEast);
		$northEastLat = $northEastArr[0];
		$northEastLng = $northEastArr[1];
		 
		$where = array();
		$where['googlemap_lat'] = array('between',array($southWestLat,$northEastLat));
		$where['googlemap_lng'] = array('between',array($southWestLng,$northEastLng));
		if (!empty($main_event_cate_id)){
			$cateids = M('EventCate')->where(array('parent_id'=>$main_event_cate_id))->getField('id,parent_id');
			$where['cate_id'] = array('in',array_keys($cateids));
		}
		if (!empty($sub_event_cate_id)){
			$where['cate_id'] = $sub_event_cate_id;
		}

		if (!empty($keyword)){
			$likewhere = array();
			$likewhere['keyword'] = array('like', "%$keyword%");
			$likewhere['name'] = array('like', "%$keyword%");
			$likewhere['address'] = array('like', "%$keyword%");
			$likewhere['_logic'] = 'or';
			$where['_complex'] = $likewhere;
		}
		 
		 
		$eventDao = D('Event');
		$mainLocationDao = D('LocationMain');
		$subLocationDao = D('LocationSub');	
		 
		$eventList = $eventDao->where($where)->limit(20)->select();

		foreach($eventList as $key=>$event){
			$mainLoc = $mainLocationDao->find($event['main_location_id']);
			$subLoc = $subLocationDao->find($event['sub_location_id']);
			if(empty($mainLoc)) $mainLoc ='';
			if(empty($subLoc)) $subLoc ='';			
			$eventList[$key]['main_location_name']=$mainLoc['name'];
			$eventList[$key]['sub_location_name']=$subLoc['name'];		
		}

		$this->ajaxReturn($eventList,'',count($eventList));
		 
	}

	public function getShopByKeyword(){
		$eventAdvShop = array();
		$eventAdvShop = C('EVENTADVSHOP');
		$key = $_GET['key'];
		$mainLocationId = $_GET['main_location_id'];
		$subLocationId = $_GET['sub_location_id'];

		$shopDao = D('Shop');
		$where = array();
		
		if (!empty($key)){
			$where['name'] = array('like', '%'.$key.'%');
		}
		if (!empty($mainLocationId)){
			$where['main_location_id'] = $mainLocationId;
		}
		if (!empty($subLocationId)){
			$where['sub_location_id'] = $subLocationId;
		}
		if(isset($eventAdvShop)){
			$where['cate_id'] = array('in',$eventAdvShop)	;
		}
		$where['is_audit']="1";
		$shoplistAdv = $shopDao->join('shop_cate_map on shop_cate_map.shop_id = shop_fields.id')->where($where)->limit(20)->order('shop_fields.id asc')->select();
		
		$remain = 20 - sizeof($shoplistAdv) ;
		if($remain > 0){
			$where2 = array();
			if (!empty($key)){
				$where2['name'] = array('like', "%$key%");
			}
			if (!empty($mainLocationId)){
				$where2['main_location_id'] = $mainLocationId;
			}
			if (!empty($subLocationId)){
				$where2['sub_location_id'] = $subLocationId;
			}
			if(isset($eventAdvShop)){
				$where2['cate_id'] = array('not in',$eventAdvShop);
			}
			$where2['is_audit']="1";
			$shoplistNonAdv = $shopDao->join('shop_cate_map on shop_cate_map.shop_id = shop_fields.id')->where($where2)->limit($remain)->order('shop_fields.id asc')->select();
			if(sizeof($shoplistNonAdv)>0){
				$shoplist = array_merge($shoplistAdv,$shoplistNonAdv);
			}
			else{
				$shoplist = $shoplistAdv;
			}
		}else{
			$shoplist = $shoplistAdv;
		}

		$mainLocationDao = D('LocationMain');
		$subLocationDao = D('LocationSub');	
		foreach($shoplist as $key=>$shop){
			$mainLoc = $mainLocationDao->find($shop['main_location_id']);
			$subLoc = $subLocationDao->find($shop['sub_location_id']);
			$shoplist[$key]['main_location']['name']=$mainLoc['name'];
			$shoplist[$key]['sub_location']['name']=$subLoc['name'];
		}

		$this->ajaxReturn($shoplist, '', 1);
	}

	public function getMall(){
		$key=$_GET['key'];
		$mall_id = $_GET['mall_id'];
		$mainLocationId = $_GET['main_location_id'];
		$subLocationId = $_GET['sub_location_id'];
		$shopDao=D('Shop');
		$mainLocationDao = D('LocationMain');
		$subLocationDao = D('LocationSub');	
		$shopCateDao = D('ShopCate');
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');
		$cate_id = "175";
		
		if (!empty($key)){
			$where['name'] = array('like', '%'.$key.'%');
		}
		if (!empty($mainLocationId)){
			$where['main_location_id'] = $mainLocationId;
		}
		if (!empty($subLocationId)){
			$where['sub_location_id'] = $subLocationId;
		}
		if (!empty($mall_id)){
			$where['shop_fields.id'] = $mall_id;
		}

		$mallList = array();
		if (count($where))
		{
			$where['cate_id'] = $cate_id;
			$where['is_audit']="1";
			$mallList = $shopDao->join('shop_cate_map on shop_cate_map.shop_id = shop_fields.id')->where($where)->limit(20)->order('shop_fields.id asc')->select();		
			foreach($mallList as $key=>$mall){
				$mainLoc = $mainLocationDao->find($mall['main_location_id']);
				$subLoc = $subLocationDao->find($mall['sub_location_id']);
				$mallList[$key]['main_location']['name']=$mainLoc['name'];
				$mallList[$key]['sub_location']['name']=$subLoc['name'];
			}
		}
		$this->ajaxReturn($mallList, 'get', 1);
	}
	
	public function getEventAddressByKeyword(){
		$key = $_GET['key'];

		$eventDao = D('Event');
		$where = array();
		if (!empty($key)){
			$where['address'] = array('like', '%'.$key.'%');
		}

		$eventlist = $eventDao->relation(true)->where($where)->limit(10)->select();
		$this->ajaxReturn($eventlist, '', 1);
	}

	public function getShopCateSubs(){
		$shopid = $_GET['shopid'];

		$shopCateMapDao = D('ShopCateMap');
		$where = array();
		$where['a.shop_id'] = $shopid;
		$catelist = $shopCateMapDao->field('name AS cate_name, a.cate_id')->table('shop_cate_map a')->join('shop_cate b on a.cate_id = b.id')->where($where)->select();
		$tempArr =array();
		foreach($catelist AS $cate){
			$cateId = $cate['cate_id'];
			$where = array();
			$where['reference_id'] = $cateId;
			$list = D('ShopCate')->where($where)->select();
			if(sizeof($list)>0){
				foreach($list as $elem){
					array_push($tempArr,array('cate_name'=>$elem['name'], 'cate_id'=>$elem['id']));		
				}
			}
		}
		if(sizeof($tempArr)>0){
			$catelist = array_merge($catelist,$tempArr);
		}
		$this->ajaxReturn($catelist, '', 1);
	}

	public function getReviewCommendCount(){
		$reviewId = $_GET['reviewId'];
		
		$reviewCommendDao = D('ReviewCommend');
		$where = array();
		$where['review_id'] = $reviewId;
		
		$this->ajaxReturn(null,'',$reviewCommendDao->where($where)->count());
	}
	
	public function appendReviewCommend(){
		import ( '@.ORG.SimpleAcl' );
		$authInfo = SimpleAcl::getLoginInfo();

		if ($authInfo){
			$reviewId = $_GET['reviewId'];

			$reviewCommendDao = D('ReviewCommend');
			$where = array();
			$where['review_id'] = $reviewId;
			$where['user_id'] = $authInfo['id'];
			
			if ($reviewCommendDao->where($where)->count() == 0){
				$data = array();
				$data['review_id'] = $reviewId;
				$data['user_id'] = $authInfo['id'];
				$data['create_time'] = time();	
				
				$reviewCommendDao->add($data);

				$where = array();
				$where['review_id'] = $reviewId;

				$where = array();
				$where['id'] = $reviewId;
				$review = D('Review')->where($where)->find();
				$where = array();
				$where['id'] = $review['shop_id'];
				$shop = M('ShopFields')->where($where)->find();
				$where = array();
				$where['shop_id'] = $review['shop_id'];
				$shopcate = M('ShopCateMap')->where($where)->select();
				$i = 0;
				foreach ( $shopcate AS $val )
				{
					$data = array();
					$data['user_id'] = $review['user_id'];
					$data['shop_id'] = $review['shop_id'];
					$data['shop_review_id'] = $review['id'];
					$data['cate_id'] = $val['cate_id'];
					$data['location_id'] = $shop['sub_location_id'];
					$data['type'] = 3;
					$data['score'] = 10;
					$data['isValid'] = !$i ? 1 : 0;
					$data['create_time'] = time();
					M('UserScoreLog')->add($data);
					$i++;
				}
				$where = array();
				$where['review_id'] = $reviewId;
				$this->ajaxReturn(null,'稱贊成功',$reviewCommendDao->where($where)->count());
			}else{
				$this->ajaxReturn(null,'您已經稱贊過該評介',0);				
			}
		}else{
			$this->ajaxReturn(null,'您尚未登錄',0);			
		}
	}

	public function getReviewReportCount(){
		$reviewId = $_GET['reviewId'];
		
		$reviewReportDao = D('ReviewReport');
		$where = array();
		$where['review_id'] = $reviewId;
		
		$this->ajaxReturn(null,'',$reviewReportDao->where($where)->count());
	}
	
	public function appendReviewReport(){
		import ( '@.ORG.SimpleAcl' );
		$authInfo = SimpleAcl::getLoginInfo();

		if ($authInfo){
			$reviewId = $_GET['reviewId'];
			
			$reviewReportDao = D('ReviewReport');
			$where = array();
			$where['review_id'] = $reviewId;
			$where['user_id'] = $authInfo['id'];
			
			if ($reviewReportDao->where($where)->count()){
				$this->ajaxReturn(null,'您已經檢舉過該評介',0);
			}else{
				$data = array();
				$data['review_id'] = $reviewId;
				$data['user_id'] = $authInfo['id'];
				$data['create_time'] = time();	
				
				$reviewReportDao->add($data);

				$where = array();
				$where['review_id'] = $reviewId;
				
				$this->ajaxReturn(null,'檢舉成功',$reviewReportDao->where($where)->count());
			}
		}else{
			$this->ajaxReturn(null,'您尚未登錄',0);			
		}
	}

	public function getEventReviewCommendCount(){
		$reviewId = $_GET['reviewId'];
		
		$reviewCommendDao = D('EventReviewCommend');
		$where = array();
		$where['review_id'] = $reviewId;
		
		$this->ajaxReturn(null,'',$reviewCommendDao->where($where)->count());
	}
	
	public function appendEventReviewCommend(){
		import ( '@.ORG.SimpleAcl' );
		$authInfo = SimpleAcl::getLoginInfo();

		if ($authInfo){
			$reviewId = $_GET['reviewId'];
			
			$reviewCommendDao = D('EventReviewCommend');
			$where = array();
			$where['review_id'] = $reviewId;
			$where['user_id'] = $authInfo['id'];
			
			if ($reviewCommendDao->where($where)->count() == 0){
				$data = array();
				$data['review_id'] = $reviewId;
				$data['user_id'] = $authInfo['id'];
				$data['create_time'] = time();	
				
				$reviewCommendDao->add($data);

				$where = array();
				$where['review_id'] = $reviewId;

				$where = array();
				$where['id'] = $reviewId;
				$review = D('EventReview')->where($where)->find();
				$where = array();
				$where['id'] = $review['event_id'];
				$event = M('EventFields')->where($where)->find();
				$data = array();
				$data['user_id'] = $review['user_id'];
				$data['event_id'] = $review['event_id'];
				$data['event_review_id'] = $review['id'];
				$data['cate_id'] = $event['sub_event_cate_id'];
				$data['location_id'] = $event['sub_location_id'];
				$data['type'] = 3;
				$data['score'] = 10;
				$data['isValid'] = 1;
				$data['create_time'] = time();
				M('UserScoreLog')->add($data);
				
				$this->ajaxReturn(null,'稱贊成功',$reviewCommendDao->where($where)->count());
			}else{
				$this->ajaxReturn(null,'您已經稱贊過該評介',0);				
			}
		}else{
			$this->ajaxReturn(null,'您尚未登錄',0);			
		}
	}

	public function getEventReviewReportCount(){
		$reviewId = $_GET['reviewId'];
		
		$reviewReportDao = D('EventReviewReport');
		$where = array();
		$where['review_id'] = $reviewId;
		
		$this->ajaxReturn(null,'',$reviewReportDao->where($where)->count());
	}
	
	public function appendEventReviewReport(){
		import ( '@.ORG.SimpleAcl' );
		$authInfo = SimpleAcl::getLoginInfo();

		if ($authInfo){
			$reviewId = $_GET['reviewId'];

			$reviewReportDao = D('EventReviewReport');
			$where = array();
			$where['review_id'] = $reviewId;
			$where['report_user'] = $authInfo['nick_name'];
			
			if ($reviewReportDao->where($where)->count() == 0){
				$data = array();
				$data['create_time'] = time();
				$data['review_id'] = $reviewId;
				$data['report_user'] = $authInfo['nick_name'];
				$reviewReportDao->add($data);

				$where = array();
				$where['review_id'] = $reviewId;
				$this->ajaxReturn(null,'檢舉成功',$reviewReportDao->where($where)->count());
			}else{
				$this->ajaxReturn(null,'您已經檢舉過該評介',0);				
			}
		}else{
			$this->ajaxReturn(null,'您尚未登錄',0);			
		}
		
	}

	public function submitReply(){
		$authInfo = $this->get('authInfo');
		if (!empty($authInfo)){
			
			$msgId = $_GET['id'];
			
			$userMessageDao = D('UserMessage');
			$where = array();
			$where['user_id'] = $authInfo['id'];
			$where['id'] = $msgId;
			
			$msgInfo = $userMessageDao->where($where)->find();
			if(!empty($msgInfo)){
				$userMessageDao->reply = $_GET['reply'];
				$userMessageDao->reply_time = time();
				$userMessageDao->save();
				$this->ajaxReturn($msgInfo,'回復成功', 1);				
			}else{
				$this->ajaxReturn(null,'沒有權限', 0);
			}
			
		}else{
			$this->ajaxReturn(null,'尚未登入', 0);
		}
		
	}

	public function submitComment(){
		$authInfo = $this->get('authInfo');
		if (!empty($authInfo)){
			$msgId = $_GET['id'];
			$userMessageDao = D('UserMessage');
			$data = array();
			$data['create_time'] = time();
			$data['user_id'] = $_POST['user_id'];
			$data['body'] = $_POST['body'];
			$data['submit_user_id'] = $authInfo['id'];
			$data['submit_user_name'] = $authInfo['nick_name'];
			$data['is_audit'] = 1;
			$newid = $userMessageDao->add($data);
			if($newid){
				generateUserStatistics($authInfo['id']);
				$this->ajaxReturn($data,'留言成功', 1);
			}else{
				$this->ajaxReturn(null,$userMessageDao->getDbError(), 0);
			}
		}else{
			$this->ajaxReturn(null,'尚未登入', 0);
		}
  	}

  	public function deleteComment(){
		$authInfo = $this->get('authInfo');
		if (!empty($authInfo)){
			$msgId = $_GET['id'];
			$userMessageDao = D('UserMessage');
			if($userMessageDao->delete($msgId)){
				generateUserStatistics($authInfo['id']);
				$this->ajaxReturn($data,'刪除成功', 1);
			}else{
				$this->ajaxReturn(null,$userMessageDao->getDbError(), 0);
			}
		}else{
			$this->ajaxReturn(null,'尚未登入', 0);
		}
 	}

	public function getSuggestCate() {
		$searchterms = $_REQUEST['q'];
		$searchterms = trim($searchterms);

		if ( !empty( $searchterms ) ) {
			preg_match_all( '/([^\s]+)/', $searchterms, $m );
			$searchterms = $m[1];
			$where = array();
			foreach ( $searchterms AS $term ) {
				$where[ 'name' ][] = array( 'like', '%' . $term . '%' );
			}
			$where[ 'name' ][] = 'or';
			$shopcate = M('ShopCate')->where($where)->limit(10)->select();
			foreach( $shopcate AS $cate ) {
				echo '想找 "' . $cate['name'] . '" ?|' . $cate['id'] . "\n";
			}
		}
	}

	function addFacebookMark() {
		$item_type = $_REQUEST['item_type'];
		$item_id = $_REQUEST['item_id'];
		$review_id = $_REQUEST['review_id'];

		import ( '@.ORG.SimpleAcl' );
		$authInfo = SimpleAcl::getLoginInfo();

		if ($authInfo) {
			if ( $item_type == 'event' ) { 
				$event = M('EventFields')->where(array('id'=>$item_id))->find();
				$data = array();
				$data['user_id'] = $authInfo['id'];
				$data['event_id'] = $event['id'];
				if ( $review_id ) { $data['event_review_id'] = $review_id; }
				$data['cate_id'] = $event['cate_id'];
				$data['location_id'] = $event['sub_location_id'];
				$data['type'] = 6;
				$data['score'] = 10;
				$data['isValid'] = 1;
				$data['create_time'] = time();
				M('UserScoreLog')->add($data);
			} else if ( $item_type == 'shop' ) { 
				$shop = M('ShopFields')->where(array('id'=>$item_id))->find();
				$shopcate = M('ShopCateMap')->where(array('shop_id'=>$item_id))->select();
				$i = 0;
				foreach ( $shopcate AS $val ) {
					$data = array();
					$data['user_id'] = $authInfo['id'];
					$data['shop_id'] = $shop['id'];
					if ( $review_id ) { $data['shop_review_id'] = $review_id; }
					$data['cate_id'] = $val['cate_id'];
					$data['location_id'] = $shop['sub_location_id'];
					$data['type'] = 6;
					$data['score'] = 10;
					$data['isValid'] = !$i ? 1 : 0;
					$data['create_time'] = time();
					M('UserScoreLog')->add($data);
					$i++;
				}
			}
		}
	}

	public function deleteFavouriteStore(){
		$authInfo = $this->get('authInfo');
		if (!empty($authInfo)){
			$Id = $_GET['id'];
			$userShopFavoriteDao = D('UserShopFavorite');
			if($userShopFavoriteDao->delete($Id)){
			$this->ajaxReturn(null,'刪除成功', 1);
			}else{
				$this->ajaxReturn(null,$userShopFavoriteDao->getDbError(), 0);
			}
		}else{
			$this->ajaxReturn(null,'尚未登入', 0);
		}
	}

	public function deleteFavouriteUser(){
		$authInfo = $this->get('authInfo');
		if (!empty($authInfo)){
			$Id = $_GET['id'];
			$userShopFavoriteDao = D('UserUserFavorite');
			if($userShopFavoriteDao->delete($Id)){
			$this->ajaxReturn(null,'刪除成功', 1);
			}else{
				$this->ajaxReturn(null,$userShopFavoriteDao->getDbError(), 0);
			}
		}else{
			$this->ajaxReturn(null,'尚未登入', 0);
		}
  	}

}

