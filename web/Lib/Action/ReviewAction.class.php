<?php
class ReviewAction extends BaseAction{

	public function write_review(){
		$this->assign('picstamp', time() . rand(1000,9999));		
		$this->display();
	}
	public function index(){

		$search_shop = $_REQUEST['search_shop'];
		$search_event = $_REQUEST['search_event'];
		$search_promo = $_REQUEST['search_promo'];
		
		$search_user = $_REQUEST['search_user'];
		$favorite_user = $_REQUEST['favorite_user'];
		$keyword = $_REQUEST['keyword'];
		$keyword = trim($keyword);

		$start_date = $_REQUEST['start_date'];
		$end_date = $_REQUEST['end_date'];
		
		if(!empty($start_date)){
			$start_date = strtotime($start_date);
		}
		if(!empty($end_date)){
			$end_date = strtotime($end_date);
		}
		
		
		$where = 'where 1=1 and is_audit = 1';
		if(!empty($search_user)){
			$userDao = D('User');
			$userInfo = $userDao->where("nick_name like'%$search_user%'")->find();
			if(!empty($userInfo)){
				$where .= ' and user_id='.$userInfo['id'];
			}else{
				$where .= ' and 2>3';
			}
		}else{
			if(!empty($favorite_user)){
				$authInfo = $this->get('authInfo');
				if(!empty($authInfo)){
					$userUserFavorite = D('UserUserFavorite');
					$favoriteUserList = $userUserFavorite->where('favorite_user_id=' . $authInfo['id'])->select();
					if(!empty($favoriteUserList)){
						$ids = '0';
						foreach($favoriteUserList as $user){
							$ids .= ',' . $user['user_id'];
						}
						$where .= " and user_id in ($ids)";
					}
				}
				
			}
		}
		
		if(!empty($start_date)){
			$where .= ' and create_time >= '. $start_date;
		}
		if(!empty($end_date)){	
			$where .= ' and create_time <= '. ($end_date + 24*60*60);
		}
		
		
		if(!empty($keyword)){
			$where .= " and (description like '%$keyword%' OR title like '%$keyword%')";
		}

		if (!empty($search_shop)){ 
			
			$shopSql = "select id, create_time, title, description, user_id, is_audit, 'shop' as for_type, shop_id as for_id from review_fields where shop_id<>0";
			
			if(!empty($search_event)){
				$eventSql = " union select id, create_time, title, description, user_id, is_audit, 'event' as for_type, event_id as for_id from review_fields where event_id<>0";
			}

			if(!empty($search_promo)){
				$promoSql = " union select id, create_time, title, description, user_id, is_audit, 'promotion' as for_type, promotion_id as for_id from review_fields where promotion_id<>0";
			}

		}else{

			if(!empty($search_event)){
				$eventSql = "select id, create_time, title, description, user_id, is_audit, 'event' as for_type, event_id as for_id from review_fields where event_id<>0";

				if(!empty($search_promo)){
					$promoSql = " union select id, create_time, title, description, user_id, is_audit, 'promotion' as for_type, promotion_id as for_id from review_fields where promotion_id<>0";
				}

			}else{
				if(!empty($search_promo)){
					$promoSql = "select id, create_time, title, description, user_id, is_audit, 'promotion' as for_type, promotion_id as for_id from review_fields where promotion_id<>0";
				}
			}
		}

		$countSql = "select count(id) as count from ($shopSql $eventSql $promoSql) as temp $where";
		$model = new Model();
		$ret = $model->query($countSql);
		
		$count = $ret[0]['count'];
		import('ORG.Util.Page');
		$p=new Page($count);
		$multiPage=$p->show();
		$limit=$p->firstRow.','.$p->listRows;

		$selectSql = "select * from ($shopSql $eventSql $promoSql) as temp $where order by create_time desc limit $limit";
		$reviewList = $model->query($selectSql);
		
		$userDao = D('User');
		$shopDao = D('Shop');
		$eventDao = D('Event');
		$promoDao = D('Promotion');
		$reviewRate = D('ReviewRate');
		
		foreach($reviewList as &$review){
			$review['user'] = $userDao->find($review['user_id']);
			switch($review['for_type']){
				case 'shop':
					$review['shop'] = $shopDao->relation(true)->find($review['for_id']);
					$review['rate'] = $reviewRate->where('review_id = '.$review['id'])->find();
				break;
				case 'event':
					$review['event'] = $eventDao->relation(true)->find($review['for_id']);
				break;
				case 'promotion':
					$review['promotion'] = $promoDao->find($review['for_id']);
				break;
			}
		}

		$this->assign("multiPage",$multiPage);
		$this->assign("reviewList",$reviewList);
		$this->display();
	}
}

