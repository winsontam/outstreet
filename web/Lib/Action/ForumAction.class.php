<?php

class ForumAction extends BaseAction{
	
	public function index(){
		
		import("ORG.Util.Page");
		
		$boardid = $_GET['boardid'];
		
		if ($_GET['boardid'] == ""){
			$_GET['boardid']=3;
			$boardid = 3;
		}

		$forumBoardDao = D('ForumBoard');
		$where = array();
		$where['id_audit'] = 1;
		$forumBoardList = $forumBoardDao->where($where)->select();
		$this->assign('forumBoardList', $forumBoardList);

		$where = array();
		$where['id'] = $boardid;
		$curBoard = $forumBoardDao->where($where)->find();
		$this->assign('curBoard', $curBoard);

		if(!empty($curBoard)){

			$forumThreadDao = D('ForumThread');

			$where = array();
			$where['is_top'] = 2;
			$userDao=D('User');
			
			$topForumThreadList = $forumThreadDao->where($where)->select();
			$topForumThreadListCount = count($topForumThreadList);
			$wheretopForumThreadList=array();
			
			for ($i=0;$i<$topForumThreadListCount;$i++){
				$wheretopForumThreadList['id']=$topForumThreadList[$i]['lastpost_user'];
				$topForumThreadList[$i]['lastpost_username']=$userDao->where($wheretopForumThreadList)->getfield('nick_name');
			}
			$this->assign('topForumThreadList', $topForumThreadList);

			$where = array();
			$where['board_id'] = $boardid;
			$where['is_top'] = array('neq', 2);
				
			//設置分頁代碼
			$count = $forumThreadDao->where($where)->count('id');
			import('ORG.Util.Page');
			$p = new Page($count);
			$multipage = $p->show();
			$limit = $p->firstRow.','.$p->listRows;
			$where['is_show'] = 1;

			$forumThreadList = $forumThreadDao->where($where)->limit($limit)->order('is_top desc,lastpost_time desc')->select();
			$countforumThreadList=count($forumThreadList);
			for ($i=0;$i<$countforumThreadList;$i++){
				$whereforumThreadList['id']=$forumThreadList[$i]['lastpost_user'];
				$forumThreadList[$i]['lastpost_username']=$userDao->where($whereforumThreadList)->getfield('nick_name');
			}
			$this->assign('multipage', $multipage);
			$this->assign('forumThreadList', $forumThreadList);
		}
		
		$this->display();
	}

	public function add(){

		$boardid = $_REQUEST['boardid'];

		$authInfo = SimpleAcl::getLoginInfo();

		$forumBoardDao = D('ForumBoard');

		$where = array();
		$where['id'] = $boardid;
		$curBoard = $forumBoardDao->where($where)->find();

		if($_POST['submit']){
			$forumThreadDao = D('ForumThread');
			$newThread = $forumThreadDao->create();
			if($newThread){
				$newThread['board_id'] = $boardid;
				$newThread['board_name'] = $curBoard['name'];
				$newThread['user_id'] = $authInfo['id'];
				$newThread['user_name'] = $authInfo['nick_name'];
				$newThread['lastpost_time'] = $newThread['create_time'];
				$newThread['lastpost_user'] = $authInfo['id'];
				$newThread['body'] = ubb2html($newThread['body']);
				$newThread['summary'] = ubb2html($newThread['summary']);

				if($newid = $forumThreadDao->add($newThread)){
						
					$curThread = $forumThreadDao->find($newid);
					if($curThread['board_id'] == '4'){

						$maxOrdersInfo = $forumThreadDao->order('orders desc')->find();
						$orders = $maxOrdersInfo['orders'] + 1;
						$forumThreadDao->where('id='.$curThread['id'])->setField('orders',$orders);
					}
						
					$picstamp = $_REQUEST['picstamp'];
						
					//$where = array();					
					$where['picstamp'] = $picstamp;						
					$dao = D('ForumThreadPicture');
					$dao->where($where)->setField('thread_id', $newid);
					
					$where1 = array();
					$where1['id'] = $boardid;					
					$forumBoardDao->where($where1)->setInc('thread_count');
					$forumBoardDao->where($where1)->setInc('post_count');					
					//$forumBoardDao->where($where)->setField('last_title', $_POST['summary']);
					$forumBoardDao->where($where1)->setField('last_title', $newThread['summary']);
					$forumBoardDao->where($where1)->setField('last_user', $authInfo['id']);
					$forumBoardDao->where($where1)->setField('last_time', time());
					$forumBoardDao->where($where1)->setField('last_thread_id', $curThread['id']);
					//$forumBoardDao->where($where)->setField('last_thread_id', $newid);

					$this->assign('jumpUrl', "__URL__/view/threadid/$newid/p/1");
						
					$options = $_POST['options'];
						
					$options = explode("\n", $options);
						
					$forumOptionDao = D('ForumOption');
					foreach($options as $opt){
						$data = array();
						$data['thread_id'] = $newid;
						$data['name'] = $opt;

						$forumOptionDao->add($data);
					}
						
					$this->success('新話題創建成功!');
				}else{
					$this->error($forumThreadDao->getDbError());
				}
			}else{
				$this->error($forumThreadDao->getError());
			}
		}

		$this->assign('curBoard', $curBoard);
		$this->assign('picstamp', time() . rand(1,1000));
		$this->display();
	}

	public function view(){
		$threadId = $_REQUEST['threadid'];

		$forumBoardDao = D('ForumBoard');
		$forumThreadDao = D('ForumThread');
		$forumPostDao = D('ForumPost');
		$forumOptionDao = D('ForumOption');
		$userDao = D('User');

		if(!empty($_POST['options'])){
			$where = array();
			$where['id'] = array('in',$_POST['options']);
			$forumOptionDao->where($where)->setInc('vote_count');
		}
		 
		//得到當前論壇版塊帖子的所有信息
		$curThread = $forumThreadDao->find($threadId);

		$where = array();
		$where['id'] = $threadId;

		//將查看后的帖子的單看次數加一
		$forumThreadDao->where($where)->setInc('view_count');

		//設置當前的版塊的帖子的用戶屬性
		$curThread['user'] = $userDao->find($curThread['user_id']);
		$curThread['options'] = $forumOptionDao->where('thread_id='.$threadId)->select();
		$total_vote = 0;
		foreach($curThread['options'] as $opt){
			$total_vote += $opt['vote_count'];
		}
		$this->assign('total_vote', $total_vote);

		if(!empty($_POST['submit'])){
			//創建相應版塊的帖子的模型對象  用於想數據庫添加數據
			$newForum = $forumPostDao->create();
				
			$newForum['thread_id'] = $curThread['id'];
				
			$userInfo = SimpleAcl::getLoginInfo();
				
			$newForum['user_id'] = $userInfo['id'];
			$newForum['user_name'] = $userInfo['nick_name'];
			$newForum['body'] = ubb2html($newForum['body']);
			
			if ($newForum){
				//如果帖子回複成功的話   多帖子表進行操
				if($forumPostDao->add($newForum)){
					//為帖子列表創建實例化對象
					$where = array();
					$where['id'] = $threadId;
						
					$forumThreadDao->where($where)->setInc('post_count');
					$forumThreadDao->where($where)->setField('lastpost_time', time());
					$forumThreadDao->where($where)->setField('lastpost_user', $userInfo['id']);

					$where = array();
					$where['id'] = $curThread['board_id'];
					$forumBoardDao->where($where)->setInc('post_count');
					$forumBoardDao->where($where)->setField('last_user', $newForum['user_name']);
					$forumBoardDao->where($where)->setField('last_title', $$curThread['summary']);
					$forumBoardDao->where($where)->setField('last_time', time());
					$this->success('回覆話題成功');
				}else{
					$this->error($forumPostDao->getDbError());
				}
			}else{
				$this->error($forumPostDao->getError());
			}
		}

		$this->assign('curThread', $curThread);

		$where = array();
		$where['thread_id'] = $threadId;

		//設置分頁代碼
		$count = $forumPostDao->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$forumPostList = $forumPostDao->limit($limit)->where($where)->select();

		foreach($forumPostList as &$post){
			$post['user'] = $userDao->find($post['user_id']);
		}	

		$this->assign('multipage', $multipage);
		$this->assign('forumPostList', $forumPostList);
		$this->display();
	}

	public function editT(){

		$this->assign('box', 1);
		$id = $_REQUEST['id'];
		$ftDao = D('ForumThread');
		if (!empty($_POST['submit'])){
			$data = array();
			$data['id'] = $id;
			$data['summary'] = $_POST['summary'];
			Import('ORG.Util.Input');
			
			$data['body'] = ubb2html($_POST['body']);
				
			$ftDao->save($data);
			$this->assign('jumpUrl', '__URL__/view/p/1/threadid/' . $id);
			$this->success('編輯成功');
		}
		$ftInfo = $ftDao->find($id);
		$this->assign('threadInfo', $ftInfo);
		$this->display();

	}

	public function editP(){
		$this->assign('box', 1);
		$id = $_REQUEST['id'];
		$fpDao = D('ForumPost');
		$fpInfo = $fpDao->find($id);
		if (!empty($_POST['submit'])){
				
			$data = array();
			$data['id'] = $id;

			Import('ORG.Util.Input');
			$data['body'] = ubb2html($_POST['body']);
				
			$fpDao->save($data);
			$this->assign('jumpUrl', '__URL__/view/p/1/threadid/' . $fpInfo['thread_id']);
			$this->success('編輯成功');
				
		}
		$this->assign('postInfo', $fpInfo);
		$this->display();
	}

	public function deleteP(){
		$id = $_REQUEST['id'];
		$fpDao = D('ForumPost');
		$fpInfo = $fpDao->find($id);
		$fpDao->where('id=' . $id)->delete();

		$where = array();
		$where['id'] = $fpInfo['thread_id'];

		$forumThreadDao = D('ForumThread');
		$forumThreadDao->where($where)->setDec('post_count');

		$ftDao = D('ForumThread');
		$ftInfo = $ftDao->find($fpInfo['thread_id']);

		$where = array();
		$where['id'] = $ftInfo['board_id'];

		$forumBoardDao = D('ForumBoard');
		$forumBoardDao->where($where)->setDec('post_count');

		$this->assign('jumpUrl', "__URL__/view/threadid/{$fpInfo['thread_id']}/p/1");
		$this->success('删除成功');
	}

	public function upfile(){
		import('ORG.Net.UploadFile');
		$upfilePath = C('OUTSTREET_ROOT').C('FORUM_UPFILE_PATH');

		$upload = new UploadFile();
		$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));
		$upload->savePath = $upfilePath;
		$upload->saveRule = 'time';
		$upload->thumb = true;
		$upload->thumbMaxWidth = 120;
		$upload->thumbMaxHeight = 120;
		$upload->uploadReplace = true;

		$xheditor_json = '{"err":"{err}","msg":"{msg}"}';

		if($upload->upload()) {
			$upinfo = $upload->getUploadFileInfo();
			$upfileerr = '';
			$upfilepath = C('OUTSTREET_ROOT').C('FORUM_UPFILE_PATH').$upinfo[0]['savename'];
			 
			$dao = D('ForumThreadPicture');
			$upfile = $dao->create();
			 
			$authInfo =  SimpleAcl::getLoginInfo();
			 
			$upfile['file_path'] = $upinfo[0]['savename'];
			$upfile['user_id'] = $authInfo['id'];
			$upfile['picstamp'] = $_REQUEST['picstamp'];
			 
			$dao->add($upfile);
		}
		else{
			$upfileerr = $upload->getErrorMsg();
			$upfilepath = '';
		}
		$xheditor_json = str_replace('{err}', $upfileerr, $xheditor_json);
		$xheditor_json = str_replace('{msg}', $upfilepath, $xheditor_json);
		echo $xheditor_json;
	}
	
	public function addComment(){

		$boardid = $_REQUEST['boardid'];
		$forumBoardDao = D('ForumBoard');

		$where = array();
		$where['id'] = $boardid;
		$curBoard = $forumBoardDao->where($where)->find();

		if($_POST['submit']){
			$forumThreadDao = D('ForumThread');
			$newThread = $forumThreadDao->create();
			if($newThread){
				$newThread['board_id'] = $boardid;
				$newThread['board_name'] = $curBoard['name'];

				$newThread['user_id'] = 0;
				$newThread['user_name'] = '意見';
				$newThread['lastpost_time'] = $newThread['create_time'];
				$newThread['body'] = ubb2html($newThread['body']);
				$newThread['is_aduit'] = 0;

				if($newid = $forumThreadDao->add($newThread)){
						
					$curThread = $forumThreadDao->find($newid);
					if($curThread['board_id'] == '4'){
						$maxOrdersInfo = $forumThreadDao->order('orders desc')->find();
						$orders = $maxOrdersInfo['orders'] + 1;
						$forumThreadDao->where('id='.$curThread['id'])->setField('orders',$orders);
					}
						
					$forumBoardDao->where($where)->setInc('thread_count');
					$forumBoardDao->where($where)->setInc('post_count');
					$forumBoardDao->where($where)->setField('last_title', $_POST['summary']);
					$forumBoardDao->where($where)->setField('last_user', 0);
					$forumBoardDao->where($where)->setField('last_time', time());
					$forumBoardDao->where($where)->setField('last_thread_id', $newid);

					$this->assign('jumpUrl', "__URL__/view/threadid/$newid/p/1");
						
					$options = $_POST['options'];
					$options = explode("\n", $options);
						
					$forumOptionDao = D('ForumOption');
					foreach($options as $opt){
						$data = array();
						$data['thread_id'] = $newid;
						$data['name'] = $opt;

						$forumOptionDao->add($data);
					}
					$this->assign('jumpUrl','__ROOT__');
					$this->success('多謝您的寶貴意見或想法，讓本網內容可不斷創新!');
				}else{
					$this->error($forumThreadDao->getDbError());
				}
			}else{
				$this->error($forumThreadDao->getError());
			}
		}

		$this->assign('curBoard', $curBoard);
		$this->assign('picstamp', time() . rand(1,1000));

		$this->display();
	}

}

