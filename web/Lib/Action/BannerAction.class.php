<?php
class BannerAction extends BaseAction
{
	public function click()
	{
		if ( $code = $_REQUEST['code'] )
		{
			M( 'BannerStats' )->execute( 'INSERT INTO banner_stats ( code, date, impression, click ) VALUES ( "' . $code . '", CURDATE(), 1, 1 ) ON DUPLICATE KEY UPDATE click = click + 1' );
		}

		if ( $url = $_REQUEST['url'] )
		{
			header( 'location:' . base64_decode( $url ) );
		}
	}

	public function impression()
	{
		if ( $code = $_REQUEST['code'] )
		{
			M( 'BannerStats' )->execute( 'INSERT INTO banner_stats ( code, date, impression, click ) VALUES ( "' . $code . '", CURDATE(), 1, 0 ) ON DUPLICATE KEY UPDATE impression = impression + 1' );
		}

		header( 'Content-type:  image/gif' );
		header( 'Expires: Wed, 11 Nov 1998 11:11:11 GMT' );
		header( 'Cache-Control: no-cache' );
		header( 'Cache-Control: must-revalidate' );
		printf( '%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c',71,73,70,56,57,97,1,0,1,0,128,255,0,192,192,192,0,0,0,33,249,4,1,0,0,0,0,44,0,0,0,0,1,0,1,0,0,2,2,68,1,0,59 );
	}
}

