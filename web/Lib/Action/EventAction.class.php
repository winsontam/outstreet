<?php

class EventAction extends BaseAction{

	public function getdatebyword($timediff){
		$firsttime = 0;
		$lasttime = 0;
		switch($timediff){
			case 'today':
				$firsttime = strtotime(date('Y-m-d 0:0:0'));
				$lasttime = strtotime(date('Y-m-d 23:59:59'));
			break;
			case 'tomorrow':
				$firsttime = strtotime(date('Y-m-d 0:0:0')) + 60 * 60 * 24;
				$lasttime = strtotime(date('Y-m-d 23:59:59')) + 60 * 60 * 24;
			break;
			case 'week':
				$firsttime = strtotime(date('Y-m-d 0:0:0')) - date('w') * 24 * 60 * 60;
				$lasttime = strtotime(date('Y-m-d 23:59:59')) + (6 - date('w')) * 24 * 60 * 60;
			break;
			case 'weekend':
				if ( date( 'N' ) < 5 ){
					$weekendstart = 5-date( 'N' );
					$firsttime = mktime(0,0,0,date('n'),date('j')+$weekendstart,date('Y'));
				}
				else { $firsttime = strtotime(date('Y-m-d 0:0:0')); }
				$weekendend = 7-date( 'N' );
				$lasttime = mktime(23,59,59,date('n'),date('j')+$weekendend,date('Y'));
			break;
			case 'month':
				$firsttime = strtotime(date('Y-m-d 0:0:0'));
				$lasttime = strtotime(date('Y-m-30 23:59:59'));
			break;
		}
		return array( $firsttime, $lasttime );
	}

	public function search(){

		// 加載數據訪問對象
		$eventDao 		= D('Event');
		$searchterms 	= trim($_REQUEST['searchterms']);
		$this->assign('event_searchterms',$searchterms);
		$timediff 		= $_REQUEST['timediff'];
		$start_date 	= strtotime($_REQUEST['start_date']);
		$end_date 		= strtotime($_REQUEST['end_date']);
		$ticket_timediff 	= $_REQUEST['ticket_timediff'];
		$ticket_start_date 	= strtotime($_REQUEST['ticket_start_date']);
		$ticket_end_date 	= strtotime($_REQUEST['ticket_end_date']);

		$cate_ids 			= $_REQUEST['event_cate_ids'];
		if (!empty($cate_ids) && !is_array($cate_ids)){$cate_ids = explode(',',$cate_ids);}
		$sub_location_ids 	= $_REQUEST['event_sub_location_ids'];
		if (!empty($sub_location_ids) && !is_array($sub_location_ids)){$sub_location_ids = explode(',',$sub_location_ids);}

		$this->assign('cate_ids',$cate_ids);
		$this->assign('event_sub_location_ids',$sub_location_ids);

		$creater_name 	= $_REQUEST['creater_name'];
		$telephone		= $_REQUEST['telephone'];

		$this->assign('creater_name',$creater_name);
		$this->assign('telephone',$telephone);

		if (!empty($sub_location_ids)){
			$locationMainDao=D('LocationMain');
			$locationSubDao = D('LocationSub');
			$where = array();
			$where['id'] = array('in', $sub_location_ids);
			$locationList = $locationSubDao->where($where)->select();

			$selected_locations = array();
			$sub_location_names = '';

			foreach($locationList as $location){
				$main_location[$location['main_location_id']]['ids'][]=$location['id'];
				$main_locationname=$locationMainDao->where('id='.$location['main_location_id'])->getField('name');
				if(!count($main_location[$location['main_location_id']]['name'])){
					$main_location[$location['main_location_id']]['name'][]=$main_locationname;
				}

				if($sub_location_names!=''){ $sub_location_names .=','; }
				$sub_location_names .= $location['name'];
				$selected_locations[$location['id']] = $location['name'];
			}

			for($i=1;$i<5;$i++){
				$limit=$locationSubDao->where('main_location_id='.$i)->count();
				if(count($main_location[$i]['ids'])!=$limit){
					unset($main_location[$i]);
				}
			}

			$this->assign('main_location',$main_location);
			$this->assign('event_sub_location_names',$sub_location_names);
			$this->assign('selected_locations',$selected_locations);
		}

		if (!empty($cate_ids)){
			$eventCateDao 	= D('EventCate');
			$where = array();
			$where['id'] = array('in', $cate_ids);
			$cateList = $eventCateDao->where($where)->select();

			$cate_names = '';
			$tempCate=array();
			$selected_cates = array();
			foreach($cateList as $cate){
				if(!in_array($cate['name'],$tempCate)){
					if($cate_names!=''){
						$cate_names .=',';
					}
					$cate_names .= $cate['name'];
					$tempCate[]=$cate['name'];
					$selected_cates[$cate['id']] = $cate['name'];
				}
			}

			$this->assign('event_cate_names',$cate_names);
			$this->assign('selected_cates',$selected_cates);
			$children = $eventCateDao->where(array('parent_id' => array('in', $cate_ids)))->select();
			$childrenIds = array();
			foreach($children AS $val) { $childrenIds[] = $val['id']; $cate_ids[] = $val['id']; }

			while( count($childrenIds) ){
				$children = $eventCateDao->where(array('parent_id' => array('in', $childrenIds)))->select();
				$childrenIds = array();
				foreach($children AS $val) { $childrenIds[] = $val['id']; $cate_ids[] = $val['id']; }
			}
		}

		$where = array();
		if (!empty($cate_ids)) $where['cate_id'] = array('in', $cate_ids);
		if (!empty($sub_location_ids)) $where['sub_location_id'] = array('in', $sub_location_ids);
		if (!empty($creater_name)) $where['creater_name'] = array('like','%'.$creater_name.'%');
		if (!empty($telephone)) $where['telephone'] = array('like','%'.$telephone.'%');
		$where['is_audit'] = 1;

		if (!empty($searchterms)){
			$terms 		= $searchterms;
    		$allterms 	= array();

			if ( preg_match_all( '/\w+/', $terms, $m ) ){
				foreach ( $m[0] AS $t ){
					$terms = str_replace($t,'',$terms);
					$allterms[$t] = 'e';
				}
			}

			$mterms = array();
    		mb_regex_encoding( 'UTF-8' );
    		mb_ereg_search_init( $terms, '\w+' );

			while ( ( $m = mb_ereg_search_regs() ) )
        		{ $allterms[ $m[0] ] = 'm'; }

			$searchcols = array(
				'CONCAT(`name`,":",`english_name`)' => 10,
				'`creater_name`' => 8
			);

			$likecolumn = '[likecols]';
			$orQuerys = array();
			$countQuerys = array();

			foreach ( $allterms AS $term => $type ) {
				if ( $type == 'e' || $type == 's' ) {
					$orQuerys[] = $likecolumn . ' REGEXP "' . $term . '"';
				} elseif ( $type == 'm' ) {
					if ( ( $len = preg_match_all( '/./u', $term, $t ) ) > 1 ) {
						$orQuerys[] = $likecolumn . ' REGEXP "(' . implode('|',$t[0]) . '){' . round($len*0.8) . ',' . ($len) . '}"';
					} else {
						$orQuerys[] = $likecolumn . ' LIKE "%' . $term . '%"';
					}
				}
				foreach ( $searchcols AS $col => $score ) {
					$countQuerys[] = '( ( ' . $col . ' REGEXP "' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "[[:<:]]' . $term . '[[:>:]]" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "^' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "' . $term . '$" ) * ' . $score . ')'
						. ' + ( IF( length(' . $col . ') = length("' . $term . '"), 1, 0 ) * ' . $score . ')';
				}
			}

			$cates=M('EventCate')->where(array('_string'=>'(' . str_replace( '[likecols]', 'name', implode( ' OR ', $orQuerys ) ) . ')'))->select();
			$searchcateids = array();
			foreach($cates AS $cate) { $searchcateids[] = $cate['id']; }
			if ( count($searchcateids) ) {
				$where[ '_string' ] = '('.str_replace( '[likecols]','CONCAT( '.implode( ',":",', array_keys($searchcols) ).' )', implode(' OR ',$orQuerys ) ).' OR cate_id IN ('.implode( ',', $searchcateids ).'))';
				$countQuerys[] = '((cate_id IN (' . implode( ',', $searchcateids ) . '))*20)';
			} else {
				$where[ '_string' ] = '('.str_replace( '[likecols]','CONCAT( '.implode( ',":",', array_keys($searchcols) ).' )', implode(' OR ',$orQuerys ) ).')';
			}

			$field = '*,(' . implode( ' + ', $countQuerys ) . ') AS scores';
			$order = 'IF(end_time+86400<'.time().',0,1) DESC, end_time ASC, scores DESC';
		} else {
    		$field = '*';
    		$order = 'IF(end_time+86400<'.time().',0,1) DESC, end_time ASC';
		}

		if( $timediff ) {
			$times = $this->getdatebyword($timediff);
			if ( $times[0] ) { $start_date = $times[0]; $where['end_time'] = array ('egt',$start_date); }
			if ( $times[1] ) { $end_date = $times[1]; $where['start_time'] = array ('elt',$end_date); }
		} elseif ($start_date || $end_date) {
			if ( is_int($start_date) ) { $where['end_time'] = array('egt',$start_date); }
			if ( is_int($end_date) ) { $where['start_time'] = array('elt',$end_date); }
		}
		$this->assign('timediff',$timediff);
		$this->assign('start_date',$start_date);
		$this->assign('end_date',$end_date);

		if( $ticket_timediff ) {
			$times = $this->getdatebyword($ticket_timediff);
			if ( $times[0] ) { $ticket_start_date = $times[0]; }
			if ( $times[1] ) { $ticket_end_date = $times[1]; }
		}
		if ( is_int($ticket_start_date) && is_int($ticket_end_date) ) {
			$where['ticket_startdate'] = array('between',array ($ticket_start_date,$ticket_end_date));
		} elseif ( is_int($ticket_start_date) ) {
    		$where['ticket_startdate'] = array('egt',$ticket_start_date);
		} elseif ( is_int($ticket_end_date) ) {
			$where['ticket_startdate'] = array('elt',$ticket_end_date);
		}

		$this->assign('ticket_timediff',$ticket_timediff);
		$this->assign('ticket_start_date',$ticket_start_date);
		$this->assign('ticket_end_date',$ticket_end_date);

		$count = $eventDao->where($where)->count();
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$eventList = $eventDao->field( $field )->where( $where )->relation( true )->limit( $limit )->order( $order )->select();

		for ($i=0;$i<count($eventList);$i++){
			if ($eventList[$i]['end_time'] < strtotime(date('Y-m-d 0:0:0'))){
				$eventList[$i]['status']= "(已結束)";
			}
		}

		if(!empty($searchterms)){
			$searchLogDao = D('SearchLog');
			$data = array();
			$data['create_time'] = time();
			$data['keywords'] = $searchterms;
			$data['results'] = $count;
			$searchLogDao->add($data);
		}

		//數據壓入模板
		$this->assign('multipage', $multipage);
		$this->assign('eventList', $eventList);

		//模板與數據整合 并輸出到HTTP流
		//$this->display('lists');

		echo $content = $this->fetch('lists');
		file_put_contents( $this->cache_filename, base64_encode( $content ) );

	}//end search

	public function index(){
		// 加載數據訪問對象
		$eventDao = D('Event');
		$statisticsEventDao = D('StatisticsEvent');
		$eventCateDao = D('EventCate');

		//獲取Event分類主目錄列表 壓入模板
		$eventCateList = $eventCateDao->where( array( 'parent_id' => 0,'type'=>array('neq','mobile') ) )->order('event_count desc')->select();
		$this->assign('eventCateList', $eventCateList);

		$where = array();
		$where['is_audit'] = 1;
		$hotEventList = $statisticsEventDao->relation(true)->limit(5)->order('review_count desc')->select();
		$this->assign('hotEventList', $hotEventList);

		/* Original new event order
		$where['start_time'] = 	array('egt', strtotime(date('Y-m-d 0:0:0')));
		$newEventList = $eventDao->relation(true)->where($where)->limit(5)->order('start_time asc, cate_id')->select();
		*/

		$newEventList = $eventDao->relation(true)->where($where)->limit(5)->order('create_time desc, cate_id')->select();
		$newEventParentCateIds = array();
		foreach ($newEventList AS $val) { $newEventParentCateIds[] = $val['cate']['parent_id']; }
		$parent = $eventCateDao->where( array( 'id' => array('in',$newEventParentCateIds) ) )->select();
		$parentCate = array();
		foreach( $parent AS $val ) { $parentCate[$val['id']] = $val;  }
		$this->assign('parentCate', $parentCate);
		$this->assign('newEventList', $newEventList);

		$reviewDao = D('EventReview');
		$reviewList = $reviewDao->where($where)->relation(true)->order('create_time desc')->limit(10)->select();
		$this->assign('reviewList', $reviewList);

		//***************************************** Start To Get Event's List ***********************************************//
		$eventDao = D('Event');
		$where=array();
		$where['is_audit'] 	 = 1;
		//$where['start_time'] = array(gt, time());
		$where['is_recommend_new'] = array(gt, 0);

		$eventList = $eventDao->where($where)->relation(true)->order('is_recommend_new asc', 'start_time asc', 'cate_id')->limit(4)->select();
		$this->assign('eventList', $eventList);
		//******************************************* End Of Get Event's List ***********************************************//

		//***************************************** Start To Get Recommended Event's List ****************************************//
		$where = array();
		$where['is_audit'] 	 = 1;
		$where['is_recommend'] = array(gt, 0);
		$recommendEventList = $eventDao->where($where)->relation(true)->order('is_recommend asc', 'id desc')->limit(3)->select();
		$this->assign('recommendEventList', $recommendEventList);
		//******************************************* End Of Get Recommended Event's List ****************************************//

		//***************************************** Start To Get Event Review List ****************************************//
		$model = new Model();
		$where = 'where event_id > 0 and is_audit = 1';
		$selectSql = "select * from review_fields as temp $where order by create_time desc limit 4";
		$reviewList = $model->query($selectSql);

		$userDao = D('User');
		$eventDao = D('Event');
		$promotionDao = D('Promotion');

		foreach($reviewList as &$review){
			$review['user'] = $userDao->find($review['user_id']);
			$review['for_type'] = 'event';
			$review['event'] = $eventDao->relation(true)->find($review['event_id']);
		}
		$this->assign('newestReviewList', $reviewList);

		//******************************************* End Of Get Event Review List ****************************************//

		//模板與數據整合 并輸出到HTTP流
		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}

	public function lists(){
		$cateId 			= $_GET['cateid'];
		$mainLocationId 	= $_GET['mainlocationid'];
		$subLocationId 		= $_GET['sublocationid'];
		// 加載數據訪問對象
		$eventDao = D('Event');
		$eventCateDao = D('EventCate');
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');

		if ($cateId)
		{
			$user_agent = $_SERVER[ 'HTTP_USER_AGENT' ];
			$referer = $_SERVER[ 'HTTP_REFERER' ];
			$ip_address = $this->getRealIpAddr();
			if ( !M( 'EventCateLog' )->where( array( 'cate_id' => $cateId, 'ip_address' => $ip_address, '_string' => 'NOW() - create_time < 3600' ) )->count() )
				{ M( 'EventCateLog' )->add( array( 'cate_id' => $cateId, 'ip_address' => $ip_address, 'user_agent' => $user_agent, 'referer' => $referer, 'create_time' => array( 'exp', 'NOW()' ) ) ); }

			$cateName = $eventCateDao->where( array( 'id' => $cateId ) )->getField('name');
			$this->assign('selected_cates', array($cateId=>$cateName));
		}

		//獲取Event分類主目錄列表 壓入模板
		if ( !$cateId ){
			$eventCateList = $eventCateDao->where( array( 'parent_id' => 0,'type'=>array('neq','mobile') ) )->order('event_count desc')->select();
		} else {
			$eventCateList = $eventCateDao->where( array( 'parent_id' => $cateId,'type'=>array('neq','mobile') ) )->order('event_count desc')->select();
		}
		$this->assign('eventCateList', $eventCateList);
		//==================================

		//獲取商圈主目錄列表 壓入模板
		$locationMainList = $locationMainDao->order('event_count desc')->select();
		$this->assign('locationMainList', $locationMainList);

		//獲取商圈子目錄列表 壓入模板
		if (isset($mainLocationId)){
			$where = array();
			$where['main_location_id'] = $mainLocationId;
			$locationSubList = $locationSubDao->where($where)->order('event_count desc')->select();
			$this->assign('locationSubList', $locationSubList);
		}

		//獲取當前SubLocation信息 壓入模板
		if (isset($subLocationId)){
			$where = array();
			$where['id'] = $subLocationId;
			$curSubLocation = $locationSubDao->where($where)->find();
			$this->assign('curSubLocation', $curSubLocation);
		}

		//指定SubLocationId時 mainLocationId根據SubLocation決定
		if ($curSubLocation){
			$mainLocationId = $curSubLocation['main_location_id'];
		}

		//獲取當前MainLocation信息 壓入模板
		if (isset($mainLocationId)){
			$where = array();
			$where['id'] = $mainLocationId;
			$curMainLocation = $locationMainDao->where($where)->find();
			$this->assign('curMainLocation', $curMainLocation);
		}
		//===================================

		//接收查詢變量
		Import('ORG.Util.Input');

		$cateIds = array();
		if ($cateId){
			$cate = $eventCateDao->where( array( 'id' => $cateId, 'parent_id' => $cateId, '_logic' => 'or' ) )->select();
			foreach( $cate AS $val ) {
				$cateIds[] = $val['id'];
				if ($val['reference_id']) { $cateIds[] = $val['reference_id']; }
			}
		}

		//設置Event的查詢條件
		$eventList1=array();
		$eventList2=array();
		$eventList3=array();
		$eventList4=array();
		$where1 = array();
		$where1['start_time'] = array('egt', strtotime(date('Y-m-d 0:0:0')));
		$where1['end_time'] = array('egt', strtotime(date('Y-m-d 0:0:0')));
		if (count($cateIds)) $where1['cate_id'] = array('in', $cateIds);
		if (!empty($mainLocationId)) $where1['main_location_id'] = $mainLocationId;
		if (!empty($subLocationId)) $where1['sub_location_id'] = $subLocationId;
		$where1['is_audit'] = 1;
		$where1['_logic'] = 'and';
		$eventList1 = $eventDao->where($where1)->order('start_time asc, cate_id')->select();

		$where2 = array();
		$where2['start_time'] = array('lt', strtotime(date('Y-m-d 0:0:0')));
		$where2['end_time'] = array('egt', strtotime(date('Y-m-d 0:0:0')));
		if (count($cateIds)) $where2['cate_id'] = array('in', $cateIds);
		if (!empty($mainLocationId)) $where2['main_location_id'] = $mainLocationId;
		if (!empty($subLocationId)) $where2['sub_location_id'] = $subLocationId;
		$where2['is_audit'] = 1;
		$where2['_logic'] = 'and';
		$eventList2 = $eventDao->where($where2)->order('start_time desc, cate_id')->select();

		if (!empty($eventList1)){
			if (!empty($eventList2)){
				$eventList3 = array_merge($eventList1, $eventList2);
			} else {
				$eventList3 = $eventList1;
			}
		} else {
			if (!empty($eventList2)){
				$eventList3 = $eventList2;
			}
		}

		$where3 = array();
		$where3['end_time'] = array('lt', strtotime(date('Y-m-d 0:0:0')));
		if (count($cateIds)) $where3['cate_id'] = array('in', $cateIds);
		if (!empty($mainLocationId)) $where3['main_location_id'] = $mainLocationId;
		if (!empty($subLocationId)) $where3['sub_location_id'] = $subLocationId;
		$where3['is_audit'] = 1;
		$where3['_logic'] = 'and';
		$eventList4 = $eventDao->where($where3)->order('start_time desc, cate_id')->select();

		if (!empty($eventList4)){
			$eventList3 = array_merge($eventList3, $eventList4);
		}

		$count = count($eventList3);
		import('ORG.Util.Page');
		$p = new Page($count, 10);
		$multipage = $p->show();
		if ($_GET['p'] != NULL){
			$nowpage = $_GET['p'];
		}else{
			$nowpage = 1;
		}
		$arrayshowstart = ($nowpage*10)-10;
		$arrayshowend = $nowpage*10;

		for ($i=$arrayshowstart;$i<$arrayshowend;$i++){
			if ($eventList3[$i] != NULL){
				$eventList[$i] = $eventList3[$i];
				if ($eventList[$i]['end_time'] < strtotime(date('Y-m-d 0:0:0'))){
					$eventList[$i]['status']= "(已結束)";
				}
			}
		}

		foreach($eventList as $key=>$val){
			$eventList[$key]['pictures']=D('EventPicture')->where('event_id='.$val['id'])->select();
			if($eventList[$key]['shop_id'] != '0'){
				$eventList[$key]['shop']=D('Shop')->where('id='.$val['shop_id'])->find();
			}
			$eventList[$key]['cate']=D('EventCate')->where('id='.$val['cate_id'])->find();
			$eventList[$key]['main_location_name']=D('LocationMain')->where('id='.$val['main_location_id'])->getField('name');
			$eventList[$key]['sub_location_name']=D('LocationSub')->where('id='.$val['Sub_location_id'])->getField('name');
		}

		//數據壓入模板
		$this->assign('multipage', $multipage);
		$this->assign('eventList', $eventList);
		//===================================

		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}

	public function view(){
		$eventDao = D('Event');
		$eventCateDao = D('EventCate');
		$eventPictureDao = D('EventPicture');

		$eventId = $_GET['id'];

		if (isset($eventId)){

			$eventInfo = $eventDao->relation(true)->find($eventId);
			$cate = $eventCateDao->where(array('id'=>$eventInfo['cate']['parent_id']))->find();
			$eventInfo['parent_id'] = $cate['id'];
			$eventInfo['parent_name'] = $cate['name'];
			$eventInfo['sub_event']=$eventDao->where('parent_event_id='.$eventId)->select();
			if($eventInfo['parent_event_id']!='0'){
				$eventInfo['parent_event_name']=$eventDao->where('id='.$eventInfo['parent_event_id'])->getField('name');
			}

			if (isset($eventInfo) && $eventInfo['is_audit']==1){
				$nowTime = strtotime(date('Y-m-d').'00:00:00');

				if ($eventInfo['start_time'] < $nowTime && $eventInfo['end_time'] > $nowTime){
					$eventstate = '活動進行中...';
				}else{
					if ($eventInfo['start_time'] > $nowTime){
						$diff = timediff($eventInfo['start_time'], time());
						$eventstate = '距離活動開始還有' . $diff['day'] . '天' . $diff['hour'] . '小時' . $diff['min'] . '分' . $diff['sec'] . '秒';
					}
					if ($eventInfo['end_time'] < $nowTime){
						$eventstate = '該活動已結束.';
						$state2 = '(己結束)';
					}
				}
				$this->assign('state2',$state2);
				$this->assign('eventstate',$eventstate);

				$reviewDao = D('EventReview');
				$reviewPictureDao = D('ReviewPicture');
				$userDao = D('User');

				if (isset($_REQUEST['reviewid'])){
					$reviewid = $_REQUEST['reviewid'];
				}
				$where = array("event_id"=>$eventId);
				if ( !@$_SESSION['isLogged'] && !$_SESSION['username'] ) { $where['is_audit'] = 1; }

				$count = $reviewDao->where($where)->count('id');
				import('ORG.Util.Page');
				$p = new Page($count);
				$multipage = $p->show();
				$limit = $p->firstRow.','.$p->listRows;

				$eventInfo['reviews'] = $reviewDao->where($where)->limit($limit)->order('create_time desc')->select();

				if(!$eventInfo['reviews']) {
					$eventInfo['reviews'] = array();
				}

				foreach($eventInfo['reviews'] as &$review){
					$review['user'] = $userDao->find($review['user_id']);
				}

				foreach($eventInfo['reviews'] as &$review){
					$review['pictures'] = $reviewPictureDao->where('review_id='.$review['id'])->select();
				}

				$eventInfo['pictures'] = $eventPictureDao->where("event_id=$eventId")->select();


				$this->assign('multipage',$multipage);

				$this->assign('picstamp', time() . rand(1000,9999));

				$this->assign('total_review_count',count($eventInfo["reviews"]));

				$picidx = $piccount = C('EVENT_PHOTO_COUNT') ? C('EVENT_PHOTO_COUNT') : 8;
				$picids = array();
				while($picidx-->0){
					$picids[] = ($piccount - $picidx);
				}
				$this->assign('picids', $picids);

				$authInfo =  SimpleAcl::getLoginInfo();

				$userEventFavoriteDao = D('UserEventFavorite');
				$where = array();
				$where['user_id'] = $authInfo['id'];
				$where['event_id'] = $eventInfo['id'];
				if($userEventFavoriteDao->where($where)->limit(1)->select()){
					$eventFavoriteState = 1;
				}else{
					$eventFavoriteState = 0;
				}
				$this->assign('eventFavoriteState', $eventFavoriteState);

				if (isset($reviewid)){
					//Calculate last and next review id
					$count_review = sizeof($eventInfo['reviews']);
					if($count_review>1){
						$reviewKey = -1;
						foreach($eventInfo['reviews'] as $rkey=>$reviewItem){
							//echo $review['id'].'<br>';
							if($reviewItem['id'] == $reviewid){
								$reviewKey = $rkey;
							}
						}
					}
					if($reviewKey==-1){			//review not found
						$this->error('沒有找到該評介，您的請求可能來源於您的收藏夾或其他歷史URL，而此評介已被刪除');
						return;
					}elseif($reviewKey==0){		//first review
						$this->assign('nextReviewId',$eventInfo['reviews'][$reviewKey+1]['id']);
					}elseif($reviewKey==$count_review-1){	//last review
						$this->assign('lastReviewId',$eventInfo['reviews'][$reviewKey-1]['id']);
					}else{	//Inclusive
						$this->assign('lastReviewId',$eventInfo['reviews'][$reviewKey-1]['id']);
						$this->assign('nextReviewId',$eventInfo['reviews'][$reviewKey+1]['id']);
					}

					//Specify one review
					foreach($eventInfo['reviews'] as $key=>$reviewItem){
						if($reviewItem['id'] == $reviewid){
							unset($eventInfo['reviews']);
							$eventInfo['reviews'][] = $reviewItem;
						}
					}

					//Customise Meta Data for facebook share
					$description="";
					$reviewWord="";

					if($eventInfo['name']!=""){
						$reviewWord.=$eventInfo['name'];
					}else{
						$reviewWord.=$eventInfo['english_name'];
					}
					$reviewWord.=' - ';
					$reviewWord .= $eventInfo['reviews'][0]['title'];
					if(isset($_REQUEST['uname'])){
						$username=$_REQUEST['uname'].' ';
					}

					$stitle = '想同你分享「'.$reviewWord.'」呢篇評介，去OutStreet睇下啦！';
					if ($_REQUEST['like']) {$stitle = $reviewWord;}
					$description.= formatInfoUTF8(strip_tags($eventInfo['reviews'][0]['description']),180);

					if(isset($review['pictures'])){	//Setting up thumbnail
						$item["pictures"] = $review['pictures'];
						$this->assign('itemType','review');
					}elseif(isset($eventInfo['pictures'])){
						$item["pictures"] = $eventInfo['pictures'];
						$this->assign('itemType','event');
					}elseif(isset($eventInfo['reviews'][0]['user'])){
						$item["pictures"] = array(0=>array('file_path'=>getFace($eventInfo['reviews'][0]['user'])));
						$this->assign('itemType','user');
					}else{
						unset($item["pictures"]);
						$this->assign('itemType','none');
					}

					$this->assign('item',$item);
					$this->assign('description',$description);
					$this->assign('stitle',$stitle);
					//End customise Meta Data for facebook share
				}else{
					//Customise Meta Data for facebook share
					$description="";
					$reviewWord="";

					if($eventInfo['name']!=""){
						$reviewWord.=$eventInfo['name'];
					}else{
						$reviewWord.=$eventInfo['english_name'];
					}
					if(isset($_REQUEST['uname'])){
						$username=$_REQUEST['uname'].' ';
					}

					$stitle = '想同你分享「'.$reviewWord.'」呢個節目，去OutStreet睇下啦！';
					if ($_REQUEST['like']) {$stitle = $reviewWord;}
					$description.= formatInfoUTF8(strip_tags($eventInfo['details']),180);

					if(isset($eventInfo['pictures'])){
						$item["pictures"] = $eventInfo['pictures'];
						$this->assign('itemType','event');
					}else{
						unset($item["pictures"]);
						$this->assign('itemType','none');
					}

					$this->assign('item',$item);
					$this->assign('description',$description);
					$this->assign('stitle',$stitle);
					//End customise Meta Data for facebook share
				}
				$this->assign('eventInfo',$eventInfo);

				//$this->display();

				echo $content = $this->fetch();
				file_put_contents( $this->cache_filename, base64_encode( $content ) );

			}else{
				$this->error('沒有找到該節目,您的請求可能來源於您的收藏夾或其他歷史URL，而此節目已被刪除');
			}
		}else{
			$this->error('URL非法，缺少要查看的節目ID');
		}

	}//end view


	public function add(){

		$eventDao = D('Event');

		if (isset($_POST['submit'])){

			$eventInfo = $eventDao->create();

			$eventKeywordDao = D('EventKeyword');
			$eventKeywordMapDao = D('EventKeywordMap');

			if($eventInfo){

				import ( '@.ORG.SimpleAcl' );
				$userInfo =  SimpleAcl::getLoginInfo();

				$reference_id = D('EventCate')->where(array('id'=>$_POST['cate_id']))->getField('reference_id');

				$picstamp = $_POST['picstamp'];

				$eventInfo['start_time'] = $_POST['start_date']." ".$_POST['start_hour'];
				$eventInfo['start_time'] = strtotime($eventInfo['start_time']);
				$eventInfo['end_time'] = $_POST['end_date']." ".$_POST['end_hour'];
				$eventInfo['end_time'] = strtotime($eventInfo['end_time']);
				$eventInfo['user_id'] = $userInfo['id'];
				$eventInfo['is_audit'] = 0;
				$eventInfo['shop_id'] = $_POST['shop_id'];
				$eventInfo['cate_id'] = $reference_id ? $reference_id : $_POST['cate_id'];
				//$EventCateSubDao = D('EventCateSub');
				//$where = array();
				//$where['id'] = $_POST['cate_id'];
				//$SubCateDate = $EventCateSubDao->where($where)->find();
				//$eventInfo['sub_event_cate_name'] = $SubCateDate['name'];
				if($_POST['ticket_startdate']!=''){
					$eventInfo['ticket_startdate'] = strtotime($_POST['ticket_startdate']);
				}else{
					$eventInfo['ticket_startdate']=0;
				}

				if($_POST['ticket_required']==''){
					$eventInfo['ticket_required'] = 0;
				}else{
					$eventInfo['ticket_required'] = 1;
				}
				$keyword = $_POST['keyword'];

				if ($newid = $eventDao->add($eventInfo)){

					generateMainLocationStatistics($eventInfo['main_location_id']);
					generateSubLocationStatistics($eventInfo['sub_location_id']);

					//在將圖片設為正常狀態
					$eventPictureDao = D('EventPicture');
					$eventPictureDao->where("picstamp=$picstamp")->setField('event_id', $newid);

					//在event_keyword_map表中添加映射字段
					$keywords = array();
					$keyword = str_replace(',','|',$keyword);
					$keyword = str_replace('，','|',$keyword);
					$keyword = str_replace(' ','|',$keyword);
					$keyword = str_replace('、','|',$keyword);

					$keywords = explode('|',$keyword);

					foreach($keywords as $key){

						$keywordInfo = $eventKeywordDao->where("keyword='$key'")->find();
						if ($keywordInfo){	//如果存在該keyword, 就只添加映射
							$data = array();
							$data['event_id'] = $newid;
							$data['keyword_id'] = $keywordInfo['id'];

							$eventKeywordMapDao->data($data)->add();
						}else{				//如果不存在該keyword, 添加映射并在keyword表中添加新的keyword
							$newkeywordid = $eventKeywordDao->data(array('keyword'=>$key))->add();
							$data = array();
							$data['event_id'] = $newid;
							$data['keyword_id'] = $newkeywordid;

							$eventKeywordMapDao->data($data)->add();
						}

					}

					//添加完成,跳轉頁面
					$this->assign('jumpUrl',__ROOT__.'/event/');
					$this->success("添加節目成功,需要管理員審核后才可見!");
				}else{
					$this->error($eventDao->getDbError());
				}

			}else{
				//Validate數據驗證失敗 顯示失敗原
				$this->error($eventDao->getError());
			}
		}else{

			$inputDao = D('Input');
			$where = array();
			$where['caption'] = '活動適合';
			$this->assign('eventAttendList', $inputDao->where($where)->select());
			$eventCateDao = D('EventCate');
			$mainLocationDao = D('LocationMain');


			//獲取Event分類主目錄列表 壓入模板
			$eventCateList = $eventCateDao->where(array('parent_id'=>0))->select();
			$this->assign('eventCateList', $eventCateList);

			//獲取Location分類主目錄列表 壓入模板
			$mainLocationList = $mainLocationDao->select();
			$this->assign('mainLocationList', $mainLocationList);

			$picidx = $piccount = C('EVENT_PHOTO_COUNT') ? C('EVENT_PHOTO_COUNT') : 6;
			$picids = array();
			while($picidx-->0){
				$picids[] = ($piccount - $picidx);
			}
			$this->assign('picids', $picids);
			$this->assign('picstamp', time() . rand(1000,9999));

			$this->display();
		}

	}//end add

	public function edit(){
		C('SHOW_PAGE_TRACE',false);

		$id = $_REQUEST['id'];
		$picstamp = $_POST['picstamp'];
		$keyword = $_POST['keyword'];

		$eventDao = D('Event');

		//check isAudit
		$checkInfo = $eventDao->relation(true)->find($id);
		if(isset($id)){
			if(isset($checkInfo) && $checkInfo['is_audit']==1){
				if (isset($_POST['submit'])){
					Import('ORG.Util.Input');

					$reference_id = D('EventCate')->where(array('id'=>$_POST['cate_id']))->getField('reference_id');
					$eventInfo = array();
					$eventInfo['cate_id'] = $reference_id ? $reference_id : $_POST['cate_id'];
					$eventInfo['main_location_id'] = $_POST['main_location_id'];
					$eventInfo['sub_location_id'] = $_POST['sub_location_id'];
					$eventInfo['name'] = $_POST['name'];
					$eventInfo['address'] = $_POST['address'];
					$eventInfo['creater_name'] = $_POST['creater_name'];
					$eventInfo['details'] = $_POST['details'];
					$eventInfo['telephone'] = $_POST['telephone'];
					$eventInfo['website'] = $_POST['website'];
					$eventInfo['email'] = $_POST['email'];
					$eventInfo['keyword'] = $_POST['keyword'];
					$eventInfo['shop_id'] = $_POST['shop_id'];
					$eventInfo['rooms'] = $_POST['rooms'];
					$eventInfo['googlemap_lat'] = $_POST['googlemap_lat'];
					$eventInfo['googlemap_lng'] = $_POST['googlemap_lng'];
					$eventInfo['start_time'] = $_POST['start_date']." ".$_POST['start_hour'];
					$eventInfo['start_time'] = strtotime($eventInfo['start_time']);
					$eventInfo['end_time'] = $_POST['end_date']." ".$_POST['end_hour'];
					$eventInfo['end_time'] = strtotime($eventInfo['end_time']);
					$eventInfo['ticket_phone'] = $_POST['ticket_phone'];
					$eventInfo['ticket_website'] = $_POST['ticket_website'];
					$eventInfo['price'] = $_POST['price'];
					$eventInfo['ticket_startdate'] = strtotime($_POST['ticket_startdate']);
					$eventInfo['ticket_required'] = $_POST['ticket_required'];

					if($_POST['ticket_startdate']!=''){
						$eventInfo['ticket_startdate'] = strtotime($_POST['ticket_startdate']);
					}else{
						$eventInfo['ticket_startdate']=0;
					}

					if($_POST['ticket_required']==''){
						$eventInfo['ticket_required'] = 0;
					}else{
						$eventInfo['ticket_required'] = 1;
					}
					$eventInfo['update_time'] = time();

					import ( '@.ORG.SimpleAcl' );
					$userInfo =  SimpleAcl::getLoginInfo();
					$where['user_id'] = $userInfo['id'];
					$where['id'] = $id;
					$eventDao->where($where)->save($eventInfo);

					generateMainLocationStatistics($eventInfo['main_location_id']);
					generateSubLocationStatistics($eventInfo['sub_location_id']);

					$eventPictureDao = D('EventPicture');
					$eventPictureDao->where("picstamp=$picstamp")->setField('event_id', $id);
					$eventKeywordDao = D('EventKeyword');
					$eventKeywordMapDao = D('EventKeywordMap');
					$keywords = array();
					$keyword = str_replace(',','|',$keyword);
					$keyword = str_replace('，','|',$keyword);
					$keyword = str_replace(' ','|',$keyword);
					$keyword = str_replace('、','|',$keyword);
					$keywords = explode('|',$keyword);

					$eventKeywordMapDao->where('event_id='.$id)->delete();
					foreach($keywords as $key){

						if (!empty($key)){
							$keywordInfo = $eventKeywordDao->where("keyword='$key'")->find();
							if ($keywordInfo){
								$data = array();
								$data['event_id'] = $id;
								$data['keyword_id'] = $keywordInfo['id'];

								$eventKeywordMapDao->data($data)->add();
							}else{
								$newkeywordid = $eventKeywordDao->data(array('keyword'=>$key))->add();
								$data = array();
								$data['event_id'] = $id;
								$data['keyword_id'] = $newkeywordid;
								$eventKeywordMapDao->data($data)->add();
							}
						}
					}

					//$this->assign('waitSecond',30);
					$this->assign('jumpUrl',__ROOT__.'/event/view/id/'.$id);
					$this->assign('box',1);
					$this->success("更新成功");
				}else{

					$eventCateSubDao = D('EventCateSub');
					$locationSubDao = D('LocationSub');

					$eventInfo = $eventDao->relation(true)->find($id);
					$eventInfo['start_date'] = date('Y/m/d',$eventInfo['start_time']);
					$eventInfo['start_hour'] = date('H:i',$eventInfo['start_time']);
					$eventInfo['end_date'] = date('Y/m/d',$eventInfo['end_time']);
					$eventInfo['end_hour'] = date('H:i',$eventInfo['end_time']);
					$eventInfo['ticket_startdate'] = date('Y/m/d',$eventInfo['ticket_startdate']);
					$eventCateDao = D('EventCate');
					$mainLocationDao = D('LocationMain');

					//獲取Event分類主目錄列表 壓入模板
					$eventCateList = $eventCateDao->where(array('parent_id'=>0))->select();
					$this->assign('eventCateList', $eventCateList);

					//獲取Location分類主目錄列表 壓入模板
					$mainLocationList = $mainLocationDao->select();
					$this->assign('mainLocationList', $mainLocationList);

					$eventPictureDao = D('EventPicture');
					$eventInfo['pictures'] = $eventPictureDao->where("event_id=$id")->select();

					$this->assign('eventInfo', $eventInfo);
					$picidx = $piccount = C('EVENT_PHOTO_COUNT') ? C('EVENT_PHOTO_COUNT') : 5;
					$picids = array();
					while($picidx-->0){
						$picids[] = ($piccount - $picidx);
					}
					$this->assign('picids', $picids);

					$this->display();
				}
			}else{
				$this->error('沒有找到該節目，您的請求可能來源於您的收藏夾或其他歷史URL，而此節目已被刪除');
			}
		}else{
			$this->error('URL非法，缺少要查看的節目ID');
		}
	}//end edit

	public function uploadPic(){
		C('SHOW_PAGE_TRACE',false);

		$this->display();
	}

	public function uploadResult(){
		C('SHOW_PAGE_TRACE',false);

		if (isset($_POST['submitBtn'])){

			$eleid = $_POST['eleid'];
			$picstamp = $_POST['picstamp'];
			$description = $_POST['description'];
			$eventid = $_POST['eventid'];
			$from = $_POST['from'];

			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));

			switch(strtoupper($from)){
				case 'REVIEW':
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('EVENT_REVIEW_UPFILE_PATH').$eventid.'/';
				break;
				default:
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('EVENT_UPFILE_PATH');
				break;
			}

			$upload->saveRule = 'time';
			$upload->thumb = true;
			$upload->thumbMaxWidth = 120;
			$upload->thumbMaxHeight = 120;
			$upload->uploadReplace = true;


			if($upload->upload()) {
				$upinfo = $upload->getUploadFileInfo();
				$upfilepath = $upinfo[0]['savename'];

				switch(strtoupper($from)){
					case 'REVIEW':
						$dao = D('ReviewPicture');
					break;
					default:
						$dao = D('EventPicture');
					break;
				}

				$upfile = $dao->create();

				$upfile['file_path'] = $upfilepath;
				$upfile['description'] = $description;
				$upfile['picstamp'] = $picstamp;

				Import('@.ORG.SimpleAcl');
				$userInfo = SimpleAcl::getLoginInfo();
				$upfile['user_id'] = $userInfo['id'];

				$picid = $dao->add($upfile);

				if($from=='review'){
					$path = C('OUTSTREET_DIR').C('EVENT_REVIEW_UPFILE_PATH').$event_id;
				}

				$this->assign('eleid', $eleid);
				$this->assign('upfilepath', $upfilepath);
				$this->assign('description', $description);
				$this->assign('picid', $picid);

				$this->assign('path', $path);
				$this->assign('eventid', $eventid);
				$this->assign('from', $from);
				$this->assign('picstamp', $picstamp);

				$this->display('uploadResult');

			}
			else{

				$upfileerr = $upload->getErrorMsg();
				$upfilepath = '';
				echo '<script text="text/javascript">';
				echo 'parent.setPic("'.$eleid.'", "'.$upfilepath.'", "'.$upfileerr.'");';
				echo 'alert("'.$upfileerr.'");';
				echo '</script>';
			}
		}

	}

	public function deletePic(){

		$id = $_GET['id'];
		$from = $_GET['from'];

		Import('@.ORG.SimpleAcl');
		$userInfo = SimpleAcl::getLoginInfo();

		$where = array();
		$where['user_id'] = $userInfo['id'];
		$where['id'] = $id;

		$dao = D('EventPicture');
		switch(strtoupper($from)){
			case 'REVIEW':
				$dao = D('EventReviewPicture');
				break;
			default:
				$dao = D('EventPicture');
		}
		$picList = $dao->where($where)->select();
		switch(strtoupper($from)){
			case 'REVIEW':
				foreach($picList as $pic){
					unlink(C('OUTSTREET_ROOT').'/'.C('EVENT_REVIEW_UPFILE_PATH').$pic['event_id'].'/'.$pic['file_path']);
					unlink(C('OUTSTREET_ROOT').'/'.C('EVENT_REVIEW_UPFILE_PATH').$pic['event_id'].'/thumb_'.$pic['file_path']);
				}
				break;
			default:
				foreach($picList as $pic){
					unlink(C('OUTSTREET_ROOT').'/'.C('EVENT_UPFILE_PATH').$pic['file_path']);
					unlink(C('OUTSTREET_ROOT').'/'.C('EVENT_UPFILE_PATH').'thumb_' . $pic['file_path']);
				}
		}

		$dao->where($where)->delete();
		$this->display();
	}

	public function maps(){

		$id = $_GET['eventid'];

		$eventDao = D('Event');
		$mainLocationDao = D('LocationMain');
		$subLocationDao = D('LocationSub');

		$eventInfo = $eventDao->find($id);

		$mainLoc = $mainLocationDao->find($eventInfo['main_location_id']);
		$subLoc = $subLocationDao->find($eventInfo['sub_location_id']);
		$eventInfo['main_location_name']=$mainLoc['name'];
		$eventInfo['sub_location_name']=$subLoc['name'];

		//獲取Shop分類主目錄列表 壓入模板
		$eventCateDao = D('EventCate');
		$eventCateList = $eventCateDao->where(array('parent_id'=>0))->select();
		$this->assign('eventCateList', $eventCateList);

		$this->assign('eventInfo',$eventInfo);
		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}

	public function sellocation(){
		//獲取event分類主目錄列表 壓入模板
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');
		$locationMainList = $locationMainDao->select();

		foreach($locationMainList as &$locationMain){
			$where = array();
			$where['main_location_id'] = $locationMain['id'];
			$locationMain['locationSubList'] = $locationSubDao->where($where)->select();
		}

		$this->assign('locationMainList', $locationMainList);

		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );

	}

}

