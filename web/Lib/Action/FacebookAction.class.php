<?php

class FacebookAction extends BaseAction{

	function register(){
		$this->display();
	}

	function do_register(){
		$password = $_REQUEST['password'];
		$acc_edm = $_REQUEST['acc_edm'];
		
		if ( count( $this->fb_info ) )
		{
			$userInfo = array();
			$userInfo['nick_name'] = $this->fb_info['name'];
			$userInfo['first_name'] = $this->fb_info['first_name'];
			$userInfo['last_name'] = $this->fb_info['last_name'];
			$userInfo['password'] = md5($password);
			$userInfo['email'] = $this->fb_info['email'];
			$userInfo['sex'] = ($this->fb_info['gender'] == 'male') ? 1 : 0;
			$userInfo['facebook_id'] = $this->fb_info['id'];
			$userInfo['is_email_verify'] = 1;
			$userInfo['acc_edm'] = $acc_edm ? 1 : 0;
			$userInfo['create_time'] = time();
			$user_id=D('User')->add($userInfo);
			echo json_encode(array('success'=>1,'user_id'=>$user_id));
		}
		else
		{
			echo json_encode(array('success'=>0));
		}
	}

	function regstep2()
	{
		$user_id = $_REQUEST['user_id'];
		
		$inputDao = D('Input');
		$where = array();
		$where['caption'] = '教育程度';
		$this->assign('educationList', $inputDao->where($where)->select());
		$where['caption'] = '家庭每月收入';
		$this->assign('incomeList', $inputDao->where($where)->select());
		$where['caption'] = '現職';
		$this->assign('professionalList', $inputDao->where($where)->select());
		$where['caption'] = '大學';
		$this->assign('schoolList', $inputDao->where($where)->select());						
		$this->assign('newid', $user_id);
		$this->display('User:reg-step2');
	}

	function do_login(){
		$email = $_REQUEST['email'];
		$password = $_REQUEST['password'];
		
		if ( count( $this->fb_info ) )
		{
			if ( $user_id = D('User')->where(array('email'=>$email,'password'=>md5($password)))->getField('id') )
			{
				$userInfo = array();
				$userInfo['facebook_id'] = $this->fb_info['id'];
				D('User')->where(array('email'=>$email))->save($userInfo);
				echo json_encode(array('success'=>1,'user_id'=>$user_id));
			}
			else
			{
				echo json_encode(array('success'=>0,'code'=>2));
			}
		}
		else
		{
			echo json_encode(array('success'=>0,'code'=>1));
		}
	}

}


