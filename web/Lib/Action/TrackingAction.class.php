<?php

require_once COMMON_PATH . 'Mobile_Detect.php';
require_once COMMON_PATH . 'JavaScriptPacker.php';

class TrackingAction extends Action
{

	public $rate = 0.08;
	public $site = 'desktop';
	public $mobile = false;

	public function __construct()
	{
		parent::__construct();

		D( 'Tracking' )->where( 'created < "' . date( 'Y-m-d H:i:s', time() - 86400 * 7 ) . '"' )->delete();

		switch ( $_SERVER[ 'SERVER_NAME' ] )
		{
			case 'm.outstreet.com.hk':
			case 'mdev.outstreet.com.hk':
				$this->rate = 0.08;
				$this->site = 'mobile';
				break;
		}

		$detect = new Mobile_Detect();

		if ( $detect->isMobile() && !$detect->isTablet() )
		{
			$this->mobile = true;
		}

		$this->assign( 'mobile', $this->mobile );
	}

	public function code()
	{
		$code = md5( uniqid( time(), true ) );

		$step = '0';

		if ( !preg_match( '/bot/i', $_SERVER[ 'HTTP_USER_AGENT' ] ) )
		{
			if ( $this->site == 'mobile' )
			{
				if ( $this->mobile )
				{
					//$step = 1;
				}
			}
			else
			{
				if ( !$this->mobile )
				{
					if ( !in_array( rtrim( $_SERVER[ 'HTTP_REFERER' ], '/' ), array( 'http://outstreet.com.hk', 'http://www.outstreet.com.hk', 'http://outstreet.com.hk/index.php', 'http://www.outstreet.com.hk/index.php' ) ) )
					{
						//$step = 1;
					}
				}
			}
		}

		D( 'Tracking' )->add( array(
			'code'		 => $code,
			'step'		 => $step,
			'ip'		 => $_SERVER[ 'REMOTE_ADDR' ],
			'pure'		 => D( 'Tracking' )->where( 'step IN ( "6", "16" ) AND ip = "' . $_SERVER[ 'REMOTE_ADDR' ] . '" AND created >= "' . date( 'Y-m-d H:i:s', time() - 86400 ) . '"' )->count() ? '0' : '1',
			'device'	 => $this->site,
			'useragent'	 => $_SERVER[ 'HTTP_USER_AGENT' ],
			'referer'	 => $_SERVER[ 'HTTP_REFERER' ],
			'created'	 => date( 'Y-m-d H:i:s' )
		) );

		$json = json_encode( array( 'code' => $code, 'mobile' => $this->mobile, 'ip' => $_SERVER[ 'REMOTE_ADDR' ] ) );

		if ( $_GET[ 'token' ] )
		{
			echo $_GET[ 'token' ] . '(' . $json . ');';
		}
		else
		{
			echo $json;
		}
	}

	public function land()
	{
		if ( $_POST[ 'code' ] )
		{
			$tracking = D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ], 'step' => '1' ) )->find();

			if ( $tracking )
			{
				D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ] ) )->save( array( 'step' => '2' ) );

				$this->assign( 'content', $this->content() );
			}
		}

		$this->display( 'index' );
	}

	public function lead()
	{
		if ( $_POST[ 'code' ] )
		{
			$tracking = D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ], 'step' => '2' ) )->find();

			if ( $tracking )
			{
				D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ] ) )->save( array( 'step' => '3' ) );

				$total = D( 'Tracking' )->where( 'step IN ( "5", "6", "15", "16" ) AND device = "' . $this->site . '" AND DATE( created ) = "' . date( 'Y-m-d', time() ) . '"' )->count();
				$transacted = D( 'Tracking' )->where( 'step IN ( "6", "16" ) AND device = "' . $this->site . '" AND DATE( created ) = "' . date( 'Y-m-d' ) . '"' )->count();
				$rate = ( $total && $transacted ) ? $transacted / $total : 0;

				$rand = mt_rand( 1, 100 );

				$transact = ( $tracking[ 'pure' ] && $rate <= $this->rate && $rand ) ? '1' : '0';
				D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ] ) )->save( array( 'transact' => $transact, 'rate' => $rate ) );

				$pool = ( $transact || $rand >= 90 ) ? 'GREATEST( ( ( ( ysm_score + ysm_adjust ) / ysm_count ) * ysm_' . $this->site . ' ), 0.01 )' : '( ysm_' . $this->site . ' + 0.01 )';
				$cate = D( 'ShopCate' )->where( array( 'type' => 'all' ) )->order( '( -LOG( 1.0 - RAND() ) / ( ' . $pool . ' ) )' )->find();
				$link = formatShopCateLink( $cate );

				$this->assign( 'link', $link );
				$this->assign( 'content', $this->content() );
			}
		}

		$this->display( 'index' );
	}

	public function page()
	{
		if ( $_POST[ 'code' ] )
		{
			$tracking = D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ], 'step' => '3' ) )->find();

			if ( $tracking )
			{
				D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ] ) )->save( array( 'step' => '4' ) );

				$id = $_REQUEST[ 'category_id' ] ? $_REQUEST[ 'category_id' ] : $_REQUEST[ 'id' ];

				if ( $id )
				{
					$cate = D( 'ShopCate' )->where( array( 'id' => $id ) )->find();

					if ( $cate )
					{
						$this->assign( 'cate', $cate );
						$this->assign( 'content', $this->content() );
					}
				}
			}
		}

		$this->display( 'index' );
	}

	public function convert()
	{
		echo '<div>';

		if ( $_POST[ 'code' ] )
		{
			$tracking = D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ], 'step' => '4' ) )->find();

			if ( $tracking )
			{
				D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ] ) )->save( array( 'step' => '5' ) );

				$id = $_REQUEST[ 'category_id' ] ? $_REQUEST[ 'category_id' ] : $_REQUEST[ 'id' ];

				if ( $id && D( 'ShopCate' )->where( array( 'id' => $id ) )->count() )
				{
					D( 'ShopCate' )->where( array( 'id' => $id ) )->save( array(
						'ysm_' . $this->site => $_POST[ 'length' ]
					) );

					echo $this->content();
				}
			}
		}

		echo '</div>';
	}

	public function verify()
	{
		echo '<div>';

		if ( $_POST[ 'code' ] )
		{
			$tracking = D( 'Tracking' )->where( array( 'code' => $_POST[ 'code' ], 'step' => '5' ) )->find();

			if ( $tracking )
			{
				if ( $tracking[ 'transact' ] )
				{
					if ( $_POST[ 'length' ] )
					{
						if ( !D( 'Tracking' )->where( 'step IN ( "6", "16" ) AND ip = "' . $_SERVER[ 'REMOTE_ADDR' ] . '" AND created >= "' . date( 'Y-m-d H:i:s', time() - 86400 ) . '"' )->count() )
						{
							echo $this->content();
						}
					}
				}
			}
		}

		echo '</div>';
	}

	public function transact()
	{
		if ( $_GET[ 'code' ] )
		{
			$tracking = D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ], 'step' => '5' ) )->find();

			if ( $tracking )
			{
				if ( $tracking[ 'transact' ] )
				{
					D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ] ) )->save( array( 'step' => '6' ) );
				}
			}
		}

		echo 'void(0);';
	}

	public function view()
	{
		if ( $_GET[ 'code' ] )
		{
			$tracking = D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ], 'step' => '1' ) )->find();

			if ( $tracking )
			{
				D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ] ) )->save( array( 'step' => '15' ) );
			}
		}

		echo 'void(0);';
	}

	public function click()
	{
		if ( $_GET[ 'code' ] )
		{
			$tracking = D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ], 'step' => '15' ) )->find();

			if ( $tracking )
			{
				D( 'Tracking' )->where( array( 'code' => $_GET[ 'code' ] ) )->save( array( 'step' => '16' ) );
			}
		}

		echo 'void(0);';
	}

	public function js()
	{
		echo 'console.log(' . json_encode( $_SERVER ) . ');';
	}

	public function redirect()
	{
		$this->assign( 'content', $this->content() );
		$this->display( 'index' );
	}

	public function content()
	{
		$content = $this->fetch();

		return preg_replace_callback( '#<script>(.+?)</script>#is', function ( $matches )
		{
			$script = $this->script( $matches[ 1 ] );

			for ( $total = mt_rand( 5, 10 ), $i = 1; $i <= $total; $i++ )
			{
				$script = $this->script( $script );
			}

			return '<script>' . $script . '</script>';
		}, $content );
	}

	public function script( $script )
	{
		$packer = new JavaScriptPacker( $script, 'Normal', false, false );

		return 'e(d("' . base64_encode( $packer->pack() ) . '"));';
	}

}
