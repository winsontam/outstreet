<?php

class UploadimageAction extends BaseAction{
	public function uploadPic(){
		C('SHOW_PAGE_TRACE',false);
		$this->display();
	}

	public function uploadResult(){
		C('SHOW_PAGE_TRACE',false);

		if (isset($_REQUEST['submitBtn'])){

			$eleid = $_REQUEST['eleid'];
			$picstamp = $_REQUEST['picstamp'];
			$description = $_REQUEST['description'];
			$itemid = $_REQUEST['itemid'];
			$from = $_REQUEST['from'];
			$reviewPictureDao = D('ReviewPicture');

			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));

			switch(strtoupper($from)){
				case 'SHOP':
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH');
				break;				
				case 'REVIEW':
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('REVIEW_UPFILE_PATH').'/';					
				break;
				case 'REQUEST':
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH');
				break;
				case 'EVENT_REVIEW':
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('EVENT_REVIEW_UPFILE_PATH').'/';	
				break;	
				case 'PROMOTION_REVIEW':
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('PROMOTION_REVIEW_UPFILE_PATH').'/';
				break;
				case 'EVENT':	
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('EVENT_UPFILE_PATH').'/';					
				break;
				case 'SHOPNEWS':
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('SHOPNEWS_UPFILE_PATH').'/';
				break;
				default:
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH');
				break;		
			}

			$upload->saveRule = 'time';
			$upload->thumb = true;

			if(strtoupper($from)=='REVIEW' || strtoupper($from)=='EVENT_REVIEW' || strtoupper($from)=='PROMOTION_REVIEW'){
				$upload->thumbMaxWidth = 240;
				$upload->thumbMaxHeight = 180;
			}else{
				$upload->thumbMaxWidth = 120;
				$upload->thumbMaxHeight = 120;
			}

			$upload->uploadReplace = true;
	   
			if($upload->upload()) {
				$upinfo = $upload->getUploadFileInfo();
				$upfilepath = $upinfo[0]['savename'];

				$dao = null;
				switch(strtoupper($from)){
					case 'SHOP':
						$dao = D('ShopPicture');
					break;
					case 'EVENT':
						$dao = D('EventPicture');
					break;
					case 'REQUEST':
						$dao = D('ShopTmpPicture');						
					break;
					case 'SHOPNEWS':
						$dao = D('ShopNewsPicture');
					break;
					default:
						$dao = D('ReviewPicture');
					break;
				}

				$upfile = $dao->create();

				$upfile['file_path'] = $upfilepath;
				$upfile['description'] = $description;
				$upfile['picstamp'] = $picstamp;
				switch(strtoupper($from)){
					case 'REVIEW':
						//$upfile['shop_id'] = $shopid;
					break;
					case 'SHOPNEWS':
						$upfile['create_time'] = time();
					break;
					case 'REQUEST':
						$upfile['create_time'] = time();						
					break;					
				}

				Import('@.ORG.SimpleAcl');
				$userInfo = SimpleAcl::getLoginInfo();
				if($userInfo){
					$upfile['user_id'] = $userInfo['id'];
				}else{
					$upfile['user_id'] = 0;
				}

				$picid = $dao->add($upfile);
				
				if(strtoupper($from)=='SHOP'){
					$path = C('OUTSTREET_DIR').C('SHOP_UPFILE_PATH');
				}else if(strtoupper($from)=='REQUEST'){
					$path = C('OUTSTREET_DIR').C('REQUEST_SHOP_UPFILE_PATH');
				}else if(strtoupper($from)=='REVIEW'){
					$path = C('OUTSTREET_DIR').C('REVIEW_UPFILE_PATH');
				}else if(strtoupper($from)=='EVENT_REVIEW'){
					$path = C('OUTSTREET_DIR').C('EVENT_REVIEW_UPFILE_PATH');	
				}else if(strtoupper($from)=='PROMOTION_REVIEW'){
					$path = C('OUTSTREET_DIR').C('PROMOTION_REVIEW_UPFILE_PATH');
				}else if(strtoupper($from)=='EVENT'){
					$path = C('OUTSTREET_DIR').C('EVENT_UPFILE_PATH');		
				}else if(strtoupper($from)=='SHOPNEWS'){
					$path = C('OUTSTREET_DIR').C('SHOPNEWS_UPFILE_PATH');
				}

				$this->assign('eleid', $eleid);
				$this->assign('upfilepath', $upfilepath);
				$this->assign('description', $description);
				$this->assign('picid', $picid);
				$this->assign('path', $path);
				$this->assign('itemid', $itemid);
				$this->assign('from', $from);				
				$this->assign('picstamp', $picstamp);				
				$this->display('uploadResult');
			} else {
				$upfileerr = $upload->getErrorMsg();
				$upfilepath = '';
				echo '<script text="text/javascript">';
				echo 'parent.setPic("'.$eleid.'", "'.$upfilepath.'", "'.$upfileerr.'","","","'.__URL__.'","'.$from.'","'.$picstamp.'","'.$itemid.'");';			
				echo 'alert("'.$upfileerr.'");';
				echo '</script>';
			}
		}
		 
	}
	
	public function deletePic(){
		$id = $_REQUEST['id'];
		$from = $_REQUEST['from'];
		 
		Import('@.ORG.SimpleAcl');
		$userInfo = SimpleAcl::getLoginInfo();
		 
		$where = array();
		$where['user_id'] = $userInfo['id'];
		$where['id'] = $id;
		 
		switch(strtoupper($from)){
			case 'SHOP':
				$dao = D('ShopPicture');
				break;
			case 'EVENT':
				$dao = D('EventPicture');
				break;
			case 'REQUEST':
				$dao = D('ShopTmpPicture');						
				break;
			case 'SHOPNEWS':
				$dao = D('ShopNewsPicture');
				break;
			default:
				$dao = D('ReviewPicture');
				break;
		}
		 
		$picList = $dao->where($where)->select();
		
		if(strtoupper($from)=='SHOP'){
			foreach($picList as $pic){
				unlink(C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH').$pic['file_path']);
				unlink(C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH').'thumb_'.$pic['file_path']);			
			}
		}else if(strtoupper($from)=='REQUEST'){
			foreach($picList as $pic){		
				unlink(C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH').'/'.$pic['file_path']);
				unlink(C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH').'/thumb_'.$pic['file_path']);			
			}
		}else if(strtoupper($from)=='REVIEW'){
			foreach($picList as $pic){
				unlink(C('OUTSTREET_ROOT').'/'.C('REVIEW_UPFILE_PATH').'/'.$pic['file_path']);
				unlink(C('OUTSTREET_ROOT').'/'.C('REVIEW_UPFILE_PATH').'/thumb_'.$pic['file_path']);							
			}
		}else if(strtoupper($from)=='EVENT_REVIEW'){
			foreach($picList as $pic){
				unlink(C('OUTSTREET_ROOT').'/'.C('EVENT_REVIEW_UPFILE_PATH').'/'.$pic['file_path']);
				unlink(C('OUTSTREET_ROOT').'/'.C('EVENT_REVIEW_UPFILE_PATH').'/thumb_'.$pic['file_path']);							
			}
		}else if(strtoupper($from)=='PROMOTION_REVIEW'){
			foreach($picList as $pic){
				unlink(C('OUTSTREET_ROOT').'/'.C('PROMOTION_REVIEW_UPFILE_PATH').'/'.$pic['file_path']);
				unlink(C('OUTSTREET_ROOT').'/'.C('PROMOTION_REVIEW_UPFILE_PATH').'/thumb_'.$pic['file_path']);							
			}
		}else if(strtoupper($from)=='EVENT'){
			unlink(C('OUTSTREET_ROOT').'/'.C('EVENT_UPFILE_PATH').'/'.$pic['file_path']);
			unlink(C('OUTSTREET_ROOT').'/'.C('EVENT_UPFILE_PATH').'/thumb_'.$pic['file_path']);		
		}else if(strtoupper($from)=='SHOPNEWS'){
			foreach($picList as $pic){
				unlink(C('OUTSTREET_ROOT').'/'.C('SHOPNEWS_UPFILE_PATH').'/'.$pic['file_path']);
				unlink(C('OUTSTREET_ROOT').'/'.C('SHOPNEWS_UPFILE_PATH').'/thumb_'.$pic['file_path']);
			}
		}
		$dao->where($where)->delete();
		$this->display();
	}
}

