<?php

class PromotionAction extends BaseAction
{
	public function index()
	{
		$promotiondbo = D('Promotion');
		$type1specialpromotion = $promotiondbo->field('promotion_fields.*')
			->relation(true)->where(array('type1'=>1,'is_special'=>1,'is_audit'=>1))->order('is_ended asc, promotion_fields.create_time desc, promotion_fields.id desc')->limit(3)->select();
		$type2specialpromotion = $promotiondbo->field('promotion_fields.*')
			->relation(true)->where(array('type1'=>2,'is_special'=>1,'is_audit'=>1))->order('is_ended asc, promotion_fields.create_time desc, promotion_fields.id desc')->limit(3)->select();

		$type1promotion = $promotiondbo->field('promotion_fields.*')
			->relation(true)->where(array('type1'=>1,'is_audit'=>1))->limit(5)->order('is_ended asc, promotion_fields.create_time desc, promotion_fields.id desc')->select();
		$type2promotion = $promotiondbo->field('promotion_fields.*')
			->relation(true)->where(array('type1'=>2,'is_audit'=>1))->limit(5)->order('is_ended asc, promotion_fields.create_time desc, promotion_fields.id desc')->select();

		$sublocation = array();
		$location = M('location_sub')->select();
		foreach( $location AS $val ) { $sublocation[$val['id']] = $val; }

		$this->assign( 'type1specialpromotion', $type1specialpromotion );
		$this->assign( 'type2specialpromotion', $type2specialpromotion );
		$this->assign( 'type1promotion', $type1promotion );
		$this->assign( 'type2promotion', $type2promotion );
		$this->assign( 'sublocation', $sublocation );
		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}

	public function view()
	{
		//checkVisitFrequecy();
		$promotionDao = D('Promotion');
		$promotionId = $_REQUEST['id'];

		if (isset($promotionId)){

			$promotionInfo = $promotionDao->field('promotion_fields.*,WEEKDAY(promotion_fields.time_start) AS time_start_weekday ,WEEKDAY(promotion_fields.time_end) AS time_end_weekday')->relation(true)->where(array('id'=>$promotionId,'is_audit'=>1))->find();

			if (isset($promotionInfo) && ($promotionInfo['is_audit']==1 || (@$_SESSION['isLogged'] && $_SESSION['username'])) ){
				$reviewDao = D('Review');
				$userDao = D('User');
				$reviewPictureDao = D('ReviewPicture');
				$where = array("promotion_id"=>$promotionId);

				if (isset($_REQUEST['reviewid'])){
					$reviewid = $_REQUEST['reviewid'];
				}

				if ( !@$_SESSION['isLogged'] && !$_SESSION['username'] ) { $where['is_audit'] = 1; }
				$count = $reviewDao->where($where)->count('id');
				import('ORG.Util.Page');
				$p = new Page($count);
				$multipage = $p->show();
				$limit = $p->firstRow.','.$p->listRows;
				$promotionInfo['reviews'] = $reviewDao->where($where)->limit($limit)->order('create_time desc')->select();

				if(!$promotionInfo['reviews']) {
					$promotionInfo['reviews'] = array();
				}

				foreach($promotionInfo['reviews'] as &$review){
					$review['user'] = $userDao->find($review['user_id']);
					$review['pictures'] = $reviewPictureDao->where("review_id={$review['id']}")->select();
				}

				$brand = M('PromotionShopMap')->field('shop_brand.*')
				->join('JOIN shop_fields ON promotion_shop_map.shop_id=shop_fields.id')
				->join('JOIN shop_brand ON shop_fields.brand_id=shop_brand.id')
				->where(array('promotion_shop_map.promotion_id'=>$promotionId))->group('shop_brand.id')->select();

				$this->assign('multipage',$multipage);
				$this->assign('picstamp', time() . rand(1000,9999));

				$picidx = $piccount = C('REVIEW_PHOTO_COUNT') ? C('REVIEW_PHOTO_COUNT') : 8;
				$picids = array();
				while($picidx-->0){
					$picids[] = ($piccount - $picidx);
				}
				$authInfo =  SimpleAcl::getLoginInfo();
				$userPromotionFavoriteDao = D('UserPromotionFavorite');
				$where = array();
				$where['user_id'] = $authInfo['id'];
				$where['promotion_id'] = $promotionInfo['id'];
				if($userPromotionFavoriteDao->where($where)->limit(1)->select()){
					$promotionFavoriteState = 1;
				}else{
					$promotionFavoriteState = 0;
				}
				$this->assign('promotionFavoriteState', $promotionFavoriteState);
				$this->assign('picids', $picids);

				$authInfo =  SimpleAcl::getLoginInfo();
				$this->assign('total_review_count',count($promotionInfo["reviews"]));

				if (isset($reviewid)){	//read review mode
					//Calculate last and next review id
					$count_review = sizeof($promotionInfo['reviews']);
					$reviewKey=-1;
					if($count_review>=1){
						foreach($promotionInfo['reviews'] as $rkey=>$reviewItem){
							if($reviewItem['id'] == $reviewid){
								$reviewKey = $rkey;
							}
						}
					}
					if($reviewKey==-1){			//review not found
						$this->error('沒有找到該評介，您的請求可能來源於您的收藏夾或其他歷史URL，而此評介已被刪除');
						return;
					}elseif($reviewKey==0){		//first review
						$this->assign('nextReviewId',$promotionInfo['reviews'][$reviewKey+1]['id']);
					}elseif($reviewKey==$count_review-1){	//last review
						$this->assign('lastReviewId',$promotionInfo['reviews'][$reviewKey-1]['id']);
					}else{	//Inclusive
						$this->assign('lastReviewId',$promotionInfo['reviews'][$reviewKey-1]['id']);
						$this->assign('nextReviewId',$promotionInfo['reviews'][$reviewKey+1]['id']);
					}

					//Specify one review
					foreach($promotionInfo['reviews'] as $key=>$reviewItem){
						if($reviewItem['id'] == $reviewid){
							unset($promotionInfo['reviews']);
							$promotionInfo['reviews'][] = $reviewItem;
						}
					}

					//Customise Meta Data for facebook share
					$description="";
					$reviewWord="";

					if($promotionInfo['name']!=""){
						$reviewWord.=$promotionInfo['name'];
					}else{
						$reviewWord.=$promotionInfo['english_name'];
					}
					$reviewWord.=' - ';
					$reviewWord.= $promotionInfo['reviews'][0]['title'];
					if(isset($_REQUEST['uname'])){
						$username=$_REQUEST['uname'].' ';
					}
					$stitle = '想同你分享「'.$reviewWord.'」呢篇評介，去OutStreet睇下啦！';
					$description.= formatInfoUTF8(strip_tags($promotionInfo['reviews'][0]['description']),180);

					if(isset($review['pictures'])){	//Setting up thumbnail
						$item["pictures"] = $review['pictures'];
						$this->assign('itemType','review');
					}elseif(isset($promotionInfo['pictures'])){
						$item["pictures"] = $promotionInfo['pictures'];
						$this->assign('itemType','promotion');
					}elseif(isset($promotionInfo['reviews'][0]['user'])){
						$item["pictures"] = array(0=>array('file_path'=>getFace($promotionInfo['reviews'][0]['user'])));
						$this->assign('itemType','user');
					}else{
						unset($item["pictures"]);
						$this->assign('itemType','none');
					}

					$this->assign('item',$item);
					$this->assign('description',$description);
					$this->assign('stitle',$stitle);
					//End customise Meta Data for facebook share
				}else{	//view shop mode
					//Customise Meta Data for facebook share
					$description="";
					$reviewWord="";

					if($promotionInfo['name']!=""){
						$reviewWord.=$promotionInfo['name'];
					}else{
						$reviewWord.=$promotionInfo['english_name'];
					}
					if(isset($_REQUEST['uname'])){
						$username=$_REQUEST['uname'].' ';
					}
					$stitle= '想同你分享「'.$reviewWord.'」呢個優惠，去OutStreet睇下啦！';
					$description.= formatInfoUTF8(strip_tags($promotionInfo['description']),180);

					if(isset($promotionInfo['pictures'])){
						$item["pictures"] = $promotionInfo['pictures'];
						$this->assign('itemType','promotion');
					}else{
						unset($item["pictures"]);
						$this->assign('itemType','none');
					}

					$this->assign('item',$item);
					$this->assign('description',$description);
					$this->assign('stitle',$stitle);
				}

				$sublocation = array();
				$location = M('location_sub')->select();
				foreach( $location AS $val ) { $sublocation[$val['id']] = $val; }

				$this->assign('promotionInfo', $promotionInfo );
				$this->assign('brand', $brand );
				$this->assign('sublocation', $sublocation );
				$this->assign('picstamp', time() . rand(1000,9999));
				//$this->display();

				echo $content = $this->fetch();
				file_put_contents( $this->cache_filename, base64_encode( $content ) );
			}else{
				$this->error('沒有找到該優惠，您的請求可能來源於您的收藏夾或其他歷史URL，而此商戶已被刪除');
			}
		}else{
			$this->error('URL非法，缺少要查看的優惠ID');
		}
	}

	public function lists()
	{
		$type1 = $_REQUEST['type1'];

		$where = array();
		if ($type1) { $where[ 'type1' ] = $type1; }
		$where[ 'is_audit' ] = 1;

		$promotiondbo = D('Promotion');

		$count = $promotiondbo->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$promotions = $promotiondbo->field('promotion_fields.*,WEEKDAY(promotion_fields.time_start) AS time_start_weekday ,WEEKDAY(promotion_fields.time_end) AS time_end_weekday')
			->relation(true)->where($where)->limit($limit)->order('is_ended asc, promotion_fields.create_time desc, promotion_fields.id DESC')->select();

		$sublocation = array();
		$location = M('location_sub')->select();
		foreach( $location AS $val ) { $sublocation[$val['id']] = $val; }

		$this->assign( 'promotions', $promotions );
		$this->assign( 'type1', $type1 );
		$this->assign( 'sublocation', $sublocation );
		$this->assign( 'multipage', $multipage );
		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}

	public function ajax_search_promotion()
	{
		$query = $_REQUEST['query'];

		$shoplist = array();
		$where = array('name'=>array('like','%'.$query.'%'));
		if ( count( $where ) )
			{ $shoplist = M('ShopFields')->where($where)->select(); }

		echo json_encode(array('list'=>$shoplist));
	}

}

