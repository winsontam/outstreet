<?php

header( 'Content-Type: text/html; charset=UTF-8' );

class ReviewformAction extends BaseAction{

	function addReview(){
		$formType 	= $_REQUEST['formType'];
		$item_id 	= $_REQUEST['item_id'];
		$picstamp 	= $_REQUEST['picstamp'];
		$rate_1 = $_REQUEST['rate_1'];
		$rate_2 = $_REQUEST['rate_2'];
		$rate_3 = $_REQUEST['rate_3'];
		$rate_4 = $_REQUEST['rate_4'];
		$rate_5 = $_REQUEST['rate_5'];
		$rate_6 = $_REQUEST['rate_6'];
		$rate_7 = $_REQUEST['rate_7'];
		$item_field = '';

		switch($formType){
			case 'SHOP':
				$item_field = 'shop_id';
				break;
			case 'EVENT':
				$item_field = 'event_id';	
				break;		
			case 'PROMOTION':	
				$item_field = 'promotion_id';			
				break;		
			default:
				return;
		}

		if (isset($_REQUEST['submit'])){

			$reviewDao = D('Review');
			$reviewInfo = $reviewDao->create();
			$reviewInfo['description']=stripslashes(html_entity_decode($_POST['description']));	

			if ($reviewInfo){
				//Login User
				Import('@.ORG.SimpleAcl');
				$userInfo = SimpleAcl::getLoginInfo();
				if($userInfo){
					$reviewInfo['user_id'] = $userInfo['id'];
				}else{//new_user
					$mail = $_REQUEST['email'];
					$nickname = $_REQUEST['nick_name'];
					$password = $this->getRandomPassword();
					//register
					$userDao = D('User');
					$data = array();
					$data['nick_name'] = $nickname;
					$data['email'] = $mail;
					$data['password'] = md5($password);
					$data['create_time'] = time();
					if ($newid = $userDao->add($data)){
						//$this->sendValidateEmail($newid,$password);
						$reviewInfo['user_id'] = $newid;
					}else{
						$this->error($userDao->getError());	
					}
				}

				$reviewInfo[$item_field] = $item_id;

				if($formType=="SHOP"){
					$reviewInfo['rate'] = array();
					$reviewInfo['rate']['shop_id'] = $item_id;
					$reviewInfo['rate']['rate_1'] = isset($rate_1) ? $rate_1 : 0 ;
					$reviewInfo['rate']['rate_2'] = isset($rate_2) ? $rate_2 : '' ;
					$reviewInfo['rate']['rate_3'] = isset($rate_3) ? $rate_3 : '' ;
					$reviewInfo['rate']['rate_4'] = isset($rate_4) ? $rate_4 : '' ;
					$reviewInfo['rate']['rate_5'] = isset($rate_5) ? $rate_5 : '' ;
					$reviewInfo['rate']['rate_6'] = isset($rate_6) ? $rate_6 : '' ;
					$reviewInfo['rate']['rate_7'] = isset($rate_7) ? $rate_7 : '' ;
		
					$rate_totals = array();
					//$rate_totals[] = $reviewInfo['rate']['rate_1'];
					if ( is_numeric( $reviewInfo['rate']['rate_2'] ) ) { $rate_totals[] = $reviewInfo['rate']['rate_2']; }
					else { $reviewInfo['rate']['rate_2'] = -1; }
					if ( is_numeric( $reviewInfo['rate']['rate_3'] ) ) { $rate_totals[] = $reviewInfo['rate']['rate_3']; }
					else { $reviewInfo['rate']['rate_3'] = -1; }
					if ( is_numeric( $reviewInfo['rate']['rate_4'] ) ) { $rate_totals[] = $reviewInfo['rate']['rate_4']; }
					else { $reviewInfo['rate']['rate_4'] = -1; }
					if ( is_numeric( $reviewInfo['rate']['rate_5'] ) ) { $rate_totals[] = $reviewInfo['rate']['rate_5']; }
					else { $reviewInfo['rate']['rate_5'] = -1; }
					if ( is_numeric( $reviewInfo['rate']['rate_6'] ) ) { $rate_totals[] = $reviewInfo['rate']['rate_6']; }
					else { $reviewInfo['rate']['rate_6'] = -1; }
					if ( is_numeric( $reviewInfo['rate']['rate_7'] ) ) { $rate_totals[] = $reviewInfo['rate']['rate_7']; }
					else { $reviewInfo['rate']['rate_7'] = -1; }
		
					$reviewInfo['rate']['rate_total'] = round( array_sum($rate_totals)/count($rate_totals), 2 );
				}

				if ($newid = $reviewDao->relation(true)->add($reviewInfo)){
					$reviewPictureDao = D('ReviewPicture');
					$reviewPictureDao->where("picstamp=$picstamp")->setField('review_id', $newid);
					
					if($formType=="SHOP"){
						?><script>alert('多謝你在OutStreet寫評介！OutStreet會盡快查閱及刊登，並以EMAIL通知你！');</script><?php
						?><script>parent.tb_remove();</script><?php
					}else{
						$this->success('多謝你在OutStreet寫評介！OutStreet會盡快查閱及刊登，並以EMAIL通知你！');
					}
				}else{
					$this->error($reviewDao->getError());
				}
			}else{
				$this->error($reviewDao->getError());
			}

			?><script>parent.tb_remove();</script><?php
		}else{
			$this->error('請勿非法提交');
		}
	}

	private function sendValidateEmail($userid,$password){
		$userDao = D('User');
    	$userInfo = $userDao->find($userid);

		$caption = '多謝註冊成為OutStreet會員！請確認你的電郵。';
		$this->assign('nickname', $userInfo['nick_name']);
		$this->assign('password', $password);
		$this->assign('login_name', $userInfo['email']);
		$this->assign('caption', '多謝註冊成為OutStreet會員！請確認你的電郵。');
		$this->assign('userstamp', $this->_userStampEncode($userid, $userInfo['create_time']));
		$this->assign('domain', $_SERVER['SERVER_NAME']);
		$body = $this->fetch('web/Tpl/default/Review/email_validate.html');

		if($userInfo['email'] != '') {
			$this->smtp_mail_log($userInfo['email'],$caption,$body,'',2,2);
		}
    }

	//進行用戶身份認證
    //唯一來路: 用戶EMAIL
	public function doValidateEmail(){
	  	$userstamp = $_GET['userstamp'];

	  	if (isset($userstamp)){
	  		$where = $this->_userStampDecode($userstamp);
	  		if ($where){
		  		$userDao = D('User');
		  		$user = $userDao->where($where)->find();
		  		$userDao->where($where)->setField('is_email_verify', 1);

				$data = array();
				$data['user_id'] = $user['id'];
				$data['type'] = 1;
				$data['score'] = 30;
				$data['isValid'] = 1;
				$data['create_time'] = time();
				M('UserScoreLog')->add($data);
		  		$this->assign('jumpUrl', '__ROOT__/user/register/submit/step2/uid/'.$user['id']);
		  		$this->success('恭喜您，您的Email已經通過認證。多謝你在OutStreet寫評介！OutStreet會盡快查閱及刊登，並以EMAIL通知你！');
	  		}else{
		  		$this->error('系統錯誤，請聯繫管理員。');
  			}
	  	}else{
	  		$this->error('認證碼格式不正確，請聯繫管理員。');
	  	}
	}

	private function _userStampEncode($id,$createtime){
    	return base64_encode(rand(1000,9999) . $createtime . $id);
    }

	private function _userStampDecode($userstamp){
    	$userstamp = base64_decode($userstamp);

    	if ($len = strlen($userstamp) < 15){
    		return false;
    	}else{
    		$where = array();
    		$where['create_time'] = substr($userstamp, 4, 10);
    		$where['id'] = substr($userstamp, 14);
    		return $where;
    	}
    }

	private function getRandomPassword($length = 8){
		$password = "";
		$possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
		$maxlength = strlen($possible);
		if ($length > $maxlength) {
			$length = $maxlength;
		}
		$i = 0; 
		while ($i < $length) {
			$char = substr($possible, mt_rand(0, $maxlength-1), 1);
		  	if (!strstr($password, $char)) {
				$password .= $char;
				$i++;
		  	}
		}
		return $password;
	}
}

