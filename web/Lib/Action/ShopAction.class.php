<?php

class ShopAction extends BaseAction{

	private function _subcate($cateids, $shopid, $shopname){

		$shopCateDao = D('ShopCate');
		$shopCateMapDao = D('ShopCateMap');

		$where = array();
		$where['shop_id'] = $shopid;
		$shopCateMapDao->where($where)->delete();

		$cateids = explode(',',$cateids);

		foreach($cateids as $cateid){
			if (!empty($cateid)){
				$cateInfo = $shopCateDao->find($cateid);
				if ($cateInfo){
					$data = array();
					$data['shop_id'] = $shopid;
					$data['shop_name'] = $shopname;
					$data['cate_id'] = $cateInfo['id'];
					$data['cate_name'] = $cateInfo['name'];

					$shopCateMapDao->data($data)->add();
				}
			}
		}

	}

	public function search(){
		//checkVisitFrequecy();
		// 加載數據訪問對象
		$shopDao = D('Shop');

		$cate_ids = $_REQUEST['shop_cate_ids'];
		$sub_location_ids = $_REQUEST['sub_location_ids'];
		$searchterms = $_REQUEST['searchterms'];
		$searchterms = trim($searchterms);
		$this->assign('shop_searchterms',$searchterms);
		$distance = (int) $_REQUEST['distance'];
		$distance = $distance ? $distance : 100;
		$nearby = trim( $_REQUEST['nearby'] );
		$price_range = array();
		for($i=1; $i<=4;$i++){
			if(strpos($_REQUEST['price_range'],$i.'')){
				$price_range[] = $i;
			}
		}
		$this->assign('price_range_arr', $price_range);

		$this->assign('distance', $distance);
		$this->assign('nearby', $nearby);

		if (!empty($cate_ids)){
			$cate_ids = explode(',',$cate_ids);
			$shopCateDao = D('ShopCate');
			$where = array();
			$where['id'] = array('in', $cate_ids);
			$where['type'] = array('neq', 'iphone');
			$cateList = $shopCateDao->where($where)->select();

			$cate_names = '';
			$tempCate=array();
			$parentCate = array();
			foreach($cateList as $cate){
				if(!in_array($cate['name'],$tempCate)){
					if($cate_names!=''){
						$cate_names .=',';
					}
					$cate_names .= $cate['name'];
					$tempCate[]=$cate['name'];
					$selected_cates[$cate['id']] = $cate['name'];

				}
				if($cate['level']==3){
					$parentCate[$cate['parent_id']]['ids'][]=$cate['id'];
				}
			}
			foreach($parentCate as $t_key=>$t_cate){
				$t_count = $shopCateDao->where('parent_id = '.$t_key)->count();
				if($t_count == count($t_cate['ids'])){
					foreach($t_cate['ids'] as $t_cate_ids){
					unset($selected_cates[$t_cate_ids]);
					}
				}
			}

			$this->assign('parent_cate',$parentCate);
			$this->assign('shop_cate_names',$cate_names);
			$this->assign('selected_cates',$selected_cates);
			$children = $shopCateDao->where(array('parent_id' => array('in', $cate_ids)))->select();
			$childrenIds = array();
			foreach($children AS $val) { $childrenIds[] = $val['id']; $cate_ids[] = $val['id']; }

			while( count($childrenIds) ){
				$children = $shopCateDao->where(array('parent_id' => array('in', $childrenIds)))->select();
				$childrenIds = array();
				foreach($children AS $val) { $childrenIds[] = $val['id']; $cate_ids[] = $val['id']; }
			}
		}

		if (!empty($sub_location_ids)){
			$locationMainDao=D('LocationMain');
			$locationSubDao = D('LocationSub');
			$where = array();
			$where['id'] = array('in', $sub_location_ids);
			$locationList = $locationSubDao->where($where)->select();

			$selected_locations = array();
			$sub_location_names = '';
			foreach($locationList as $location){
				$main_location[$location['main_location_id']]['ids'][]=$location['id'];
				$main_locationname=$locationMainDao->where('id='.$location['main_location_id'])->getField('name');
				if(!count($main_location[$location['main_location_id']]['name'])){
					$main_location[$location['main_location_id']]['name'][]=$main_locationname;
				}

				if($sub_location_names!=''){ $sub_location_names .=','; }
				$sub_location_names .= $location['name'];
				$selected_locations[$location['id']] = $location['name'];
			}

			for($i=1;$i<5;$i++){
				$limit=$locationSubDao->where('main_location_id='.$i)->count();
				if(count($main_location[$i]['ids'])!=$limit){
					unset($main_location[$i]);
				}
			}

			$this->assign('main_location',$main_location);
			$this->assign('sub_location_names',$sub_location_names);
			$this->assign('selected_locations',$selected_locations);
		}

		$where = array();

		foreach($cate_ids as &$cate_id){
			$reference_id = $shopCateDao->where('id='.$cate_id)->getField('reference_id');
			$cate_id = $reference_id?$reference_id:$cate_id;
		}
		unset($cate_id);
		if (!empty($cate_ids)) $where['cate_id'] = array('in', $cate_ids);
		if (!empty($sub_location_ids)) $where['sub_location_id'] = array('in', $sub_location_ids);
		$where['is_audit'] = 1;
		if($price_range){
			$where['price_range'] = array('in',$price_range);
		}
		$countQuerys = array();

		if ( $searchterms ) {
			$terms = $searchterms;
			$allterms = array();
			if ( preg_match_all( '/\w+/', $terms, $m ) ) {
				foreach ( $m[0] AS $t ) {
					$terms = str_replace($t,'',$terms);
					$allterms[$t] = 'e';
				}
			}
			$mterms = array();
			mb_regex_encoding( 'UTF-8' );
			mb_ereg_search_init( $terms, '\w+' );
			while ( ( $m = mb_ereg_search_regs() ) )
				{ $allterms[ $m[0] ] = 'm'; }

			$termpharse = array();
			$searchtermspharse = explode(' ',$searchterms);
			foreach($searchtermspharse as $pharse){if($pharse){$termpharse[]=strtoupper($pharse);}}
			$suggestWhere = array();
			foreach ( $termpharse AS $term) {
				$suggestWhere[ 'UPPER(keyword)' ][] = array( 'like', $term );
			}

			$suggestWhere[ 'UPPER(keyword)' ][] = 'or';
			$suggestKeywords = M('KeywordTranslate')->where($suggestWhere)->select();
			foreach ( $suggestKeywords AS $val ) {
				$keywords = explode( ',', $val['suggest_keywords'] );
				foreach ( $keywords AS $k ) { if ($k) { $allterms[ $k ] = 's'; } }
			}

			//START Cate keywords
			$shopCateDao = D('ShopCate');
			$where2 = array();
			$where2['UPPER(keyword)'] = array('like', '%'.strtoupper($searchterms).'%');
			$where2['type'] = array('neq', 'iphone');
			$cateList = $shopCateDao->where($where2)->select();
			if(!empty($cateList)){
				foreach($cateList AS $cate) { $key_cate_ids[] = $cate['id']; }
				$where[ '_string' ] = ' cate_id in 	('.implode(',',getsubcateids($key_cate_ids)).') OR ';
			};
			//END Cate keywords

			$searchcols = array(
				'CONCAT(`name`,":",`eng_name`)' => 10,
				'`keyword`' => 6,
				'`address`' => 4
			);
			$likecolumn = '[likecols]';
			$orQuerys = array();

			foreach ( $allterms AS $term => $type ) {
				if ( $type == 'e' || $type == 's' ) {
					//$orQuerys[] = $likecolumn . ' REGEXP "[[:<:]]' . $term . '.{0,3}[[:>:]]"';
				 	$orQuerys[] = $likecolumn . ' REGEXP "' . $term . '"';
				} elseif ( $type == 'm' ) {
					if ( ( $len = preg_match_all( '/./u', $term, $t ) ) > 1 ) {
						$orQuerys[] = $likecolumn . ' REGEXP "(' . implode('|',$t[0]) . '){' . round($len*0.8) . ',' . ($len) . '}"';
					} else {
						$orQuerys[] = $likecolumn . ' LIKE "%' . $term . '%"';
					}
				}
				foreach ( $searchcols AS $col => $score ) {
					$countQuerys[] = '( ( ' . $col . ' REGEXP "' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "[[:<:]]' . $term . '[[:>:]]" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "^' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "' . $term . '$" ) * ' . $score . ')'
						. ' + ( IF( length(' . $col . ') = length("' . $term . '"), 1, 0 ) * ' . $score . ')';
				}
			}

			$cates=M('ShopCate')->where(array('type'=>array('neq','iphone'),'_string'=>'(' . str_replace( '[likecols]', 'name', implode( ' OR ', $orQuerys ) ) . ')'))->select();
			$searchcateids = array();
			foreach($cates AS $cate) { $searchcateids[] = $cate['id']; }
			if ( count($searchcateids) ) {
				$where[ '_string' ] .= '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' AND ', $orQuerys ) ) . ' OR b.cate_id IN (' . implode( ',', $searchcateids ) . '))';
				$countQuerys[] = '((b.cate_id IN (' . implode( ',', $searchcateids ) . '))*20)';
			} else {
				$where[ '_string' ] .= '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' AND ', $orQuerys ) ) . ')';
			}
		}

		/*if ($nearby) {
			$api_url = 'http://maps.google.com.hk/maps/api/geocode/json?address='.urlencode('香港 '.$nearby).'&sensor=false';
			$json = file_get_contents($api_url);
			if ($json) {
				$json = json_decode($json, true);
				//print_r($json);
				if ($json['status'] == 'OK') {
					$lat=$json['results'][0]['geometry']['location']['lat'];
					$lng=$json['results'][0]['geometry']['location']['lng'];
				}
			}
			if ($lat && $lng && $lat != 22.396428 && $lng != 114.109497 && floor($lat) == 22)
			{
				if ($where[ '_string' ]) { $where[ '_string' ] .= ' AND '; }
				else { $where[ '_string' ] = ''; }
				$where[ '_string' ] .= '( 6371 * acos( cos( radians('.$lat.') ) * cos( radians( googlemap_lat ) ) * cos( radians( googlemap_lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians( googlemap_lat ) ) ) ) <= '.$distance.'/1000';
			} else {
				$this->assign('isNearbyNotFound', true);
				$this->display('lists');
				exit();
			}
		}*/

		$countQuerys[] = '( IF( length(telephone), 1, 0 ) )' . ' + ( IF( a.shop_status > 0, 1, 0 ) * -5 )' . ' + ( IF( d.id>0, 5, 0 ) )';
		if(!empty($key_cate_ids)){$countQuerys[] = '(b.cate_id in ('.implode(',',getsubcateids($key_cate_ids)).'))*6';}
		$countField = '((' . implode( ' + ', $countQuerys ) . ') + ( IF(c.review_count is null,0,c.review_count) ) + IF ( a.shop_level, a.shop_level*1000, 0 )) AS scores';
		if ( !$_REQUEST['groupbrand'] ) {
			$count = $shopDao->table( 'shop_fields a join shop_cate_map b on a.id = b.shop_id' )->join( 'shop_picture d ON b.shop_id = d.shop_id' )->where( $where )->count( 'distinct a.id' );
			import('ORG.Util.Page');
			$p = new Page($count);
			$multipage = $p->show();
			$limit = $p->firstRow.','.$p->listRows;

			//$shopList = array_slice($shopDao->relation(true)->field( 'a.*,'.$countField )->table( 'shop_fields a join shop_cate_map b on a.id = b.shop_id' )->join( 'statistics_shop c ON b.shop_id = c.shop_id' )->join( 'shop_picture d ON b.shop_id = d.shop_id' )->where( $where )->group( 'a.id' )->order( 'a.shop_status, scores DESC, hit_rate DESC' )->select(),$p->firstRow,0);
			$shopList = $shopDao->relation(true)->field( 'a.*,'.$countField )->table( 'shop_fields a join shop_cate_map b on a.id = b.shop_id' )->join( 'statistics_shop c ON b.shop_id = c.shop_id' )->join( 'shop_picture d ON b.shop_id = d.shop_id' )->where( $where )->group( 'a.id' )->limit( $limit )->order( 'a.shop_status, scores DESC, hit_rate DESC' )->select();
		} else {
			$count = $shopDao->field('COUNT(distinct IF(a.brand_id>0,CONCAT("b",a.brand_id),CONCAT("s",a.id))) AS count')->table( 'shop_fields a join shop_cate_map b on a.id = b.shop_id' )->join( 'shop_picture d ON b.shop_id = d.shop_id' )->where( $where )->find();
			$count = $count['count'];
			import('ORG.Util.Page');
			$p = new Page($count);
			$multipage = $p->show();
			$limit = $p->firstRow.','.$p->listRows;

			//$shopList = array_slice($shopDao->relation(true)->field( 'a.*,'.$countField )->table( 'shop_fields a join shop_cate_map b on a.id = b.shop_id' )->join( 'statistics_shop c ON b.shop_id = c.shop_id' )->join( 'shop_picture d ON b.shop_id = d.shop_id' )->where( $where )->group( 'IF(a.brand_id>0,CONCAT("b",a.brand_id),CONCAT("s",a.id))' )->order( 'a.shop_status, scores DESC, hit_rate DESC' )->select(),$p->firstRow,0);
			$shopList = $shopDao->relation(true)->field( 'a.*,'.$countField )->table( 'shop_fields a join shop_cate_map b on a.id = b.shop_id' )->join( 'statistics_shop c ON b.shop_id = c.shop_id' )->join( 'shop_picture d ON b.shop_id = d.shop_id' )->where( $where )->group( 'IF(a.brand_id>0,CONCAT("b",a.brand_id),CONCAT("s",a.id))' )->limit( $limit )->order( 'a.shop_status, scores DESC, hit_rate DESC' )->select();
		}

		foreach($shopList as &$shopInfo){
			$shopInfo['description']=getShopDetail($shopInfo,1);
			$shopInfo['website']=getShopDetail($shopInfo,2);
			$shopInfo['email']=getShopDetail($shopInfo,3);
			$shopInfo['pictures']=getShopDetail($shopInfo,4);
			//working hour logic
			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shopInfo['id']);
			$working_hour = $shopTimeDao->where($where)->order('start_time,end_time,recurring_week')->select();

			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shopInfo['id'],"recurring_week "=>0);
			$special_working_hour = $shopTimeDao->where($where)->order('start_day,end_day')->select();

			$output = generateShopWorkTime($shopInfo['work_time'],$working_hour,$special_working_hour);

			$shopInfo['work_time'] = $output;
			//end working hour logic
		}
		unset($shopInfo);

		if(!empty($searchterms)){
			$searchLogDao = D('SearchLog');
			$data = array();
			$data['create_time'] = time();
			$data['keywords'] = $searchterms;
			$data['results'] = $count;
			$data['request_uri'] = getenv('REQUEST_URI');
			$data['http_referer'] = getenv('HTTP_REFERER');
			$searchLogDao->add($data);
		}

		//數據壓入模板
		$this->assign('multipage', $multipage);
		$this->assign('shopList', $shopList);

		//模板與數據整合 并輸出到HTTP流
		//$this->display('lists');

		echo $content = $this->fetch('lists');
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}

	public function lists(){
		//checkVisitFrequecy();

		// 加載數據訪問對象
		$shopDao = D('Shop');
		$shopCateDao = D('ShopCate');
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');
		$brandDao = D('Brand');

		//接收查詢變量
		Import('ORG.Util.Input');
		$cate_id 		= $_GET['shop_cate_ids'] ? $_GET['shop_cate_ids'] : $_GET['id'];
		$mainlocationid = $_GET['mainlocationid'];
		$sublocationid 	= $_GET['sublocationid'];
		$brandId 		= $_GET['brandid'];
		$parentshopid 	= $_GET['parentshopid'];
		$rootlocation 	= $_GET['rootlocation'];

		if ( $cate_id ) {

			$user_agent = $_SERVER[ 'HTTP_USER_AGENT' ];
			$referer = $_SERVER[ 'HTTP_REFERER' ];
			$ip_address = $this->getRealIpAddr();
			if ( !M( 'ShopCateLog' )->where( array( 'cate_id' => $cate_id, 'ip_address' => $ip_address, '_string' => 'NOW() - create_time < 3600' ) )->count() )
				{ M( 'ShopCateLog' )->add( array( 'cate_id' => $cate_id, 'ip_address' => $ip_address, 'user_agent' => $user_agent, 'referer' => $referer, 'create_time' => array( 'exp', 'NOW()' ) ) ); }

			$cate = $shopCateDao->where( array( 'id' => $cate_id ) )->find();
			if ($cate) { $parentCateId = $cate['parent_id']; $shop_cate_name = $cate['name'];}
			if ( $parentCateId ) { $parentCate = $shopCateDao->where( array( 'id' => $parentCateId ) )->find(); }

			$this->assign('cateId', $cate_id);
			$this->assign('parentCateId', $parentCateId);
			$this->assign('shop_cate_ids',$cate_id);
			$this->assign('shop_cate_names',$shop_cate_name);
			$this->assign('cate', $cate);
			$this->assign('parentCate', $parentCate);
		}

		$cate_ids = array();
		if ( $cate_id && $shopCateDao->where(array('id'=>$cate_id))->count() ) {
			$cate_ids[] = $cate_id;
			$shopCateList = $shopCateDao->where(array('parent_id'=>$cate_id))->order('shop_count desc')->select();
			foreach( $shopCateList AS $val ) { $cate_ids[] = $val['id']; }
			$this->assign('shopCateList', $shopCateList);
		} else {
			$shopCateList = $shopCateDao->where(array('parent_id'=>0))->order('shop_count desc')->select();
			$this->assign('shopCateList', $shopCateList);
		}

		$parent_id = $cate_id;
		while( $parent_id ) {
			$cate = $shopCateDao->find($parent_id);
			$cate_list[] = $cate;
			$parent_id = $cate['parent_id'];
		}
		$this->assign('cate_list', array_reverse($cate_list));
		$this->assign('cate_id', $cate_id);

		$search_cate_ids = array();

		if ($cate_id) {
			$reference_id = $shopCateDao->where('id='.$cate_id)->getField('reference_id');
			$cate_id = $reference_id?$reference_id:$cate_id;
			$search_cate_ids[] = $cate_id;
			$loopids = array();
			$loopids[] = $cate_id;
			$loop = true;
			while($loop) {
				$shopCateList = $shopCateDao->where(array('parent_id'=>array('in',$loopids)))->select();
				$cate = array();
				foreach( $shopCateList AS $val ) {
					$cate[] = $val['id'];
					$search_cate_ids[] = $val['reference_id']?$val['reference_id']:$val['id'];
				}
				if ( count($cate) ) { $loopids = $cate; }
				else { $loop = false; }
			}
		}

		$whereRef = array();
		$whereRef['parent_id']=$cate_id;
		$redirectShopList = $shopCateDao->where($whereRef)->order('shop_count desc')->select();
		if(sizeof($redirectShopList)>0&&isset($cate_id)){
			$cate_direct_id = $redirectShopList[0]['id'];
		}else{
			$cate_direct_id = $cate_id;
		}
		$this->assign('cate_direct_id',$cate_direct_id);

		//SESSION狀態維護  END
		//=====================================

		//獲取商戶主目錄列表 壓入模板
		$locationMainList = $locationMainDao->order('id')->select();
		$this->assign('locationMainList', $locationMainList);

		//獲取商戶子目錄列表 壓入模板
		if (isset($mainlocationid)){
			$where = array();
			$where['main_location_id'] = $mainlocationid;
			$locationSubList = $locationSubDao->where($where)->order('shop_count desc')->select();
			$this->assign('locationSubList', $locationSubList);
		}

		//獲取當前SubLocation信息 壓入模板
		if (isset($sublocationid)){
			$where = array();
			$where['id'] = $sublocationid;
			$curSubLocation = $locationSubDao->where($where)->find();
			$this->assign('curSubLocation', $curSubLocation);
		}

		//指定sublocationid時 mainlocationid根據SubLocation決定
		if ($curSubLocation){
			$mainlocationid = $curSubLocation['main_location_id'];
		}

		//獲取當前MainLocation信息 壓入模板
		if (isset($mainlocationid)){
			$where = array();
			$where['id'] = $mainlocationid;
			$curMainLocation = $locationMainDao->where($where)->find();
			$this->assign('curMainLocation', $curMainLocation);
		}

		$where = array();
		$where[ 'shop_fields.is_audit' ] = 1;
		if ( count($search_cate_ids) ) $where[ 'shop_cate_map.cate_id' ] = array( 'in', $search_cate_ids );
		if ( $mainlocationid ) $where[ 'shop_fields.main_location_id' ] = $mainlocationid;
		if ( $sublocationid ) $where[ 'shop_fields.sub_location_id' ] = $sublocationid;
		if ( $brandId ) $where[ 'shop_fields.brand_id' ] = $brandId;
		if ( $parentshopid ) $where[ 'shop_fields.parent_shop_id' ] = $parentshopid;

		$count = $shopDao->join( 'shop_cate_map ON shop_fields.id = shop_cate_map.shop_id' )->where( $where )->count('distinct shop_fields.id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		//生成sql语句
		$sql = "select shop_fields.* from shop_fields join shop_cate_map $sql_where ";
		$sql .= "group by shop_fields.id order by shop_status asc, hit_rate desc, id desc limit $limit";

		$shopList = $shopDao
			->field('shop_fields.*,
				( IF( length(shop_fields.telephone), 1, 0 ) )
				+ ( IF( shop_fields.shop_status > 0, 1, 0 ) * -5 )
				+ ( IF( shop_picture.id>0, 5, 0 ) )
				+ ( IF( statistics_shop.review_count is null,0,statistics_shop.review_count) )
				+ ( IF( shop_fields.shop_level, shop_fields.shop_level*1000, 0 ) ) AS scores
			')
			->join( 'JOIN shop_cate_map ON shop_fields.id = shop_cate_map.shop_id' )
			->join('statistics_shop ON shop_fields.id = statistics_shop.shop_id')
			->join('shop_picture ON shop_fields.id = shop_picture.shop_id')
			->where( $where )
			->group( 'shop_fields.id' )
			->order('shop_fields.shop_status, scores DESC, hit_rate DESC')
			->limit( $limit )
			->select();

		foreach($shopList as $key=>$shop){
			$shopDao = D('Shop');
			$where=array();
			$where['id']=$shop['id'];
			$shopRel = $shopDao->relation(true)->where($where)->find();
			$shopList[$key]['sub_location']['name']=$shopRel['sub_location']['name'];
			$shopList[$key]['main_location']['name']=$shopRel['main_location']['name'];
			$shopList[$key]['description']=getShopDetail($shopList[$key],1);
			$shopList[$key]['website']=getShopDetail($shopList[$key],2);
			$shopList[$key]['email']=getShopDetail($shopList[$key],3);
			$shopList[$key]['pictures']=getShopDetail($shopRel,4);
		}

		$statisticsShopDao = D('StatisticsShop');
		$userDao = D('User');
		foreach($shopList as &$shopInfo){
			$where = array();
			$where['shop_id'] = $shopInfo['id'];
			$shopInfo['statistics'] = $statisticsShopDao->where($where)->find();
			$shopInfo['keywords']=explode(",",$shopInfo['keyword']);

			// list sub cates
			$sqlcate = "select b.* from shop_cate_map a left join shop_cate b on a.cate_id = b.id where shop_id = ".$shopInfo['id'];
			$shopInfo['sub_cates'] = $shopDao->query($sqlcate);

			//working hour logic
			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shopInfo['id']);
			$working_hour = $shopTimeDao->where($where)->order('start_time,end_time,recurring_week')->select();

			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shopInfo['id'],"recurring_week "=>0);
			$special_working_hour = $shopTimeDao->where($where)->order('start_day,end_day')->select();

			$output = generateShopWorkTime($shopInfo['work_time'],$working_hour,$special_working_hour);

			$shopInfo['work_time'] = $output;
			//end working hour logic
		}

		//數據壓入模板
		$this->assign('multipage', $multipage);
		$this->assign('shopList', $shopList);

		$statisticsShopDao = D('StatisticsShop');
		$where = array();
		$shopreviewlist = $statisticsShopDao->relation(true)->where($where)->limit(5)->order('review_count desc')->select();
		$this->assign('shopreviewlist', $shopreviewlist);

		//模板與數據整合 并輸出到HTTP流
		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}

	public function index(){
		// 加載數據訪問對象
		$ShopCateDao = D('ShopCate');

		//獲取Shop分類主目錄列表 壓入模板
		$ShopCateList = $ShopCateDao->where( array( 'parent_id' => 0,'type'=>array('neq','iphone') ) )->order('shop_count desc')->select();
		$this->assign('ShopCateList', $ShopCateList);

		//獲取商戶主目錄列表 壓入模板
		$this->getRateList(1,'indexlist01');
		$this->getRateList(10,'indexlist02');
		$this->getRateList(6,'indexlist03');

		//***************************************** Start To Get Latest Shop *************************************************//
		$shopDao = D('Shop');
		$where=array();
		$where['is_audit'] 	 = 1;
		$shopIds = array();
		$shopLatestList = array();
		foreach(array(18,10,140) as $parentId ) {
			$where['shop_cate.parent_id'] = $parentId;
			$shop = $shopDao->field('shop_fields.*')
				->join('JOIN shop_cate_map ON shop_fields.id = shop_cate_map.shop_id')
				->join('JOIN shop_cate ON shop_cate_map.cate_id = shop_cate.id')
				->where($where)->order('create_time desc')->relation(true)->find();
			$where['shop_fields.id'][] = array('neq',$shop['id']);
			$shop['description']=getShopDetail($shop,1);
			$shopLatestList[] = $shop;
		}

		$this->assign('shopLatestList', $shopLatestList);
		//******************************************* End Of Get Latest Shop *************************************************//

		//模板與數據整合 并輸出到HTTP流
		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}//end index

	private function getRateList($shop_cate_id, $rateListName){
		$shopDao = D('Shop');

		$where=array();
		$where['shop_fields.is_audit'] = 1;
		$where['shop_special.cate_id'] = $shop_cate_id;
		$shopIdList = $shopDao->field('shop_fields.id as shop_id')->join('shop_special ON shop_fields.id = shop_special.shop_id')->where($where)->order('shop_special.rank asc')->limit(2)->select();

		foreach($shopIdList as $key=>$val){
			$shopInfo = $shopDao->relation(true)->find($val['shop_id']);
			$rateList[$key]['shop_id']			=$shopInfo['id'];
			$rateList[$key]['brand_id']			=$shopInfo['brand_id'];
			$rateList[$key]['shop_name']		=$shopInfo['name'];
			$rateList[$key]['eng_name']			=$shopInfo['eng_name'];
			$rateList[$key]['sub_cates']		=$shopInfo['sub_cates'];
			$rateList[$key]['sub_location']		=$shopInfo['sub_location'];
			$rateList[$key]['pictures']			=$shopInfo['pictures'];
			$rateList[$key]['description']		=getShopDetail($shopInfo,1);
			$rateList[$key]['statistics']		=$shopInfo['statistics'];
		}

		$this->assign("$rateListName",$rateList);
	}

	public function view(){
		//checkVisitFrequecy();
		$shopDao = D('Shop');
		$shopId = $_REQUEST['id'];

		if (isset($shopId)){
			$shopInfo = $shopDao->relation(true)->find($shopId);
			$shopInfo['description']=getShopDetail($shopInfo,1);
			$shopInfo['website']=getShopDetail($shopInfo,2);
			$shopInfo['email']=getShopDetail($shopInfo,3);

			if (isset($shopInfo) && ($shopInfo['is_audit']==1 || (@$_SESSION['isLogged'] && $_SESSION['username'])) ){
				$reviewDao = D('Review');
				$reviewRateDao = D('ReviewRate');
				$userDao = D('User');
				$reviewPictureDao = D('ReviewPicture');
				$where = array("shop_id"=>$shopId);

				if (isset($_REQUEST['reviewid'])){
					$reviewid = $_REQUEST['reviewid'];
				}

				if ( !@$_SESSION['isLogged'] && !$_SESSION['username'] ) { $where['is_audit'] = 1; }
				$count = $reviewDao->where($where)->count('id');
				import('ORG.Util.Page');
				$p = new Page($count);
				$multipage = $p->show();
				$limit = $p->firstRow.','.$p->listRows;
				$shopInfo['reviews'] = $reviewDao->where($where)->limit($limit)->order('create_time desc')->select();

				if(!$shopInfo['reviews']) {
					$shopInfo['reviews'] = array();
				}

				$shopInfo['keywords']=explode(",",$shopInfo['keyword']);
				/*if(count($shopInfo['keywords'])>3){
					$shopInfo['keywords'] = array_slice($shopInfo['keywords'],0,3);
				}*/

				foreach($shopInfo['reviews'] as &$review){
					$review['rate'] = $reviewRateDao->where("review_id={$review['id']}")->find();
					$review['user'] = $userDao->find($review['user_id']);
					$review['pictures'] = $reviewPictureDao->where("review_id={$review['id']}")->select();
				}

				if (!empty($shopInfo['brand_id'])){
					$where = array();
					$where['brand_id'] = $shopInfo['brand_id'];
					$this->assign('brandShopList', $shopDao->where($where)->order('create_time asc')->select());
				}

				if(count($shopInfo['pictures']) == 0) {
					$brandDao = D('Brand');
					$brandInfo = $brandDao->where('id='.$shopInfo['brand_id'])->select();

					if (!empty($brandInfo[0]['file_path'])){
						$shopInfo['pictures'][0]['file_path'] = $brandInfo[0]['file_path'];
					}
				}

				/*Count 有效PDF*/
				$countpdf = 0;
				foreach($shopInfo['pdf'] as $key=>$val){
					if($val['is_audit']==1 && strtotime($val['start_date'])<=time() && strtotime($val['end_date'])>=time()){
						$countpdf++;
					}
				}
				$this->assign('countpdf',$countpdf);
				/*Count 有效PDF 完*/

				$avgRate = $shopInfo['statistics']["total_rating"];

				$this->assign('avgRate',$avgRate);
				$this->assign('multipage',$multipage);
				$this->assign('picstamp', time().rand(1000,9999));

				$picidx = $piccount = C('REVIEW_PHOTO_COUNT') ? C('REVIEW_PHOTO_COUNT') : 8;
				$picids = array();
				while($picidx-->0){
					$picids[] = ($piccount - $picidx);
				}
				$this->assign('picids', $picids);

				$shopCateDao = D('ShopCate');
				$cate_list = array();
				$sub_cates = array();
				foreach ( $shopInfo['sub_cates'] AS $val ) { $sub_cates[$val['id']] = $val; }
				$cate_id = ( $_REQUEST['cate_id'] && $sub_cates[$_REQUEST['cate_id']] ) ? $_REQUEST['cate_id'] : $shopInfo['sub_cates'][0]['id'];
				$cate_list[] = $sub_cates[$cate_id];

				$parent_id = $sub_cates[$cate_id]['parent_id'];
				while( $parent_id )
				{
					$cate = $shopCateDao->find($parent_id);
					$cate_list[] = $cate;
					$parent_id = $cate['parent_id'];
				}
				$this->assign('cate_list', array_reverse($cate_list));

				$authInfo =  SimpleAcl::getLoginInfo();

				$userShopFavoriteDao = D('UserShopFavorite');
				$where = array();
				$where['user_id'] = $authInfo['id'];
				$where['shop_id'] = $shopInfo['id'];
				if($userShopFavoriteDao->where($where)->limit(1)->select()){
					$shopFavoriteState = 1;
				}else{
					$shopFavoriteState = 0;
				}
				$this->assign('shopFavoriteState', $shopFavoriteState);

				//Hit rate 增加
				/*$hit_rate = $shopInfo['hit_rate'];
				$hit_rate++;
				$shopDao->where("id=$shopId")->setField('hit_rate', $hit_rate);*/

				//working hour logic
				$shopTimeDao = D('ShopTime');
				$where = array("shop_id"=>$shopId);
				$working_hour = $shopTimeDao->where($where)->order('start_time,end_time,recurring_week')->select();

				$shopTimeDao = D('ShopTime');
				$where = array("shop_id"=>$shopId,"recurring_week "=>0);
				$special_working_hour = $shopTimeDao->where($where)->order('start_day,end_day')->select();

				//商戶消息
				$shopnewsDao=D('ShopNews');
				$where = array();
				$where['b.shop_id']=$shopId;
				$where['a.is_audit']=1;
				$shopnewsList = $shopnewsDao
				->table('shop_news a')
				->field('a.*')
				->join('shop_news_map b on a.id=b.news_id')
				->where($where)
				->order('time_start desc')
				->limit(6)
				->select();
				$shopnewspictureDao=D('ShopNewsPicture');
				foreach($shopnewsList as $key=>$val){
					$shopnewsList[$key]['picture']=$shopnewspictureDao->where('picstamp='.$val['picstamp'])->find();
				}

				//商戶消息--節目
				$shopEventDao = D('Event');
				$where = array();
				$where["shop_id"]=$shopId;
				//$where["end_time"]=array('gt',strtotime(date('Y-m-d 0:0:0')));
				$where["is_audit"]=1;

				$shopEventList =  $shopEventDao->where($where)->order('start_time desc')->limit(6)->select();
				//related promotion
				$relatedpromotion = M('PromotionFields')->field('promotion_fields.*,WEEKDAY(promotion_fields.time_start) AS time_start_weekday ,WEEKDAY(promotion_fields.time_end) AS time_end_weekday, IF(promotion_fields.time_start,promotion_fields.time_start,promotion_fields.create_time) AS sorter')
				->join('JOIN promotion_shop_map ON promotion_fields.id=promotion_shop_map.promotion_id')
				->where(array('promotion_shop_map.shop_id'=>$shopId,'promotion_fields.is_audit'=>1))->group('promotion_fields.id')->limit(6)->order('sorter desc')->select();
				//combine event & promotion
				$combineList = array();
				$c = 0;
				foreach($shopnewsList as $news){
					$combineList[strtotime($news['time_start'])+$c]= array('type'=>'0','item'=>$news);
					$c++;
				}
				foreach($shopEventList as $event){
					$combineList[$event['start_time']+$c]= array('type'=>'1','item'=>$event);
					$c++;
				}
				foreach($relatedpromotion as $promotion){
					$combineList[$promotion['sorter']+$c] = array('type'=>'2','item'=>$promotion);
					$c++;
				}
				krsort($combineList);	//sort by key desc
				$count = 0;
				foreach($combineList as $key=>$obj){
					$count++;
					if($count>6){
						unset($combineList[$key]);
					}
				}
				$this->assign('combineList',$combineList);
				$this->assign('eventList',$shopEventList);
				$this->assign('relatedpromotion',$relatedpromotion);

				$output = generateShopWorkTime($shopInfo['work_time'],$working_hour,$special_working_hour);

				$this->assign('working_hour',$output);
				//end working hour logic

				$this->assign('similar_shops',$this->selectedShop(1,$shopInfo['id'],$cate_list,$shopInfo['sub_location_id']));
				$this->assign('total_review_count',count($shopInfo["reviews"]));

				if (isset($reviewid)){	//read review mode
					//Calculate last and next review id
					$count_review = sizeof($shopInfo['reviews']);
					$reviewKey=-1;
					if($count_review>=1){
						foreach($shopInfo['reviews'] as $rkey=>$reviewItem){
							//echo $review['id'].'<br>';
							if($reviewItem['id'] == $reviewid){
								$reviewKey = $rkey;
							}
						}
					}
					if($reviewKey==-1){			//review not found
						$this->error('沒有找到該評介，您的請求可能來源於您的收藏夾或其他歷史URL，而此評介已被刪除');
						return;
					}elseif($reviewKey==0){		//first review
						$this->assign('nextReviewId',$shopInfo['reviews'][$reviewKey+1]['id']);
					}elseif($reviewKey==$count_review-1){	//last review
						$this->assign('lastReviewId',$shopInfo['reviews'][$reviewKey-1]['id']);
					}else{	//Inclusive
						$this->assign('lastReviewId',$shopInfo['reviews'][$reviewKey-1]['id']);
						$this->assign('nextReviewId',$shopInfo['reviews'][$reviewKey+1]['id']);
					}

					//Specify one review
					foreach($shopInfo['reviews'] as $key=>$reviewItem){
						if($reviewItem['id'] == $reviewid){
							unset($shopInfo['reviews']);
							$shopInfo['reviews'][] = $reviewItem;
						}
					}

					//Customise Meta Data for facebook share
					$description="";
					$reviewWord="";

					if($shopInfo['name']!=""){
						$reviewWord.=$shopInfo['name'];
					}else{
						$reviewWord.=$shopInfo['eng_name'];
					}
					$reviewWord.=' - ';
					$reviewWord.= $shopInfo['reviews'][0]['title'];
					if(isset($_REQUEST['uname'])){
						$username=$_REQUEST['uname'].' ';
					}
					$stitle = '想同你分享「'.$reviewWord.'」呢篇評介，去OutStreet睇下啦！';
					if ($_REQUEST['like']) {$stitle = $reviewWord;}
					$description.= formatInfoUTF8(strip_tags($shopInfo['reviews'][0]['description']),180);

					if(isset($review['pictures'])){	//Setting up thumbnail
						$item["pictures"] = $review['pictures'];
						$this->assign('itemType','review');
					}elseif(isset($shopInfo['pictures'])){
						$item["pictures"] = $shopInfo['pictures'];
						$this->assign('itemType','shop');
					}elseif(isset($shopInfo['reviews'][0]['user'])){
						$item["pictures"] = array(0=>array('file_path'=>getFace($shopInfo['reviews'][0]['user'])));
						$this->assign('itemType','user');
					}else{
						unset($item["pictures"]);
						$this->assign('itemType','none');
					}

					$this->assign('item',$item);
					$this->assign('description',$description);
					$this->assign('stitle',$stitle);
					//End customise Meta Data for facebook share
				}else{	//view shop mode
					//Customise Meta Data for facebook share
					$description="";
					$reviewWord="";

					if($shopInfo['name']!=""){
						$reviewWord.=$shopInfo['name'];
					}else{
						$reviewWord.=$shopInfo['eng_name'];
					}
					if(isset($_REQUEST['uname'])){
						$username=$_REQUEST['uname'].' ';
					}
					$stitle= '想同你分享「'.$reviewWord.'」呢間商戶，去OutStreet睇下啦！';
					if ($_REQUEST['like']) {$stitle = $reviewWord;}
					$description.= formatInfoUTF8(strip_tags($shopInfo['description']),180);

					if(isset($shopInfo['pictures'])){
						$item["pictures"] = $shopInfo['pictures'];
						$this->assign('itemType','shop');
					}else{
						unset($item["pictures"]);
						$this->assign('itemType','none');
					}

					$this->assign('item',$item);
					$this->assign('description',$description);
					$this->assign('stitle',$stitle);
				}


				$this->assign('shopInfo',$shopInfo);
				//$this->display();

				echo $content = $this->fetch();
				file_put_contents( $this->cache_filename, base64_encode( $content ) );
			}else{
				$this->error('沒有找到該商戶，您的請求可能來源於您的收藏夾或其他歷史URL，而此商戶已被刪除');
			}
		}else{
			$this->error('URL非法，缺少要查看的商戶ID');
		}

	}//end view

	public function viewshopnews(){
		$shop_id=$_REQUEST['shopid'];
		$row_id=$_REQUEST['id'];
		$shopnewsDao=D('ShopNews');
		$shopnewsPictureDao=D('ShopNewsPicture');
		$newsdata=$shopnewsDao->find($row_id);
		$newsdata['picture']=$shopnewsPictureDao->where('picstamp='.$newsdata['picstamp'])->select();
		$this->assign('url',C('OUTSTREET_DIR').C('SHOPNEWS_UPFILE_PATH'));
		$this->assign('newsdata',$newsdata);
		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}

	public function selshopcate(){
		//獲取Shop分類主目錄列表 壓入模板
		$cate = M('ShopCate')->getField('id,name');
		$level_1 = M('ShopCate')->where(array('level'=>1))->getField('id,parent_id');
		$level_2 = M('ShopCate')->where(array('level'=>2))->getField('id,parent_id');
		$level_3 = M('ShopCate')->where(array('level'=>3))->getField('id,parent_id');

		$cateList = array();
		foreach ( $level_1 AS $level_1_id => $level_1_parent_id )
		{
			if ($cate[ $level_1_id ])
				$cateList[ $level_1_id ][ 'name' ] = $cate[ $level_1_id ];
		}
		foreach ( $level_2 AS $level_2_id => $level_2_parent_id )
		{
			if ($cateList[$level_2_parent_id][ 'name' ] && $cate[ $level_2_id ])
				$cateList[$level_2_parent_id]['item'][ $level_2_id ][ 'name' ] = $cate[ $level_2_id ];
		}
		foreach ( $level_3 AS $level_3_id => $level_3_parent_id )
		{
			if ($cateList[$level_2[$level_3_parent_id]][ 'name' ] && $cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['name'] && $cate[ $level_3_id ])
				$cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['item'][ $level_3_id ][ 'name' ] = $cate[ $level_3_id ];
		}

		//Shop Cate Ref List
		$shopCateList = D('ShopCate')->field('id,reference_id')->select();
		$refList = array();
		foreach($shopCateList as $key=>$shopCate){
			$cateId = $shopCate['id'];
			$refId = $shopCate['reference_id'];
			if($refId!=0){
				$refList[$cateId][] = $refId;
				$refList[$refId][] = $cateId;
			}
		}
		foreach($refList as $key=>$ref){
			foreach($ref as $cateId){
				foreach($refList[$cateId] as $elem){
					$refList[$key][] = $elem;
				}
			}
			$refList[$key] = array_unique($refList[$key]);
		}

		$this->assign('refList', $refList);
		$this->assign('cateList', $cateList);
		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}

	public function sellocation(){
		//獲取Shop分類主目錄列表 壓入模板
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');
		$locationMainList = $locationMainDao->select();

		foreach($locationMainList as &$locationMain){
			$where = array();
			$where['main_location_id'] = $locationMain['id'];
			$locationMain['locationSubList'] = $locationSubDao->where($where)->select();
		}

		$this->assign('locationMainList', $locationMainList);
		header('Cache-Control: max-age=86400');
		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}

	public function uploadPic(){
		C('SHOW_PAGE_TRACE',false);
		$this->display();
	}

	public function uploadResult(){
		C('SHOW_PAGE_TRACE',false);

		if (isset($_REQUEST['submitBtn'])){

			$eleid = $_REQUEST['eleid'];
			$picstamp = $_REQUEST['picstamp'];
			$description = $_REQUEST['description'];
			$shopid = $_REQUEST['shopid'];
			$from = $_REQUEST['from'];
			$reviewPictureDao = D('ReviewPicture');

			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));

			switch(strtoupper($from)){
				case 'REVIEW':
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('REVIEW_UPFILE_PATH').$shopid.'/';
					break;
				case 'REQUEST':
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH');
					break;
				default:
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH');
			}

			$upload->saveRule = 'time';
			$upload->thumb = true;
			$upload->thumbMaxWidth = 120;
			$upload->thumbMaxHeight = 120;
			$upload->uploadReplace = true;

			if($upload->upload()) {
				$upinfo = $upload->getUploadFileInfo();
				$upfilepath = $upinfo[0]['savename'];

				$dao = null;
				switch(strtoupper($from)){
					case 'REVIEW':
						$dao = D('ReviewPicture');
						break;
					case 'REQUEST':
						$dao = D('ShopTmpPicture');
						break;
					default:
						$dao = D('ShopPicture');
				}

				$upfile = $dao->create();

				$upfile['file_path'] = $upfilepath;
				$upfile['description'] = $description;
				$upfile['picstamp'] = $picstamp;
				switch(strtoupper($from)){
					case 'REVIEW':
						//$upfile['shop_id'] = $shopid;
					break;
					case 'REQUEST':
						$upfile['create_time'] = time();
					break;
				}

				Import('@.ORG.SimpleAcl');
				$userInfo = SimpleAcl::getLoginInfo();
				$upfile['user_id'] = $userInfo['id'];

				$picid = $dao->add($upfile);

				if($from=='shop'){
					$path = C('OUTSTREET_DIR').C('SHOP_UPFILE_PATH');
				}else if($from=='request'){
					$path = C('OUTSTREET_DIR').C('REQUEST_SHOP_UPFILE_PATH');
				}else if($from=='review'){
					$path = C('OUTSTREET_DIR').C('REVIEW_UPFILE_PATH').$shopid;
				}

				$this->assign('eleid', $eleid);
				$this->assign('upfilepath', $upfilepath);
				$this->assign('description', $description);
				$this->assign('picid', $picid);
				$this->assign('path', $path);
				$this->assign('shopid', $shopid);
				$this->assign('from', $from);
				$this->assign('picstamp', $picstamp);
				$this->display('uploadResult');
			} else{
				$upfileerr = $upload->getErrorMsg();
				$upfilepath = '';
				echo '<script text="text/javascript">';
				echo 'parent.setPic("'.$eleid.'", "'.$upfilepath.'", "'.$upfileerr.'","","","'.__URL__.'","'.$from.'","'.$picstamp.'","'.$shopid.'");';
				echo 'alert("'.$upfileerr.'");';
				echo '</script>';
			}
		}

	}

	public function deletePic(){
		$id = $_REQUEST['id'];
		$from = $_REQUEST['from'];

		Import('@.ORG.SimpleAcl');
		$userInfo = SimpleAcl::getLoginInfo();

		$where = array();
		$where['user_id'] = $userInfo['id'];
		$where['id'] = $id;

		switch(strtoupper($from)){
			case 'REVIEW':
				$dao = D('ReviewPicture');
				break;
			default:
				$dao = D('ShopPicture');
		}

		$picList = $dao->where($where)->select();
		switch(strtoupper($from)){
			case 'REVIEW':
				foreach($picList as $pic){
					unlink(C('OUTSTREET_ROOT').'/'.C('REVIEW_UPFILE_PATH').$pic['shop_id'].'/'.$pic['file_path']);
					unlink(C('OUTSTREET_ROOT').'/'.C('REVIEW_UPFILE_PATH').$pic['shop_id'].'/thumb_'.$pic['file_path']);
				}
				break;
			default:
				foreach($picList as $pic){
					unlink(C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH').$pic['file_path']);
					unlink(C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH').'thumb_' . $pic['file_path']);
				}
		}

		$dao->where($where)->delete();
		$this->display();
	}

	public function maps(){

		$id = $_REQUEST['shopid'];
		$cate_id = $_REQUEST['cate_id'];

		$shopDao = D('Shop');
		$mainLocationDao = D('LocationMain');
		$subLocationDao = D('LocationSub');

		$shopInfo = $shopDao->find($id);
		$mainLoc = $mainLocationDao->find($shopInfo['main_location_id']);
		$subLoc = $subLocationDao->find($shopInfo['sub_location_id']);
		$shopInfo['main_location_name']=$mainLoc['name'];
		$shopInfo['sub_location_name']=$subLoc['name'];

		//獲取Shop分類主目錄列表 壓入模板
		while( $cate_id ) {
			$cate = M('ShopCate')->find($cate_id);
			$cate_list[] = $cate;
			$cate_id = $cate['parent_id'];
		}
		$i=0;
		foreach ($cate_list AS $val) {
			$shopCateList[$i] = M('ShopCate')->where(array('parent_id'=>$val['parent_id']))->select();
			$i++;
		}

		$this->assign('cate_list', array_reverse($cate_list));
		$this->assign('shopCateList', array_reverse($shopCateList));
		$this->assign('shopInfo',$shopInfo);
		//$this->display();

		echo $content = $this->fetch();
		file_put_contents( $this->cache_filename, base64_encode( $content ) );
	}

	private function getAllSonList($shop_cate_id){
		$ShopCateDao = D('ShopCate');
		$where = array();
		$where['parent_id'] = array('IN',$shop_cate_id);
		$shopCateList = $ShopCateDao->where($where)->select();

		foreach($shopCateList as $elem){
			array_push($shop_cate_id, $elem['id']);
			$this->getAllSonList($elem['id']);
		}
		return $shop_cate_id;
	}

	private function mapShopCateRef($shopcate){
		//mapping for shopcate reference id
		foreach($shopcate AS $key=>$cateId){
			$referenceId = D('ShopCate')->where("id=$cateId")->getfield('reference_id');
			if($referenceId!=0){
				$shopcate[$key] = $referenceId;
			}
		}
		return array_unique($shopcate);
	}

	public function selectedShop($type, $shop_id, $sub_cates, $sub_location_id) {
		$shopDao = D('Shop');

		foreach($sub_cates as $vo){
			$cate_id[] = $vo['id'];
		}

		switch($type) {
			case 1: //Similar Shop
				$where = array();
				$where['shop_fields.sub_location_id'] = $sub_location_id;
				$where['shop_cate_map.cate_id'] = array('in',$cate_id);
				$where['shop_fields.is_audit'] = 1;
				$where['shop_fields.id'] = array('neq', $shop_id);

				$shopIdArray = $shopDao
					->field('shop_fields.*')
					->join('shop_cate_map on shop_fields.id = shop_cate_map.shop_id' )
					->join('statistics_shop ON shop_fields.id = statistics_shop.shop_id')
					->where($where)
					->order('shop_fields.shop_level DESC, RAND()')
					->group('shop_fields.id')
					->limit(3)
					->select();

				foreach($shopIdArray as $val) {
					$selectedShopArray[] = $shopDao->relation(true)->find($val['id']);
				}
			break;
		}

		return $selectedShopArray;
	}

	public function shop_provide_info(){
		$id = $_REQUEST['shop_id'];
		$shopDao = D('Shop');
		$shopInfo = $shopDao->relation(true)->find($id);
		$shopInfo['pictures']=getShopDetail($shopInfo,4);
		$this->assign('shopInfo', $shopInfo);
		$this->assign('id', $id);
		$this->display();
	}

	public function request_add_shop(){
		//************************** Start Get Information From Form **************************//
		$shopInputFields = array();
		$shopInputFields = $this->getRequestValue($_REQUEST,'Shop');
		$keyword 	= trim($shopInputFields['keyword']);
		$cate_ids 	= explode(',',$_REQUEST['cate_ids']);
		//**************************** End Get Information From Form **************************//

		$shopDao = D('ShopTmpFields');

		if (isset($_REQUEST['submit'])){
			//********************************** Start Checking **********************************//
			if(!$this->chkshopkeyword($keyword)) { $formChkError[] = $this->chkshopkeyword($keyword); }
			//********************************** End Checking ************************************//

			if(count($formChkError) == 0) {
				$shopInfo = $shopDao->create();

				if($shopInfo){
					$shopInfoArray = array('parent_shop_id','brand_id','main_location_id','sub_location_id','name','eng_name','address','rooms','eng_address','keyword','brand_id','parking','description','telephone','sub_telephone','website','email','googlemap_lng','googlemap_lat','eng_rooms','eng_description','eng_parking','eng_keyword');

					if($_REQUEST['is_closed']){
						$shopInfo['shop_status']= '2';
					}else if($_REQUEST['is_renovation']){
						$shopInfo['shop_status']= '3';
					}else if($_REQUEST['is_moved']){
						$shopInfo['shop_status']= '1';
					}else{
						$shopInfo['shop_status']= '0';
					}


					$shopInfo['create_time']	= time();
					$shopInfo['status']='add';

					if ($newid = $shopDao->add($shopInfo)){
						$shopInfo	 = array();
						$shopInfo	 = $this->processRequestValue($shopInputFields,$shopInfoArray);
						$where['id'] = $newid;
						$shopDao->where($where)->save($shopInfo);
						$this->saveWorkTime(0,$_REQUEST,'ShopTmpTime',$newid);
						$this->saveCate(0,$cate_ids,'ShopTmpCateMap',$newid);

						$return = array();
						$return['status'] 	= true;
						/*if($_REQUEST['owner']=='owner'){
							//$return['url'] =__URL__."/shop_provide_info/shoppage/4/shop_tmp_fields_id/".$shop_tmp_fields_id;
							$return['url'] =__URL__."/request_own_shop/shop_tmp_fields_id/".$newid;
							$return['message'] ='多謝你提交新商戶資料！我們在核對資料之後，會及時將新商戶發布到網站上，並以email通知你。多謝你的支持！現在請到我是店主進行登記！';
						}else{
							$return['url'] =__ROOT__;
							$return['message'] ='多謝你提交新商戶資料！我們在核對資料之後，會及時將新商戶發布到網站上，並以email通知你。多謝你的支持！';
						}*/
						$return['url'] =__URL__;
						if($_REQUEST['is_owner']){
							$return['message'] ='閣下的店舖資料將於兩個工作天內完成核對程序，OutStreet 會有專人聯絡跟進。感謝您的支持和耐心等候！';
						} else {
							$return['message'] ='感謝您提供的資料！我們將盡快更新此資訊！';
						}
						echo json_encode($return);
					}else{
						$return = array();
						$return['status'] 	= false;
						$return['message'] = '未知錯誤，請聯繫管理員！';
						echo json_encode($return);
					}
				}else{
					$return = array();
					$return['status'] = false;
					$return['message'] = '數據驗證失敗';
					echo json_encode($return);
				}
			} else {
				$return = array();
				$return['status'] = false;
				$return['keywordWarning'] = $formChkError[0];
				echo json_encode($return);
			}
		} else {
			$brandDao = D('Brand');
			$mainLocationDao = D('LocationMain');

			//獲取Brand列表 壓入模
			$brandList = $brandDao->select();
			$this->assign('brandList', $brandList);

			//獲取Location分類主目錄列表 壓入模板
			$mainLocationList = $mainLocationDao->select();
			$this->assign('mainLocationList', $mainLocationList);

			$common_shop_form = $this->fetch('common_shop_form');
			$this->assign('commonShopForm',$common_shop_form);
			$this->display();
		}
	}

	public function request_edit_shop(){
		//************************** Start Get Information From Form **************************//
		$shopInputFields = array();
		$shopInputFields = $this->getRequestValue($_REQUEST,'Shop');
		$id 		= $shopInputFields['id'];
		$keyword 	= trim($shopInputFields['keyword']);
		$cate_ids 	= explode(',',$_REQUEST['cate_ids']);
		//**************************** End Get Information From Form **************************//

		//********************************** Start Check If The Shop Exist **********************************//
		$shopDao = D('Shop');
		$shopTmpDao = D('ShopTmpFields');
		$checkInfo = $shopDao->relation(true)->find($id);

		if(isset($id)){
			if(isset($checkInfo) && $checkInfo['is_audit']==1){
				if (isset($_REQUEST['submit'])){
					import ( '@.ORG.SimpleAcl' );
					$userInfo =  SimpleAcl::getLoginInfo();
					$data = $_REQUEST;

					//********************************** Start Checking **********************************//
					if(!$this->chkshopkeyword($keyword)) { $formChkError[] = $this->chkshopkeyword($keyword); }
					//********************************** End Checking **********************************//

					if(count($formChkError) == 0) {
						$shopCompareArray = array(
							'brand_id' => 'int',
							'parent_shop_id' => 'int',
							'main_location_id'=>'int',
							'sub_location_id' => 'int',
							'name' => 'string',
							'eng_name'=>'string',
							'address' => 'string',
							'rooms' => 'string',
							'googlemap_lat' => 'string',
							'googlemap_lng' => 'string',
							'eng_address'=>'string',
							'parking' => 'string',
							'description' => 'string',
							'telephone' => 'int',
							'sub_telephone' => 'int',
							'email' => 'string',
							'keyword'=>'string',
							'eng_rooms'=>'string',
							'eng_description'=>'string',
							'eng_parking'=>'string',
							'eng_keyword'=>'string');

						$shop = M('ShopFields')->where( array( 'id' => $id ) )->find();

						$savedata = array();
						foreach( $shopCompareArray as $name => $type ) {
							settype( $data[$name], $type );
							settype( $shop[$name], $type );
							if ( trim(preg_replace('/\n|\r/','',html_entity_decode($data[$name]))) != trim(preg_replace('/\n|\r/','',html_entity_decode($shop[$name]))) )
								{ $savedata[$name] = $data[$name]; }
						}
						if($_REQUEST['website_check']==1){//Check if allow website modi
							if($shop['website'] != $_REQUEST['website']){
								$savedata['website'] = $_REQUEST['website'];
							}
						}
						if($_REQUEST['is_closed']){
							$status= 2;
						}else if($_REQUEST['is_renovation']){
							$status= 3;
						}else if($_REQUEST['is_moved']){
							$status= 1;
						}else{
							$status= 0;
						}
						if($data['shop_status'] != $status){
							$savedata['shop_status']	= $status;
						}
						if($savedata['parent_shop_id']||$savedata['address']||$savedata['rooms']||$savedata['main_location_id']||$savedata['sub_location_id']||$savedata['googlemap_lat']||$savedata['googlemap_lng']||$savedata['eng_address']||$savedata['eng_rooms']){
							 $savedata['parent_shop_id']	= 	$data['parent_shop_id'];
							 $savedata['address']			= 	$data['address'];
							 $savedata['rooms']				= 	$data['rooms'];
							 $savedata['main_location_id']	= 	$data['main_location_id'];
							 $savedata['sub_location_id']	= 	$data['sub_location_id'];
							 $savedata['googlemap_lat']		=	$data['googlemap_lat'];
							 $savedata['googlemap_lng']		=	$data['googlemap_lng'];
							 $savedata['eng_address']		=	$data['eng_address'];
							 $savedata['eng_rooms']			=	$data['eng_rooms'];
						}

						$affectedFields = array_keys($savedata);

						$savedata['shop_id'] 		= $id;
						$savedata['create_time']	= time();

						if(!empty($userInfo)){
							$savedata['user_id'] 		= $userInfo[ 'id' ];
						}

						$savedata['status'] 		= 'edit';

						$shopTime = M('ShopTime')->where( array( 'shop_id' => $id ) )->select();
						$shopTimeKey = array();
						foreach( $shopTime as $val ) { $shopTimeKey[] = $val['start_time'] . ':' . $val['end_time'] . ':' . $val['recurring_week']; }

						$shopTimeInputKey = array();
						for( $i=0; $i<8; $i++ )
						{
							if ( @$_REQUEST['work_dayoff_'.$i] != 1 &&  @$_REQUEST['work_start_time_'.$i] && @$_REQUEST['work_end_time_'.$i] )
								{ $shopTimeInputKey[] = $_REQUEST['work_start_time_'.$i] . ':' . $_REQUEST['work_end_time_'.$i] . ':' . ($i+1);  }
						}
						if ( count($shopTimeKey) >= count($shopTimeInputKey) && count(array_diff( $shopTimeKey, $shopTimeInputKey )) )
							{ $affectedFields[] = 'shoptime'; }
						elseif ( count($shopTimeKey) < count($shopTimeInputKey) && count(array_diff( $shopTimeInputKey, $shopTimeKey )) )
							{ $affectedFields[] = 'shoptime'; }

						foreach($cate_ids as $key=>$val){
							$where=array();
							$where['id']=$val;
							$where['reference_id']=array('neq','');
							if(D('ShopCate')->where($where)->count()){
								$new_cate_id=D('ShopCate')->where('id='.$val)->getField('reference_id');
								if(!in_array($new_cate_id,$new_cate_ids)){
									$new_cate_ids[]=$new_cate_id;
								}
							}else{
								$new_cate_ids[]=$val;
							}
							unset($where);
						}
						$cate_ids=$new_cate_ids;
						$shopCate = M('ShopCateMap')->where( array( 'shop_id' => $id ) )->select();
						$shopCateId = array();
						foreach( $shopCate AS $val ) { $shopCateId[] = $val['cate_id']; }
						if ( count($shopCateId) >= count($cate_ids) && count(array_diff( $shopCateId, $cate_ids )) )
							{ $affectedFields[] = 'shopcate'; }
						elseif ( count($shopCateId) < count($cate_ids) && count(array_diff( $cate_ids, $shopCateId )) )
							{ $affectedFields[] = 'shopcate'; }
						if ( count($affectedFields) ) {
							$savedata[ 'affectedFields' ] = serialize($affectedFields);
							if($shopTmpDao->add($savedata)){
								$shop_tmp_fields_id = $shopDao->getLastInsID();

								if ( in_array('shoptime',$affectedFields) ) {
									$this->saveWorkTime($id,$_REQUEST,'ShopTmpTime',$shop_tmp_fields_id);
								}

								if ( in_array('shopcate',$affectedFields) ) {
									$this->saveCate($id,$cate_ids,'ShopTmpCateMap',$shop_tmp_fields_id);
								}
							}
						}

						$return = array();
						$return['status'] 	= true;
						$return['message'] 	= '多謝你提交商戶最新資料！我們在核對資料之後，將會及時更新網站內容，並以email通知你。多謝你的支持！';
						$return['url'] 		= __ROOT__."/shop/view/id/".$id;
						echo json_encode($return);
					} else {
						$return = array();
						$return['status'] = false;
						$return['keywordWarning'] = $formChkError[0];
						echo json_encode($return);
					}
				}else{
					$id = $_REQUEST['id'];
					$shopDao = D('Shop');

					$shopInfo = $shopDao->relation(true)->find($id);
					$this->assign('shopInfo', $shopInfo);

					$newShopTime = array();
					$shopTime = M('ShopTime')->where( array( 'shop_id' => $id ) )->select();
					foreach ( $shopTime as $val ){
						$newShopTime[$val['recurring_week']] = $val;
					}
					$this->assign('shopTime', $newShopTime);

					$brandDao = D('Brand');
					$mainLocationDao = D('LocationMain');
					$subLocationDao = D('LocationSub');

					//獲取Brand列表 壓入模板
					$brandList = $brandDao->select();
					$this->assign('brandList', $brandList);

					//獲取Location分類主目錄列表 壓入模板
					$mainLocationList = $mainLocationDao->select();
					$this->assign('mainLocationList', $mainLocationList);
					$subLocationList = $subLocationDao->select();
					$this->assign('subLocationList', $subLocationList);

					$common_shop_form = $this->fetch('common_shop_form');
					$this->assign('commonShopForm',$common_shop_form);
					$this->display();
				}
			} else {
				echo '<script>alert("沒有找到該商戶，您的請求可能來源於您的收藏夾或其他歷史URL，而此商戶已被刪除");</script>';
				echo '<script>parent.tb_remove();</script>';
			}
		} else {
			echo '<script>alert("URL非法，缺少要查看的商戶ID");</script>';
			echo '<script>parent.tb_remove();</script>';
		}
		//********************************** End Check If The Shop Exist **********************************//
	}

	public function request_provide_photo(){
		$id = $_REQUEST['id'];
		$shopDao = D('Shop');
		$shopInfo = $shopDao->relation(true)->find($id);
		$this->assign('shopInfo', $shopInfo);
		$this->assign('picstamp', time() . rand(1000,9999));
		$this->assign('id', $id);
		$this->display();
	}

	public function provide_photo(){
		$id = $_REQUEST['id'];
		$shopDao = D('Shop');
		$shopInfo = $shopDao->relation(true)->find($id);
		$this->assign('shopInfo', $shopInfo);
		$this->assign('picstamp', time() . rand(1000,9999));
		$this->assign('id', $id);
		$this->display();
	}

	public function request_submit_provide_photo(){
		$shop_id = $_REQUEST['id'];
		$picstamp = $_REQUEST['picstamp'];

		$shopPictureDao = D('ShopTmpPicture');
		$picstamp_count = $shopPictureDao->where("picstamp=$picstamp")->count();
		if($picstamp_count==0){
			echo json_encode( array( 'success'=> 0,'message'=>'請先上載相片 然後點擊提交' ) );
		}else{

			if($_REQUEST['confirm']=="confirm"){

				$userInfo = SimpleAcl::getLoginInfo();

				if($userInfo){
					$shopPictureDao->where("picstamp=$picstamp")->setField('user_id', $userInfo['id']);
					$shopPictureDao->where("picstamp=$picstamp")->setField('shop_id', $shop_id);

					$return = array();
					$return['success'] =1;
					$return['message'] ='多謝你提供商戶相片！我們審核相片後會及時更新網站內容，並以email通知你。OutStreet的進一步完善，有賴你的支持！';
					$return['url'] =__ROOT__."/shop/view/id/".$shop_id;
					echo json_encode($return);

				}else if($_REQUEST['non_member_name'] && $_REQUEST['non_member_email']){
					$shopPictureDao->where("picstamp=$picstamp")->setField('provider_name', $_REQUEST['non_member_name']);
					$shopPictureDao->where("picstamp=$picstamp")->setField('provider_email', $_REQUEST['non_member_email']);
					$shopPictureDao->where("picstamp=$picstamp")->setField('shop_id', $shop_id);

					$return = array();
					$return['success'] =1;
					$return['message'] ='多謝你提供商戶相片！我們審核相片後會及時更新網站內容，並以email通知你。OutStreet的進一步完善，有賴你的支持！';
					$return['url'] =__ROOT__."/shop/view/id/".$shop_id;
					echo json_encode($return);

				}else echo json_encode( array( 'success'=> 0,'message' => '請輸入你的會員E-mail和密碼 或 輸入你的名稱和電郵' ) );

			}else echo json_encode( array( 'success'=> 0,'message'=>'請確定你所提交相片並不含任何侵犯版權之內容!' ) );
		}
	}

	public function request_own_shop(){
		if($_REQUEST['shop_tmp_fields_id']){
			$shop_tmp_fields_id = $_REQUEST['shop_tmp_fields_id'];
		}else{
			$id = $_REQUEST['id'];
		}
		if($_REQUEST['submit']){
			$type = $_REQUEST['type'];
			$contactPerson = $_REQUEST['contactPerson'];
			$email = $_REQUEST['email'];
			$telephone = $_REQUEST['telephone'];
			$position = trim($_REQUEST['post']);
			$company_name = trim($_REQUEST['company_name']);
			$password = md5($_REQUEST['password']);

			$Owner = D('Owner');
			$data = array();
			$return = array();
			$data['o_company_name'] = $company_name;
			$data['o_position'] = $position;
			if(!empty($shop_tmp_fields_id)){
				$data['admin_shop_tmp_id'] = $shop_tmp_fields_id;
			}
			$data['o_contact_person'] = $contactPerson;
			$data['email'] 			= $email;
			$data['o_telephone'] 		= $telephone;
			$data['create_time'] 	= time();
			$data['password'] 	= $password;
			$data['is_disabled'] = 1;

			if($Owner->data($data)->add()){
				//email to Owner
				$caption = '感謝你登記成為店主';
				$this->assign('nickname',$contactPerson);
				$this->assign('caption',$caption);
				$this->assign('link',C('OUTSTREET_DIR') . '/shop/view/id/'.$shop_id);
				$body = $this->fetch('email_request_shop_owner');
				if($email != '') {
					$this->smtp_mail_log($email,$caption,$body,'',2,1);
				}

				//email to admin
				$caption = '登記成為店主記錄';
				$this->assign('shopName',$company_name);
				$this->assign('contactPerson',$contactPerson);
				$this->assign('email',$email);
				$this->assign('telephone',$telephone);
				if($from==0){
					$this->assign('source','商戶');
				}else{
					$this->assign('source','節目');
				}
				$this->assign('caption',$caption);
				$body = $this->fetch('email_request_admin_shop_owner');
				$this->smtp_mail_log('info@outstreet.com.hk',$caption,$body,'',2,1);

				$return['success'] =1;
				if ($type == 1) {
					$return['msg'] = '請在下一頁輸入店鋪資料。(如果你是手機用戶而不便填寫資料，我們會於兩個工作天內致電跟進。)';
					$return['url'] =__ROOT__.'/shop/shop_provide_info/shoppage/1/is_owner/1/';
				}
				else {
					$return['msg'] = '我們的客戶服務專員會於兩個工作天內致電跟進。';
					$return['url'] =__ROOT__;
				}
			}else{
				//Problem while add to db
				$return['success'] =0;
				$return['msg'] = '未知錯誤，請聯繫管理員！';
			}
			echo json_encode($return);
		}else{
			//default Owner info

			/*$userArr = SimpleAcl::getLoginInfo();
			$User = D('User');
			$where = array();
			$where['id'] = $userArr['id'];
			$userInfo = $User->where($where)->find();

			$db_company_name ='';
			$db_contactPerson = '';
			$db_email = '';
			$db_telephone = '';
			if(!empty($userInfo)){
				$db_contactPerson = $userInfo['nick_name'];
				$db_email = $userInfo['email'];
				$db_telephone = $userInfo['telephone'];
			}
			if(!empty($shopInfo)){
				if(trim($shopInfo['name'])!=''){
					$db_company_name = $shopInfo['name'];
				}else{
					$db_company_name = $shopInfo['english_name'];
				}
				if(trim($db_email)=='' && trim($shopInfo['email'])!=''){
					$db_email = $shopInfo['email'];
				}
			}

			$this->assign('company_name', trim($db_company_name));
			$this->assign('contactPerson', trim($db_contactPerson));
			$this->assign('email', trim($db_email));
			$this->assign('telephone', trim($db_telephone));
			*/
			$this->display();
		}
	}
	//************************************************ End Form Function ******************************************//

	//****************************************** Start Share Form Function ****************************************//
	public function chkshopkeyword($keyword){
		if($keyword!=''){
			$keyword_error_type = 0;
			$keywordArr = explode(',',$keyword);
			foreach($keywordArr as $key=>$keyword){
				if(trim($keyword)==""){
					$keyword_error_type = 1;	//Blank keyword
				}elseif(hasSymbol(trim($keyword))){
					$keyword_error_type = 2;	//Invalid Characters
				}
			}

			switch($keyword_error_type){
				case 1:
					return '關鍵字中不能為空!';
				break;
				case 2:
					return '關鍵字中不允許使用符號!';
				break;
			}
		}

		return true;
	}

	public function saveCate($id,$cate_ids,$destination,$shop_tmp_fields_id=''){
		$where = array();
		if($shop_tmp_fields_id != '') {
			$where['shop_tmp_fields_id'] = $shop_tmp_fields_id;
		}else{
			$where['shop_id'] = $id;
		}
		$existCateIds = M($destination)->where($where)->select();

		$cateIdsList = array();
		foreach($existCateIds as $elem){
			array_push($cateIdsList,$elem['cate_id']);
		}
		if($shop_tmp_fields_id == '') {
			generateShopCateStatByShopID($id,'delete');
		}
		M($destination)->where( $where )->delete();

		$cate_ids = $this->mapShopCateRef($cate_ids);
		foreach ( $cate_ids as $cate_id ) {
			$cate_iddsf = $cate_id;
			if($shop_tmp_fields_id != '') {
				M($destination)->data( array( 'cate_id'=>$cate_id, 'shop_id'=>$id, 'shop_tmp_fields_id'=>$shop_tmp_fields_id ) )->add();
			}else{
				M($destination)->data( array( 'cate_id'=>$cate_id, 'shop_id'=>$id ) )->add();
			}
		}
		if($shop_tmp_fields_id == '') {
			generateShopCateStatByShopID($id,'add');
		}
	}

	public function saveWorkTime($id,$requestValue,$destination,$shop_tmp_fields_id=''){
		$has_work_time 	= 0;
		$where = array();
		if($shop_tmp_fields_id != '') {
			$where['shop_tmp_fields_id'] = $shop_tmp_fields_id;
		}else{
			$where['shop_id'] = $id;
		}
		M($destination)->where($where)->delete();
		for( $i=0; $i<8; $i++ ){
			if($requestValue['work_dayoff_'.$i] == 1){
				// Do not enter record
			} else {
				if ($requestValue['work_start_time_'.$i] && $requestValue['work_end_time_'.$i] ) {
					$has_work_time = 1;
					if($shop_tmp_fields_id == '') { //add, edit
						M($destination)->data( array(
							'shop_id' => $id,
							'start_time' => $requestValue['work_start_time_'.$i],
							'end_time' => $requestValue['work_end_time_'.$i],
							'recurring_week' => $i+1 ) )->add();
					}else{
						M($destination)->data( array(//request add, request edit
							'shop_id' => $id,
							'shop_tmp_fields_id' => $shop_tmp_fields_id,
							'start_time' => $requestValue['work_start_time_'.$i],
							'end_time' => $requestValue['work_end_time_'.$i],
							'recurring_week' => $i+1 ) )->add();
					}
				}
			}
		}

		$shopDao = D('Shop');
		$shopInfo['work_time']		= $has_work_time;
		$shopDao->where(array( 'id'=>$id ))->save($shopInfo);
	}

	public function getRequestValue($requestValue, $daoName){
		Import('ORG.Util.Input');
		$Dao = D($daoName);
		$DaoFields = $Dao->getDbFields();

		foreach($DaoFields as $key) {
			$returnValue[$key] = Input::getVar($requestValue[$key]);
		}

		return $returnValue;
	}

	public function processRequestValue($shopInputFields,$shopInfoArray) {
		foreach($shopInfoArray as $key) {
			$returnValue[$key] = $shopInputFields[$key];
		}

		return $returnValue;
	}

	public function shop_error(){
		$error = $_REQUEST['error'];
		$this->assign('error', $error);
		$this->display();
	}
	//****************************************** End Share Form Function ******************************************//

}

