<?php

class PosterAction extends BaseAction {

	public function view(){

		$posterId = $_REQUEST['posterid'];
		
		$posterDao = D('Poster');
		$posterblockDao = M('PosterBlock');

		$count = $posterDao->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		
		$where=array();
		$where['status'] = 1;

		$posterList = $posterDao->where($where)->limit($limit)->order('orders desc, id desc')->select();
		$posterblocks = $posterblockDao->where(array('poster_id'=>$posterId))->order('orders')->select();
		$posterInfo = $posterDao->where($where)->relation(true)->find($posterId);
		
		//meta data
		$stitle = 'OutStreet 推介 - '.strip_tags($posterInfo['title']);
		$description = strip_tags($posterInfo['lead']);
		$this->assign('stitle',$stitle);
		$this->assign('description',$description);		
		
		$this->assign('multipage', $multipage);
		$this->assign('posterList', $posterList);
		$this->assign('posterblocks', $posterblocks);
		
		$this->assign('posterInfo',$posterInfo);
		$this->assign('itemType','poster');
		$this->display();

	}
	
	public function listAll(){
		
		$posterDao = D('Poster');

		$count = $posterDao->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		
		$where=array();
		$where['status'] = 1;
		
		$posterList = $posterDao->where($where)->limit($limit)->order('orders desc, id desc')->select();

		$this->assign('multipage', $multipage);
		$this->assign('posterList', $posterList);

		$this->display();
	
	}

}


