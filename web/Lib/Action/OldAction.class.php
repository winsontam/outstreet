<?php

class OldAction extends BaseAction{

	public function index(){

		//header( 'Location:http://www.outstreethk.com/');

		//***************************************** Start To latest news ***********************************************//
		$news = M('news')->order('create_time desc')->limit(2)->select();
		$this->assign('news', $news);
		//******************************************* End Of latest news ***********************************************//

		//***************************************** Start To Get Shop's Main Category ***********************************************//
		$shopCateDao = D('ShopCate');
		$index_shop_cates=explode(',',C('INDEX_SHOP_CATES'));

		$cates = array();
		$tmpCates = $shopCateDao->where( array('id' => array('in',$index_shop_cates), 'level' => 1 ,'type'=>array('neq','iphone')))->select();
		foreach ($tmpCates AS $tmpCate) { $cates[$tmpCate['id']] = $tmpCate; }

		$shopCateListlv1 = array();
		foreach ($index_shop_cates AS $cate_id) { $shopCateListlv1[] = $cates[$cate_id]; }

		foreach($shopCateListlv1 as $key=>$val){
			//$shopCateListlv1[$key]['has_level2']=$shopCateDao->where('parent_id='.$shopCateListlv1[$key]['id'])->order('shop_count desc')->select();
			/*foreach($shopCateListlv1[$key]['has_level2'] as $subkey=>$subval){
				$shopCateListlv1[$key]['has_level2'][$subkey]['has_level3']=$shopCateDao->where('parent_id='.$shopCateListlv1[$key]['has_level2'][$subkey]['id'])->order('shop_count desc')->select();
			}*/
			if(C('SUB_CATE_ORDER_'.$shopCateListlv1[$key]['id'])){
				$cates = array();
				$tmpCates = array();
				$tmpCates0 = array();
				$tmpCates0=$shopCateDao->where('parent_id='.$shopCateListlv1[$key]['id'])->select();
				$sub_cate_order=explode(',',C('SUB_CATE_ORDER_'.$shopCateListlv1[$key]['id']));
				foreach($tmpCates0 as $key1=>$val1){
					$tmpCates[$val1['id']]=$tmpCates0[$key1];
				}
				foreach($sub_cate_order as $key2=>$val2){
					$shopCateListlv1[$key]['has_level2'][$key2]=$tmpCates[$val2];
				}
			}
		}
		$this->assign('shopCateListlv1', $shopCateListlv1);
		//******************************************* End Of Get Shop's Main Category ***********************************************//

		//***************************************** Start To Get Event's Main Category ***********************************************//
		$eventCateDao = D('EventCate');
		$index_event_cates=explode(',',C('INDEX_EVENT_CATES'));

		$cates = array();
		$tmpCates = $eventCateDao->where( array('id' => array('in',$index_event_cates), 'level' => 1,'type'=>array('neq','mobile')) )->select();
		foreach ($tmpCates AS $tmpCate) { $cates[$tmpCate['id']] = $tmpCate; }

		$eventCateListlv1 = array();
		foreach ($index_event_cates AS $cate_id) { $eventCateListlv1[] = $cates[$cate_id]; }

		foreach($eventCateListlv1 as $key=>$val){
			$eventCateListlv1[$key]['has_level2']=$eventCateDao->where(array('parent_id'=>$eventCateListlv1[$key]['id'],'type'=>array('neq','mobile')))->order('event_count desc')->select();
			foreach($eventCateListlv1[$key]['has_level2'] as $subkey=>$subval){
				$eventCateListlv1[$key]['has_level2'][$subkey]['has_level3']=$eventCateDao->where('parent_id='.$eventCateListlv1[$key]['has_level2'][$subkey]['id'])->order('event_count desc')->select();
			}
		}
		$this->assign('eventCateListlv1', $eventCateListlv1);
		//******************************************* End Of Get Shop's Main Category ***********************************************//

		//***************************************** Start To Get Latest Shop *************************************************//
		$shopDao = D('Shop');
		$shopspecialDao=D('ShopSpecial');
		$where=array();
		$where['id']=$shopspecialDao->where('type=1')->getField('shop_id');
		$shopLatestList = $shopDao->where($where)->relation(true)->find();
		$shopLatestList['pictures']=getShopDetail($shopLatestList,4);
		$this->assign('shopLatestList', $shopLatestList);
		//******************************************* End Of Get Latest Shop *************************************************//

		//***************************************** Start To Get Event's List ***********************************************//
		$eventDao = D('Event');
		$where=array();
		$where['is_audit'] 	 = 1;
		//$where['start_time'] = array(gt, time());
		$where['is_recommend_new'] = array(gt, 0);

		$eventList = $eventDao->where($where)->relation(true)->order('is_recommend_new asc', 'start_time asc', 'cate_id')->limit(4)->select();
		$this->assign('eventList', $eventList);
		//******************************************* End Of Get Event's List ***********************************************//

		//***************************************** Start To Get Recommended Event's List ****************************************//
		$where = array();
		$where['is_audit'] 	 = 1;
		$where['is_recommend'] = array(gt, 0);
		$recommendEventList = $eventDao->where($where)->relation(true)->order('is_recommend asc', 'id desc')->limit(5)->select();
		$this->assign('recommendEventList', $recommendEventList);
		//******************************************* End Of Get Recommended Event's List ****************************************//

		//************************************** Start To Get Outstreet's Recommend *************************************************//
		$posterDao = D('Poster');
		$where=array();
		$where['status'] = 1;

		$posterList = $posterDao->where($where)->relation(true)->order('orders desc, id desc')->limit(5)->select();
		$this->assign('posterList',$posterList);
		//*************************************** End Of Get Outstreet's Recommend *************************************************//

		//********************************************* Start Of Get Remark **************************************************//
		$search_shop = true;
		$search_event = true;

		$where = 'where 1=1 and is_audit = 1';

		$countSql = "select count(id) as count from review_fields as temp $where";
		$model = new Model();
		$ret = $model->query($countSql);

		$count = $ret[0]['count'];
		import('ORG.Util.Page');
		$p=new Page($count);
		$multiPage=$p->show();
		$limit=$p->firstRow.','.$p->listRows;

		$selectSql = "select * from review_fields as temp $where order by create_time desc limit 4";
		$reviewList = $model->query($selectSql);

		$userDao = D('User');
		$eventDao = D('Event');
		$promotionDao = D('Promotion');

		foreach($reviewList as &$review){
			$review['user'] = $userDao->find($review['user_id']);

			if($review['shop_id']!=0){
				$review['for_type'] = 'shop';
				$review['shop'] = $shopDao->relation(true)->find($review['shop_id']);
			}elseif($review['event_id']!=0){
				$review['for_type'] = 'event';
				$review['event'] = $eventDao->relation(true)->find($review['event_id']);
			}elseif($review['promotion_id']!=0){
				$review['for_type'] = 'promotion';
				$review['promotion'] = $promotionDao->relation(true)->find($review['promotion_id']);
			}
		}

		$this->assign("multiPage",$multiPage);
		$this->assign('reviewList',$reviewList);
		//********************************************* End Of Get Remark **************************************************//

		//********************************************* Start Of Promotion **************************************************//

		$promotiondbo = D('Promotion');
		$type1specialpromotion = $promotiondbo->field('promotion_fields.*')
			->relation(true)->where(array('type1'=>1,'is_special'=>1,'is_audit'=>1))->order('is_ended asc, promotion_fields.create_time desc, promotion_fields.id desc')->limit(3)->select();
		$type2specialpromotion = $promotiondbo->field('promotion_fields.*')
			->relation(true)->where(array('type1'=>2,'is_special'=>1,'is_audit'=>1))->order('is_ended asc, promotion_fields.create_time desc, promotion_fields.id desc')->limit(3)->select();

		$sublocation = array();
		$location = M('location_sub')->select();
		foreach( $location AS $val ) { $sublocation[$val['id']] = $val; }

		$this->assign( 'type1specialpromotion', $type1specialpromotion );
		$this->assign( 'type2specialpromotion', $type2specialpromotion );
		$this->assign( 'sublocation', $sublocation );
		//********************************************* End  Of Promotion **************************************************//

		//$this->display();

		echo $content = $this->fetch();
		//file_put_contents( $this->cache_filename, base64_encode( $content ) );

	}

	public function findAllcommend(){
		echo "?周?薦";
	}

}

