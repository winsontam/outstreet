/*Initiation*/
var map = null;
var geocoder = null;

$("#address").keypress(function(e) {
  if (e.which == 13) {
    return false;
  }
});
/*End initiation*/

/*Validation*/
function getformValidateCriteria(formName, path,thick){

	if (typeof(description_len) == "undefined") { description_len = 20; }
	if (typeof(keyword_num) == "undefined") { keyword_num = 3; }

	var formValidateCriteria = new Array();
	formValidateCriteria = {
		event: "blur",
		rules: {
			cate_ids: "required",
			location_id: "required",
			name: "required",
			address: {
				required: true,
				minlength: 2
			},
			telephone: {
				minlength: 8,
				number: true
			},
			sub_telephone: {
				minlength: 8,
				number: true
			},
			description:{
				maxlength:description_len
			},
			eng_description:{
				maxlength:description_len
			},
			parking:{
				maxlength: 100
			},
			email: {
				email: true
			},
			website:{
				url: true
			},
			non_member_email: {
				email: true
			}
		},
		messages: {
			cate_ids: "請選擇至少一個所屬類別",
			location_id: "請選擇商戶所在商圈",
			name:"請輸入名稱",
			address: {
				required: "請輸入地址",
				minlength: "地址不能少於2個字符"
			},
			telephone:{
				minlength: "請輸入8位電話號碼",
				number: "電話號碼必需為數字"
			},
			sub_telephone:{
				minlength: "請輸入8位電話號碼",
				number: "電話號碼必需為數字"
			},
			description: {				
				maxlength: "商戶介紹不能大於"+description_len+"個字符"
			},
			eng_description: {
				maxlength: "商戶介紹(英文)不能大於"+description_len+"個字符"
			},
			parking: {
				maxlength: "最多只能輸入100個字符"
			},
			email: "請輸入一個正確的電郵地址",
			website:"請輸入合法的鏈接",
			non_member_email: {
				email: "請輸入一個正確的E-mail"
			}
		},
		submitHandler: function(frm){
			$('#keywordWarning').css('color','#999');
			$('#engKeywordWarning').css('color','#999');
			
			var keywordsNum = 0;
			var keywords = $(frm).find('[name=keyword]').val().split(',');
			for (var i=0; i<keywords.length; i++){
				if (keywords[i]) { keywordsNum++; }
			}
			
			var engKeywordsNum = 0;
			var engKeywords = $(frm).find('[name=eng_keyword]').val().split(',');
			for (var i=0; i<engKeywords.length; i++){
				if (engKeywords[i]) { engKeywordsNum++; }
			}

			workTimeError = validateWorkTime();		

			if(keywordsNum > keyword_num){
				$('#keywordWarning').text('請輸入不多於'+keyword_num+'個關鍵字'); $('#keywordWarning').css('color','#ff0000');
				return;
			}else if(engKeywordsNum > keyword_num){
				$('#engKeywordWarning').text('請輸入不多於'+keyword_num+'個關鍵字'); $('#engKeywordWarning').css('color','#ff0000');
				return;
			}else if(workTimeError <0){
				if(workTimeError==-1){ $('#timeWarning').text('請輸入正確的營業時間'); $('#timeWarning').css('color','#ff0000'); }			
				return;
			}else{
				var con1 = $('#is_renovation').attr('checked');
				var con2 = $('#is_closed').attr('checked');
				var con3 = $('#is_moved').attr('checked');						
				if( (con1 && con2 && con3)||(con1 && con2)||(con2 && con3)||(con1 && con3) ){
					$('#statusWarning').text('特殊狀態只可選擇其中一項'); $('#statusWarning').css('color','#ff0000');
				}else{
					var jform = $('#'+formName);
					var params = jform.serialize();					
					var ajaxuri = path;

					$.post(ajaxuri, params, function(json){
						if(json.status){
							alert(json.message);
							if(thick ==1){
								parent.tb_remove();
							}else{
								location.href = json.url;
							}
						} else {
							if(json.keywordWarning) {
								$('#keywordWarning').text(json.keywordWarning); $('#keywordWarning').css('color','#ff0000');
							} else {
								alert(json.message);
							}		
						}
					},'json');
					return false;
				}
			}
		}
	};
	return formValidateCriteria;	
}

function validateWorkTime(){			
	weekinput= 0;
	warning  = 0;
	for(i=0; i<8; i++){
		if(!$('#work_dayoff_'+i).attr('checked')) {
			if ( $('#work_start_time_'+i).val() > 0 && $('#work_start_time_'+i).val() < 2400
			&& $('#work_end_time_'+i).val() > 0 && $('#work_end_time_'+i).val() < 2400
			&& $('#work_start_time_'+i).val().length == 4 && $('#work_end_time_'+i).val().length == 4 ) {weekinput++; } else {
				warning++;
			}
		}
	}
	if (warning > 0){
		return -1;
	}else{
		return 0;	
	}
}
/*End valiatation*/

/*EventHandler*/
function resetFormHandler(){
	$('#mallname').attr({readonly:false});
	$('#mallname').css('background-color','#FFFFFF');
	$('#mallname').val('');
	$('#address').val('');
	$('#googlemap_lat').val('');
	$('#googlemap_lng').val('');
	$('#mall').attr('checked',false).trigger('click').attr('checked',false);
	$('#reset').css('display','none');
}

function addressBlurEventHandler(){
	if ($('#address').val() != '{$shopInfo.address}'){
		if ($('#location_id').val()==''){	
			alert('未輸入區域');
			return;
		}

		//$('#addressError').text('正在搜索您輸入的街道'+$('#main_location_name').val() + ' ' + $('#sub_location_name').val() + ' ' + $('#address').val()+'是否存在...');
		//$('#addressError').css('background-color','#66f');
		geocoder.getLatLng(
			$('#location_id option:selected').text() + ' ' + $('#address').val(),
			function(point) {
				if (!point) {
					$('#addressError').text('無法找到您輸入的街道地址');
					$('#addressError').css('color','#ff0000');
					//$('#addressError').css('background-color','#f66');
					$('#address').focus();
				} else {
					$('#addressError').text('');
					//$('#addressError').text('您輸入的街道地址位於東經'+point.lat()+'度,北緯'+point.lng()+'度');
					//$('#addressError').css('background-color','#6f6');
					$('#googlemap_lat').val(point.lat());
					$('#googlemap_lng').val(point.lng());
					map.clearOverlays();
					map.setCenter(point, 17);
					var marker = new GMarker(point, {draggable: true});
					GEvent.addListener(marker, "dragstart", function() {
						map.closeInfoWindow();
					});
					GEvent.addListener(marker, "dragend", function() {
						var mlatlng = marker.getLatLng();
						$('#googlemap_lat').val(mlatlng.lat());
						$('#googlemap_lng').val(mlatlng.lng());
					});
					map.addOverlay(marker);
					marker.openInfoWindowHtml($('#sub_location_name').val() + ' ' + $('#address').val());
				}
			}
		);
	}	
}


function mallClickEventHandler(){
	if($('#mall').attr('checked')==true){
		$('#address').attr({readonly:true});
		$('#mallname').attr({readonly:false});
		$('#location_id').css({'visibility':'hidden'});
		$('#location_name').text($('#location_id option:selected').text());			
		$('#address').css('background-color','#ebebe4');
		$('#mallname').css('background-color','#FFFFFF');		
		$('#googlemap_lat').val('');
		$('#googlemap_lng').val('');
		$('#bymallselect').css("display","");
		$('#parent_shop_id').val('0');
		$('#mall_id').val('0');
		$('#mallname').val('');
		$('#address').val('');
	}else{
		$('#address').attr({readonly:false});
		$('#mallname').attr({readonly:true});
		$('#location_id').css({'visibility':'visible'});
		$('#location_name').text('');	
		$('#address').css('background-color','#FFFFFF');
		$('#bymallselect').css("display","none");
		$('#malllist').hide();
		$('#parent_shop_id').val('0');
		$('#mall_id').val('0');
		$('#mallname').val('');
		$('#address').val('');
		$('#googlemap_lat').val('');
		$('#googlemap_lng').val('');
		$('#address').trigger('blur');
	}	
}
/*End EventHandler*/

/*General function*/
function changeWorkTime(i) {
	if ($('#work_dayoff_'+i).attr('checked')) {
		$('#work_start_time_'+i).val('');
		$('#work_end_time_'+i).val('');
		$('#time_input_'+i).hide(); 
		$('#time_input_'+i).hide(); 
	} else {
		$('#time_input_'+i).show();
		$('#time_input_'+i).show();
	}
}

function setcates(cates){
	$('#cate_ids').val('');
	$('#cate_names').val('');
	var _ids = $('#cate_ids');
	var _names = $('#cate_names');	
	var temp_name='';
	for(idx in cates){
		if (_ids.val() != ''){
			_ids.val(_ids.val()+ ',')
		}
		if (_names.val() != ''){
			_names.val(_names.val()+ ',')
		}
		if(temp_name!=''){
			temp_name = temp_name + ',';
		}
		var id_ = cates[idx].value.split('||')[0];
		var name_ = cates[idx].value.split('||')[1];
		_ids.val(_ids.val() + id_);
		temp_name = temp_name + name_;
		_names.val(_names.val() + name_);
	}
	temp_name=shopuniquecates(temp_name);
	_names.val(temp_name);
}

function shopuniquecates(target){
	var ele = target.split(',');
	ele = shopunique(ele);	
	target='';
	for(var i=0;i<ele.length;i++){
		if(target!=''){
			target=target+',';
		}
		target=target+ele[i];
	}
	return target;
}

function shopunique(arrayName){
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ ){  
	for(var j=0; j<newArray.length;j++ ){
		if(newArray[j]==arrayName[i]) 
			continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}

function getcates(){
	var arr = $('#cate_ids').val().split(',');
	for(idx in arr){
		if (arr[idx] != ''){
			$('#cate_id_' + arr[idx]).attr('checked', true);
		}
	}
}

function buildGooglemap(googlemap_lat,googlemap_lng){
	map = new GMap2(document.getElementById("gmap"));
	geocoder = new GClientGeocoder();

	map.addControl(new GLargeMapControl());
	if(googlemap_lat!=null && googlemap_lng!=null){
		var latlng = new GLatLng(googlemap_lat,googlemap_lng);
		map.setCenter(latlng, 17);
		var marker = new GMarker(latlng, {draggable: true});
		map.addOverlay(marker);
		marker.openInfoWindowHtml($('#sub_location_name').val() + ' ' + $('#address').val());
		GEvent.addListener(marker, "dragstart", function() {
		  	map.closeInfoWindow();
		});
		GEvent.addListener(marker, "dragend", function() {
			var mlatlng = marker.getLatLng();
			$('#googlemap_lat').val(mlatlng.lat());
			$('#googlemap_lng').val(mlatlng.lng());							
		});	
	}
}

function fillSelectedMall(root){
	var locationId = $('#location_id').val();
	if (locationId == ''){
		alert('請選擇地區');
		$('#bymallselect').css("display","none");
		$('#mall').attr('checked', false);
		return;
	}

	var uri = root +'/Ajax/getMall?sub_location_id='+locationId+'&key=' + encodeURI($('#mallname').val());
	$.get(uri, function(response) {
		var json = $.parseJSON(response);
		$('#malllist').empty();	
		$('#malllist').show();
		$('#nomalltip').hide();
		if (json.data != null){
			for (idx in json.data){				
				var li;

				if (json.data[idx].name != ''){
					li = $('<li id="mall'+json.data[idx].shop_id+'">' + json.data[idx].name + '(' + json.data[idx].sub_location.name + json.data[idx].address + json.data[idx].rooms + ')</li>');
					$('#mallname').val(json.data[idx].name);
					$('#address').val(json.data[idx].address);
					$('#parent_shop_id').val(json.data[idx].shop_id);
				}else{					
					li = $('<li id="mall'+json.data[idx].shop_id+'">' + json.data[idx].eng_name + '(' + json.data[idx].sub_location.name + json.data[idx].address + json.data[idx].rooms+ ')</li>');
					$('#mallname').val(json.data[idx].eng_name);
					$('#address').val(json.data[idx].address);
					$('#parent_shop_id').val(json.data[idx].shop_id);
				}
				li.data('mall', json.data[idx]);
				li.bind('click', function(){
					$('#mallname').val($(this).data('mall').name);
					$('#address').val($(this).data('mall').address);
					$('#parent_shop_id').val($(this).data('mall').shop_id);
					$('#mallname').attr({readonly:true});
					$('#mallname').css('background-color','#ebebe4');
					$('#reset').css({display:''});
					$('#address').trigger('blur');
					$(this).parents('#malllist').hide();
				});				
				$('#malllist').append(li);
			}			
		}else{
			$('#nomalltip').show();			
		}
	});
}

function checkExistMall(root,mall_id){
	$().ready( function() {
		var uri = root+'/Ajax/getMall?mall_id='+ mall_id;
		$.get(uri, function(response) {
			var json = $.parseJSON(response);
			if (json.data[0] != null){
				$('#mall').attr('checked',true).trigger('click').attr('checked',true);
					$('#mallname').val(json.data[0].name);
					$('#address').val(json.data[0].address);
					$('#parent_shop_id').val(json.data[0].shop_id);
					$('#mallname').attr({readonly:true});
					$('#mallname').css('background-color','#ebebe4');
					$('#reset').css({display:''});
					$('#address').trigger('blur');
					$(this).parents('#malllist').hide();
			}
		});
	} );
		
}
/*End general function*/
