<?php
import("Think.Core.Model.RelationModel");
class ForumThreadModel extends RelationModel{
	protected $tableName = 'forum_thread';

	protected $_auto = array(
	array('create_time','time',self::MODEL_INSERT,'function'),
	);

	protected $_link = array(
		'pictures'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'forum_thread_picture',
			'foreign_key'=>'thread_id',
			'mapping_name'=>'pictures',
	),
	);

	protected $fields = array(
		'id',
		'create_time',
		'update_time',
		'user_id',
		'user_name',
		'board_id',
		'board_name',
		'summary',
		'body',
		'is_top',
	  'orders',
		'post_count',
		'view_count',
		'is_vote',
		'is_recommend',
		'lastpost_time',
		'lastpost_user'
		);
}
?>