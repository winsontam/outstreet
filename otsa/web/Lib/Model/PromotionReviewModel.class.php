<?php
import("Think.Core.Model.RelationModel");
class PromotionReviewModel extends RelationModel{
	protected $tableName = 'review_fields';

	protected $_auto = array(
		array('create_time','time',self::MODEL_INSERT,'function'),
	);

	protected $_link = array(
		'pictures'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'review_pictures',
			'foreign_key'=>'review_id',
			'mapping_name'=>'pictures',
		),
		'promotion'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'promotion_fields',
			'foreign_key'=>'promotion_id',
		),
		'user'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'user_fields',
			'foreign_key'=>'user_id',
		),
		'userstatistics'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'statistics_user',
			'foreign_key'=>'user_id',
		),
	);
}

?>