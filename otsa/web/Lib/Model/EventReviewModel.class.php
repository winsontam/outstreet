<?php
import("Think.Core.Model.RelationModel");
class EventReviewModel extends RelationModel{
	protected $tableName = 'review_fields';

	protected $_auto = array(
		array('create_time','time',self::MODEL_INSERT,'function'),
	);

	protected $_link = array(
		'pictures'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'review_pictures',
			'foreign_key'=>'review_id',
			'mapping_name'=>'pictures',
		),
		'event'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'event_fields',
			'foreign_key'=>'event_id',
		),
		'user'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'user_fields',
			'foreign_key'=>'user_id',
		),
		'userstatistics'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'statistics_user',
			'foreign_key'=>'user_id',
		),


	);


}
?>