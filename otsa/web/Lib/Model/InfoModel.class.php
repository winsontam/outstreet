<?php
class InfoModel extends Model{
	protected $tableName = 'wea_info';

	protected $_auto = array(
	array('create_time','time',self::MODEL_INSERT,'function'),
	);

	protected $fields=array(
	'id',
	'date',
	'weather_day',
	'temperature_day',
	'fengxiang_day',
	'speed_day',
	'weather_night',
	'temperature_night',
	'fengxiang_night',
	'speed_night',
	'create_time',
	'_pk'=>'id',
	'_autoinc'=>'true'
	);
}

?>