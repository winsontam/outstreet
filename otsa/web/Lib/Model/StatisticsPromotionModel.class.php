<?php

import("Think.Core.Model.RelationModel");
class StatisticsPromotionModel extends RelationModel{
	protected $tableName = 'statistics_promotion';

	protected $_link = array(
		'promotion'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'promotion_fields',
			'foreign_key'=>'promotion_id',
			'mapping_name'=>'promotion',
		),
	);
}

?>