<?php
import("Think.Core.Model.RelationModel");
class UserShopFavoriteModel extends RelationModel{
	protected $tableName = 'user_shop_favorite';

	protected $_auto = array(
	array('create_time','time',self::MODEL_INSERT,'function'),
	);
	
	protected $_link = array(
		'pictures'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'shop_picture',
			'foreign_key'=>'shop_id',
			'mapping_name'=>'pictures',
			'mapping_order'=>'create_time desc'
			),
		'shop'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'shop_fields',
			'foreign_key'=>'shop_id',
			'mapping_name'=>'shop',
		),
	);

	protected $fields = array(
		'id',  
		'create_time',    
		'user_id',
		'user_name',
		'shop_id',
		'shop_name',
		'_pk'=>'id',
		'_autoinc'=>true
	);

}
?>