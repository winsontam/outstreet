<?php

import( "Think.Core.Model.RelationModel" );

class AppCategoryModel extends RelationModel
{

	protected $tableName = 'app_category';
	protected $_link = array(
		'parent'	 => array(
			'mapping_type'	 => BELONGS_TO,
			'class_name'	 => 'app_category',
			'mapping_name'	 => 'parent',
			'foreign_key'	 => 'parent_id'
		),
		'reference'	 => array(
			'mapping_type'	 => BELONGS_TO,
			'class_name'	 => 'app_category',
			'mapping_name'	 => 'reference',
			'foreign_key'	 => 'reference_id'
		),
	);

}
