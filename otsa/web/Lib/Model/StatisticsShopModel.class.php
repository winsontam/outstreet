<?php

import("Think.Core.Model.RelationModel");
class StatisticsShopModel extends RelationModel{
	protected $tableName = 'statistics_shop';

	protected $_link = array(
		'shop'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'shop_fields',
			'foreign_key'=>'shop_id',
			'mapping_name'=>'shop',
		),
	);

	protected $fields = array(
		'shop_id',
		'shop_name',
		'review_count',
		'review_picture_count',
		'picture_count',
		'favorite_count',
		'total_rating',
		'avg_rate_1',
		'avg_rate_2',
		'avg_rate_3',
		'avg_rate_4',
		'avg_rate_5',
		'avg_rate_6',
		'avg_rate_7'
	);
}

?>