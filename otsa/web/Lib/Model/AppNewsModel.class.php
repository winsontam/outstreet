<?php

import( "Think.Core.Model.RelationModel" );

class AppNewsModel extends RelationModel
{

	protected $tableName = 'app_news';
	protected $_link = array(
		'photos'	 => array(
			'mapping_type'	 => HAS_MANY,
			'class_name'	 => 'app_news_photo',
			'mapping_name'	 => 'photos',
			'foreign_key'	 => 'news_id'
		),
	);

}
