<?php

import( "Think.Core.Model.RelationModel" );

class AppShopModel extends RelationModel
{

	protected $tableName = 'app_shop';
	protected $_link = array(
		'categories' => array(
			'mapping_type'			 => MANY_TO_MANY,
			'class_name'			 => 'app_category',
			'mapping_name'			 => 'categories',
			'foreign_key'			 => 'shop_id',
			'relation_foreign_key'	 => 'category_id',
			'relation_table'		 => 'app_shop_category',
		),
		'location'	 => array(
			'mapping_type'	 => BELONGS_TO,
			'class_name'	 => 'location_sub',
			'mapping_name'	 => 'location',
			'foreign_key'	 => 'location_id',
		),
		'photos'	 => array(
			'mapping_type'	 => HAS_MANY,
			'class_name'	 => 'app_shop_photo',
			'mapping_name'	 => 'photos',
			'foreign_key'	 => 'shop_id',
			'mapping_order' => 'cover DESC',
		),
	);

}
