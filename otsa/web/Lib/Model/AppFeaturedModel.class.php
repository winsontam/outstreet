<?php
import("Think.Core.Model.RelationModel");

class AppFeaturedModel extends RelationModel
{
    protected $tableName = 'app_featured';
    protected $_link     = array(
        'category' => array(
            'mapping_type' => BELONGS_TO,
            'class_name'   => 'app_category',
            'mapping_name' => 'category',
            'foreign_key'  => 'category_id',
        ),
        'shops'    => array(
            'mapping_type'         => MANY_TO_MANY,
            'class_name'           => 'app_shop',
            'mapping_name'         => 'shops',
            'foreign_key'          => 'featured_id',
            'relation_foreign_key' => 'shop_id',
            'relation_table'       => 'app_featured_shop',
        ),
    );

}