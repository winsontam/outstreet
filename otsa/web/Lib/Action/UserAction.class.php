<?php

class UserAction extends SaAction {

	public function listall(){
		$userDao=D('User');
		$id = @$_REQUEST[ 'id' ];
		$nick_name = @$_REQUEST[ 'nick_name' ];
		$email = @$_REQUEST[ 'email' ];
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'id';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'asc';
		$where = array();
		if ( $id ) { $where[ 'id' ] = array('like','%'.$id.'%'); }
		if ( $email ) { $where[ 'email' ] = array('like','%'.$email.'%'); }
		if ( $nick_name ) { $where[ 'nick_name' ] = array('like','%'.$nick_name.'%'); }		
		$count = $userDao->where($where)->count();		
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;		
		$userList = $userDao->where($where)->limit($limit)->order( $sort . ' ' . $sortorder )->select();
		$this->assign('page',$page);
		$this->assign('multipage', $multipage);
		$this->assign('userList',$userList);		
		$this->display();
	}
}

?>