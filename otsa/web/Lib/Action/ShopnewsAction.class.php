<?php

class ShopnewsAction extends SaAction {

	public function listAll(){

		$dbo = M( 'ShopNews' );

		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'shop_news.create_time';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';

		$where = array();
		$where['shop_news.is_audit'] = array('neq',-1);
		$count =  $dbo->field('shop_news.*,shop_fields.name,shopnews_picture.file_path')
			->join('shopnews_picture ON shop_news.id = shopnews_picture.shop_news_id')
			->where($where)->count('shop_news.id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$list = $dbo->field('shop_news.*,shopnews_picture.file_path')
			->join('shopnews_picture ON shop_news.id = shopnews_picture.shop_news_id')->where($where)->limit($limit)->order( $sort . ' ' . $sortorder )->select();

		foreach($list AS &$news){
			$news['shops'] = M('shop_news_map')->field('shop_fields.id, shop_fields.name')->join('shop_fields on shop_fields.id = shop_news_map.shop_id')->where('news_id='.$news['id'])->limit(1)->select();
			$news['shopcount'] = M('shop_news_map')->field('shop_fields.id, shop_fields.name')->join('shop_fields on shop_fields.id = shop_news_map.shop_id')->where('news_id='.$news['id'])->count();
		}
		unset($news);
		$this->assign( 'list' , $list );
		$this->display();
	}

	public function doAudit(){
	
		$id = $_REQUEST[ 'id' ];
		$isAudit = $_REQUEST[ 'isAudit' ];

		$dbo = M( 'ShopNews' );
		$dbo->where(array('id'=>$id))->save(array('is_audit'=>$isAudit));

	}

}
	
?>