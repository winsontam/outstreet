<?php

class AppFeaturedAction extends SaAction
{

	public function __construct()
	{
		parent::__construct();
	}

	public function listAll()
	{
		$title = $_REQUEST[ 'title' ];

		$dbo = D( 'AppFeatured' );

		$where = array();

		if ( $title )
		{
			$where[ 'title' ] = array( 'like', '%' . $title . '%' );
		}

		$count = $dbo
			->where( $where )
			->count( 'id' );

		import( 'ORG.Util.Page' );
		$pagination = new Page( $count, 100 );
		$multipage = $pagination->show();

		$rows = $dbo
			->where( $where )
			->limit( $pagination->firstRow . ',' . $pagination->listRows )
			->order( 'created_at ASC' )
			->relation( true )
			->select();

		$this->assign( 'rows', $rows );
		$this->assign( 'multipage', $multipage );
		$this->display();
	}

	public function add()
	{
		if ( $_POST )
		{
			$featuredDbo = M( 'AppFeatured' );
			$featuredShopDbo = M( 'AppFeaturedShop' );

			$data = $_REQUEST;
			$data[ 'created_at' ] = date( 'Y-m-d H:i:s' );
			$data[ 'updated_at' ] = date( 'Y-m-d H:i:s' );
			$featured_id = $featuredDbo->add( $data );

			foreach ( $data[ 'shop_ids' ] AS $shop_id )
			{
				$featuredShopDbo->add( compact( 'featured_id', 'shop_id' ) );
			}

			echo '<html><body><script>';
			echo 'alert("新增成功");';
			echo 'window.location = "' . __APP__ . '/app_featured/add";';
			echo '</script></body></html>';
		}
		else
		{
			$categoryDbo = D( 'AppCategory' );

			$categories = $categoryDbo
				->where( array( 'reference_id' => 0 ) )
				->select();

			$this->assign( 'categories', $categories );

			$this->display( 'save' );
		}
	}

	public function edit()
	{
		$featured_id = $_REQUEST[ 'id' ];

		if ( $_POST )
		{
			$featuredDbo = M( 'AppFeatured' );
			$featuredShopDbo = M( 'AppFeaturedShop' );

			$data = $_REQUEST;
			$data[ 'updated_at' ] = date( 'Y-m-d H:i:s' );
			$featuredDbo->save( $data );

			$featuredShopDbo->where( compact( 'featured_id' ) )->delete();

			foreach ( $data[ 'shop_ids' ] AS $shop_id )
			{
				$featuredShopDbo->add( compact( 'featured_id', 'shop_id' ) );
			}

			echo '<html><body><script>';
			echo 'alert("修改成功");';
			echo 'window.location = "' . __APP__ . '/app_featured/listAll";';
			echo '</script></body></html>';
		}
		else
		{
			$featuredDbo = D( 'AppFeatured' );

			$featured = $featuredDbo
				->where( array( 'id' => $featured_id ) )
				->relation( true )
				->find();

			$this->assign( 'featured', $featured );

			$categoryDbo = D( 'AppCategory' );

			$categories = $categoryDbo
				->where( array( 'reference_id' => 0 ) )
				->select();

			$this->assign( 'categories', $categories );

			$this->display( 'save' );
		}
	}

	public function delete()
	{
		$featured_id = $_POST[ 'id' ];

		if ( $featured_id )
		{
			$featuredDbo		 = D( 'AppFeatured' );
			$featuredShopDbo = M( 'AppFeaturedShop' );

			$featured = $featuredDbo
				->where( array( 'id' => $featured_id ) )
				->find();

			if ( $featured )
			{
				$featuredDbo->where( array( 'id' => $featured_id ) )->delete();
                $featuredShopDbo->where( compact( 'featured_id' ) )->delete();
			}
		}
	}

    public function hidden()
    {
        $featured_id = $_POST[ 'id' ];

        if ( $featured_id )
        {
			$featuredDbo		 = D( 'AppFeatured' );

            $data = array();
            $data[ 'id' ] = $featured_id;
            $data[ 'hidden' ] = 1;

            $featuredDbo->save( $data );
        }
    }

    public function visible()
    {
        $featured_id = $_POST[ 'id' ];

        if ( $featured_id )
        {
			$featuredDbo		 = D( 'AppFeatured' );

            $data = array();
            $data[ 'id' ] = $featured_id;
            $data[ 'hidden' ] = 0;

            $featuredDbo->save( $data );
        }
    }

}
