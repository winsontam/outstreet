<?php
class AjaxAction extends SaAction{

	public function getMall(){
		$key=$_GET['key'];
		$mall_id = $_GET['mall_id'];
		$mainLocationId = $_GET['main_location_id'];
		$subLocationId = $_GET['sub_location_id'];
		$shopDao=D('Shop');
		$mainLocationDao = D('LocationMain');
		$subLocationDao = D('LocationSub');	
		$shopCateDao = D('ShopCate');
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');
		$cate_id = "175";
		
		if (!empty($key)){
			$where['name'] = array('like', '%'.$key.'%');
		}
		if (!empty($mainLocationId)){
			$where['main_location_id'] = $mainLocationId;
		}
		if (!empty($subLocationId)){
			$where['sub_location_id'] = $subLocationId;
		}
		if (!empty($mall_id)){
			$where['shop_fields.id'] = $mall_id;
		}

		$mallList = array();
		if (count($where))
		{
			$where['cate_id'] = $cate_id;
			$where['is_audit']="1";
			$mallList = $shopDao->join('shop_cate_map on shop_cate_map.shop_id = shop_fields.id')->where($where)->limit(20)->order('shop_fields.id asc')->select();		
			foreach($mallList as $key=>$mall){
				$mainLoc = $mainLocationDao->find($mall['main_location_id']);
				$subLoc = $subLocationDao->find($mall['sub_location_id']);
				$mallList[$key]['main_location']['name']=$mainLoc['name'];
				$mallList[$key]['sub_location']['name']=$subLoc['name'];
			}
		}
		$this->ajaxReturn($mallList, 'get', 1);
	}

}

?>