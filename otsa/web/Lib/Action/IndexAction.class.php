<?php
class IndexAction extends SaAction
{
    public function index()
	{
		$this->display();
    }

    public function extjs()
	{
		$this->display();
    }

    public function doLogin()
	{
		$username = @$_REQUEST[ 'username' ];
		$password = @$_REQUEST[ 'password' ];
		
		$success = false;
		$_SESSION[ 'isLogged' ] = false;

		if ( $username && $password && M('admin')->where( array( 'username' => $username, 'password'=> md5($password) ) )->count() )
		{
			M('admin')->where( array( 'username' => $username ) )->setField('updated_at', date("Y-m-d G:i:s"));
			$_SESSION[ 'username' ] = $username;
			$_SESSION[ 'isLogged' ] = true;
			$success = true;
		}
		
		echo json_encode( array( 'success' => $success ) );
    }
}
?>