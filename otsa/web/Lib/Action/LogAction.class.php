<?php

class LogAction extends SaAction {

    public function appStats()
	{
		$unique_time = $_REQUEST['unique_time'];
		$start_date = $_REQUEST['start_date'];
		$end_date = $_REQUEST['end_date'];

		if ($unique_time == 'd') {
			$date_field = 'DATE_FORMAT(create_time,"%d %b %Y")';
			$count_field = 'COUNT(DISTINCT CONCAT(DATE_FORMAT(create_time,"%Y-%m-%d"), ",",`ip_address`))';
			$table_name = 'app_log.impression_daily';
			$datetime_field = 'DATE_FORMAT(datetime,"%d %b %Y")';
		} elseif ($unique_time == 'm') {
			$date_field = 'DATE_FORMAT(create_time,"%b %Y")';
			$count_field = 'COUNT(DISTINCT CONCAT(DATE_FORMAT(create_time,"%Y-%m-01"), ",",`ip_address`))';
			$table_name = 'app_log.impression_monthly';
			$datetime_field = 'DATE_FORMAT(datetime,"%b %Y")';
		} else {
			$date_field = 'DATE_FORMAT(create_time,"%d %b %Y &nbsp;&nbsp; %H:00")';
			$count_field = 'COUNT(`ip_address`)';
			$table_name = 'app_log.impression_hourly';
			$datetime_field = 'DATE_FORMAT(datetime,"%d %b %Y &nbsp;&nbsp; %H:00")';
		}

		$real_where = array();
		if ($start_date) { $real_where['create_time'][] = array('exp','>= "'.$start_date.'"'); }
		if ($end_date) { $real_where['create_time'][] = array('exp','<= "'.$end_date.'"'); }

		$log_where = array();
		if ($start_date) { $log_where['datetime'][] = array('exp','>= "'.$start_date.'"'); }
		if ($end_date) { $log_where['datetime'][] = array('exp','<= "'.$end_date.'"'); }

		$real_count = (int) array_pop( M()->table('app_log.impression')->field('COUNT(DISTINCT '.$date_field.')')->where($real_where)->find() );
		$log_count = (int) M()->table($table_name)->where($log_where)->count();
		$count = $real_count+$log_count;

		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;	

		$real_statslist = M()->table('app_log.impression')
			->field($date_field.' as `date`,'.$count_field.' as `count`')
			->where($real_where)
			->group('`date`')
			->order('`create_time` DESC')
			->limit($limit)
			->rows('date');

		$log_statslist = M()->table($table_name)
			->field($datetime_field.' as `date`,`count`')
			->where($log_where)
			->order('`datetime` DESC')
			->limit($limit)
			->rows('date');

		$statslist = (array)$real_statslist+(array)$log_statslist;

		$this->assign('unique_time',$unique_time);
		$this->assign('start_date',$start_date);	
		$this->assign('end_date',$end_date);
		$this->assign('multipage',$multipage);
		$this->assign('statslist',$statslist);		
		$this->display();
	}
	
	public function webStats()
	{
		$start_date = $_REQUEST['start_date'];
		$end_date = $_REQUEST['end_date'];

		$real_where = array();
		if ($start_date) { $real_where['create_time'][] = array('exp','>= unix_timestamp("'.$start_date.'")'); }
		if ($end_date) { $real_where['create_time'][] = array('exp','<= unix_timestamp("'.$end_date.'")'); }

		$log_where = array();
		if ($start_date) { $log_where['date'][] = array('exp','>= "'.$start_date.'"'); }
		if ($end_date) { $log_where['date'][] = array('exp','<= "'.$end_date.'"'); }

		$real_count = (int) array_pop( M()->table('outstreet_main.log_fields')->field('COUNT(DISTINCT DATE_FORMAT(FROM_UNIXTIME(create_time),"%Y-%m-%d"))')->where($real_where)->find() );
		$log_count = (int) M()->table('outstreet_log.log_summary')->where($log_where)->count();
		$count = $real_count+$log_count;

		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;	

		$real_statslist = M()->table('outstreet_main.log_fields')
			->field('DATE_FORMAT(FROM_UNIXTIME(create_time),"%Y-%m-%d") as date, count(*) as page_view, count(distinct(ip)) as visit, sum(redirect) as redirect')
			->where($real_where)
			->group('date')
			->order('create_time DESC')
			->limit($limit)
			->rows('date');

		$log_statslist = M()->table('outstreet_log.log_summary')
			->field('date, page_view, visit, redirect')
			->where($log_where)
			->order('date DESC')
			->limit($limit)
			->rows('date');

		$statslist = (array)$real_statslist+(array)$log_statslist;

		$this->assign('start_date',$start_date);	
		$this->assign('end_date',$end_date);
		$this->assign('multipage',$multipage);
		$this->assign('statslist',$statslist);		
		$this->display();
	}

}

?>