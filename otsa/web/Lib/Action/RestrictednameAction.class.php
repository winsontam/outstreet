<?php
class RestrictedNameAction extends SaAction {
	
    public function listAll() {
		
		$dbo = D( 'RestrictedName' );
		$name = @$_REQUEST[ 'name' ];
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'id';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'asc';

		$where = array();
		if ( $name ) { $where[ 'keyword' ] = array( 'like', '%' . $name . '%' ); }

		$count = $dbo->where( $where )->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		$list = $dbo->where( $where )->limit($limit)->order( $sort . ' ' . $sortorder )->select();
		$this->assign( 'list' , $list );
		$this->assign( 'multipage' , $multipage );
		$this->display();
		
    }
	
	public function examinelist(){
		$dbo = M( 'UserMessage' );
		$start_date = @$_REQUEST[ 'start_date' ];
		$end_date = @$_REQUEST[ 'end_date' ];
		$user_name = @$_REQUEST[ 'user_name' ];
				
		$submit_user_name = @$_REQUEST[ 'submit_user_name' ];	
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'create_time';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';
		$where = array();
		if ( $user_name ) { $where[ 'user_user_fields.nick_name' ] = array( 'like', '%' . $user_name . '%' ); }
		if ( $submit_user_name ) { $where[ 'submit_user_fields.nick_name' ] = array( 'like', '%' . $submit_user_name . '%' ); }		
		if ( $start_date && $end_date )
			{ $where[ 'user_message.create_time' ] = array( 'between', strtotime($start_date.' 00:00:00') . ',' . strtotime($end_date.' 23:59:59') ); }
		elseif ( $start_date )
			{ $where[ 'user_message.create_time' ] = array( 'gt', strtotime($start_date.' 00:00:00') ); }
		elseif ( $end_date )
			{ $where[ 'user_message.create_time' ] = array( 'lt', strtotime($end_date.' 23:59:59' )); }

		$count = $dbo->where( $where )->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		$list = $dbo
					->join( 'user_fields as user_user_fields on user_user_fields.id = user_message.user_id')
					->join( 'user_fields as submit_user_fields on submit_user_fields.id = user_message.submit_user_id')
					->field('user_message.id as id, user_message.body as body, user_message.reply as reply, user_message.create_time as create_time, user_message.is_audit as is_audit,user_user_fields.nick_name as user_name, submit_user_fields.nick_name as submit_user_name, user_message.user_id as user_id, user_message.submit_user_id as submit_user_id')
					->where( $where )									
					->limit($limit)->order( $sort . ' ' . $sortorder )					
					->select();
		
		$this->assign( 'list' , $list );
		$this->assign( 'multipage' , $multipage );
		$this->display();
	}
	
	public function doAudit()
	{
		$dbo = D( 'UserMessage' );
		if ( $id = @$_REQUEST[ 'id' ] )
		{
			//Start change status
			$data = array();
			$data[ 'is_audit' ] = @$_REQUEST[ 'is_audit' ] ? 0 : 1;
			$dbo->where( array( 'id' => $id ) )->data( $data )->save();
			//End change status			
		}
    }

    public function addRestricted() {
		
		$memberDb = D( 'RestrictedName' );		
		$input = @$_REQUEST[ 'restricted_name' ];
		$restricted_name = trim(strtolower(@$_REQUEST[ 'restricted_name' ]));

		$same = $memberDb->where( array( 'keyword' => $restricted_name ) )->count();
		
		if ($same > 0) {
			$keyword = $memberDb->where( array( 'keyword' => $restricted_name ) )->select();
			echo json_encode( array( 'result'=> '0', 'msg' => '已有此關鍵字!' , 'keyword' => $keyword[0]['keyword']) );
		} else {
			$data[ 'keyword' ] = $restricted_name;
			$memberDb->add( $data );
			echo json_encode( array( 'result'=> '1', 'msg' => '新增成功!' ) );
		}
		
    }
	
    public function doDelete() {
		
		$memberDb = D( 'RestrictedName' );		
		$keyword = @$_REQUEST[ 'keyword' ];

		$memberDb->where( array( 'keyword' => $keyword ) )->delete();
		
		$this->redirect( 'listAll' );
		
    }	
	
    public function doModify() {
		
		$memberDb = D( 'RestrictedName' );	
		$id = @$_REQUEST[ 'id' ];
		$new_keyword = trim(strtolower(@$_REQUEST[ 'new_keyword' ]));
		
		$same = $memberDb->where( array( 'keyword' => $new_keyword ) )->count();
		
		if ($same > 0) {
			$keyword = $memberDb->where( array( 'id' => $id ) )->select();
			echo json_encode( array( 'result'=> '0', 'msg' => '已有此關鍵字!' , 'keyword' => $keyword[0]['keyword']) );
		} else {
			$memberDb->where( array( 'id' => $id ) )->setField('keyword',$new_keyword);
			echo json_encode( array( 'result'=> '1', 'msg' => '修改成功!' ) );
		}

    }	

	public function memberlist() {

		$userDao = D('Memberlist');

		$name = trim($_REQUEST['name']);
		$create_date = $_REQUEST['create_date'];
		$addrkey = trim($_REQUEST['addrkey']);

		$status = 0; //0無操作  大於0成功樣式表   小於0失敗樣式
		//$actionInfo = '刪除后不可恢復,請謹慎操作'; //操作提示

		if (is_numeric($disable_id = $_GET['disable_id'])) {
			$userInfo = $userDao->find($disable_id);
			$where = array();
			$where['id'] = $disable_id;
			if ($userInfo['is_disabled'] == 1){
				$userDao->where($where)->setField('is_disabled', 0);
			}else{
				$userDao->where($where)->setField('is_disabled', 1);
			}
		}

		$where = array();
		if ( $name ) { $where[ 'nick_name ' ] = array( 'like', '%' . $name . '%' ); }	
		if ( $addrkey ) { $where[ 'email' ] = array( 'like', '%' . $addrkey . '%' ); }	

		//設置分頁代碼
		$count = $userDao->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$memberList = $userDao->where($where)->limit($limit)->order('id desc')->select();

		if ( $_REQUEST["p"] ) $page = $_REQUEST["p"];
		$this->assign('page',$page);
		$this->assign('status', $status);
		$this->assign('actionInfo', $actionInfo);
		$this->assign('multipage', $multipage);
		$this->assign('memberList', $memberList);

		$this->display();

	}

	public function userlogin() {
		import('@.ORG.SimpleAcl');
		
		$id = $_GET['id'];
		$userDao = D('User');
		
		$userInfo = $userDao->find($id);
		$authInfo = array();
		$authInfo['id'] = $userInfo['id'];
		$authInfo['nick_name'] = $userInfo['nick_name'];
		$authInfo['email'] = $userInfo['email'];
		$authInfo['sex'] = $userInfo['sex'];
		
		SimpleAcl::saveLogin($authInfo);
    	if(SimpleAcl::getLoginInfo()){
			header('location:http://www.outstreet.com.hk/index.php/user/view/id/'.$id);
    	}
	}

   //編輯個人信息
	public function edit() {

  		Import('@.ORG.SimpleAcl');
  		$id = $_REQUEST['id'];
		$this->assign('id',$id);
  		$userDao = D('User');
  		$this->assign('box',1);
  		if (isset($_POST['submit'])){
  			
  			$userInfo = $userDao->find( $_REQUEST['id'] );

  			$data = array();
  		
  			if (!empty($_FILES)){
				import('ORG.Net.UploadFile');
				$upload = new UploadFile();
				$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));
				$upload->savePath = C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH') ;
				$upload->saveRule = 'time';
	      		$upload->thumb = true;
	      		$upload->thumbRemoveOrigin = true;
	      		$upload->thumbPrefix = 'face';
	      		$upload->thumbMaxWidth = 120;
	      		$upload->thumbMaxHeight = 120;
	      		$upload->uploadReplace = true;
	      		if($upload->upload()) {
	      			$upinfo = $upload->getUploadFileInfo();
	      			$upfilepath = $upinfo[0]['savename'];
	      			$data['face_path'] = $upfilepath;
					unlink(C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH') . $userInfo['face_path']);
					unlink(C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH') . 'face' . $userInfo['face_path']);
	      		}
  			}
  		
			Import('ORG.Util.Input');

			if(isset($_POST['sex'])) $data['sex'] = $_POST['sex'];
			if(isset($_POST['birthday'])) $data['birthday'] = strtotime($_POST['birthday']);
			if(isset($_POST['income'])) $data['income'] = $_POST['income'];
			if(isset($_POST['education'])) $data['education'] = $_POST['education'];
			if(isset($_POST['school'])) $data['school'] = $_POST['school'];
			if(isset($_POST['professional'])) $data['professional'] = $_POST['professional'];
			if(isset($_POST['telephone'])) $data['telephone'] = $_POST['telephone'];
			if(isset($_POST['address'])) $data['address'] = $_POST['address'];
			if(isset($_POST['likelist'])) $data['likelist'] = $_POST['likelist'];
			if(isset($_POST['desc'])) $data['desc'] = Input::getVar($_POST['desc']);
  			$data['update_time'] = time();
			
  			if ($userDao->where( array( 'id' => $userInfo['id'] ) )->data($data)->save()) {
				$userInfo = $userDao->find($_REQUEST['id']);
				$authInfo = array();
				$authInfo['id'] = $userInfo['id'];
				$authInfo['nick_name'] = $userInfo['nick_name'];
				$authInfo['email'] = $userInfo['email'];
				$authInfo['sex'] = $userInfo['sex'];
				$authInfo['face_path'] = $userInfo['face_path'];
	
				SimpleAcl::saveLogin($authInfo);    		

				$this->assign('jumpUrl', '__URL__/memberList');
    			$this->success('更新資料成功');
  			}else{
  				$this->error("更新失敗");
  			}
  		
		}else{
	
			$userDao = D('User');
			$authInfo = SimpleAcl::getLoginInfo();
			$this->assign('userInfo', $userDao->find($_REQUEST['id']));

			$inputDao = D('Input');
			$where = array();
			$where['caption'] = '教育程度';
			$this->assign('educationList', $inputDao->where($where)->select());
			$where['caption'] = '家庭每月收入';
			$this->assign('incomeList', $inputDao->where($where)->select());
			$where['caption'] = '現職';
			$this->assign('professionalList', $inputDao->where($where)->select());
			$where['caption'] = '大學';
			$this->assign('schoolList', $inputDao->where($where)->select());

			$this->display();
		}
	}
		
    private function _userStampEncode($id,$createtime){
    	return base64_encode(rand(1000,9999) . $createtime . $id);
    }

	public function revalid() {
    	
    	$userDao = D('User');
		$userid = $_REQUEST['userid'];
    	$userInfo = $userDao->find($userid);
    	
		$caption = '多謝註冊成為OutStreet會員！請確認你的電郵。';
		$this->assign('nickname', $userInfo['nick_name']);
		$this->assign('caption',$caption);
		$this->assign('userstamp', $this->_userStampEncode($userid, $userInfo['create_time']));
		$this->assign('domain', $_SERVER['SERVER_NAME']);
		$body = $this->fetch('email_validate');

		if($userInfo['email'] != '') {
			$this->smtp_mail_log($userInfo['email'],$caption,$body,'',1,2);
		}
		$this->redirect('memberList');
    }
	
    public function doUpload() {
		
		$dbo = D( 'User' );
		$id = @$_REQUEST[ 'id' ];

		if ( $id )
		{
			$file = 'n' . $id;
			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));
			$upload->savePath = C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH');
			$upload->saveRule = 'time';

			if($upload->upload()) {
				$info =  $upload->getUploadFileInfo();
				$dbo->where( array( 'id' => $id ) )->save( array( 'face_path' => $info[0]['savename'] ) );
			}
		}
		$this->redirect( 'memberList' );

    }	
	
	public function deletePic(){
		$authInfo = $this->get('authInfo');
		
		$userid = $_REQUEST['userid'];
		$dao = D('User');
		$dao->where( array( 'id' => $userid ) )->setField('face_path','');

		unlink(C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH').$authInfo['face_path']);
		unlink(C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH').'face'.$authInfo['face_path']);
		$this->display();
	}
	
	public function uploadResult(){

		C('SHOW_PAGE_TRACE',false);

		$eleid = $_REQUEST['eleid'];
		$userid = $_REQUEST['userid'];
		$description = $_REQUEST['description'];

		import('ORG.Net.UploadFile');
		$upload = new UploadFile();
		$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));

		$upload->savePath = C('OUTSTREET_ROOT').C('USERFACE_UPFILE_PATH') ;

		$upload->saveRule = 'time';
		$upload->thumb = true;
		$upload->thumbMaxWidth = 120;
		$upload->thumbMaxHeight = 120;
		$upload->uploadReplace = true;
		$upload->thumbPrefix = 'face';
   
		if($upload->upload()) {
			$upinfo = $upload->getUploadFileInfo();
			$upfilepath = $upinfo[0]['savename'];

			$dao = D('User');
			
			$dao->where('id='.$userid)->setField('face_path', $upfilepath);

			$this->assign('eleid', $eleid);
			$this->assign('upfilepath', $upfilepath);
			$this->assign('description', $description);
			$this->assign('picid', $picid);
			$this->display('uploadResult');
		}else{
			$upfileerr = $upload->getErrorMsg();
			$upfilepath = '';
			echo '<script text="text/javascript">';
			echo 'parent.setPic("'.$eleid.'", "'.$upfilepath.'", "'.$upfileerr.'");';
			echo 'alert("'.$upfileerr.'");';
			echo '</script>';
		}

	}
	
}

?>
