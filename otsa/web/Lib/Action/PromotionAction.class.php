<?php

class PromotionAction extends SaAction
{

	public function listAction()
	{
		$promotionDao = D( 'Promotion' );
		$url = $_REQUEST[ 'url' ];
		$user_id = $this->getUserid( $_SESSION[ 'username' ] );

		$searchLink = $this->searchLink( $url );
		$sortLink = $this->sortLink( $url );
		$page = "/p/" . $_REQUEST[ 'p' ];
		$targetid = $_REQUEST[ 'targetid' ];
		$action = $_REQUEST[ 'action' ];
		$type = $_REQUEST[ 'type' ];
		if ( $type == 'audit' )
		{
			if ( $action == 0 )
			{
				$data[ 'is_special' ] = 0;
			}
			$data[ 'is_audit' ] = $action;
			$promotionDao->where( 'id=' . $targetid )->save( $data );

			$this->addLogTable( $user_id, $targetid, 'audit_' . $action );
		}
		if ( $type == 'isend' )
		{
			if ( $action == 1 )
			{
				$data[ 'is_special' ] = 0;
			}
			$data[ 'is_ended' ] = $action;
			$promotionDao->where( 'id=' . $targetid )->save( $data );

			$this->addLogTable( $user_id, $targetid, 'ended_' . $action );
		}
		if ( $type == 'delete' && $action == '1' )
		{
			$data[ 'is_special' ] = 0;
			$data[ 'is_audit' ] = '-1';
			$promotionDao->where( 'id=' . $targetid )->save( $data );

			$this->addLogTable( $user_id, $targetid, 'delete' );
		}
		if ( $_REQUEST[ 'type' ] == 'downgrade' )
		{
			$id = $_REQUEST[ 'targetid' ];
			$data[ 'is_special' ] = 0;
			$promotionDao->where( 'id=' . $id )->save( $data );
			$data[ 'is_special' ] = 1;
			$upgradeid = $_REQUEST[ 'upgradeid' ];
			$promotionDao->where( 'id=' . $upgradeid )->save( $data );

			$this->addLogTable( $user_id, $upgradeid, 'upgrade' );
			$this->addLogTable( $user_id, $id, 'downgrade' );
		}

		//$link= "/listAll".$sortLink.$searchLink.$page;
		//$this->redirect("Promotion/".$link);
		$this->redirect( "Promotion/listAll" );
	}

	private function getUserid( $username )
	{
		$adminDao = D( 'Admin' );
		$id = $adminDao->where( 'username="' . $username . '"' )->getField( 'admin_id' );
		return($id);
	}

	private function checkEnded()
	{
		for ( $i = 0; $i <= 1; $i++ )
		{
			$promotionDao = D( 'Promotion' );
			$user_id = $this->getUserid( $_SESSION[ 'username' ] );
			$today = date( "Y-m-d" );
			$where[ '_string' ] = 'time_end !="0000-00-00" AND time_end <"' . $today . '"';

			$con[ '_complex' ] = $where;
			$con[ 'is_ended' ] = 0;
			if ( $i == 0 )
			{
				$con[ 'is_special' ] = '1';
			}
			else
			{
				$con[ 'is_special' ] = '0';
			}

			$endedList = $promotionDao->where( $con )->select();
			foreach ( $endedList as $key => $val )
			{
				switch ( $val[ 'type1' ] )
				{
					case 1:
						$type = '大減價';
						break;
					case 2:
						$type = '店舖優惠';
						break;
					case 3:
						$type = '信用卡優惠';
						break;
				}
				$data[ 'is_special' ] = '0';
				$data[ 'is_ended' ] = '1';
				$promotionDao->where( 'id=' . $val[ 'id' ] )->save( $data );
				unset( $data );
				$data[ 'user_id' ] = $user_id;
				$data[ 'date' ] = time();
				$data[ 'promotion_id' ] = $val[ 'id' ];
				if ( $i == 0 )
				{
					$type = 'autoEndedwithSpecial';
				}
				else
				{
					$type = 'autoEnded';
				}
				$this->addLogTable( $user_id, $val[ 'id' ], $type );
				unset( $data );
			}
		}
	}

	public function listAll()
	{
		$logtable = $this->genLogTable();
		$this->checkEnded();
		$url = $this->strtohex( __SELF__ );
		$searchLink = $this->searchLink( $url );
		$promotionDao = D( 'Promotion' );
		$id = @$_REQUEST[ 'id' ];
		$shop_id = @$_REQUEST[ 'shop_id' ];
		$start_date = @$_REQUEST[ 'start_date' ];
		$end_date = @$_REQUEST[ 'end_date' ];
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'promotion_fields.id';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';
		$where = array( );
		$where[ 'is_audit' ] = array( 'neq', '-1' );
		$name = @$_REQUEST[ 'name' ];
		if ( $name )
		{
			$where[ 'name' ] = array( 'like', '%' . $name . '%' );
		}
		if ( $id )
		{
			$where[ 'id' ] = $id;
		}
		if ( $shop_id )
		{
			$where[ 'shop_id' ] = $shop_id;
		}
		if ( $start_date && $end_date )
		{
			$con[ 'time_start' ] = array( 'between', $start_date . ',' . $end_date );
			$con[ 'time_end' ] = array( 'between', $start_date . ',' . $end_date );
			$con[ '_logic' ] = 'AND';
			$where[ '_complex' ] = $con;
		}
		elseif ( $start_date )
		{
			$where[ 'time_start' ] = array( 'egt', $start_date );
		}
		elseif ( $end_date )
		{
			$where[ 'time_end' ] = array( 'elt', $end_date );
		}

		$count = $promotionDao->join( 'promotion_shop_map ON promotion_fields.id=promotion_shop_map.promotion_id' )->where( $where )->count( 'DISTINCT id' );
		import( 'ORG.Util.Page' );
		$p = new Page( $count );
		$multipage = $p->show();
		$limit = $p->firstRow . ',' . $p->listRows;
		$promotionList = $promotionDao->join( 'promotion_shop_map ON promotion_fields.id=promotion_shop_map.promotion_id' )->where( $where )->limit( $limit )->group( 'promotion_fields.id' )->order( $sort . ' ' . $sortorder )->select();
		foreach ( $promotionList as $key => $val )
		{
			$promotionList[ $key ][ 'id' ] = $promotionList[ $key ][ 'promotion_id' ];
			$promotionList[ $key ][ 'shop' ] = M( 'ShopFields' )->where( 'id=' . $promotionList[ $key ][ 'shop_id' ] )->find();
		}
		if ( $_REQUEST[ "p" ] )
			$page = $_REQUEST[ "p" ];
		$this->assign( 'logtable', $logtable );
		$this->assign( 'page', $page );
		$this->assign( 'searchLink', $searchLink );
		$this->assign( 'url', $url );
		$this->assign( 'multipage', $multipage );
		$this->assign( 'promotionList', $promotionList );
		$this->display();
	}

	public function promotionEdit()
	{
		if ( $_REQUEST[ 'type' ] == 'doedit' )
		{
			$picstamp = $_REQUEST[ 'picstamp' ];
			$data = $_REQUEST;
			$data[ 'update_time' ] = time();
			$id = $data[ 'id' ];
			$picid = $data[ 'picid' ];
			$cateids = explode( ',', $data[ 'cateids' ] );
			$shopids = explode( ',', $data[ 'shopids' ] );
			$data[ 'time_start' ] = str_replace( '/', '-', $data[ 'time_start' ] );
			$data[ 'time_end' ] = str_replace( '/', '-', $data[ 'time_end' ] );
			$data[ 'description' ] = stripslashes( html_entity_decode( $data[ 'description' ] ) );
			$data[ 'terms' ] = stripslashes( html_entity_decode( $data[ 'terms' ] ) );
			$data[ 'remarks' ] = stripslashes( html_entity_decode( $data[ 'remarks' ] ) );
			$promotionDao = D( 'Promotion' );
			$promotionDao->create( $data );
			$success = $promotionDao->save();
			M( 'promotion_cate_map' )->where( 'promotion_id=' . $id )->delete();
			M( 'promotion_shop_map' )->where( 'promotion_id=' . $id )->delete();
			foreach ( $cateids AS $val )
			{
				M( 'promotion_cate_map' )->add( array( 'promotion_id' => $id, 'cate_id' => $val ) );
			}
			foreach ( $shopids AS $val )
			{
				M( 'promotion_shop_map' )->add( array( 'promotion_id' => $id, 'shop_id' => $val ) );
			}
			D( 'PromotionPicture' )->where( "picstamp=$picstamp" )->setField( 'promotion_id', $id );
			D( 'PromotionPicture' )->where( "promotion_id=$id" )->setField( 'is_main', 0 );
			if ( $picid )
			{
				D( 'PromotionPicture' )->where( "id=$picid" )->limit( 1 )->setField( 'is_main', 1 );
			}
			else
			{
				D( 'PromotionPicture' )->where( "promotion_id=$id" )->limit( 1 )->setField( 'is_main', 1 );
			}

			$return = array( );
			if ( $success == 1 )
			{
				$user_id = $this->getUserid( $_SESSION[ 'username' ] );
				$this->addLogTable( $user_id, $id, 'edit' );
				$return[ 'success' ] = 1;
				$retuen[ 'message' ] = 1;
				$return[ 'url' ] = __ROOT__;
				echo json_encode( $return );
			}
			else
			{
				$return[ 'success' ] = 0;
				$retuen[ 'message' ] = 2;
				$return[ 'url' ] = __ROOT__;
				echo json_encode( $return );
			}
		}
		elseif ( $_REQUEST[ 'type' ] == 'doadd' )
		{
			$picstamp = $_REQUEST[ 'picstamp' ];
			$picid = $data[ 'picid' ];
			$data = $_REQUEST;
			$cateids = explode( ',', $data[ 'cateids' ] );
			$shopids = explode( ',', $data[ 'shopids' ] );
			$data[ 'time_start' ] = str_replace( '/', '-', $data[ 'time_start' ] );
			$data[ 'time_end' ] = str_replace( '/', '-', $data[ 'time_end' ] );
			$data[ 'description' ] = stripslashes( html_entity_decode( $data[ 'description' ] ) );
			$data[ 'terms' ] = stripslashes( html_entity_decode( $data[ 'terms' ] ) );
			$data[ 'remarks' ] = stripslashes( html_entity_decode( $data[ 'remarks' ] ) );
			$data[ 'create_time' ] = time();
			$promotionDao = D( 'Promotion' );
			$promotionDao->create( $data );
			$success = $promotionDao->add();
			foreach ( $cateids AS $val )
			{
				M( 'promotion_cate_map' )->add( array( 'promotion_id' => $success, 'cate_id' => $val ) );
			}
			foreach ( $shopids AS $val )
			{
				M( 'promotion_shop_map' )->add( array( 'promotion_id' => $success, 'shop_id' => $val ) );
			}
			D( 'PromotionPicture' )->where( "picstamp=$picstamp" )->setField( 'promotion_id', $success );
			if ( $picid )
			{
				D( 'PromotionPicture' )->where( "id=$picid" )->limit( 1 )->setField( 'is_main', 1 );
			}
			else
			{
				D( 'PromotionPicture' )->where( "promotion_id=$success" )->limit( 1 )->setField( 'is_main', 1 );
			}

			$return = array( );
			if ( $success > 0 )
			{
				$user_id = $this->getUserid( $_SESSION[ 'username' ] );
				$type = 'add';
				$this->addLogTable( $user_id, $success, $type );
				$return[ 'success' ] = 1;
				$retuen[ 'message' ] = 3;
				$return[ 'url' ] = __ROOT__;
				echo json_encode( $return );
			}
			else
			{
				$return[ 'success' ] = 0;
				$retuen[ 'message' ] = 4;
				$return[ 'url' ] = __ROOT__;
				echo json_encode( $return );
			}
		}
		elseif ( $_REQUEST[ 'type' ] == 'edit' )
		{
			$type = $_REQUEST[ 'type' ];
			$id = $_REQUEST[ 'id' ];
			$promotionDao = D( 'Promotion' );
			$shopDao = D( 'Shop' );
			$promotionInfo = $promotionDao->where( 'id=' . $id )->relation( true )->find();
			$mappedshops = M( 'promotion_shop_map' )->where( 'promotion_id=' . $id )->select();
			foreach ( $mappedshops as $key => $val )
			{
				$mappedshops[ $key ][ 'name' ] = $shopDao->where( 'id=' . $val[ 'shop_id' ] )->getField( 'name' );
				$mappedshops[ $key ][ 'address' ] = $shopDao->where( 'id=' . $val[ 'shop_id' ] )->getField( 'address' );
				if ( $key == 0 )
				{
					$mappedshopids.=$val[ 'shop_id' ];
				}
				else
				{
					$mappedshopids.=',' . $val[ 'shop_id' ];
				}
			}
			foreach ( $promotionInfo[ 'sub_cates' ] as $key => $val )
			{
				if ( $key == 0 )
				{
					$mappedcateids.=$val[ 'id' ];
					$mappedcatenames.=$val[ 'name' ];
				}
				else
				{
					$mappedcateids.=',' . $val[ 'id' ];
					$mappedcatenames.=',' . $val[ 'name' ];
				}
			}

			$picidx = $piccount = 4;
			$picids = array( );
			while ( $picidx-- > 0 )
			{
				$picids[ ] = ($piccount - $picidx);
			}
			$this->assign( 'picids', $picids );
			$this->assign( 'picid', D( 'PromotionPicture' )->where( "is_main=1" )->getField( 'id' ) );

			$this->assign( 'promotionInfo', $promotionInfo );
			$this->assign( 'mappedshopids', $mappedshopids );
			$this->assign( 'mappedcateids', $mappedcateids );
			$this->assign( 'mappedcatenames', $mappedcatenames );
			$this->assign( 'mappedshops', $mappedshops );

			$this->assign( 'type', $type );
			$this->assign( 'picstamp', time() . rand( 1000, 9999 ) );
			$this->display();
		}
		elseif ( $_REQUEST[ 'type' ] == 'add' )
		{
			$type = $_REQUEST[ 'type' ];

			$picidx = $piccount = 4;
			$picids = array( );
			while ( $picidx-- > 0 )
			{
				$picids[ ] = ($piccount - $picidx);
			}
			$this->assign( 'picids', $picids );

			$this->assign( 'type', $type );
			$this->assign( 'picstamp', time() . rand( 1000, 9999 ) );
			$this->display();
		}
	}

	public function upgrade()
	{
		$url = $_REQUEST[ 'url' ];
		$type1 = $_REQUEST[ 'type1' ];
		$promotionDao = D( 'Promotion' );
		$id = $_REQUEST[ 'promotionid' ];
		$isspecial = $promotionDao->where( 'is_special=1 and type1=' . $type1 )->count();
		if ( $isspecial == 3 )
		{
			$full = true;
			$upgradedList = $promotionDao->where( 'is_special=1 and type1=' . $type1 )->select();
			$this->assign( 'upgradedList', $upgradedList );
			$this->assign( 'type1', $type1 );
			$this->assign( 'id', $id );
		}
		else
		{
			$full = false;
			$data[ 'is_special' ] = 1;
			$promotionDao->where( 'id=' . $id )->save( $data );
			$searchLink = $this->searchLink( $url );
			$sortLink = $this->sortLink( $url );

			$this->addLogTable( $this->getUserid( $_SESSION[ 'username' ] ), $id, 'upgrade' );

			if ( $_REQUEST[ 'p' ] != '' )
			{
				$page = "/p/" . $_REQUEST[ 'p' ];
			}
			$url = "/listAll" . $sortLink . $searchLink . $page;
		}
		$this->assign( 'url', $url );
		$this->assign( 'full', $full );
		$this->display();
	}

	public function selshopcate()
	{
		//獲取Shop分類主目錄列表 壓入模板
		$cate = M( 'ShopCate' )->getField( 'id,name' );
		$cateidshaslevel3 = M( 'ShopCate' )->where( array( 'level' => 2 ) )->getField( 'id,parent_id' );
		$level_1 = M( 'ShopCate' )->where( array( 'level' => 1 ) )->getField( 'id,parent_id' );
		$level_2 = M( 'ShopCate' )->where( array( 'level' => 2 ) )->getField( 'id,parent_id' );
		$level_3 = M( 'ShopCate' )->where( array( 'level' => 3 ) )->getField( 'id,parent_id' );

		$cateList = array( );
		foreach ( $level_1 AS $level_1_id => $level_1_parent_id )
		{
			if ( $cate[ $level_1_id ] )
				$cateList[ $level_1_id ][ 'name' ] = $cate[ $level_1_id ];
		}
		foreach ( $level_2 AS $level_2_id => $level_2_parent_id )
		{
			if ( $cateList[ $level_2_parent_id ][ 'name' ] && $cate[ $level_2_id ] )
				$cateList[ $level_2_parent_id ][ 'item' ][ $level_2_id ][ 'name' ] = $cate[ $level_2_id ];
		}
		$haslevel3 = array( );
		foreach ( $level_3 AS $level_3_id => $level_3_parent_id )
		{
			if ( !in_array( $level_3_parent_id, $haslevel3 ) )
			{
				$haslevel3[ ] = $level_3_parent_id;
			}
			if ( $cateList[ $level_2[ $level_3_parent_id ] ][ 'name' ] && $cateList[ $level_2[ $level_3_parent_id ] ][ 'item' ][ $level_3_parent_id ][ 'name' ] && $cate[ $level_3_id ] )
				$cateList[ $level_2[ $level_3_parent_id ] ][ 'item' ][ $level_3_parent_id ][ 'item' ][ $level_3_id ][ 'name' ] = $cate[ $level_3_id ];
		}

		//Shop Cate Ref List
		$shopCateList = D( 'ShopCate' )->field( 'id,reference_id,level' )->select();
		$refList = array( );
		foreach ( $shopCateList as $key => $shopCate )
		{
			$cateId = $shopCate[ 'id' ];
			$refId = $shopCate[ 'reference_id' ];
			if ( $refId != 0 )
			{
				$refList[ $cateId ][ ] = $refId;
				$refList[ $refId ][ ] = $cateId;
			}
		}
		foreach ( $refList as $key => $ref )
		{
			foreach ( $ref as $cateId )
			{
				foreach ( $refList[ $cateId ] as $elem )
				{
					$refList[ $key ][ ] = $elem;
				}
			}
			$refList[ $key ] = array_unique( $refList[ $key ] );
		}

		$this->assign( 'haslevel3', $haslevel3 );
		$this->assign( 'refList', $refList );
		$this->assign( 'cateList', $cateList );
		$this->display();
	}

	public function ajax_search_promotion()
	{
		$query = $_REQUEST[ 'query' ];
		$shoplist = array( );
		$where = array( 'name' => array( 'like', '%' . $query . '%' ), 'shop_status' => array( 'neq', '2' ) );
		if ( count( $where ) )
		{
			$shoplist = M( 'ShopFields' )->field( 'id,name,address' )->where( $where )->select();
			foreach ( $shoplist as $key => $val )
			{
				$shoplist[ $key ][ 'cates' ] = implode( ', ', $this->rows( M( 'ShopCateMap' )->field( 'b.name' )->table( 'shop_cate_map a' )->join( 'shop_cate b ON a.cate_id=b.id' )->where( 'shop_id=' . $val[ 'id' ] )->select(), null, 'name' ) );
			}
		}

		echo json_encode( array( 'list' => $shoplist ) );
	}

	private function addLogTable( $user_id, $promotion_id, $type )
	{
		$promotionLogDao = D( 'SaLog' );
		$logdata[ 'admin_id' ] = $user_id;
		$logdata[ 'class_id' ] = $promotion_id;
		$logdata[ 'date' ] = time();
		$logdata[ 'type' ] = $type;
		$logdata[ 'class' ] = 'promotion';
		$promotionLogDao->create( $logdata );
		$promotionLogDao->add();
	}

	private function genLogTable()
	{
		$promotionLogDao = D( 'SaLog' );
		$adminDao = D( 'Admin' );
		$where[ 'class' ] = 'promotion';
		$logList = $promotionLogDao->limit( 20 )->where( $where )->order( 'id desc' )->select();
		foreach ( $logList as $key => $val )
		{
			$logList[ $key ][ 'date' ] = date( 'Y-m-d H:i:s', $val[ 'date' ] );
			$logList[ $key ][ 'user_name' ] = $adminDao->where( 'admin_id=' . $val[ 'admin_id' ] )->getField( 'username' );
		}
		return ($logList);
	}

	public function uploadPic()
	{
		C( 'SHOW_PAGE_TRACE', false );
		$this->display();
	}

	public function uploadResult()
	{
		C( 'SHOW_PAGE_TRACE', false );

		if ( isset( $_REQUEST[ 'submitBtn' ] ) )
		{
			$eleid = $_REQUEST[ 'eleid' ];
			$picstamp = $_REQUEST[ 'picstamp' ];
			$description = $_REQUEST[ 'description' ];
			$promotionid = $_REQUEST[ 'promotionid' ];
			$from = $_REQUEST[ 'from' ];

			import( 'ORG.Net.UploadFile' );
			$upload = new UploadFile();
			$upload->allowExts = explode( ',', C( 'UPFILE_ALLOW_IMGEXT' ) );
			$upload->savePath = C( 'OUTSTREET_ROOT' ) . '/' . C( 'PROMOTION_UPFILE_PATH' );
			$upload->saveRule = 'time';
			$upload->thumb = true;
			$upload->thumbMaxWidth = 120;
			$upload->thumbMaxHeight = 120;
			$upload->uploadReplace = true;

			if ( $upload->upload() )
			{
				$upinfo = $upload->getUploadFileInfo();
				$upfilepath = $upinfo[ 0 ][ 'savename' ];
				$dao = null;
				$dao = D( 'PromotionPicture' );
				$upfile = $dao->create();

				$upfile[ 'file_path' ] = $upfilepath;
				$upfile[ 'description' ] = $description;
				$upfile[ 'picstamp' ] = $picstamp;
				$upfile[ 'promotion_id' ] = 1;
				$upfile[ 'admin_id' ] = 1;

				$picid = $dao->add( $upfile );

				$this->assign( 'eleid', $eleid );
				$this->assign( 'upfilepath', $upfilepath );
				$this->assign( 'description', $description );
				$this->assign( 'picid', $picid );

				/* echo '<script text="text/javascript">';
				  echo 'setPic("'.$eleid.'","'.$upfilepath.'","'.$description.'","'.$picid.'");';
				  echo '</script>'; */

				$this->display( 'uploadResult' );
			}
			else
			{
				/* $upfileerr = $upload->getErrorMsg();
				  $upfilepath = '';
				  echo '<script text="text/javascript">';
				  echo 'setPic("'.$eleid.'", "'.$upfilepath.'", "'.$upfileerr.'");';
				  echo 'alert("'.$upfileerr.'");';
				  echo '</script>'; */
			}
		}
	}

	public function deletePic()
	{
		$id = $_REQUEST[ 'id' ];
		$from = $_REQUEST[ 'from' ];

		Import( '@.ORG.SimpleAcl' );
		$userInfo = SimpleAcl::getLoginInfo();

		$where = array( );
		$where[ 'id' ] = $id;

		$dao = D( 'PromotionPicture' );
		$picList = $dao->where( $where )->select();
		foreach ( $picList as $pic )
		{
			unlink( C( 'OUTSTREET_ROOT' ) . '/' . C( 'PROMOTION_UPFILE_PATH' ) . $pic[ 'file_path' ] );
			unlink( C( 'OUTSTREET_ROOT' ) . '/' . C( 'PROMOTION_UPFILE_PATH' ) . 'thumb_' . $pic[ 'file_path' ] );
		}
		$dao->where( $where )->delete();
		$this->display();
	}

	private function strToHex( $string )
	{
		$hex = '';
		for ( $i = 0; $i < strlen( $string ); $i++ )
		{
			$hex .= dechex( ord( $string[ $i ] ) );
		}
		return $hex;
	}

	private function hexToStr( $hex )
	{
		$string = '';
		for ( $i = 0; $i < strlen( $hex ) - 1; $i+=2 )
		{
			$string .= chr( hexdec( $hex[ $i ] . $hex[ $i + 1 ] ) );
		}
		return $string;
	}

	private function searchLink( $url )
	{
		$url = $this->hextostr( $url );
		if ( preg_match( '/(start_date\/)(?<start_date>\d{4}-\d{2}-\d{2})(\/)/', $url, $m ) )
		{
			$start_date = $m[ 'start_date' ];
			$link.='/start_date/' . $start_date;
		}
		if ( preg_match( '/(end_date\/)(?<end_date>\d{4}-\d{2}-\d{2})(\/)/', $url, $m ) )
		{
			$end_date = $m[ 'end_date' ];
			$link.='/end_date/' . $end_date;
		}
		if ( preg_match( '/\/(shop_id\/)(?<shop_id>\d+)(\/)/', $url, $m ) )
		{
			$shop_id = $m[ 'shop_id' ];
			$link.='/shop_id/' . $shop_id;
		}
		if ( preg_match( '/\/(id\/)(?<id>\d+)(\/)/', $url, $m ) )
		{
			$id = $m[ 'id' ];
			$link.='/id/' . $id;
		}
		return($link);
	}

	private function sortLink( $url )
	{
		$url = $this->hextostr( $url );

		if ( preg_match( '/(\/sort\/)(?<sort>.*?)(\/sortorder\/)/', $url, $m ) )
		{
			$sort = $m[ 'sort' ];
			$link.='/sort/' . $sort;
		}
		if ( preg_match( '/(\/sortorder\/)(?<sortorder>\w+)/', $url, $m ) )
		{
			$sortorder = $m[ 'sortorder' ];
			$link.='/sortorder/' . $sortorder;
		}
		return($link);
	}

}

?>