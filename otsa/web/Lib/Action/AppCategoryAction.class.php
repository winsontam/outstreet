<?php

class AppCategoryAction extends SaAction
{

	public function __construct()
	{
		parent::__construct();
	}

	public function listAll()
	{
		$name = $_REQUEST[ 'name' ];

		$dbo = D( 'AppCategory' );

		$where = array();

		if ( $name )
		{
			$where[ 'name' ] = array( 'like', '%' . $name . '%' );
			$where[ 'english_name' ] = array( 'like', '%' . $name . '%' );
            $where[ '_logic' ] = 'OR';
		}

		$count = $dbo
			->where( $where )
			->count( 'id' );

		import( 'ORG.Util.Page' );
		$pagination = new Page( $count, 100 );
		$multipage = $pagination->show();

		$rows = $dbo
			->where( $where )
			->limit( $pagination->firstRow . ',' . $pagination->listRows )
			->order( 'parent_id ASC, sort DESC' )
			->relation( true )
			->select();

		$this->assign( 'rows', $rows );
		$this->assign( 'multipage', $multipage );
		$this->display();
	}

	public function add()
	{
		if ( $_POST )
		{
			$categoryDbo = M( 'AppCategory' );

			$data = $_REQUEST;
			$category_id = $categoryDbo->add( $data );

			echo '<html><body><script>';
			echo 'alert("新增成功");';
			echo 'window.location = "' . __APP__ . '/app_category/add";';
			echo '</script></body></html>';
		}
		else
		{
			$categoryDbo = D( 'AppCategory' );
            
			$parentCategories = $categoryDbo
				->where( array( 'parent_id' => 0 ) )
				->select();

			$referenceCategories = $categoryDbo
				->where( array( 'reference_id' => 0 ) )
				->select();

			$this->assign( 'parentCategories', $parentCategories );
			$this->assign( 'referenceCategories', $referenceCategories );
			$this->display( 'save' );
		}
	}

	public function edit()
	{
		$category_id = $_REQUEST[ 'id' ];

		if ( $_POST )
		{
			$categoryDbo = M( 'AppCategory' );

			$data = $_REQUEST;
			$categoryDbo->save( $data );

			echo '<html><body><script>';
			echo 'alert("修改成功");';
			echo 'window.location = "' . __APP__ . '/app_category/listAll";';
			echo '</script></body></html>';
		}
		else
		{
			$categoryDbo = D( 'AppCategory' );

			$category = $categoryDbo
				->where( array( 'id' => $category_id ) )
				->find();

			$parentCategories = $categoryDbo
				->where( array( 'parent_id' => 0 ) )
				->select();

			$referenceCategories = $categoryDbo
				->where( array( 'reference_id' => 0 ) )
				->select();

			$this->assign( 'category', $category );
			$this->assign( 'parentCategories', $parentCategories );
			$this->assign( 'referenceCategories', $referenceCategories );
			$this->display( 'save' );
		}
	}

}
