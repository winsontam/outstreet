<?php

class ShopexcelAction extends SaAction {

	protected $excelmap = array(
		'name',
		'english_name',
		'brand_id',
		'cate_ids',
		'sub_location_id',
		'parent_shop_id',
		'address',
		'rooms',
		'telephone',
		'sub_telephone',
		'website',
		'email',
		'keyword',
		'start_time_1',
		'end_time_1',
		'start_time_2',
		'end_time_2',
		'start_time_3',
		'end_time_3',
		'start_time_4',
		'end_time_4',
		'start_time_5',
		'end_time_5',
		'start_time_6',
		'end_time_6',
		'start_time_7',
		'end_time_7',
		'start_time_8',
		'end_time_8',
		'consume_level',
		'description',
		'is_renovation'
	);

    public function doUpload()
	{
		$dbo = M('ShopTmp');
		$file = './Public/tmp/' . basename( $_FILES['file']['name'] );
		if( !move_uploaded_file( $_FILES['file']['tmp_name'], $file ) )
		{
			echo "There was an error uploading the file, please try again!";
		}
		else
		{
			require_once COMMON_PATH . 'excel/Classes/PHPExcel/IOFactory.php';
			$reader = PHPExcel_IOFactory::createReader('Excel5');
			$PHPExcel = $reader->load( $file );
			$sheet = $PHPExcel->getSheet( 0 );
			$highestRow = $sheet->getHighestRow();
			$highestCol = count( $this->excelmap );

			$errors = array();
			for ($row=2; $row<=$highestRow; $row++)
			{
				$emptynum = 0;
				$result = array();
				for ($column=0; $column<$highestCol; $column++)
				{
					$result[ $this->excelmap[ $column ] ] = trim($sheet->getCellByColumnAndRow( $column, $row )->getValue());   
					if ( !$result[ $this->excelmap[ $column ] ] ) { $emptynum++; }
				}
				if ( $emptynum != $highestCol )
				{
					$id = md5(implode('',$result));
					$result['id'] = $id;
					$dbo->data($result)->add();
				}
			}
		}
		echo '<script>alert("上傳成功");window.location = "' . __URL__ . '/listAll";</script>';
	}

    public function listAll()
	{
		$dbo = M('ShopTmp');

		$is_all = $_REQUEST['is_all'];
		
		$where = array();
		if (!$is_all) { $where['isError'] = 1; }

		$total = $dbo->where(array('isError'=>0))->count('id');

		$count = $dbo->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count,20);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$list = $dbo->where($where)->limit($limit)->select();

		$this->assign( 'total' , $total );
		$this->assign( 'list' , $list );
		$this->assign( 'multipage' , $multipage );
		$this->display();

	}

    public function doEdit()
	{
		$dbo = M('ShopTmp');
		$shop = $_REQUEST['shop'];
		
		foreach( $shop AS $key => $val )
		{
			$dbo->where(array('id' => $key))->data(array_merge($val,array('isError'=>0,'errorMsg'=>'')))->save();
		}
		echo '<script>alert("修改成功");window.location = "' . __URL__ . '/listAll";</script>';
	}

    public function doDelete()
	{
		$dbo = M('ShopTmp');
		$id = $_REQUEST['id'];
		
		$dbo->where(array('id' => $id))->delete();

		echo '<script>alert("刪除成功");window.location = "' . __URL__ . '/listAll";</script>';
	}

    public function process()
	{
		echo 'Loading...';

		$dbo = M('ShopTmp');

		$brand = M('ShopBrand')->select();
		$brandIds = array();
		foreach( $brand AS $val ) { $brandIds[] = $val['id']; }
		$cate = M('ShopCate')->where(array('parent_id'=>array('gt',0)))->select();
		$cateList = array();
		foreach( $cate AS $val ) { $cateList[$val['id']] = $val; }

		$sublocation = M('LocationSub')
			->join( 'location_main ON location_main.id = location_sub.main_location_id' )
			->field('location_sub.id AS sub_location_id, location_sub.main_location_id, location_main.name AS main_location_name, location_sub.name AS sub_location_name')
			->order('main_location_id')->select();
		$location = array();
		foreach( $sublocation AS $val ) { $location[$val['sub_location_id']] = $val; }

		$list = $dbo->limit(100)->where(array('isError'=>0))->select();

		foreach ( $list AS $val )
		{
			$isError = false;
			$errorMsg = array();
			$shop = $val;

			$shop['name'] = $shop['name'] ? $shop['name'] : $shop['english_name'];
			if ( !$shop['name'] ) { $isError = true; }

			if ( $shop['brand_id'] && !in_array( $shop['brand_id'], $brandIds ) )
				{ $isError = true; $errorMsg['brand_id'] = $shop['brand_id'] . ' not found.'; }

			$cate_ids = explode( ',', $shop['cate_ids'] );
			$filtered_cate_ids = array_unique( array_intersect( $cate_ids, array_keys($cateList) ) );
			if ( !$shop['cate_ids'] || !count( $cate_ids ) || !count( $filtered_cate_ids ) )
				{ $isError = true; $errorMsg['cate_ids'] = 'not found.'; }
			elseif ( count( $cate_ids ) != count( $filtered_cate_ids ) )
				{ $isError = true; $errorMsg['cate_ids'] = 'not found.(#' . implode(',',$filtered_cate_ids) . ' only?)'; }

			$idPaths = array();
			$problemIds = array();
			foreach ( $filtered_cate_ids AS $cate_id )
			{
				$topId = $cateList[$cate_id]['parent_id'];

				$idPath = '[' . $cate_id . ']';
				while( $topId ) { $idPath = '[' . $topId . ']' . $idPath; $topId = $cateList[$topId]['parent_id']; }

				$isDuplicate = false;
				
				foreach($idPaths AS $key=>$val)
				{
					if ( strlen($key)>strlen($idPath) && strpos($key,$idPath) === 0 )
						{ $isDuplicate = true; $problemIds[] = $val; $problemIds[] = $cate_id; }
					elseif ( strlen($key)<strlen($idPath) && strpos($idPath,$key) === 0 )
						{ $isDuplicate = true; $problemIds[] = $val; $problemIds[] = $cate_id; }
				}
				if (!$isDuplicate) { $idPaths[$idPath] = $cate_id; }
			}
			$problemIds = array_unique( $problemIds );
			if ( count( $problemIds ) )
				{ $isError = true; $errorMsg['cate_ids'] .= '<br/>duplicate parent.(#' . implode(',',$problemIds) . ')'; }

			if ( !$location[$shop['sub_location_id']] )
				{ $isError = true; $errorMsg['sub_location_id'] = 'not found.'; }
			else
			{
				$shop['sub_location_name'] = $location[$shop['sub_location_id']]['sub_location_name'];
				$shop['main_location_id'] = $location[$shop['sub_location_id']]['main_location_id'];
				$shop['main_location_name'] = $location[$shop['sub_location_id']]['main_location_name'];
			}

			if ( !$shop['address'] )
				{ $isError = true; $errorMsg['address'] = 'required.'; }

			$shop['telephone'] = str_replace(' ', '', $shop['telephone']);
			if ( strlen($shop['telephone']) != 8 || !is_numeric($shop['telephone']) )
				{ $isError = true; $errorMsg['telephone'] = 'not correct.'; }

			$shop['sub_telephone'] = str_replace(' ', '', $shop['sub_telephone']);
			if ( $shop['sub_telephone'] && (strlen($shop['sub_telephone']) != 8 || !is_numeric($shop['sub_telephone'])) )
				{ $isError = true; $errorMsg['sub_telephone'] = 'not correct.'; }

			if ( $shop['website'] && !preg_match('/^https?:\/\/([-\w]+[-\w\.]+)+(:\d+)?(\/([\w\/_\.]*(\?\S+)?)?)?$/is', $shop['website']) )
				{ $isError = true; $errorMsg['website'] = 'not correct.'; }
			
			if ( $shop['email'] && !preg_match('/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/is', $shop['email']) )
				{ $isError = true; $errorMsg['email'] = 'not correct.'; }
			
			$shop['is_renovation'] = $shop['is_renovation'] ? 1 : 0;

			for ( $i=1; $i<=8; $i++ )
			{
				if ($shop['start_time_'.$i] || $shop['end_time_'.$i]) {
					if (strlen($shop['start_time_'.$i]) == 4 && strlen($shop['end_time_'.$i]) == 4
						&& substr($shop['start_time_'.$i],0,2)<24 && substr($shop['start_time_'.$i],2,2)<60 
						&& substr($shop['end_time_'.$i],0,2)<24 && substr($shop['end_time_'.$i],2,2)<60 )
					{
						$shop['shoptime'][$i]['recurring_week'] = $i;
						$shop['shoptime'][$i]['start_time'] = $shop['start_time_'.$i];
						$shop['shoptime'][$i]['end_time'] = $shop['end_time_'.$i];
					}
					else { $isError = true; $errorMsg['time_'.$i] = 'not correct.'; }
				}
			}

			if (!$shop['googlemap_lat'] || !$shop['googlemap_lng'])
			{
				$map_api_key="ABQIAAAA6GRVtSVFZO2hhcu6JWc29BT2yXp_ZAY8_ufC3CFXhHIE1NvwkxS9NCHU2VbUZAkyhPfzTSHhaIWEcQ";
				$csv_url = "http://maps.google.com/maps/geo?q=".urlencode( $shop['address'] )."&output=csv&key=".$map_api_key;
				$result = explode(',', file_get_contents($csv_url) );
				if ( $result[0] == 200
					&& $result[2] >= 22.15 && $result[2] <= 22.53152
					&& $result[3] >= 113.8 && $result[3] <= 114.4 )
				{
					$shop['googlemap_lat'] = $result[2];
					$shop['googlemap_lng'] = $result[3];
				}
				else { $isError = true; $errorMsg['latlng'] = 'lat lng not found.'; }
			}
			
			if ($shop['parent_shop_id'])
			{
				$parent_shop = M('ShopFields')->where( array( 'id' => $shop['parent_shop_id'] ) )->find();
				if ( !$parent_shop )
					{ $isError = true; $errorMsg['parent_shop_id'] = 'not found.'; }
				else
				{
					$shop['address'] = $parent_shop['address'];
					$shop['rooms'] = $parent_shop['rooms'];
					$shop['sub_location_id'] = $parent_shop['sub_location_id'];
					$shop['sub_location_name'] = $parent_shop['sub_location_name'];
					$shop['main_location_id'] = $parent_shop['main_location_id'];
					$shop['main_location_name'] = $parent_shop['main_location_name'];
					$shop['googlemap_lat'] = $parent_shop['googlemap_lat'];
					$shop['googlemap_lng'] = $parent_shop['googlemap_lng'];
				}
			}

			if ( M('ShopFields')->where( array( 'name' => $shop['name'], 'address' => $shop['address'], 'telephone' => $shop['telephone'] ) )->count() )
				{ $isError = true; $errorMsg['name'] = 'duplicate entry.'; }

			M('ShopTmp')->where(array('id'=>$shop['id']))->data($shop)->save();

			if (!$isError)
			{
				$shop['create_time'] = time();
				$dbo->where(array('id' => $shop['id']))->delete();
				unset($shop['id']);
				$id = M('ShopFields')->data($shop)->add();
				foreach( $shop['shoptime'] AS $val )
				{
					M('ShopTime')->data(array_merge(array('shop_id'=>$id),$val))->add();
				}

				$loop = true;
				$loopIds = $cate_ids;
				$chlidrenIds = array();
				$parentIds = array();
				while($loop)
				{
					$cate = M('ShopCate')->where(array('parent_id'=>array('in',$loopIds)))->select();
					foreach ($cate AS $val) { $loopIds[] = $val['id']; $parentIds[] = $val['parent_id']; }
					if (count($loopIds)) { $chlidrenIds = array_merge($chlidrenIds,$loopIds); $loopIds = array(); }
					else { $loop = false; }
				}
				$cate_ids = array_unique(array_diff($chlidrenIds,$parentIds));

				foreach( $cate_ids AS $val )
				{
					M('ShopCateMap')->data(array('cate_id'=>$val, 'shop_id'=>$id))->add();
				}
				$keywords = explode( ',', $shop['keyword'] );
				if ( count($keywords) )
				{
					foreach( $keywords AS $val )
					{
						if ($val){ M('ShopKeywordMap')->data(array('keyword'=>$val, 'shop_id'=>$id, 'shop_name'=>$shop['name'], 'class' => 'main'))->add(); }
					}
					$existKeywords = M('ShopKeyword')->where(array('keyword'=>array('in',$keywords)))->getField('keyword');
					if ( count($existKeywords) ) { $keywords = array_diff($keywords,$existKeywords); }
					foreach( $keywords AS $val )
					{
						if ($val){ M('ShopKeyword')->data(array('keyword'=>$val, 'class' => 'main'))->add(); }
					}
				}
			}
			else
			{
				$dbo->where(array('id' => $shop['id']))->data(array('isError'=>1, 'errorMsg' => serialize($errorMsg)))->save();
			}
		}
		echo '<script>alert("處理成功");window.location = "' . __URL__ . '/listAll";</script>';

	}

}
	
?>