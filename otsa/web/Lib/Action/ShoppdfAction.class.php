<?php

class ShoppdfAction extends SaAction {

	public function listAll(){

		$dbo = M( 'ShopPdf' );

		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'shop_pdf.create_time';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';

		$where = array();
		$where['shop_pdf.is_audit'] = array('neq',-1);
		$count = $dbo->where($where)->count('shop_pdf.id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$list = $dbo->where($where)->limit($limit)->order( $sort . ' ' . $sortorder )->select();
		foreach($list AS &$pdf){
			$pdf['shops'] = M('shop_pdf_map')->field('shop_fields.id, shop_fields.name')->join('shop_fields on shop_fields.id = shop_pdf_map.shop_id')->where('pdf_id='.$pdf['id'])->limit(1)->select();
			$pdf['shopcount'] = M('shop_pdf_map')->field('shop_fields.id, shop_fields.name')->join('shop_fields on shop_fields.id = shop_pdf_map.shop_id')->where('pdf_id='.$pdf['id'])->count();
		}
		unset($pdf);
		$this->assign( 'list' , $list );
		$this->display();
	}

	public function doAudit(){
	
		$id = $_REQUEST[ 'id' ];
		$isAudit = $_REQUEST[ 'isAudit' ];

		$dbo = M( 'ShopPdf' );
		$dbo->where(array('id'=>$id))->save(array('is_audit'=>$isAudit));

	}

}
	
?>