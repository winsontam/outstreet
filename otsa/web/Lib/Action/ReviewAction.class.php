<?php

class ReviewAction extends SaAction {

    public function listAll(){	
		$reviewDao=D('Review');
		$userDao=D('User');
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'create_time';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';
		
		$title = @$_REQUEST[ 'title' ];
		$where=array();
		if ( $title ) { $where[ 'title' ] = array('like','%'.$title.'%'); }
		import('ORG.Util.Page');
		$count = $reviewDao->where($where)->count('id');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;		
		$reviewList=$reviewDao->limit($limit)->where($where)->order( $sort . ' ' . $sortorder )->select();
		foreach($reviewList as $key=>$val){
			if($val['shop_id']!=0){
				$reviewList[$key]['typenumber']=1;
				$reviewList[$key]['type']=$this->typeName(1);
				$reviewList[$key]['class_id']=$val['shop_id'];
				$reviewList[$key]['class']='shop';
			}elseif($val['event_id']!=0){
				$reviewList[$key]['typenumber']=2;
				$reviewList[$key]['type']=$this->typeName(2);
				$reviewList[$key]['class_id']=$val['event_id'];
				$reviewList[$key]['class']='event';
			}elseif($val['promotion_id']!=0){
				$reviewList[$key]['typenumber']=3;
				$reviewList[$key]['type']=$this->typeName(3);
				$reviewList[$key]['class_id']=$val['promotion_id'];
				$reviewList[$key]['class']='promotion';
			}
			$reviewList[$key]['username']=$userDao->where('id='.$val['user_id'])->getField('nick_name');
			$reviewList[$key]['is_email_verify']=$userDao->where('id='.$val['user_id'])->getField('is_email_verify');
		}		
		$this->assign('server',C('OUTSTREET_DIR'));	
		$this->assign('multipage' , $multipage );
		$this->assign('reviewList',$reviewList);		
		$this->display();
	}

	private function typeName($id){
		switch($id){
			case 1:
			$name='商戶';
			break;
			case 2:
			$name='節目';
			break;
			case 3:
			$name='優惠';
			break;
		}
		return($name);
	}	

	public function listAction(){
		$reviewDao	= D('Review');
		$actiontype	= $_REQUEST['actiontype'];
		$id			= $_REQUEST['id'];		
		$typenumber	= $_REQUEST['typenumber'];		
		$action		= $_REQUEST['action'];	
		
		$aInfo = $reviewDao->relation(true)->find($id);

		switch ($typenumber){
			case 1:
				if (!empty($aInfo)){				
					$isFirst = !$reviewDao->where(array('shop_id'=>$aInfo['shop_id'],'is_audit'=>1))->count();
					$audit = $_REQUEST['audit'];
					$reviewDao->where('id='.$id)->setField('is_audit',1);
					if($actiontype=='audit'&&$action==1){
						generateUserStatistics($aInfo['user_id']);
						generateShopReviewRateStatistics($aInfo['shop_id']);						
						$shopcate = M('ShopCateMap')->where(array('shop_id'=>$aInfo['shop_id']))->select();
						$i = 0;
						foreach ( $shopcate AS $val ) {
							if ($isFirst) {
								$data = array();
								$data['user_id'] = $aInfo['user_id'];
								$data['shop_id'] = $aInfo['shop_id'];
								$data['shop_review_id'] = $id;
								$data['cate_id'] = $val['cate_id'];
								$data['location_id'] = $aInfo['shop']['sub_location_id'];
								$data['type'] = 5;
								$data['score'] = 30;
								$data['isValid'] = !$i ? 1 : 0;
								$data['create_time'] = time();
								M('UserScoreLog')->add($data);
							}
							$data = array();
							$data['user_id'] = $aInfo['user_id'];
							$data['shop_id'] = $aInfo['shop_id'];
							$data['shop_review_id'] = $id;
							$data['cate_id'] = $val['cate_id'];
							$data['location_id'] = $aInfo['shop']['sub_location_id'];
							$data['type'] = 4;
							$data['score'] = 20;
							$data['isValid'] = !$i ? 1 : 0;
							$data['create_time'] = time();
							M('UserScoreLog')->add($data);
							$i++;
						}
		
						$caption = '你已成功發表評介';
						$this->assign('nickname',$aInfo['user']['nick_name']);
						$this->assign('shopname',$aInfo['shop']['name']);
						$this->assign('caption',$caption);
						$this->assign('description',$aInfo['description']);
						$this->assign('link',C('OUTSTREET_DIR') . '/shop/view/id/'.$aInfo['shop_id']);
							
						$body = $this->fetch('email_shop_review');
	
						if($aInfo['user']['email'] != '') {
							$this->smtp_mail_log($aInfo['user']['email'],$caption,$body,'',1,4);
						}

						$shopDao = D('Shop');
						$shopOwnerMapDao = D('ShopOwnerMap');
						$shop_id = $aInfo['shop_id'];
						$m = new Model();
						$owner = $m->query('select b.* from shop_owner_map as a, user_fields as b, shop_fields as c where a.shop_id=c.id and a.user_id= b.id and a.shop_id='.$shop_id.' and shop_level<>0');
						if(!$owner){
							//check brand
							$brand_id = $shopDao->where('id='.$shop_id)->getField('brand_id');
							if($brand_id){
								$owner = $m->query('select b.* from shop_owner_map as a, user_fields as b, shop_fields as c where a.brand_id=c.brand_id and a.user_id= b.id and a.brand_id='.$brand_id.' and shop_level<>0');
							}
						}
						if ($owner) {
							$caption = '你的商戶有一篇新評介';
							$this->assign('o_contact_person',$owner[0]['o_contact_person'].' '.$owner[0]['email']);
							$this->assign('title',$aInfo['title']);
							$this->assign('link',C('OUTSTREET_DIR') . '/shop/view/id/'.$aInfo['shop_id']);
							$body = $this->fetch('email_owner_review');
							if($owner[0]['email'] != '') {
								$this->smtp_mail_log($owner[0]['email'],$caption,$body,'',1,1);
							}
						}
					}
				}
			break;
			case 2:
				$isFirst = !$reviewDao->where(array('event_id'=>$aInfo['event_id'],'is_audit'=>1))->count();
				$eventDao=D('Event');
				$aInfo['event']=$eventDao->where('id='.$aInfo['event_id'])->find();				
				if($actiontype=='audit'&&$action==1){
					$reviewDao->where('id='.$id)->setField('is_audit',1);
					generateUserStatistics($aInfo['user_id']);
					generateEventStatistics($aInfo['event_id']);
					if ($isFirst){
						$data = array();
						$data['user_id'] = $aInfo['user_id'];
						$data['event_id'] = $aInfo['event_id'];
						$data['event_review_id'] = $id;
						$data['cate_id'] = $aInfo['event']['sub_event_cate_id'];
						$data['location_id'] = $aInfo['event']['sub_location_id'];
						$data['type'] = 5;
						$data['score'] = 30;
						$data['isValid'] = 1;
						$data['create_time'] = time();
						M('UserScoreLog')->add($data);					
					}
					$data = array();
					$data['user_id'] = $aInfo['user_id'];
					$data['event_id'] = $aInfo['event_id'];
					$data['event_review_id'] = $id;
					$data['cate_id'] = $aInfo['event']['cate_id'];
					$data['location_id'] = $aInfo['event']['sub_location_id'];
					$data['type'] = 4;
					$data['score'] = 20;
					$data['isValid'] = 1;
					$data['create_time'] = time();
					M('UserScoreLog')->add($data);
		
					$caption = '你已成功發表評介';
					$this->assign('nickname',$aInfo['user']['nick_name']);
					$this->assign('eventname',$aInfo['event']['name']);
					$this->assign('caption',$caption);
					$this->assign('description',$aInfo['description']);
					$this->assign('link',C('OUTSTREET_DIR') . '/event/view/id/'.$aInfo['event_id']);
						
					$body = $this->fetch('email_event_review');
		
					if($aInfo['user']['email'] != '') {
						$this->smtp_mail_log($aInfo['user']['email'],$caption,$body,'',1,1);
					}
				}
			break;
			case 3:
				$isFirst = !$reviewDao->where(array('promotion_id'=>$aInfo['promotion_id'],'is_audit'=>1))->count();
				$promotionDao=D('Promotion');
				$aInfo['promotion']=$promotionDao->where('id='.$aInfo['promotion_id'])->find();				
				if($actiontype=='audit'&&$action==1){
					$audit = $_REQUEST['audit'];
					$reviewDao->where('id='.$id)->setField('is_audit',1);
					generateUserStatistics($aInfo['user_id']);
					generatePromotionStatistics($aInfo['promotion_id']);
					$shopcate = M('PromotionCateMap')->where(array('promotion_id'=>$aInfo['promotion_id']))->select();				
					$i = 0;
					foreach ( $shopcate AS $val ){
						if ($isFirst){
							$data = array();
							$data['user_id'] = $aInfo['user_id'];
							$data['promotion_id'] = $aInfo['promotion_id'];
							$data['promotion_review_id'] = $id;
							$data['cate_id']=$val['cate_id'];						
							
							$data['type'] = 5;
							$data['score'] = 30;
							$data['isValid'] = !$i ? 1 : 0;
							$data['create_time'] = time();
							M('UserScoreLog')->add($data);					
						}
						$data = array();
						$data['user_id'] = $aInfo['user_id'];
						$data['promotion_id'] = $aInfo['promotion_id'];
						$data['promotion_review_id'] = $id;
						$data['cate_id']=$val['cate_id'];					
						
						$data['type'] = 4;
						$data['score'] = 20;
						$data['isValid'] = !$i ? 1 : 0;
						$data['create_time'] = time();
						M('UserScoreLog')->add($data);
						$i++;
					}	
					$caption = '你已成功發表評介';
					$this->assign('nickname',$aInfo['user']['nick_name']);
					$this->assign('promotionname',$aInfo['promotion']['name']);
					$this->assign('caption',$caption);
					$this->assign('description',$aInfo['description']);
					$this->assign('link',C('OUTSTREET_DIR') . '/promotion/view/id/'.$aInfo['promotion_id']);					
					$body = $this->fetch('email_promotion_review');
					if($aInfo['user']['email'] != '') {
						$this->smtp_mail_log($aInfo['user']['email'],$caption,$body,'',1,1);
					}	
				}
			break;
		}
		
		$this->redirect("Review/listAll");
	}

}

?>