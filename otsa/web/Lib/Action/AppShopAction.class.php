<?php

class AppShopAction extends SaAction
{

    public function __construct()
    {
        parent::__construct();
    }

    public function listAll()
    {
        $name = $_REQUEST[ 'name' ];

        $dbo = D( 'AppShop' );

        $where = array();

        if ( $name )
        {
            $where[ 'name' ] = array( 'like', '%' . $name . '%' );
        }

        $sort      = !empty( $_REQUEST[ 'sort' ] ) ? $_REQUEST[ 'sort' ] : 'created_at';
        $sortorder = !empty( $_REQUEST[ 'sortorder' ] ) ? $_REQUEST[ 'sortorder' ] : 'ASC';

        $count = $dbo
            ->where( $where )
            ->count( 'id' );

        import( 'ORG.Util.Page' );
        $pagination = new Page( $count, 100 );
        $multipage  = $pagination->show();

        $rows = $dbo
            ->where( $where )
            ->limit( $pagination->firstRow . ',' . $pagination->listRows )
            ->order( $sort . ' ' . $sortorder )
            ->relation( true )
            ->select();

        $this->assign( 'rows', $rows );
        $this->assign( 'multipage', $multipage );
        $this->display();
    }

    public function add()
    {
        if ( $_POST )
        {
            $shopDbo         = M( 'AppShop' );
            $shopCategoryDbo = M( 'AppShopCategory' );
            $shopPhotoDbo    = M( 'AppShopPhoto' );

            $data                 = $_REQUEST;
            $data[ 'created_at' ] = date( 'Y-m-d H:i:s' );
            $data[ 'updated_at' ] = date( 'Y-m-d H:i:s' );
            $shop_id              = $shopDbo->add( $data );

            foreach ( $data[ 'categories' ] AS $category_id )
            {
                if ( !$shopCategoryDbo->where( compact( 'shop_id', 'category_id' ) )->count( 'id' ) )
                {
                    $shopCategoryDbo->add( compact( 'shop_id', 'category_id' ) );
                }
            }

            foreach ( $data[ 'photos' ] AS $photo )
            {
                $file  = $photo;
                $cover = ( $file == $data[ 'cover_file' ] ) ? 1 : 0;
                $shopPhotoDbo->add( compact( 'shop_id', 'file', 'cover' ) );
                rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/' . $file );
                rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/small_' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/small_' . $file );
                rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/large_' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/large_' . $file );
            }

            echo '<html><body><script>';
            echo 'alert("新增成功");';
            echo 'window.location = "' . __APP__ . '/app_shop/add";';
            echo '</script></body></html>';
        }
        else
        {
            $this->display( 'save' );
        }
    }

    public function edit()
    {
        $shop_id = $_REQUEST[ 'id' ];

        if ( $_POST )
        {
            $shopDbo         = M( 'AppShop' );
            $shopCategoryDbo = M( 'AppShopCategory' );
            $shopPhotoDbo    = M( 'AppShopPhoto' );

            $data                 = $_REQUEST;
            $data[ 'updated_at' ] = date( 'Y-m-d H:i:s' );
            $shopDbo->save( $data );

            $shopCategoryDbo->where( compact( 'shop_id' ) )->delete();

            foreach ( $data[ 'categories' ] AS $category_id )
            {
                if ( !$shopCategoryDbo->where( compact( 'shop_id', 'category_id' ) )->count( 'id' ) )
                {
                    $shopCategoryDbo->add( compact( 'shop_id', 'category_id' ) );
                }
            }

            $shopPhotoDbo->where( compact( 'shop_id' ) )->delete();

            foreach ( $data[ 'currentPhotos' ] AS $file )
            {
                $cover = ( $file == $data[ 'cover_file' ] ) ? 1 : 0;
                $shopPhotoDbo->add( compact( 'shop_id', 'file', 'cover' ) );
            }

            foreach ( $data[ 'photos' ] AS $file )
            {
                $cover = ( $file == $data[ 'cover_file' ] ) ? 1 : 0;
                $shopPhotoDbo->add( compact( 'shop_id', 'file', 'cover' ) );
                rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/' . $file );
                rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/small_' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/small_' . $file );
                rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/large_' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/large_' . $file );
            }

            echo '<html><body><script>';
            echo 'alert("修改成功");';
            echo 'window.location = "' . __APP__ . '/app_shop/listAll";';
            echo '</script></body></html>';
        }
        else
        {
            $shopDbo = D( 'AppShop' );

            $shop = $shopDbo
                ->where( array( 'id' => $shop_id ) )
                ->relation( true )
                ->find();

            $this->assign( 'shop', $shop );
            $this->display( 'save' );
        }
    }

    public function delete()
    {
        $shop_id = $_POST[ 'id' ];

        if ( $shop_id )
        {
            $shopDbo         = D( 'AppShop' );
            $shopCategoryDbo = D( 'AppShopCategory' );
            $shopPhotoDbo    = D( 'AppShopPhoto' );

            $shop = $shopDbo
                ->where( array( 'id' => $shop_id ) )
                ->relation( true )
                ->find();

            if ( $shop )
            {
                foreach ( $shop[ 'photos' ] AS $index => $photo )
                {
                    unlink( C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/' . $photo[ 'file' ] );
                    unlink( C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/small_' . $photo[ 'file' ] );
                    unlink( C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/large_' . $photo[ 'file' ] );
                }

                $shopDbo->where( array( 'id' => $shop_id ) )->delete();
                $shopPhotoDbo->where( compact( 'shop_id' ) )->delete();
                $shopCategoryDbo->where( compact( 'shop_id' ) )->delete();
            }
        }
    }

    public function hidden()
    {
        $shop_id = $_POST[ 'id' ];

        if ( $shop_id )
        {
            $shopDbo         = D( 'AppShop' );

            $data = array();
            $data[ 'id' ] = $shop_id;
            $data[ 'hidden' ] = 1;
            
            $shopDbo->save( $data );
        }
    }

    public function visible()
    {
        $shop_id = $_POST[ 'id' ];

        if ( $shop_id )
        {
            $shopDbo         = D( 'AppShop' );

            $data = array();
            $data[ 'id' ] = $shop_id;
            $data[ 'hidden' ] = 0;
            
            $shopDbo->save( $data );
        }
    }

    public function stats()
    {
        $categories = D( 'AppCategory' )->where( array( 'parent_id' => array( 'neq', 0 ), 'reference_id' => 0 ) )->select();
        $locations  = D( 'LocationSub' )->select();

        $rows = M()->query( 'SELECT'
            . ' CONCAT( t2.category_id, ",", t1.location_id ) AS id,'
            . ' COUNT( t1.id ) AS number'
            . ' FROM app_shop t1'
            . ' JOIN app_shop_category t2 ON t1.id = t2.shop_id'
            . ' GROUP BY id'
        );

        $numbers = array();

        foreach ( $rows AS $row )
        {
            $numbers[ $row[ 'id' ] ] = $row[ 'number' ];
        }

        $this->assign( 'locations', $locations );
        $this->assign( 'categories', $categories );
        $this->assign( 'numbers', $numbers );

        $this->display( 'stats' );
    }

    public function doUploadPhoto()
    {
        import( 'ORG.Net.UploadFile' );
        import( 'ORG.Util.Image' );

        $upload                = new UploadFile();
        $upload->allowExts     = explode( ',', C( 'UPFILE_ALLOW_IMGEXT' ) );
        $upload->savePath      = C( 'OUTSTREET_ROOT' ) . '/Public/tmp/';
        $upload->saveRule      = 'time';
        $upload->uploadReplace = true;

        if ( $upload->upload() )
        {
            $info = $upload->getUploadFileInfo();
            $file = $info[ 0 ][ 'savename' ];

            $srcFile   = C( 'OUTSTREET_ROOT' ) . '/Public/tmp/' . $file;
            $smallFile = C( 'OUTSTREET_ROOT' ) . '/Public/tmp/small_' . $file;
            $largeFile = C( 'OUTSTREET_ROOT' ) . '/Public/tmp/large_' . $file;

            Image::thumb( $srcFile, $smallFile, null, 400, 400 );
            Image::thumb( $srcFile, $largeFile, null, 1000, 1000 );

            echo json_encode( array( 'file' => $file ) );
        }
    }

    public function doDeletePhoto()
    {
        $filename = basename( $_REQUEST[ 'filename' ] );

        unlink( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/' . $filename );
        unlink( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/small_' . $filename );
        unlink( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/large_' . $filename );
    }

    public function doResizePhotos()
    {
        set_time_limit( 0 );

        import( 'ORG.Util.Image' );

        $shopPhotoDbo = D( 'AppShopPhoto' );
        $shopPhotos   = $shopPhotoDbo->select();

        foreach ( $shopPhotos AS $shopPhoto )
        {
            $srcFile   = C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/' . $shopPhoto[ 'file' ];
            $smallFile = C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/small_' . $shopPhoto[ 'file' ];
            $largeFile = C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_shop/large_' . $shopPhoto[ 'file' ];

            unlink( $smallFile );
            Image::thumb( $srcFile, $smallFile, null, 400, 400 );

            //unlink( $largeFile );
            //Image::thumb( $srcFile, $largeFile, null, 1000, 1000 );

            echo $srcFile . '<br/>';
        }
    }
}
