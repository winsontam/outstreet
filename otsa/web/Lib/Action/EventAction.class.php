<?php

class EventAction extends SaAction {

    public function listAll() {
		$dbo = D( 'Event' );
		$is_recommend = @$_REQUEST[ 'is_recommend' ];
		$is_recommend_new = @$_REQUEST[ 'is_recommend_new' ];
		$name = @$_REQUEST[ 'name' ];
		$description = @$_REQUEST[ 'description' ];
		$start_date = @$_REQUEST[ 'start_date' ];
		$end_date = @$_REQUEST[ 'end_date' ];
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'create_time';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';

		$where = array();

		if ( $is_recommend != NULL ) { 
			if ( $is_recommend == 0 ) { 
				$where[ 'is_recommend' ] = 0; $where[ 'is_recommend_new' ] = 0; 
			}else $where[ 'is_recommend' ] = array( 'gt' , 0 );
		}else $where = NULL;
		
		if ( $is_recommend_new ) { $where[ 'is_recommend_new' ] = array( 'gt' , 0 ); }
		if ( $name ) { $where[ 'name' ] = array( 'like', '%' . $name . '%' ); }
		if ( $start_date && $end_date )
			{ $where[ 'create_time' ] = array( 'between', strtotime($start_date) . ',' . strtotime($end_date . ' 23:59') ); }
		elseif ( $start_date )
			{ $where[ 'create_time' ] = array( 'gt', strtotime($start_date) ); }
		elseif ( $end_date )
			{ $where[ 'create_time' ] = array( 'lt', strtotime($end_date. ' 23:59') ); }
		
		$count = $dbo->where( $where )->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		$list = $dbo->where( $where )->limit($limit)->order( $sort . ' ' . $sortorder )->select();

		$this->assign( 'list' , $list );
		$this->assign( 'multipage' , $multipage );
		$this->display();
    }
	
	public function doRecommend_new() {
		
		$dbo = D( 'Event' );

		if ( $id = @$_REQUEST[ 'id' ] ) {
			$data = array();
			$data[ 'is_recommend_new' ] = @$_REQUEST[ 'is_recommend_new' ] ? 0 : 1;

			$dbo->where( array( 'id' => $id ) )->data( $data )->save();
		}
		
		if ( $_REQUEST[ 'event_id' ] && $_REQUEST[ 'is_recommend_new' ] ) {	
			if ($dbo->where( array( 'id' =>$_REQUEST[ 'event_id' ] ) )->setField('is_recommend_new',$_REQUEST[ 'is_recommend_new' ]))
			echo json_encode( array( 'result'=> '1', 'msg' => '成功修改優先度!' ) );
			else echo json_encode( array( 'msg' => '修改失敗!' ) );
		}
    }

    public function doRecommend() {
		$dbo = D( 'Event' );

		if ( $id = @$_REQUEST[ 'id' ] ) {
			$data = array();
			$data[ 'is_recommend' ] = @$_REQUEST[ 'is_recommend' ] ? 0 : 1;

			$dbo->where( array( 'id' => $id ) )->data( $data )->save();
		}
		
		if ( $_REQUEST[ 'event_id' ] && $_REQUEST[ 'is_recommend' ] ) {	
			if ($dbo->where( array( 'id' =>$_REQUEST[ 'event_id' ] ) )->setField('is_recommend',$_REQUEST[ 'is_recommend' ]))
			echo json_encode( array( 'result'=> '1', 'msg' => '成功修改優先度!' ) );
			else echo json_encode( array( 'msg' => '修改失敗!' ) );
		}
    }

	public function doAudit() {
		$dbo = D( 'Event' );
		if ( $id = @$_REQUEST[ 'id' ] ) {
			//Start change status
			$data = array();
			$data[ 'is_audit' ] = @$_REQUEST[ 'is_audit' ] ? 0 : 1;
			$dbo->where( array( 'id' => $id ) )->data( $data )->save();
			
			generateMainLocationStatistics($eventInfo['main_location_id']);
			generateSubLocationStatistics($eventInfo['sub_location_id']);
			
			//End change status			
		}
    }
	
	public function eventEdit(){
		C('SHOW_PAGE_TRACE',false);
		 
		$id = $_REQUEST['id'];
		$picstamp = $_POST['picstamp'];
		$keyword = $_POST['keyword'];

		 
		$eventDao = D('Event');

		if (isset($_POST['submitBtn'])){
			 $id = $_POST['id'];
			 
			Import('ORG.Util.Input');

			$reference_id = D('EventCate')->where(array('id'=>$_POST['cate_id']))->getField('reference_id');

			$eventInfo = array();
			$eventInfo['cate_id'] = $reference_id ? $reference_id : $_POST['cate_id'];
			$eventInfo['main_location_id'] = $_POST['main_location_id'];
			$eventInfo['sub_location_id'] = $_POST['sub_location_id'];
			$eventInfo['name'] = $_POST['name'];
			$eventInfo['address'] = $_POST['address'];
			$eventInfo['creater_name'] = $_POST['creater_name'];
			$eventInfo['details'] = $_POST['details'];
			$eventInfo['telephone'] = $_POST['telephone'];
			$eventInfo['website'] = $_POST['website'];
			$eventInfo['email'] = $_POST['email'];
			$eventInfo['time_more'] = $_POST['time_more'];
			$eventInfo['keyword'] = $_POST['keyword'];
			$eventInfo['start_time'] = $_POST['start_date']." ".$_POST['start_hour'];
			$eventInfo['start_time'] = strtotime($eventInfo['start_time']);
			$eventInfo['end_time'] = $_POST['end_date']." ".$_POST['end_hour'];
			$eventInfo['end_time'] = strtotime($eventInfo['end_time']);
			$eventInfo['googlemap_lat'] = $_POST['googlemap_lat'];
			$eventInfo['googlemap_lng'] = $_POST['googlemap_lng'];
			$eventInfo['ticket_phone'] = $_POST['ticket_phone'];
			$eventInfo['ticket_website'] = $_POST['ticket_website'];
			$eventInfo['price'] = $_POST['price'];
			$eventInfo['ticket_startdate'] = strtotime($_POST['ticket_startdate']);	
			$eventInfo['ticket_required'] = $_POST['ticket_required'];	
			$eventInfo['parent_event_id'] = $_POST['parent_event_id'];	

			if($_POST['ticket_startdate']!=''){
				$eventInfo['ticket_startdate'] = strtotime($_POST['ticket_startdate']);		
			}else{
				$eventInfo['ticket_startdate']=0;
			}
			
			if($_POST['ticket_required']==''){
				$eventInfo['ticket_required'] = 0;					
			}else{
				$eventInfo['ticket_required'] = 1;										
			}				

			$eventInfo['update_time'] = time();
			$eventInfo['shop_id'] = $_POST['shop_id'];
			
			import ( '@.ORG.SimpleAcl' );
			$where = array();
			$where['id'] = $id;

			$eventDao->where($where)->save($eventInfo);			
			generateMainLocationStatistics($eventInfo['main_location_id']);
			generateSubLocationStatistics($eventInfo['sub_location_id']);
			$eventPictureDao = D('EventPicture');
			$eventPictureDao->where("picstamp=$picstamp")->setField('event_id', $_POST['id']);		
			$eventKeywordDao = D('EventKeyword');
			$eventKeywordMapDao = D('EventKeywordMap');
			$keywords = array();
			$keyword = str_replace(',','|',$keyword);
			$keyword = str_replace('，','|',$keyword);
			$keyword = str_replace(' ','|',$keyword);
			$keyword = str_replace('、','|',$keyword);
			$keywords = explode('|',$keyword);
				
			$eventKeywordMapDao->where('event_id='.$id)->delete();
			foreach($keywords as $key){
					
				if (!empty($key)){
					$keywordInfo = $eventKeywordDao->where("keyword='$key'")->find();
					if ($keywordInfo){
						$data = array();
						$data['event_id'] = $id;
						$data['keyword_id'] = $keywordInfo['id'];

						$eventKeywordMapDao->data($data)->add();
					}else{
						$newkeywordid = $eventKeywordDao->data(array('keyword'=>$key))->add();
						$data = array();
						$data['event_id'] = $id;
						$data['keyword_id'] = $newkeywordid;
							
						$eventKeywordMapDao->data($data)->add();
					}
				}
			}

				$this->assign('jumpUrl', '__URL__/listAll/');
    			$this->success('更新成功');
			//$this->assign('box',0);
			//$this->success("更新成功");
				
				
		}else{	
			$locationSubDao = D('LocationSub');
			$eventInfo = $eventDao->relation(true)->find($id);

			$eventInfo['start_date'] = date('Y/m/d',$eventInfo['start_time']);
			$eventInfo['start_hour'] = date('H:i',$eventInfo['start_time']);
			$eventInfo['end_date'] = date('Y/m/d',$eventInfo['end_time']);
			$eventInfo['end_hour'] = date('H:i',$eventInfo['end_time']);
			if($eventInfo['ticket_startdate']==0)$eventInfo['ticket_startdate']='';			
			$eventInfo['ticket_startdate'] = date('d-m-Y H:i:s',$eventInfo['ticket_startdate']);			
			if($eventInfo['ticket_phone']==0)$eventInfo['ticket_phone']='';

			$eventCateDao = D('EventCate');
			$mainLocationDao = D('LocationMain');

			$inputDao = D('Input');
			$where = array();
			$where['caption'] = '活動適合';
			$this->assign('eventAttendList', $inputDao->where($where)->select());

			//獲取Event分類主目錄列表 壓入模板
			$eventCateList = $eventCateDao->where(array('parent_id'=>0))->select();
			$this->assign('eventCateList', $eventCateList);

			//獲取Location分類主目錄列表 壓入模板
			$mainLocationList = $mainLocationDao->select();
			$this->assign('mainLocationList', $mainLocationList);

			$eventPictureDao = D('EventPicture');
			$eventInfo['pictures'] = $eventPictureDao->where("event_id=$id")->select();
			
			if($eventInfo['shop_id']==0){
				$eventInfo['shop_id']='';
			}

			$this->assign('eventInfo', $eventInfo);
			$picidx = $piccount = C('EVENT_PHOTO_COUNT') ? C('EVENT_PHOTO_COUNT') : 5;
			$picids = array();
			while($picidx-->0){
				$picids[] = ($piccount - $picidx);
			}
			$this->assign('picids', $picids);

			$this->display();
		}
	}//end edit
	public function uploadPic(){
		C('SHOW_PAGE_TRACE',false);
		$this->display();
	}

	public function uploadResult(){
		C('SHOW_PAGE_TRACE',false);
		if (isset($_POST['btnSubmit'])){
			$eleid = $_POST['eleid'];
			$picstamp = $_POST['picstamp'];
			$description = $_POST['description'];
			$eventid = $_POST['eventid'];
			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));
			$upload->savePath = C('OUTSTREET_ROOT').C('EVENT_UPFILE_PATH') ;
						
			$upload->saveRule = 'time';
			$upload->thumb = true;
			$upload->thumbMaxWidth = 120;
			$upload->thumbMaxHeight = 120;
			$upload->uploadReplace = true;
	   			
			if($upload->upload()) {

				$upinfo = $upload->getUploadFileInfo();
				$upfilepath = $upinfo[0]['savename'];
				$dao = D('EventPicture');
				$upfile = $dao->create();
				$upfile['file_path'] = $upfilepath;
				$upfile['description'] = $description;
				$upfile['picstamp'] = $picstamp;				
				$upfile['user_id'] = 1;
				
				$picid = $dao->add($upfile);			
				$this->assign('eleid', $eleid);
				$this->assign('upfilepath', $upfilepath);
				$this->assign('description', $description);
				$this->assign('event_id', $eventid);										

				$this->assign('picid', $picid);
					
				$this->display('uploadResult');

			}
			else{
				$upfileerr = $upload->getErrorMsg();
				$upfilepath = '';
				echo '<script text="text/javascript">';
				echo 'parent.setPic("'.$eleid.'", "'.$upfilepath.'", "'.$upfileerr.'");';
				echo 'alert("'.$upfileerr.'");';
				echo '</script>';
			}
		}
		 
	}

	public function deletePic(){
		 
		$id = $_GET['id'];
		$from = $_GET['from'];
		 
		Import('@.ORG.SimpleAcl');
		$userInfo = SimpleAcl::getLoginInfo();
		 
		$where = array();
		$where['user_id'] = 1;
		$where['id'] = $id;
		 
		$dao = D('EventPicture');
		$picList = $dao->where($where)->select();
		foreach($picList as $pic){
			unlink($_SERVER['DOCUMENT_ROOT'].'/'. C('EVENT_UPFILE_PATH') . $pic['file_path']);
			unlink($_SERVER['DOCUMENT_ROOT'].'/'. C('EVENT_UPFILE_PATH') . 'thumb_' . $pic['file_path']);
		}

		$dao->where($where)->delete();
		$this->display();
	}
	
	public function eventCateSub(){

		$mainid = $_GET['mainid'];

		$eventCateDao = D('EventCate');
		 
		$where = array();
		$where['parent_id'] = $mainid;
		$eventCateList = $eventCateDao->where($where)->select();
		 
		$this->ajaxReturn($eventCateList);

	}

	//要求$_GET傳遞MainLocationId
	//輸出指定MainLocationId下的所有商圈
	public function locationSub(){

		$mainLocationId = $_GET['mainlocationid'];

		$locationSubDao = D('LocationSub');
		 
		$where = array();
		$where['main_location_id'] = $mainLocationId;
		$SubLocationList = $locationSubDao->where($where)->select();
		 
		$this->ajaxReturn($SubLocationList);
	}
	
	public function setEventMainPic(){
		$id = $_REQUEST['id'];
		if(!empty($id)){
			
			$epDao = D('EventPicture');
			
			$epInfo = $epDao->find($id);
			$where = array();
			$where['event_id'] = $epInfo['event_id'];
			$epDao->where($where)->setField('is_main','0');

			$where = array();
			$where['id'] = $id;
			$epDao->where($where)->setField('is_main','1');
			
			$this->ajaxReturn(null, '設置成功');
		}else{
			$this->ajaxReturn(null, '設置失敗');
		}
	}
	
	public function shopName(){
		$shopId = $_GET['shopId'];
		if($shopId==''){
			$this->ajaxReturn(null,'');
		}else{
			$shopDao = D('Shop');
			$where = array();
			$where['id'] = $shopId;
			$shopList = $shopDao->where($where)->select();
			if(count($shopList)==1){
				$this->ajaxReturn($shopList);
			}else{
				$this->ajaxReturn(null,'找不到此場地');
			}
		}
	}


}

?>