<?php

class ShoprequestAction extends SaAction {
	
	public function listShopAudit() {
		$dbo = M( 'ShopTmpFields' );

		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'id';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'asc';

		$where = array();
		if ( $_REQUEST['is_audit'] != 'all' ) {
			$where['a.is_audit'] = $_REQUEST['is_audit'] ? $_REQUEST['is_audit'] : 0;
		}

		$count = $dbo->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$shopList = $dbo->field('a.id, a.shop_id, a.create_time, a.is_audit, a.status, a.name AS tmpname, b.name AS shopname,c.user_type, a.user_id')->table('shop_tmp_fields a')->join('shop_fields b ON a.shop_id = b.id')->join('user_fields c on c.id=a.user_id')->where($where)->limit($limit)->order($sort.' '.$sortorder)->select();

		$this->assign('shopList',$shopList);
		$this->assign('sort',$sort);
		$this->assign('shopTypeList',$shopTypeList);
		$this->assign('sortorder',$sortorder);
		$this->assign('multipage',$multipage);
		$this->display();
    }
	
	public function doShopAudit() {
		$tmpId = $_REQUEST['tmpId'];
		$audit = $_REQUEST['audit'];
		$tmpShop = M('ShopTmpFields')->where( array( 'id'=>$tmpId ) )->find();
		
		if ($audit == 1) {
			if ( $tmpShop['status'] == 'edit' ) {
				$oldShop = M('ShopFields')->where( array( 'id'=>$tmpShop['shop_id'] ) )->find();
				$user_id = $tmpShop['user_id'];
				$shop_id = $tmpShop['shop_id'];
				$affectedFields = unserialize( $tmpShop['affectedFields'] );
				$data = $tmpShop;
				
				$savedata = array();
				foreach( $affectedFields AS $name ) {
					$savedata[$name] = $data[$name];
				}
				$savedata['is_audit'] = 1;

				if( in_array( 'brand_id', $affectedFields )){
					if($data['brand_id']=='0'&&$data['brand_name']!=''){
						$brandDao=D('Brand');
						$countBrand=$brandDao->where('name="'.$data['brand_name'].'"')->count();
						if($countBrand==''||$countBrand=='0'){
							$newBranddata['name']=$data['brand_name'];
							$newbrand_id=$brandDao->add($newBranddata);
							$savedata['brand_id']=$newbrand_id;
						}
					}else{
						$savedata['brand_id']=$data['brand_id'];
					}
					unset($savedata['new_brand']);					
				}
				$savedata['approved_time']=time();
				M('ShopFields')->where( array( 'id' => $shop_id ) )->save( $savedata );
				
				if ( in_array( 'shoptime', $affectedFields ) ) {
					$tmpTime = M('ShopTmpTime')->where( array( 'shop_tmp_fields_id' => $tmpId ) )->select();
					M('ShopTime')->where( array( 'shop_id' => $shop_id ) )->delete();
					foreach ( $tmpTime AS $data ) {
						M('ShopTime')->add( $data );
					}
					if ( count( $tmpTime) ) {
						M('ShopFields')->where( array( 'id' => $shop_id ) )->setField('work_time', 1);
					}
				}

				if ( in_array( 'shopcate', $affectedFields ) ) {
					$tmpCateMap = M('ShopTmpCateMap')->where( array( 'shop_tmp_fields_id' => $tmpId ) )->select();
					generateShopCateStatByShopID($shop_id, 'delete');
					M('ShopCateMap')->where( array( 'shop_id' => $shop_id ) )->delete();					
					foreach ($tmpCateMap AS $data){							
						M('ShopCateMap')->add($data);
						generateShopCateStatByShopID($shop_id, 'add');
					}
				}
			} elseif ( $tmpShop['status'] == 'add' ) {
				$user_id = $tmpShop['user_id'];
				$data = $tmpShop;
				$data['is_audit'] = 1;
				unset( $data[ 'id' ], $data[ 'user_id' ]);
				$data[ 'picstamp' ] = time() . rand(1000,9999);
				if($data['brand_id']=='0'&&$data['brand_name']!=''){
					$brandDao=D('Brand');
					$countBrand=$brandDao->where('name="'.$data['brand_name'].'"')->count();
					if($countBrand==''||$countBrand=='0'){							
						$newBranddata['name']=$data['brand_name'];					
						$newbrand_id=$brandDao->add($newBranddata);
						$savedata['brand_id']=$newbrand_id;
					}else{
						echo $countBrand.'fail';
					}
				}else{
					$savedata['brand_id']=$data['brand_id'];
				}
				$shop_id = M('ShopFields')->data( $data )->add();

				//update owner when audit request add shop
				$m = new Model();
				$m->execute('insert into shop_owner_map(user_id,shop_id,brand_id) values ('.$user_id.','.$shop_id.',0)');

				$tmpTime = M('ShopTmpTime')->where( array( 'shop_tmp_fields_id' => $tmpId ) )->select();
				foreach ($tmpTime AS $data) {
					$data['shop_id'] = $shop_id;
					M('ShopTime')->data( $data )->add();
				}
				if ( count( $tmpTime) ) {
					M('ShopFields')->where( array( 'id' => $shop_id ) )->setField('work_time', 1);
				}

				$tmpCateMap = M('ShopTmpCateMap')->where( array( 'shop_tmp_fields_id' => $tmpId ) )->select();
				foreach ( $tmpCateMap AS $data) {
					$data['shop_id'] = $shop_id;
					M('ShopCateMap')->data( $data )->add();
					generateShopCateStatByShopID($shop_id, 'add');
				}
			}
			M('ShopTmpFields')->where( array( 'id' => $tmpId ) )->setField('shop_id', $shop_id);
			M('ShopTmpFields')->where( array( 'id' => $tmpId ) )->setField('is_audit', 1);
			M('ShopTmpFields')->where( array( 'id' => $tmpId ) )->setField('approved_time', time());

			$shop = M('ShopFields')->where(array('id'=>$shop_id))->select();
			$shopcate = M('ShopCateMap')->where(array('shop_id'=>$shop_id))->select();
			$i = 0;
			foreach ( $shopcate AS $val )
			{
				$log = array();
				$log['user_id'] = $user_id;
				$log['shop_id'] = $shop_id;
				$log['cate_id'] = $val['cate_id'];
				$log['location_id'] = $shop['sub_location_id'];
				$log['type'] = 2;
				$log['score'] = 10;
				$log['isValid'] = !$i ? 1 : 0;
				$log['create_time'] = time();
				M('UserScoreLog')->add($log);
				$i++;
			}

		} elseif ( $audit == 2 ) {
			M('ShopTmpFields')->where( array( 'id' => $tmpId ) )->setField('is_audit', 2);
			M('ShopTmpFields')->where( array( 'id' => $tmpId ) )->setField('approved_time', time());
		}

		if($audit==1&&$tmpShop['status'] == 'add'){			
			$userDao=D('User');
			$tmpShop['user']=$userDao->where('id='.$tmpShop['user_id'])->find();			
			$caption = '成功加入新商戶資料';
			if($tmpShop['user']['nick_name']!=''){
				$this->assign('nickname',$tmpShop['user']['nick_name']);
			}elseif($tmpShop['provider_name']!=''){
				$this->assign('nickname',$tmpShop['provider_name']);
			}

			$this->assign('shopname',$tmpShop['name']);
			$this->assign('caption',$caption);
			$this->assign('description',$tmpShop['description']);
			$this->assign('link',C('OUTSTREET_DIR').'/shop/view/id/'.$shop_id);
			$body = $this->fetch('email_request_add_shop');
			
			if($tmpShop['user']['email'] != '') {
				$this->smtp_mail_log($tmpShop['user']['email'],$caption,$body,'',1,5);
			}elseif($tmpShop['provider_email']!=''){
				$this->smtp_mail_log($tmpShop['provider_email'],$caption,$body,'',1,5);				
			}
		}
		if($audit==1&&$tmpShop['status'] == 'edit'){			
			$userDao=D('User');
			$tmpShop['user']=$userDao->where('id='.$tmpShop['user_id'])->find();			
			$caption = '成功更新商戶資料';
			if($tmpShop['user']['nick_name']!=''){
				$this->assign('nickname',$tmpShop['user']['nick_name']);
			}elseif($tmpShop['provider_name']!=''){
				$this->assign('nickname',$tmpShop['provider_name']);
			}
			$this->assign('shopname',$oldShop['name']);
			$this->assign('caption',$caption);
			$this->assign('description',$tmpShop['description']);
			$this->assign('link',C('OUTSTREET_DIR').'/shop/view/id/'.$shop_id);
			$body = $this->fetch('email_request_edit_shop');
			
			if($tmpShop['user']['email'] != '') {
				$this->smtp_mail_log($tmpShop['user']['email'],$caption,$body,'',1,6);
			}elseif($tmpShop['provider_email']!=''){
				$this->smtp_mail_log($tmpShop['provider_email'],$caption,$body,'',1,6);				
			}
		}
		echo json_encode( array( 'result'=> '1', 'msg' => '完成審核!' ) );
	}

	public function shopAuditAddEdit() {
		$dbo = M( 'ShopTmpFields' );
		
		$id = @$_REQUEST[ 'id' ];
		
		$data = $dbo->where( array( 'id' => $id ) )->find();
		$status = $data['status'];
		$shop_id = $data['shop_id'];
		$brand = M('ShopBrand')->select();
		$shopDao=D('Shop');
		$data['mall_name']=$shopDao->where('id='.$data['parent_shop_id'])->getField('name');
		$sublocation = M('LocationSub')
			->join( 'location_main ON location_main.id = location_sub.main_location_id' )
			->field('location_sub.id AS location_sub_id, location_main.name AS main_location_name, location_sub.name AS sub_location_name')
			->order('main_location_id')->select();

		$ShopCateMap = M('ShopTmpCateMap')->where( array( 'shop_tmp_fields_id' => $id ) )->select();
		$shopTimeList = M('ShopTmpTime')->where( array( 'shop_tmp_fields_id' => $id ) )->select();
		$shopTime = array();
		foreach( $shopTimeList AS $val ) { $shopTime[$val['recurring_week']] = $val; }

		$this->assign('shop_cate',$shop_cate);
		$this->assign( 'id' , $id );
		$this->assign( 'data' , $data );
		$this->assign( 'brand' , $brand );
		$this->assign( 'status' , $status );
		$this->assign( 'affectedFields' , unserialize($data['affectedFields']) );
		$this->assign( 'sublocation' , $sublocation );
		$this->assign( 'ShopCateMap' , $ShopCateMap );
		$this->assign( 'shopTime' , $shopTime );
		$this->assign( 'picture' , $picture );
		$this->display();
	}
	
	public function getMainlocationid(){		
		$id=$_GET['sub_location_id'];
		$dbo=D('LocationSub');		
		$sub_location=$dbo->where('id='.$id)->find();
		$main_id=$sub_location['main_location_id'];
		$this->ajaxReturn($main_id, 'get', 1);
	}
	
	public function selshopcate(){
		//獲取Shop分類主目錄列表 壓入模板
		$cate = M('ShopCate')->getField('id,name');
		$level_1 = M('ShopCate')->where(array('level'=>1))->getField('id,parent_id');
		$level_2 = M('ShopCate')->where(array('level'=>2))->getField('id,parent_id');
		$level_3 = M('ShopCate')->where(array('level'=>3))->getField('id,parent_id');

		$cateList = array();
		foreach ( $level_1 AS $level_1_id => $level_1_parent_id )
		{
			if ($cate[ $level_1_id ])
				$cateList[ $level_1_id ][ 'name' ] = $cate[ $level_1_id ];
		}
		foreach ( $level_2 AS $level_2_id => $level_2_parent_id )
		{
			if ($cateList[$level_2_parent_id][ 'name' ] && $cate[ $level_2_id ])
				$cateList[$level_2_parent_id]['item'][ $level_2_id ][ 'name' ] = $cate[ $level_2_id ];
		}
		foreach ( $level_3 AS $level_3_id => $level_3_parent_id )
		{
			if ($cateList[$level_2[$level_3_parent_id]][ 'name' ] && $cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['name'] && $cate[ $level_3_id ])
				$cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['item'][ $level_3_id ][ 'name' ] = $cate[ $level_3_id ];
		}
		
		//Shop Cate Ref List
		$shopCateList = D('ShopCate')->field('id,reference_id')->select();
		$refList = array();
		foreach($shopCateList as $key=>$shopCate){
			$cateId = $shopCate['id'];
			$refId = $shopCate['reference_id'];
			if($refId!=0){
				$refList[$cateId][] = $refId;
				$refList[$refId][] = $cateId;
			}
		}
		foreach($refList as $key=>$ref){
			foreach($ref as $cateId){
				foreach($refList[$cateId] as $elem){
					$refList[$key][] = $elem;	
				}
			}
			$refList[$key] = array_unique($refList[$key]);
		}
			
		$this->assign('refList', $refList);
		$this->assign('cateList', $cateList);
		$this->display();
	}
	
	private function mapShopCateRef($shopcate){
		//mapping for shopcate reference id
		foreach($shopcate AS $key=>$cateId){
			$referenceId = D('ShopCate')->where("id=$cateId")->getfield('reference_id');
			if($referenceId!=0){
				$shopcate[$key] = $referenceId;		
			}
		}
		return array_unique($shopcate);
	}
	
	public function getMall(){
		$key=$_GET['key'];
		$mall_id = $_GET['mall_id'];
		$mainLocationId = $_GET['main_location_id'];
		$subLocationId = $_GET['sub_location_id'];
		$shopDao=D('Shop');
		$mainLocationDao = D('LocationMain');
		$subLocationDao = D('LocationSub');	
		$shopCateDao = D('ShopCate');
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');
		$cate_id = "175";
		
		if (!empty($key)){
			$where['name'] = array('like', '%'.$key.'%');
		}
		if (!empty($mainLocationId)){
			$where['main_location_id'] = $mainLocationId;
		}
		if (!empty($subLocationId)){
			$where['sub_location_id'] = $subLocationId;
		}
		if (!empty($mall_id)){
			$where['shop_fields.id'] = $mall_id;
		}

		$mallList = array();
		if (count($where)) {
			$where['cate_id'] = $cate_id;
			$where['is_audit']="1";
			$mallList = $shopDao->join('shop_cate_map on shop_cate_map.shop_id = shop_fields.id')->where($where)->limit(20)->order('shop_fields.id asc')->select();		
			foreach($mallList as $key=>$mall){
				$mainLoc = $mainLocationDao->find($mall['main_location_id']);
				$subLoc = $subLocationDao->find($mall['sub_location_id']);
				$mallList[$key]['main_location']['name']=$mainLoc['name'];
				$mallList[$key]['sub_location']['name']=$subLoc['name'];
			}
		}
		$this->ajaxReturn($mallList, 'get', 1);
	}
	
	public function doShopAuditAddEdit() {
		$id = @$_REQUEST[ 'id' ];		
		$dbo = M( 'ShopTmpFields' );
		$tmpdata=$dbo->where( array( 'id' => $id ) )->find();
		$shop_id = @$_REQUEST[ 'shop_id' ];

		if(empty($_REQUEST['cate_ids'])){
			echo json_encode(array('warning' => 'Fail','result' => false));			
			return;			
		}else{
			$shopcate = array();
			$shopcate = $_REQUEST["shopcate"];
			$cate_ids = $_REQUEST['cate_ids'];
			$cate_ids = explode(',', $cate_ids);
			$loop = true;
			$loopIds = $cate_ids;
			$childrenIds = array();
			$parentIds = array();
			while($loop) {
				$cate = M('ShopCate')->where(array('parent_id'=>array('in',$loopIds)))->select();
				foreach ($cate AS $val) { $loopIds[] = $val['id']; $parentIds[] = $val['parent_id']; }
				if (count($loopIds)) { $childrenIds = array_merge($childrenIds,$loopIds); $loopIds = array(); }
				else { $loop = false; }
			}

			$cate_ids = array_diff($childrenIds,$parentIds);
			if (!count($cate_ids)) { echo json_encode( array('warning' => '類別錯誤','result' => false) ); return; }			
			$cate_ids = $this->mapShopCateRef($cate_ids);
			M('ShopTmpCateMap')->where( array( 'shop_tmp_fields_id' => $id ) )->delete();
			
			foreach ( $cate_ids AS $cate_id ) { 
				M('ShopTmpCateMap')->data( array( 
				'shop_tmp_fields_id'=>$id,
				'cate_id'=>$cate_id,
				'shop_id'=>$shop_id ) )->add(); 
			}
		}

		if ($id) {
			$data = $_REQUEST;
							
			$sublocation = M('LocationSub')->where( array( 'id' => $data['sub_location_id'] ) )->find();
			$data[ 'sub_location_id' ] = $data['sub_location_id'];	
			$data[ 'main_location_id' ] = $sublocation[ 'main_location_id' ];
							
			if($data['brand_id']==''){						
				$data['brand_id']='0';
				$data['brand_name']=$data['brand_name'];						
			}else if($data['brand_id']=='0'){						
				$data['brand_id']='0';
				$data['brand_name']='';
			}else if($data['brand_id']>=1){
				$data['brand_id']=$data['brand_id'];
				$data['brand_name']='';
			}else if(in_array('brand_id',$affectedFields)||$tmpdata['status']=='add'){
				if($data['brand_id']==''){
					$data['brand_id']='0';
					$data['brand_name']=$data['brand_name'];
				}else if($data['brand_id']=='0'){
					$data['brand_id']='0';
					$data['brand_name']='';
				}else if($data['brand_id']>=1){
					$data['brand_id']=$data['brand_id'];
					$data['brand_name']='';
				}
			}

			if($_REQUEST['is_closed']){
				$data['shop_status']= '2';
			}else if($_REQUEST['is_renovation']){
				$data['shop_status']= '3';
			}else if($_REQUEST['is_moved']){
				$data['shop_status']= '1';
			}else{
				$data['shop_status']= '0';
			}
			$data['last_modify']=time();
	
			M('ShopTmpFields')->where( array( 'id' => $id ) )->data( $data )->save();			
			M('ShopTmpTime')->where( array( 'shop_tmp_fields_id' => $id ) )->delete();
			for( $i=0; $i<8; $i++ ) {
				if ( @$data['work_start_time_'.$i] && @$data['work_end_time_'.$i] ) {
					M('ShopTmpTime')->data( array(
							'shop_tmp_fields_id' => $id,
							'shop_id' => $shop_id,
							'start_time' => $data['work_start_time_'.$i],
							'end_time' => $data['work_end_time_'.$i],
							'recurring_week' => $i+1 ) )->add();
				}
			}
			
			echo json_encode( array( 'warning' => "修改成功",'result' => true ) );
			return;
		} else {
			echo json_encode( array( 'result' => false,'warning'=>'123' ) );
			return;
		}
	}
	
	public function shopAuditEditEdit() {

		$dbo = M( 'ShopTmpFields' );
		$id = @$_REQUEST[ 'id' ];
		
		$data = $dbo->where( array( 'id' => $id ) )->find();		
		$status = $data['status'];
		$shop_id = $data['shop_id'];
		$brand = M('ShopBrand')->select();
		$sublocation = M('LocationSub')
			->join( 'location_main ON location_main.id = location_sub.main_location_id' )
			->field('location_sub.id AS location_sub_id, location_main.name AS main_location_name, location_sub.name AS sub_location_name')
			->order('main_location_id')->select();

		$ShopCateMap = M('ShopTmpCateMap')->where( array( 'shop_tmp_fields_id' => $id ) )->select();
		$shopTimeList = M('ShopTmpTime')->where( array( 'shop_tmp_fields_id' => $id ) )->select();
		$shopTime = array();
		foreach( $shopTimeList AS $val ) { $shopTime[$val['recurring_week']] = $val; }
				

		$_data = M( 'ShopFields' )->where( array( 'id' => $shop_id ) )->find();
		$_data['mall_name']=M( 'ShopFields' )->where( array( 'id' =>$_data['parent_shop_id'] ) )->getField('name');
		$data['mall_name']=M( 'ShopFields' )->where( array( 'id' =>$data['parent_shop_id'] ) )->getField('name');		
		$_ShopCateMap = M('ShopCateMap')->where( array( 'shop_id' => $shop_id ) )->select();
		$_shopTimeList = M('ShopTime')->where( array( 'shop_id' => $shop_id ) )->select();
		$_shopTime = array();
		foreach( $_shopTimeList AS $val ) { $_shopTime[$val['recurring_week']] = $val; }
		$_shop_cate = M('ShopCate')->where( array( 'id' => $cate_id ) )->select();
		
		$this->assign( '_data' , $_data );
		$this->assign( '_shop_cate',$_shop_cate);
		$this->assign( '_ShopCateMap' , $_ShopCateMap );
		$this->assign( '_shopTime' , $_shopTime );
		$this->assign('shop_cate',$shop_cate);
		$this->assign( 'id' , $id );
		$this->assign( 'data' , $data );
		$this->assign( 'brand' , $brand );
		$this->assign( 'status' , $status );
		$this->assign( 'affectedFields' , unserialize($data['affectedFields']) );
		$this->assign( 'sublocation' , $sublocation );
		$this->assign( 'ShopCateMap' , $ShopCateMap );
		$this->assign( 'shopTime' , $shopTime );
		$this->assign( 'picture' , $picture );
		$this->display();
	}
	
	public function doShopAuditEditEdit() {
		$id = @$_REQUEST[ 'id' ];
		$dbo = M( 'ShopTmpFields' );
		$tmpdata=$dbo->where( array( 'id' => $id ) )->find();		
		$affectedFields=unserialize($tmpdata['affectedFields']);
		$shop_id = @$_REQUEST[ 'shop_id' ];			
		if(in_array('shopcate',$affectedFields)||$tmpdata['status']=='add'){			
			if(empty($_REQUEST['cate_ids'])){				
				echo json_encode(array('warning' => 'Fail','result' => false));			
				return;			
			}else{					
				$shopcate = array();
				$shopcate = $_REQUEST["shopcate"];
				$cate_ids = $_REQUEST['cate_ids'];
				$cate_ids = explode(',', $cate_ids);
				$loop = true;
				$loopIds = $cate_ids;
				$childrenIds = array();
				$parentIds = array();
				while($loop) {
					$cate = M('ShopCate')->where(array('parent_id'=>array('in',$loopIds)))->select();
					foreach ($cate AS $val) { $loopIds[] = $val['id']; $parentIds[] = $val['parent_id']; }
					if (count($loopIds)) { $childrenIds = array_merge($childrenIds,$loopIds); $loopIds = array(); }
					else { $loop = false; }
				}
				$cate_ids = array_diff($childrenIds,$parentIds);
				if (!count($cate_ids)) { echo json_encode( array('warning' => '類別錯誤','result' => false) ); return; }			
				$cate_ids = $this->mapShopCateRef($cate_ids);
				
				M('ShopTmpCateMap')->where( array( 'shop_tmp_fields_id' => $id ) )->delete();
					
				foreach ( $cate_ids AS $cate_id ) { 
					M('ShopTmpCateMap')->data( array( 
					'shop_tmp_fields_id'=>$id,
					'cate_id'=>$cate_id,
					'shop_id'=>$shop_id ) )->add(); 
				}
			}
		}

		if($id){
			$data = $_REQUEST;
								
			if(in_array('sub_location_id',$affectedFields)||$tmpdata['status']=='add'){
				$sublocation = M('LocationSub')->where( array( 'id' => $data['sub_location_id'] ) )->find();
				$data[ 'sub_location_id' ] = $data['sub_location_id'];	
				$data[ 'main_location_id' ] = $sublocation[ 'main_location_id' ];
			}
				
			if(in_array('new_brand',$affectedFields)||$tmpdata['status']=='add'){					
				if($data['brand_id']==''){						
					$data['brand_id']='0';
					$data['brand_name']=$data['brand_name'];						
				}else if($data['brand_id']=='0'){						
					$data['brand_id']='0';
					$data['brand_name']='';
				}else if($data['brand_id']>=1){
					$data['brand_id']=$data['brand_id'];
					$data['brand_name']='';
				}
			}else if(in_array('brand_id',$affectedFields)||$tmpdata['status']=='add'){					
				if($data['brand_id']==''){
					$data['brand_id']='0';
					$data['brand_name']=$data['brand_name'];						
				}else if($data['brand_id']=='0'){
					$data['brand_id']='0';
					$data['brand_name']='';						
				}else if($data['brand_id']>=1){
					$data['brand_id']=$data['brand_id'];
					$data['brand_name']='';
				}
			}
			
			if($_REQUEST['is_closed']){
				$data['shop_status']= '2';
			}else if($_REQUEST['is_renovation']){
				$data['shop_status']= '3';
			}else if($_REQUEST['is_moved']){
				$data['shop_status']= '1';
			}else{
				$data['shop_status']= '0';
			}

			unset($data['affectedFields']);
			$data['last_modify']=time();	
			M('ShopTmpFields')->where( array( 'id' => $id ) )->data( $data )->save();
			if(in_array('shoptime',$affectedFields)){					
				M('ShopTmpTime')->where( array( 'shop_tmp_fields_id' => $id ) )->delete();
				for( $i=0; $i<8; $i++ )
				{
					if ( @$data['work_start_time_'.$i] && @$data['work_end_time_'.$i] ) {
						M('ShopTmpTime')->data( array(
							'shop_tmp_fields_id' => $id,
							'shop_id' => $shop_id,
							'start_time' => $data['work_start_time_'.$i],
							'end_time' => $data['work_end_time_'.$i],
							'recurring_week' => $i+1 ) )->add();
					}
				}
			}				
			echo json_encode( array( 'warning' => '修改成功','result' => true ) );
			return;
		} else {
			echo json_encode( array( 'result' => false,'warning'=>'Fail' ) );
			return;
		}
	}

	public function listShopPicAudit() {
		$dbo = M( 'ShopTmpPicture' );
		$is_audit=$_REQUEST['is_audit'];
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'a.id';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'asc';

		$where = array();
		$where['a.shop_id'] = array( 'neq', 0 ); 
		if ( $_REQUEST['is_audit'] != 'all' ) {
			$where['a.is_audit'] = $_REQUEST['is_audit'] ? $_REQUEST['is_audit'] : 0;
		}

		$count = $dbo->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$picList = $dbo->field('a.*, b.name')->table( 'shop_tmp_picture a' )->join('shop_fields b ON a.shop_id = b.id')->where($where)->limit($limit)->order( $sort . ' ' . $sortorder )->select();
		$this->assign('is_audit',$is_audit);
		$this->assign('picList',$picList);
		$this->assign('sortorder',$sortorder);
		$this->assign('multipage',$multipage);
		$this->display();
    }
	
	public function doShopPicAudit() {
		$tmpId = $_REQUEST['tmpId'];
		$audit = $_REQUEST['audit'];
		$tmpShopPic = M('ShopTmpPicture')->where( array( 'id'=>$tmpId ) )->find();

		if ( $audit == 1 ) {		
			$tmpShopPic['create_time']=time();
			$data = $tmpShopPic;
			$file_path=$data['file_path'];
			$data['is_audit'] = 1;
			unset( $data[ 'id' ] );
			$id = M('ShopPicture')->data( $data )->add();
			M('ShopTmpPicture')->where( array( 'id' => $tmpId ) )->setField('is_audit', 1);
			$ori=C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH').'/'.$file_path;
			$new=C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH').'/'.$file_path;
			rename($ori,$new);
			unlink( C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH').'/'.$file_path );
			
			$ori=C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH').'/'.thumb_.$file_path;
			$new=C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH').'/'.thumb_.$file_path;
			rename($ori,$new);
			unlink( C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH').'/'.thumb_.$file_path );
			
			$tmpShop=D('Shop')->where( array( 'id' => $data['shop_id'] ) )->find();
			$shop_id=$data['shop_id'];
			$userDao=D('User');
			$tmpShop['user']=$userDao->where('id='.$data['user_id'])->find();	
			$caption = '你已成功加入商戶相片';
			$this->assign('nickname',$tmpShop['user']['nick_name']);
			$this->assign('shopname',$tmpShop['name']);
			$this->assign('caption',$caption);
			$this->assign('description',$tmpShop['description']);
			$this->assign('link',C('OUTSTREET_DIR') . '/shop/view/id/'.$shop_id);
			$body = $this->fetch('email_request_shop_picture');
			
			if($tmpShop['user']['email'] != '') {
				$this->smtp_mail_log($tmpShop['user']['email'],$caption,$body,'',1,7);
			}
		} else if ( $audit == 2 ) {
			M('ShopTmpPicture')->where( array( 'id' => $tmpId ) )->setField('is_audit', 2);
			$data = $tmpShopPic;
			$file_path=$data['file_path'];
			
			$ori=C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH').'/'.$file_path;
			$new=C('OUTSTREET_ROOT').'/'.C('REQUEST_FAIL_SHOP_UPFILE_PATH').'/'.$file_path;
			rename($ori,$new);
			unlink( C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH').'/'.$file_path );	
			
			$ori=C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH').'/'.thumb_.$file_path;
			$new=C('OUTSTREET_ROOT').'/'.C('REQUEST_FAIL_SHOP_UPFILE_PATH').'/'.thumb_.$file_path;
			rename($ori,$new);
			unlink( C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH').'/'.thumb_.$file_path );
		}
		echo json_encode( array( 'result'=> '1', 'msg' => '完成審核!' ) );
	}

	public function doShopOwnerAudit() {
		$id = $_REQUEST['id'];
		$audit = $_REQUEST['audit'];
		if ( $audit == 1 ) {
			$owner = M('ShopTmpOwner')->where(array('id'=>$id))->find();			
			if ( $owner['shop_id'] ) {
				M('ShopTmpOwner')->where(array('id'=>$id))->save(array('is_audit'=>1, 'approved_time'=>time()));
				$tmpShop=D('Shop')->where( array( 'id' => $owner['shop_id'] ) )->find();
				$shop_id=$owner['shop_id'];
				$userDao=D('User');

				$tmpShop['user']=$userDao->where('id='.$owner['user_id'])->find();	
				$caption = '你已成功登記成為店主';
				$this->assign('nickname',$tmpShop['user']['nick_name']);
				$this->assign('shopname',$tmpShop['name']);
				$this->assign('caption',$caption);
				$this->assign('description',$tmpShop['description']);
				$this->assign('link',C('OUTSTREET_DIR') . '/shop/view/id/'.$shop_id);
				$body = $this->fetch('email_request_shop_owner');
				
				if($tmpShop['user']['email'] != '') {
					$this->smtp_mail_log($tmpShop['user']['email'],$caption,$body,'',1,8);
				}
			} elseif ( $owner['shop_tmp_fields_id'] ) {
				$tmpShop=M('ShopTmpFields')->where( array( 'id' => $owner['shop_tmp_fields_id'] ) )->find();
				if($tmpShop['is_audit']=='0'){
					echo json_encode( array( 'result'=> '0', 'msg' => '相關商店還未approve!' ) );
					return;
				}else{
					M('ShopTmpOwner')->where(array('id'=>$id))->save(array('is_audit'=>1, 'approved_time'=>time()));
					$tmpShop=M('ShopTmpFields')->where( array( 'id' => $owner['shop_tmp_fields_id'] ) )->find();
					$shop_id=$tmpShop['shop_id'];
					$userDao=D('User');
					$tmpShop['user']=$userDao->where('id='.$owner['user_id'])->find();			
					$caption = '你已成功登記成為店主';
					$this->assign('nickname',$tmpShop['user']['nick_name']);
					$this->assign('shopname',$tmpShop['name']);
					$this->assign('caption',$caption);
					$this->assign('description',$tmpShop['description']);
					$this->assign('link',C('OUTSTREET_DIR') . '/shop/view/id/'.$shop_id);
					$body = $this->fetch('email_request_shop_owner');
					
					if($tmpShop['user']['email'] != '') {
						$this->smtp_mail_log($tmpShop['user']['email'],$caption,$body,'',1,8);
					}
				}
				M('ShopTmpFields')->where(array('id'=>$owner['shop_tmp_fields_id']))->setField('owner_id',$owner['user_id']);
			}
		} elseif ( $audit == 2 ) {
			M('ShopTmpOwner')->where(array('id'=>$id))->save(array('is_audit'=>2, 'approved_time'=>time()));
		}	
		echo json_encode( array( 'result'=> '1', 'msg' => '完成審核!' ) );
	}

	public function addShopOwnerRemark(){
		$id=$_GET['id'];
		$remark=$_GET['remark'];
		$success = D('Owner')->where('id='.$id)->setField('remark', $remark);
		$return = array();
		$return['result'] = $success;
		echo json_encode($return);
	}

}
	
?>