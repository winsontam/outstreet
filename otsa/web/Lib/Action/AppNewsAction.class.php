<?php

class AppNewsAction extends SaAction
{

	public function __construct()
	{
		parent::__construct();
	}

	public function listAll()
	{
		$title = $_REQUEST[ 'title' ];

		$dbo = D( 'AppNews' );

		$where = array();

		if ( $title )
		{
			$where[ 'title' ] = array( 'like', '%' . $title . '%' );
		}

		$count = $dbo
			->where( $where )
			->count( 'id' );

		import( 'ORG.Util.Page' );
		$pagination = new Page( $count, 100 );
		$multipage = $pagination->show();

		$rows = $dbo
			->where( $where )
			->limit( $pagination->firstRow . ',' . $pagination->listRows )
			->order( 'created_at ASC' )
			->relation( true )
			->select();

		$this->assign( 'rows', $rows );
		$this->assign( 'multipage', $multipage );
		$this->display();
	}

	public function add()
	{
		if ( $_POST )
		{
			$newsDbo = M( 'AppNews' );
			$newsPhotoDbo = M( 'AppNewsPhoto' );

			$data = $_REQUEST;
			$data[ 'created_at' ] = date( 'Y-m-d H:i:s' );
			$data[ 'updated_at' ] = date( 'Y-m-d H:i:s' );
			$news_id = $newsDbo->add( $data );

			foreach ( $data[ 'photos' ] AS $photo )
			{
				$file = $photo;
				$cover = ( $file == $data[ 'cover_file' ] ) ? 1 : 0;
				$newsPhotoDbo->add( compact( 'news_id', 'file', 'cover' ) );
				rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_news/' . $file );
				rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/small_' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_news/small_' . $file );
				rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/large_' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_news/large_' . $file );
			}

			echo '<html><body><script>';
			echo 'alert("新增成功");';
			echo 'window.location = "' . __APP__ . '/app_news/add";';
			echo '</script></body></html>';
		}
		else
		{
			$this->display( 'save' );
		}
	}

	public function edit()
	{
		$news_id = $_REQUEST[ 'id' ];

		if ( $_POST )
		{
			$newsDbo = M( 'AppNews' );
			$newsPhotoDbo = M( 'AppNewsPhoto' );

			$data = $_REQUEST;
			$data[ 'updated_at' ] = date( 'Y-m-d H:i:s' );
			$newsDbo->save( $data );

			$newsPhotoDbo->where( compact( 'news_id' ) )->delete();
			foreach ( $data[ 'currentPhotos' ] AS $file )
			{
				$cover = ( $file == $data[ 'cover_file' ] ) ? 1 : 0;
				$newsPhotoDbo->add( compact( 'news_id', 'file', 'cover' ) );
			}

			foreach ( $data[ 'photos' ] AS $file )
			{
				$cover = ( $file == $data[ 'cover_file' ] ) ? 1 : 0;
				$newsPhotoDbo->add( compact( 'news_id', 'file', 'cover' ) );
				rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_news/' . $file );
				rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/small_' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_news/small_' . $file );
				rename( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/large_' . $file, C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_news/large_' . $file );
			}

			echo '<html><body><script>';
			echo 'alert("修改成功");';
			echo 'window.location = "' . __APP__ . '/app_news/listAll";';
			echo '</script></body></html>';
		}
		else
		{
			$newsDbo = D( 'AppNews' );

			$news = $newsDbo
				->where( array( 'id' => $news_id ) )
				->relation( true )
				->find();

			$this->assign( 'news', $news );
			$this->display( 'save' );
		}
	}

	public function delete()
	{
		$news_id = $_POST[ 'id' ];

		if ( $news_id )
		{
			$newsDbo		 = D( 'AppNews' );
			$newsPhotoDbo	 = D( 'AppNewsPhoto' );

			$news = $newsDbo
				->where( array( 'id' => $news_id ) )
				->relation( true )
				->find();

			if ( $news )
			{
				foreach ( $news[ 'photos' ] AS $index => $photo )
				{
					unlink( C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_news/' . $photo[ 'file' ] );
					unlink( C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_news/small_' . $photo[ 'file' ] );
					unlink( C( 'OUTSTREET_ROOT' ) . '/Public/uploadimages/app_news/large_' . $photo[ 'file' ] );
				}

				$newsDbo->where( array( 'id' => $news_id ) )->delete();
				$newsPhotoDbo->where( compact( 'news_id' ) )->delete();
			}
		}
	}

	function doUploadPhoto()
	{
		import( 'ORG.Net.UploadFile' );
		import( 'ORG.Util.Image' );

		$upload = new UploadFile();
		$upload->allowExts = explode( ',', C( 'UPFILE_ALLOW_IMGEXT' ) );
		$upload->savePath = C( 'OUTSTREET_ROOT' ) . '/Public/tmp/';
		$upload->saveRule = 'time';
		$upload->uploadReplace = true;

		if ( $upload->upload() )
		{
			$info = $upload->getUploadFileInfo();
			$file = $info[ 0 ][ 'savename' ];

			$srcFile = C( 'OUTSTREET_ROOT' ) . '/Public/tmp/' . $file;
			$smallFile = C( 'OUTSTREET_ROOT' ) . '/Public/tmp/small_' . $file;
			$largeFile = C( 'OUTSTREET_ROOT' ) . '/Public/tmp/large_' . $file;

			Image::thumb( $srcFile, $smallFile, null, 200, 200 );
			Image::thumb( $srcFile, $largeFile, null, 1000, 1000 );

			echo json_encode( array( 'file' => $file ) );
		}
	}

	public function doDeletePhoto()
	{
		$filename = basename( $_REQUEST[ 'filename' ] );

		unlink( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/' . $filename );
		unlink( C( 'OUTSTREET_ROOT' ) . '/Public/tmp/thumb_' . $filename );
	}

}
