<?php

class OwnerAction extends SaAction {
	public function listAll(){
		$logtable = $this->genLogTable();
		$ownerDao=D('Owner');
		$shopOwnerMap = D('ShopOwnerMap');
		$brand = D('Brand');

		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'id';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';	
		$is_disabled=$_REQUEST['is_disabled'];	

		$where=array();
		if ( $_REQUEST['is_disabled'] != 'all'){
			if(isset($is_disabled)){
				$where['is_disabled'] = $is_disabled;
			}
		}

		$count = $ownerDao->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;		
		$ownerList = $ownerDao->where($where)->limit($limit)->order( $sort . ' ' . $sortorder )->select();				
		foreach($ownerList as $key=>$owner){
			$brand_id = $brand->where('owner_id='.$owner['id'])->getField('id');
			$shop_id = $shopOwnerMap->where('owner_id='.$owner['id'])->getField('shop_id');
			if($brand_id){
				$ownerList[$key]['admin_brand_id'] = $brand_id;
			}elseif($shop_id){
				$ownerList[$key]['admin_shop_id'] = $shop_id;				
			}else{
				$ownerList[$key]['admin_shop_id'] = 0;				
			}
		}
		$this->assign('logtable',$logtable);
		$this->assign('page',$page);
		$this->assign('multipage', $multipage);
		$this->assign('ownerList',$ownerList);
		$this->display();
	}

	public function auditowner(){
		$owner_id=$_REQUEST['owner_id'];
		$shop_id=$_REQUEST['shop_id'];
		$brand_id=$_REQUEST['brand_id'];
		$ownerDao=D('Owner');
		$shopOwnerMapDao = D('ShopOwnerMap');
		$shopDao = D('Shop');
		$brand = D('Brand');
		$auditaction=$_REQUEST['auditaction'];

		if($auditaction==0){
			$is_brand = false;
			if($shop_id){
				$existshop=$shopOwnerMapDao->where('shop_id='.$shop_id)->select();
				$shopList = $shopDao->where('id='.$shop_id)->select();
				foreach($shopList as $shop){
					if($shop['brand_id']!=0){
						$is_brand=true;	
					}
				}
			}

			if($brand_id){
				$where = array();
				$where['owner_id'] = array('neq',0);
				$where['id'] = $brand_id;	
				$existbrand=$brand->where($where)->select();
			}

			if($shop_id && count($existshop)!=0){
				$return['success']=0;
				$return['msg']=0;
				echo json_encode($return);
				exit();
			}/*elseif($shop_id && $is_brand){
				$return['success']=0;
				$return['msg']=2;
				echo json_encode($return);
				exit();	
			}*/elseif($brand_id && count($existbrand)!=0){
				$return['success']=0;
				$return['msg']=1;
				echo json_encode($return);
				exit();
			}else{ //map owner to shop/brand
				$ownerDao->query('delete from shop_owner_map where owner_id='.$owner_id);
				if($shop_id){$ownerDao->query('insert into shop_owner_map(shop_id,owner_id) values("'.$shop_id.'","'.$owner_id.'")');}
				if($brand_id){
					$ownerDao->query('update shop_brand set owner_id = 0 where owner_id='.$owner_id);
					$ownerDao->query('update shop_brand set owner_id = '.$owner_id.' where id = '.$brand_id);}
				}
		}else{
			//remove all map
			if($owner_id){
				$ownerDao->query('delete from shop_owner_map where owner_id='.$owner_id);
				$ownerDao->query('update shop_brand set owner_id = 0 where owner_id='.$owner_id);
			}
		}
		$data['is_disabled']=$auditaction;		
		$success=$ownerDao->where('id='.$owner_id)->save($data);
		if($success){
			$return['success']=1;
			$admin_id=$this->getUserid($_SESSION['username']);			
			if($auditaction==0){				
				$this->addLogTable($admin_id,$owner_id,'audit_1');
			}else{
				$this->addLogTable($admin_id,$owner_id,'audit_0');
			}
		}else{
			$return['success']=0;
		}
		echo json_encode($return);
	}

	public function selectshopid(){
		$shopDao=D('Shop');
		$brandDao = D('Brand');
		$ownerDao=D('User');
		$owner_id=$_REQUEST['owner_id'];
		$is_brand = $_REQUEST['is_brand'];
		
		if(!empty($_REQUEST['o_company_name'])){
			$o_company_name=$_REQUEST['o_company_name'];
		}else{
			$o_company_name=$ownerDao->where('id='.$owner_id)->getField('o_company_name');
		}
		if($is_brand==1){
			$where['name']=array('like', '%'.$o_company_name.'%');
			$shopList=$brandDao->where($where)->limit('0,5')->select();
		}else{
			$where['name']=array('like', '%'.$o_company_name.'%');
			$where['eng_name']=array('like', '%'.$o_company_name.'%');
			$where['_logic']='OR';
			$shopList=$shopDao->where($where)->limit('0,5')->select();
		}

		$this->assign('owner_id',$owner_id);	
		$this->assign('shopList',$shopList);
		$this->assign('o_company_name',$o_company_name);
		$this->display();
	}

	private function addLogTable($admin_id,$owner_id,$type) {
		$promotionLogDao		= D('SaLog');
		$logdata['admin_id']	= $admin_id;
		$logdata['class_id']	= $owner_id;
		$logdata['date']		= time();
		$logdata['type']		= $type;
		$logdata['class']		= 'owner';
		$promotionLogDao->create($logdata);
		$promotionLogDao->add();
	}

	private function getAdminid($username){
		$adminDao = D('Admin');
		$id=$adminDao->where('username="'.$username.'"')->getField('admin_id');		
		return($id);
	}

	private function getUserid($username){
		$adminDao = D('Admin');
		$id=$adminDao->where('username="'.$username.'"')->getField('admin_id');		
		return($id);
	}

	private function genLogTable() {
		$saLogDao=D('SaLog');
		$adminDao=D('Admin');
		$where['class']='owner';
		$logList=$saLogDao->limit(20)->where($where)->order('id desc')->select();
		foreach ($logList as $key=>$val){
			$logList[$key]['date']=date('Y-m-d H:i:s',$val['date']);
			$logList[$key]['user_name']=$adminDao->where('admin_id='.$val['admin_id'])->getField('username');
		}
		return ($logList);
	}	

}

?>