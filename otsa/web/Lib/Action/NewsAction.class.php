<?php
class NewsAction extends SaAction
{

    public function add()
	{
		$this->display();
    }

    public function doAdd()
	{
		$dbo = D( 'News' );

		$data = $_REQUEST;
		$data[ 'create_time' ] = time();
		$data[ 'status' ] = '1';
		$dbo->add( $data );
		echo json_encode( array( 'success' => true ) );
    }

    public function edit()
	{
		$dbo = D( 'News' );

		$id = @$_REQUEST[ 'id' ];
		
		$news = $dbo->where( array( 'id' => $id ) )->find();

		$this->assign( 'id' , $id );
		$this->assign( 'news' , $news );
		$this->display();
    }

    public function doEdit()
	{
		$dbo = D( 'News' );

		$id = @$_REQUEST[ 'id' ];

		if ( $id )
		{
			$data = $_REQUEST;
			$dbo->where( array( 'id' => $id ) )->data( $data )->save();
			echo json_encode( array( 'success' => true ) );
		}
		else
		{
			echo json_encode( array( 'success' => false ) );
		}
    }


    public function doDel()
	{
		$dbo = D( 'News' );

		$id = @$_REQUEST[ 'id' ];

		if ( $id )
		{
			$dbo->where( array( 'id' => $id ) )->delete();
		}
    }

    public function listAll()
	{
		$dbo = D( 'News' );

		$description = @$_REQUEST[ 'description' ];
		$start_date = @$_REQUEST[ 'start_date' ];
		$end_date = @$_REQUEST[ 'end_date' ];
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'create_time';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';

		$where = array();
		if ( $description ) { $where[ 'description' ] = array( 'like', '%' . $description . '%' ); }
		if ( $start_date && $end_date )
			{ $where[ 'create_time' ] = array( 'between', strtotime($start_date) . ',' . strtotime($end_date) ); }
		elseif ( $start_date )
			{ $where[ 'create_time' ] = array( 'gt', strtotime($start_date) ); }
		elseif ( $end_date )
			{ $where[ 'create_time' ] = array( 'lt', strtotime($end_date) ); }

		$count = $dbo->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$list = $dbo->where($where)->limit($limit)->order( $sort . ' ' . $sortorder )->select();

		$this->assign( 'list' , $list );
		$this->display();
    }

}
?>