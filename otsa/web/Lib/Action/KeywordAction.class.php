<?php

class keywordAction extends SaAction
{
    public function add()
	{
		$keywords = $this->rows( D( 'Keyword' )->order( 'id' )->limit( 10 )->select(), null, 'keyword' );
		$this->assign( 'keywords', $keywords );

		$types = $this->rows( D( 'Keyword' )->order( 'id' )->limit( 10 )->select(), null, 'type' );
		
		$this->assign( 'types', $types );
		$this->display();
    }

    public function doAdd()
	{
		
		$keywordDb = D( 'Keyword' );		
		$keywords = (array) @$_REQUEST[ 'keywords' ];
		$type = (array) @$_REQUEST[ 'type' ];
		
		if ( count( $keywords ) <= 10 )
		{
			$keywordDb->limit( 10 )->delete();
			$a = 0;
			foreach( $keywords AS $val )
			{
				if ( $val )
				{
					$data =array();
					$data[ 'keyword' ] = $val;
					$data[ 'type' ] = $type[$a];
					$keywordDb->add( $data );
				}
				$a++;
			}
			
		}
		
		$this->redirect( 'add' );
    }

}

?>