<?php

class StatisticsAction extends SaAction{

	public function index(){
		$this->display();
	}

	public function shop(){
		$shopDao	= D('Shop');
		$totalCount = $shopDao->field('id')->where(array('is_audit'=>1,'id'=>array('gt',0)))->select();

		$this->assign('shopStatTitle','統計每個商戶的信息');
		$this->assign('totalCount',count($totalCount));
		$this->display();
	}
	
	public function subShop() {
		$type = $_GET['type'];

		switch($type) {
			case 1:
				$shopDao	= D('Shop');
				$totalCount = $shopDao->field('id')->where(array('is_audit'=>1,'id'=>array('gt',0)))->select();
				$this->ajaxReturn(count($totalCount),'ok',1);
			break;
			case 2:
				$reviewDao	= D('Review');
				$totalCount = $reviewDao->field('shop_id')->where(array('is_audit'=>1,'shop_id'=>array('gt',0)))->group('shop_id')->select();
				$this->ajaxReturn(count($totalCount),'ok',1);
			break;
			case 3:
				$shopPictureDao = D('ShopPicture');
				$totalCount 	= $shopPictureDao->join('shop_fields on shop_picture.shop_id = shop_fields.id')->where(array('shop_fields.is_audit'=>1))->group('shop_id')->select();
				$this->ajaxReturn(count($totalCount),'ok',1);
			break;
			case 4:
				$eventDao 		= D('Event');
				$totalCount 	= $eventDao->join('shop_fields on shop_fields.id = event_fields.shop_id')->where(array('shop_fields.is_audit'=>1,'event_fields.is_audit'=>1))->group('event_fields.shop_id')->select();
				$this->ajaxReturn(count($totalCount),'ok',1);
			break;
			case 5:
				$userShopFavoriteDao = D('UserShopFavorite');
				$totalCount 	= $userShopFavoriteDao->join('shop_fields on user_shop_favorite.shop_id = shop_fields.id')->where(array('shop_fields.is_audit'=>1))->group('user_shop_favorite.shop_id')->select();
				
				$this->ajaxReturn(count($totalCount),'ok',1);
			break;
		}
	}
	
	public function initShop(){
		echo '伺服器回應: ok!';
	}
	
	public function doShop(){
		$type	= $_GET['type'];
		$limit	= $_GET['limit'];
		$datas  = array();
		
		switch($type) {
			case 1:
				$datas	= generateShopRecordStat($limit);
			break;
			case 2:
				$datas	= generateShopReviewStat($limit);
			break;
			case 3:
				$datas	= generateShopPicStat($limit);
			break;
			case 4:
				$datas	= generateEventStat($limit);
			break;
			case 5:
				$datas	= generateUserShopFavoriteStat($limit);
			break;
		}

		$this->ajaxReturn($datas,'ok',1);
	}
	
	public function event(){
		$eventDao = D('Event');
		$eventCount = $eventDao->count();

		$this->assign('eventCount', $eventCount);
		$this->display();
	}
	
	public function initEvent(){
		$statisticsEventDao = D('StatisticsEvent');
		$statisticsEventDao->query('truncate __TABLE__');
		echo '伺服器回應: ok!';
	}
	
	public function doEvent(){

		$limit = $_GET['limit'];

		$eventDao = D('Event');
		$statisticsEventDao = D('StatisticsEvent');
		$eventInfos = $eventDao->limit($limit)->select();

		$eventReviewDao = D('EventReview');
		$eventPictureDao = D('EventPicture');
		$reviewPictureDao = D('ReviewPicture');

		$datas = array();

		foreach($eventInfos as $eventInfo){
			$where = array();
			$where['event_id'] = $eventInfo['id'];

			$data = array();
			$data['event_id'] = $eventInfo['id'];
			$data['event_name'] = $eventInfo['name'];
			
			$where['is_audit'] = 1;
			$data['review_count'] = $eventReviewDao->where($where)->count();
			unset($where['is_audit']);
			$reviewList = $eventReviewDao->where($where)->select();			
			$data['picture_count'] = $eventPictureDao->where($where)->count();
			$data['review_picture_count'] = 0;
			foreach($reviewList as $review){
				$data['review_picture_count'] += $reviewPictureDao->where('review_id='.$review['id'])->count();	
			}
			$statisticsEventDao->add($data);
			$datas[] = $data;
		}

		$this->ajaxReturn($datas,'ok',1);
	}

	public function user(){
		$userDao = D('User');
		$userCount = $userDao->count();

		$this->assign('userCount', $userCount);
		$this->display();
	}
	
	public function initUser(){
		$statisticsUserDao = D('StatisticsUser');
		$statisticsUserDao->query('truncate __TABLE__');
		echo '伺服器回應: ok!';
	}
	
	public function doUser(){

		$limit = $_GET['limit'];

		$userDao = D('User');
		$statisticsUserDao = D('StatisticsUser');
		$userInfos = $userDao->limit($limit)->select();

		$shopDao = D('Shop');
		$reviewDao = D('Review');
		$eventReviewDao = D('EventReview');
		$eventDao = D('Event');
		$userShopFavoriteDao = D('UserShopFavorite');
		$userUserFavoriteDao = D('UserUserFavorite');
		$userMessageDao = D('UserMessage');
		$shopPictureDao = D('ShopPicture');
		$reviewPictureDao = D('ReviewPicture');
		$reviewCommendDao = D('ReviewCommend');

		$datas = array();

		foreach($userInfos as $userInfo){
			$where = array();
			$where['user_id'] = $userInfo['id'];
			$data = array();
			$data['user_id'] = $userInfo['id'];
			$data['nick_name'] = $userInfo['nick_name'];
			
			$where['is_audit'] = 1;
			$data['review_count'] = $reviewDao->where($where)->count();
			unset($where['is_audit']);
			
			$data['event_count'] = $eventDao->where($where)->count();
			$data['favorite_shop_count'] = $userShopFavoriteDao->where($where)->count();
			$data['message_count'] = $userMessageDao->where($where)->count();
			$data['submit_message_count'] = $userMessageDao->where('submit_user_id=' . $userInfo['id'])->count();
			$data['picture_count'] = $reviewPictureDao->where($where)->count();
			
			$data['favorite_user_count'] = $userUserFavoriteDao->where('user_id=' . $userInfo['id'])->count();
			$data['favorite_me_count'] = $userUserFavoriteDao->where('favorite_user_id=' . $userInfo['id'])->count();
			$data['commend_review_count'] = $reviewCommendDao->where("user_id='" . $userInfo['id'] . "'")->count();
			$statisticsUserDao->add($data);
			$datas[] = $data;
		}

		$this->ajaxReturn($datas,'ok',1);
	}

	public function locationMain(){
		$locationMainDao = D('LocationMain');
		$locationMainCount = $locationMainDao->count();

		$this->assign('locationMainCount', $locationMainCount);
		$this->display();
	}

	public function initLocationMain(){
		echo '伺服器回應: ok!';
	}

	public function doLocationMain(){

		$limit = $_GET['limit'];

		$locationMainDao = D('LocationMain');
		$locationMainList = $locationMainDao->limit($limit)->select();

		$shopDao = D('Shop');
		$eventDao = D('Event');

		$datas = array();
		foreach($locationMainList as $locationInfo){
			$where = array();
			$where['main_location_id'] = $locationInfo['id'];
			$where['is_audit'] = 1;

			$data = array();
			$data['name'] = $locationInfo['name'];
			$data['event_count'] = $eventDao->where($where)->count();
			$data['shop_count'] = $shopDao->where($where)->count();
				
			$locationMainDao->where("id=".$locationInfo['id'])->setField('shop_count',$data['shop_count']);
			$locationMainDao->where("id=".$locationInfo['id'])->setField('event_count',$data['event_count']);
			$datas[] = $data;
		}

		$this->ajaxReturn($datas,'ok',1);
	}

	public function locationSub(){
		$locationSubDao = D('LocationSub');
		$locationSubCount = $locationSubDao->count();
		$this->assign('locationSubCount', $locationSubCount);
		$this->display();
	}
	
	public function initLocationSub(){
		echo '伺服器回應: ok!';
	}

	public function doLocationSub(){

		$limit = $_GET['limit'];

		$locationSubDao = D('LocationSub');
		$locationSubList = $locationSubDao->limit($limit)->select();

		$shopDao = D('Shop');
		$eventDao = D('Event');

		$datas = array();
		foreach($locationSubList as $locationInfo){
			$where = array();
			$where['sub_location_id'] = $locationInfo['id'];
			$where['is_audit'] = 1;

			$data = array();
			$data['name'] = $locationInfo['name'];
			$data['event_count'] = $eventDao->where($where)->count();
			$data['shop_count'] = $shopDao->where($where)->count();

			$locationSubDao->where("id=".$locationInfo['id'])->setField('shop_count',$data['shop_count']);
			$locationSubDao->where("id=".$locationInfo['id'])->setField('event_count',$data['event_count']);
			$datas[] = $data;
		}

		$this->ajaxReturn($datas,'ok',1);
	}

	public function shopCate(){
		$shopCateDao = D('ShopCate');
		$shopCateCount = $shopCateDao->count();

		$this->assign('shopCateCount', $shopCateCount);
		$this->display();
	}
	
	public function initShopCate(){
		echo '伺服器回應: ok!';
	}
	
	public function doShopCate(){
		$limit = $_GET['limit'];
		$datas = generateAllShopCateStatistics($limit);
		$this->ajaxReturn($datas,'ok',1);
	}

	public function eventCate(){
		$eventCateDao = M('EventCate');
		$eventCateCount = $eventCateDao->count();

		$this->assign('eventCateCount', $eventCateCount);
		$this->display();
	}
	
	public function initEventCate(){
		echo '伺服器回應: ok!';
	}
	
	public function doEventCate(){
		$limit = $_GET['limit'];
		$datas = generateAllEventCateStatistics($limit);
		$this->ajaxReturn($datas,'ok',1);
	}

}

?>