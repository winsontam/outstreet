<?php

class MainAction extends SaAction {
	
    public function index() {
		
		$this->username = $_SESSION[ 'username' ];
		$this->display();
		
    }

	public function adminInfo() {
		
		$admintable = D('Admin');
		$admininfo = $admintable->where( array( 'username' => $_SESSION['username'] ) )->select();
		$status = 0;
		
		if( $_POST['submit'] ) {
			
			if( $admintable->where( array( 'password' => md5($_POST['old_pw']) ) )->select()) {
				
				if( $_POST['new_pw'] == $_POST['old_pw'] ) {
					
					$status = 2;
					$actionInfo = '修改失敗!(新密碼與舊密碼相同)';
					
				} elseif( $_POST['new_pw'] == $_POST['confirm_pw'] ) {
				
					$status = 1;
					$actionInfo = '修改成功!';
					$admintable->where( array( 'username' => $_SESSION['username'] ) )->setField('password', md5($_POST['confirm_pw']));
				
				} else {
					
					$status = 2;
					$actionInfo = '修改失敗!(新密碼與確認密碼不符)';	
					
				}
				
			} else {
			
				$status = 2;
				$actionInfo = '修改失敗!(舊密碼錯誤)';	
				
			}
			
		}
		
		$this->assign( 'status' , $status );
		$this->assign( 'actionInfo' , $actionInfo );
		$this->assign( 'admininfo' , $admininfo );
		$this->display();

    }

    public function logout() {
		unset( $_SESSION[ 'username' ] );
		unset( $_SESSION[ 'isLogged' ] );
		$this->redirect( 'index/index' );
    }

}

?>