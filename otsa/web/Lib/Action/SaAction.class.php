<?php

class SaAction extends Action {

	public function __construct() {
		parent::__construct();

		include COMMON_PATH.'shopform.php';

		if ( strtolower( MODULE_NAME ) != 'index' && !@$_SESSION[ 'isLogged' ] ) {
			$this->redirect( 'index/index' ); 
			exit();
		} else if ( ( strtolower( MODULE_NAME ) != 'main' && strtolower( MODULE_NAME ) != 'index' && strtolower( MODULE_NAME ) != 'shopbrand') && (!@$_SESSION[ 'isLogged' ] || $_SESSION[ 'username' ] == 'henry') ) {
			$this->redirect( 'Shopbrand/listAll' ); 
			exit();
		}
	}

	protected function rows( $array, $key = null , $item = null , $isMultiItem = false ) {
		$rows = array();
		if ( $key && $item ) {
			if ( $isMultiItem ) { foreach ( $array AS $row ) { $rows[ $row[ $key ] ][] = $row[ $item ]; } } 
			else { foreach ( $array AS $row ) { $rows[ $row[ $key ] ] = $row[ $item ]; } } 
		}
		elseif ( $key ) {
			if ( $isMultiItem ) { foreach ( $array AS $row ) { $rows[ $row[ $key ] ][] = $row; } } 
			else { foreach ( $array AS $row ) { $rows[ $row[ $key ] ] = $row; } } 
		}
		elseif ( $item ) {
			foreach ( $array AS $row ) { $rows[] = $row[ $item ]; }
		}
		else {
			foreach ( $array AS $row ) { $rows[] = $row; }
		}
		return $rows;
	}

	/********* Type *********/
	/* 1. SA */
	/********* Source *********/
	/* 1. Event Review */
	/* 2. Member Validation */
	/* 3. Add Shop */
	/* 4. Shop Review */
	/* 5. Request Add Shop */
	/* 6. Request Edit Shop */
	/* 7. Request Add Shop Photo */
	/* 8. Request Shop Owner */
	public function smtp_mail_log($sendto_mail, $subject='', $body='', $sendto_name='',$type, $source){

		vendor("phpmailer.class#phpmailer");
		$mail = new PHPMailer();
		$mail->IsSMTP(); // set mailer to use SMTP	
		$mail->SMTPAuth = true; // turn on SMTP authentication
		$mail->Host = C('SMTP_SERVER'); // specify main and backup server
		//$mail->SMTPSecure = "ssl";
		$mail->Port = 25;
		$mail->Username = C('SMTP_USERNAME'); // SMTP username
		$mail->Password = C('SMTP_PASSWORD'); // SMTP password
		$mail->From = C('SMTP_FROM');
		$mail->FromName = C('SMTP_FROMNAME');
		$mail->AddAddress($sendto_mail, $sendto_name);
		$mail->CharSet = 'utf8';
		$mail->Subject = $subject;
		$mail->Body = $body;
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->WordWrap = 50;
		$mail->MsgHTML($body);
		$mail->IsHTML(true);
		if(!$mail->Send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
			exit;
		}
		else {
			//Start Save Email Log
			$userDao = D('User');
			$where = array();
			$where['email'] = $sendto_mail;
			$userInfo = $userDao->where($where)->find();
			if(!empty($userInfo)){
				$memid = $userInfo['id'];
			}	
			
			//Start Insert to DB
			$emailLogDao = D('EmailLog');
			$emailLogDao->create();
			$data = array();
			$data['datetime'] = date('Y-m-d H:i:s');			
			$data['mem_id'] = $memid;
			$data['email_address'] = $sendto_mail;
			$data['type'] = $type;
			$data['source'] = $source;
			$emailLogDao->add($data);
			//End Insert to DB
				
			//End Save Email Log
		}
	}
	
	public function locationSub(){

		$mainLocationId = $_GET['mainlocationid'];

		$locationSubDao = D('LocationSub');
		 
		$where = array();
		$where['main_location_id'] = $mainLocationId;
		$SubLocationList = $locationSubDao->where($where)->select();
		 
		$this->ajaxReturn($SubLocationList);
	}
	
	public function getShopCateSubs(){
		$shopid = $_GET['shopid'];
		$type = $_GET['type'];
		if($type=='request'){
			$shopCateMapDao = D('ShopTmpCateMap');
			$table='shop_tmp_cate_map a';
		}else{
			$shopCateMapDao = D('ShopCateMap');
			$table='shop_cate_map a';
		}
		$where = array();
		if($type=='request'){
			$where['a.shop_tmp_fields_id'] = $shopid;
		}else{		
			$where['a.shop_id'] = $shopid;
		}
		$catelist = $shopCateMapDao->field('name AS cate_name, a.cate_id')->table($table)->join('shop_cate b on a.cate_id = b.id')->where($where)->select();
		$tempArr =array();
		foreach($catelist AS $cate){
			$cateId = $cate['cate_id'];
			$where = array();
			$where['reference_id'] = $cateId;
			$list = D('ShopCate')->where($where)->select();
			if(sizeof($list)>0){
				foreach($list as $elem){
					array_push($tempArr,array('cate_name'=>$elem['name'], 'cate_id'=>$elem['id']));		
				}

			}
		}
		if(sizeof($tempArr)>0){
			$catelist = array_merge($catelist,$tempArr);
		}
		$this->ajaxReturn($catelist, '', 1);
	}

}

?>