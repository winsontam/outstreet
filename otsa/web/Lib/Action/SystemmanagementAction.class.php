<?php

class SystemmanagementAction extends SaAction {
	
	public function main() {
		
		$shopDao = D('Shop');
		$shopCount = $shopDao->count('id');
		$this->assign('shopCount',$shopCount);

		$webserver = 'PHP v'.PHP_VERSION;
		$webserver .= @ini_get('safe_mode') ? ' Safe Mode' : NULL;
		$fileupload = @ini_get('file_uploads') ? ini_get('upload_max_filesize') : '<font color="red">'.$lang['no'].'</font>';
		$magic_quote_gpc = get_magic_quotes_gpc() ? 'On' : 'Off';
		$debugmode = C('DEBUG_MODE') ? 'On' : 'Off';
		$urlmodel = '普通模式';
		switch (C('URL_MODEL')) {
			case 1 :
				$urlmodel = 'PATHINFO';
				break;
			case 2 :
				$urlmodel = 'REWRITE';
				break;
			case 3 :
				$urlmodel = '兼容模式';
				break;
		}

		$this->assign('os', PHP_OS);
		$this->assign('webserver', $webserver);
		$this->assign('fileupload', $fileupload);
		$this->assign('debugmode', $debugmode);
		$this->assign('urlmodel', $urlmodel);
		$this->assign('magic_quote_gpc', $magic_quote_gpc);
		$this->display();
		
	}

	public function backuplist(){

		$status = 0;
		$actionInfo = '創建備份文件將占用大量磁盤空間,請及時刪除沒用的備份文件.<br>恢復備份數據非常需時,恢復前請謹慎考慮!';

		if($_GET['do'] == 'backup') {
			
			$this->backup();
			$status = 1;
			$actionInfo = '新備份文件創建成功!';
			
		}elseif($_GET['do'] == 'restore') {
			
			$this->backup();
			
			$filename = $_GET['filename'];
			
			set_time_limit(1800);

			$sql   = file_get_contents($_SERVER['DOCUMENT_ROOT'] .'/'. C('DB_BACKUP_PATH').$filename);
			$sql   = str_replace("\r\n", "\n", $sql);
			
			if(false === $this->patchExecute($sql)) {
				
				$status = -1;
				$actionInfo = '恢復過程出現錯誤,請聯繫技術支援!';
				
			}else{
				
				$status = 1;
				$actionInfo = '恢復成功!並且已備份恢復前數據';
				
			}
				
		}

		if(isset($_POST['submit'])) {
			
			$filearr = $_POST['filename'];
			foreach($filearr as $file)  unlink($_SERVER['DOCUMENT_ROOT'] .'/'. C('DB_BACKUP_PATH') . $file);
			$status = 1;
			$actionInfo = '備份刪除成功!';
			
		}

		$filelist = scandir($_SERVER['DOCUMENT_ROOT'] .'/'. C('DB_BACKUP_PATH'));
		$backuplist = array();

		foreach($filelist as $file) {
			
			if ($file != "." && $file != "..") {
				
				$backup = array();
				$backup['filename'] = $file;
				$backup['createtime'] = filectime($_SERVER['DOCUMENT_ROOT'] .'/'. C('DB_BACKUP_PATH') . $file);
				$backuplist[]=$backup;
				
			}
			
		}
		
		import('ORG.Util.Page');
		$count = count($filelist)-2;
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		
		if ( $_REQUEST["p"] ) $offset = ($_REQUEST["p"]-1)*10;
		else $offset = 0;
		set_time_limit(60);

		$this->assign('offset',$offset);
		$this->assign('multipage',$multipage);
		$this->assign('backuplist',$backuplist);
		$this->assign('status', $status);
		$this->assign('actionInfo', $actionInfo);
		$this->display();
		
	}

	public function inputlist() {
		
		$dao = D('Input'); 
		$status = 0;
		$actionInfo = '刪除後不可恢復,請謹慎操作';

		if($_POST['submit'] == '添加') {
			
			$data = $dao->create();
			if($data)$dao->add($data);
			$status = 1;
			$actionInfo = '添加成功';
			
		}elseif($_POST['submit'] == '刪除所選') {
				
			$ids = $this->implode($_POST['id']);
			$where = array();
			$where['id'] = array('in', $ids);
			$dao->where($where)->delete();
			$status = 1;
			$actionInfo = '刪除成功';
			
		}

		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'id';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'asc';

		$count = $dao->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		
		$inputlist = $dao->limit($limit)->order( $sort . ' ' . $sortorder )->select();
		
		$this->assign('multipage' , $multipage );
		$this->assign('status', $status);
		$this->assign('actionInfo', $actionInfo);
		$this->assign('inputlist', $inputlist);
		$this->display('inputlist');
		
	}
	

	protected function backup() {

		import("Think.Db.Db"); 
		$db = DB::getInstance();  		
		$tables = $db->getTables(C('DB_NAME'));
		$sql  = "-- OUTSTREET SQL Dump\n";
		
		foreach($tables as $key=>$table) {
			
			$sql .= "-- \n-- 表格的結構 `$table`\n-- \n";
			$sql .= "DROP TABLE `$table`;\n";
			$info = $db->query("SHOW CREATE TABLE  $table");
			$sql .= $info[0]['Create Table'];
			$sql .= ";\n-- \n-- 導出表格中的數據 `$table`\n--\n";
			$result = $db->query("SELECT * FROM $table ");
			
			foreach($result as $key=>$val) {
				
				foreach ($val as $k=>$field) {
					
					if(is_string($field)) $val[$k] = '\''.$db->escape_string($field).'\'';
					elseif(empty($field)) $val[$k] =  'NULL';
					
				}
				
				$sql .= "INSERT INTO `$table` VALUES (".implode(',',$val).");\n";
			}
			
		}
		
		$filename   =  time();
		file_put_contents($_SERVER['DOCUMENT_ROOT'] .'/'. C('DB_BACKUP_PATH').$filename.'.sql',trim($sql));
		
	}		

	protected function patchExecute($querySql) {
		
		import("Think.Db.Db"); 
		$db = DB::getInstance(); 
		
		if(is_string($querySql)) $querySql   =  explode(";\n", trim($querySql)) ;

		$ret = array();
		$num = 0;
		
		foreach($querySql as $query) {
			
			$queries = explode("\n", trim($query));
			
			foreach($queries as $query) $ret[$num] .= $query[0] == '#' || $query[0].$query[1] == '--' ? '' : $query;
			$num++;
			
		}

		foreach($ret as $query) {
			
			if(!empty($query)) {
				
				$result   = $db->execute($query);
				
				if(false === $result) {
					
					echo $query;
					return false;
					
				}
				
			}
			
		}
		
		return $result;
		
	}
	
	public function implode($arr) {
		
		return implode(",", (array)$arr);
		
	}
	
}

?>