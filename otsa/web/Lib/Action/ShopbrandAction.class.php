<?php

class ShopbrandAction extends SaAction {

	/* START from front end shop action */
	private function _subcate($cateids, $shopid, $shopname){

		$shopCateSubDao = M('ShopCateSub');
		$shopCateSubMapDao = M('ShopCatesubMap');

		$where = array();
		$where['shop_id'] = $shopid;
		$shopCateSubMapDao->where($where)->delete();

		$cateids = explode(',',$cateids);

		foreach($cateids as $cateid){
			if (!empty($cateid)){
				$cateInfo = $shopCateSubDao->find($cateid);
				if ($cateInfo){
					$data = array();
					$data['shop_id'] = $shopid;
					$data['shop_name'] = $shopname;
					$data['cate_id'] = $cateInfo['id'];
					$data['cate_name'] = $cateInfo['name'];
					$shopCateSubMapDao->data($data)->add();
				}
			}
		}

	}
	/* END from front end shop action */

    public function edit() {
		$dbo = D( 'Shopbrand' );
		$id = @$_REQUEST[ 'id' ];
		$data = $dbo->where( array( 'id' => $id ) )->find();

		$this->assign( 'id' , $id );
		$this->assign( 'data' , $data );
		$this->display();
    }

	public function doAdd() {

		$dbo = D( 'Shopbrand' );

		$_REQUEST[ 'name' ] = trim(@$_REQUEST[ 'name' ]);
		if($_REQUEST[ 'file_name' ]) $file_path = 'brand/'.$_REQUEST[ 'file_name' ];
		$same = $dbo->where( array( 'name' => $_REQUEST[ 'name' ] ) )->count();

		if ($same > 0) {
			echo json_encode( array('result'=> '0','msg' => '已有此商戶品牌!') );
		} else {
			$data = $_REQUEST;
			if($file_path) $data['file_path'] = $file_path;
			$dbo->data($data)->add();
			echo json_encode( array( 'result'=> '1', 'msg' => '新增商戶品牌成功!') );
		}

    }

	public function add_doUpload() {
		import('ORG.Net.UploadFile');
		$upload = new UploadFile();
		$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));

		$upload->savePath = C('OUTSTREET_ROOT') . '/Public/uploadimages/shop/brand/';
		$upload->saveRule = 'time';

		if($upload->upload()) {
			$info =  $upload->getUploadFileInfo();
			echo json_encode( array( 'file' => $info[0]['savename'] ) );
		}
	}

    public function doEdit() {

		$dbo = D('Shopbrand');

		$id = $_REQUEST['id'];
		$_REQUEST[ 'name' ] = trim(@$_REQUEST[ 'name' ]);
		if($_REQUEST[ 'file_name' ]) $file_path = 'brand/'.$_REQUEST[ 'file_name' ];

		$same = $dbo->where( array( 'id'=>array('neq',$id), 'name' => $_REQUEST[ 'name' ] ) )->count();

		if($same > 0) {
			echo json_encode( array('result'=> '0','msg' => '已有此商戶品牌!') );
		} else {
			$data = $_REQUEST;
			if($file_path) $data['file_path'] = $file_path;

			$dbo->where( array( 'id' => $id ) )->save($data);
			echo json_encode( array( 'result'=> '1', 'brand_name' => $brand_name , 'msg' => '修改商戶品牌成功!') );
		}

    }

	public function deletePic(){

		$id = $_REQUEST['id'];
		$file_path = trim(urldecode($_REQUEST['file_path']));
		if($file_path !='') $file_path = 'brand/'.$file_path;

		$dbo = D('Shopbrand');
		$where = array();
		$where['id'] = $id;
		$list = $dbo->where($where)->find();

		if($list['file_path'] == '') {
			unlink(C('OUTSTREET_ROOT') . '/Public/uploadimages/shop/'.$file_path);
		} else {
			$where = array();
			$where['id'] = $id;
			$dbo->where($where)->setField('file_path', '');

			unlink(C('OUTSTREET_ROOT') . '/Public/uploadimages/shop/'.$list['file_path']);
		}

		echo json_encode( array( 'result' => '1') );
	}

	public function deletePic_shopEdit(){
		$id = $_REQUEST['id'];
		$from = $_REQUEST['from'];

		Import('@.ORG.SimpleAcl');
		$userInfo = SimpleAcl::getLoginInfo();

		$where = array();
		$where['id'] = $id;

		$dao = D('ShopPicture');
		$picList = $dao->where($where)->select();
		foreach($picList as $pic){
			unlink(C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH').$pic['file_path']);
			unlink(C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH').'thumb_' . $pic['file_path']);
		}
		$dao->where($where)->delete();
		$this->display();
	}

    public function listAll() {
		$dbo = D( 'Shopbrand' );

		$name = @$_REQUEST[ 'name' ];
		$isFile = @$_REQUEST[ 'isFile' ];
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'id';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'asc';

		$where = array();
		if ( $name ) { $where[ 'name' ] = array( 'like', '%' . $name . '%' ); }
		if ( $isFile ) { $where[ 'file_path' ] = array( 'neq', '' ); }

		$count = $dbo->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$list = $dbo->where($where)->limit($limit)->order( $sort . ' ' . $sortorder )->select();

		$this->assign( 'list' , $list );
		$this->assign( 'sort' , $sort );
		$this->assign( 'sortorder' , $sortorder );
		$this->assign( 'multipage' , $multipage );
		$this->display();
    }

	public function Index(){
		$this->display();
	}

	public function shoplist(){
		$shopspecialDao=D('ShopSpecial');
		$shopDao = D('Shop');
		$status = 0;

		if (isset($_POST['submit'])){

			$ids = implode(',',$_POST['id']);

			$where = array();
			$where['shop_id'] = array('in', $ids);

			$userShopFavoriteDao = D('UserShopFavorite');
			$userShopFavoriteDao->where($where)->delete(); //刪除用戶對該商戶的收藏
			$usershopRequestDao = D('UsershopRequest');
			$usershopRequestDao->where($where)->delete();//刪除用戶對該商戶管理的請求

			$reviewRateDao = D('ReviewRate');
			$reviewRateDao->where($where)->delete(); //刪除該商戶的評論的Rate

			$reviewDaoDao = D('Review');
			$reviewDaoDao->where($where)->delete(); //刪除該商戶的評論

			$reviewPictureDao = D('ReviewPicture');
			$picList = $reviewPictureDao->where($where)->select();
			foreach($picList as $pic){
				unlink($_SERVER['DOCUMENT_ROOT'] .'/'. C('REVIEW_UPFILE_PATH') . $pic['shop_id'] . '/' . $pic['file_path']);
				unlink($_SERVER['DOCUMENT_ROOT'] .'/'. C('REVIEW_UPFILE_PATH') . $pic['shop_id'] . '/thumb_' . $pic['file_path']);
			}

			$reviewPictureDao->where($where)->delete();

			$shopPictureDao = D('ShopPicture');
			$picList = $shopPictureDao->where($where)->select();
			foreach($picList as $pic){
				unlink($_SERVER['DOCUMENT_ROOT'] .'/'. C('SHOP_UPFILE_PATH') . $pic['file_path']);
				unlink($_SERVER['DOCUMENT_ROOT'] .'/'. C('SHOP_UPFILE_PATH') . 'thumb_' . $pic['file_path']);
			}

			$shopPictureDao->where($where)->delete();

			$where = array();
			$where['id'] = array('in',$ids);
			$shopDao->where($where)->delete(); //最後把商戶删了

			$status = 1;
			$actionInfo = '刪除成功';
		}

		if (is_numeric($authid = $_GET['authid'])) {
			$shopInfo = $shopDao->relation(true)->find($authid);
			$where = array();
			$where['id'] = $authid;
			$audit = 1;
			if ($shopInfo['is_audit'] == 1) {
				$audit = 0;
			}
			$shopDao->where($where)->setField('is_audit', $audit);
			if($audit == 1){
				generateShopCateStatByShopID($authid,'add');
			}
			if($audit == 0){
				generateShopCateStatByShopID($authid,'delete');
			}

			generateMainLocationStatistics($shopInfo['main_location_id']);
			generateSubLocationStatistics($shopInfo['sub_location_id']);
		}

		//設置分頁代碼
		$shopid = @$_REQUEST[ 'shopid' ];
		$name = @$_REQUEST[ 'name' ];
		$address = @$_REQUEST[ 'address' ];
		$start_date = @$_REQUEST[ 'start_date' ];
		$end_date = @$_REQUEST[ 'end_date' ];
		$ownerid = @$_REQUEST[ 'ownerid' ];
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'id';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'asc';
		$where = array();
		if ( $shopid ) { $where[ 'id' ] = array( 'like', '%' . $shopid . '%' ); }
		if ( $name ) { $where[ 'name' ] = array( 'like', '%' . $name . '%' ); }
		if ( $address ) { $where[ 'address' ] = array( 'like', '%' . $address . '%' ); }
		if ( $start_date && $end_date )
			{ $where[ 'create_time' ] = array( 'between', strtotime($start_date) . ',' . strtotime($end_date) ); }
		elseif ( $start_date )
			{ $where[ 'create_time' ] = array( 'gt', strtotime($start_date) ); }
		elseif ( $end_date )
			{ $where[ 'create_time' ] = array( 'lt', strtotime($end_date) ); }
		if ( $ownerid )
			{ $where[ '_string' ] = '( id IN(SELECT shop_id FROM shop_owner_map WHERE owner_id = "' . $ownerid . '") OR brand_id IN(SELECT id FROM shop_brand WHERE owner_id = "' . $ownerid . '"))'; }

		$count = $shopDao->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		$shopList = $shopDao->where($where)->limit($limit)->order( $sort . ' ' . $sortorder )->select();

		$shopIds = array();
		foreach ( $shopList AS $key=>$val ){
			$shopIds[] = $val['id'];
			$where=array();
			$where['shop_id']=$val['id'];
			$where['rank']=array('neq',0);
			$where['type']='shop_index';
			$shopList[$key]['is_special']=$shopspecialDao->where($where)->getField('rank');
			$shopList[$key]['is_special_type']=$shopspecialDao->where($where)->getField('cate_id');
			if ( $owner_id = M('ShopOwnerMap')->where(array('shop_id'=>$val['id']))->getField('owner_id') )
				{ $shopList[$key]['owner_id']=$owner_id.' (商戶)'; }
			elseif ( $owner_id = M('ShopBrand')->where(array('id'=>$val['brand_id']))->getField('owner_id') )
				{ $shopList[$key]['owner_id']=$owner_id.' (品牌)'; }
			$where=array();
			$where['rank']=0;
			$where['type']='index_index';
			$shopList[$key]['is_new']=$shopspecialDao->where($where)->getField('shop_id');
		}

		$shopCateDao=D('ShopCate');
		$ShopCateMapDao = D('ShopCateMap');
		$shopTypeList = array();
		$ShopCateList = $ShopCateMapDao
			->join('shop_cate ON shop_cate_map.cate_id = shop_cate.id')
			->field('shop_cate_map.shop_id,shop_cate.id, GROUP_CONCAT(shop_cate.name) AS cate_name')
			->where( array( 'shop_id' => array( 'in', $shopIds ) ) )
			->group('shop_id')
			->select();

		foreach ( $ShopCateList AS $val ) {
			$shopTypeList[$val['shop_id']] = $val['cate_name'];
			$level1id=$this->checkcate($val['id']);
			$topLevelList[$val['shop_id']]['id']=$level1id;
			$topLevelList[$val['shop_id']]['name']=$shopCateDao->where('id='.$level1id)->getField('name');
		}
		$keywordList = $shopDao->field('id, keyword')->where( array( 'id' => array( 'in', $shopIds ) ) )->group('id')->select();
		foreach ( $keywordList AS $val ) { $shopKeywordList[$val['id']] = $val['keyword'];}

		if ( $_REQUEST["p"] ) $page = $_REQUEST["p"];
		$this->assign('topLevelList',$topLevelList);
		$this->assign('shopTypeList',$shopTypeList);
		$this->assign('page',$page);
		$this->assign('link',C('OUTSTREET_DIR') . '/shop/view/id/');
		$this->assign('status', $status);
		$this->assign('actionInfo', $actionInfo);
		$this->assign('multipage', $multipage);
		$this->assign('shopList', $shopList);
		$this->assign('shopKeywordList', $shopKeywordList);
		$this->display();
	}

	public function upgrade(){
		$shopDao=D('Shop');
		$shopspecialDao=D('ShopSpecial');
		$action=$_REQUEST['action'];
		$shop_id=$_REQUEST['shop_id'];
		$is_special_type=$_REQUEST['is_special_type'];
		$isspecialList=$shopDao->join('shop_special ON shop_fields.id=shop_special.shop_id')->where('rank>0 and cate_id='.$is_special_type)->select();
		$isspecialcount=$shopDao->join('shop_special ON shop_fields.id=shop_special.shop_id')->where('rank>0 and cate_id='.$is_special_type)->count();
		if($isspecialcount==2){
			$full=true;
			$this->assign('isspecialList', $isspecialList);
			$this->assign('is_special_type',$is_special_type);
			$this->assign('shop_id',$shop_id);
		}else{
			$full=false;
			$where['cate_id']=$is_special_type;
			for($i=1;$i<=2;$i++){
				$where['rank']=$i;
				$getVal=$shopspecialDao->where($where)->select();
				if($getVal==''){
					$data['rank']=$i;
					$data['cate_id']=$is_special_type;
					$data['shop_id']=$shop_id;
					$data['type']='shop_index';
					$shopspecialDao->add($data);
					break;
				}
			}
		}
		$this->assign('full', $full);
		$this->display();
	}

	public function listAction(){
		$shopspecialDao=D('ShopSpecial');
		$shopDao=D('Shop');
		$type=$_REQUEST['type'];
		$action=$_REQUEST['action'];
		$shop_id=$_REQUEST['shop_id'];
		$is_special_type=$_REQUEST['is_special_type'];
		switch ($type){
			case 'downgrade':
				$replace_id=$_REQUEST['replace_id'];
				$data['shop_id']=$replace_id;
				$where=array();
				$where['shop_id']=$shop_id;
				$where['type']='shop_index';
				$success=$shopspecialDao->where($where)->save($data);
				if($success){
					echo json_encode( array( 'result'=> '1', 'msg' => '1','data'=>$shopList ) );
				}else{
					echo json_encode( array( 'result'=> '2', 'msg' => '2','data'=>$shopList ) );
				}
			break;
			case 'special_order':
				$oriorder=$shopspecialDao->where('shop_id='.$shop_id)->getField('rank');
				if($is_special_type && $oriorder){
					if($oriorder>$action){
						$data=array();
						$data['rank'] = $action;
						$where=array();
						$where['shop_id']=$shop_id;
						$where['type']='shop_index';
						$shopspecialDao->where($where)->save($data);
						$where=array();
						$where['type']='shop_index';
						$where['cate_id'] = $is_special_type;
						$where['rank'] = array('between',$action.',4');
						$where['shop_id'] = array('neq',$shop_id);
						$shopList=$shopspecialDao->where($where)->select();
						$count = count($shopList);
						foreach($shopList as $key => $val){
							$data['rank']=$data['rank']+1;
							$where=array();
							$where['type']='shop_index';
							$where['shop_id']=$val['shop_id'];
							$shopspecialDao->where($where)->save($data);
						}
					}else{
						$data=array();
						$data['rank'] = $action;
						$where=array();
						$where['type']='shop_index';
						$where['shop_id']=$shop_id;
						$shopspecialDao->where($where)->save($data);
						$where=array();
						$where['type']='shop_index';
						$where['cate_id'] = $is_special_type;
						$where['rank'] = array('between','2,'.$action);
						$where['shop_id'] = array('neq',$shop_id);
						$shopList=$shopspecialDao->where($where)->select();
						$count = count($shopList);
						foreach($shopList as $key => $val){
							$data['rank']=$data['rank']-1;
							$where=array();
							$where['type']='shop_index';
							$where['shop_id']=$val['shop_id'];
							$shopspecialDao->where('shop_id='.$val['shop_id'])->save($data);
						}
					}
					echo json_encode( array( 'result'=> '1', 'msg' => '1' ) );
				}else{
					echo json_encode( array( 'result'=> '2', 'msg' => '2' ) );
				}
			break;
			case 'is_new':
				$data['shop_id']=$_REQUEST['shop_id'];
				if($shopspecialDao->where('type="index_index"')->count()){
					$success=$shopspecialDao->where('type="index_index"')->save($data);
				}else{
					$data['type']='index_index';
					$success=$shopspecialDao->where('type="index_index"')->add($data);
				}
				if($success){
					echo json_encode( array( 'result'=> '1', 'msg' => '1' ) );
				}else{
					echo json_encode( array( 'result'=> '2', 'msg' => '2' ) );
				}
			break;
		}
	}

	private function checkcate($id){
		$shopCateDao=D('ShopCate');
		$cateinfo=$shopCateDao->where('id='.$id)->find();
		if($cateinfo['parent_id']!=0){
			$topid=$this->checkcate($cateinfo['parent_id']);
			return ($topid);
		}else{
			return ($id);
		}
	}

	public function uploadPic(){
		C('SHOW_PAGE_TRACE',false);
		$this->display();
	}

	public function uploadResult(){
		C('SHOW_PAGE_TRACE',false);

		if (isset($_REQUEST['submitBtn'])){
			$eleid = $_REQUEST['eleid'];
			$picstamp = $_REQUEST['picstamp'];
			$description = $_REQUEST['description'];
			$shopid = $_REQUEST['shopid'];
			$from = $_REQUEST['from'];

			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));
			$upload->savePath = C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH');
			$upload->saveRule = 'time';
			$upload->thumb = true;
			$upload->thumbMaxWidth = 120;
			$upload->thumbMaxHeight = 120;
			$upload->uploadReplace = true;

			if($upload->upload()) {
				$upinfo = $upload->getUploadFileInfo();
				$upfilepath = $upinfo[0]['savename'];
				$dao = null;
				$dao = D('ShopPicture');
				$upfile = $dao->create();

								echo '<script text="text/javascript">';
				echo 'alert("'.$upfilepath.'");';
				echo '</script>';


				$upfile['file_path'] = $upfilepath;
				$upfile['description'] = $description;
				$upfile['picstamp'] = $picstamp;
				$upfile['shop_id'] = 1;
				$upfile['admin_id'] = 1;

				$picid = $dao->add($upfile);

				$this->assign('eleid', $eleid);
				$this->assign('upfilepath', $upfilepath);
				$this->assign('description', $description);
				$this->assign('picid', $picid);

				/*echo '<script text="text/javascript">';
				echo 'setPic("'.$eleid.'","'.$upfilepath.'","'.$description.'","'.$picid.'");';
				echo '</script>';*/

				$this->display('uploadResult');
			}
			else{
				/*$upfileerr = $upload->getErrorMsg();
				$upfilepath = '';
				echo '<script text="text/javascript">';
				echo 'setPic("'.$eleid.'", "'.$upfilepath.'", "'.$upfileerr.'");';
				echo 'alert("'.$upfileerr.'");';
				echo '</script>';*/
			}
		}

	}

	public function selshopcate(){
		//獲取Shop分類主目錄列表 壓入模板
		$cate = M('ShopCate')->getField('id,name');
		$level_1 = M('ShopCate')->where(array('level'=>1))->getField('id,parent_id');
		$level_2 = M('ShopCate')->where(array('level'=>2))->getField('id,parent_id');
		$level_3 = M('ShopCate')->where(array('level'=>3))->getField('id,parent_id');

		$cateList = array();
		foreach ( $level_1 AS $level_1_id => $level_1_parent_id )
		{
			if ($cate[ $level_1_id ])
				$cateList[ $level_1_id ][ 'name' ] = $cate[ $level_1_id ];
		}
		foreach ( $level_2 AS $level_2_id => $level_2_parent_id )
		{
			if ($cateList[$level_2_parent_id][ 'name' ] && $cate[ $level_2_id ])
				$cateList[$level_2_parent_id]['item'][ $level_2_id ][ 'name' ] = $cate[ $level_2_id ];
		}
		foreach ( $level_3 AS $level_3_id => $level_3_parent_id )
		{
			if ($cateList[$level_2[$level_3_parent_id]][ 'name' ] && $cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['name'] && $cate[ $level_3_id ])
				$cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['item'][ $level_3_id ][ 'name' ] = $cate[ $level_3_id ];
		}

		//Shop Cate Ref List
		$shopCateList = D('ShopCate')->field('id,reference_id')->select();
		$refList = array();
		foreach($shopCateList as $key=>$shopCate){
			$cateId = $shopCate['id'];
			$refId = $shopCate['reference_id'];
			if($refId!=0){
				$refList[$cateId][] = $refId;
				$refList[$refId][] = $cateId;
			}
		}
		foreach($refList as $key=>$ref){
			foreach($ref as $cateId){
				foreach($refList[$cateId] as $elem){
					$refList[$key][] = $elem;
				}
			}
			$refList[$key] = array_unique($refList[$key]);
		}

		$this->assign('refList', $refList);
		$this->assign('cateList', $cateList);
		$this->display();
	}

	public function shopAdd() {
		if (isset($_REQUEST['submit'])){
			$cate_ids = $_REQUEST['cate_ids'];
			$cate_ids = explode(',',$cate_ids);
			$loop = true;
			$loopIds = $cate_ids;
			$childrenIds = array();
			$parentIds = array();
			while($loop) {
				$cate = M('ShopCate')->where(array('parent_id'=>array('in',$loopIds)))->select();
				foreach ($cate AS $val) { $loopIds[] = $val['id']; $parentIds[] = $val['parent_id']; }
				if (count($loopIds)) { $childrenIds = array_merge($childrenIds,$loopIds); $loopIds = array(); }
				else { $loop = false; }
			}
			$cate_ids = array_diff($childrenIds,$parentIds);
			if (!count($cate_ids)) { echo json_encode( array('cate_error' => "類別錯誤",'status' => false) ); return; }

			$shopDao 	= D('Shop');
			$shopInfo 	= $shopDao->create();

			if($shopInfo){
				$shopInfo['parent_shop_id']     = $_REQUEST['parent_shop_id'];
				$shopInfo['brand_id'] 			= $_REQUEST['brand_id'];
				$shopInfo['main_location_id']   = M('LocationSub')->where(array('id'=>$_REQUEST['location_id']))->getField('main_location_id');
				$shopInfo['sub_location_id']    = $_REQUEST['location_id'];
				$shopInfo['name'] 				= $_REQUEST['name'];
				$shopInfo['eng_name'] 			= $_REQUEST['eng_name'];
				$shopInfo['address'] 			= $_REQUEST['address'];
				$shopInfo['eng_address']		= $_REQUEST['eng_address'];
				$shopInfo['rooms'] 				= $_REQUEST['rooms'];
				$shopInfo['eng_rooms'] 			= $_REQUEST['eng_rooms'];
				$shopInfo['parking'] 			= $_REQUEST['parking'];
				$shopInfo['eng_parking'] 		= $_REQUEST['eng_parking'];
				$shopInfo['description'] 		= $_REQUEST['description'];
				$shopInfo['eng_description'] 	= $_REQUEST['eng_description'];
				$shopInfo['telephone'] 			= $_REQUEST['telephone'];
				$shopInfo['sub_telephone'] 		= $_REQUEST['sub_telephone'];
				$shopInfo['website'] 			= $_REQUEST['website'];
				$shopInfo['email'] 				= $_REQUEST['email'];
				$shopInfo['keyword'] 			= $_REQUEST['keyword'];
				$shopInfo['eng_keyword'] 		= $_REQUEST['eng_keyword'];
				$shopInfo['price_range'] 		= $_REQUEST['price_range'];
				$shopInfo['googlemap_lng'] 		= $_REQUEST['googlemap_lng'];
				$shopInfo['googlemap_lat'] 		= $_REQUEST['googlemap_lat'];
				$shopInfo['is_audit ']			= 1;
				$shopInfo['update_time'] 		= time();

				if($_REQUEST['is_closed']){
					$shopInfo['shop_status']= '2';
				}else if($_REQUEST['is_renovation']){
					$shopInfo['shop_status']= '3';
				}else if($_REQUEST['is_moved']){
					$shopInfo['shop_status']= '1';
				}else{
					$shopInfo['shop_status']= '0';
				}

				if ($id = $shopDao->add($shopInfo)){
					$shopInfo	 = array();

					$has_work_time = 0;
					for( $i=0; $i<8; $i++ ) {
						if(@$_REQUEST['work_dayoff_'.$i] == 1) {
							// Do not enter record
						} else {
							if ( @$_REQUEST['work_start_time_'.$i] && @$_REQUEST['work_end_time_'.$i] ) {
								$has_work_time = 1;
								M('ShopTime')->data( array(
									'shop_id' => $id,
									'start_time' => $_REQUEST['work_start_time_'.$i],
									'end_time' => $_REQUEST['work_end_time_'.$i],
									'recurring_week' => $i+1 ) )->add();
							}
						}
					}

					if($_REQUEST['newbrand'] != ''){
						$brandDao = D('Brand');
						$data = array();
						$data['name'] = $_REQUEST['newbrand'];
						$brandId = $brandDao->data($data)->add();

						$shopInfo['brand_id'] = $brandId;
					}

					$shopInfo['work_time'] = $has_work_time;
					$where['id'] = $id;
					$shopDao->where($where)->save($shopInfo);

					$cate_ids = $this->mapShopCateRef($cate_ids);
					foreach ( $cate_ids as $cate_id ) {
						M('ShopCateMap')->data( array( 'cate_id'=>$cate_id, 'shop_id'=>$id ) )->add();
					}
					generateShopCateStatByShopID($id,'add');

					$shopPictureDao = D('ShopPicture');
					$shopPictureDao->where("picstamp=".$_REQUEST['picstamp'])->setField('shop_id', $id);

					generateMainLocationStatistics($_REQUEST['main_location_id']);
					generateSubLocationStatistics($_REQUEST['sub_location_id']);

					echo json_encode( array('status' => "true",'info' => '新增商戶資料成功！') ); return;
				} else {
					echo json_encode( array('status' => "false",'info' => '未知錯誤，請聯繫管理員！') ); return;
				}
			} else {
				echo json_encode( array('status' => "false",'info' => '數據驗證失敗') ); return;
			}
		} else {
			//獲取Brand列表 壓入模
			$brandDao = D('Brand');
			$brandList = $brandDao->select();
			$this->assign('brandList', $brandList);

			//獲取Location分類主目錄列表 壓入模板
			$mainLocationDao = D('LocationMain');
			$mainLocationList = $mainLocationDao->select();
			$this->assign('mainLocationList', $mainLocationList);

					$temp = new Model();
					$subLocList = $temp->query("SELECT * FROM location_sub");
					$this->assign('subLocationList',$subLocList);

			$this->assign('picstamp', time().rand(1000,9999));
			$this->display();
		}
	}

	public function shopEdit() {
		$id = $_REQUEST['id'];
		$picstamp = $_REQUEST['picstamp'];
		$cate_ids = $_REQUEST['cate_ids'];
		$cate_ids = explode(',',$cate_ids);
		$loop = true;
		$loopIds = $cate_ids;
		$childrenIds = array();
		$parentIds = array();
		while($loop)
		{
			$cate = M('ShopCate')->where(array('parent_id'=>array('in',$loopIds)))->select();
			foreach ($cate AS $val) { $loopIds[] = $val['id']; $parentIds[] = $val['parent_id']; }
			if (count($loopIds)) { $childrenIds = array_merge($childrenIds,$loopIds); $loopIds = array(); }
			else { $loop = false; }
		}
		$cate_ids = array_diff($childrenIds,$parentIds);
		if (!count($cate_ids)) { echo json_encode( array('cate_error' => "類別錯誤",'status' => false) ); return; }

		$shopDao = D('Shop');

		//check isAudit
		$checkInfo = $shopDao->relation(true)->find($id);
		if(isset($id)){
			if(isset($checkInfo) && $checkInfo['is_audit']==1){
				if (isset($_REQUEST['submit'])){
					Import('ORG.Util.Input');
					$shopInfo = array();
					$shopInfo['parent_shop_id'] = $_REQUEST['parent_shop_id'];
					$shopInfo['brand_id'] = $_REQUEST['brand_id'];
					$shopInfo['main_location_id'] = M('LocationSub')->where(array('id'=>$_REQUEST['location_id']))->getField('main_location_id');
					$shopInfo['sub_location_id'] = $_REQUEST['location_id'];
					$shopInfo['name'] = $_REQUEST['name'];
					$shopInfo['eng_name'] = Input::getVar($_REQUEST['eng_name']);

					if ( $shopInfo['parent_shop_id'] )
					{
						$parentInfo = $shopDao->find($shopInfo['parent_shop_id']);
						$shopInfo['address'] = $parentInfo['address'];
						$shopInfo['googlemap_lng'] = $parentInfo['googlemap_lng'];
						$shopInfo['googlemap_lat'] = $parentInfo['googlemap_lat'];
					}
					else
					{
						$shopInfo['address'] = $_REQUEST['address'];
						$shopInfo['googlemap_lng'] = $_REQUEST['googlemap_lng'];
						$shopInfo['googlemap_lat'] = $_REQUEST['googlemap_lat'];
					}
					$shopInfo['rooms'] = $_REQUEST['rooms'];
					$shopInfo['eng_address'] = Input::getVar($_REQUEST['eng_address']);
					$shopInfo['keyword'] = $_REQUEST['keyword'];
					$has_work_time = 0;
					M('ShopTime')->where( array( 'shop_id' => $id ) )->delete();
					for( $i=0; $i<8; $i++ )
					{
						if(@$_REQUEST['work_dayoff_'.$i] == 1) {
							// Do not enter record
						} else {
							if ( @$_REQUEST['work_start_time_'.$i] && @$_REQUEST['work_end_time_'.$i] ) {
								$has_work_time = 1;
								M('ShopTime')->data( array(
									'shop_id' => $id,
									'start_time' => $_REQUEST['work_start_time_'.$i],
									'end_time' => $_REQUEST['work_end_time_'.$i],
									'recurring_week' => $i+1 ) )->add();
							}
						}
					}

					$shopInfo['work_time'] = $has_work_time;
					$shopInfo['parking'] = $_REQUEST['parking'];
					$shopInfo['description'] = $_REQUEST['description'];
					$shopInfo['telephone'] = $_REQUEST['telephone'];
					$shopInfo['sub_telephone'] = $_REQUEST['sub_telephone'];
					$shopInfo['website'] = $_REQUEST['website'];
					$shopInfo['email'] = $_REQUEST['email'];

					if($_REQUEST['is_closed']){
						$shopInfo['shop_status']= '2';
					}else if($_REQUEST['is_renovation']){
						$shopInfo['shop_status']= '3';
					}else if($_REQUEST['is_moved']){
						$shopInfo['shop_status']= '1';
					}else{
						$shopInfo['shop_status']= '0';
					}
					$shopInfo['price_range'] = $_REQUEST['price_range'];
					$shopInfo['update_time'] = time();

					if($_REQUEST['newbrand'] != ''){
						$brandDao = D('Brand');
						$data = array();
						$data['name'] = $_REQUEST['newbrand'];
						$brandId = $brandDao->data($data)->add();

						$shopInfo['brand_id'] = $brandId;
					}else{
						if (empty($shopInfo['brand_id'])){
							$shopInfo['brand_id'] = 0;
						}
					}
					/* 數據驗證處理 END */
					$shopInfo['eng_rooms'] 			= $_REQUEST['eng_rooms'];
					$shopInfo['eng_description'] 	= $_REQUEST['eng_description'];
					$shopInfo['eng_parking'] 		= $_REQUEST['eng_parking'];
					$shopInfo['eng_keyword'] 		= $_REQUEST['eng_keyword'];

					import ( '@.ORG.SimpleAcl' );
					$userInfo =  SimpleAcl::getLoginInfo();
					$where['id'] = $id;
					$shopDao->where($where)->save($shopInfo);

					generateMainLocationStatistics($shopInfo['main_location_id']);
					generateSubLocationStatistics($shopInfo['sub_location_id']);

					$shopPictureDao = D('ShopPicture');
					$shopPictureDao->where("picstamp=$picstamp")->setField('shop_id', $id);

					$existCateIds = M('ShopCateMap')->where( array( 'shop_id' => $id ) )->select();

					$cateIdsList = array();
					foreach($existCateIds as $elem){
						array_push($cateIdsList,$elem['cate_id']);
					}
					generateShopCateStatByShopID($id,'delete');
					M('ShopCateMap')->where( array( 'shop_id' => $id ) )->delete();

					$cate_ids = $this->mapShopCateRef($cate_ids);
					foreach ( $cate_ids as $cate_id ) {
						M('ShopCateMap')->data( array( 'cate_id'=>$cate_id, 'shop_id'=>$id ) )->add();
					}
					generateShopCateStatByShopID($id,'add');
					echo json_encode( array('status' => true,'info' => '更新成功!') ); return;
				}else{
					$shopInfo = $shopDao->relation(true)->find($id);
					$this->assign('shopInfo', $shopInfo);

					$newShopTime = array();
					$shopTime = M('ShopTime')->where( array( 'shop_id' => $id ) )->select();
					foreach ( $shopTime AS $val )
					{
						$newShopTime[$val['recurring_week']] = $val;
					}
					$this->assign('shopTime', $newShopTime);

					$picidx = $piccount = 20;
					$picids = array();
					while($picidx-->0){
						$picids[] = ($piccount - $picidx);
					}
					$this->assign('picids', $picids);

					$brandDao = D('Brand');
					$mainLocationDao = D('LocationMain');
					$shopKeyword=$shopInfo['keyword'];

					//獲取Brand列表 壓入模板
					$brandList = $brandDao->select();
					$this->assign('brandList', $brandList);

					//獲取Location分類主目錄列表 壓入模板
					$mainLocationList = $mainLocationDao->select();
					$this->assign('mainLocationList', $mainLocationList);

					$temp = new Model();
					$subLocList = $temp->query("SELECT * FROM location_sub");

					$this->assign('subLocationList',$subLocList);
					$this->display();
				}
			}else{
				$this->error('沒有找到該商戶，您的請求可能來源於您的收藏夾或其他歷史URL，而此商戶已被刪除');
			}
		}else{
			$this->error('URL非法，缺少要查看的商戶ID');
		}
	}

	function doShopUpload() {
		import('ORG.Net.UploadFile');
		$upload = new UploadFile();
		$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));

		$upload->savePath = C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH');
		$upload->saveRule = 'time';
		$upload->thumb = true;
		$upload->thumbMaxWidth = 120;
		$upload->thumbMaxHeight = 120;
		$upload->uploadReplace = true;

		if($upload->upload()) {
			$info =  $upload->getUploadFileInfo();
			echo json_encode( array( 'file' => $info[0]['savename'] ) );
		}
	}

	public function doShopUpgrade() {
		$id = $_REQUEST['id'];
		$level = $_REQUEST['level'];

		$shopDao = D('Shop');

		$shopInfo = $shopDao->where( array( 'id' => $id ) )->relation(true)->find();

		$errors = array();
		$isAbleToUpgrade = true;
		if ( mb_strlen($shopInfo['description'],'UTF-8') > getShopLevelSetting('description_len',$level) )
		{
			$errors[] = '請先將公司簡介修改至少於'.getShopLevelSetting('description_len',$level).'字';
			$isAbleToUpgrade = false;
		}
		if ( !getShopLevelSetting('add_website',$level) && $shopInfo['website'] )
		{
			$errors[] = '請先刪除網頁連結';
			$isAbleToUpgrade = false;
		}
		if ( $shopInfo['keyword'] )
		{
			$keywords = explode( ',', $shopInfo['keyword'] );
			$keywordsNum = 0;
			foreach( $keywords AS $k ) { if ($k) { $keywordsNum++; } }
			if ( $keywordsNum > getShopLevelSetting('keyword_num',$level) )
			{
				$errors[] = '請先將關鍵字修改至少於'.getShopLevelSetting('description_len',$level).'個';
				$isAbleToUpgrade = false;
			}
		}
		if ( count( $shopInfo['pictures'] ) > getShopLevelSetting('photo_num',$level) )
		{
			$errors[] = '請先將相片修改至少於'.getShopLevelSetting('description_len',$level).'張';
			$isAbleToUpgrade = false;
		}

		if ( $isAbleToUpgrade )
			{ $shopDao->where( array( 'id' => $id ) )->save( array( 'shop_level' => $level ) ); }

		echo json_encode( array( 'success' => $isAbleToUpgrade, 'errors' => $errors ) );
	}

	private function mapShopCateRef($shopcate){
		//mapping for shopcate reference id
		foreach($shopcate AS $key=>$cateId){
			$referenceId = D('ShopCate')->where("id=$cateId")->getfield('reference_id');
			if($referenceId!=0){
				$shopcate[$key] = $referenceId;
			}
		}
		return array_unique($shopcate);
	}

}

?>