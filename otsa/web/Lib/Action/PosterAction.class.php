<?php
class PosterAction extends SaAction
{

    public function add()
	{
		$this->display();
    }

	public function doAdd()
	{
		$dbo = D( 'Poster' );
		$dbo2 = D( 'Posterblock' );

		$data = $_REQUEST;
		$data[ 'create_time' ] = time();
		$data[ 'status' ] = '0';
		$data[ 'thumb_path' ] = $data[ 'thumb' ];
		$data[ 'banner_path' ] = $data[ 'banner' ];
		$posterId = $dbo->add( $data );
		$dbo->where( array( 'id' => $posterId ) )->data( array( 'orders' => $posterId ) )->save();

		$order = 1;
		foreach( $data[ 'blocks' ] AS $key => $val ) {
			if ( $val[ 'description' ] ) {
				$val[ 'orders' ] = $order;
				$val[ 'poster_id' ] = $posterId;
				$val[ 'image_path' ] = $val[ 'image' ];
				$dbo2->add( $val );
			}
			$order++;
		}

		echo '<script>alert("添加成功");window.location = "' . __APP__ . '/Poster/listAll";</script>';
    }
	
	public function edit()
	{
		$dbo = D( 'Poster' );
		$dbo2 = D( 'Posterblock' );

		$id = @$_REQUEST[ 'id' ];
		
		$poster = $dbo->where( array( 'id' => $id ) )->find();
		$posterblocks = $dbo2->where( array( 'poster_id' => $id ) )->order( 'orders' )->select();

		$this->assign( 'id' , $id );
		$this->assign( 'poster' , $poster );
		$this->assign( 'posterblocks' , $posterblocks );
		$this->display();
    }

	public function doEdit()
	{
		$dbo = D( 'Poster' );
		$dbo2 = D( 'Posterblock' );

		$id = @$_REQUEST[ 'id' ];

		if ( $id )
		{
			$data = $_REQUEST;
			$data[ 'thumb_path' ] = $data[ 'thumb' ];
			$data[ 'banner_path' ] = $data[ 'banner' ];
			$dbo->where( array( 'id' => $id ) )->data( $data )->save();

			
			$dbo2->where( array( 'poster_id' => $id ) )->delete();
			$order = 1;
			foreach( $data[ 'blocks' ] AS $key => $val ) {
				if ( $val[ 'description' ] ) {
					$val[ 'orders' ] = $order;
					$val[ 'poster_id' ] = $id;
					$val[ 'image_path' ] = $val[ 'image' ];
					$dbo2->add( $val );
				}
				$order++;
			}

			echo '<script>alert("修改成功");window.location = "' . __APP__ . '/Poster/listAll";</script>';
		}
    }

	function doPreview()
	{
		$data = $_REQUEST;
		$this->assign( 'data' , $data );
		$this->display();
	}

	function doUpload()
	{
		import('ORG.Net.UploadFile');
		$upload = new UploadFile();
		$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));
	
		$upload->savePath = C('OUTSTREET_ROOT') . '/Public/uploadimages/outstreetpromo/';
		$upload->saveRule = 'time';

		if($upload->upload()) {
			$info =  $upload->getUploadFileInfo();
			echo json_encode( array( 'file' => $info[0]['savename'] ) );
		}
	}

    public function doDel()
	{
		$dbo = D( 'Poster' );
		$dbo2 = D( 'Posterblock' );

		$id = @$_REQUEST[ 'id' ];

		if ( $id )
		{
			$dbo->where( array( 'id' => $id ) )->data( array('status'=>2) )->save();
			//$dbo->where( array( 'id' => $id ) )->delete();
			//$dbo2->where( array( 'poster_id' => $id ) )->delete();
		}
    }

    public function doPublic()
	{
		$dbo = D( 'Poster' );

		$id = @$_REQUEST[ 'id' ];

		if ( $id )
		{
			$dbo->where( array( 'id' => $id ) )->data( array('status'=>1) )->save();
		}
    }

    public function doUnPublic()
	{
		$dbo = D( 'Poster' );

		$id = @$_REQUEST[ 'id' ];

		if ( $id )
		{
			$dbo->where( array( 'id' => $id ) )->data( array('status'=>0) )->save();
		}
    }

    public function listAll()
	{
		$dbo = D( 'Poster' );

		$title = @$_REQUEST[ 'title' ];
		$lead = @$_REQUEST[ 'lead' ];
		$start_date = @$_REQUEST[ 'start_date' ];
		$end_date = @$_REQUEST[ 'end_date' ];
		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'orders';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'asc';

		$where = array('status'=>array('neq',2));
		if ( $title ) { $where[ 'title' ] = array( 'like', '%' . $title . '%' ); }
		if ( $lead ) { $where[ 'lead' ] = array( 'like', '%' . $lead . '%' ); }
		if ( $start_date && $end_date )
			{ $where[ 'create_time' ] = array( 'between', strtotime($start_date) . ',' . strtotime($end_date) ); }
		elseif ( $start_date )
			{ $where[ 'create_time' ] = array( 'gt', strtotime($start_date) ); }
		elseif ( $end_date )
			{ $where[ 'create_time' ] = array( 'lt', strtotime($end_date) ); }

		$count = $dbo->where($where)->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;

		$list = $dbo->where($where)->limit($limit)->order( $sort . ' ' . $sortorder )->select();

		$this->assign( 'list' , $list );
		$this->assign( 'sort' , $sort );
		$this->assign( 'sortorder' , $sortorder );
		$this->assign( 'multipage' , $multipage );
		$this->display();
    }

    public function moveUp()
	{
		$dbo = D( 'Poster' );

		$id = @$_REQUEST[ 'id' ];
		$orders = @$_REQUEST[ 'orders' ];

		$affected = $dbo->where( array( 'orders' => array( 'lt', $orders ) ) )->order('orders desc')->find();
		if ( $affected )
		{
			$dbo->where( array( 'id' => $affected[ 'id' ] ) )->data( array( 'orders' => $orders ) )->save();
			$dbo->where( array( 'id' => $id ) )->data( array( 'orders' => $affected[ 'orders' ] ) )->save();
		}
    }

    public function moveDown()
	{
		$dbo = D( 'Poster' );

		$id = @$_REQUEST[ 'id' ];
		$orders = @$_REQUEST[ 'orders' ];

		$affected = $dbo->where( array( 'orders' =>array( 'gt', $orders )  ) )->order('orders')->find();
		if ( $affected )
		{
			$dbo->where( array( 'id' => $affected[ 'id' ] ) )->data( array( 'orders' => $orders ) )->save();
			$dbo->where( array( 'id' => $id ) )->data( array( 'orders' => $affected[ 'orders' ] ) )->save();
		}
    }

}

?>