<?php

class ThreadAction extends SaAction {
	
	public function threadlist() {

		$threadDao = D('ForumThread');
		$postDao = D('ForumPost');
		$id = $_GET['id'];
		$top = $_GET['top'];
		
		if ($_GET['del']==1){
			
			$where = array();
			$postDao->where('thread_id='.$id)->delete();

			$where = array();
			$threadDao->where('id='.$id)->delete();
			
		}

		if (is_numeric($recommendid = $_GET['recommendid'])) {
			
			$threadInfo = $threadDao->find($recommendid);
			$where = array();
			$where['id'] = $recommendid;
			
			if ($threadInfo['is_recommend'] == 1){
				$threadDao->where($where)->setField('is_recommend', 0);
			}else{
				$threadDao->where($where)->setField('is_recommend', 1);
			}
			
		}

		if (!empty($id)) {
			$threadDao->where('id=' . $id)->setField('is_top',$top);
		}

		$status = 0; //0無操作  大於0成功樣式表   小於0失敗樣式
		$actionInfo = '刪除後不可恢復,請謹慎操作'; //操作提示

		$sort = @$_REQUEST[ 'sort' ] ? $_REQUEST[ 'sort' ] : 'create_time';
		$sortorder = @$_REQUEST[ 'sortorder' ] ? $_REQUEST[ 'sortorder' ] : 'desc';

		//設置分頁代碼
		$count = $threadDao->count('id');
		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		$threadList = $threadDao->limit($limit)->order( $sort . ' ' . $sortorder )->select();

		$this->assign('status', $status);
		$this->assign('actionInfo', $actionInfo);
		$this->assign('multipage', $multipage);
		$this->assign('threadList', $threadList);
		$this->assign('threadlink',C('OUTSTREET_DIR').'/forum/view/p/1/threadid/');
		$this->assign('boardlink',C('OUTSTREET_DIR').'/forum/index/boardid/');
		$this->display();

	}

}

?>
