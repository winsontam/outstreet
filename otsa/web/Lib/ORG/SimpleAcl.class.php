<?php

//
//	簡單的用戶驗證
//
//	coderlee 2009-10-19

class SimpleAcl extends Think
{
	// 认证方法
	static public function authenticate($map,$model='')
	{
		if(empty($model)) $model =  C('USER_AUTH_MODEL');
		//使用给定的Map进行认证
		return D($model)->where($map)->find();
	}

	//用于保存登錄信息的方法
	static public function saveLogin($authInfo)
	{
		$_SESSION[C('USER_AUTH_KEY')]	=	$authInfo;
	}

	static public function clearLogin(){
		$_SESSION['NOT_AUTH_ACTION'] = null;
		$_SESSION[C('USER_AUTH_KEY')] = null;
	}

	static public function getLoginInfo(){
		if (isset($_SESSION[C('USER_AUTH_KEY')])){
			return $_SESSION[C('USER_AUTH_KEY')];
		}else{
			return false;
		}
	}

	// 登录检查
	static public function checkLogin() {
		//检查当前操作是否需要认证
		if(SimpleAcl::checkAccess()) {
			
	 		$_SESSION['NOT_AUTH_ACTION'] = WEB_ROOT . '/' .MODULE_NAME . '/' . ACTION_NAME;

			//检查认证识别号
			if(!$_SESSION[C('USER_AUTH_KEY')]) {
				//跳轉到認證網關
				redirect(PHP_FILE.C('USER_AUTH_GATEWAY'));
			}
		}
		return true;
	}

	//检查当前操作是否需要认证
	static function checkAccess()
	{
		//如果项目要求认证，并且当前模块需要认证，则进行权限认证
		if( C('USER_AUTH_ON') ){
			$_module	=	array();
			$_action	=	array();
			if("" != C('REQUIRE_AUTH_MODULE')) {
				//需要认证的模块
				$_module['yes'] = explode(',',strtoupper(C('REQUIRE_AUTH_MODULE')));
			}else {
				//无需认证的模块
				$_module['no'] = explode(',',strtoupper(C('NOT_AUTH_MODULE')));
			}
			//检查当前模块是否需要认证
			if((!empty($_module['no']) && !in_array(strtoupper(MODULE_NAME),$_module['no'])) || (!empty($_module['yes']) && in_array(strtoupper(MODULE_NAME),$_module['yes']))) {
				if("" != C('REQUIRE_AUTH_ACTION')) {
					//需要认证的操作
					$_action['yes'] = explode(',',strtoupper(C('REQUIRE_AUTH_ACTION')));
				}else {
					//无需认证的操作
					$_action['no'] = explode(',',strtoupper(C('NOT_AUTH_ACTION')));
				}
				//检查当前操作是否需要认证
				if((!empty($_action['no']) && !in_array(strtoupper(ACTION_NAME),$_action['no'])) || (!empty($_action['yes']) && in_array(strtoupper(ACTION_NAME),$_action['yes']))) {
					return true;
				}else {
					return false;
				}
			}else {
				return false;
			}
		}
		return false;
	}


}//end class
?>