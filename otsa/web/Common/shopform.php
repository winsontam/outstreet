<?php

function generateMall($style){
	$output='<input type="checkbox" name="is_mall" id="mall" value="1" /><input type="hidden" id="parent_shop_id" name="parent_shop_id" value="">';
	$output.='<span id="bymallselect" style="padding-left:50px;display:none">';
	$output.='<span class="ot_d_data ot_d_data_4lvl">根據商戶選擇：</span>';
	$output.='<input type="text" id="mallname" class="forge_drop" />';
	$output.='<button id="reset" class="ot_o_resetbtn" style="display:none">重新選擇商場</button>';
	$output.='<ul style="'.$style.'" id="malllist"></ul>';
	$output.='<div id="nomalltip" style="display:none">沒有找到您輸入的商鋪</div>';
	return $output;
}

function generateGooglemap($shopInfo){
	$output='';
	if($shopInfo['parent_shop_id']!=0){
		$output .='<input type="text" style="background-color:#ebebe4" name="address" value="';
		if(isset($shopInfo)){
			$output .=$shopInfo['address'];
		}
		$output .='" id="address" readonly="true" />';
	}else{
		$output .='<input type="text" name="address" value="';
		if(isset($shopInfo)){
			$output .=$shopInfo['address'];
		}
		$output .='" id="address" />';
	}
	$output .='<span id="addressError">如:xx街xx號</span><br/>';
	$output .='<div class="gmapedit"> <div id="gmap"></div> </div><br />';
	$output .='<span class="ot_d_data_3lvl">';
	$output .='<input type="text" name="googlemap_lat" readonly="true" id="googlemap_lat" value="';
	if(isset($shopInfo)){
			$output .=$shopInfo['googlemap_lat'];
	}
	$output .='" class="small" />';
	$output .='<input type="text" name="googlemap_lng" readonly="true" id="googlemap_lng" value="';
	if(isset($shopInfo)){
			$output .=$shopInfo['googlemap_lng'];
	}
	$output .='" class="small" />';
	$output .='</span>';
	
	return $output;
}

function generateShopWeek($shopTime,$style){
	$output = '';
	$week = array( '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日', '公眾假期' );
	$output.= '<table style="'.$style.'" cellspacing="0" cellpadding="0">';
	for( $i=0; $i<8; $i++ ){
		$output.= '<tr><td style="height:20px; width:60px;">'.$week[$i].'</td>';
		$output.= '<td style="width:60px;"><input type="checkbox" name="work_dayoff_'.$i.'" id="work_dayoff_'.$i.'" value="1" onclick="changeWorkTime(\''.$i.'\')"';
		if(isset($shopTime)){
			if($shopTime[$i+1]['start_time'] == '' && $shopTime[$i+1]['end_time'] == ''){
				$output.='checked="true"';
			}
		}else{
			$output.='checked="true"';
		}
		$output.='/>&nbsp;休息</td>';
		$output.='<td style="width:185px;">';
		$output.='<div id="time_input_'.$i.'" style="';
		if(isset($shopTime)){
			if($shopTime[$i+1]['start_time'] == '' && $shopTime[$i+1]['end_time'] == ''){
				$output.='display:none';
			}
		}else{
			$output.='display:none';
		}
		$output.='">';
		$output.='<input type="text" name="work_start_time_'.$i.'" id="work_start_time_'.$i.'" value="';
		if(isset($shopTime)){
			$output.=$shopTime[$i+1]['start_time'];
		}
		$output.='" style="width: 80px;" maxlength="4" />';
		$output.=' - ';
		$output.='<input type="text" name="work_end_time_'.$i.'" id="work_end_time_'.$i.'" value="';
		if(isset($shopTime)){
			$output.=$shopTime[$i+1]['end_time'];
		}
		$output.='" style="width: 80px;" maxlength="4"/>&nbsp;';
		$output.='</div>';

		if($i == 0) {
			$output .= '</td><td rowspan="8"><span id="timeWarning" style="color:#999">如: 0900 - 2230</span>';
		}

		$output.='</td></tr>';
	}
	$output .= '</table>';
	return $output;
}

function displayMallButton( $shopInfo ) {
	if ( $shopInfo['parent_shop_id'] && M('ShopCateMap')->where(array('shop_id'=>$shopInfo['parent_shop_id'], 'cate_id'=>175))->count() ) {
		return '<a class="thickbox" href="__ROOT__/base/mall/parentshopid/' . $shopInfo['parent_shop_id'] . '?height=500&width=850&modal=false&TB_iframe=true"><img src="__PUBLIC__/images/buttons/shop_profile_mall.jpg" /></a>';
	}
	elseif ( M('ShopCateMap')->where(array('shop_id'=>$shopInfo['id'], 'cate_id'=>175))->count() ) {
		return '<a class="thickbox" href="__ROOT__/base/mall/parentshopid/' . $shopInfo['id'] . '?height=500&width=850&modal=false&TB_iframe=true"><img src="__PUBLIC__/images/buttons/shop_profile_mall.jpg" /></a>';
	}
}

?>