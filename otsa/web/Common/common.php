<?php

function contentShortForm($word, $length){
	if(strlen($word)==""){
		$word="暫無 ";
		return $word;
	}else if(strlen($word)>=$length){
		$tempword=msubstr($word, 0, $length-3);
		$word=$tempword . "... ";
		return $word;
	}else{
		return $word;
	}	
}

function indexWordShortForm($word, $length){
	if(strlen($word)>=$length){
		$tempword=msubstr($word, 0, $length-4);
		$word=$tempword . "...";
		return $word;
	}else{
		return $word;
	}
}

function urlShortForm($url, $length){
	if(strlen($url)>=$length){
		$tempurl=substr($url, 0, $length-4);
		$tempurl=$tempurl . " ... ";
		$url=$tempurl.substr($url, -5, 5);
		return $url;
	}else{
		return $url;
	}
}

function chDate($number) {
	switch ($number){
		case "1":
		$str = "Y-m-d";
		break;
		case "2":
		$str = "Y年m月d日";
		break;
		case "3":
		$str = "d/m";
		break;
	}
	return $str;
}

function getUserIdByName($nick_name){
	$userDao = D('User');
	$where = array();
	$where['nick_name'] = $nick_name;
	$userInfo = $userDao->where($where)->find();
	if(!empty($userInfo)){
		return $userInfo['id'];
	}
}

function getFace($userInfo){
	if (!empty($userInfo['face_path'])){
		return C('USERFACE_UPFILE_PATH') . 'face' . $userInfo['face_path'];
	}
	
	if ($userInf['sex'] == 0){
		return __PUBLIC__ . '/images/face/0.gif';
	}else{
		return __PUBLIC__ . '/images/face/1.gif';
	}
}

function getFirstPicture($modelInfo, $type, $thumb = null) {	
	
	if (!empty($modelInfo['pictures'])){
		if ($type == 'review'){
			if(isset($thumb)){
				return C('REVIEW_UPFILE_PATH') . $modelInfo['pictures'][0]['shop_id'] . '/' . 'thumb_' . $modelInfo['pictures'][0]['file_path'];
			}else{
				return C('REVIEW_UPFILE_PATH') . $modelInfo['pictures'][0]['shop_id'] . '/' . '' . $modelInfo['pictures'][0]['file_path'];
			}
		}else if($type == 'shop'){
			if(isset($thumb)){
				return C('SHOP_UPFILE_PATH') . '/' . 'thumb_' . $modelInfo['pictures'][0]['file_path'];
			}else{
				return C('SHOP_UPFILE_PATH') . '/' . '' . $modelInfo['pictures'][0]['file_path'];
			}
		}else if($type == 'event'){
			if(isset($thumb)){
				return C('EVENT_UPFILE_PATH') . '/' . 'thumb_' . $modelInfo['pictures'][0]['file_path'];
			}else{
				return C('EVENT_UPFILE_PATH') . '/' . '' . $modelInfo['pictures'][0]['file_path'];
			}
		}
	}else{
		if(isset($thumb)){
			return __PUBLIC__ . '/images/thumb_nopic.jpg';			
		}else{
			return __PUBLIC__ . '/images/nopic.jpg';
		}
	}
}

function msubstr($str, $start=0, $length, $charset="utf-8", $suffix=true) {
	if(function_exists("mb_substr"))
	return mb_substr($str, $start, $length, $charset);
	elseif(function_exists('iconv_substr')) {
		return iconv_substr($str,$start,$length,$charset);
	}
	$re['utf-8']   = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
	$re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
	$re['gbk']	  = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
	$re['big5']	  = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
	preg_match_all($re[$charset], $str, $match);
	$slice = join("",array_slice($match[0], $start, $length));
	if($suffix) return $slice."…";
	return $slice;
}

function obcopy($ob){
	$serialized_contents = serialize($ob);
	return unserialize($serialized_contents);
}

function week2ch($number) {
    $number = intval($number);
    $array  = array('日','一','二','三','四','五','六');
    $str = $array[$number];
    return $str;
}

function timediff($begin_time, $end_time){
	if($begin_time < $end_time){
		$starttime = $begin_time;
		$endtime = $end_time;
	} else {
		$starttime = $end_time;
		$endtime = $begin_time;
	}

	$timediff = $endtime - $starttime;
	$days = intval($timediff/86400);
	$remain = $timediff%86400;
	$hours = intval($remain/3600);
	$remain = $remain%3600;
	$mins = intval($remain/60);
	$secs = $remain%60;

	$res = array("day"=>$days,"hour"=>$hours,"min"=>$mins,"sec"=>$secs);
	return $res;
}

//***************************************** Start Shop Statistic Group **********************************************//
function generateShopRecordStat($limit){
	$shopDao = D('Shop');
	$statisticsShopDao = D('StatisticsShop');

	$shopInfos		= $shopDao->field('shop_fields.id as shopId,shop_fields.name as shopName,statistics_shop.shop_id as sShopId')->join('statistics_shop on shop_fields.id = statistics_shop.shop_id')->where(array('shop_fields.is_audit'=>1,'shop_fields.id'=>array('gt',0)))->limit($limit)->group('shop_fields.id')->order('shop_fields.id asc')->select();
	
	foreach($shopInfos as $shopInfo){
		if($shopInfo['sShopId'] == '') {
			$data = array();
			$data['shop_id']		= $shopInfo['shopId'];
			$data['shop_name']		= $shopInfo['shopName'];
			$statisticsShopId 		= $statisticsShopDao->add( $data );
			$data['count']			= '';
			$datas[] = $data;
		}
	}
	
	return $datas;
}

function generateShopReviewStat($limit){
	$reviewDao 			= D('Review');
	$reviewRateDao 		= D('ReviewRate');
	$reviewPictureDao 	= D('ReviewPicture');
	$statisticsShopDao 	= D('StatisticsShop');

	$reviewInfos		= $reviewDao->field('shop_id as shopId,count(*) as countShop')->where(array('is_audit'=>1,'shop_id'=>array('gt',0)))->limit($limit)->group('shop_id')->order('shop_id asc')->select();
	
	foreach($reviewInfos as $reviewInfo){
		$data = array();
		$data['shop_id']		= $reviewInfo['shopId'];
		$data['review_count']   = $reviewInfo['countShop'];
		$data['review_picture_count'] = $reviewPictureDao->join('review_fields on review_pictures.review_id = review_fields.id')->where(array('review_fields.shop_id'=>$reviewInfo['shopId']))->count();
		$data['total_rating'] = $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_total'=>array('gt',0)))->avg('rate_total');
		$data['avg_rate_1'] = $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_1'=>array('gt',0)))->avg('rate_1');
		$data['avg_rate_2'] = $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_2'=>array('gt',0)))->avg('rate_2');
		$data['avg_rate_3'] = $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_3'=>array('gt',0)))->avg('rate_3');
		$data['avg_rate_4'] = $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_4'=>array('gt',0)))->avg('rate_4');
		$data['avg_rate_5'] = $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_5'=>array('gt',0)))->avg('rate_5');
		$data['avg_rate_6'] = $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_6'=>array('gt',0)))->avg('rate_6');
		$data['avg_rate_7'] = $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_7'=>array('gt',0)))->avg('rate_7');

		$statisticsShopDao->where(array('shop_id'=>$data['shop_id']))->save($data);
		$statShopName			= $statisticsShopDao->field('shop_name')->where(array('shop_id'=>$reviewInfo['shopId']))->select();
		$data['shop_name']		= $statShopName[0]['shop_name'];
		$data['count']			= $reviewInfo['countShop'];
		$datas[] = $data;
	}
	
	return $datas;
}

function generateShopPicStat($limit){
	$shopPictureDao = D('ShopPicture');
	$statisticsShopDao 	= D('StatisticsShop');

	$picInfos 		= $shopPictureDao->field('shop_picture.shop_id as shopId,count(*) as countPic')->join('shop_fields on shop_picture.shop_id = shop_fields.id')->where(array('shop_fields.is_audit'=>1))->limit($limit)->group('shop_picture.shop_id')->order('shop_picture.shop_id asc')->select();

	foreach($picInfos as $picInfo){
		$data 					= array();
		$data['shop_id']		= $picInfo['shopId'];
		$data['picture_count'] 	= $picInfo['countPic'];
		$statisticsShopDao->where(array('shop_id'=>$data['shop_id']))->save($data);
		$statShopName			= $statisticsShopDao->field('shop_name')->where(array('shop_id'=>$picInfo['shopId']))->select();
		$data['shop_name']		= $statShopName[0]['shop_name'];
		$data['count']			= $picInfo['countPic'];
		$datas[] = $data;
	}
	
	return $datas;
}

function generateEventStat($limit){
	$eventDao 		= D('Event');
	$statisticsShopDao 	= D('StatisticsShop');

	$eventInfos 	= $eventDao->field('event_fields.shop_id as shopId,count(*) as countEvent')->join('shop_fields on event_fields.shop_id = shop_fields.id')->where(array('shop_fields.is_audit'=>1,'event_fields.is_audit'=>1))->limit($limit)->group('event_fields.shop_id')->order('event_fields.shop_id asc')->select();

	foreach($eventInfos as $eventInfo){
		$data 					= array();
		$data['shop_id']		= $eventInfo['shopId'];
		$data['event_count'] 	= $eventInfo['countEvent'];
		$statisticsShopDao->where(array('shop_id'=>$data['shop_id']))->save($data);
		$statShopName			= $statisticsShopDao->field('shop_name')->where(array('shop_id'=>$eventInfo['shopId']))->select();
		$data['shop_name']		= $statShopName[0]['shop_name'];
		$data['count']			= $eventInfo['countEvent'];
		$datas[] = $data;
	}
	
	return $datas;
}

function generateUserShopFavoriteStat($limit){
	$UserShopFavoriteDao 	= D('UserShopFavorite');
	$statisticsShopDao 		= D('StatisticsShop');

	$userShopFavoriteInfos 	= $UserShopFavoriteDao->field('user_shop_favorite.shop_id as shopId,count(*) as countUserShopFavorite')->join('shop_fields on user_shop_favorite.shop_id = shop_fields.id')->where(array('shop_fields.is_audit'=>1))->limit($limit)->group('user_shop_favorite.shop_id')->order('user_shop_favorite.shop_id asc')->select();

	foreach($userShopFavoriteInfos as $userShopFavoriteInfo){
		$data 					= array();
		$data['shop_id']		= $userShopFavoriteInfo['shopId'];
		$data['favorite_count'] = $userShopFavoriteInfo['countUserShopFavorite'];
		$statisticsShopDao->where(array('shop_id'=>$data['shop_id']))->save($data);
		$statShopName			= $statisticsShopDao->field('shop_name')->where(array('shop_id'=>$userShopFavoriteInfo['shopId']))->select();
		$data['shop_name']		= $statShopName[0]['shop_name'];
		$data['count']			= $userShopFavoriteInfo['countUserShopFavorite'];
		$datas[] = $data;
	}
	
	return $datas;
}

function generateShopReviewRateStatistics($shopId){
	$reviewDao 			= D('Review');
	$shopDao 			= D('Shop');
	$reviewRateDao 		= D('ReviewRate');
	$reviewPictureDao 	= D('ReviewPicture');
	$statisticsShopDao 	= D('StatisticsShop');
	
	//**************************************** Check If Shop Statistic Record Exist **************************************************//
	$statShopRecord		= $statisticsShopDao->where(array('shop_id'=>$shopId))->find();
	$shopInfo			= $shopDao->field('id as shopId,name as shopName')->where(array('id'=>$shopId))->select();

	if(!$statShopRecord) {
		$data = array();
		$data['shop_id']		= $shopInfo[0]['shopId'];
		$data['shop_name']		= $shopInfo[0]['shopName'];
		$statisticsShopDao->add($data);
	}
	//**************************************** Check If Shop Statistic Record Exist **************************************************//

	$reviewInfos		= $reviewDao->field('shop_id as shopId,count(*) as countShop')->where(array('is_audit'=>1,'shop_id'=>$shopId))->group('shop_id')->select();

	foreach($reviewInfos as $reviewInfo){
		$data = array();
		$data['shop_id']				= $reviewInfo['shopId'];
		$data['review_count']   		= $reviewInfo['countShop'];
		$data['review_picture_count']	= $reviewPictureDao->join('review_fields on review_pictures.review_id = review_fields.id')->where(array('review_fields.shop_id'=>$reviewInfo['shopId']))->count();
		$data['total_rating'] 			= $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_total'=>array('gt',0)))->avg('rate_total');
		$data['avg_rate_1'] 			= $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_1'=>array('gt',0)))->avg('rate_1');
		$data['avg_rate_2'] 			= $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_2'=>array('gt',0)))->avg('rate_2');
		$data['avg_rate_3'] 			= $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_3'=>array('gt',0)))->avg('rate_3');
		$data['avg_rate_4'] 			= $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_4'=>array('gt',0)))->avg('rate_4');
		$data['avg_rate_5'] 			= $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_5'=>array('gt',0)))->avg('rate_5');
		$data['avg_rate_6'] 			= $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_6'=>array('gt',0)))->avg('rate_6');
		$data['avg_rate_7'] 			= $reviewRateDao->join('review_fields on review_rate.review_id = review_fields.id')->where(array('review_rate.shop_id'=>$reviewInfo['shopId'],'is_audit'=>1,'rate_7'=>array('gt',0)))->avg('rate_7');

		$statisticsShopDao->where(array('shop_id'=>$data['shop_id']))->save($data);
	}
}
//******************************************* End Shop Statistic Group **********************************************//

function generateEventStatistics($eventId){
	$eventDao = D('Event');
	$statisticsEventDao = D('StatisticsEvent');
	
	$eventInfo = $eventDao->find($eventId);
	
	if(!empty($eventInfo)){
	
		$statisticsEventDao->where('event_id='.$eventId)->delete();
		$eventReviewDao = D('EventReview');
		$eventPictureDao = D('EventPicture');
		$reviewPictureDao = D('ReviewPicture');

		$where = array();
		$where['event_id'] = $eventInfo['id'];

		$data = array();
		$data['event_id'] = $eventInfo['id'];
		$data['event_name'] = $eventInfo['name'];
		$data['review_count'] = $eventReviewDao->where($where)->count();
		$data['picture_count'] = $eventPictureDao->where($where)->count();
		$reviewList = $eventReviewDao->where($where)->select();
		$data['review_picture_count'] = 0;
		foreach($reviewList as $review){
			$data['review_picture_count'] += $reviewPictureDao->where('review_id='.$review['id'])->count();	
		}
		$statisticsEventDao->add($data);
	}
}

function generatePromotionStatistics($promotionId){
	$promotionDao = D('Promotion');
	$statisticsPromotionDao = D('StatisticsPromotion');
	$promotionInfo = $promotionDao->find($promotionId);
	
	if(!empty($promotionInfo)){
		$statisticsPromotionDao->where('promotion_id='.$promotionId)->delete();
		$promotionReviewDao = D('PromotionReview');
		$reviewPictureDao = D('ReviewPicture');
		$where = array();
		$where['promotion_id'] = $promotionInfo['id'];
		$data = array();
		$data['promotion_id'] = $promotionInfo['id'];
		$data['promotion_name'] = $promotionInfo['name'];
		$data['review_count'] = $promotionReviewDao->where($where)->count();
		$data['picture_count'] = $promotionReviewDao->where($where)->count();
		$reviewList = $promotionReviewDao->where($where)->select();
		$data['review_picture_count'] = 0;
		foreach($reviewList as $review){
			$data['review_picture_count'] += $reviewPictureDao->where('review_id='.$review['id'])->count();	
		}
	}	
	$statisticsPromotionDao->add($data);
}

function generateUserStatistics($userId){
	$userDao = D('User');
	$statisticsUserDao = D('StatisticsUser');

	$userInfo = $userDao->find($userId);

	if(!empty($userInfo)){
		
		$statisticsUserDao->where('user_id='.$userId)->delete();
		
		$shopDao = D('Shop');
		$reviewDao = D('Review');
		$eventReviewDao = D('EventReview');
		$eventDao = D('Event');
		$userShopFavoriteDao = D('UserShopFavorite');
		$userUserFavoriteDao = D('UserUserFavorite');
		$userMessageDao = D('UserMessage');
		$shopPictureDao = D('ShopPicture');
		$reviewPictureDao = D('ReviewPicture');
		$reviewCommendDao = D('ReviewCommend');

		$where = array();
		$where['user_id'] = $userInfo['id'];

		$data = array();
		$data['user_id'] = $userInfo['id'];
		$data['nick_name'] = $userInfo['nick_name'];
		
		$where['is_audit'] = 1;
		$data['review_count'] = $reviewDao->where($where)->count();
		$data['review_count'] += $eventReviewDao->where($where)->count();
		
		unset($where['is_audit']);
		
		$data['event_count'] = $eventDao->where($where)->count();
		$data['favorite_shop_count'] = $userShopFavoriteDao->where($where)->count();
		$data['message_count'] = $userMessageDao->where($where)->count();
		$data['submit_message_count'] = $userMessageDao->where('submit_user_id=' . $userInfo['id'])->count();
		$data['picture_count'] = $reviewPictureDao->where($where)->count();
		$data['favorite_user_count'] = $userUserFavoriteDao->where('user_id=' . $userInfo['id'])->count();
		$data['favorite_me_count'] = $userUserFavoriteDao->where('favorite_user_id=' . $userInfo['id'])->count();
		$data['commend_review_count'] = $reviewCommendDao->where("user_id='" . $userInfo['id'] . "'")->count();
		$statisticsUserDao->add($data);
	}

}

function generateMainLocationStatistics($locationId){

	$locationMainDao = D('LocationMain');
	$shopDao = D('Shop');
	$eventDao = D('Event');

	$where = array();
	$where['main_location_id'] = $locationId;
	$where['is_audit'] = 1;
		
	$locationMainDao->where("id=".$locationId)->setField('shop_count',$shopDao->where($where)->count());
	$locationMainDao->where("id=".$locationId)->setField('event_count',$eventDao->where($where)->count());
}

function generateSubLocationStatistics($locationId){

	$locationSubDao = D('LocationSub');
	$shopDao = D('Shop');
	$eventDao = D('Event');

	$where = array();
	$where['sub_location_id'] = $locationId;
	$where['is_audit'] = 1;

	$locationSubDao->where("id=".$locationId)->setField('shop_count',$shopDao->where($where)->count());
	$locationSubDao->where("id=".$locationId)->setField('event_count',$eventDao->where($where)->count());

}

function generateAllShopCateStatistics($limit){	
	$shopCateDao= D('ShopCate');
	$cateList	= $shopCateDao->limit($limit)->select();
	
	foreach($cateList as $val) {
		$oriCateIdArray[] = $val['id'];
		$cateIdArray[] = generateRelatedCateIDByCateID($val['id']);
	}
	
	foreach($cateIdArray as $val) {
		$val = array_unique($val);
		$tmpShopIdArray = array();
		
		foreach($val as $sval) { 
			$tmpShopIdArray = array_merge($tmpShopIdArray, generateRelatedShopIDByCateID($sval));
		}

		$shopIdArray[] = array_unique($tmpShopIdArray);
	}

	$datas = array();
	for($i = 0; $i < count($cateIdArray); $i++) {
		$data		=	array();
		$data['id']	=	$oriCateIdArray[$i];
		$data['shop_count']	=	count($shopIdArray[$i]);
		$shopCateDao->where($where)->save($data);		
		$datas[] = $data;
	}
	
	return $datas;
}

function generateShopCateStatByShopID($shopId, $action){
	$ShopCateDao		= D('ShopCate');
	$ShopCateMapDao		= D('ShopCateMap');
	$shopCateMapList	= $ShopCateMapDao->where("shop_id =$shopId")->select();
	$cateIdArray = array();

	foreach($shopCateMapList as $val) {
		$cateIdArray[] = $val['cate_id'];
		$tmpCateIdArray = generateParentCateIDListByCateID($val['cate_id']);
		
		foreach($tmpCateIdArray as $tmpVal) {
			$cateIdArray[] = $tmpVal;
		}
	}

	$cateIdArray = array_unique($cateIdArray);
	
	foreach($cateIdArray as $val) {
		switch($action) {
			case 'add':
				$ShopCateDao->query("update `shop_cate` set shop_count = shop_count + 1 where id = '".$val."'");
			break;
			case 'delete':
				$ShopCateDao->query("update `shop_cate` set shop_count = shop_count - 1 where id = '".$val."'");
			break;
		}
	}
	
	return true;
}

function generateParentCateIDListByCateID($cateId) {
	$shopCateDao = D('ShopCate');
	$cateIdArray = array();
	$shopCateList	= $shopCateDao->where("id =$cateId")->select();
	$level 			= $shopCateList[0]['level'];
	$parentCateId   = $shopCateList[0]['parent_id'];
	
	while($level != 1) {
		$cateIdArray[]  = $parentCateId;
		$shopCateList	= $shopCateDao->where("id =$parentCateId")->select();
		$level = $shopCateList[0]['level'];
		$parentCateId = $shopCateList[0]['parent_id'];
	}
	
	return $cateIdArray;
}

function generateRelatedCateIDByCateID($cateId){
	$shopCateDao = D('ShopCate');
	$cateIdArray = array();
	if($cateId) {
		$initialCate 	= $shopCateDao->where('id='.$cateId)->select();
		$cateIdArray[] 	= $initialCate[0]['reference_id']?$initialCate[0]['reference_id']:$initialCate[0]['id'];

		$loopids = array();
		$loopids[] = $cateId;
		$loop = true;
		while($loop) {
			$shopCateList = $shopCateDao->where(array('parent_id'=>array('in',$loopids)))->select();
			$cate = array();
			foreach($shopCateList as $val) {
				$cate[] = $val['id'];
				$cateIdArray[] = $val['reference_id']?$val['reference_id']:$val['id'];
			}
			if (count($cate)) { $loopids = $cate; }
			else { $loop = false; }
		}
	}
	
	return $cateIdArray;
}

function generateRelatedShopIDByCateID($cateId){
	$ShopCateMapDao		= D('ShopCateMap');
	$shopCateMapList 	= $ShopCateMapDao->query("select shop_cate_map.shop_id from shop_cate_map left join shop_fields on shop_cate_map.shop_id=shop_fields.id where shop_fields.is_audit=1 and cate_id =$cateId");
	$shopIdArray 		= array();

	foreach($shopCateMapList as $key=>$val){
		$shopIdArray[] = $val['shop_id'];
	}
	
	return $shopIdArray;
}

/*-----------------------------------------------------------------------------------------------------------------*/

function generateAllEventCateStatistics($limit){	
	$eventCateDao= D('EventCate');
	$cateList	= $eventCateDao->limit($limit)->select();
	
	foreach($cateList as $val) {
		$oriCateIdArray[] = $val['id'];
		$cateIdArray[] = generateEventRelatedCateIDByCateID($val['id']);
	}
	
	foreach($cateIdArray as $val) {
		$val = array_unique($val);
		$tmpEventIdArray = array();
		
		foreach($val as $sval) { 
			$tmpEventIdArray = array_merge($tmpEventIdArray, generateRelatedEventIDByCateID($sval));
		}

		$eventIdArray[] = array_unique($tmpEventIdArray);
	}

	$datas = array();
	for($i = 0; $i < count($cateIdArray); $i++) {
		$data		=	array();
		$data['id']	=	$oriCateIdArray[$i];
		$data['event_count']	=	count($eventIdArray[$i]);
		$eventCateDao->where($where)->save($data);		
		$datas[] = $data;
	}
	
	return $datas;
}

function generateEventCateStatByEventID($eventId, $action){
	$EventCateDao		= D('EventCate');
	$EventFieldsDao		= M('EventFields');
	$eventList	= $EventFieldsDao->where("event_id =$eventId")->select();
	$cateIdArray = array();

	foreach($eventList as $val) {
		$cateIdArray[] = $val['cate_id'];
		$tmpCateIdArray = generateEventParentCateIDListByCateID($val['cate_id']);
		
		foreach($tmpCateIdArray as $tmpVal) {
			$cateIdArray[] = $tmpVal;
		}
	}

	$cateIdArray = array_unique($cateIdArray);
	
	foreach($cateIdArray as $val) {
		switch($action) {
			case 'add':
				$EventCateDao->query("update `event_cate` set event_count = event_count + 1 where id = '".$val."'");
			break;
			case 'delete':
				$EventCateDao->query("update `event_cate` set event_count = event_count - 1 where id = '".$val."'");
			break;
		}
	}
	
	return true;
}

function generateEventParentCateIDListByCateID($cateId) {
	$eventCateDao = D('EventCate');
	$cateIdArray = array();
	$eventCateList	= $eventCateDao->where("id =$cateId")->select();
	$level 			= $eventCateList[0]['level'];
	$parentCateId   = $eventCateList[0]['parent_id'];
	
	while($level != 1) {
		$cateIdArray[]  = $parentCateId;
		$eventCateList	= $eventCateDao->where("id =$parentCateId")->select();
		$level = $eventCateList[0]['level'];
		$parentCateId = $eventCateList[0]['parent_id'];
	}
	
	return $cateIdArray;
}

function generateEventRelatedCateIDByCateID($cateId){
	$eventCateDao = D('EventCate');
	$cateIdArray = array();
	if($cateId) {
		$initialCate 	= $eventCateDao->where('id='.$cateId)->select();
		$cateIdArray[] 	= $initialCate[0]['reference_id']?$initialCate[0]['reference_id']:$initialCate[0]['id'];

		$loopids = array();
		$loopids[] = $cateId;
		$loop = true;
		while($loop) {
			$eventCateList = $eventCateDao->where(array('parent_id'=>array('in',$loopids)))->select();
			$cate = array();
			foreach($eventCateList as $val) {
				$cate[] = $val['id'];
				$cateIdArray[] = $val['reference_id']?$val['reference_id']:$val['id'];
			}
			if (count($cate)) { $loopids = $cate; }
			else { $loop = false; }
		}
	}
	
	return $cateIdArray;
}

function generateRelatedEventIDByCateID($cateId){
	$EventFieldsDao		= M('EventFields');
	$eventCateList 	= $EventFieldsDao->query("select * from event_fields where is_audit=1 and cate_id =$cateId");
	$eventIdArray 		= array();

	foreach($eventCateList as $key=>$val){
		$eventIdArray[] = $val['id'];
	}
	
	return $eventIdArray;
}

/*!
 * ubb2html support for php
 * @requires xhEditor
 * 
 * @author Yanis.Wang<yanis.wang@gmail.com>
 * @site http://xheditor.com/
 * @licence LGPL(http://www.opensource.org/licenses/lgpl-license.php)
 * 
 * @Version: 0.9.6 build 100221
 */
function ubb2html($sUBB)
{	
	$sHtml=$sUBB;
	
	global $emotPath,$cnum,$arrcode,$bUbb2htmlFunctionInit;$cnum=0;$arrcode=array();
	$emotPath='../xheditor_emot/';//表情根路径
	
	if(!$bUbb2htmlFunctionInit){
	function saveCodeArea($match)
	{
		global $cnum,$arrcode;
		$cnum++;$arrcode[$cnum]=$match[0];
		return "[\tubbcodeplace_".$cnum."\t]";
	}}
	$sHtml=preg_replace_callback('/\[code\s*(?:=\s*((?:(?!")[\s\S])+?)(?:"[\s\S]*?)?)?\]([\s\S]*?)\[\/code\]/i','saveCodeArea',$sHtml);
	
	$sHtml=preg_replace("/&/",'&amp;',$sHtml);
	$sHtml=preg_replace("/</",'&lt;',$sHtml);
	$sHtml=preg_replace("/>/",'&gt;',$sHtml);
	$sHtml=preg_replace("/\r?\n/",'<br />',$sHtml);
	
	$sHtml=preg_replace("/\[(\/?)(b|u|i|s|sup|sub)\]/i",'<$1$2>',$sHtml);
	$sHtml=preg_replace('/\[color\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]/i','<span style="color:$1;">',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getSizeName($match)
	{
		$arrSize=array('8pt','10pt','12pt','14pt','18pt','24pt','36pt');
		return '<span style="font-size:'.$arrSize[$match[1]-1].';">';
	}}
	$sHtml=preg_replace_callback("/\[size\s*=\s*(\d+?)\s*\]/i",'getSizeName',$sHtml);
	$sHtml=preg_replace('/\[font\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]/i','<span style="font-family:$1;">',$sHtml);
	$sHtml=preg_replace('/\[back\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]/i','<span style="background-color:$1;">',$sHtml);
	$sHtml=preg_replace("/\[\/(color|size|font|back)\]/i",'</span>',$sHtml);
	
	for($i=0;$i<3;$i++)$sHtml=preg_replace('/\[align\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\](((?!\[align(?:\s+[^\]]+)?\])[\s\S])*?)\[\/align\]/','<p align="$1">$2</p>',$sHtml);
	$sHtml=preg_replace('/\[img\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/img\]/i','<img src="$1" />',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getImg($match)
	{
		$p1=$match[1];$p2=$match[2];$p3=$match[3];$src=$match[4];
		$a=$p3?$p3:(!is_numeric($p1)?$p1:'');
		return '<img src="'.$src.'"'.(is_numeric($p1)?' width="'.$p1.'"':'').(is_numeric($p2)?' height="'.$p2.'"':'').($a?' align="'.$a.'"':'').' />';
	}}
	$sHtml=preg_replace_callback('/\[img\s*=(?:\s*(\d*%?)\s*,\s*(\d*%?)\s*)?(?:,?\s*(\w+))?\s*\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/img\]/i','getImg',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getEmot($match)
	{
		global $emotPath;
		$arr=split(',',$match[1]);
		if(!$arr[1]){$arr[1]=$arr[0];$arr[0]='default';}
		$path=$emotPath.$arr[0].'/'.$arr[1].'.gif';
		return '<img src="'.$path.'" />';
	}}
	$sHtml=preg_replace_callback('/\[emot\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\/\]/i','getEmot',$sHtml);
	$sHtml=preg_replace('/\[url\]\s*(((?!")[\s\S])*?)(?:"[\s\S]*?)?\s*\[\/url\]/i','<a href="$1">$1</a>',$sHtml);
	$sHtml=preg_replace('/\[url\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]\s*([\s\S]*?)\s*\[\/url\]/i','<a href="$1">$2</a>',$sHtml);
	$sHtml=preg_replace('/\[email\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/email\]/i','<a href="mailto:$1">$1</a>',$sHtml);
	$sHtml=preg_replace('/\[email\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]\s*([\s\S]+?)\s*\[\/email\]/i','<a href="mailto:$1">$2</a>',$sHtml);
	$sHtml=preg_replace("/\[quote\]([\s\S]*?)\[\/quote\]/i",'<blockquote>$1</blockquote>',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getFlash($match)
	{
		$w=$match[1];$h=$match[2];$url=$match[3];
		if(!$w)$w=550;if(!$h)$h=400;
		return '<embed type="application/x-shockwave-flash" src="'.$url.'" wmode="opaque" quality="high" bgcolor="#ffffff" menu="false" play="true" loop="true" width="'.$w.'" height="'.$h.'" />';
	}}
	$sHtml=preg_replace_callback('/\[flash\s*(?:=\s*(\d+)\s*,\s*(\d+)\s*)?\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/flash\]/i','getFlash',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getMedia($match)
	{
		$w=$match[1];$h=$match[2];$play=$match[3];$url=$match[4];
		if(!$w)$w=550;if(!$h)$h=400;
		return '<embed type="application/x-mplayer2" src="'.$url.'" enablecontextmenu="false" autostart="'.($play=='1'?'true':'false').'" width="'.$w.'" height="'.$h.'" />';
	}}
	$sHtml=preg_replace_callback('/\[media\s*(?:=\s*(\d+)\s*,\s*(\d+)\s*(?:,\s*(\d+)\s*)?)?\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/media\]/i','getMedia',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getTable($match)
	{
		return '<table'.(isset($match[1])?' width="'.$match[1].'"':'').(isset($match[2])?' bgcolor="'.$match[2].'"':'').'>';
	}}
	$sHtml=preg_replace_callback('/\[table\s*(?:=(\d{1,4}%?)\s*(?:,\s*([^\]"]+)(?:"[^\]]*?)?)?)?\s*\]/i','getTable',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getTR($match){return '<tr'.(isset($match[1])?' bgcolor="'.$match[1].'"':'').'>';}}
	$sHtml=preg_replace_callback('/\[tr\s*(?:=(\s*[^\]"]+))?(?:"[^\]]*?)?\s*\]/i','getTR',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getTD($match){
		$col=isset($match[1])?$match[1]:0;$row=isset($match[2])?$match[2]:0;$w=isset($match[3])?$match[3]:null;
		return '<td'.($col>1?' colspan="'.$col.'"':'').($row>1?' rowspan="'.$row.'"':'').($w?' width="'.$w.'"':'').'>';
	}}
	$sHtml=preg_replace_callback("/\[td\s*(?:=\s*(\d{1,2})\s*,\s*(\d{1,2})\s*(?:,\s*(\d{1,4}%?))?)?\s*\]/i",'getTD',$sHtml);
	$sHtml=preg_replace("/\[\/(table|tr|td)\]/i",'</$1>',$sHtml);
	$sHtml=preg_replace("/\[\*\]([^\[]+)/i",'<li>$1</li>',$sHtml);
	if(!$bUbb2htmlFunctionInit){
	function getUL($match)
	{
		$str='<ul';
		if(isset($match[1]))$str.=' type="'.$match[1].'"';
		return $str.'>';
	}}
	$sHtml=preg_replace_callback('/\[list\s*(?:=\s*([^\]"]+))?(?:"[^\]]*?)?\s*\]/i','getUL',$sHtml);
	$sHtml=preg_replace("/\[\/list\]/i",'</ul>',$sHtml);

	for($i=1;$i<=$cnum;$i++)$sHtml=str_replace("[\tubbcodeplace_".$i."\t]", $arrcode[$i],$sHtml);

	if(!$bUbb2htmlFunctionInit){
	function fixText($match)
	{
		$text=$match[2];
		$text=preg_replace("/\t/",'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$text);
		$text=preg_replace("/  /",' &nbsp;',$text);
		return $match[1].$text;
	}}
	$sHtml=preg_replace_callback('/(^|<\/?\w+(?:\s+[^>]*?)?>)([^<$]+)/i','fixText',$sHtml);
	
	$bUbb2htmlFunctionInit=true;
	
	return $sHtml;
}

function getShopLevelSetting( $name, $level = 0 )
{
	$settings = array(
		'description_len' => array(
			0 => 20,
			1 => 500,
			2 => 1000
		),
		'upload_movie' => array(
			0 => false,
			1 => false,
			2 => true
		),
		'pdf_num' => array(
			0 => 0,
			1 => 3,
			2 => 3
		),
		'add_website' => array(
			0 => false,
			1 => true,
			2 => true
		),
		'photo_num' => array(
			0 => 1,
			1 => 10,
			2 => 20
		),
		'news_num' => array(
			0 => 1,
			1 => 10,
			2 => 20
		),
		'keyword_num' => array(
			0 => 3,
			1 => 5,
			2 => 10
		),
		'notify_review' => array(
			0 => false,
			1 => true,
			2 => true
		),
	);

	return $settings[$name][$level];
}

?>