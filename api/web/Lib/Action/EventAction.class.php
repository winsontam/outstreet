<?php

class EventAction extends BaseAction
{
	public function index()
	{
		$category_id = (int) $_REQUEST[ 'category_id' ];

		$where = array();
		$where[ 'parent_id' ] = $category_id;
		$where[ 'type' ] = array('neq','mobile');
		$temp_cates = D( 'EventCate' )->field( 'id AS category_id, name' )->where( $where )->count() ? D( 'EventCate' )->field( 'if(reference_id,reference_id,id) as category_id, name' )->where( $where )->rows('category_id'): array();
		$categories = array();
		if($category_id==0){
			$index_event_cates=explode(',',C('EVENT_CATE_ORDER'));
			foreach($index_event_cates as $val){
				if ($temp_cates[$val])
					{ $categories[]=$temp_cates[$val]; }
			}
		}else{
			if(C('EVENT_CATE_ORDER_'.$category_id)){
				$sub_cate_order=explode(',',C('EVENT_CATE_ORDER_'.$category_id));
				foreach($sub_cate_order as $key=>$val){
					if ($temp_cates[$val])
						{ $categories[]=$temp_cates[$val]; }
				}
			}else{
				$categories=array_values($temp_cates);
			}
		}

		$where = array();
		$where[ 'a.id' ] = D('EventSpecial')->where('type="iphone"')->getField('event_id');
		$getMallName="if(a.shop_id > 0,(SELECT name from shop_fields where id=a.shop_id),''),' '";
		$getMainSubLocation="(SELECT name from location_sub where id=a.sub_location_id)";
		$special_event = D( 'EventFields' )
		->table('event_fields a')
		->field( 'a.id AS event_id, if(a.name!=a.english_name,CONCAT(a.name," ",a.english_name),a.name) as name, if(c.file_path<>"",CONCAT("'.C('OUTSTREET_DIR').C('EVENT_UPFILE_PATH').'thumb_",c.file_path),"") AS picture_url,b.name as category_names,a.start_time,a.end_time' )
		->join('event_cate b ON a.cate_id = b.id')
		->join('event_picture c ON a.id = c.event_id')
		->where( $where )
		->find();
		$special_event['date_range']=date(chDate(1),$special_event['start_time'])."(".week2ch(date("w",$special_event['start_time'])).") - ".date(chDate(1),$special_event['end_time'])."(".week2ch(date("w",$special_event['end_time'])).")";
		unset($special_event['start_time']);
		unset($special_event['end_time']);
		/********************************************** Start Array Management *********************************************/
		$categories = rtn_array_string($categories);
		/********************************************** End Array Management ***********************************************/
		$json= array(
			'categories' => $categories,
			'special_event' => $special_event,
		);
		$json=utf8_encode_array($json);
		$this->mydisplay($json);
	}

	public function search()
	{
		$shop_id = (int) $_REQUEST[ 'shop_id' ];
		$query = (string) $_REQUEST[ 'query' ];
		if($_REQUEST[ 'category_id' ]) { $category_ids=(array)$_REQUEST[ 'category_id' ]; }
		else { $category_ids=(array)$_REQUEST[ 'category_ids' ]; }
		$main_location_id = (int) $_REQUEST[ 'main_location_id' ];
		$sub_location_ids = (array) $_REQUEST[ 'sub_location_ids' ];
		$googlemap_lat = (float) $_REQUEST[ 'googlemap_lat' ];
		$googlemap_lng = (float) $_REQUEST[ 'googlemap_lng' ];
		$distance = (int) $_REQUEST[ 'distance' ];
		$date_range = $_REQUEST['date_range'];
		$ticket_timediff = $_REQUEST['ticket_timediff'];
		$parent_event_id = (int) $_REQUEST[ 'parent_event_id' ];

		$page = (int) $_REQUEST[ 'page' ];
		if($main_location_id||$sub_location_ids){
			$distance='';
		}
		$category_ids = $this->getsubcateids( $category_ids );

		$where = array();

		$where['is_audit'] = 1;

		$countQuerys = array();

		if ( $query ) {
			$terms = $query;
			$allterms = array();
			if ( preg_match_all( '/\w+/', $terms, $m ) ) {
				foreach ( $m[0] AS $t ) {
					$terms = str_replace($t,'',$terms);
					$allterms[$t] = 'e';
				}
			}
			$mterms = array();
			mb_regex_encoding( 'UTF-8' );
			mb_ereg_search_init( $terms, '\w+' );
			while ( ( $m = mb_ereg_search_regs() ) )
				{ $allterms[ $m[0] ] = 'm'; }

			$searchcols = array(
				'CONCAT(`name`,":",`english_name`)' => 10,
				'`creater_name`' => 8
			);
			$likecolumn = '[likecols]';
			$orQuerys = array();

			foreach ( $allterms AS $term => $type )
			{
				if ( $type == 'e' )
					{
					//$orQuerys[] = $likecolumn . ' REGEXP "[[:<:]]' . $term . '.{0,3}[[:>:]]"';
					$orQuerys[] = $likecolumn . ' REGEXP "' . $term . '"';
					}
				elseif ( $type == 'm' )
				{
					if ( ( $len = preg_match_all( '/./u', $term, $t ) ) > 1 )
					{
						$orQuerys[] = $likecolumn . ' REGEXP "(' . implode('|',$t[0]) . '){' . round($len*0.8) . ',' . ($len) . '}"';
					}
					else
					{
						$orQuerys[] = $likecolumn . ' LIKE "%' . $term . '%"';
					}
				}
				foreach ( $searchcols AS $col => $score ) {
					$countQuerys[] = '( ( ' . $col . ' REGEXP "' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "[[:<:]]' . $term . '[[:>:]]" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "^' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "' . $term . '$" ) * ' . $score . ')'
						. ' + ( IF( length(' . $col . ') = length("' . $term . '"), 1, 0 ) * ' . $score . ')';
				}
			}

			$where['_string'] = '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' OR ', $orQuerys ) ) . ')';
			$countOrder = '(' . implode( ' + ', $countQuerys ) . ') DESC, end_time DESC';
		}
		else
		{
			$countOrder = 'end_time DESC';
		}

		if ($main_location_id) { $where['a.main_location_id']=$main_location_id; }
		if ($sub_location_ids) { $where['a.sub_location_id']=array('in',$sub_location_ids); }
		if ($category_ids) { $where['a.cate_id']=array('in',$category_ids); }
		if($parent_event_id){$where['parent_event_id']=$parent_event_id;};


		/*user GPS distance search*/
		if ($googlemap_lat && $googlemap_lng && floor($googlemap_lat) == 22 && $distance){
			if ($where[ '_string' ]) { $where[ '_string' ] .= ' AND '; }
			else { $where[ '_string' ] = ''; }
			$where[ '_string' ] .= '( 6371 * acos( cos( radians('.$googlemap_lat.') ) * cos( radians( googlemap_lat ) ) * cos( radians( googlemap_lng ) - radians('.$googlemap_lng.') ) + sin( radians('.$googlemap_lat.') ) * sin( radians( googlemap_lat ) ) ) ) <= '.$distance.'/1000';
		}
		/*user GPS distance search*/

		if($shop_id){
			$where['a.shop_id']=$shop_id;
		}

		/*timediff*/
		if( $date_range )
		{
			$times = $this->getdatebyword($date_range);
			if ( $times[0] ) { $start_date = $times[0]; $where['end_time'] = array('egt',$start_date); }
			if ( $times[1] ) { $end_date = $times[1]; $where['start_time'] = array('elt',$end_date); }
		} elseif ($start_date || $end_date) {
			if ( is_int($start_date) ) { $where['end_time'] = array('egt',$start_date); }
			if ( is_int($end_date) ) { $where['start_time'] = array('elt',$end_date); }
		}
		/*timediff*/

		/*input page*/
		$result_per_page=C('RESULT_PER_PAGE');
		$count = D('EventFields');
		$count=$count->table('event_fields a')
		->join('statistics_event c ON a.id=c.event_id')
		->join('event_picture d ON a.id = d.event_id')
		->where($where)->count('distinct a.id');
		$totalpage=ceil($count/$result_per_page);
		if($page==''){
			$page=1;
		}
		/*input page*/
		/*join address into concat*/
		/*join address into concat*/
		$category_name=D('EventCate')->where('id='.$category_id)->getField('name');
		$results = D('EventFields')->field('a.cate_id,a.id as event_id,if(a.name!=a.english_name,CONCAT(a.name," ",a.english_name),a.name) as name,main_location_id,sub_location_id,address,shop_id,rooms, a.googlemap_lat, a.googlemap_lng, if(d.file_path<>"",CONCAT("'.C('OUTSTREET_DIR').C('EVENT_UPFILE_PATH').'thumb_",d.file_path),"") AS picture_url,a.end_time,a.start_time');
		$results=$results->table('event_fields a')
		->join('statistics_event c ON a.id=c.event_id')
		->join('event_picture d ON a.id = d.event_id')
		->where($where)->group('event_id')->limit($result_per_page)->page($page)->order( $countOrder )->select();
		/*Handle results shop_cates display*/
		foreach($results as $key=>$val){
			$results[$key]['main_location_name']=D('LocationMain')->where('id='.$results[$key]['main_location_id'])->getField('name');
			$results[$key]['sub_location_name']=D('LocationSub')->where('id='.$results[$key]['sub_location_id'])->getField('name');
			if($results[$key]['shop_id']!='0'){
				$results[$key]['address'] = D('ShopFields')->where('id='.$results[$key]['shop_id'])->getField('name')." ".$results[$key]['rooms'];
			}
			$results[$key]['address']=$results[$key]['main_location_name']." ".$results[$key]['sub_location_name']." ".$results[$key]['address'];
			$results[$key]['category_names']=D('EventCate')->where('id='.$val['cate_id'])->getField('name');
			//unset($results[$key]['scores']);
			unset($results[$key]['cate_id']);
			$results[$key]['date_range']=date(chDate(1),$val['start_time'])."(".week2ch(date("w",$val['start_time'])).") - ".date(chDate(1),$val['end_time'])."(".week2ch(date("w",$val['end_time'])).")";
			if( mktime(23,59,59,date('n',$val['end_time']),date('j',$val['end_time']),date('Y',$val['end_time'])) < mktime(23,59,59,date('n'),date('j'),date('Y')) ){
				$results[$key]['name']="(已完結) ".$val['name'];
			}

			//unset($results[$key]['end_time']);
			unset($results[$key]['start_time']);
			unset($results[$key]['main_location_id']);
			unset($results[$key]['sub_location_id']);
			unset($results[$key]['main_location_name']);
			unset($results[$key]['sub_location_name']);
			unset($results[$key]['shop_id']);
			unset($results[$key]['rooms']);
			//unset($results[$key]['address']);
		}
		/*Handle results shop_cates display*/

		/********************************************** Start Array Management *********************************************/
		$results = rtn_array_string($results);
		/********************************************** End Array Management ***********************************************/
		$json= array(
			'total_page' => $totalpage,
			'total_number' => $count,
			'category_id' => $category_id,
			'category_name' => $category_name,
			'results' => $results,
		);
		$json=utf8_encode_array($json);
		$this->mydisplay($json);
	}

	public function email(){
		$event_id=$_REQUEST['event_id'];
		$eventInfo=D('EventFields')
		->field('english_name,name,end_time,start_time,shop_id,address,main_location_id,sub_location_id,cate_id,details as description')
		->where('id='.$event_id)
		->find();
		if($eventInfo['english_name']!=''&&$eventInfo['english_name']!=$eventInfo['name']){
			$eventInfo['name'].=' '.$eventInfo['english_name'];
		}
		if( mktime(23,59,59,date('n',$eventInfo['end_time']),date('j',$eventInfo['end_time']),date('Y',$eventInfo['end_time'])) < mktime(23,59,59,date('n'),date('j'),date('Y')) ){
			$eventInfo['name'].="(已完結)";
		}
		$eventInfo['main_location_name']=D('LocationMain')->where('id='.$eventInfo['main_location_id'])->getField('name');
		$eventInfo['sub_location_name']=D('LocationSub')->where('id='.$eventInfo['sub_location_id'])->getField('name');
		if($eventInfo['shop_id']!=0){
			$eventInfo['address'] = D('ShopFields')->where('id='.$eventInfo['shop_id'])->getField('name')." ".$eventInfo['address'];
		}
		$eventInfo['address']=$eventInfo['main_location_name']." ".$eventInfo['sub_location_name']." ".$eventInfo['address'];
		$eventInfo['category_names']=D('EventCate')->where('id='.$eventInfo['cate_id'])->getField('name');
		$eventInfo['date_range']=date(chDate(1),$eventInfo['start_time'])."(".week2ch(date("w",$eventInfo['start_time'])).") - ".date(chDate(1),$eventInfo['end_time'])."(".week2ch(date("w",$eventInfo['end_time'])).")";
		$eventInfo['link']=C('OUTSTREET_DIR').'/event/view/id/'.$event_id;
		$title=trim($eventInfo['name'])." (".$eventInfo['sub_location_name'].")";

		$content = "";
		$content .= "\n";
		$content .= "節目名稱 : ".$eventInfo['name']."\n";
		$content .= "節目地址 : ".$eventInfo['address']."\n";
		$content .= "節目地區 : ".$eventInfo['sub_location_name']."\n";
		$content .= "節目類別 : ".$eventInfo['category_names']."\n";
		$content .= "節目時間 : ".$eventInfo['date_range']."\n";
		if($eventInfo['description']){$content .= "節目介紹 : ".$eventInfo['description']."\n";}
		$content .= "節目網址 : ".$eventInfo['link'];
		$json=array( 'title'=>$title,'content' => $content);
		$this->mydisplay($json);
	}

	public function view(){
		$event_id=$_REQUEST['event_id'];
		$eventInfo=D('EventFields')->where('id='.$event_id)->find();
		if($eventInfo['english_name']!=''&&$eventInfo['english_name']!=$eventInfo['name']){
			$eventInfo['name'].=' '.$eventInfo['english_name'];
		}
		if( mktime(23,59,59,date('n',$eventInfo['end_time']),date('j',$eventInfo['end_time']),date('Y',$eventInfo['end_time'])) < mktime(23,59,59,date('n'),date('j'),date('Y')) ){
			$eventInfo['name'].="(已完結)";
		}
		$eventInfo['event_id']=$event_id;
		$eventInfo['main_location_name']=D('LocationMain')->where('id='.$eventInfo['main_location_id'])->getField('name');
		$eventInfo['sub_location_name']=D('LocationSub')->where('id='.$eventInfo['sub_location_id'])->getField('name');
		if($eventInfo['shop_id']!='0'){
			$eventInfo['address'] = D('ShopFields')->where('id='.$eventInfo['shop_id'])->getField('name')." ".$eventInfo['rooms'];
		}
		$eventInfo['address']=$eventInfo['main_location_name']." ".$eventInfo['sub_location_name']." ".$eventInfo['address'];

		/*ticket*/
		$eventInfo['ticket_price']=$eventInfo['price']?$eventInfo['price']:'';
		$eventInfo['ticket_date']=$eventInfo['ticket_startdate']?date(chDate(1),$eventInfo['ticket_startdate'])."(".week2ch(date("w",$eventInfo['ticket_startdate'])).")":'';
		$eventInfo['ticket_website']=$eventInfo['ticket_website']?$eventInfo['ticket_website']:'';
		$eventInfo['ticket_phone']=$eventInfo['ticket_phone']?formatTelephone($eventInfo['ticket_phone'],1,$startSalt,$endSalt,true):'';
		/*ticket*/
		$eventInfo['telephone']=$eventInfo['telephone']?formatTelephone($eventInfo['telephone'],1,$startSalt,$endSalt,true):'';

		$eventInfo['description']=$eventInfo['details'];
		$eventInfo['date_range']=date(chDate(1),$eventInfo['start_time'])."(".week2ch(date("w",$eventInfo['start_time'])).") - ".date(chDate(1),$eventInfo['end_time'])."(".week2ch(date("w",$eventInfo['end_time'])).")";
		$eventInfo['address']=$eventInfo['address']." ".$eventInfo['rooms'];
		$eventInfo['review_count']=D('StatisticsEvent')->where('event_id='.$event_id)->getField('review_count');

		$eventInfo['category_names']=D('EventCate')->where('id='.$eventInfo['cate_id'])->getField('name');
		$eventInfo['category_id']=$eventInfo['cate_id'];
		$eventInfo['is_sub_event']=$eventInfo['parent_event_id']?1:0;
		$eventInfo['is_main_event']=D('EventFields')->where('parent_event_id='.$event_id)->count()?1:0;

		if($eventInfo['is_main_event']){
			$eventInfo['time_more']='';
			$sub_event=D('EventFields')->where(array('parent_event_id'=>$event_id,'is_audit'=>1))->select();
			foreach ($sub_event as $key=>$val){
				if($key!=0){
					$eventInfo['time_more'].="\n\r";
				}
				$eventInfo['time_more'].= $sub_event[$key]['name']." ".date(chDate(1),$sub_event[$key]['start_time'])."(".week2ch(date("w",$sub_event[$key]['start_time'])).") - ".date(chDate(1),$sub_event[$key]['end_time'])."(".week2ch(date("w",$sub_event[$key]['end_time'])).")";
			}
		}

		$getPictures=D('EventPicture')->field('file_path')->where('event_id='.$event_id)->select();
		foreach($getPictures as $key=>$val){
			$eventInfo['pictures'][$key]=C('OUTSTREET_DIR').C('Event_UPFILE_PATH').$val['file_path'];
		}
		$eventInfo['reviews']=D('ReviewFields')
		->field('a.id as review_id,a.title,a.description,c.nick_name as user_name, if(c.face_path,CONCAT("'.C('OUTSTREET_DIR').C('USERFACE_UPFILE_PATH').'face",c.face_path),CONCAT("'.C('OUTSTREET_DIR').C('USERDEFAULTFACE_UPFILE_PATH').'",c.face_path)) AS user_picture_url,FROM_UNIXTIME(a.create_time,"%d/%m") as create_time,FROM_UNIXTIME(a.create_time,"%w") as week_time')
		->table('review_fields a')
		->join('user_fields c ON a.user_id=c.id')
		->where(array('a.event_id'=>$event_id,'a.is_audit'=>'1'))
		->select();
		foreach ($eventInfo['reviews'] as $key=>$val){
			$eventInfo['reviews'][$key]['week_time']=week2ch($eventInfo['reviews'][$key]['week_time']);
			$eventInfo['reviews'][$key]['description']=preg_replace("/<br(.*?)\/+?>/", "\n", $eventInfo['reviews'][$key]['description']);
			$eventInfo['reviews'][$key]['description']=strip_tags($eventInfo['reviews'][$key]['description']);
			$eventInfo['reviews'][$key]['create_time'].="(".$eventInfo['reviews'][$key]['week_time'].")";
			unset($eventInfo['reviews'][$key]['week_time']);
		}
		unset($eventInfo['id']);
		unset($eventInfo['create_time']);
		unset($eventInfo['update_time']);
		unset($eventInfo['rooms']);
		unset($eventInfo['eng_address']);
		unset($eventInfo['email']);
		unset($eventInfo['keyword']);
		unset($eventInfo['picstamp']);
		unset($eventInfo['is_audit']);
		unset($eventInfo['user_id']);
		unset($eventInfo['english_name']);
		unset($eventInfo['price']);
		unset($eventInfo['ticket_startdate']);
		unset($eventInfo['ticket_required']);
		unset($eventInfo['is_cancelled']);
		unset($eventInfo['is_recommend']);
		unset($eventInfo['is_recommend_new']);
		//unset($eventInfo['shop_id']);
		unset($eventInfo['start_time']);
		unset($eventInfo['end_time']);
		unset($eventInfo['cate_id']);
		unset($eventInfo['getting_there']);
		unset($eventInfo['cost_fixed']);
		unset($eventInfo['attending_cate']);
		unset($eventInfo['details']);
		unset($eventInfo['max_person']);
		unset($eventInfo['payment_details']);


		/********************************************** Start Array Management *********************************************/
		$eventInfo['reviews'] = rtn_array_string($eventInfo['reviews']);
		$eventInfo['pictures'] = rtn_array_string($eventInfo['pictures']);
		/********************************************** End Array Management ***********************************************/
		$json= array( 'eventInfo' => $eventInfo );
		$json=utf8_encode_array($json);
		$this->mydisplay($json);
	}

	public function event_category(){
		$level=(int)$_REQUEST['level'];
		$parent_category_id=(int)$_REQUEST['parent_category_id'];
		if($level){$where['level']=$level;}
		if($parent_category_id){$where['parent_id']=$parent_category_id;}
		$where[ 'type' ] = array('neq','mobile');
		$result=D('EventCate')
		->field('if(reference_id,reference_id,id) as id,name,parent_id as parent_category_id,level')
		->where($where)
		->rows('id');

		if (!$parent_category_id)
		{
			$orders = $index_orders = explode(',',C('EVENT_CATE_ORDER'));

			foreach( $index_orders AS $index_order )
			{
				$orders = array_merge($orders,explode(',',C('EVENT_CATE_ORDER_'.$index_order)));
			}
		}
		elseif ( C('EVENT_CATE_ORDER_'.$parent_category_id) )
		{
			$orders = explode(',',C('EVENT_CATE_ORDER_'.$parent_category_id));
		}

		$categories = array();
		foreach($orders as $key=>$val){
			if ($result[$val])
			{
				$categories[]=$result[$val];
				unset($result[$val]);
			}
		}

		$categories = rtn_array_string(array_merge($categories,array_values($result)));
		$json=(array('category_count'=>count($categories),'category'=>$categories));
		$this->mydisplay($json);
	}

	public function facebook()
	{
		$event_id = (int)$_REQUEST['event_id'];
		$title = '想同你分享「%1$s」呢個節目，去OutStreet睇下啦！';

		$eventInfo = D( 'EventFields' )->where( array('id'=>$event_id) )->find();
		$info = array();
		$info['title'] = sprintf( $title, $eventInfo['name'] );
		$info['link'] = C('OUTSTREET_DIR').'/event/view/id/'.$event_id;
		$info['description'] = $eventInfo['details'];

		$json=utf8_encode_array($info);
		$this->mydisplay($json);
	}

	private function getsublocationids($mainlocation_id) {
		$locationsubDao=D('LocationSub');
		return $locationsubDao->where(array('main_location_id'=>$mainlocation_id))->rows(null,'id');
	}

	private function getsubcateids($category_ids) {
		$eventCateDao=D('EventCate');
		$search_cate_ids = $category_ids;
		$loopids = array();
		$loopids = $category_ids;
		$loop = true;

		while($loop) {
			$eventCateList = $eventCateDao->where(array('parent_id'=>array('in',$loopids)))->select();
			$cate = array();
			foreach( $eventCateList AS $val ) {
				$cate[] = $val['id'];
				$search_cate_ids[] = $val['reference_id']?$val['reference_id']:$val['id'];
			}
			if ( count($cate) ) { $loopids = $cate; }
			else { $loop = false; }
		}
		return $search_cate_ids;
	}

	public function getdatebyword($timediff){
		$firsttime = 0;
		$lasttime = 0;
		switch($timediff){
			case '1':
				$firsttime = strtotime(date('Y-m-d 00:00:00'));
				$lasttime = strtotime(date('Y-m-d 23:59:59'));
			break;
			case '2':
				$firsttime = strtotime(date('Y-m-d 00:00:00')) + 60 * 60 * 24;
				$lasttime = strtotime(date('Y-m-d 23:59:59')) + 60 * 60 * 24;
			break;
			case '3':
				$firsttime = strtotime(date('Y-m-d 00:00:00')) - date('w') * 24 * 60 * 60;
				$lasttime = strtotime(date('Y-m-d 23:59:59')) + (6 - date('w')) * 24 * 60 * 60;
			break;
			case 'weekend':
				if ( date( 'N' ) < 5 ){
					$weekendstart = 5-date( 'N' );
					$firsttime = mktime(0,0,0,date('n'),date('j')+$weekendstart,date('Y'));
				}
				else { $firsttime = strtotime(date('Y-m-d 00:00:00')); }
				$weekendend = 7-date( 'N' );
				$lasttime = mktime(23,59,59,date('n'),date('j')+$weekendend,date('Y'));
			break;
			case '4':
				$firsttime = strtotime(date('Y-m-d 00:00:00'));
				$lasttime = strtotime(date('Y-m-30 23:59:59'));
			break;
		}
		return array( $firsttime, $lasttime );
	}
}
?>