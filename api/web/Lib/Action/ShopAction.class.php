<?php

class ShopAction extends BaseAction
{

	public function index()
	{
		$category_id = (int) $_REQUEST[ 'category_id' ];
		$where = array();
		$where[ 'parent_id' ] = $category_id;
		$where[ 'type' ] = array( 'neq', 'website' );
		$temp_cates = D( 'ShopCate' )->field( 'id AS category_id, name, end_node' )->where( $where )->count() ? D( 'ShopCate' )->field( 'if(reference_id,reference_id,id) as category_id, name, end_node,id' )->where( $where )->order( 'shop_count desc' )->rows( 'id' ) : array();
		foreach ( $temp_cates as $key => $val )
		{
			unset( $temp_cates[ $key ][ 'id' ] );
		}
		$categories = array();
		if ( $category_id == 0 )
		{
			$index_shop_cates = explode( ',', C( 'SHOP_CATE_ORDER' ) );
			foreach ( $index_shop_cates as $val )
			{
				if ( $temp_cates[ $val ] )
				{
					$categories[] = $temp_cates[ $val ];
				}
			}
		}
		else
		{
			if ( C( 'SHOP_CATE_ORDER_' . $category_id ) )
			{
				$sub_cate_order = explode( ',', C( 'SHOP_CATE_ORDER_' . $category_id ) );
				foreach ( $sub_cate_order as $key => $val )
				{
					if ( $temp_cates[ $val ] )
					{
						$categories[] = $temp_cates[ $val ];
					}
				}
			}
			else
			{
				$categories = array_values( $temp_cates );
			}
		}
		$where = array();
		$where[ 'a.id' ] = D( 'ShopSpecial' )->where( 'type="iphone"' )->getField( 'shop_id' );
		$getParentShopAddress = "if(a.parent_shop_id > 0,(SELECT CONCAT(name,' ') as name from shop_fields where id=a.parent_shop_id),'')";
		$getMainSubLocation = "(SELECT name from location_sub where id=a.sub_location_id)";
		$special_shop = D( 'ShopFields' )
			->table( 'shop_fields a' )
			->field( 'a.id AS shop_id, if(a.name!=a.eng_name,CONCAT(a.name," ",a.eng_name),a.name) as name, b.avg_rate_1 as rating, CONCAT(' . $getMainSubLocation . '," ",a.address," ",' . $getParentShopAddress . ', a.rooms) AS address, if(c.file_path<>"",CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'SHOP_UPFILE_PATH' ) . '",c.file_path),"") AS picture_url' )
			->join( 'statistics_shop b ON a.id = b.shop_id' )
			->join( 'shop_picture c ON a.id = c.shop_id' )
			->where( $where )
			->find();

		$cates = D( 'ShopCateMap' )->field( 'b.name' )->table( 'shop_cate_map a' )->join( 'shop_cate b ON a.cate_id=b.id' )->where( 'shop_id=' . $special_shop[ 'shop_id' ] )->select();
		$tempstring = "";
		foreach ( $cates as $key2 => $names )
		{
			if ( $key2 != 0 )
			{
				$tempstring.=',';
			}
			$tempstring.=$names[ 'name' ];
		}
		$special_shop[ 'category_names' ] = $tempstring;

		/*		 * ******************************************** Start Array Management ******************************************** */
		$categories = rtn_array_string( $categories );
		/*		 * ******************************************** End Array Management ********************************************** */
		//$json= utf8_encode_array(array( 'categories' => $categories, 'special_shop' => $special_shop ));
		$json = utf8_encode_array( array( 'categories' => $categories ) );
		$this->mydisplay( $json );
	}

	public function search()
	{
		$query = (string) $_REQUEST[ 'query' ];
		if ( $_REQUEST[ 'category_id' ] )
		{
			$category_ids = (array) $_REQUEST[ 'category_id' ];
		}
		else
		{
			$category_ids = (array) $_REQUEST[ 'category_ids' ];
		}
		$main_location_id = (int) $_REQUEST[ 'main_location_id' ];
		$sub_location_ids = (array) $_REQUEST[ 'sub_location_ids' ];
		$googlemap_lat = (float) $_REQUEST[ 'googlemap_lat' ];
		$googlemap_lng = (float) $_REQUEST[ 'googlemap_lng' ];
		$distance = (int) $_REQUEST[ 'distance' ];
		$price_range = (int) $_REQUEST[ 'price_range' ];
		$is_open_now = (bool) $_REQUEST[ 'is_open_now' ];
		$mall_id = (int) $_REQUEST[ 'mall_id' ];
		$brand_id = (int) $_REQUEST[ 'brand_id' ];
		$page = (int) $_REQUEST[ 'page' ];
		if ( $main_location_id || $sub_location_ids )
		{
			$distance = '';
		}

		$category_ids = $this->getsubcateids( $category_ids );

		$where = array();

		$where[ 'is_audit' ] = 1;
		if ( $price_range )
		{
			$where[ 'price_range' ] = array( 'in', $price_range );
		}

		$countQuerys = array();

		if ( $query )
		{
			$terms = $query;
			$allterms = array();
			if ( preg_match_all( '/\w+/', $terms, $m ) )
			{
				foreach ( $m[ 0 ] AS $t )
				{
					$terms = str_replace( $t, '', $terms );
					$allterms[ $t ] = 'e';
				}
			}
			$mterms = array();
			mb_regex_encoding( 'UTF-8' );
			mb_ereg_search_init( $terms, '\w+' );
			while ( ( $m = mb_ereg_search_regs() ) )
			{
				$allterms[ $m[ 0 ] ] = 'm';
			}

			$suggestWhere = array();
			foreach ( $allterms AS $term => $type )
			{
				$suggestWhere[ 'keyword' ][] = array( 'like', $term );
			}
			$suggestWhere[ 'keyword' ][] = 'or';
			$suggestKeywords = M( 'KeywordTranslate' )->where( $suggestWhere )->select();
			foreach ( $suggestKeywords AS $val )
			{
				$keywords = explode( ',', $val[ 'suggest_keywords' ] );
				foreach ( $keywords AS $k )
				{
					if ( $k )
					{
						$allterms[ $k ] = 's';
					}
				}
			}

			$searchcols = array(
				'CONCAT(`name`,":",`eng_name`)'	 => 10,
				'`keyword`'						 => 6,
				'`address`'						 => 4
			);
			$likecolumn = '[likecols]';
			$orQuerys = array();

			foreach ( $allterms AS $term => $type )
			{
				if ( $type == 'e' || $type == 's' )
				{
					//$orQuerys[] = $likecolumn . ' REGEXP "[[:<:]]' . $term . '.{0,3}[[:>:]]"';
					$orQuerys[] = $likecolumn . ' REGEXP "' . $term . '"';
				}
				elseif ( $type == 'm' )
				{
					if ( ( $len = preg_match_all( '/./u', $term, $t ) ) > 1 )
					{
						$orQuerys[] = $likecolumn . ' REGEXP "(' . implode( '|', $t[ 0 ] ) . '){' . round( $len * 0.8 ) . ',' . ($len) . '}"';
					}
					else
					{
						$orQuerys[] = $likecolumn . ' LIKE "%' . $term . '%"';
					}
				}
				foreach ( $searchcols AS $col => $score )
				{
					$countQuerys[] = '( ( ' . $col . ' REGEXP "' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "[[:<:]]' . $term . '[[:>:]]" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "^' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "' . $term . '$" ) * ' . $score . ')'
						. ' + ( IF( length(' . $col . ') = length("' . $term . '"), 1, 0 ) * ' . $score . ')';
				}
			}
			/* for search category */
			$searchcateids = D( 'ShopCate' )->where( array( 'type' => array( 'neq', 'website' ), '_string' => '(' . str_replace( '[likecols]', 'name', implode( ' OR ', $orQuerys ) ) . ')' ) )->rows( null, 'id' );
			if ( count( $searchcateids ) )
			{
				$where[ '_string' ] = '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' AND ', $orQuerys ) ) . ' OR b.cate_id IN (' . implode( ',', $searchcateids ) . '))';
				$countQuerys[] = '((b.cate_id IN (' . implode( ',', $searchcateids ) . '))*20)';
			}
			else
			{
				$where[ '_string' ] = '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' AND ', $orQuerys ) ) . ')';
			}
		}

		$countQuerys[] = '( IF( length(telephone), 1, 0 ) )' . ' + ( IF( a.shop_status > 0, 1, 0 ) * -5 )' . ' + ( IF( d.id>0, 5, 0 ) )';
		$countField = '((' . implode( ' + ', $countQuerys ) . ') + ( IF(c.review_count is null,0,c.review_count) ) + ( IF ( a.shop_level, a.shop_level*1000, 0 ) ) ) AS scores';

		if ( $main_location_id )
		{
			$where[ 'a.main_location_id' ] = $main_location_id;
		}
		if ( $sub_location_ids )
		{
			$where[ 'a.sub_location_id' ] = array( 'in', $sub_location_ids );
		}
		if ( $mall_id )
		{
			$where[ 'a.parent_shop_id' ] = $mall_id;
		}
		if ( $price_range )
		{
			$where[ 'a.price_range' ] = $price_range;
		}
		if ( $brand_id )
		{
			$where[ 'a.brand_id' ] = $brand_id;
		}
		if ( $category_ids )
		{
			$where[ 'b.cate_id' ] = array( 'in', $category_ids );
		}

		/* is open now */
		if ( $is_open_now )
		{
			$today_date = date( "Y-m-d", time() );

			$today_weekday = date( "N", time() );

			$today_time = date( "Hm", time() );

			if ( D( 'PublicHoliday' )->where( 'holiday="' . $today_date . '"' )->select() )
			{
				if ( $where[ '_string' ] )
				{
					$where[ '_string' ] .= ' AND ';
				}
				else
				{
					$where[ '_string' ] = '';
				}
				$where[ '_string' ] .= "
				(
					(e.recurring_week=8
					and
					(
						if(e.start_time>e.end_time," . $today_time . " between e.start_time and e.end_time+2400," . $today_time . " between e.start_time and e.end_time))
						OR
						(if(" . ($today_weekday - 1) . "=0,e.recurring_week=7,e.recurring_week=" . ($today_weekday - 1) . ") and " . $today_time . " between '0000' and e.end_time)

					)
				)";
			}
			else
			{
				if ( $where[ '_string' ] )
				{
					$where[ '_string' ] .= ' AND ';
				}
				else
				{
					$where[ '_string' ] = '';
				}
				$where[ '_string' ] .= "
				(
					(e.recurring_week=" . $today_weekday . "
					and
					(

						if(e.start_time>e.end_time," . $today_time . " between e.start_time and e.end_time+2400," . $today_time . " between e.start_time and e.end_time))
						OR
						(if(" . ($today_weekday - 1) . "=0,e.recurring_week=7,e.recurring_week=" . ($today_weekday - 1) . ") and " . $today_time . " between '0000' and e.end_time)
					)
				)";
			}
		}
		/* is open now */

		/* user GPS distance search */
		if ( $googlemap_lat && $googlemap_lng && floor( $googlemap_lat ) == 22 && $distance )
		{
			if ( $where[ '_string' ] )
			{
				$where[ '_string' ] .= ' AND ';
			}
			else
			{
				$where[ '_string' ] = '';
			}
			$where[ '_string' ] .= '( 6371 * acos( cos( radians(' . $googlemap_lat . ') ) * cos( radians( googlemap_lat ) ) * cos( radians( googlemap_lng ) - radians(' . $googlemap_lng . ') ) + sin( radians(' . $googlemap_lat . ') ) * sin( radians( googlemap_lat ) ) ) ) <= ' . $distance . '/1000';
		}
		/* user GPS distance search */

		/* input page */
		$result_per_page = C( 'RESULT_PER_PAGE' );
		$count = D( 'ShopFields' );
		if ( $is_open_now )
		{
			$count = $count->join( 'JOIN shop_time e on a.id = e.shop_id' );
		}
		$count = $count->table( 'shop_fields a' )
				->join( 'JOIN shop_cate_map b ON a.id = b.shop_id' )
				->join( 'statistics_shop c ON a.id = c.shop_id' )
				->join( 'shop_picture d ON a.id = d.shop_id' )
				->where( $where )->count( 'distinct a.id' );
		$totalpage = ceil( $count / $result_per_page );
		if ( $page == '' )
		{
			$page = 1;
		}
		/* input page */
		/* join address into concat */
		$getParentShopAddress = "if(a.parent_shop_id > 0,(SELECT CONCAT(name,' ') as name from shop_fields where id=a.parent_shop_id),'')";
		$getMainSubLocation = "(SELECT name from location_sub where id=a.sub_location_id)";
		/* join address into concat */
		$category_name = D( 'ShopCate' )->where( array( 'type' => array( 'neq', 'website' ), 'id' => $category_id ) )->getField( 'name' );
		$results = D( 'ShopFields' )->field( 'c.avg_rate_1 as rating,b.shop_id,if(a.name!=a.eng_name,CONCAT(a.name," ",a.eng_name),a.name) as name, CONCAT(' . $getMainSubLocation . '," ",a.address," ",' . $getParentShopAddress . ',a.rooms) AS address, a.googlemap_lat, a.googlemap_lng, if(d.file_path<>"",CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'SHOP_UPFILE_PATH' ) . 'thumb_",d.file_path),"") AS picture_url,a.description as remarks,a.remarks as sub_remarks,shop_status,' . $countField );
		if ( $is_open_now )
		{
			$results = $results->join( 'JOIN shop_time e on a.id = e.shop_id' );
		}
		$results = $results->table( 'shop_fields a' )
				->join( 'JOIN shop_cate_map b ON a.id = b.shop_id' )
				->join( 'statistics_shop c ON a.id = c.shop_id' )
				->join( 'shop_picture d ON a.id = d.shop_id' )
				->where( $where )->group( 'a.id' )->limit( $result_per_page )->page( $page )->order( 'a.shop_status, scores DESC, hit_rate DESC' )->select();

		/* Handle results shop_cates display */
		foreach ( $results as $key => $val )
		{
			$cates = D( 'ShopCateMap' )->field( 'b.name' )->table( 'shop_cate_map a' )->join( 'shop_cate b ON a.cate_id=b.id' )->where( 'shop_id=' . $val[ 'shop_id' ] )->select();
			$tempstring = "";
			unset( $results[ $key ][ 'scores' ] );
			foreach ( $cates as $key2 => $names )
			{
				if ( $key2 != 0 )
				{
					$tempstring.=',';
				}
				$tempstring.=$names[ 'name' ];
			}
			$results[ $key ][ 'category_names' ] = $tempstring;
			foreach ( $cates as $cate )
			{
				if ( $cate[ 'name' ] == '停車場' )
				{
					$remarks = explode( "\n", $results[ $key ][ 'remarks' ] );
					$results[ $key ][ 'sub_remarks' ] = $results[ $key ][ 'remarks' ];
				}
			}
			if ( $val[ 'shop_status' ] != 0 )
			{
				switch ( $val[ 'shop_status' ] )
				{
					case 1:
						$results[ $key ][ 'name' ] = ' (已搬址) ' . $results[ $key ][ 'name' ];
						break;
					case 2:
						$results[ $key ][ 'name' ] = ' (已結業) ' . $results[ $key ][ 'name' ];
						break;
					case 3:
						$results[ $key ][ 'name' ] = ' (裝修中) ' . $results[ $key ][ 'name' ];
						break;
				}
			}
			unset( $results[ $key ][ 'shop_status' ] );
		}

		/*		 * ******************************************** Start Array Management ******************************************** */
		$results = rtn_array_string( $results );
		/*		 * ******************************************** End Array Management ********************************************** */

		/* Handle results shop_cates display */
		$json = utf8_encode_array( array(
			'total_page'	 => $totalpage,
			'total_number'	 => $count,
			'category_id'	 => $category_id,
			'category_name'	 => $category_name,
			'results'		 => $results,
		) );
		$this->mydisplay( $json );
	}

	public function view()
	{
		$shop_id = $_REQUEST[ 'shop_id' ];
		$shopInfo = D( 'ShopFields' )->where( 'id=' . $shop_id )->find();
		if ( $shopInfo[ 'eng_name' ] != '' && $shopInfo[ 'eng_name' ] != $shopInfo[ 'name' ] )
		{
			$shopInfo[ 'name' ].=' ' . $shopInfo[ 'eng_name' ];
		}
		if ( $shopInfo[ 'shop_status' ] != 0 )
		{
			switch ( $shopInfo[ 'shop_status' ] )
			{
				case 1:
					$shopInfo[ 'name' ].=' (已搬址)';
					break;
				case 2:
					$shopInfo[ 'name' ].=' (已結業)';
					break;
				case 3:
					$shopInfo[ 'name' ].=' (裝修中)';
					break;
			}
		}
		$shopInfo[ 'shop_id' ] = $shop_id;
		$shopInfo[ 'mall_id' ] = $shopInfo[ 'parent_shop_id' ];
		$shopInfo[ 'main_location_name' ] = D( 'LocationMain' )->where( 'id=' . $shopInfo[ 'main_location_id' ] )->getField( 'name' );
		$shopInfo[ 'sub_location_name' ] = D( 'LocationSub' )->where( 'id=' . $shopInfo[ 'sub_location_id' ] )->getField( 'name' );
		if ( $shopInfo[ 'mall_id' ] != 0 )
		{
			$shopInfo[ 'address' ] = D( 'ShopFields' )->where( 'id=' . $shopInfo[ 'mall_id' ] )->getField( 'name' ) . " " . $shopInfo[ 'address' ];
		}
		$shopInfo[ 'telephone' ] = formatTelephone( $shopInfo[ 'telephone' ], 1, $startSalt, $endSalt, true );
		$shopInfo[ 'address' ] = $shopInfo[ 'main_location_name' ] . " " . $shopInfo[ 'sub_location_name' ] . " " . $shopInfo[ 'address' ];
		$shopInfo[ 'brand_name' ] = D( 'ShopBrand' )->where( 'id=' . $shopInfo[ 'brand_id' ] )->getField( 'name' );

		$shopInfo[ 'address' ] = $shopInfo[ 'address' ] . " " . $shopInfo[ 'rooms' ];
		$shopInfo[ 'rating' ] = D( 'StatisticsShop' )->where( 'shop_id=' . $shop_id )->getField( 'avg_rate_1' );

		$working_hour = D( 'ShopTime' )->where( 'shop_id=' . $shop_id )->order( 'start_time,end_time,recurring_week' )->select();
		$where = array( "shop_id" => $shop_id, "recurring_week " => 0 );
		$special_working_hour = D( 'ShopTime' )->where( $where )->order( 'start_day,end_day' )->select();
		$shopInfo[ 'worktime' ] = generateShopWorkTime( $shopInfo[ 'work_time' ], $working_hour, $special_working_hour );

		$ratingDetail = D( 'StatisticsShop' )->where( 'shop_id=' . $shop_id )->find();
		for ( $i = 0; $i < 6; $i++ )
		{
			$shopInfo[ 'ratings' ][ $i ] = $ratingDetail[ 'avg_rate_' . ($i + 2) ];
		}
		$shopInfo[ 'review_count' ] = D( 'StatisticsShop' )->where( 'shop_id=' . $shop_id )->getField( 'review_count' );
		$shopInfo[ 'ratings' ][ 6 ] = D( 'StatisticsShop' )->where( 'shop_id=' . $shop_id )->getField( 'total_rating' );
		$getCates = D( 'ShopCateMap' )->where( 'shop_id=' . $shop_id )->select();
		foreach ( $getCates as $key => $val )
		{
			if ( $shopInfo[ 'category_names' ] )
			{
				$shopInfo[ 'category_names' ].=',';
			}
			$shopInfo[ 'category_names' ].=D( 'ShopCate' )->where( array( 'type' => array( 'neq', 'website' ), 'id' => $val[ 'cate_id' ] ) )->getField( 'name' );
		}
		$getPictures = D( 'ShopPicture' )->field( 'file_path' )->where( 'Shop_id=' . $shop_id )->select();
		foreach ( $getPictures as $key => $val )
		{
			$shopInfo[ 'pictures' ][ $key ] = C( 'OUTSTREET_DIR' ) . C( 'SHOP_UPFILE_PATH' ) . $val[ 'file_path' ];
		}

		$shopInfo[ 'reviews' ] = D( 'ReviewFields' )
			->field( 'a.id as review_id,a.title,a.description,b.rate_1 as rating,c.nick_name as user_name, if(c.face_path,CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'USERFACE_UPFILE_PATH' ) . 'face",c.face_path),CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'USERDEFAULTFACE_UPFILE_PATH' ) . '",c.face_path)) AS user_picture_url,FROM_UNIXTIME(a.create_time,"%d/%m") as create_time,FROM_UNIXTIME(a.create_time,"%w") as week_time' )
			->table( 'review_fields a' )
			->join( 'review_rate b ON a.id=b.review_id' )
			->join( 'user_fields c ON a.user_id=c.id' )
			->where( array( 'a.shop_id' => $shop_id, 'a.is_audit' => '1' ) )
			->select();
		foreach ( $shopInfo[ 'reviews' ] as $key => $val )
		{
			$shopInfo[ 'reviews' ][ $key ][ 'week_time' ] = week2ch( $shopInfo[ 'reviews' ][ $key ][ 'week_time' ] );
			$shopInfo[ 'reviews' ][ $key ][ 'description' ] = preg_replace( "/<br(.*?)\/+?>/", "\n", $shopInfo[ 'reviews' ][ $key ][ 'description' ] );
			$shopInfo[ 'reviews' ][ $key ][ 'description' ] = strip_tags( $shopInfo[ 'reviews' ][ $key ][ 'description' ] );
			$shopInfo[ 'reviews' ][ $key ][ 'create_time' ].="(" . $shopInfo[ 'reviews' ][ $key ][ 'week_time' ] . ")";
			unset( $shopInfo[ 'reviews' ][ $key ][ 'week_time' ] );
		}

		$pdf = M( 'ShopPdf' )->where( array( 'shop_id' => $shop_id ) )->find();
		$shopInfo[ 'pdf_description' ] = $pdf ? $pdf[ 'description' ] : '';
		$shopInfo[ 'pdf_url' ] = $pdf ? C( 'OUTSTREET_DIR' ) . '/ajax/downloadpdf/shop_id/' . $shop_id . '/id/' . $pdf[ 'id' ] : '';

		$shopInfo[ 'eng_keyword' ] = $shopInfo[ 'keyword' ];
		$shopInfo[ 'description' ] = (string) getShopDetail( $shopInfo, 1 );
		$shopInfo[ 'website' ] = getShopDetail( $shopInfo, 2 );
		$shopInfo[ 'email' ] = getShopDetail( $shopInfo, 3 );

		unset( $shopInfo[ 'id' ] );
		unset( $shopInfo[ 'parent_shop_id' ] );
		unset( $shopInfo[ 'create_time' ] );
		unset( $shopInfo[ 'update_time' ] );
		unset( $shopInfo[ 'owner_id' ] );
		unset( $shopInfo[ 'work_time' ] );
		unset( $shopInfo[ 'rooms' ] );
		unset( $shopInfo[ 'eng_address' ] );
		unset( $shopInfo[ 'lower_consume' ] );
		unset( $shopInfo[ 'upper_consume' ] );
		//$shopInfo['eng_parking'] = $shopInfo['parking'];
		unset( $shopInfo[ 'eng_parking' ] );
		unset( $shopInfo[ 'fax_no' ] );
		unset( $shopInfo[ 'email' ] );
		//unset($shopInfo['keyword']);
		unset( $shopInfo[ 'picstamp' ] );
		unset( $shopInfo[ 'is_audit' ] );
		unset( $shopInfo[ 'hit_rate' ] );
		unset( $shopInfo[ 'prev_address' ] );
		unset( $shopInfo[ 'prev_rooms' ] );
		unset( $shopInfo[ 'prev_googlemap_lat' ] );
		unset( $shopInfo[ 'prev_googlemap_lng' ] );
		unset( $shopInfo[ 'shop_status' ] );
		unset( $shopInfo[ 'shop_level' ] );
		unset( $shopInfo[ 'user_id' ] );
		unset( $shopInfo[ 'eng_name' ] );
		unset( $shopInfo[ 'ok' ] );
		$shopInfo[ 'is_mall' ] = D( 'ShopFields' )->where( 'parent_shop_id=' . $shop_id )->count() ? 1 : 0;
		$shopInfo[ 'is_belong_to_mall' ] = $shopInfo[ 'mall_id' ] ? 1 : 0;
		$shopInfo[ 'is_chain_store' ] = $shopInfo[ 'brand_id' ] ? 1 : 0;
		$shopInfo[ 'is_having_event' ] = D( 'EventFields' )->where( 'shop_id=' . $shop_id )->count() ? 1 : 0;
		$shopInfo[ 'is_having_promotion' ] = D( 'PromotionShopMap' )
				->table( 'promotion_shop_map a' )
				->join( 'promotion_fields b ON a.promotion_id=b.id' )
				->where( array( 'a.shop_id' => $shop_id, 'b.is_audit' => 1, 'b.is_ended' => 0 ) )
				->count() ? 1 : 0;
		$shopnewsDao = D( 'ShopNews' );
		$where = array();
		$where[ 'shop_id' ] = $shop_id;
		$where[ 'is_audit' ] = 1;
		$shopnewsList = $shopnewsDao
				->field( 'shop_news.id,title,UNIX_TIMESTAMP(time_start) as time_start,UNIX_TIMESTAMP(time_end) as time_end' )
				->join( 'shop_news_map ON news_id=shop_news.id' )
				->where( $where )->order( 'time_start desc' )->limit( 6 )->select();

		//商戶消息--節目
		/* $where = array();
		  $where["shop_id"]=$shop_id;
		  $where["is_audit"]=1;

		  $shopEventList =  D('EventFields')->field('id,start_time,end_time,name as title')->where($where)->order('start_time desc')->limit(6)->select(); */

		//related promotion
		/* $relatedpromotion = M('PromotionFields')->field('promotion_fields.id,promotion_fields.name as title,WEEKDAY(promotion_fields.time_start) AS time_start_weekday ,WEEKDAY(promotion_fields.time_end) AS time_end_weekday, IF(promotion_fields.time_start,UNIX_TIMESTAMP(promotion_fields.time_start),promotion_fields.create_time) AS sorter,UNIX_TIMESTAMP(promotion_fields.time_start) as time_start,UNIX_TIMESTAMP(promotion_fields.time_end) as time_end,promotion_fields.time_remarks,promotion_fields.is_ended,promotion_fields.type1')
		  ->join('JOIN promotion_shop_map ON promotion_fields.id=promotion_shop_map.promotion_id')
		  ->where(array('promotion_shop_map.shop_id'=>$shop_id,'promotion_fields.is_audit'=>1))->group('promotion_fields.id')->limit(6)->order('sorter desc')->select(); */
		//combine event & promotion
		$combineList = array();
		$c = 0;
		foreach ( $shopnewsList as $key => $news )
		{
			$combineList[ $news[ 'time_start' ] + $c ] = array_merge( array( 'type' => '0' ), $news );
			$c++;
		}
		/* foreach($shopEventList as $event){
		  $combineList[$event['start_time']+$c]= array_merge(array('type'=>'1'),$event);
		  $c++;
		  } */
		/* foreach($relatedpromotion as $promotion){
		  $combineList[$promotion['sorter']+$c] = array_merge(array('type'=>'2'),$promotion);
		  $c++;
		  } */
		krsort( $combineList ); //sort by key desc
		$count = 0;

		foreach ( $combineList as $key => $obj )
		{
			$count++;
			if ( $count > 6 )
			{
				unset( $combineList[ $key ] );
			}
			else
			{
				$combineList[ $key ][ 'title' ] = formatInfoUTF8( $combineList[ $key ][ 'title' ], 40 );
				switch ( $combineList[ $key ][ 'type' ] )
				{
					case 0:
						$combineList[ $key ][ 'type_name' ] = "消息";
						if ( $combineList[ $key ][ 'time_end' ] )
						{
							$combineList[ $key ][ 'create_time' ] = date( chDate( 3 ), $combineList[ $key ][ 'time_start' ] ) . "(" . week2ch( date( 'w', $combineList[ $key ][ 'time_start' ] ) ) . ") - " . date( chDate( 3 ), $combineList[ $key ][ 'time_end' ] ) . "(" . week2ch( date( 'w', $combineList[ $key ][ 'time_end' ] ) ) . ")";
						}
						else
						{
							$combineList[ $key ][ 'create_time' ] = "已完結";
						}
						$combineList[ $key ][ 'url' ] = C( 'OUTSTREET_DIR' ) . "/shop/viewshopnews/id/" . $combineList[ $key ][ 'id' ];
						break;
					case 1:
						$combineList[ $key ][ 'type_name' ] = "節目";
						if ( $combineList[ $key ][ 'start_time' ] > time() )
						{
							$combineList[ $key ][ 'create_time' ] = date( chDate( 3 ), $combineList[ $key ][ 'start_time' ] ) . "(" . week2ch( date( 'w', $combineList[ $key ][ 'start_time' ] ) ) . ") - " . date( chDate( 3 ), $combineList[ $key ][ 'end_time' ] ) . "(" . week2ch( date( 'w', $combineList[ $key ][ 'end_time' ] ) ) . ")";
						}
						else
						{
							$combineList[ $key ][ 'create_time' ] = "已完結";
						}
						$combineList[ $key ][ 'url' ] = C( 'OUTSTREET_DIR' ) . "/event/view/id/" . $combineList[ $key ][ 'id' ];
						break;
					case 2:
						$combineList[ $key ][ 'type_name' ] = rtnPromotionType( $combineList[ $key ][ 'type1' ] );
						$weekdays = array( '一', '二', '三', '四', '五', '六', '日' );
						if ( $combineList[ $key ][ 'is_ended' ] || ($combineList[ $key ][ 'time_end' ] && (strtotime( date( 'Y-m-d' ) ) > $combineList[ $key ][ 'time_end' ])) )
						{
							$combineList[ $key ][ 'create_time' ] = "已完結";
						}
						elseif ( $combineList[ $key ][ 'time_start' ] && $combineList[ $key ][ 'time_end' ] )
						{
							$combineList[ $key ][ 'create_time' ] = date( chDate( 3 ), $combineList[ $key ][ 'time_start' ] ) . "(" . week2ch( date( 'w', $combineList[ $key ][ 'time_start' ] ) ) . ") 至 " . date( chDate( 3 ), $combineList[ $key ][ 'time_end' ] ) . "(" . week2ch( date( 'w', $combineList[ $key ][ 'time_end' ] ) ) . ")";
						}
						elseif ( $combineList[ $key ][ 'time_start' ] )
						{
							$combineList[ $key ][ 'create_time' ] = date( chDate( 3 ), $combineList[ $key ][ 'time_start' ] ) . $weekdays[ $combineList[ $key ][ 'time_start_weekday' ] ] . "起";
						}
						elseif ( $combineList[ $key ][ 'time_end' ] )
						{
							$combineList[ $key ][ 'create_time' ] = "即日起 至" . date( chDate( 3 ), $combineList[ $key ][ 'time_end' ] ) . $weekdays[ $combineList[ $key ][ 'time_end_weekday' ] ];
						}
						if ( !($combineList[ $key ][ 'is_ended' ] || ($combineList[ $key ][ 'time_end' ] && strtotime( date( 'Y-m-d' ) ) > $combineList[ $key ][ 'time_end' ])) )
						{
							$combineList[ $key ][ 'create_time' ].=formatInfoUTF8( $combineList[ $key ][ 'time_remarks' ], 20 );
						}
						$combineList[ $key ][ 'url' ] = C( 'OUTSTREET_DIR' ) . "/promotion/view/id/" . $combineList[ $key ][ 'id' ];
						break;
				}
				unset( $combineList[ $key ][ 'time_start_weekday' ] );
				unset( $combineList[ $key ][ 'time_end_weekday' ] );
				unset( $combineList[ $key ][ 'is_ended' ] );
				unset( $combineList[ $key ][ 'time_remarks' ] );
				unset( $combineList[ $key ][ 'start_time' ] );
				unset( $combineList[ $key ][ 'end_time' ] );
				unset( $combineList[ $key ][ 'time_start' ] );
				unset( $combineList[ $key ][ 'time_end' ] );
				unset( $combineList[ $key ][ 'sorter' ] );
				unset( $combineList[ $key ][ 'type1' ] );
				unset( $combineList[ $key ][ 'type' ] );
				unset( $combineList[ $key ][ 'type_name' ] );
			}
		}


		/*		 * ******************************************** Start Array Management ******************************************** */
		$shopInfo[ 'pictures' ] = rtn_array_string( $shopInfo[ 'pictures' ] );
		$shopInfo[ 'reviews' ] = rtn_array_string( $shopInfo[ 'reviews' ] );
		$shopInfo[ 'all_news' ] = rtn_array_string( array_values( $combineList ) );
		/*		 * ******************************************** End Array Management ********************************************** */

		$json = utf8_encode_array( array( 'shopInfo' => $shopInfo ) );
		$this->mydisplay( $json );
	}

	public function email()
	{
		$shop_id = $_REQUEST[ 'shop_id' ];
		$shopInfo = D( 'ShopFields' )
			->table( 'shop_fields a' )
			->field( 'a.name,a.eng_name,a.shop_status ,b.name as sub_location_name,c.name as main_location_name,a.address,a.parent_shop_id as mall_id,a.telephone,a.description' )
			->join( 'location_sub b on a.sub_location_id = b.id ' )
			->join( 'location_main c on a.main_location_id = c.id' )
			->where( 'a.id=' . $shop_id )
			->find();
		if ( $shopInfo[ 'eng_name' ] != '' && $shopInfo[ 'eng_name' ] != $shopInfo[ 'name' ] )
		{
			$shopInfo[ 'name' ].=' ' . $shopInfo[ 'eng_name' ];
		}
		if ( $shopInfo[ 'shop_status' ] != 0 )
		{
			switch ( $shopInfo[ 'shop_status' ] )
			{
				case 1:
					$shopInfo[ 'name' ].=' (已搬址)';
					break;
				case 2:
					$shopInfo[ 'name' ].=' (已結業)';
					break;
				case 3:
					$shopInfo[ 'name' ].=' (裝修中)';
					break;
			}
		}
		$getCates = D( 'ShopCateMap' )->where( 'shop_id=' . $shop_id )->select();
		foreach ( $getCates as $key => $val )
		{
			if ( $shopInfo[ 'category_names' ] )
			{
				$shopInfo[ 'category_names' ].=',';
			}
			$shopInfo[ 'category_names' ].=D( 'ShopCate' )->where( array( 'type' => array( 'neq', 'website' ), 'id' => $val[ 'cate_id' ] ) )->getField( 'name' );
		}
		if ( $shopInfo[ 'mall_id' ] != 0 )
		{
			$shopInfo[ 'address' ] = D( 'ShopFields' )->where( 'id=' . $shopInfo[ 'mall_id' ] )->getField( 'name' ) . " " . $shopInfo[ 'address' ];
		}
		$shopInfo[ 'link' ] = C( 'OUTSTREET_DIR' ) . '/shop/view/id/' . $shop_id;
		$shopInfo[ 'address' ] = $shopInfo[ 'main_location_name' ] . " " . $shopInfo[ 'sub_location_name' ] . " " . $shopInfo[ 'address' ];
		$content = "";
		$content .= "\n";
		$content .= "商戶名稱 : " . $shopInfo[ 'name' ] . "\n";
		$content .= "商戶地區 : " . $shopInfo[ 'sub_location_name' ] . "\n";
		$content .= "商戶類別 : " . $shopInfo[ 'category_names' ] . "\n";
		$content .= "商戶地址 : " . $shopInfo[ 'address' ] . "\n";
		if ( $shopInfo[ 'telephone' ] )
		{
			$content .= "商戶電話 : " . formatTelephone( $shopInfo[ 'telephone' ], 1, $startSalt, $endSalt, true ) . "\n";
		}
		if ( $shopInfo[ 'description' ] )
		{
			$content .= "商戶介紹 : " . $shopInfo[ 'description' ] . "\n";
		}
		$content .= "商戶網址 : " . $shopInfo[ 'link' ];
		$title = $shopInfo[ 'name' ] . " (" . $shopInfo[ 'sub_location_name' ] . ")";
		$json = utf8_encode_array( array( 'title' => $title, 'content' => $content ) );
		$this->mydisplay( $json );
	}

	public function mall_list()
	{
		$mall_id = (int) $_REQUEST[ 'mall_id' ];
		$page = (int) $_REQUEST[ 'page' ];
		$mall_name = D( 'ShopFields' )->where( 'id=' . $mall_id )->getField( 'name' );
		$result_per_page = C( 'RESULT_PER_PAGE_NOMAP' );
		$where = array();
		$where[ 'is_audit' ] = 1;
		$where[ 'parent_shop_id' ] = $mall_id;
		$count = D( 'ShopFields' )->where( $where )->count( 'distinct id' );
		$total_page = ceil( $count / $result_per_page );
		if ( $page == '' )
		{
			$page = 1;
		}
		$total_number = $count;
		$result = D( 'ShopFields' )->field( 'a.id as shop_id,if(a.name!=a.eng_name,CONCAT(a.name," ",a.eng_name),a.name) as name,a.rooms as address_rooms' )
			->table( 'shop_fields a' )
			->where( $where )
			->limit( $result_per_page )->page( $page )
			->order( 'if(a.eng_name,a.eng_name,name) asc' )
			->select();
		foreach ( $result as $key => $val )
		{
			$result[ $key ][ 'cates' ] = D( 'ShopCateMap' )
				->field( 'b.name' )
				->table( 'shop_cate_map a' )
				->join( 'shop_cate b on b.id=a.cate_id' )
				->where( array( 'b.type' => array( 'neq', 'website' ), 'shop_id' => $val[ 'shop_id' ] ) )
				->select();
			foreach ( $result[ $key ][ 'cates' ] as $key2 => $val2 )
			{
				if ( $result[ $key ][ 'category_names' ] )
				{
					$result[ $key ][ 'category_names' ].=',';
				}
				$result[ $key ][ 'category_names' ].=$val2[ 'name' ];
			}
			unset( $result[ $key ][ 'cates' ] );
		}

		/*		 * ******************************************** Start Array Management ******************************************** */
		$result = rtn_array_string( $result );
		/*		 * ******************************************** End Array Management ********************************************** */

		$json = utf8_encode_array( array( 'mall_id' => $mall_id, 'mall_name' => $mall_name, 'total_number' => $total_number, 'total_page' => $total_page, 'result' => $result ) );
		$this->mydisplay( $json );
	}

	public function shop_category()
	{
		$level = (int) $_REQUEST[ 'level' ];
		$parent_category_id = (int) $_REQUEST[ 'parent_category_id' ];
		if ( $level )
		{
			$where[ 'level' ] = $level;
		}
		if ( $parent_category_id )
		{
			$where[ 'parent_id' ] = $parent_category_id;
		}
		$result = D( 'ShopCate' )
			->field( 'if(reference_id,reference_id,id) as id,name,parent_id as parent_category_id,level' )
			->where( $where )
			->rows( 'id' );

		if ( !$parent_category_id )
		{
			$orders = $index_orders = explode( ',', C( 'SHOP_CATE_ORDER' ) );

			foreach ( $index_orders AS $index_order )
			{
				$orders = array_merge( $orders, explode( ',', C( 'SHOP_CATE_ORDER_' . $index_order ) ) );
			}
		}
		elseif ( C( 'SHOP_CATE_ORDER_' . $parent_category_id ) )
		{
			$orders = explode( ',', C( 'SHOP_CATE_ORDER_' . $parent_category_id ) );
		}

		$categories = array();
		foreach ( $orders as $key => $val )
		{
			if ( $result[ $val ] )
			{
				$categories[] = $result[ $val ];
				unset( $result[ $val ] );
			}
		}

		$categories = rtn_array_string( array_merge( $categories, array_values( $result ) ) );
		$json = (array( 'category_count' => count( $categories ), 'category' => $categories ));
		$this->mydisplay( $json );
	}

	public function brand_list()
	{
		$brand_id = (int) $_REQUEST[ 'brand_id' ];
		$page = (int) $_REQUEST[ 'page' ];
		$brand_name = D( 'ShopBrand' )->where( 'id=' . $brand_id )->getField( 'name' );
		$result_per_page = C( 'RESULT_PER_PAGE_NOMAP' );
		$where = array();
		$where[ 'is_audit' ] = 1;
		$where[ 'brand_id' ] = $brand_id;
		$count = D( 'ShopFields' )->where( $where )->count( 'distinct id' );
		$total_page = ceil( $count / $result_per_page );
		if ( $page == '' )
		{
			$page = 1;
		}
		$total_number = $count;
		$getParentShopAddress = "if(a.parent_shop_id > 0,(SELECT CONCAT(name,' ')as name from shop_fields where id=a.parent_shop_id),'')";
		$getMainSubLocation = "(SELECT name from location_sub where id=a.sub_location_id)";

		$result = D( 'ShopFields' )->field( 'a.id as shop_id,if(a.name!=a.eng_name,CONCAT(a.name," ",a.eng_name),a.name) as name,CONCAT(' . $getMainSubLocation . '," ",a.address," ",' . $getParentShopAddress . ', a.rooms) AS address,telephone,sub_location_id,(SELECT name from location_sub where id=a.sub_location_id) as sub_location_name' )
			->table( 'shop_fields a' )
			->where( $where )
			->limit( $result_per_page )->page( $page )
			->select();
		foreach ( $result as $key => $val )
		{
			$result[ $key ][ 'telephone' ] = formatTelephone( $result[ $key ][ 'telephone' ], 1, $startSalt, $endSalt, true );
		}

		/*		 * ******************************************** Start Array Management ******************************************** */
		$result = rtn_array_string( $result );
		/*		 * ******************************************** End Array Management ********************************************** */

		$json = utf8_encode_array( array( 'brand_id' => $brand_id, 'brand_name' => $brand_name, 'total_number' => $total_number, 'total_page' => $total_page, 'result' => $result ) );
		$this->mydisplay( $json );
	}

	public function facebook()
	{
		$shop_id = (int) $_REQUEST[ 'shop_id' ];
		$title = '想同你分享「%1$s」呢個商戶，去OutStreet睇下啦！';

		$shopInfo = D( 'ShopFields' )->where( array( 'id' => $shop_id ) )->find();
		$info = array();
		$info[ 'title' ] = sprintf( $title, $shopInfo[ 'name' ] );
		$info[ 'link' ] = C( 'OUTSTREET_DIR' ) . '/shop/view/id/' . $shop_id;
		$info[ 'description' ] = $shopInfo[ 'description' ];

		$json = utf8_encode_array( $info );
		$this->mydisplay( $json );
	}

	private function getsubcateids( $category_ids )
	{
		$shopCateDao = D( 'ShopCate' );
		$search_cate_ids = $category_ids;
		$loopids = array();
		$loopids = $category_ids;
		$loop = true;

		while ( $loop )
		{
			$shopCateList = $shopCateDao->where( array( 'type' => array( 'neq', 'website' ), 'parent_id' => array( 'in', $loopids ) ) )->select();
			$cate = array();
			foreach ( $shopCateList AS $val )
			{
				$cate[] = $val[ 'id' ];
				$search_cate_ids[] = $val[ 'reference_id' ] ? $val[ 'reference_id' ] : $val[ 'id' ];
			}
			if ( count( $cate ) )
			{
				$loopids = $cate;
			}
			else
			{
				$loop = false;
			}
		}
		return $search_cate_ids;
	}

	private function getsublocationids( $mainlocation_id )
	{
		$locationsubDao = D( 'LocationSub' );
		return $locationsubDao->where( array( 'main_location_id' => $mainlocation_id ) )->rows( null, 'id' );
	}

}

?>