<?php

include_once './config.inc.php';
global $conn;

$mktime = mktime( 0, 0, 0, date('m')-1, 1, date('Y') );
$table_name = 'app_log_'.date( 'Ym', $mktime );

if ( !$conn->query('SHOW TABLES FROM app_log LIKE "'.$table_name.'"') )
{
	$conn->query('CREATE TABLE IF NOT EXISTS app_log.'.$table_name.' LIKE app_log.impression');
	$conn->query('
		INSERT INTO app_log.'.$table_name.' ( ip_address, user_agent, create_time )
			SELECT ip_address, user_agent, create_time FROM app_log.impression
				WHERE UNIX_TIMESTAMP(create_time) >= UNIX_TIMESTAMP("'.date( 'Y-m-d', $mktime ).'")
					AND UNIX_TIMESTAMP(create_time) < UNIX_TIMESTAMP("'.date( 'Y-m-d', $mktime ).'" + INTERVAL 1 MONTH)
	');
	
	$conn->query('
		DELETE FROM app_log.impression
			WHERE UNIX_TIMESTAMP(create_time) >= UNIX_TIMESTAMP("'.date( 'Y-m-d', $mktime ).'")
				AND UNIX_TIMESTAMP(create_time) < UNIX_TIMESTAMP("'.date( 'Y-m-d', $mktime ).'" + INTERVAL 1 MONTH)
	');

	$conn->query('
		INSERT INTO app_log.impression_hourly ( datetime, count )
			SELECT DATE_FORMAT(create_time,"%Y-%m-%d %H:00:00") as `datetime`, COUNT(ip_address) as `count` FROM app_log.'.$table_name.'
				GROUP BY `datetime`
	');

	$conn->query('
		INSERT INTO app_log.impression_daily ( datetime, count )
			SELECT DATE_FORMAT(create_time,"%Y-%m-%d 00:00:00") as `datetime`, COUNT(DISTINCT CONCAT(DATE_FORMAT(create_time,"%Y-%m-%d"), ",",`ip_address`)) as `count` FROM app_log.'.$table_name.'
				GROUP BY `datetime`
	');

	$conn->query('
		INSERT INTO app_log.impression_monthly ( datetime, count )
			SELECT DATE_FORMAT(create_time,"%Y-%m-01 00:00:00") as `datetime`, COUNT(DISTINCT CONCAT(DATE_FORMAT(create_time,"%Y-%m-01"), ",",`ip_address`)) as `count` FROM app_log.'.$table_name.'
				GROUP BY `datetime`
	');
}

echo 'finish';

?>