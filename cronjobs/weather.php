#!/usr/local/bin/php.cli 

<?php

include_once './config.inc.php';

global $select_db, $insert_db, $update_db, $delete_db;


		$weathers = array();
		$current_year = date('Y');
		$current_month = date('m');
		$current_day = date('d');

		$html = file_get_contents( 'http://www.hko.gov.hk/wxinfo/currwx/fndc.htm' );
		$html = strip_tags( $html, '<td><img>' );


		if ( $total = preg_match_all( '/<td\s*headers="'.iconv('UTF-8','big5','日期').'"\s*width="12%">(\d+)\/(\d+)\((.*?)\)<\/td>/is', $html, $m ) )
		{
			for ( $i=0; $i<$total; $i++ )
			{
				if ($m[2][$i] == 1 && $current_month == 12) { $year = $current_year+1; }
				else { $year = $current_year; }
				$weathers[$i]['date'] = $year.'-'.$m[2][$i].'-'.$m[1][$i];
				$weathers[$i]['weekday'] = iconv('big5','UTF-8',$m[3][$i]);
			}
		}
		if ( $total = preg_match_all( '/<td\s*width="12%"\s*align="center"\s*height="60"><img \s*border="0"\s*src="(.*?)"\s*alt="/is', $html, $m ) )
		{
			for ( $i=0; $i<$total; $i++ )
			{
				$weathers[$i]['weather_icon'] = basename($m[1][$i]);
			}
		}
		if ( $total = preg_match_all( '/<td\s*headers="'.iconv('UTF-8','big5','天氣').'"\s*width="12%"\s*valign="top">(.*?)<\/td>/is', $html, $m ) )
		{
			for ( $i=0; $i<$total; $i++ )
			{
				$weathers[$i]['weather_description'] = str_replace(' ', '', iconv('big5','UTF-8',$m[1][$i]));
			}
		}
		if ( $total = preg_match_all( '/<td\s*headers="'.iconv('UTF-8','big5','風力').'"\s*width="12%"\s*valign="top">(.*?)<\/td>/is', $html, $m ) )
		{
			for ( $i=0; $i<$total; $i++ )
			{
				$weathers[$i]['wind_description'] = str_replace(' ', '', iconv('big5','UTF-8',$m[1][$i]));
			}
		}
		if ( $total = preg_match_all( '/<td\s*headers="'.iconv('UTF-8','big5','溫度').'"\s*width="12%"\s*align="center">(.*?)<\/td>/is', $html, $m ) )
		{
			for ( $i=0; $i<$total; $i++ )
			{
				$weathers[$i]['temp_description'] = str_replace(' ', '', iconv('big5','UTF-8',$m[1][$i]));
			}
		}
		if ( $total = preg_match_all( '/<td\s*headers="'.iconv('UTF-8','big5','相對濕度').'"\s*width="12%"\s*align="center">(.*?)<\/td>/is', $html, $m ) )
		{
			for ( $i=0; $i<$total; $i++ )
			{
				$weathers[$i]['rh_range'] = str_replace(' ', '', iconv('big5','UTF-8',$m[1][$i]));
			}
		}
		//print_r( $weathers );
		foreach($weathers AS $weather)
		{
			$select_db->query(
				'INSERT INTO weather_fields'.
				' (date,weekday,weather_icon,weather_description,wind_description,temp_description,rh_range) VALUES'.
				' ("'.$weather['date'].'","'.$weather['weekday'].'","'.$weather['weather_icon'].'","'.$weather['weather_description'].'","'.$weather['wind_description'].'","'
					.$weather['temp_description'].'","'.$weather['rh_range'].'")'.
				' ON DUPLICATE KEY UPDATE weather_icon="'.$weather['weather_icon'].'",weather_description="'.$weather['weather_description'].'",'.
					'wind_description="'.$weather['wind_description'].'",temp_description="'.$weather['temp_description'].'",'.
					'rh_range="'.$weather['rh_range'].'"' );
		}

		echo 'finish';

?>