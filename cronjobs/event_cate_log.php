<?php

include_once './config.inc.php';
global $conn;

$mktime = mktime( 0, 0, 0, date('m')-1, 1, date('Y') );
$table_name = 'event_cate_log_'.date( 'Ym', $mktime );

if ( !$conn->query('SHOW TABLES FROM outstreet_log LIKE "'.$table_name.'"') )
{
	$conn->query('CREATE TABLE IF NOT EXISTS outstreet_log.'.$table_name.' LIKE outstreet_main.event_cate_log');
	$conn->query('
		INSERT INTO outstreet_log.'.$table_name.' ( ip_address, user_agent, create_time )
			SELECT ip_address, user_agent, create_time FROM outstreet_main.event_cate_log
				WHERE UNIX_TIMESTAMP(create_time) >= UNIX_TIMESTAMP("'.date( 'Y-m-d', $mktime ).'")
					AND UNIX_TIMESTAMP(create_time) < UNIX_TIMESTAMP("'.date( 'Y-m-d', $mktime ).'" + INTERVAL 1 MONTH)
	');

	$conn->query('
		DELETE FROM outstreet_main.event_cate_log
			WHERE UNIX_TIMESTAMP(create_time) >= UNIX_TIMESTAMP("'.date( 'Y-m-d', $mktime ).'")
				AND UNIX_TIMESTAMP(create_time) < UNIX_TIMESTAMP("'.date( 'Y-m-d', $mktime ).'" + INTERVAL 1 MONTH)
	');
}

echo 'finish';

?>