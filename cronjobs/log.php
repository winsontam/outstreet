#!/usr/local/bin/php.cli 

<?php
include_once './config.inc.php';
global $conn;
$NUMOFDAYS = 5;	//Number of days to be handled
$date_r = array();
for($i=1;$i<=$NUMOFDAYS;$i++){
$date_r[] = array('date'=>date("Y-m-d", time()-(86400*$i)),'table_name'=>'log_'.date('ymd',time()-(86400*$i)));
}
foreach($date_r as $item){
	$conn->query('CREATE TABLE IF NOT EXISTS `outstreet_log`.`'.$item['table_name'].'` like `outstreet_main`.`log_fields`');
	$affectedRow = $conn->query('REPLACE INTO `outstreet_log`.`'.$item['table_name'].'` (SELECT * FROM `outstreet_main`.`log_fields` where create_time between UNIX_TIMESTAMP("'.$item['date'].' 00:00:00") and UNIX_TIMESTAMP(concat("'.$item['date'].' 23:59:59")));');
	if($affectedRow>0){
		$conn->query('DELETE FROM `outstreet_main`.`log_fields` where create_time between UNIX_TIMESTAMP("'.$item['date'].' 00:00:00") and UNIX_TIMESTAMP(concat("'.$item['date'].' 23:59:59"));');	
	}
	$conn->query('
		INSERT INTO outstreet_log.log_summary ( `date`, `page_view`, `visit`, `redirect` )
			SELECT
				(select DATE_FORMAT(FROM_UNIXTIME(create_time),"%Y-%m-%d") from `outstreet_log`.`'.$item['table_name'].'` limit 1),
				(select count(*) from `outstreet_log`.`'.$item['table_name'].'`),
				(select count(distinct(ip)) from `outstreet_log`.`'.$item['table_name'].'`),
				(select count(*) from `outstreet_log`.`'.$item['table_name'].'` where redirect =1)
			ON DUPLICATE KEY UPDATE
				`page_view` = (select count(*) from `outstreet_log`.`'.$item['table_name'].'`),
				`visit` = (select count(distinct(ip)) from `outstreet_log`.`'.$item['table_name'].'`),
				`redirect` = (select count(*) from `outstreet_log`.`'.$item['table_name'].'` where redirect =1)

	');
}
echo 'finish';
?>