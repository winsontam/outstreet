<?php
import("Think.Core.Model.RelationModel");
class OfferModel extends RelationModel{
	protected $tableName = 'offer_fields';

	protected $_link = array(
		'cates'=>array(
			'mapping_type'=>MANY_TO_MANY,
			'class_name'=>'shop_cate',
			'mapping_name'=>'cates',
			'foreign_key'=>'offer_id',
			'relation_foreign_key'=>'cate_id',
			'relation_table'=>'offer_cate_map',
		),
		'shops'=>array(
			'mapping_type'=>MANY_TO_MANY,
			'class_name'=>'shop_fields',
			'mapping_name'=>'shops',
			'foreign_key'=>'offer_id',
			'relation_foreign_key'=>'shop_id',
			'relation_table'=>'offer_shop_map',
		)
	);

	protected $_auto = array(
		array('create_time','time',self::MODEL_INSERT,'function'),
	);
	 
}

?>