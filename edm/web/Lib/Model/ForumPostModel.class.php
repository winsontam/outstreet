<?php
class ForumPostModel extends Model{
	protected $tableName = 'forum_post';

	protected $_auto = array(
	array('create_time','time',self::MODEL_INSERT,'function'),
	);

	protected $fields = array(
		'id',
		'create_time',
		'update_time',
		'thread_id',
		'user_id',
		'user_name',
		'summary',
		'body',
	);
}
?>