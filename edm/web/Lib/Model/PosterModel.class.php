<?php
import("Think.Core.Model.RelationModel");
class PosterModel extends RelationModel{
	protected $tableName = 'poster_fields';
	
	
	protected $_link = array(
		'pictures'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'poster_picture',
			'foreign_key'=>'poster_id',
			'mapping_name'=>'pictures',
			'mapping_order'=>'id asc'
		)
	);

}


?>