<?php
import("Think.Core.Model.RelationModel");
class ReviewModel extends RelationModel{
	protected $tableName = 'review_fields';

	protected $_auto = array(
		array('create_time','time',self::MODEL_INSERT,'function'),
	);

	protected $_link = array(
		'shop'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'shop_fields',
			'foreign_key'=>'shop_id',
		),
		'user'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'user_fields',
			'foreign_key'=>'user_id',
		),
		'userstatistics'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'statistics_user',
			'foreign_key'=>'user_id',
		),
		'commends'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'review_commend',
			'foreign_key'=>'review_id',
		),
		'pictures'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'review_pictures',
			'foreign_key'=>'review_id',
			'mapping_name'=>'pictures',
		),
		'rate'=>array(
			'mapping_type'=>HAS_ONE,
			'class_name'=>'review_rate',
			'foreign_key'=>'review_id',
			'mapping_name'=>'rate',
		),
	);
}
?>