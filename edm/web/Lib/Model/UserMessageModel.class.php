<?php
import("Think.Core.Model.RelationModel");
class UserMessageModel extends RelationModel{
	protected $tableName = 'user_message';

	protected $_auto = array(
		array('create_time','time',self::MODEL_INSERT,'function'),
	);
	
	protected $_link = array(
		'user'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'user_fields',
			'foreign_key'=>'user_id',
		),
		'submit_user'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'user_fields',
			'foreign_key'=>'submit_user_id',
		)
	);

	protected $fields = array(
		'id',  
		'create_time',    
		'user_id',
		'submit_user_id',
		'submit_user_name',
		'body',
		'reply',
		'reply_time',
		'is_audit',
		'_pk'=>'id',
		'_autoinc'=>true
	);

}
?>