<?php
import("Think.Core.Model.RelationModel");
class EventModel extends RelationModel{
	protected $tableName = 'event_fields';

	protected $_link = array(
		'pictures'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'event_picture',
			'foreign_key'=>'event_id',
			'mapping_name'=>'pictures',
			'mapping_order'=>'create_time desc'
			),
		'keywords'=>array(
			'mapping_type'=>MANY_TO_MANY,
			'class_name'=>'event_keyword',
			'mapping_name'=>'keywords',
			'foreign_key'=>'event_id',
			'relation_foreign_key'=>'keyword_id',
			'relation_table'=>'event_keyword_map',
			),
		'statistics'=>array(
			'mapping_type'=>HAS_ONE,
			'class_name'=>'statistics_event',
			'mapping_name'=>'statistics',
			'foreign_key'=>'event_id',
			'as_fields'=>'review_count',
			),
		'shop'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'shop_fields',
			'foreign_key'=>'shop_id',
			'mapping_name'=>'shop',
			'mapping_order'=>'id desc',
			'mapping_fields'=>'name,english_name,googlemap_lat,googlemap_lng',
		),
		'sub_location'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'location_sub',
			'mapping_name'=>'sub_location',
			'foreign_key'=>'sub_location_id',
		),
		'main_location'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'location_main',
			'mapping_name'=>'main_location',
			'foreign_key'=>'main_location_id',
		),
		'cate'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'event_cate',
			'mapping_name'=>'cate',
			'foreign_key'=>'cate_id',
		),
	);

	protected $_validate  =  array(
		array('name','require','節目名稱是必須的','add'),
	  	array('address','require','地址必須填寫','add'),
	  	array('details','require','節目介紹必須填寫','add'),
	  	array('telephone','require','電話必須填寫','add'),
	);

	protected $_auto = array(
		array('create_time','time',self::MODEL_INSERT,'function'),
	);
	 
}

?>