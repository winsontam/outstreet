<?php

import("Think.Core.Model.RelationModel");
class PromotionModel extends RelationModel{
	protected $tableName = 'promotion_fields';

	protected $_link = array(
		'pictures'=>array(
			'mapping_type'=>HAS_MANY,
			'class_name'=>'promotion_picture',
			'foreign_key'=>'promotion_id',
			'mapping_name'=>'pictures',
			'mapping_order'=>'create_time desc'
		),
		'cates'=>array(
			'mapping_type'=>MANY_TO_MANY,
			'class_name'=>'shop_cate',
			'mapping_name'=>'cates',
			'foreign_key'=>'promotion_id',
			'relation_foreign_key'=>'cate_id',
			'relation_table'=>'promotion_cate_map',
		),
		'shops'=>array(
			'mapping_type'=>MANY_TO_MANY,
			'class_name'=>'shop_fields',
			'mapping_name'=>'shops',
			'foreign_key'=>'promotion_id',
			'relation_foreign_key'=>'shop_id',
			'relation_table'=>'promotion_shop_map',
		)
	);

	protected $_auto = array(
		array('create_time','time',self::MODEL_INSERT,'function'),
	);
	 
}

?>