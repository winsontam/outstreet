<?php
class BaseAction extends Action{
	
	public function __construct() {
		parent::__construct();

		//include common files
		include COMMON_PATH.'auth.php';		
		if(strtoupper(MODULE_NAME)=='SHOP'){
			include COMMON_PATH.'shopform.php';
		}

		$hotKeywords = M('keyword')->order('id asc')->select();
		$this->assign('hotKeywords', $hotKeywords);
		
		$this->assign('startSalt',getStartSalt());
		$this->assign('endSalt',getEndSalt());
	}

	public function selshopcate(){
		//獲取Shop分類主目錄列表 壓入模板
		$cate = M('ShopCate')->getField('id,name');
		$level_1 = M('ShopCate')->where(array('level'=>1))->getField('id,parent_id');
		$level_2 = M('ShopCate')->where(array('level'=>2))->getField('id,parent_id');
		$level_3 = M('ShopCate')->where(array('level'=>3))->getField('id,parent_id');

		$cateList = array();
		foreach ( $level_1 AS $level_1_id => $level_1_parent_id )
		{
			if ($cate[ $level_1_id ])
				$cateList[ $level_1_id ][ 'name' ] = $cate[ $level_1_id ];
		}
		foreach ( $level_2 AS $level_2_id => $level_2_parent_id )
		{
			if ($cateList[$level_2_parent_id][ 'name' ] && $cate[ $level_2_id ])
				$cateList[$level_2_parent_id]['item'][ $level_2_id ][ 'name' ] = $cate[ $level_2_id ];
		}
		foreach ( $level_3 AS $level_3_id => $level_3_parent_id )
		{
			if ($cateList[$level_2[$level_3_parent_id]][ 'name' ] && $cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['name'] && $cate[ $level_3_id ])
				$cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['item'][ $level_3_id ][ 'name' ] = $cate[ $level_3_id ];
		}
		
		//Shop Cate Ref List
		$shopCateList = D('ShopCate')->field('id,reference_id')->select();
		$refList = array();
		foreach($shopCateList as $key=>$shopCate){
			$cateId = $shopCate['id'];
			$refId = $shopCate['reference_id'];
			if($refId!=0){
				$refList[$cateId][] = $refId;
				$refList[$refId][] = $cateId;
			}
		}
		foreach($refList as $key=>$ref){
			foreach($ref as $cateId){
				foreach($refList[$cateId] as $elem){
					$refList[$key][] = $elem;	
				}
			}
			$refList[$key] = array_unique($refList[$key]);
		}
			
		$this->assign('refList', $refList);
		$this->assign('cateList', $cateList);
		$this->display();
	}

	public function _initialize(){
		import ( '@.ORG.SimpleAcl' );

		vendor("facebook.facebook");
			
		$api_key = '079846a4ec9c666db3c9d0c60c43250c';
		$secret = '2c36f48f09eb33bbcebd11f63e2fd456';
			
		$fb	= new Facebook($api_key,$secret);
		$fb_user = $fb->get_loggedin_user();

		$this->assign('fb_user', $fb_user);

		if (SimpleAcl::checkLogin()) $this->assign('authInfo', SimpleAcl::getLoginInfo());
			
		$this->assign('page', $_REQUEST[C('VAR_PAGE')]);
			
		$searchLogDao = D('SearchLog');
		$searchList = $searchLogDao->query('SELECT create_time,keywords,count(id) as search_count,results FROM __TABLE__ where results!=0 group by keywords order by search_count desc,results desc limit 10');
		$this->assign('searchList',$searchList);
			
		$this->assign('title', 'OutStreet');
		$this->assign('keywords', '香港購物,出街,逛街,美食');
		$this->assign('description', 'OutStreet 出街');
		
		/* 天氣預報*/
		$weatherDao=D('Info');
		$weatherInfo_Public=$weatherDao->field('temperature_day,temperature_night,weather_day,weather_night')->where('id=1 or id=2')->select();
		$weatherNew=array();
		for($i=0;$i<=1;$i++){
			$weatherNew[$i]['temperature_day']=mb_substr($weatherInfo_Public[$i]['temperature_day'],2,4,'utf8');
			$weatherNew[$i]['temperature_night']=mb_substr($weatherInfo_Public[$i]['temperature_night'],2,4,'utf8');
			
			$weatherNew[$i]['weather_day']=$weatherInfo_Public[$i]['weather_day'];
			$weatherNew[$i]['weather_night']=$weatherInfo_Public[$i]['weather_night'];
		}
		$this->assign("weainfo_Public",$weatherNew);
		/*天氣預報  END*/
		
		$_POST = array_map('htmlspecialchars', $_POST);
		$_REQUEST = array_map('htmlspecialchars', $_REQUEST);
		$_GET = array_map('htmlspecialchars', $_GET);
	}

	public function successBox($msg){
		if (!$this->get('waitSecond')) {
			$this->assign('waitSecond',3);
		}

		$this->assign('box', 1);
		$this->success($msg);
	}
	public function errorBox($msg){
		if (!$this->get('waitSecond')) {
			$this->assign('waitSecond',3);
		}

		$this->assign('box', 1);
		$this->error($msg);
	}

	public function sellocation(){
		//獲取Shop分類主目錄列表 壓入模板
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');
		$locationMainList = $locationMainDao->select();
		 
		foreach($locationMainList as &$locationMain){
			$where = array();
			$where['main_location_id'] = $locationMain['id'];
			$locationMain['locationSubList'] = $locationSubDao->where($where)->select();
		}
		 
		$this->assign('locationMainList', $locationMainList);
		$this->display();
	}

	/**
	 * 对字符或者数组加逗号连接, 用来
	 *
	 * @param string/array $arr 可以传入数字或者字串
	 * @return string 这样的格式: '1','2','3'
	 */
	public function implode($arr) {
		return implode(",", (array)$arr);
	}
	
	/********* Type *********/
	/* 2. Frontend */
	/********* Source *********/
	/* 1. Request Shop Owner */
	/* 2. Member Validation */
	/* 3. Member Resend Password */
	public function smtp_mail_log($sendto_mail, $subject="", $body="", $sendto_name="",$type, $source){

		vendor("phpmailer.class#phpmailer");
		$mail = new PHPMailer();
		$mail->IsSMTP(); // set mailer to use SMTP	
		$mail->SMTPAuth = true; // turn on SMTP authentication
		$mail->Host = C('SMTP_SERVER'); // specify main and backup server
		//$mail->SMTPSecure = "ssl";
		$mail->Port = 25;
		$mail->Username = C('SMTP_USERNAME'); // SMTP username
		$mail->Password = C('SMTP_PASSWORD'); // SMTP password
		$mail->From = C('SMTP_FROM');
		$mail->FromName = C('SMTP_FROMNAME');
		$mail->AddAddress($sendto_mail, $sendto_name);
		$mail->CharSet = 'big5';
		$mail->Subject = $subject;
		$mail->Body = $body;
		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
		$mail->WordWrap = 50;
		$mail->MsgHTML($body);
		$mail->IsHTML(true);
		if(!$mail->Send())
		{
			echo "Message could not be sent. <p>";
			echo "Mailer Error: " . $mail->ErrorInfo;
			exit;
		}
		else
		{
			//Start Save Email Log
			$memid = 0;
			$userDao = D('User');
			$where = array();
			$where['email'] = $sendto_mail;
			$userInfo = $userDao->where($where)->find();
			if(!empty($userInfo)){
				$memid = $userInfo['id'];
			}	
			
			//Start Insert to DB
			$emailLogDao = D('EmailLog');
			$emailLogDao->create();
			$data = array();
			$data['datetime'] = time();			
			$data['mem_id'] = $memid;
			$data['email_address'] = $sendto_mail;
			$data['type'] = $type;
			$data['source'] = $source;
			$emailLogDao->add($data);
			//End Insert to DB
				
			//End Save Email Log
		}
	}

}

?>