<?php
class IndexAction extends BaseAction{	
	public function index(){		
		if (!empty($_SERVER['HTTP_CLIENT_IP'])){			
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip=$_SERVER['REMOTE_ADDR'];		
		}
		if(gethostbyname('monitor.obox.com.hk')==$ip || $ip=="127.0.0.1" || preg_match( '/220\.246\.2/', $ip )|| preg_match( '/220\.246/', $ip ) ){		
			$dir=scandir(TMPL_PATH."default/Index");						
			foreach($dir as $key=>$val){
				if(!preg_match ( '/\./' ,$val,$match)){				
					$edmList[]=$val;
				}
			}
			$userDao=D('User');
			$where['acc_edm']=1;
			$where['is_email_verify']=1;
			$userList=$userDao->where($where)->select();
			$this->assign('userList',$userList);
			$this->assign('edmList',$edmList);
			$this->display('Index:index','big5');
		}else{
			header('location: http://www.outstreet.com.hk');
		}
	}
	public function getedm(){
		$name = $_REQUEST['name'];		
		$result =file_get_contents("http://edm.outstreet.com.hk/".TMPL_PATH."default/Index/".$name."/edm.html");		
		echo json_encode( array( 'result'=> '1','data'=>$result) );
	}
	public function test(){
		echo date();
	}
	public function edm(){
		$link=C('OUTSTREET_DIR');
		$this->assign('link',$link);
		$this->display();
	}	
	public function save(){
		$action=$_REQUEST['action_hidden'];		
		$edmDao=D('Edm');
		$edmList=$edmDao->select();
		$id=$_REQUEST['edmid'];
		$data['posterid']=$_REQUEST['posterid'];
		$data['event_id_01']=$_REQUEST['event_id_1'];
		$data['event_id_02']=$_REQUEST['event_id_2'];
		$data['event_id_03']=$_REQUEST['event_id_3'];
		$data['event_id_04']=$_REQUEST['event_id_4'];
		$data['promotion_id_01']=$_REQUEST['promotion_id_1'];
		$data['promotion_id_02']=$_REQUEST['promotion_id_2'];
		$data['promotion_id_03']=$_REQUEST['promotion_id_3'];
		$data['promotion_id_04']=$_REQUEST['promotion_id_4'];
		$data['review_id_01']=$_REQUEST['review_id_1'];
		$data['review_id_02']=$_REQUEST['review_id_2'];
		$data['review_id_03']=$_REQUEST['review_id_3'];
		$data['review_id_01_content']=$_REQUEST['review_id_1_content'];
		$data['review_id_02_content']=$_REQUEST['review_id_2_content'];
		$data['review_id_03_content']=$_REQUEST['review_id_3_content'];
		$data['event_id_01_date']=$_REQUEST['event_id_1_date'];
		$data['event_id_02_date']=$_REQUEST['event_id_2_date'];
		$data['event_id_03_date']=$_REQUEST['event_id_3_date'];
		$data['event_id_04_date']=$_REQUEST['event_id_4_date'];
		$data['caption']=$_REQUEST['caption_hidden'];
		if($action==1){
			if($newid=($edmDao->add($data))){
				echo json_encode( array( 'success'=> 1, 'edmid'=>$newid) );
			}else{
				echo json_encode( array( 'success'=> 0) );
			}		
		}else{
			$data['update_time']=time();				
			if($edmDao->where('id='.$id)->save($data)){
				echo json_encode( array( 'success'=> 1) );
			}else{
				echo json_encode( array( 'success'=> 0) );
			}		
		}
		
	}
	public function load(){
		$edmid=$_REQUEST['id'];
		$edmDao=D('Edm');
		if($edm=$edmDao->find($edmid)){			
			echo json_encode( array( 'result'=> 1,'data'=>$edm));
		}else{
			echo json_encode( array( 'result'=> 0));
		}
	}
	public function generateedm($type){
		$poster_id=$_REQUEST['posterid'];
		$event_id_1=$_REQUEST['event_id_1'];
		$event_id_2=$_REQUEST['event_id_2'];
		$event_id_3=$_REQUEST['event_id_3'];
		$event_id_4=$_REQUEST['event_id_4'];
		$promotion_id_1=$_REQUEST['promotion_id_1'];
		$promotion_id_2=$_REQUEST['promotion_id_2'];
		$promotion_id_3=$_REQUEST['promotion_id_3'];
		$promotion_id_4=$_REQUEST['promotion_id_4'];
		$review_id_[1]=$_REQUEST['review_id_1'];
		$review_id_[2]=$_REQUEST['review_id_2'];
		$review_id_[3]=$_REQUEST['review_id_3'];	
		
		$weatherDao=D('Weather');
		$date=date('Y-m-d',time()+86400);		
		$where['date']=array('egt',$date);
		$weatherList=$weatherDao->where($where)->Limit(3)->select();
		foreach($weatherList as $key=>$val){			
			$weatherList[$key]['temp_description'] = explode(iconv("BIG5","UTF8","至"),$val['temp_description']);			
		}		
		$this->assign('weatherList',$weatherList);
							
		$posterDao=D('Poster');
		$poster=$posterDao->relation(true)->find($poster_id);
		
		
		$review_content[1]=urldecode(base64_decode($_REQUEST['review_id_1_content']));
		$review_content[2]=urldecode(base64_decode($_REQUEST['review_id_2_content']));
		$review_content[3]=urldecode(base64_decode($_REQUEST['review_id_3_content']));
		
		
		
		$eventDao=D('Event');		
		$event_1=$eventDao->relation(true)->find($event_id_1);
		$event_2=$eventDao->relation(true)->find($event_id_2);
		$event_3=$eventDao->relation(true)->find($event_id_3);
		$event_4=$eventDao->relation(true)->find($event_id_4);
		
		$event_1['date']=urldecode(base64_decode($_REQUEST['event_id_1_date']));
		$event_2['date']=urldecode(base64_decode($_REQUEST['event_id_2_date']));
		$event_3['date']=urldecode(base64_decode($_REQUEST['event_id_3_date']));
		$event_4['date']=urldecode(base64_decode($_REQUEST['event_id_4_date']));
		
		$promotionDao=D('Promotion');
		$promotion_1=$promotionDao->relation(true)->find($promotion_id_1);
		$promotion_2=$promotionDao->relation(true)->find($promotion_id_2);
		$promotion_3=$promotionDao->relation(true)->find($promotion_id_3);
		$promotion_4=$promotionDao->relation(true)->find($promotion_id_4);
		
		$reviewDao=D('Review');	
		
		for($i=1;$i<=3;$i++){
			$review_[$i]=$reviewDao->find($review_id_[$i]);	
			$userDao = D('User');
			$eventDao = D('Event');
			$promotionDao = D('Promotion');
			$shopDao=D('Shop');
			$review_[$i]['content']=$review_content[$i];
			$review_[$i]['user'] = $userDao->find($review_[$i]['user_id']);			
			if($review_[$i]['shop_id']!=0){
				$review_[$i]['for_type'] = 'shop';
				$review_[$i]['shop'] = $shopDao->relation(true)->find($review_[$i]['shop_id']);
			}elseif($review_[$i]['event_id']!=0){
				$review_[$i]['for_type'] = 'event';
				$review_[$i]['event'] = $eventDao->relation(true)->find($review_[$i]['event_id']);
			}elseif($review_[$i]['promotion_id']!=0){
				$review_[$i]['for_type'] = 'promotion';
				$review_[$i]['promotion'] = $promotionDao->relation(true)->find($review_[$i]['promotion_id']);
			}			
		}
		
		$newsList=M('news')->limit(2)->order('id desc')->select();		
		
		$this->assign('review_',$review_);	
		$this->assign('newsList',$newsList);
		$this->assign('poster',$poster);
				
		$this->assign('event_1',$event_1);
		$this->assign('event_2',$event_2);
		$this->assign('event_3',$event_3);
		$this->assign('event_4',$event_4);
		
		$this->assign('promotion_1',$promotion_1);
		$this->assign('promotion_2',$promotion_2);
		$this->assign('promotion_3',$promotion_3);
		$this->assign('promotion_4',$promotion_4);
		
		$this->assign('review_1',$review_1);
		$this->assign('review_2',$review_2);
		$this->assign('review_3',$review_3);
		$link=iconv("BIG5","UTF8","如不想收到OutStreet之最新情報及電郵<a href='##link##' style='color:#2155b5;padding:0 2px 0 2px;'>按此</a>取消");
		//print_r($event_1);
		if($type=='gen'){
			$this->assign('link',$link);
			$body=$this->fetch('Index:generateedm','big5');
			return($body);
		}elseif($type=='save'){			
		}else{			
			//$this->display('Index:generateedm','big5');
			$this->assign('link',$link);
			$this->display( 'Index:generateedm','big5');
		}		
	}
	public function edmidlist(){
		$edmDao=D('Edm');
		$edmidlist=$edmDao->select();
		$this->ajaxReturn($edmidlist);
	}
	public function send(){
		$edmid=$_REQUEST['edmid'];		
		//$src=base64_decode($src);		
		if($_REQUEST['key']=='0531'){
			$send=true;
		}
		$caption=iconv("UTF-8","BIG5//TRANSLIT",base64_decode($_REQUEST['caption']));		
		//$caprion=utf8conv2charset(base64_decode($_REQUEST['caption']));
		$edmselector=$_REQUEST['edmselector'];
		$sendtype=$_REQUEST['sendtype'];
		if($sendtype=='all'&&$send==true){
			$where['acc_edm']=1;
			$where['is_email_verify']=1;
			$where['email']=array('neq','');
			$where['id']=array('gt','17');
		}else{
			$where['id']=$_REQUEST['userid'];
			$where['acc_edm']=1;
			$where['email']=array('neq','');
		}		
		$userDao=D('User');
		$maillist=$userDao->where($where)->select();		
		$success=0;
		if($edmselector=='generateEdm'){		
			$edmDao=D('Edm');
			$edm=$edmDao->where('id='.$edmid)->select();			
			$poster_id=$edm[0]['posterid'];
			$event_id_1=$edm[0]['event_id_01'];
			$event_id_2=$edm[0]['event_id_02'];
			$event_id_3=$edm[0]['event_id_03'];
			$event_id_4=$edm[0]['event_id_04'];
			$promotion_id_1=$edm[0]['promotion_id_01'];
			$promotion_id_2=$edm[0]['promotion_id_02'];
			$promotion_id_3=$edm[0]['promotion_id_03'];
			$promotion_id_4=$edm[0]['promotion_id_04'];
			$review_id_[1]=$edm[0]['review_id_01'];
			$review_id_[2]=$edm[0]['review_id_02'];
			$review_id_[3]=$edm[0]['review_id_03'];
			
			$weatherDao=D('Weather');
			$date=date('Y-m-d',time()+86400);
			$where=array();
			$where['date']=array('egt',$date);
			$weatherList=$weatherDao->where($where)->Limit(3)->select();
			foreach($weatherList as $key=>$val){
				$weatherList[$key]['temp_description'] = explode(iconv("BIG5","UTF8","至"),$val['temp_description']);			
			}			
			$posterDao=D('Poster');
			$poster=$posterDao->relation(true)->find($poster_id);			
			
			/*$review_content[1]=urldecode(base64_decode($_REQUEST['review_id_1_content']));
			$review_content[2]=urldecode(base64_decode($_REQUEST['review_id_2_content']));
			$review_content[3]=urldecode(base64_decode($_REQUEST['review_id_3_content']));*/
			
			$review_content[1]=$edm[0]['review_id_01_content'];
			$review_content[2]=$edm[0]['review_id_02_content'];
			$review_content[3]=$edm[0]['review_id_03_content'];			
			
			$eventDao=D('Event');		
			$event_1=$eventDao->relation(true)->find($event_id_1);
			$event_2=$eventDao->relation(true)->find($event_id_2);
			$event_3=$eventDao->relation(true)->find($event_id_3);
			$event_4=$eventDao->relation(true)->find($event_id_4);
			
			/*$event_1['date']=urldecode(base64_decode($_REQUEST['event_id_1_date']));
			$event_2['date']=urldecode(base64_decode($_REQUEST['event_id_2_date']));
			$event_3['date']=urldecode(base64_decode($_REQUEST['event_id_3_date']));
			$event_4['date']=urldecode(base64_decode($_REQUEST['event_id_4_date']));*/
			$event_1['date']=$edm[0]['event_id_01_date'];
			$event_2['date']=$edm[0]['event_id_02_date'];
			$event_3['date']=$edm[0]['event_id_03_date'];
			$event_4['date']=$edm[0]['event_id_04_date'];
			
			$promotionDao=D('Promotion');
			$promotion_1=$promotionDao->relation(true)->find($promotion_id_1);
			$promotion_2=$promotionDao->relation(true)->find($promotion_id_2);
			$promotion_3=$promotionDao->relation(true)->find($promotion_id_3);
			$promotion_4=$promotionDao->relation(true)->find($promotion_id_4);
			
			$reviewDao=D('Review');	
			
			for($i=1;$i<=3;$i++){
				$review_[$i]=$reviewDao->find($review_id_[$i]);	
				$userDao = D('User');
				$eventDao = D('Event');
				$promotionDao = D('Promotion');
				$shopDao=D('Shop');
				$review_[$i]['content']=$review_content[$i];
				$review_[$i]['user'] = $userDao->find($review_[$i]['user_id']);			
				if($review_[$i]['shop_id']!=0){
					$review_[$i]['for_type'] = 'shop';
					$review_[$i]['shop'] = $shopDao->relation(true)->find($review_[$i]['shop_id']);
				}elseif($review_[$i]['event_id']!=0){
					$review_[$i]['for_type'] = 'event';
					$review_[$i]['event'] = $eventDao->relation(true)->find($review_[$i]['event_id']);
				}elseif($review_[$i]['promotion_id']!=0){
					$review_[$i]['for_type'] = 'promotion';
					$review_[$i]['promotion'] = $promotionDao->relation(true)->find($review_[$i]['promotion_id']);
				}			
			}
			
			$newsList=M('news')->limit(2)->order('id desc')->select();		
			
			$this->assign('review_',$review_);	
			$this->assign('newsList',$newsList);
			$this->assign('poster',$poster);
					
			$this->assign('event_1',$event_1);
			$this->assign('event_2',$event_2);
			$this->assign('event_3',$event_3);
			$this->assign('event_4',$event_4);
			$this->assign('weatherList',$weatherList);			
			$this->assign('promotion_1',$promotion_1);
			$this->assign('promotion_2',$promotion_2);
			$this->assign('promotion_3',$promotion_3);
			$this->assign('promotion_4',$promotion_4);
			
			$this->assign('review_1',$review_1);
			$this->assign('review_2',$review_2);
			$this->assign('review_3',$review_3);			
			$link=iconv("BIG5","UTF8","如不想收到OutStreet之最新情報及電郵<a href='##link##' style='color:#2155b5;padding:0 2px 0 2px;'>按此</a>取消");
			$this->assign('link',$link);
			$date=date('Y_m_d',time());		
				
			$url="http://edm.outstreet.com.hk/generate/".$date.".shtml";
			$mailbody = "<div style='text-align:center;'>如無法觀看以下內容,請登入<a href='".$url."'>".$url."</a></div>";
			$mailbody .= $this->fetch('Index:generateedm','big5');			
			$caption=iconv("UTF-8","BIG5",$edm[0]['caption']);
		}else{
			$mailbody = $this->fetch('web/Tpl/default/Index/'.$edmselector.'/edm.html','big5');
		}
		foreach($maillist as $key=>$val){
			$email=$val['email'];			
			$key=C('EMAIL_ENCRYPT_KEY');
			$hash=md5($val['id'].$key);
			$body=str_replace('##link##','http://outstreet.com.hk/user/unsubscribe/id/'.$val['id'].'/hash/'.$hash,$mailbody);			
			$this->smtp_mail_log($val['email'],$caption,$body,'',1,4);
			$success++;
		}
		$head="<?xml version='1.0' encoding='BIG5'?>";
		$head.="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.1//EN' 'http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd'>";
		$head.="<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='zh-TW'>";
		$head.="<head>";
		$head.="<meta http-equiv='content-type' content='application/xhtml+xml; charset=BIG5' />";
		$head.="</head>";
		$body_top="<body>";
		$body_bot="</body>";
		$this->assign('head',$head);
		$this->assign('body_top',$body_top);
		$this->assign('body_bot',$body_bot);
		$link="";
		$this->assign('link',$link);
		$this->buildHtml($date,"generate/","Index:generateedm","big5","text/html");
		if($success!=0){
			echo json_encode( array( 'result'=> '1','msg'=>'1') );
		}else{
			echo json_encode( array( 'result'=> '0','msg'=>'2') );
		}
	}
}

?>