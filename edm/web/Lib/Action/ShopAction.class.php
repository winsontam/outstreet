<?php

class ShopAction extends BaseAction{

	private function _subcate($cateids, $shopid, $shopname){

		$shopCateDao = D('ShopCate');
		$shopCateMapDao = D('ShopCateMap');
			
		$where = array();
		$where['shop_id'] = $shopid;
		$shopCateMapDao->where($where)->delete();

		$cateids = explode(',',$cateids);
			
		foreach($cateids as $cateid){
			if (!empty($cateid)){
				$cateInfo = $shopCateDao->find($cateid);
				if ($cateInfo){
					$data = array();
					$data['shop_id'] = $shopid;
					$data['shop_name'] = $shopname;
					$data['cate_id'] = $cateInfo['id'];
					$data['cate_name'] = $cateInfo['name'];

					$shopCateMapDao->data($data)->add();
				}
			}
		}

	}

	public function search(){
		checkVisitFrequecy();
		// 加載數據訪問對象
		$shopDao = D('Shop');
		 
		$cate_ids = $_REQUEST['cate_ids_search'];
		$sub_location_ids = $_REQUEST['sub_location_ids'];
		$searchterms = $_REQUEST['searchterms'];
		$searchterms = trim($searchterms);

		if (!empty($cate_ids)){
			$cate_ids = explode(',',$cate_ids);
			$shopCateDao = D('ShopCate');
			$where = array();
			$where['id'] = array('in', $cate_ids);
			$cateList = $shopCateDao->where($where)->select();
	
			$cate_names = '';
			foreach($cateList as $cate){
				$cate_names .= $cate['name'] . ',';
			}
			$this->assign('cate_names',$cate_names);

			$children = $shopCateDao->where(array('parent_id' => array('in', $cate_ids)))->select();
			$childrenIds = array();
			foreach($children AS $val) { $childrenIds[] = $val['id']; $cate_ids[] = $val['id']; }
			
			while( count($childrenIds) )
			{
				$children = $shopCateDao->where(array('parent_id' => array('in', $childrenIds)))->select();
				$childrenIds = array();
				foreach($children AS $val) { $childrenIds[] = $val['id']; $cate_ids[] = $val['id']; }
			}
		}

		if (!empty($sub_location_ids)){
			$sub_location_ids = gzinflate(base64_decode(urldecode($sub_location_ids)));
			$locationSubDao = D('LocationSub');
			$where = array();
			$where['id'] = array('in', $sub_location_ids);
			$locationList = $locationSubDao->where($where)->select();
	
			$sub_location_names = '';
			foreach($locationList as $location){
				$sub_location_names .= $location['name'] . ',';
			}
			$this->assign('sub_location_names',$sub_location_names);
		}

		$where = array();
		 
		if (!empty($cate_ids)) $where['cate_id'] = array('in', $cate_ids);
		if (!empty($sub_location_ids)) $where['sub_location_id'] = array('in', $sub_location_ids);
		$where['is_audit'] = 1;
		 
		/*if (!empty($searchterms)){				
			$strArr = explode(" ", $searchterms);
			$count = count($strArr);		
			for ($i=0;$i<$count;$i++){
				$like = "岈".$strArr[$i]."岈";
				$transsql = "select `name` from translate where `keywords` LIKE '%$like%'";
				$model = new Model();
				$transTemp = $model->query($transsql);
				$trans[$i] = $transTemp[0]['name'];			
				if ($trans!=""){				
					$transword = $transword.$trans[$i];				
				}			 
			}					
			
			if($transword!=""){			
				$trans = array_unique($trans);
				$tcount = count($trans);
				
				for($i=0;$i<$count;$i++){
					if ($trans[$i] !=''){
					$ttrans[].=$trans[$i];
					}
				}
					
				$column = Array("`name`", "`keyword`", "`english_name`", "`address`");
				$columncount = count($column);
				$k=0;
				
				for($j=0;$j<$columncount;$j++){
					for($i=0;$i<$tcount;$i++){
						if ($k < 1){
							if ($ttrans[$i]!=''){
						$tempwhere[$k] = " $column[$j] LIKE '%$ttrans[$i]%'";
							}				
						}
						else {
							if ($ttrans[$i]!=''){
								$tempwhere[$k] = " or $column[$j] LIKE '%$ttrans[$i]%'";
							}			
						}
						$k++;
						$tempwhere = array_unique($tempwhere);				
					}
				}
	
				for($i=0;$i<$k;$i++){
					$twhere = $twhere."".$tempwhere[$i];
				}								   
			} else {
				$likewhere = array();
				$likewhere['keyword'] = array('like', "%$searchterms%");		
				$likewhere['name'] = array('like', "%$searchterms%");			
				$likewhere['english_name'] = array('like', "%$searchterms%");
				$likewhere['address'] = array('like', "%$searchterms%");
				$likewhere['_logic'] = 'or';
				$where['_complex'] = $likewhere;
			}
		}
		
		//設置分頁代碼
		$count = $shopDao->join( 'shop_cate_map on shop_fields.id = shop_cate_map.shop_id' )->where($where)->count('distinct shop_fields.id');

		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
					
		//new 獲取數據列表
		if($transword!=''){
			$tsql = "select * from `shop_fields` where $twhere order by `hit_rate`";
			$shopList = $shopDao->relation(true)->field('shop_fields.*')->join( 'shop_cate_map on shop_fields.id = shop_cate_map.shop_id' )->where($where)->group('shop_fields.id')->where($twhere)->limit($limit)->order('hit_rate desc, name asc, english_name asc')->select();
			$count = $shopDao->join( 'shop_cate_map on shop_fields.id = shop_cate_map.shop_id' )->where($twhere)->count('distinct shop_fields.id');
			import('ORG.Util.Page');
			$p = new Page($count);
			$multipage = $p->show();
			$limit = $p->firstRow.','.$p->listRows;
		} else {
			$shopList = $shopDao->relation(true)->field('shop_fields.*')->join( 'shop_cate_map on shop_fields.id = shop_cate_map.shop_id' )->where($where)->group('shop_fields.id')->limit($limit)->order('hit_rate desc, name asc, english_name asc')->select();
		}*/

		if ( $searchterms )
		{
			$terms = $searchterms;
			$allterms = array();
			if ( preg_match_all( '/\w+/', $terms, $m ) )
			{
				foreach ( $m[0] AS $t )
				{
					$terms = str_replace($t,'',$terms);
					$allterms[$t] = 'e';
				}
			}
			$mterms = array();
			mb_regex_encoding( 'UTF-8' );
			mb_ereg_search_init( $terms, '\w+' );
			while ( ( $m = mb_ereg_search_regs() ) )
				{ $allterms[ $m[0] ] = 'm'; }

			$suggestWhere = array();
			foreach ( $allterms AS $term => $type )
			{
				$suggestWhere[ 'keyword' ][] = array( 'like', $term );
			}
			$suggestWhere[ 'keyword' ][] = 'or';
			$suggestKeywords = M('KeywordTranslate')->where($suggestWhere)->select();
			foreach ( $suggestKeywords AS $val )
			{
				$keywords = explode( ',', $val['suggest_keywords'] );
				foreach ( $keywords AS $k ) { if ($k) { $allterms[ $k ] = 's'; } }
			}

			$searchcols = array(
				'CONCAT(`name`,":",`english_name`)' => 10,
				'`keyword`' => 6,
				'`address`' => 4
			);
			$likecolumn = '[likecols]';
			$orQuerys = array();
			$countQuerys = array();
			foreach ( $allterms AS $term => $type )
			{
				if ( $type == 'e' || $type == 's' )
					{ $orQuerys[] = $likecolumn . ' REGEXP "[[:<:]]' . $term . '.{0,3}[[:>:]]"'; }
				elseif ( $type == 'm' )
				{
					if ( ( $len = preg_match_all( '/./u', $term, $t ) ) > 1 )
					{
						$orQuerys[] = $likecolumn . ' REGEXP "(' . implode('|',$t[0]) . '){' . round($len*0.8) . ',' . ($len) . '}"';
					}
					else
					{
						$orQuerys[] = $likecolumn . ' LIKE "%' . $term . '%"';
					}
				}
				foreach ( $searchcols AS $col => $score )
				{
					$countQuerys[] = '( ( ' . $col . ' REGEXP "' . $term . '" ) * ' . $score . ')' 
						. ' + ( ( ' . $col . ' REGEXP "[[:<:]]' . $term . '[[:>:]]" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "^' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "' . $term . '$" ) * ' . $score . ')'
						. ' + ( IF( length(' . $col . ') = length("' . $term . '"), 1, 0 ) * ' . $score . ')';
				}
			}

			$cates=M('ShopCate')->where(array('_string'=>'(' . str_replace( '[likecols]', 'name', implode( ' OR ', $orQuerys ) ) . ')'))->select();
			$searchcateids = array();
			foreach($cates AS $cate) { $searchcateids[] = $cate['id']; }
			if ( count($searchcateids) ) {
				$where[ '_string' ] = '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' OR ', $orQuerys ) ) . ' OR b.cate_id IN (' . implode( ',', $searchcateids ) . '))';
				$countQuerys[] = '((b.cate_id IN (' . implode( ',', $searchcateids ) . '))*20)';
			}
			else{
				$where[ '_string' ] = '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' OR ', $orQuerys ) ) . ')';
			}

			$countField = '((' . implode( ' + ', $countQuerys ) . ') + ( IF(c.review_count is null,0,c.review_count) )) AS scores';
			//print_r($orQuerys);
		}
		else
		{
			$countField = 'IF(c.review_count is null,0,c.review_count) AS scores';
		}
		
		if ( !$_REQUEST['groupbrand'] )
		{
			$count = $shopDao->table( 'shop_cate_map b' )->join( 'shop_fields a ON a.id = b.shop_id' )->where( $where )->count( 'distinct a.id' );
			import('ORG.Util.Page');
			$p = new Page($count);
			$multipage = $p->show();
			$limit = $p->firstRow.','.$p->listRows;

			$shopList = $shopDao->relation(true)->field( 'a.*,' . $countField )->table( 'shop_cate_map b' )->join( 'shop_fields a ON a.id = b.shop_id' )->join( 'statistics_shop c ON b.shop_id = c.shop_id' )->where( $where )->group( 'a.id' )->limit( $limit )->order( 'scores DESC, hit_rate DESC' )->select();
		}
		else
		{
			$count = $shopDao->field('COUNT(distinct IF(a.brand_id>0,CONCAT("b",a.brand_id),CONCAT("s",a.id))) AS count')->table( 'shop_cate_map b' )->join( 'shop_fields a ON a.id = b.shop_id' )->where( $where )->find();
			$count = $count['count'];
			import('ORG.Util.Page');
			$p = new Page($count);
			$multipage = $p->show();
			$limit = $p->firstRow.','.$p->listRows;

			$shopList = $shopDao->relation(true)->field( 'a.*,' . $countField )->table( 'shop_cate_map b' )->join( 'shop_fields a ON a.id = b.shop_id' )->join( 'statistics_shop c ON b.shop_id = c.shop_id' )->where( $where )->group( 'IF(a.brand_id>0,CONCAT("b",a.brand_id),CONCAT("s",a.id))' )->limit( $limit )->order( 'scores DESC, hit_rate DESC' )->select();
		}

		foreach($shopList as &$shopInfo){
			// Temp for list image (20100621 by tammy)
			$sqlImg = "select file_path from shop_picture where shop_id = ".$shopInfo['id'];
			$shopImg = $shopDao->query($sqlImg);
			
			if (!empty($shopImg[0]['file_path'])){
				$shopInfo['pictures'][0]['file_path'] = $shopImg[0]['file_path'];
			} else {
				$brandDao = D('Brand');
				$brandInfo = $brandDao->where('id='.$shopInfo['brand_id'])->select();
				
				if (!empty($brandInfo[0]['file_path'])){
					$shopInfo['pictures'][0]['file_path'] = $brandInfo[0]['file_path'];
				}
			}
			// Temp for list image (20100621 by tammy)
			
			//working hour logic
			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shopInfo['id']);
			$working_hour = $shopTimeDao->where($where)->order('start_time,end_time,recurring_week')->select();
			
			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shopInfo['id'],"recurring_week "=>0);
			$special_working_hour = $shopTimeDao->where($where)->order('start_day,end_day')->select();
			
			$output = generateShopWorkTime($shopInfo['work_time'],$working_hour,$special_working_hour);
			
			$shopInfo['work_time'] = $output;
			//end working hour logic
		}

		if(!empty($searchterms)){
			$searchLogDao = D('SearchLog');
			$data = array();
			$data['create_time'] = time();
			$data['keywords'] = $searchterms;
			$data['results'] = $count;
			$searchLogDao->add($data);
		}

		//數據壓入模板
		$this->assign('multipage', $multipage);
		$this->assign('shopList', $shopList);
		 
		//模板與數據整合 并輸出到HTTP流
		$this->display();
	}

	public function lists(){
		checkVisitFrequecy();

		// 加載數據訪問對象
		$shopDao = D('Shop');
		$shopCateDao = D('ShopCate');
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');
		$brandDao = D('Brand');

		//接收查詢變量	
		Import('ORG.Util.Input');
		$cate_id = $_GET['cate_id'];
		$mainlocationid = $_GET['mainlocationid'];
		if ($mainlocationid) { $_SESSION[ '_mainlocationid' ] = $mainlocationid; }
		$sublocationid = $_GET['sublocationid'];
		if ($sublocationid) { $_SESSION[ '_sublocationid' ] = $sublocationid; }
		$brandId = $_GET['brandid'];
		$parentshopid = $_GET['parentshopid'];
		$rootlocation = $_GET['rootlocation'];

		if( $cate_id ){
			if (!$mainlocationid) { $mainlocationid = $_SESSION[ '_mainlocationid' ]; }
			if (!$sublocationid) { $sublocationid = $_SESSION[ '_sublocationid' ]; }
		} else { $_SESSION[ '_mainlocationid' ] = $_SESSION[ '_sublocationid' ] = null; }
		
		$cate_ids = array();
		if ( $cate_id && $shopCateDao->where(array('id'=>$cate_id))->count() ) {
			$cate_ids[] = $cate_id;
			$shopCateList = $shopCateDao->where(array('parent_id'=>$cate_id))->order('shop_count desc')->select();
			foreach( $shopCateList AS $val ) { $cate_ids[] = $val['id']; }
			$this->assign('shopCateList', $shopCateList);
		} else {
			$shopCateList = $shopCateDao->where(array('parent_id'=>0))->order('shop_count desc')->select();
			$this->assign('shopCateList', $shopCateList);
		}
		
		$parent_id = $cate_id;
		while( $parent_id ) {
			$cate = $shopCateDao->find($parent_id);
			$cate_list[] = $cate;
			$parent_id = $cate['parent_id'];
		}
		$this->assign('cate_list', array_reverse($cate_list));
		$this->assign('cate_id', $cate_id);

		$search_cate_ids = array();
		if ($cate_id)
		{
			$search_cate_ids[] = $cate_id;
			$loopids = array();
			$loopids[] = $cate_id;
			$loop = true;
			while($loop)
			{
				$shopCateList = $shopCateDao->where(array('parent_id'=>array('in',$loopids)))->select();
				$cate = array();
				foreach( $shopCateList AS $val ) {
					$cate[] = $val['id'];
					$search_cate_ids[] = $val['reference_id']?$val['reference_id']:$val['id'];
				}
				if ( count($cate) ) { $loopids = $cate; }
				else { $loop = false; }
			}
		}

		$whereRef = array();
		$whereRef['parent_id']=$cate_id;
		$redirectShopList = $shopCateDao->where($whereRef)->order('shop_count desc')->select();
		if(sizeof($redirectShopList)>0&&isset($cate_id)){
			$cate_direct_id = $redirectShopList[0]['id'];
		}else{
			$cate_direct_id = $cate_id;
		}	
		$this->assign('cate_direct_id',$cate_direct_id);

		//SESSION狀態維護  END
		//=====================================

		//獲取商戶主目錄列表 壓入模板
		$locationMainList = $locationMainDao->order('shop_count desc')->select();
		$this->assign('locationMainList', $locationMainList);

		//獲取商戶子目錄列表 壓入模板
		if (isset($mainlocationid)){
			$where = array();
			$where['main_location_id'] = $mainlocationid;
			$locationSubList = $locationSubDao->where($where)->order('shop_count desc')->select();
			$this->assign('locationSubList', $locationSubList);
		}
		 
		//獲取當前SubLocation信息 壓入模板
		if (isset($sublocationid)){
			$where = array();
			$where['id'] = $sublocationid;
			$curSubLocation = $locationSubDao->where($where)->find();
			$this->assign('curSubLocation', $curSubLocation);
		}
			
		//指定sublocationid時 mainlocationid根據SubLocation決定
		if ($curSubLocation){
			$mainlocationid = $curSubLocation['main_location_id'];
		}

		//獲取當前MainLocation信息 壓入模板
		if (isset($mainlocationid)){
			$where = array();
			$where['id'] = $mainlocationid;
			$curMainLocation = $locationMainDao->where($where)->find();
			$this->assign('curMainLocation', $curMainLocation);
		}
		
		$sql_where = ' where shop_fields.id = shop_cate_map.shop_id AND is_audit=1';

		if (count($search_cate_ids)) $sql_where .= ' and shop_cate_map.cate_id IN (' . implode(',',$search_cate_ids) . ')';
		if (!empty($mainlocationid)) $sql_where .= ' and main_location_id = ' . $mainlocationid;
		if (!empty($sublocationid)) $sql_where .= ' and sub_location_id = ' . $sublocationid;
		if (!empty($brandId)) $sql_where .= ' and brand_id = ' . $brandId;
		if (!empty($parentshopid)) $sql_where .= ' and parent_shop_id = ' . $parentshopid;

		if (!empty($searchterms)){
			$sql_likewhere = ' (1=0';
			$sql_likewhere .= " or keyword like '%$searchterms%'";
			$sql_likewhere .= " or name like '%$searchterms%'";
			$sql_likewhere .= " or address like '%$searchterms%'";
			$sql_likewhere .= " )";
			$sql_where .= 'and ' . $sql_likewhere;
		}
		
		//=======================================
		//构造union查询的where子句
		$sql_unionwhere = ' where is_audit=1';
		if (!empty($mainlocationid)) $sql_unionwhere .= ' and main_location_id = ' . $mainlocationid;
		if (!empty($sublocationid)) $sql_unionwhere .= ' and sub_location_id = ' . $sublocationid;
		if (!empty($brandId)) $sql_unionwhere .= ' and brand_id = ' . $brandId;
		if (!empty($parentshopid)) $sql_unionwhere .= ' and parent_shop_id = ' . $parentshopid;
		if (!empty($searchterms)){
			$sql_likewhere = ' (1=0';
			$sql_likewhere .= " or keyword like '%$searchterms%'";
			$sql_likewhere .= " or name like '%$searchterms%'";
			$sql_likewhere .= " or address like '%$searchterms%'";
			$sql_likewhere .= " )";
			$sql_unionwhere .= 'and ' . $sql_likewhere;
		}
		
		//生成用于count的sql语句
		$sql = "select count(DISTINCT shop_fields.id) as ct from shop_fields join shop_cate_map $sql_where";
		
		//====================================
		//生成limit子句  和 分页html代码
		$countInfo = $shopDao->query($sql);
		$count = $countInfo[0]['ct'];
		$count += $countInfo[1]['ct'];

		import('ORG.Util.Page');
		$p = new Page($count);
		$multipage = $p->show();
		$limit = $p->firstRow.','.$p->listRows;
		
		//生成sql语句
		$sql = "select shop_fields.* from shop_fields join shop_cate_map $sql_where ";
		$sql .= "group by shop_fields.id order by hit_rate desc, id desc limit $limit";

		$shopList = $shopDao->query($sql);

		foreach($shopList as $key=>$shop){
			$shopDao = D('Shop');
			$where['id']=$shop['id'];
			$shopRel = $shopDao->relation(true)->where($where)->find();
			$shopList[$key]['sub_location']['name']=$shopRel['sub_location']['name'];
			$shopList[$key]['main_location']['name']=$shopRel['main_location']['name'];			
		}

		$statisticsShopDao = D('StatisticsShop');
		$shopRateDao = D('ShopRate');
		$userDao = D('User');
		foreach($shopList as &$shopInfo){
			$where = array();
			$where['shop_id'] = $shopInfo['id'];
			$shopInfo['statistics'] = $statisticsShopDao->where($where)->find();
			$shopInfo['rate'] = $shopRateDao->where($where)->find();
			$shopInfo['keywords']=explode(",",$shopInfo['keyword']);
			
			// Temp for list image (20100621 by tammy)
			$sqlImg = "select file_path from shop_picture where shop_id = ".$shopInfo['id'];
			$shopImg = $shopDao->query($sqlImg);
			
			if (!empty($shopImg[0]['file_path'])){
				$shopInfo['pictures'][0]['file_path'] = $shopImg[0]['file_path'];
			} else {
				$brandInfo = $brandDao->where('id='.$shopInfo['brand_id'])->select();
				
				if (!empty($brandInfo[0]['file_path'])){
					$shopInfo['pictures'][0]['file_path'] = $brandInfo[0]['file_path'];
				}
			}
			// Temp for list image (20100621 by tammy)
			// list sub cates
			$sqlcate = "select b.* from shop_cate_map a left join shop_cate b on a.cate_id = b.id where shop_id = ".$shopInfo['id'];
			$shopInfo['sub_cates'] = $shopDao->query($sqlcate);

			//working hour logic
			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shopInfo['id']);
			$working_hour = $shopTimeDao->where($where)->order('start_time,end_time,recurring_week')->select();
			
			$shopTimeDao = D('ShopTime');
			$where = array("shop_id"=>$shopInfo['id'],"recurring_week "=>0);
			$special_working_hour = $shopTimeDao->where($where)->order('start_day,end_day')->select();
			
			$output = generateShopWorkTime($shopInfo['work_time'],$working_hour,$special_working_hour);

			$shopInfo['work_time'] = $output;
			//end working hour logic
		}

		//數據壓入模板
		$this->assign('multipage', $multipage);
		$this->assign('shopList', $shopList);

		$statisticsShopDao = D('StatisticsShop');
		$where = array();
		$shopreviewlist = $statisticsShopDao->relation(true)->where($where)->limit(5)->order('review_count desc')->select();
		$this->assign('shopreviewlist', $shopreviewlist);

		//模板與數據整合 并輸出到HTTP流
		$this->display();
	}
	
	public function index(){
		 
		// 加載數據訪問對象
		$shopDao = D('Shop');
		$statisticsShopDao = D('StatisticsShop');
		$ShopCateDao = D('ShopCate');
		$locationMainDao = D('LocationMain');

		//獲取Shop分類主目錄列表 壓入模板
		$ShopCateList = $ShopCateDao->where( array( 'parent_id' => 0 ) )->order('shop_count desc')->select();
		$this->assign('ShopCateList', $ShopCateList);
		 
		//獲取商戶主目錄列表 壓入模板
		$locationMainList = $locationMainDao->order('shop_count desc')->select();
		$this->assign('locationMainList', $locationMainList);

		$this->getRateList(array(1),'buycount','buyratelist');
		$this->getRateList(array(7),'clubcount','clubratelist');
		$this->getRateList(array(2),'beautycount','beautyratelist');
		 
		$where = array();
		$where['is_audit'] = 1;
		$shopnewlist = $shopDao->relation(true)->where($where)->limit(5)->order('create_time desc')->select();
		$this->assign('shopnewlist', $shopnewlist);

		$where = array();
		$shopreviewlist = $statisticsShopDao->relation(true)->where($where)->limit(5)->order('review_count desc')->select();
		$this->assign('shopreviewlist', $shopreviewlist);
		 
		$where = array();
		$reviewDao = D('Review');
		$where['is_audit'] = 1;
		$reviewList = $reviewDao->relation(true)->where($where)->order('create_time desc')->limit(10)->select();
		$this->assign('reviewList', $reviewList);

		$threadDao = D('ForumThread');
		$threadList = $threadDao->order('post_count desc')->limit(5)->select();
		$this->assign('threadList', $threadList);

		//模板與數據整合 并輸出到HTTP流
		$this->display();

	}//end index

	public function view(){
		checkVisitFrequecy();
		$shopDao = D('Shop');
		$shopId = $_REQUEST['id'];

		if (isset($shopId)){

			$shopInfo = $shopDao->relation(true)->find($shopId);
			$shopRate = M('shopRate')->find($shopId);

			if (isset($shopInfo) && ($shopInfo['is_audit']==1 || (@$_SESSION['isLogged'] && $_SESSION['username'])) ){
				$reviewDao = D('Review');
				$reviewRateDao = D('ReviewRate');
				$userDao = D('User');
				$reviewPictureDao = D('ReviewPicture');
				$where = array("shop_id"=>$shopId);

				if (isset($_REQUEST['reviewid'])){					 
					$reviewid = $_REQUEST['reviewid'];
				}

				if ( $reviewid && !@$_SESSION['isLogged'] && !$_SESSION['username'] ) { $where['is_audit'] = 1; }
				$count = $reviewDao->where($where)->count('id');
				import('ORG.Util.Page');
				$p = new Page($count);
				$multipage = $p->show();
				$limit = $p->firstRow.','.$p->listRows;
				$shopInfo['reviews'] = $reviewDao->where($where)->limit($limit)->order('create_time desc')->select();		
				
				if(!$shopInfo['reviews']) {
					$shopInfo['reviews'] = array();
				}
				
				$shopInfo['keywords']=explode(",",$shopInfo['keyword']);

				foreach($shopInfo['reviews'] as &$review){
					$review['rate'] = $reviewRateDao->where("review_id={$review['id']}")->find();
					$review['user'] = $userDao->find($review['user_id']);
					$review['pictures'] = $reviewPictureDao->where("review_id={$review['id']}")->select();
				}

				if (!empty($shopInfo['brand_id'])){
					$where = array();
					$where['brand_id'] = $shopInfo['brand_id'];
					$this->assign('brandShopList', $shopDao->where($where)->order('is_main_shop desc, create_time asc')->select());
				}

				if(count($shopInfo['pictures']) == 0) {
					$brandDao = D('Brand');
					$brandInfo = $brandDao->where('id='.$shopInfo['brand_id'])->select();
					
					if (!empty($brandInfo[0]['file_path'])){
						$shopInfo['pictures'][0]['file_path'] = $brandInfo[0]['file_path'];
					}
				}
				
				$avgRate = round($shopRate["rate_total"],2);
				
				$this->assign('avgRate',$avgRate);
				$this->assign('multipage',$multipage);
				$this->assign('shopRate',$shopRate);
				$this->assign('picstamp', time() . rand(1000,9999));
				 
				$picidx = $piccount = C('REVIEW_PHOTO_COUNT') ? C('REVIEW_PHOTO_COUNT') : 8;
				$picids = array();
				while($picidx-->0){
					$picids[] = ($piccount - $picidx);
				}
				$this->assign('picids', $picids);

				$shopCateDao = D('ShopCate');
				$cate_list = array();
				$sub_cates = array();
				foreach ( $shopInfo['sub_cates'] AS $val ) { $sub_cates[$val['id']] = $val; }
				$cate_id = ( $_REQUEST['cate_id'] && $sub_cates[$_REQUEST['cate_id']] ) ? $_REQUEST['cate_id'] : $shopInfo['sub_cates'][0]['id'];
				$cate_list[] = $sub_cates[$cate_id];

				$parent_id = $sub_cates[$cate_id]['parent_id'];
				while( $parent_id )
				{
					$cate = $shopCateDao->find($parent_id);
					$cate_list[] = $cate;
					$parent_id = $cate['parent_id'];
				}
				$this->assign('cate_list', array_reverse($cate_list));

				$authInfo =  SimpleAcl::getLoginInfo();

				$userShopFavoriteDao = D('UserShopFavorite');
				$where = array();
				$where['user_id'] = $authInfo['id'];
				$where['shop_id'] = $shopInfo['id'];
				if($userShopFavoriteDao->where($where)->limit(1)->select()){
					$shopFavoriteState = 1;
				}else{
					$shopFavoriteState = 0;
				}
				$this->assign('shopFavoriteState', $shopFavoriteState);

				//Hit rate 增加
				$hit_rate = $shopInfo['hit_rate'];				
				$hit_rate++;
				$shopDao->where("id=$shopId")->setField('hit_rate', $hit_rate);
				
				//working hour logic
				$shopTimeDao = D('ShopTime');
				$where = array("shop_id"=>$shopId);
				$working_hour = $shopTimeDao->where($where)->order('start_time,end_time,recurring_week')->select();
				
				$shopTimeDao = D('ShopTime');
				$where = array("shop_id"=>$shopId,"recurring_week "=>0);
				$special_working_hour = $shopTimeDao->where($where)->order('start_day,end_day')->select();
				
				//商戶消息--節目
				$shopEventDao = D('Event');
				$where = array();
				$where["shop_id"]=$shopId;
				$where["end_time"]=array('gt',strtotime(date('Y-m-d 0:0:0')));
				$where["is_audit"]=1;
				$shopEventList =  $shopEventDao->where($where)->order('start_time')->limit(3)->select();
				$this->assign('eventList',$shopEventList);
				
				$output = generateShopWorkTime($shopInfo['work_time'],$working_hour,$special_working_hour);
				
				$this->assign('working_hour',$output);
				//end working hour logic
				
				$this->assign('similar_shops',$this->selectedShop(1,$shopInfo['id'],$cate_list,$shopInfo['sub_location_id']));
				$this->assign('total_review_count',count($shopInfo["reviews"]));

				if (isset($reviewid)){	//read review mode
					//Calculate last and next review id
					$count_review = sizeof($shopInfo['reviews']);
					$reviewKey=-1;
					if($count_review>=1){
						foreach($shopInfo['reviews'] as $rkey=>$reviewItem){
							//echo $review['id'].'<br>';
							if($reviewItem['id'] == $reviewid){
								$reviewKey = $rkey;
							}
						}
					}
					if($reviewKey==-1){			//review not found
						$this->error('沒有找到該評介，您的請求可能來源於您的收藏夾或其他歷史URL，而此評介已被刪除');
						return;
					}elseif($reviewKey==0){		//first review
						$this->assign('nextReviewId',$shopInfo['reviews'][$reviewKey+1]['id']);
					}elseif($reviewKey==$count_review-1){	//last review
						$this->assign('lastReviewId',$shopInfo['reviews'][$reviewKey-1]['id']);
					}else{	//Inclusive
						$this->assign('lastReviewId',$shopInfo['reviews'][$reviewKey-1]['id']);
						$this->assign('nextReviewId',$shopInfo['reviews'][$reviewKey+1]['id']);
					}
					
					//Specify one review
					foreach($shopInfo['reviews'] as $key=>$reviewItem){
						if($reviewItem['id'] == $reviewid){
							unset($shopInfo['reviews']);
							$shopInfo['reviews'][] = $reviewItem;
						}
					}
					
					//Customise Meta Data for facebook share
					$description="";
					$reviewWord="";
					
					if($shopInfo['name']!=""){
						$reviewWord.=$shopInfo['name'];	
					}else{
						$reviewWord.=$shopInfo['english_name'];					
					}
					$reviewWord.=' - ';
					$reviewWord.= $shopInfo['reviews'][0]['title'];
					if(isset($_REQUEST['uname'])){
						$username=$_REQUEST['uname'].' ';
					}
					$stitle = $username.'想同你分享「'.$reviewWord.'」呢篇評介，去OutStreet睇下啦！';
					if ($_REQUEST['like']) {$stitle = $reviewWord;}
					$description.= formatInfoUTF8(strip_tags($shopInfo['reviews'][0]['description']),180);
										
					if(isset($review['pictures'])){	//Setting up thumbnail						
						$item["pictures"] = $review['pictures'];
						$this->assign('itemType','review');
					}elseif(isset($shopInfo['pictures'])){
						$item["pictures"] = $shopInfo['pictures'];
						$this->assign('itemType','shop');
					}elseif(isset($shopInfo['reviews'][0]['user'])){
						$item["pictures"] = array(0=>array('file_path'=>getFace($shopInfo['reviews'][0]['user'])));	
						$this->assign('itemType','user');
					}else{
						unset($item["pictures"]);
						$this->assign('itemType','none');						
					}
					
					$this->assign('item',$item);					
					$this->assign('description',$description);
					$this->assign('stitle',$stitle);
					//End customise Meta Data for facebook share
				}else{	//view shop mode
					//Customise Meta Data for facebook share
					$description="";
					$reviewWord="";
					
					if($shopInfo['name']!=""){
						$reviewWord.=$shopInfo['name'];	
					}else{
						$reviewWord.=$shopInfo['english_name'];					
					}
					if(isset($_REQUEST['uname'])){
						$username=$_REQUEST['uname'].' ';
					}
					$stitle= $username.'想同你分享「'.$reviewWord.'」呢間商戶，去OutStreet睇下啦！';
					if ($_REQUEST['like']) {$stitle = $reviewWord;}
					$description.= formatInfoUTF8(strip_tags($shopInfo['description']),180);

					if(isset($shopInfo['pictures'])){
						$item["pictures"] = $shopInfo['pictures'];
						$this->assign('itemType','shop');
					}else{
						unset($item["pictures"]);
						$this->assign('itemType','none');						
					}
					
					$this->assign('item',$item);					
					$this->assign('description',$description);
					$this->assign('stitle',$stitle);
				}
				
				
				$this->assign('shopInfo',$shopInfo);
				$this->display();
			}else{
				$this->error('沒有找到該商戶，您的請求可能來源於您的收藏夾或其他歷史URL，而此商戶已被刪除');
			}
		}else{
			$this->error('URL非法，缺少要查看的商戶ID');
		}
	 
	}//end view

	public function selshopcate(){
		//獲取Shop分類主目錄列表 壓入模板
		$cate = M('ShopCate')->getField('id,name');
		$level_1 = M('ShopCate')->where(array('level'=>1))->getField('id,parent_id');
		$level_2 = M('ShopCate')->where(array('level'=>2))->getField('id,parent_id');
		$level_3 = M('ShopCate')->where(array('level'=>3))->getField('id,parent_id');

		$cateList = array();
		foreach ( $level_1 AS $level_1_id => $level_1_parent_id )
		{
			if ($cate[ $level_1_id ])
				$cateList[ $level_1_id ][ 'name' ] = $cate[ $level_1_id ];
		}
		foreach ( $level_2 AS $level_2_id => $level_2_parent_id )
		{
			if ($cateList[$level_2_parent_id][ 'name' ] && $cate[ $level_2_id ])
				$cateList[$level_2_parent_id]['item'][ $level_2_id ][ 'name' ] = $cate[ $level_2_id ];
		}
		foreach ( $level_3 AS $level_3_id => $level_3_parent_id )
		{
			if ($cateList[$level_2[$level_3_parent_id]][ 'name' ] && $cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['name'] && $cate[ $level_3_id ])
				$cateList[$level_2[$level_3_parent_id]]['item'][$level_3_parent_id]['item'][ $level_3_id ][ 'name' ] = $cate[ $level_3_id ];
		}
		
		//Shop Cate Ref List
		$shopCateList = D('ShopCate')->field('id,reference_id')->select();
		$refList = array();
		foreach($shopCateList as $key=>$shopCate){
			$cateId = $shopCate['id'];
			$refId = $shopCate['reference_id'];
			if($refId!=0){
				$refList[$cateId][] = $refId;
				$refList[$refId][] = $cateId;
			}
		}
		foreach($refList as $key=>$ref){
			foreach($ref as $cateId){
				foreach($refList[$cateId] as $elem){
					$refList[$key][] = $elem;	
				}
			}
			$refList[$key] = array_unique($refList[$key]);
		}
			
		$this->assign('refList', $refList);
		$this->assign('cateList', $cateList);
		$this->display();
	}

	public function sellocation(){
		//獲取Shop分類主目錄列表 壓入模板
		$locationMainDao = D('LocationMain');
		$locationSubDao = D('LocationSub');
		$locationMainList = $locationMainDao->select();
		 
		foreach($locationMainList as &$locationMain){
			$where = array();
			$where['main_location_id'] = $locationMain['id'];
			$locationMain['locationSubList'] = $locationSubDao->where($where)->select();
		}
		 
		$this->assign('locationMainList', $locationMainList);
		header('Cache-Control: max-age=86400');
		$this->display();
	}

	public function uploadPic(){
		C('SHOW_PAGE_TRACE',false);
		$this->display();
	}

	public function uploadResult(){
		C('SHOW_PAGE_TRACE',false);

		if (isset($_REQUEST['submitBtn'])){

			$eleid = $_REQUEST['eleid'];
			$picstamp = $_REQUEST['picstamp'];
			$description = $_REQUEST['description'];
			$shopid = $_REQUEST['shopid'];
			$from = $_REQUEST['from'];
			$reviewPictureDao = D('ReviewPicture');

			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->allowExts = explode(',',C('UPFILE_ALLOW_IMGEXT'));

			switch(strtoupper($from)){
				case 'REVIEW':
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('REVIEW_UPFILE_PATH').$shopid.'/';
					break;
				case 'REQUEST':	
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('REQUEST_SHOP_UPFILE_PATH');
					break;	
				default:
					$upload->savePath = C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH');
			}

			$upload->saveRule = 'time';
			$upload->thumb = true;
			$upload->thumbMaxWidth = 120;
			$upload->thumbMaxHeight = 120;
			$upload->uploadReplace = true;
	   
			if($upload->upload()) {
				$upinfo = $upload->getUploadFileInfo();
				$upfilepath = $upinfo[0]['savename'];

				$dao = null;
				switch(strtoupper($from)){
					case 'REVIEW':
						$dao = D('ReviewPicture');
						break;
					case 'REQUEST':
						$dao = D('ShopTmpPicture');						
						break;	
					default:
						$dao = D('ShopPicture');
				}

				$upfile = $dao->create();

				$upfile['file_path'] = $upfilepath;
				$upfile['description'] = $description;
				$upfile['picstamp'] = $picstamp;
				switch(strtoupper($from)){
					case 'REVIEW':
						//$upfile['shop_id'] = $shopid;
					break;
					case 'REQUEST':
						$upfile['create_time'] = time();						
					break;
				}

				Import('@.ORG.SimpleAcl');
				$userInfo = SimpleAcl::getLoginInfo();
				$upfile['user_id'] = $userInfo['id'];

				$picid = $dao->add($upfile);
				
				if($from=='shop'){
					$path = C('OUTSTREET_DIR').C('SHOP_UPFILE_PATH');
				}else if($from=='request'){
					$path = C('OUTSTREET_DIR').C('REQUEST_SHOP_UPFILE_PATH');
				}else if($from=='review'){
					$path = C('OUTSTREET_DIR').C('REVIEW_UPFILE_PATH').$shopid;
				}	

				$this->assign('eleid', $eleid);
				$this->assign('upfilepath', $upfilepath);
				$this->assign('description', $description);
				$this->assign('picid', $picid);
				$this->assign('path', $path);
				$this->assign('shopid', $shopid);
				$this->assign('from', $from);				
				$this->assign('picstamp', $picstamp);				
				$this->display('uploadResult');
			} else{
				$upfileerr = $upload->getErrorMsg();
				$upfilepath = '';
				echo '<script text="text/javascript">';
				echo 'parent.setPic("'.$eleid.'", "'.$upfilepath.'", "'.$upfileerr.'","","","'.__URL__.'","'.$from.'","'.$picstamp.'","'.$shopid.'");';			
				echo 'alert("'.$upfileerr.'");';
				echo '</script>';
			}
		}
		 
	}

	public function deletePic(){
		$id = $_REQUEST['id'];
		$from = $_REQUEST['from'];
		 
		Import('@.ORG.SimpleAcl');
		$userInfo = SimpleAcl::getLoginInfo();
		 
		$where = array();
		$where['user_id'] = $userInfo['id'];
		$where['id'] = $id;
		 
		switch(strtoupper($from)){
			case 'REVIEW':
				$dao = D('ReviewPicture');
				break;
			default:
				$dao = D('ShopPicture');
		}
		 
		$picList = $dao->where($where)->select();
		switch(strtoupper($from)){
			case 'REVIEW':
				foreach($picList as $pic){
					unlink(C('OUTSTREET_ROOT').'/'.C('REVIEW_UPFILE_PATH').$pic['shop_id'].'/'.$pic['file_path']);
					unlink(C('OUTSTREET_ROOT').'/'.C('REVIEW_UPFILE_PATH').$pic['shop_id'].'/thumb_'.$pic['file_path']);
				}
				break;
			default:
				foreach($picList as $pic){
					unlink(C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH').$pic['file_path']);
					unlink(C('OUTSTREET_ROOT').'/'.C('SHOP_UPFILE_PATH').'thumb_' . $pic['file_path']);
				}
		}
		 
		$dao->where($where)->delete();
		$this->display();
	}

	public function maps(){
		 
		$id = $_REQUEST['shopid'];
		$cate_id = $_REQUEST['cate_id'];
		 
		$shopDao = D('Shop');
		$mainLocationDao = D('LocationMain');
		$subLocationDao = D('LocationSub');				
			
		$shopInfo = $shopDao->find($id);
		$mainLoc = $mainLocationDao->find($shopInfo['main_location_id']);
		$subLoc = $subLocationDao->find($shopInfo['sub_location_id']);
		$shopInfo['main_location_name']=$mainLoc['name'];
		$shopInfo['sub_location_name']=$subLoc['name'];			
		
		//獲取Shop分類主目錄列表 壓入模板
		while( $cate_id ) {
			$cate = M('ShopCate')->find($cate_id);
			$cate_list[] = $cate;
			$cate_id = $cate['parent_id'];
		}
		$i=0;
		foreach ($cate_list AS $val) {
			$shopCateList[$i] = M('ShopCate')->where(array('parent_id'=>$val['parent_id']))->select();
			$i++;
		}
		
		$this->assign('cate_list', array_reverse($cate_list));
		$this->assign('shopCateList', array_reverse($shopCateList));
		$this->assign('shopInfo',$shopInfo);
		$this->display();
	}

	private function getRateList($shop_cate_id, $rateCountName, $rateListName){		
		$shopDao = D('Shop');
		$ShopCateDao = D('ShopCate');
		
		$shopCateIdList = array();
		$shopCateIdList = $this->getAllSonList($shop_cate_id);

		$where = array();
		$where['cate_id'] = array('IN',$shopCateIdList);
		$where['is_audit'] = 1;
		
		$rateList = $shopDao->join('shop_cate_map on shop_fields.id = shop_cate_map.shop_id')->join('statistics_shop on shop_fields.id = statistics_shop.shop_id')->join('location_sub on shop_fields.sub_location_id = location_sub.id')->field('shop_fields.name as shop_name, shop_fields.english_name as english_name, shop_fields.id as shop_id, location_sub.name as sub_location_name')->relation(true)->where($where)->limit(5)->group('shop_cate_map.shop_id')->order('total_rating, picture_count, favorite_count desc')->select();

		$where = array();
		$where['id'] = array('IN',$shop_cate_id);
		$cateCount =  $ShopCateDao->where($where)->sum('shop_count');
		
		$this->assign("$rateCountName",$cateCount);
		$this->assign("$rateListName",$rateList);
	}

	private function getAllSonList($shop_cate_id){
		$ShopCateDao = D('ShopCate');
		$where = array();
		$where['parent_id'] = array('IN',$shop_cate_id);
		$shopCateList = $ShopCateDao->where($where)->select();
		
		foreach($shopCateList as $elem){
			array_push($shop_cate_id, $elem['id']);			
			$this->getAllSonList($elem['id']);
		}
		return $shop_cate_id;
	}

	private function mapShopCateRef($shopcate){
		//mapping for shopcate reference id
		foreach($shopcate AS $key=>$cateId){
			$referenceId = D('ShopCate')->where("id=$cateId")->getfield('reference_id');
			if($referenceId!=0){
				$shopcate[$key] = $referenceId;		
			}
		}
		return array_unique($shopcate);
	}

	public function selectedShop($type, $shop_id, $sub_cates, $sub_location_id) {
		$shopDao = D('Shop');
		
		foreach($sub_cates as $vo){
			$cate_id[] = $vo['id'];
		}

		switch($type) {
			case 1: //Similar Shop
				$where = array();
				$where['sub_location_id'] = $sub_location_id;
				$where['cate_id'] = array('in',$cate_id);
				$where['is_audit'] = 1;
				$where['shop_id'] = array('neq', $shop_id);

				$shopCount = $shopDao->join('shop_cate_map on shop_fields.id = shop_cate_map.shop_id')->where($where)->count();	
				
				$limit = mt_rand(0,$shopCount-3).',3';
				$shopIdArray = $shopDao->join('shop_cate_map on shop_fields.id = shop_cate_map.shop_id' )->where($where)->group('shop_fields.id')->limit($limit)->select();
				
				foreach($shopIdArray as $val) {
					$selectedShopArray[] = $shopDao->relation(true)->find($val['shop_id']);
				}
			break;
		}

		return $selectedShopArray;
	}

	//****************************************** Start Form Function ******************************************//
	public function add(){
		//************************** Start Get Information From Form **************************//
		$shopInputFields = array();
		$shopInputFields = $this->getRequestValue($_REQUEST,'Shop');
		$keyword 	= trim($shopInputFields['keyword']);
		$cate_ids 	= explode(',',$_REQUEST['cate_ids']);
		//**************************** End Get Information From Form **************************//

		$shopDao = D('Shop');

		if (isset($_REQUEST['submit'])){
			//********************************** Start Checking **********************************//
			if(!$this->chkshopkeyword($keyword)) { $formChkError[] = $this->chkshopkeyword($keyword); }
			//********************************** End Checking ************************************//

			if(count($formChkError) == 0) {
				$shopInfo = $shopDao->create();
				
				if($shopInfo){
					$shopInfoArray = array('parent_shop_id','brand_id','main_location_id','sub_location_id','name','english_name','address','rooms','english_address','keyword','brand_id','parking','description','telephone','sub_telephone','website','email','is_closed','is_renovation','googlemap_lng','googlemap_lat');
					
					$authInfo =  SimpleAcl::getLoginInfo();
					$shopInfo['user_id'] 		= $authInfo['id'];
					$shopInfo['update_time']	= time();
					$shopInfo['is_audit ']		= 1;

					if ($newid = $shopDao->add($shopInfo)){
						$shopInfo	 = array();
						$shopInfo	 = $this->processRequestValue($shopInputFields,$shopInfoArray);
						$where['id'] = $newid;
						$shopDao->where($where)->save($shopInfo);
						$this->saveWorkTime($newid,$_REQUEST,'ShopTime');
						$this->saveCate($newid,$cate_ids,'ShopCateMap');
						
						generateMainLocationStatistics($shopInputFields['main_location_id']);
						generateSubLocationStatistics($shopInputFields['sub_location_id']);
						
						$shopPictureDao = D('ShopPicture');
						$shopPictureDao->where("picstamp=".$_REQUEST['picstamp'])->setField('shop_id', $newid);

						$return = array();
						$return['status'] 	= true;
						$return['message'] = '新增商戶資料成功！';
						$return['url'] =__ROOT__."/shop/view/id/".$newid;
						echo json_encode($return);
					}else{
						$return = array();
						$return['status'] 	= false;
						$return['message'] = '未知錯誤，請聯繫管理員！';
						echo json_encode($return);
					}	
				}else{
					$return = array();
					$return['status'] = false;
					$return['message'] = '數據驗證失敗';
					echo json_encode($return);
				}
			} else {
				$return = array();
				$return['status'] = false;
				$return['keywordWarning'] = $formChkError[0];
				echo json_encode($return);
			}
		} else {
			$brandDao = D('Brand');
			$mainLocationDao = D('LocationMain');

			//獲取Brand列表 壓入模
			$brandList = $brandDao->select();
			$this->assign('brandList', $brandList);

			//獲取Location分類主目錄列表 壓入模板
			$mainLocationList = $mainLocationDao->select();
			$this->assign('mainLocationList', $mainLocationList);
			$this->assign('picstamp', time() . rand(1000,9999));
			$common_shop_form = $this->fetch('common_shop_form');
			$this->assign('commonShopForm',$common_shop_form);
			$this->display();
		}
	}

	public function edit(){
		//************************** Start Get Information From Form **************************//
		$shopInputFields = array();
		$shopInputFields = $this->getRequestValue($_REQUEST,'Shop');
		$id 		= $shopInputFields['id'];
		$keyword 	= trim($shopInputFields['keyword']);
		$cate_ids 	= explode(',',$_REQUEST['cate_ids']);
		//**************************** End Get Information From Form **************************//

		//********************************** Start Check If The Shop Exist **********************************//
		$shopDao = D('Shop');
		$checkInfo = $shopDao->relation(true)->find($id);

		if(isset($id)){
			if(isset($checkInfo) && $checkInfo['is_audit']==1){
				if (isset($_REQUEST['submit'])){
					//********************************** Start Checking **********************************//
					if(!$this->chkshopkeyword($keyword)) { $formChkError[] = $this->chkshopkeyword($keyword); }
					//********************************** End Checking **********************************//

					if(count($formChkError) == 0) {
						$shopInfoArray = array('parent_shop_id','brand_id','main_location_id','sub_location_id','name','english_name','address','rooms','english_address','keyword','parking','description','telephone','sub_telephone','website','email','is_closed','is_renovation','googlemap_lng','googlemap_lat');
						
						$shopInfo					= $this->processRequestValue($shopInputFields,$shopInfoArray);
						$shopInfo['update_time']	= time();
						$where 						= array();
						$where['id'] 				= $id;
						$shopDao->where($where)->save($shopInfo);

						$this->saveWorkTime($id,$_REQUEST,'ShopTime');
						$this->saveCate($id,$cate_ids,'ShopCateMap');
						generateMainLocationStatistics($shopInputFields['main_location_id']);
						generateSubLocationStatistics($shopInputFields['sub_location_id']);
	
						$shopPictureDao = D('ShopPicture');
						$shopPictureDao->where("picstamp=".$_REQUEST['picstamp'])->setField('shop_id', $id);

						$return = array();
						$return['status'] 	= true;
						$return['message'] = '更改商戶資料成功！'.$shopInfo['id'].$shopInfo['sub_telephone'];
						$return['url'] =__ROOT__."/shop/view/id/".$id;
						echo json_encode($return);
					} else {
						$return = array();
						$return['status'] = false;
						$return['keywordWarning'] = $formChkError[0];
						echo json_encode($return);
					}
				}else{
					$shopInfo = $shopDao->relation(true)->find($id);
					$this->assign('shopInfo', $shopInfo);
		
					$newShopTime = array();
					$shopTime = M('ShopTime')->where( array( 'shop_id' => $id ) )->select();
					foreach ( $shopTime as $val ){
						$newShopTime[$val['recurring_week']] = $val;
					}
					$this->assign('shopTime', $newShopTime);
		
					$brandDao = D('Brand');
					$mainLocationDao = D('LocationMain');
					$subLocationDao = D('LocationSub');
		
					//獲取Brand列表 壓入模板
					$brandList = $brandDao->select();
					$this->assign('brandList', $brandList);
		
					//獲取Location分類目錄列表 壓入模板
					$mainLocationList = $mainLocationDao->select();
					$this->assign('mainLocationList', $mainLocationList);
					$subLocationList = $subLocationDao->select();
					$this->assign('subLocationList',$subLocationList);

					$common_shop_form = $this->fetch('common_shop_form');
					$this->assign('commonShopForm',$common_shop_form);
					$this->display();
				}
			} else {
				$this->error('沒有找到該商戶，您的請求可能來源於您的收藏夾或其他歷史URL，而此商戶已被刪除');
			}
		} else {
			$this->error('URL非法，缺少要查看的商戶ID');
		}
		//********************************** End Check If The Shop Exist **********************************//
	}

	public function request_add_shop(){
		//************************** Start Get Information From Form **************************//
		$shopInputFields = array();
		$shopInputFields = $this->getRequestValue($_REQUEST,'Shop');
		$keyword 	= trim($shopInputFields['keyword']);
		$cate_ids 	= explode(',',$_REQUEST['cate_ids']);
		//**************************** End Get Information From Form **************************//

		$shopDao = D('ShopTmpFields');

		if (isset($_REQUEST['submit'])){
			//********************************** Start Checking **********************************//
			if(!$this->chkshopkeyword($keyword)) { $formChkError[] = $this->chkshopkeyword($keyword); }
			//********************************** End Checking ************************************//
			
			if(count($formChkError) == 0) {
				$shopInfo = $shopDao->create();
				
				if($shopInfo){
					$shopInfoArray = array('parent_shop_id','brand_id','main_location_id','sub_location_id','name','english_name','address','rooms','english_address','keyword','brand_id','parking','description','telephone','sub_telephone','website','email','is_closed','is_renovation','googlemap_lng','googlemap_lat');

					if($_REQUEST['is_closed']){
						$shopInfo['is_closed']= 1;
					}
					if($_REQUEST['is_renovation']){
						$shopInfo['is_renovation']= 1;
					}
					$authInfo =  SimpleAcl::getLoginInfo();
					$shopInfo['user_id'] 		= $authInfo['id'];
					$shopInfo['create_time']	= time();

					if ($newid = $shopDao->add($shopInfo)){
						$shopInfo	 = array();
						$shopInfo	 = $this->processRequestValue($shopInputFields,$shopInfoArray);
						$where['id'] = $newid;
						$shopDao->where($where)->save($shopInfo);
						$this->saveWorkTime($newid,$_REQUEST,'ShopTmpTime');
						$this->saveCate($newid,$cate_ids,'ShopTmpCateMap');

						$return = array();
						$return['status'] 	= true;
						if($_REQUEST['owner']=='owner'){
							$return['url'] =__URL__."/shop_provide_info/shoppage/4/shop_tmp_fields_id/".$shop_tmp_fields_id;
							$return['message'] ='多謝你提交新商戶資料！我們在核對資料之後，會及時將新商戶發布到網站上，並以email通知你。多謝你的支持！現在請到我是店主進行登記！';
						}else{
							$return['url'] =__ROOT__;
							$return['message'] ='多謝你提交新商戶資料！我們在核對資料之後，會及時將新商戶發布到網站上，並以email通知你。多謝你的支持！';
						}
						echo json_encode($return);
					}else{
						$return = array();
						$return['status'] 	= false;
						$return['message'] = '未知錯誤，請聯繫管理員！';
						echo json_encode($return);
					}	
				}else{
					$return = array();
					$return['status'] = false;
					$return['message'] = '數據驗證失敗';
					echo json_encode($return);
				}
			} else {
				$return = array();
				$return['status'] = false;
				$return['keywordWarning'] = $formChkError[0];
				echo json_encode($return);
			}
		} else {
			$brandDao = D('Brand');
			$mainLocationDao = D('LocationMain');

			//獲取Brand列表 壓入模
			$brandList = $brandDao->select();
			$this->assign('brandList', $brandList);

			//獲取Location分類主目錄列表 壓入模板
			$mainLocationList = $mainLocationDao->select();
			$this->assign('mainLocationList', $mainLocationList);
			
			$common_shop_form = $this->fetch('common_shop_form');
			$this->assign('commonShopForm',$common_shop_form);
			$this->display();
		}
	}

	public function request_edit_shop(){
		//************************** Start Get Information From Form **************************//
		$shopInputFields = array();
		$shopInputFields = $this->getRequestValue($_REQUEST,'Shop');
		$id 		= $shopInputFields['id'];
		$keyword 	= trim($shopInputFields['keyword']);
		$cate_ids 	= explode(',',$_REQUEST['cate_ids']);
		//**************************** End Get Information From Form **************************//

		//********************************** Start Check If The Shop Exist **********************************//
		$shopDao = D('Shop');
		$shopTmpDao = D('ShopTmpFields');
		$checkInfo = $shopDao->relation(true)->find($id);

		if(isset($id)){
			if(isset($checkInfo) && $checkInfo['is_audit']==1){
				if (isset($_REQUEST['submit'])){
					import ( '@.ORG.SimpleAcl' );
					$userInfo =  SimpleAcl::getLoginInfo();
					$data = $_REQUEST;

					//********************************** Start Checking **********************************//
					if(!$this->chkshopkeyword($keyword)) { $formChkError[] = $this->chkshopkeyword($keyword); }
					//********************************** End Checking **********************************//

					if(count($formChkError) == 0) {
						$shopCompareArray = array(
							'brand_id' => 'int',
							'parent_shop_id' => 'int',
							'main_location_id'=>'int',
							'sub_location_id' => 'int',
							'name' => 'string',
							'english_name' => 'string',
							'address' => 'string',
							'rooms' => 'string',
							'googlemap_lat' => 'string',
							'googlemap_lng' => 'string',
							'english_address' => 'string',
							'parking' => 'string',
							'description' => 'string',
							'telephone' => 'string',
							'sub_telephone' => 'string',
							'website' => 'string',
							'email' => 'string',
							'keyword'=>'string',
							'is_closed' => 'bool',				
							'is_renovation' => 'bool' );
						
						$shop = M('ShopFields')->where( array( 'id' => $id ) )->find();
			
						$savedata = array();
						foreach( $shopCompareArray as $name => $type ) {
							settype( $data[$name], $type );
							settype( $shop[$name], $type );
							if ( trim(preg_replace('/\n|\r/','',html_entity_decode($data[$name]))) != trim(preg_replace('/\n|\r/','',html_entity_decode($shop[$name]))) )
								{ $savedata[$name] = $data[$name]; }
						}

						if($savedata['parent_shop_id']||$savedata['address']||$savedata['main_location_id']||$savedata['sub_location_id']||$savedata['googlemap_lat']||$savedata['googlemap_lng']){				
							 $savedata['parent_shop_id']	= 	$data['parent_shop_id'];
							 $savedata['address']			= 	$data['address'];
							 $savedata['main_location_id']	= 	$data['main_location_id'];
							 $savedata['sub_location_id']	= 	$data['sub_location_id'];
							 $savedata['googlemap_lat']		=	$data['googlemap_lat'];
							 $savedata['googlemap_lng']		=	$data['googlemap_lng'];
						}
						
						$affectedFields = array_keys($savedata);
						
						$savedata['shop_id'] 		= $id;
						$savedata['create_time']	= time();
						$savedata['user_id'] 		= $userInfo[ 'id' ];
						$savedata['status'] 		= 'edit';
						
						$shopTime = M('ShopTime')->where( array( 'shop_id' => $id ) )->select();
						$shopTimeKey = array();
						foreach( $shopTime as $val ) { $shopTimeKey[] = $val['start_time'] . ':' . $val['end_time'] . ':' . $val['recurring_week']; }
		
						$shopTimeInputKey = array();
						for( $i=0; $i<8; $i++ )
						{
							if ( @$_REQUEST['work_dayoff_'.$i] != 1 &&  @$_REQUEST['work_start_time_'.$i] && @$_REQUEST['work_end_time_'.$i] )
								{ $shopTimeInputKey[] = $_REQUEST['work_start_time_'.$i] . ':' . $_REQUEST['work_end_time_'.$i] . ':' . ($i+1);  }
						}
						if ( count($shopTimeKey) >= count($shopTimeInputKey) && count(array_diff( $shopTimeKey, $shopTimeInputKey )) )
							{ $affectedFields[] = 'shoptime'; }
						elseif ( count($shopTimeKey) < count($shopTimeInputKey) && count(array_diff( $shopTimeInputKey, $shopTimeKey )) )
							{ $affectedFields[] = 'shoptime'; }
		
						$shopCate = M('ShopCateMap')->where( array( 'shop_id' => $id ) )->select();
						$shopCateId = array();
						foreach( $shopCate AS $val ) { $shopCateId[] = $val['cate_id']; }
						if ( count($shopCateId) >= count($cate_ids) && count(array_diff( $shopCateId, $cate_ids )) )
							{ $affectedFields[] = 'shopcate'; }
						elseif ( count($shopCateId) < count($cate_ids) && count(array_diff( $cate_ids, $shopCateId )) )
							{ $affectedFields[] = 'shopcate'; }
					
						if ( count($affectedFields) ) {
							$savedata[ 'affectedFields' ] = serialize($affectedFields);
							if($shopTmpDao->add($savedata)){
								$shop_tmp_fields_id = $shopDao->getLastInsID();
				
								if ( in_array('shoptime',$affectedFields) ) {
									$this->saveWorkTime($id,$_REQUEST,'ShopTmpTime',$shop_tmp_fields_id);
								}
	
								if ( in_array('shopcate',$affectedFields) ) {
									$this->saveCate($id,$cate_ids,'ShopCateMap',$shop_tmp_fields_id);
								}
							}
						}

						$return = array();
						$return['status'] 	= true;
						$return['message'] 	= '多謝你提交商戶最新資料！我們在核對資料之後，將會及時更新網站內容，並以email通知你。多謝你的支持！';
						$return['url'] 		= __ROOT__."/shop/view/id/".$id;
						echo json_encode($return);
					} else {
						$return = array();
						$return['status'] = false;
						$return['keywordWarning'] = $formChkError[0];
						echo json_encode($return);
					}
				}else{
					$id = $_REQUEST['id'];
					$shopDao = D('Shop');
		
					$shopInfo = $shopDao->relation(true)->find($id);
					$this->assign('shopInfo', $shopInfo);
		
					$newShopTime = array();
					$shopTime = M('ShopTime')->where( array( 'shop_id' => $id ) )->select();
					foreach ( $shopTime as $val ){
						$newShopTime[$val['recurring_week']] = $val;
					}
					$this->assign('shopTime', $newShopTime);
		
					$brandDao = D('Brand');
					$mainLocationDao = D('LocationMain');
					$subLocationDao = D('LocationSub');
		
					//獲取Brand列表 壓入模板
					$brandList = $brandDao->select();
					$this->assign('brandList', $brandList);

					//獲取Location分類主目錄列表 壓入模板
					$mainLocationList = $mainLocationDao->select();
					$this->assign('mainLocationList', $mainLocationList);
					$subLocationList = $subLocationDao->select();
					$this->assign('subLocationList', $subLocationList);
					
					$common_shop_form = $this->fetch('common_shop_form');
					$this->assign('commonShopForm',$common_shop_form);
					$this->display();
				}
			} else {
				$this->error('沒有找到該商戶，您的請求可能來源於您的收藏夾或其他歷史URL，而此商戶已被刪除');
			}
		} else {
			$this->error('URL非法，缺少要查看的商戶ID');
		}
		//********************************** End Check If The Shop Exist **********************************//
	}

	public function request_provide_photo(){
		$id = $_REQUEST['id'];
		$this->assign('picstamp', time() . rand(1000,9999));
		$this->assign('id', $id);
		$this->display();
	}

	public function provide_photo(){
		$id = $_REQUEST['id'];
		$this->assign('picstamp', time() . rand(1000,9999));
		$this->assign('id', $id);
		$this->display();
	}

	public function request_submit_provide_photo(){
		$shop_id = $_REQUEST['id'];
		$picstamp = $_REQUEST['picstamp'];
		
		$shopPictureDao = D('ShopTmpPicture');
		$picstamp_count = $shopPictureDao->where("picstamp=$picstamp")->count();
		if($picstamp_count==0){ 
			echo json_encode( array( 'success'=> 0,'message'=>'請先上載相片 然後點擊提交' ) );
		}else{
		
			if($_REQUEST['confirm']=="confirm"){
				
				$userInfo = SimpleAcl::getLoginInfo();
				
				if($userInfo){ 
					$shopPictureDao->where("picstamp=$picstamp")->setField('user_id', $userInfo['id']);
					$shopPictureDao->where("picstamp=$picstamp")->setField('shop_id', $shop_id);
					
					$return = array();
					$return['success'] =1;
					$return['message'] ='多謝你提供商戶相片！我們審核相片後會及時更新網站內容，並以email通知你。OutStreet的進一步完善，有賴你的支持！';
					$return['url'] =__ROOT__."/shop/view/id/".$shop_id;
					echo json_encode($return);
					
				}else if($_REQUEST['non_member_name'] && $_REQUEST['non_member_email']){
					$shopPictureDao->where("picstamp=$picstamp")->setField('provider_name', $_REQUEST['non_member_name']);
					$shopPictureDao->where("picstamp=$picstamp")->setField('provider_email', $_REQUEST['non_member_email']);
					$shopPictureDao->where("picstamp=$picstamp")->setField('shop_id', $shop_id);
					
					$return = array();
					$return['success'] =1;
					$return['message'] ='多謝你提供商戶相片！我們審核相片後會及時更新網站內容，並以email通知你。OutStreet的進一步完善，有賴你的支持！';
					$return['url'] =__ROOT__."/shop/view/id/".$shop_id;
					echo json_encode($return);
					
				}else echo json_encode( array( 'success'=> 0,'message' => '請輸入你的會員E-mail和密碼 或 輸入你的名稱和電郵' ) );
				
			}else echo json_encode( array( 'success'=> 0,'message'=>'請確定你所提交相片並不含任何侵犯版權之內容!' ) );
		}	
	}

	public function request_own_shop(){
		$ShopDao = D('Shop');			
		$ShopTmpDao = D('ShopTmpFields');
		
		if($_REQUEST['shop_tmp_fields_id']){
			$shop_tmp_fields_id = $_REQUEST['shop_tmp_fields_id'];
			$shopInfo = $ShopTmpDao->where('id='.$shop_tmp_fields_id)->find();
		}else{
			$id = $_REQUEST['id'];
			$shopInfo = $ShopDao->where('id='.$id)->find();
		}

		if($_REQUEST['submit']){
			
			$shopname = $_REQUEST['shopName'];
			$contactPerson = $_REQUEST['contactPerson'];
			$email = $_REQUEST['email'];
			$telephone = $_REQUEST['telephone'];

			if(!empty($_REQUEST['from'])){
				$from = $_REQUEST['from'];
			}else{
				$from =0;
			}
					
			/*
			$company_name = trim($_REQUEST['company']);
			$position = trim($_REQUEST['post']);
			$phone = trim($_REQUEST['telephone']);
			$email = trim($_REQUEST['email']);
			$password = md5($_REQUEST['password']);

			$User = D('User');
			$where = array();
			$where['email'] = $email;
			$where['password'] = $password;
			$count = $User->where($where)->count();
			*/
			
			/*if ( $count ==0 ){ 
				$return = array();
				$return['success'] = 0;
				if(!$authInfo = SimpleAcl::getLoginInfo()) $return['url'] =C('OUTSTREET_DIR')."/user/register";
				
				echo json_encode($return);
			}else {*/
				$ShopTmpOwner = D('ShopTmpOwner');
				
				$data = array();
				$return = array();

				/*
				$user = $User->where($where)->find();
				$user_id = $user['id'];
				$nick_name=$user['nick_name'];
				$mail_address=$user['email'];

				$data['company_name'] = $company_name;
				$data['shop_id'] = $id;
				$data['user_id'] = $user_id;
				$data['position'] = $position;
				$data['phone'] = $phone;
				$data['is_audit'] = 0;
				*/

				$data['shop_id'] 		= $id;
				$data['shop_name'] 		= $shopname;
				$data['contact_person'] = $contactPerson;
				$data['email'] 			= $email;
				$data['telephone'] 		= $telephone;
				$data['is_audit'] 		= 0;
				$data['create_time'] 	= time();
				$data['from'] 	= $from;
				$ShopTmpOwner->data($data)->add();

				/*//email to user
				$caption = '感謝你登記成為店主';
				$this->assign('nickname',$contactPerson);
				$this->assign('caption',$caption);				
				$this->assign('link',C('OUTSTREET_DIR') . '/shop/view/id/'.$shop_id);
				$body = $this->fetch('email_request_shop_owner');
				if($email != '') {
					$this->smtp_mail_log($email,$caption,$body,'',2,1);
				}
				*/
				
				//email to admin
				$caption = '登記成為店主記錄';
				if(trim($shopInfo['name'])!=''){
					$this->assign('shop',$shopInfo['name']);
				}else{
					$this->assign('shop',$shopInfo['english_name']);
				}
				$this->assign('shopName',$shopname);
				$this->assign('contactPerson',$contactPerson);
				$this->assign('email',$email);	
				$this->assign('telephone',$telephone);
				if($from==0){
					$this->assign('source','商戶');					
				}else{
					$this->assign('source','節目');					
				}
				$this->assign('caption',$caption);				
				$body = $this->fetch('email_request_admin_shop_owner');
				//$this->smtp_mail_log('info@outstreet.com.hk',$caption,$body,'',2,1);
				$this->smtp_mail_log('tammy@outstreet.com.hk',$caption,$body,'',2,1);
				
				if($shop_tmp_fields_id!=0){
					$return['success'] =1;
					$return['url'] =__ROOT__;
				}else if($id!=0){
					$return['success'] =2;
					$return['url'] =__ROOT__."/shop/view/id/".$id;
				}else{
					$return['success'] =2;
					$return['url'] =__ROOT__;
				}
				echo json_encode($return);
			//}
		}else{
			//user info
			$userArr = SimpleAcl::getLoginInfo();
			$User = D('User');
			$where = array();
			$where['id'] = $userArr['id'];
			$userInfo = $User->where($where)->find();
						
			$db_shopName ='';
			$db_contactPerson = '';
			$db_email = '';
			$db_telephone = '';
			if(!empty($userInfo)){
				$db_contactPerson = $userInfo['nick_name'];
				$db_email = $userInfo['email'];
				$db_telephone = $userInfo['telephone'];
			}	
			if(!empty($shopInfo)){
				if(trim($shopInfo['name'])!=''){
					$db_shopName = $shopInfo['name'];
				}else{
					$db_shopName = $shopInfo['english_name'];
				}
				if(trim($db_email)=='' && trim($shopInfo['email'])!=''){
					$db_email = $shopInfo['email'];
				}
				/*if(trim($db_telephone)=='' && $shopInfo['telephone']!=0){
					$db_telephone = $shopInfo['telephone'];
				}*/
			}
			
			$this->assign('shopName', trim($db_shopName));
			$this->assign('contactPerson', trim($db_contactPerson));
			$this->assign('email', trim($db_email));
			$this->assign('telephone', trim($db_telephone));
			$this->display();
		}
	}
	//************************************************ End Form Function ******************************************//
	
	//****************************************** Start Share Form Function ****************************************//
	public function chkshopkeyword($keyword){
		if($keyword!=''){
			$keyword_error_type = 0;
			$keywordArr = explode(',',$keyword);
			foreach($keywordArr as $key=>$keyword){
				if(trim($keyword)==""){
					$keyword_error_type = 1;	//Blank keyword
				}elseif(hasSymbol(trim($keyword))){
					$keyword_error_type = 2;	//Invalid Characters			
				}
			}
			
			switch($keyword_error_type){
				case 1:
					return '關鍵字中不能為空!';
				break;	
				case 2:
					return '關鍵字中不允許使用符號!';
				break;					
			}
		}
		
		return true;
	}

	public function saveCate($id,$cate_ids,$destination,$shop_tmp_fields_id=''){
		$where = array();
		$where['shop_id'] = $id;
		if($shop_tmp_fields_id != '') { $where['shop_tmp_fields_id'] = $shop_tmp_fields_id; }
		$existCateIds = M($destination)->where($where)->select();
						
		$cateIdsList = array();
		foreach($existCateIds as $elem){
			array_push($cateIdsList,$elem['cate_id']);
		}
		generateShopCateStatByShopID($id,'delete');
		M($destination)->where( array( 'shop_id' => $id ) )->delete();
		
		$cate_ids = $this->mapShopCateRef($cate_ids);
		foreach ( $cate_ids as $cate_id ) {
			$cate_iddsf = $cate_id;
			M($destination)->data( array( 'cate_id'=>$cate_id, 'shop_id'=>$id ) )->add(); 
		}
		generateShopCateStatByShopID($id,'add');
	}

	public function saveWorkTime($id,$requestValue,$destination,$shop_tmp_fields_id=''){
		$has_work_time 	= 0;

		$where = array();
		$where['shop_id'] = $id;
		if($shop_tmp_fields_id != '') { $where['shop_tmp_fields_id'] = $shop_tmp_fields_id; }
		M($destination)->where($where)->delete();
		for( $i=0; $i<8; $i++ ){
			if($requestValue['work_dayoff_'.$i] == 1){
				// Do not enter record
			} else {
				if ($requestValue['work_start_time_'.$i] && $requestValue['work_end_time_'.$i] ) {
					$has_work_time = 1;
					M($destination)->data( array(
						'shop_id' => $id,
						'start_time' => $requestValue['work_start_time_'.$i],
						'end_time' => $requestValue['work_end_time_'.$i],
						'recurring_week' => $i+1 ) )->add();
				}
			}
		}

		$shopDao = D('Shop');
		$shopInfo['work_time']		= $has_work_time;
		$shopDao->where(array( 'id'=>$id ))->save($shopInfo);
	}

	public function getRequestValue($requestValue, $daoName){
		Import('ORG.Util.Input');
		$Dao = D($daoName);
		$DaoFields = $Dao->getDbFields();

		foreach($DaoFields as $key) {
			$returnValue[$key] = Input::getVar($requestValue[$key]);
		}
		
		return $returnValue;
	}

	public function processRequestValue($shopInputFields,$shopInfoArray) {
		foreach($shopInfoArray as $key) {
			$returnValue[$key] = $shopInputFields[$key];
		}
		
		return $returnValue;
	}

	public function shop_error(){
		$error = $_REQUEST['error'];
		$this->assign('error', $error);
		$this->display();
	}
	//****************************************** End Share Form Function ******************************************//

}

?>