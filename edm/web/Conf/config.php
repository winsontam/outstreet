<?php

if (!defined('THINK_PATH')) exit();

return array(

	'OUTSTREET_ROOT'=>'../',
	'OUTSTREET_DIR'=>'http://www.outstreet.com.hk',
	'FB_SHARE_URL'=>'http://beta.outstreet.com.hk',

	'DB_TYPE'=>'mysql', //数据库类型
	'DB_HOST'=>'localhost', //服务器地址
	'DB_NAME'=>'outweb_main', //数据库名
	'DB_USER'=>'outweb_main', //用户名
	'DB_PWD'=>'out8989', //密码
	'DB_PORT'=>3306, //端口
	'DB_PREFIX'=>'', //数据库表前缀
	'URL_MODE'=>2,	//URLREWRITE模式
	'TEMPLATE_SUFFIX'=>'.html',	//模板文件後綴
	'URL_HTML_SUFFIX'=>'.html',	//偽靜態後綴
	'URL_CASE_INSENSITIVE'=>true,	//URL是否區分大小寫
	'PAGE_ROLLPAGE'=>5,
	'PAGE_LISTROWS'=>10,	//每頁數量
	'APP_DEBUG'=>false,	//DEBUG模式是否開啟
	'TMPL_ACTION_SUCCESS'=>'Public:message', //正確信息提示頁面
	'TMPL_ACTION_ERROR'=>'Public:message',	//錯誤信息提示頁面
	'TMPL_CACHE_TIME' => 1,
	'EMAIL_ENCRYPT_KEY'=>'91#25D1cC2G/*$9**/$$vkl:{rfgtv8}4vqBT62^gg7r',

	'TOKEN_ON'=>false,
	'URL_ROUTER_ON'=>true, //URL路由開啟

//@.ORG.Simple用到的配置 STAR
	'USER_AUTH_ON'=>true,	//是否開啟身份認證
	'USER_AUTH_MODEL'=>'User',
	'USER_AUTH_GATEWAY'=>'/User/login/',	//身份認證的登錄頁面
	'REQUIRE_AUTH_ACTION'=>'add,edit,mypanel,changePsd,changeEmail,addStepOne,getOwner,submitMessage,joinShop2Favorite',	//需要認證的操作  啟用該項 其他操作均無需認證
//'NOT_AUTH_ACTION'=>'index,view,login,register',	//無需認證的操作   和需要認證的操作 啟用一項即可
//@.ORG.Simple用到的配置 END

//common.php的smtp_mail函數的配置 START
	'SMTP_SERVER'=>'mail.outstreet.com.hk',
	'SMTP_USERNAME'=>'admin@outstreet.com.hk',
	'SMTP_PASSWORD'=>'outstreet-1234',
	'SMTP_FROM'=>'admin@outstreet.com.hk',
	'SMTP_FROMNAME'=>'OutStreet',
//common.php的smtp_mail函數的配置 END

//ShopAction.class.php中用到的參數	START
	'SHOP_PHOTO_COUNT'=>8,		//創建商鋪時所能添加的圖片數量
	'EVENT_PHOTO_COUNT'=>6,		//創建節目時所能添加的圖片數量
	'REVIEW_PHOTO_COUNT'=>6,		//評論時所能添加的圖片數量
//ShopAction.class.php中用到的參數	END

//ShopAction.class.php的uploadPic中用到的參數 START
	'REVIEW_UPFILE_PATH'=> '/Public/uploadimages/review/',
	'SHOP_UPFILE_PATH'=> '/Public/uploadimages/shop/',
	'EVENT_UPFILE_PATH'=> '/Public/uploadimages/event/',
	'EVENT_REVIEW_UPFILE_PATH'=> '/Public/uploadimages/event_review/',
	'REQUEST_SHOP_UPFILE_PATH'=> '/Public/uploadimages/request/shop/',
	'REQUEST_EVENT_UPFILE_PATH'=> '/Public/uploadimages/request/event/',
	'PROMO_UPFILE_PATH'=> '/Public/uploadimages/promo/',
	'USERFACE_UPFILE_PATH'=> '/Public/uploadimages/userface/',
	'OTPROMO_UPFILE_PATH'=> '/Public/uploadimages/outstreetpromo/',
	'POSTER_HTML_UPFILE_PATH'=> '/Public/uploadimages/outstreetpromo/poster_html/',
	'PROMOTION_UPFILE_PATH'=> '/Public/uploadimages/promotion/',
	'PROMOTION_REVIEW_UPFILE_PATH'=> '/Public/uploadimages/promotion_review/',
//關於POSTER的變數 END

//ForumAction.class.php的upfile用到的參數
	'FORUM_UPFILE_PATH'=> '/Public/uploadimages/forum/',
//end

//SA模塊用到的配置 START
	'DB_BACKUP_PATH'=> 'backup/',		//備份文件建立的路徑
//SA模塊用到的配置 END

	'UPFILE_ALLOW_IMGEXT'=>'jpg,jpeg,gif,png',
	'COOKIE_EXPIRE'=> 3600 * 24,	//COOKIE有效期

//EventAction.class.php的getShopByKeyword中用到的參數eventAdvShop
	'EVENTADVSHOP'=>array('76','77','78','79','80','81','82','83','84'),

//Security Log
	'TIME_VISIT_TOLERATE'=>2,	//in second
	'COUNT_VISIT_TOLERATE'=>3	//in time

);

?>