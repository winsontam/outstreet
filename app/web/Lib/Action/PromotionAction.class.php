<?php

class PromotionAction extends BaseAction
{

	public function search()
	{

		$shop_id = (int) $_REQUEST[ 'shop_id' ];

		$type1 = $_REQUEST[ 'type' ] ? $_REQUEST[ 'type' ] : 1;

		$query = (string) $_REQUEST[ 'query' ];

		if ( $_REQUEST[ 'category_id' ] )
		{
			$category_ids = (array) $_REQUEST[ 'category_id' ];
		}
		else
		{
			$category_ids = (array) $_REQUEST[ 'category_ids' ];
		}

		$category_ids = $this->getsubcateids( $category_ids );
		if ( $brand_id )
		{
			$where[ 'a.brand_id' ] = $brand_id;
		}

		$main_location_id = (int) $_REQUEST[ 'main_location_id' ];

		$sub_location_ids = (array) $_REQUEST[ 'sub_location_ids' ];

		$brand_id = (int) $_REQUEST[ 'brand_id' ];

		$googlemap_lat = (float) $_REQUEST[ 'googlemap_lat' ];

		$googlemap_lng = (float) $_REQUEST[ 'googlemap_lng' ];

		$distance = (int) $_REQUEST[ 'distance' ];

		$price_range = (int) $_REQUEST[ 'price_range' ];

		$page = (int) $_REQUEST[ 'page' ];

		$category_name = D( 'ShopCate' )->where( 'id=' . $category_id )->getField( 'name' );

		$where = array( );

		$where[ 'a.is_audit' ] = 1;

		$where[ 'a.type1' ] = $type1;

		if ( $main_location_id )
		{
			$where[ 'e.main_location_id' ] = $main_location_id;
		}

		if ( $sub_location_ids )
		{
			$where[ 'e.sub_location_id' ] = array( 'in', $sub_location_ids );
		}

		if ( $category_ids )
		{
			$where[ 'b.cate_id' ] = array( 'in', $category_ids );
		}

		if ( $price_range )
		{

			$where[ 'a.price_range' ] = array( 'in', $price_range );
		}

		/* user GPS distance search */

		if ( $main_location_id || $sub_location_ids )
		{

			$distance = '';
		}

		if ( $googlemap_lat && $googlemap_lng && floor( $googlemap_lat ) == 22 && $distance )
		{

			if ( $where[ '_string' ] )
			{
				$where[ '_string' ] .= ' AND ';
			}
			else
			{
				$where[ '_string' ] = '';
			}

			$where[ '_string' ] .= '( 6371 * acos( cos( radians(' . $googlemap_lat . ') ) * cos( radians( e.googlemap_lat ) ) * cos( radians( e.googlemap_lng ) - radians(' . $googlemap_lng . ') ) + sin( radians(' . $googlemap_lat . ') ) * sin( radians( e.googlemap_lat ) ) ) ) <= ' . $distance . '/1000';
		}

		/* user GPS distance search */

		if ( $shop_id )
		{

			if ( $where[ '_string' ] )
			{
				$where[ '_string' ] .= ' AND ';
			}
			else
			{
				$where[ '_string' ] = '';
			}

			$where[ '_string' ] .= '(e.id ="' . $shop_id . '" OR e.parent_shop_id="' . $shop_id . '")';
		}

		/* query */

		$countQuerys = array( );

		if ( $query )
		{

			$terms = $query;

			$allterms = array( );

			if ( preg_match_all( '/\w+/', $terms, $m ) )
			{

				foreach ( $m[ 0 ] AS $t )
				{

					$terms = str_replace( $t, '', $terms );

					$allterms[ $t ] = 'e';
				}
			}

			$mterms = array( );

			mb_regex_encoding( 'UTF-8' );

			mb_ereg_search_init( $terms, '\w+' );

			while ( ( $m = mb_ereg_search_regs() ) )
			{
				$allterms[ $m[ 0 ] ] = 'm';
			}



			$suggestWhere = array( );

			foreach ( $allterms AS $term => $type )
			{

				$suggestWhere[ 'keyword' ][ ] = array( 'like', $term );
			}

			$suggestWhere[ 'keyword' ][ ] = 'or';

			$suggestKeywords = M( 'KeywordTranslate' )->where( $suggestWhere )->select();

			foreach ( $suggestKeywords AS $val )
			{

				$keywords = explode( ',', $val[ 'suggest_keywords' ] );

				foreach ( $keywords AS $k )
				{
					if ( $k )
					{
						$allterms[ $k ] = 's';
					}
				}
			}

			$searchcols = array(
				'a.name' => 10
			);

			$likecolumn = '[likecols]';

			$orQuerys = array( );

			foreach ( $allterms AS $term => $type )
			{

				if ( $type == 'e' || $type == 's' )
				{

					//$orQuerys[] = $likecolumn . ' REGEXP "[[:<:]]' . $term . '.{0,3}[[:>:]]"';

					$orQuerys[ ] = $likecolumn . ' REGEXP "' . $term . '"';
				}
				elseif ( $type == 'm' )
				{

					if ( ( $len = preg_match_all( '/./u', $term, $t ) ) > 1 )
					{

						$orQuerys[ ] = $likecolumn . ' REGEXP "(' . implode( '|', $t[ 0 ] ) . '){' . round( $len * 0.8 ) . ',' . ($len) . '}"';
					}
					else
					{

						$orQuerys[ ] = $likecolumn . ' LIKE "%' . $term . '%"';
					}
				}

				foreach ( $searchcols AS $col => $score )
				{

					$countQuerys[ ] = '( ( ' . $col . ' REGEXP "' . $term . '" ) * ' . $score . ')'
							. ' + ( ( ' . $col . ' REGEXP "[[:<:]]' . $term . '[[:>:]]" ) * ' . $score . ')'
							. ' + ( ( ' . $col . ' REGEXP "^' . $term . '" ) * ' . $score . ')'
							. ' + ( ( ' . $col . ' REGEXP "' . $term . '$" ) * ' . $score . ')'
							. ' + ( IF( length(' . $col . ') = length("' . $term . '"), 1, 0 ) * ' . $score . ')';
				}
			}

			/* $searchcateids=D('ShopCate')->where(array('_string'=>'(' . str_replace( '[likecols]', 'name', implode( ' OR ', $orQuerys ) ) . ')'))->rows('id');

			  if ( count($searchcateids) ) {

			  $where[ '_string' ] = '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' OR ', $orQuerys ) ) . ' OR b.cate_id IN (' . implode( ',', $searchcateids ) . '))';

			  $countQuerys[] = '((b.cate_id IN (' . implode( ',', $searchcateids ) . '))*20)';

			  } else { */

			$where[ '_string' ] = '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' OR ', $orQuerys ) ) . ')';

			//}

			$countOrder = 'is_special DESC, DATEDIFF( time_end , NOW() ) <= 0, is_ended ASC, (' . implode( ' + ', $countQuerys ) . ') DESC, a.create_time DESC';
		}
		else
		{

			$countOrder = 'is_special DESC, DATEDIFF( time_end , NOW() ) <= 0, is_ended ASC, a.create_time DESC, a.id DESC';
		}

		/* query */

		$result_per_page = C( 'RESULT_PER_PAGE' );

		$count = D( 'PromotionFields' );

		$count = $count->table( 'promotion_fields a' )
						->join( 'JOIN promotion_cate_map b ON a.id = b.promotion_id' )
						->join( 'JOIN promotion_shop_map d ON a.id = d.promotion_id' )
						->join( 'JOIN shop_fields e ON d.shop_id = e.id' )
						->where( $where )->count( 'distinct a.id' );

		$totalpage = ceil( $count / $result_per_page );

		if ( $page == '' )
		{

			$page = 1;
		}

		$results = D( 'PromotionFields' )->field( 'a.id as promotion_id,a.name as name,a.is_special,f.file_path as picture_url,a.time_start,a.time_end,a.type1,a.is_ended,WEEKDAY(a.time_start) AS time_start_weekday ,WEEKDAY(a.time_end) AS time_end_weekday' );

		$results = $results
				->table( 'promotion_fields a' )
				->join( 'JOIN promotion_cate_map b ON a.id = b.promotion_id' )
				->join( 'LEFT JOIN statistics_promotion c ON a.id = c.promotion_id' )
				->join( 'JOIN promotion_shop_map d ON a.id = d.promotion_id' )
				->join( 'JOIN shop_fields e ON d.shop_id = e.id' )
				->join( 'LEFT JOIN promotion_picture f ON a.id = f.promotion_id and f.is_main = 1' )
				->where( $where )->group( 'a.id' )
				->limit( $result_per_page )->page( $page )
				->order( $countOrder )
				->select();

		$weekdays = array( '一', '二', '三', '四', '五', '六', '日' );

		foreach ( $results as $key => $val )
		{

			if ( strtotime( $results[ $key ][ 'time_start' ] ) && strtotime( $results[ $key ][ 'time_end' ] ) )
			{

				$results[ $key ][ 'date_range' ] = date( chDate( 3 ), strtotime( $results[ $key ][ 'time_start' ] ) ) . "(" . $weekdays[ $results[ $key ][ 'time_start_weekday' ] ] . ")" . " 至 " . date( chDate( 3 ), strtotime( $results[ $key ][ 'time_end' ] ) ) . "(" . $weekdays[ $results[ $key ][ 'time_end_weekday' ] ] . ")";
			}
			elseif ( strtotime( $results[ $key ][ 'time_start' ] ) )
			{

				$results[ $key ][ 'date_range' ] = date( chDate( 3 ), strtotime( $results[ $key ][ 'time_start' ] ) ) . "(" . $weekdays[ $results[ $key ][ 'time_start_weekday' ] ] . ")" . " 起 ";
			}
			elseif ( strtotime( $results[ $key ][ 'time_end' ] ) )
			{

				$results[ $key ][ 'date_range' ] = "即日起 至 " . date( chDate( 3 ), strtotime( $results[ $key ][ 'time_end' ] ) ) . "(" . $weekdays[ $results[ $key ][ 'time_end_weekday' ] ] . ")";
			}

			if ( $results[ $key ][ 'is_ended' ] || (strtotime( $results[ $key ][ 'time_end' ] ) && strtotime( date( 'Y-m-d' ) ) > strtotime( $results[ $key ][ 'time_end' ] )) )
			{

				$results[ $key ][ 'date_range' ] = "(已完結)";
				$results[ $key ][ 'name' ] = "(已完結) " . $results[ $key ][ 'name' ];
			}
			else
			{

				$results[ $key ][ 'date_range' ].=$results[ $key ][ 'time_remarks' ];
			}

			$results[ $key ][ 'picture_url' ] = rtnPromotionImg( $results[ $key ] );
			$results[ $key ][ 'picture_url' ] = $results[ $key ][ 'picture_url' ][ 0 ];

			$results[ $key ][ 'location_name' ] = D( 'ShopFields' )
					->table( 'shop_fields a' )
					->field( 'a.id,a.name as shop_name, c.name as sub_location_name, a.googlemap_lat, a.googlemap_lng' )
					->join( 'promotion_shop_map b on a.id = b.shop_id' )
					->join( 'location_sub c on a.sub_location_id=c.id' )
					->where( 'b.promotion_id=' . $val[ 'promotion_id' ] )
					->select();

			$results[ $key ][ 'googlemap_lat' ] = '';

			$results[ $key ][ 'googlemap_lng' ] = '';

			if ( count( $results[ $key ][ 'location_name' ] ) == 1 )
			{

				$results[ $key ][ 'googlemap_lat' ] = $results[ $key ][ 'location_name' ][ 0 ][ 'googlemap_lat' ];

				$results[ $key ][ 'googlemap_lng' ] = $results[ $key ][ 'location_name' ][ 0 ][ 'googlemap_lng' ];
			}

			$results[ $key ][ 'location_name' ] = count( $results[ $key ][ 'location_name' ] ) > 1 ? $results[ $key ][ 'location_name' ][ 0 ][ 'shop_name' ] . " (" . count( $results[ $key ][ 'location_name' ] ) . "個地點)" : $results[ $key ][ 'location_name' ][ 0 ][ 'shop_name' ] . " " . $results[ $key ][ 'location_name' ][ 0 ][ 'sub_location_name' ];

			$results[ $key ][ 'category_names' ] = D( 'ShopCate' )
							->table( 'shop_cate a' )
							->join( 'JOIN promotion_cate_map b on a.id=b.cate_id' )
							->where( 'b.promotion_id=' . $val[ 'promotion_id' ] )->select();

			$results[ $key ][ 'category_names' ] = formatCategory( $results[ $key ][ 'category_names' ], 100, false );

			$results[ $key ][ 'promotion_url' ] = 'http://app.outstreet.com.hk/index.php?m=promotion&a=view&promotion_id=' . $results[ $key ][ 'promotion_id' ];

			unset( $results[ $key ][ 'scores' ] );
			unset( $results[ $key ][ 'type1' ] );
			unset( $results[ $key ][ 'time_start' ] );
			unset( $results[ $key ][ 'time_end' ] );
			unset( $results[ $key ][ 'time_start_weekday' ] );
			unset( $results[ $key ][ 'time_end_weekday' ] );
			unset( $results[ $key ][ 'is_ended' ] );
			unset( $results[ $key ][ 'is_special' ] );

			if ( $val[ 'is_special' ] == 1 )
			{
				$special_list[ ] = $results[ $key ];
			}
			else
			{
				$promotion_list[ ] = $results[ $key ];
			}
		}

		/*		 * ******************************************** Start Array Management ******************************************** */
		$special_list = rtn_array_string( $special_list );
		$promotion_list = rtn_array_string( $promotion_list );
		/*		 * ******************************************** End Array Management ********************************************** */

		$json = utf8_encode_array( array(
			'info' => array( array(
					'category_id' => $category_id,
					'category_name' => $category_name,
					'total_page' => $totalpage,
					'total_number' => $count,
					'ps_page' => $totalpage ? $page - 1 : 0,
					'brand_id' => $brand_id,
					'now_page' => $totalpage ? $page : 0,
					'more_page' => $page < $totalpage ? $page + 1 : 0 ), array(
					'special_list' => $special_list,
					'promotion_list' => $promotion_list,
			) ),
			'special_list' => $special_list,
			'promotion_list' => $promotion_list,
				) );
		$this->mydisplay( $json );
	}

	public function email()
	{

		$promotion_id = (int) $_REQUEST[ 'promotion_id' ];

		$promotionInfo = D( 'PromotionFields' )
				->field( 'time_start,time_end,is_ended,price_range,type1,description,terms,remarks,WEEKDAY(time_start) AS time_start_weekday,WEEKDAY(time_end) AS time_end_weekday,time_remarks' )
				->where( 'id=' . $promotion_id )
				->find();

		$promotionInfo[ 'type' ] = rtnPromotionType( $promotionInfo[ 'type1' ] );

		$cates = D( 'ShopCate' )
						->table( 'shop_cate a' )
						->join( 'JOIN promotion_cate_map b on a.id=b.cate_id' )
						->where( 'b.promotion_id=' . $promotion_id )->select();

		$promotionInfo[ 'category_names' ] = formatCategory( $cates, 100, false );

		$promotionInfo[ 'link' ] = C( 'OUTSTREET_DIR' ) . '/promotion/view/id/' . $promotion_id;

		$weekdays = array( '一', '二', '三', '四', '五', '六', '日' );

		if ( strtotime( $promotionInfo[ 'time_start' ] ) && strtotime( $promotionInfo[ 'time_end' ] ) )
		{

			$promotionInfo[ 'date_range' ] = date( chDate( 3 ), strtotime( $promotionInfo[ 'time_start' ] ) ) . "(" . $weekdays[ $promotionInfo[ 'time_start_weekday' ] ] . ")" . " 至 " . date( chDate( 3 ), strtotime( $promotionInfo[ 'time_end' ] ) ) . "(" . $weekdays[ $promotionInfo[ 'time_end_weekday' ] ] . ")";
		}
		elseif ( strtotime( $promotionInfo[ 'time_start' ] ) )
		{

			$promotionInfo[ 'date_range' ] = date( chDate( 3 ), strtotime( $promotionInfo[ 'time_start' ] ) ) . "(" . $weekdays[ $promotionInfo[ 'time_start_weekday' ] ] . ")" . " 起 ";
		}
		elseif ( strtotime( $promotionInfo[ 'time_end' ] ) )
		{

			$promotionInfo[ 'date_range' ] = "即日起 至 " . date( chDate( 3 ), strtotime( $promotionInfo[ 'time_end' ] ) ) . "(" . $weekdays[ $promotionInfo[ 'time_end_weekday' ] ] . ")";
		}

		if ( $promotionInfo[ 'is_ended' ] || (strtotime( $promotionInfo[ 'time_end' ] ) && strtotime( date( 'Y-m-d' ) ) > strtotime( $promotionInfo[ 'time_end' ] )) )
		{

			$promotionInfo[ 'date_range' ] = "(已完結)";
			$promotionInfo[ 'name' ] = "(已完結) " . $promotionInfo[ 'name' ];
		}
		else
		{

			$promotionInfo[ 'date_range' ].=$promotionInfo[ 'time_remarks' ];
		}

		$promotionInfo[ 'location_name' ] = D( 'ShopFields' )
				->table( 'shop_fields a' )
				->field( 'a.id,a.name as shop_name, c.name as sub_location_name, a.googlemap_lat, a.googlemap_lng' )
				->join( 'promotion_shop_map b on a.id = b.shop_id' )
				->join( 'location_sub c on a.sub_location_id=c.id' )
				->where( 'b.promotion_id=' . $promotion_id )
				->select();

		$promotionInfo[ 'location_name' ] = count( $promotionInfo[ 'location_name' ] ) > 1 ? $promotionInfo[ 'location_name' ][ 0 ][ 'shop_name' ] . " (" . count( $promotionInfo[ 'location_name' ] ) . "個地點)" : $promotionInfo[ 'location_name' ][ 0 ][ 'shop_name' ] . " " . $promotionInfo[ 'location_name' ][ 0 ][ 'sub_location_name' ];

		$title = $promotionInfo[ 'name' ];

		$content = "";

		$content .= "\n";

		$content .= "優惠名稱 : " . $promotionInfo[ 'name' ] . "\n";

		$content .= "優惠地區 : " . $promotionInfo[ 'location_name' ] . "\n";

		$content .= "優惠類別 : " . $promotionInfo[ 'category_names' ] . "\n";

		if ( $promotionInfo[ 'date_range' ] )
		{
			$content .= "優惠時間 : " . $promotionInfo[ 'date_range' ] . "\n";
		}

		if ( $promotionInfo[ 'description' ] )
		{
			$content .= "優惠介紹 : " . $promotionInfo[ 'description' ] . "\n";
		}

		$content .= "優惠網址 : " . $promotionInfo[ 'link' ];

		$json = utf8_encode_array( array( 'title' => $title, 'content' => $content ) );

		$this->mydisplay( $json );
	}

	public function view()
	{

		$promotion_id = (int) $_REQUEST[ 'promotion_id' ];

		$promotionInfo = D( 'PromotionFields' )
				->field( 'a.id as promotion_id,a.id as promotoion_id,a.time_start,a.time_end,a.price_range,a.name as name,a.type1,a.description,a.terms,a.remarks,b.file_path as picture_url,a.is_ended,WEEKDAY(a.time_start) AS time_start_weekday,WEEKDAY(a.time_end) AS time_end_weekday,a.time_remarks' )
				->table( 'promotion_fields a' )
				->join( 'LEFT JOIN promotion_picture b ON a.id = b.promotion_id and b.is_main = 1' )
				->where( 'a.id=' . $promotion_id )
				->find();

		$promotionInfo[ 'picture_url' ] = rtnPromotionImg( $promotionInfo );
		$promotionInfo[ 'type' ] = rtnPromotionType( $promotionInfo[ 'type1' ] );

		$cates = D( 'ShopCate' )
						->table( 'shop_cate a' )
						->join( 'JOIN promotion_cate_map b on a.id=b.cate_id' )
						->where( 'b.promotion_id=' . $promotion_id )->select();

		$promotionInfo[ 'category_names' ] = formatCategory( $cates, 100, false );

		$shops = D( 'PromotionShopMap' )->where( 'promotion_id=' . $promotion_id )->select();

		$promotionInfo[ 'locations' ] = D( 'ShopFields' )
				->table( 'shop_fields a' )
				->field( 'a.id as shop_id,CONCAT(a.name," (",c.name,") ",a.address) as name, a.googlemap_lat,googlemap_lng' )
				->join( 'promotion_shop_map b on a.id=b.shop_id' )
				->join( 'location_sub c on a.sub_location_id=c.id' )
				->where( 'b.promotion_id=' . $promotion_id )
				->select();

		$promotionInfo[ 'brand' ] = D( 'PromotionShopMap' )
						->join( 'JOIN shop_fields ON promotion_shop_map.shop_id=shop_fields.id' )
						->join( 'JOIN shop_brand ON shop_fields.brand_id=shop_brand.id' )
						->where( array( 'promotion_shop_map.promotion_id' => $promotion_id ) )->group( 'shop_brand.id' )->getField( 'shop_brand.name' );

		$weekdays = array( '一', '二', '三', '四', '五', '六', '日' );

		if ( strtotime( $promotionInfo[ 'time_start' ] ) && strtotime( $promotionInfo[ 'time_end' ] ) )
		{
			$promotionInfo[ 'date_range' ] = date( chDate( 3 ), strtotime( $promotionInfo[ 'time_start' ] ) ) . "(" . $weekdays[ $promotionInfo[ 'time_start_weekday' ] ] . ")" . " 至 " . date( chDate( 3 ), strtotime( $promotionInfo[ 'time_end' ] ) ) . "(" . $weekdays[ $promotionInfo[ 'time_end_weekday' ] ] . ")";
		}
		elseif ( strtotime( $promotionInfo[ 'time_start' ] ) )
		{
			$promotionInfo[ 'date_range' ] = date( chDate( 3 ), strtotime( $promotionInfo[ 'time_start' ] ) ) . "(" . $weekdays[ $promotionInfo[ 'time_start_weekday' ] ] . ")" . " 起 ";
		}
		elseif ( strtotime( $promotionInfo[ 'time_end' ] ) )
		{
			$promotionInfo[ 'date_range' ] = "即日起 至 " . date( chDate( 3 ), strtotime( $promotionInfo[ 'time_end' ] ) ) . "(" . $weekdays[ $promotionInfo[ 'time_end_weekday' ] ] . ")";
		}

		if ( $promotionInfo[ 'is_ended' ] || (strtotime( $promotionInfo[ 'time_end' ] ) && strtotime( date( 'Y-m-d' ) ) > strtotime( $promotionInfo[ 'time_end' ] )) )
		{
			$promotionInfo[ 'date_range' ] = "(已完結)";
			$promotionInfo[ 'name' ] = "(已完結) " . $promotionInfo[ 'name' ];
		}
		else
		{
			$promotionInfo[ 'date_range' ].=$promotionInfo[ 'time_remarks' ];
		}

		$promotionInfo[ 'review_count' ] = D( 'ReviewFields' )->where( array( 'promotion_id' => $promotion_id, 'is_audit' => '1' ) )->count();

		$promotionInfo[ 'reviews' ] = D( 'ReviewFields' )
				->field( 'a.id as review_id,a.title,a.description,c.nick_name as user_name, if(c.face_path,CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'USERFACE_UPFILE_PATH' ) . 'face",c.face_path),CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'USERDEFAULTFACE_UPFILE_PATH' ) . '",c.face_path)) AS user_picture_url,FROM_UNIXTIME(a.create_time,"%d/%m") as create_time,FROM_UNIXTIME(a.create_time,"%w") as week_time' )
				->table( 'review_fields a' )
				->join( 'user_fields c ON a.user_id=c.id' )
				->where( array( 'a.promotion_id' => $promotion_id, 'a.is_audit' => '1' ) )
				->select();

		foreach ( $promotionInfo[ 'reviews' ] as $key => $val )
		{

			$promotionInfo[ 'reviews' ][ $key ][ 'week_time' ] = week2ch( $promotionInfo[ 'reviews' ][ $key ][ 'week_time' ] );

			$promotionInfo[ 'reviews' ][ $key ][ 'description' ] = preg_replace( "/<br(.*?)\/+?>/", "\n", $promotionInfo[ 'reviews' ][ $key ][ 'description' ] );

			$promotionInfo[ 'reviews' ][ $key ][ 'description' ] = strip_tags( $promotionInfo[ 'reviews' ][ $key ][ 'description' ] );

			$promotionInfo[ 'reviews' ][ $key ][ 'create_time' ].="(" . $promotionInfo[ 'reviews' ][ $key ][ 'week_time' ] . ")";

			unset( $promotionInfo[ 'reviews' ][ $key ][ 'week_time' ] );
		}

		/*		 * ******************************************** Start Array Management ******************************************** */

		$promotionInfo[ 'reviews' ] = rtn_array_string( $promotionInfo[ 'reviews' ] );

		$promotionInfo[ 'locations' ] = rtn_array_string( $promotionInfo[ 'locations' ] );

		/*		 * ******************************************** End Array Management ********************************************** */

		unset( $promotionInfo[ 'time_start' ] );

		unset( $promotionInfo[ 'time_end' ] );

		unset( $promotionInfo[ 'time_start_weekday' ] );

		unset( $promotionInfo[ 'time_end_weekday' ] );

		unset( $promotionInfo[ 'type1' ] );

		unset( $promotionInfo[ 'is_ended' ] );

		$promotionInfo[ 'remarks' ] = html2txt( $promotionInfo[ 'remarks' ] );

		$promotionInfo[ 'terms' ] = html2txt( $promotionInfo[ 'terms' ] );

		$promotionInfo[ 'description' ] = html2txt( $promotionInfo[ 'description' ] );

		$json = utf8_encode_array( array(
			'results' => array( $promotionInfo ),
				) );

		$this->mydisplay( $json );
	}

	public function facebook()
	{

		$promotion_id = (int) $_REQUEST[ 'promotion_id' ];

		$title = '想同你分享「%1$s」呢個優惠，去OutStreet睇下啦！';

		$promotionInfo = D( 'PromotionFields' )->where( array( 'id' => $promotion_id ) )->find();

		if ( $promotionInfo[ 'is_ended' ] == 1 )
		{
			$promotionInfo[ 'name' ] = $promotionInfo[ 'name' ] . " (已完結)";
		}

		$info = array( );

		$info[ 'title' ] = sprintf( $title, $promotionInfo[ 'name' ] );

		$info[ 'link' ] = C( 'OUTSTREET_DIR' ) . '/promotion/view/id/' . $promotion_id;

		$info[ 'description' ] = $promotionInfo[ 'description' ];

		$json = utf8_encode_array( $info );

		$this->mydisplay( $json );
	}

	private function getsubcateids( $category_ids )
	{

		$shopCateDao = D( 'ShopCate' );

		$search_cate_ids = $category_ids;

		$loopids = array( );

		$loopids = $category_ids;

		$loop = true;

		while ( $loop )
		{

			$shopCateList = $shopCateDao->where( array( 'parent_id' => array( 'in', $loopids ) ) )->select();

			$cate = array( );

			foreach ( $shopCateList AS $val )
			{

				$cate[ ] = $val[ 'id' ];

				$search_cate_ids[ ] = $val[ 'reference_id' ] ? $val[ 'reference_id' ] : $val[ 'id' ];
			}

			if ( count( $cate ) )
			{
				$loopids = $cate;
			}
			else
			{
				$loop = false;
			}
		}

		return $search_cate_ids;
	}

}

?>