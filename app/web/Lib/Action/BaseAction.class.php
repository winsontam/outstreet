<?php

class BaseAction extends Action
{

	protected $cache_filename = '';

	function __construct()
	{
		$this->cache_filename = APP_PATH . '/Cache/cache_' . preg_replace( '/[^a-zA-Z0-9]/', '_', urlencode( getenv( 'QUERY_STRING' ) ) );

		if ( file_exists( $this->cache_filename ) )
		{
			if ( time() - filemtime( $this->cache_filename ) <= 3600 * 24 )
			{
				exit( base64_decode( file_get_contents( $this->cache_filename ) ) );
			}
		}
	}

	public function mydisplay( $json )
	{
		Vendor( 'JSON.JSON' );
		$json_services = new Services_JSON();
		$is_dev = $_REQUEST[ 'is_dev' ];

		if ( $is_dev )
		{
			echo '<pre>';
			print_r( $json );
			echo '</pre>';
		}
		else
		{
			//$json = json_encode($json);
			//echo str_replace("\/", "/", $json);
			$json = $json_services->encode( $json );
			$json = preg_replace( "#\\\u([0-9a-f]{4}+)#ie", "iconv('UCS-2BE', 'UTF-8', pack('H4', '\\1'))", $json );
			$json = preg_replace( "/<br\/?>/", "\n", $json );
			$json = str_replace( "\\r", "", $json );
			$json = str_replace( "\\t", "", $json );
			/* $json = preg_replace('/(\\\\n)+/','\n',$json);	// replace consecutive \n */
			echo $json;

			if ( isset( $_REQUEST[ 'query' ] )
					|| isset( $_REQUEST[ 'googlemap_lat' ] )
					|| isset( $_REQUEST[ 'googlemap_lng' ] )
					|| isset( $_REQUEST[ 'distance' ] ) )
			{
				file_put_contents( $this->cache_filename, base64_encode( $json ) );
			}
		}
	}

}

?>