<?php

class SearchAction extends BaseAction
{
	public function category()
	{
		$category_id = (int) $_REQUEST[ 'category_id' ];
		$where = array();
		$where[ 'parent_id' ] = $category_id;
		$where[ 'type' ] = array('neq','website');
		$temp_cates = D( 'ShopCate' )->field( 'id AS category_id, name, end_node' )->where( $where )->count() ? D( 'ShopCate' )->field( 'if(reference_id,reference_id,id) as category_id, name, end_node,id' )->where( $where )->order('shop_count desc')->rows('id'): array();
		foreach($temp_cates as $key=>$val){
			unset($temp_cates[$key]['id']);
		}
		$categories = array();
		if($category_id==0){
			$index_shop_cates=explode(',',C('INDEX_SHOP_CATES'));
			foreach($index_shop_cates as $val){
				$categories[]=$temp_cates[$val];
			}
		}else{
			if(C('SUB_CATE_ORDER_'.$category_id)){
				$sub_cate_order=explode(',',C('SUB_CATE_ORDER_'.$category_id));
				foreach($sub_cate_order as $key=>$val){
					$categories[]=$temp_cates[$val];
				}
			}else{
				$categories=array_values($temp_cates);
			}
		}
		
		/********************************************** Start Array Management *********************************************/
		$categories	= rtn_array_string($categories);
		/********************************************** End Array Management ***********************************************/
		
		$json= array( 'categories' => $categories);

		$json=utf8_encode_array($json);
		$this->mydisplay($json);		
	}

	public function district()
	{
		$main_location_id = (int) $_REQUEST[ 'main_location_id' ];		
		$where = array();
		if ($main_location_id) { 
			$where['main_location_id']=$main_location_id;
			$districts = D( 'LocationSub' )->field( 'id AS sub_location_id, name' )->where( $where )->order('m_orders, id')->select();
		}else{
			$districts = D( 'LocationMain' )->field( 'id AS main_location_id, name' )->where( $where )->order('orders, id')->select();
		}
		
		/********************************************** Start Array Management *********************************************/
		$districts	= rtn_array_string($districts);
		/********************************************** End Array Management ***********************************************/

		$json= array( 'districts' => $districts);
		$json=utf8_encode_array($json);
		$this->mydisplay($json);
	}

}

?>