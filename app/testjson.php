{
  
    "resources": [
      {
        "@type": "HotFlog",
        "@url": "http://api.storageroomapp.com/accounts/509c79670f6602025600181a/collections/509c7c770f660222930015a4/entries/509c83e30f66025f4d000702",
        "@collection_url": "http://api.storageroomapp.com/accounts/509c79670f6602025600181a/collections/509c7c770f660222930015a4",
        "@version": 12,
        "@trash": false,
        "@created_at": "2012-11-09T04:17:39Z",
        "@updated_at": "2012-11-09T11:17:27Z",
        "@shop_name": "\u9d3b\u798f\u6d77\u9bae\u56db\u5b63\u706b\u934b",
        "catalog": "\u5730\u9053\u706b\u934b",
        "shop_des_text": "\u5730\u9053\u70ad\u7210\u6253\u908a\u7210\uff01\u591a\u6b3e\u7279\u8272\u914d\u6599\uff01",
        "shop_image": {
          "@type": "Image",
          "@url": "http://files.storageroomapp.com/accounts/509c79670f6602025600181a/collection/509c7c770f660222930015a4/entries/509c83e30f66025f4d000702/fields/k509c82d90f660261120002a2/file.jpg",
          "@processing": false,
          "@versions": {
            
          }
        },
        "image_url": "http://files.storageroomapp.com/accounts/509c79670f6602025600181a/collection/509c7c770f660222930015a4/entries/509c83e30f66025f4d000702/fields/k509c82d90f660261120002a2/file.jpg",
        "address": "\u4e5d\u9f8d \u571f\u74dc\u7063 \u843d\u5c71\u905386\u865f \u5730\u4e0b\u5df7\u5167",
        "shop_loc": {
          "@type": "Location",
          "lat": 22.316059,
          "lng": 114.190108
        },
        "work_time": "NA",
        "prices": "$200/\u4eba",
        "hot_sales": "\u9bca\u9b5a\u6e6f\u5e95",
        "tel": "2365 0112",
        "rating": 5,
        "shop_url": "NA"
      },
      {
        "@type": "HotFlog",
        "@url": "http://api.storageroomapp.com/accounts/509c79670f6602025600181a/collections/509c7c770f660222930015a4/entries/509c87d10f6602606600048e",
        "@collection_url": "http://api.storageroomapp.com/accounts/509c79670f6602025600181a/collections/509c7c770f660222930015a4",
        "@version": 8,
        "@trash": false,
        "@created_at": "2012-11-09T04:34:25Z",
        "@updated_at": "2012-11-09T11:18:02Z",
        "@shop_name": "\u934b\u5c45\u6d77\u9bae\u706b\u934b",
        "catalog": "\u5730\u9053\u706b\u934b",
        "shop_des_text": "\u4e2d\u65e5\u5f0f\u706b\u934b,\u6d3b\u505a\u523a\u8eab\u53ca\u65e5\u5f0f\u71d2\u70e4",
        "shop_image": {
          "@type": "Image",
          "@url": "http://files.storageroomapp.com/accounts/509c79670f6602025600181a/collection/509c7c770f660222930015a4/entries/509c87d10f6602606600048e/fields/k509c82d90f660261120002a2/file.jpg",
          "@processing": false,
          "@versions": {
            
          }
        },
        "image_url": "http://files.storageroomapp.com/accounts/509c79670f6602025600181a/collection/509c7c770f660222930015a4/entries/509c87d10f6602606600048e/fields/k509c82d90f660261120002a2/file.jpg",
        "address": "\u9999\u6e2f\u5317\u89d2\u4e03\u59ca\u59b9\u9053112-114\u865f\u5408\u5049\u5927\u53a61\u6a13",
        "shop_loc": {
          "@type": "Location",
          "lat": 22.291084,
          "lng": 114.203743
        },
        "work_time": " \u661f\u671f\u4e00\u81f3\u65e5  18:00 - 02:00 ",
        "prices": "NA",
        "hot_sales": "NA",
        "tel": "2677 3829",
        "rating": 3,
        "shop_url": "NA"
      }
    ]
  }

