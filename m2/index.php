<?php

header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
define('THINK_PATH','./ThinkPHP');
define('APP_NAME','web');
define('APP_PATH','web/');
require(THINK_PATH.'/ThinkPHP.php');
$App = new App();
$App->run();

?>