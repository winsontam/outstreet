<?php

class ShopAction extends BaseAction
{

	public function search()
	{
		$category_ids = (array) $_REQUEST[ 'category_id' ];
		if ( $_REQUEST[ 'category_id' ] )
		{
			$category_ids = (array) $_REQUEST[ 'category_id' ];
		}
		else
		{
			$category_ids = (array) $_REQUEST[ 'category_ids' ];
		}
		$category_id = !empty( $category_ids ) ? $category_ids[ 0 ] : 0;
		$this->assign( 'category_id', $category_id );

		$generate_result = generate_categoryList( $category_id );
		if ( !empty( $generate_result[ 1 ] ) )
		{
			$this->assign( 'title', $generate_result[ 1 ][ 'name' ] . ' ' . $generate_result[ 1 ][ 'english_name' ] );
			$this->assign( 'request_cate_Info', $generate_result[ 1 ] );
		}
		if ( !empty( $generate_result[ 0 ] ) )
		{
			$this->assign( 'categoryList', $generate_result[ 0 ] );
		}

		/* core */
		$query = (string) $_REQUEST[ 'query' ];
		$main_location_id = (int) $_REQUEST[ 'main_location_id' ];
		$sub_location_ids = (array) $_REQUEST[ 'sub_location_ids' ];

		if ( $_REQUEST[ 'location_id' ] )
		{
			$this->assign( 'location_id', $location_id );
			$location = explode( '_', $_REQUEST[ 'location_id' ] );
			if ( $location[ 0 ] == 'main' )
			{
				$main_location_id = $location[ 1 ];
			}
			else
			{
				$sub_location_ids[] = $location[ 1 ];
			}
		}
		$this->assign( 'main_location_id', $main_location_id );
		$this->assign( 'sub_location_ids', $sub_location_ids );
		$googlemap_lat = (float) $_COOKIE[ 'googlemap_lat' ];
		$googlemap_lng = (float) $_COOKIE[ 'googlemap_lng' ];
		$nearby = (string) $_REQUEST[ 'nearby' ];
		$price_range = (int) $_REQUEST[ 'price_range' ];
		$is_open_now = (bool) $_REQUEST[ 'is_open_now' ];
		$parent_shop_id = (int) $_REQUEST[ 'parent_shop_id' ];
		$brand_id = (int) $_REQUEST[ 'brand_id' ];
		$page = (int) $_REQUEST[ 'page' ];

		$main_location_list = getmainlocationList();
		$this->assign( 'main_location_list', $main_location_list );
		for ( $i = 0; $i < 4; $i++ )
		{
			$sub_location_list[ $i ] = getsublocationList( $i + 1 );
		}
		$this->assign( 'sub_location_list', $sub_location_list );

		$category_ids = getsubcateids( $category_ids );
		$where = array();

		$where[ 'is_audit' ] = 1;
		if ( $price_range )
		{
			$where[ 'price_range' ] = array( 'in', $price_range );
		}

		$countQuerys = array();

		if ( $query )
		{
			$this->assign( 'query', $query );
			$terms = $query;
			$allterms = array();
			if ( preg_match_all( '/\w+/', $terms, $m ) )
			{
				foreach ( $m[ 0 ] AS $t )
				{
					$terms = str_replace( $t, '', $terms );
					$allterms[ $t ] = 'e';
				}
			}
			$mterms = array();
			mb_regex_encoding( 'UTF-8' );
			mb_ereg_search_init( $terms, '\w+' );
			while ( ( $m = mb_ereg_search_regs() ) )
			{
				$allterms[ $m[ 0 ] ] = 'm';
			}

			$suggestWhere = array();
			foreach ( $allterms AS $term => $type )
			{
				$suggestWhere[ 'keyword' ][] = array( 'like', $term );
			}
			$suggestWhere[ 'keyword' ][] = 'or';
			$suggestKeywords = M( 'KeywordTranslate' )->where( $suggestWhere )->select();
			foreach ( $suggestKeywords AS $val )
			{
				$keywords = explode( ',', $val[ 'suggest_keywords' ] );
				foreach ( $keywords AS $k )
				{
					if ( $k )
					{
						$allterms[ $k ] = 's';
					}
				}
			}

			$searchcols = array(
				'CONCAT(`name`,":",`eng_name`)'	 => 10,
				'`keyword`'						 => 6,
				'`address`'						 => 4
			);
			$likecolumn = '[likecols]';
			$orQuerys = array();

			foreach ( $allterms AS $term => $type )
			{
				if ( $type == 'e' || $type == 's' )
				{
					//$orQuerys[] = $likecolumn . ' REGEXP "[[:<:]]' . $term . '.{0,3}[[:>:]]"';
					$orQuerys[] = $likecolumn . ' REGEXP "' . $term . '"';
				}
				elseif ( $type == 'm' )
				{
					if ( ( $len = preg_match_all( '/./u', $term, $t ) ) > 1 )
					{
						$orQuerys[] = $likecolumn . ' REGEXP "(' . implode( '|', $t[ 0 ] ) . '){' . round( $len * 0.8 ) . ',' . ($len) . '}"';
					}
					else
					{
						$orQuerys[] = $likecolumn . ' LIKE "%' . $term . '%"';
					}
				}
				foreach ( $searchcols AS $col => $score )
				{
					$countQuerys[] = '( ( ' . $col . ' REGEXP "' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "[[:<:]]' . $term . '[[:>:]]" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "^' . $term . '" ) * ' . $score . ')'
						. ' + ( ( ' . $col . ' REGEXP "' . $term . '$" ) * ' . $score . ')'
						. ' + ( IF( length(' . $col . ') = length("' . $term . '"), 1, 0 ) * ' . $score . ')';
				}
			}

			$searchcateids = D( 'ShopCate' )->where( array( 'type' => array( 'neq', 'website' ), '_string' => '(' . str_replace( '[likecols]', 'name', implode( ' OR ', $orQuerys ) ) . ')' ) )->rows( 'id' );
			if ( count( $searchcateids ) )
			{
				$where[ '_string' ] = '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' OR ', $orQuerys ) ) . ' OR b.cate_id IN (' . implode( ',', $searchcateids ) . '))';
				$countQuerys[] = '((b.cate_id IN (' . implode( ',', $searchcateids ) . '))*20)';
			}
			else
			{
				$where[ '_string' ] = '(' . str_replace( '[likecols]', 'CONCAT( ' . implode( ',":",', array_keys( $searchcols ) ) . ' )', implode( ' OR ', $orQuerys ) ) . ')';
			}
		}

		$countQuerys[] = '( IF( length(telephone), 1, 0 ) )' . ' + ( IF( a.shop_status > 0, 1, 0 ) * -5 )' . ' + ( IF( d.id>0, 5, 0 ) )';
		$countField = '((' . implode( ' + ', $countQuerys ) . ') + ( IF(c.review_count is null,0,c.review_count) ) + ( IF ( a.shop_level, a.shop_level*1000, 0 ) ) ) AS scores';

		if ( $main_location_id )
		{
			$where[ 'a.sub_location_id' ] = array( 'exp', ' IN (SELECT id FROM location_sub WHERE main_location_id = "' . $main_location_id . '" ) ' );
		}
		if ( $sub_location_ids )
		{
			$where[ 'a.sub_location_id' ] = array( 'in', $sub_location_ids );
		}
		if ( $parent_shop_id )
		{
			$where[ 'a.parent_shop_id' ] = $parent_shop_id;
		}
		if ( $price_range )
		{
			$where[ 'a.price_range' ] = $price_range;
		}
		if ( $brand_id )
		{
			$where[ 'a.brand_id' ] = $brand_id;
		}
		if ( $category_ids )
		{
			$where[ 'b.cate_id' ] = array( 'in', $category_ids );
		}

		/* is open now */
		if ( $is_open_now )
		{
			$today_date = date( "Y-m-d", time() );

			$today_weekday = date( "N", time() );

			$today_time = date( "Hm", time() );

			if ( D( 'PublicHoliday' )->where( 'holiday="' . $today_date . '"' )->select() )
			{
				if ( $where[ '_string' ] )
				{
					$where[ '_string' ] .= ' AND ';
				}
				else
				{
					$where[ '_string' ] = '';
				}
				$where[ '_string' ] .= "
				(
					(e.recurring_week=8
					and
					(
						if(e.start_time>e.end_time," . $today_time . " between e.start_time and e.end_time+2400," . $today_time . " between e.start_time and e.end_time))
						OR
						(if(" . ($today_weekday - 1) . "=0,e.recurring_week=7,e.recurring_week=" . ($today_weekday - 1) . ") and " . $today_time . " between '0000' and e.end_time)

					)
				)";
			}
			else
			{
				if ( $where[ '_string' ] )
				{
					$where[ '_string' ] .= ' AND ';
				}
				else
				{
					$where[ '_string' ] = '';
				}
				$where[ '_string' ] .= "
				(
					(e.recurring_week=" . $today_weekday . "
					and
					(

						if(e.start_time>e.end_time," . $today_time . " between e.start_time and e.end_time+2400," . $today_time . " between e.start_time and e.end_time))
						OR
						(if(" . ($today_weekday - 1) . "=0,e.recurring_week=7,e.recurring_week=" . ($today_weekday - 1) . ") and " . $today_time . " between '0000' and e.end_time)
					)
				)";
			}
		}
		/* is open now */

		/* user GPS distance search */
		if ( !$main_location_id && !count( $sub_location_ids ) && $googlemap_lat && $googlemap_lng )
		{
			if ( $where[ '_string' ] )
			{
				$where[ '_string' ] .= ' AND ';
			}
			else
			{
				$where[ '_string' ] = '';
			}
			$where[ '_string' ] .= '( 6371 * acos( cos( radians(' . $googlemap_lat . ') ) * cos( radians( googlemap_lat ) ) * cos( radians( googlemap_lng ) - radians(' . $googlemap_lng . ') ) + sin( radians(' . $googlemap_lat . ') ) * sin( radians( googlemap_lat ) ) ) ) <= 1';
		}
		/* user GPS distance search */

		/* input page */
		$result_per_page = C( 'RESULT_PER_PAGE' );

		$count = D( 'ShopFields' );
		if ( $is_open_now )
		{
			$count = $count->join( 'JOIN shop_time e on a.id = e.shop_id' );
		}
		$count = $count->table( 'shop_fields a' )
				->join( 'JOIN shop_cate_map b ON a.id = b.shop_id' )
				->join( 'statistics_shop c ON a.id = c.shop_id' )
				->join( 'shop_picture d ON a.id = d.shop_id' )
				->where( $where )->count( 'distinct a.id' );
		$totalpage = ceil( $count / $result_per_page );

		if ( $page == '' )
		{
			$page = 1;
		}
		if ( $page > $totalpage )
		{
			$page = $totalpage;
		}
		/* input page */
		$category_name = D( 'ShopCate' )->where( array( 'type' => array( 'neq', 'website' ), 'id' => $category_id ) )->getField( 'name' );
		$results = D( 'ShopFields' )->field( 'c.review_count,c.avg_rate_1 as rating,a.description,a.telephone,b.shop_id,if(a.name!=a.eng_name,CONCAT(a.name," ",a.eng_name),a.name) as name,a.main_location_id,a.sub_location_id,a.address,a.rooms,a.parent_shop_id, a.googlemap_lat, a.googlemap_lng, if(d.file_path<>"",CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'SHOP_UPFILE_PATH' ) . 'thumb_",d.file_path),"") AS picture_url,a.remarks,' . $countField );
		if ( $is_open_now )
		{
			$results = $results->join( 'JOIN shop_time e on a.id = e.shop_id' );
		}
		$results = $results->table( 'shop_fields a' )
				->join( 'JOIN shop_cate_map b ON a.id = b.shop_id' )
				->join( 'statistics_shop c ON a.id = c.shop_id' )
				->join( 'shop_picture d ON a.id = d.shop_id' )
				->where( $where )->group( 'a.id' )->limit( $result_per_page )->page( $page )->order( 'scores DESC, hit_rate DESC' )->select();

		/* Handle results shop_cates display */
		foreach ( $results as $key => $val )
		{
			$results[ $key ][ 'main_location_name' ] = D( 'LocationMain' )->where( 'id=' . $results[ $key ][ 'main_location_id' ] )->getField( 'name' );
			$results[ $key ][ 'sub_location_name' ] = D( 'LocationSub' )->where( 'id=' . $results[ $key ][ 'sub_location_id' ] )->getField( 'name' );
			if ( $results[ $key ][ 'parent_shop_id' ] != 0 )
			{
				$results[ $key ][ 'parent_shop_name' ] = D( 'ShopFields' )->where( 'id=' . $results[ $key ][ 'parent_shop_id' ] )->getField( 'name' );
			}
			$results[ $key ][ 'cates' ] = D( 'ShopCateMap' )->field( 'b.id,b.name' )->table( 'shop_cate_map a' )->join( 'shop_cate b ON a.cate_id=b.id' )->where( 'shop_id=' . $val[ 'shop_id' ] )->select();
		}
		/* end core */

		$this->assign( 'page_url', preg_replace( '/\/page\/\d+/is', '', $_SERVER[ 'REQUEST_URI' ] ) );
		$this->assign( 'results', $results );
		$this->assign( 'result_per_page', $result_per_page );
		$this->assign( 'totalpage', $totalpage );
		$this->assign( 'price_range', $price_range );
		$this->assign( 'nearby', $nearby );
		$this->assign( 'page', $page );
		$this->assign( 'result_count_current_page', count( $result ) );
		$this->assign( 'result_count', $count );
		$this->display();
	}

	public function searchform()
	{
		$category_id = (int) $_REQUEST[ 'category_id' ];
		if ( $category_id )
		{
			$request_cateinfo = D( 'ShopCate' )->where( 'id=' . $category_id )->find();
			$level = $request_cateinfo[ 'level' ];
			$parent_id = $request_cateinfo[ 'parent_id' ];
			$cateList[ $level + 1 ] = array_pop( generate_categoryList( $category_id, 'shop' ) );
			if ( empty( $cateList[ $level + 1 ] ) )
			{
				unset( $cateList[ $level + 1 ] );
			}
			$selected_cate[ $level ] = $category_id;
			for ( $i = $level; $i >= 1; $i-- )
			{
				$selected_cate[ $i - 1 ] = $parent_id;
				$cateList[ $i ] = array_pop( generate_categoryList( $parent_id, 'shop' ) );
				$parent_id = D( 'ShopCate' )->where( 'id=' . $parent_id )->getField( 'parent_id' );
			}
			$nextLevel = $level + 1;
			$this->assign( $category_id );
		}
		else
		{
			$cateList[ 1 ] = array_pop( generate_categoryList( 0, 'shop' ) );
		}
		$this->assign( 'price_range', $_REQUEST[ 'price_range' ] );
		$this->assign( 'nearby', $_REQUEST[ 'nearby' ] );
		$this->assign( 'query', $_REQUEST[ 'query' ] );
		$this->assign( 'selected_cate', $selected_cate );
		$this->assign( 'cateList', $cateList );
		$this->assign( 'category_id', $category_id );

		if ( $_REQUEST[ 'location_id' ] )
		{
			$this->assign( 'location_id', $location_id );
			$location = explode( '_', $_REQUEST[ 'location_id' ] );
			if ( $location[ 0 ] == 'main' )
			{
				$main_location_id = $location[ 1 ];
			}
			else
			{
				$sub_location_ids[] = $location[ 1 ];
			}
		}
		$main_location_list = getmainlocationList();
		$this->assign( 'main_location_list', $main_location_list );
		for ( $i = 0; $i < 4; $i++ )
		{
			$sub_location_list[ $i ] = getsublocationList( $i + 1 );
		}
		$this->assign( 'sub_location_list', $sub_location_list );
		$this->assign( 'main_location_id', $main_location_id );
		$this->assign( 'sub_location_ids', $sub_location_ids );
		$this->display();
	}

	public function view()
	{
		$page = (int) $_REQUEST[ 'page' ];
		$shop_id = (int) $_REQUEST[ 'shop_id' ];
		$shopInfo = D( 'ShopFields' )
			->find( $shop_id );
		if ( $shopInfo[ 'eng_name' ] != '' && $shopInfo[ 'eng_name' ] != $shopInfo[ 'name' ] )
		{
			$shopInfo[ 'name' ].=' ' . $shopInfo[ 'eng_name' ];
		}
		if ( $shopInfo[ 'shop_status' ] != 0 )
		{
			switch ( $shopInfo[ 'shop_status' ] )
			{
				case 1:
					$shopInfo[ 'name' ].=' (已搬址)';
					break;
				case 2:
					$shopInfo[ 'name' ].=' (已結業)';
					break;
				case 3:
					$shopInfo[ 'name' ].=' (裝修中)';
					break;
			}
		}
		$getCates = D( 'ShopCateMap' )->where( 'shop_id=' . $shop_id )->select();
		foreach ( $getCates as $key => $val )
		{
			$shopInfo[ 'category_names' ][ $val[ 'cate_id' ] ][ 'id' ] = $val[ 'cate_id' ];
			$shopInfo[ 'category_names' ][ $val[ 'cate_id' ] ][ 'name' ] = D( 'ShopCate' )->where( array( 'type' => array( 'neq', 'website' ), 'id' => $val[ 'cate_id' ] ) )->getField( 'name' );
		}
		$shopInfo[ 'shop_id' ] = $shop_id;
		$shopInfo[ 'mall_id' ] = $shopInfo[ 'parent_shop_id' ];
		$shopInfo[ 'main_location_name' ] = D( 'LocationMain' )->where( 'id=' . $shopInfo[ 'main_location_id' ] )->getField( 'name' );
		$shopInfo[ 'sub_location_name' ] = D( 'LocationSub' )->where( 'id=' . $shopInfo[ 'sub_location_id' ] )->getField( 'name' );
		if ( $shopInfo[ 'mall_id' ] != 0 )
		{
			$shopInfo[ 'address' ] = D( 'ShopFields' )->where( 'id=' . $shopInfo[ 'mall_id' ] )->getField( 'name' ) . " " . $shopInfo[ 'address' ];
		}
		$shopInfo[ 'address' ] = $shopInfo[ 'main_location_name' ] . " " . $shopInfo[ 'sub_location_name' ] . " " . $shopInfo[ 'address' ];
		$shopInfo[ 'address' ] = $shopInfo[ 'address' ] . " " . $shopInfo[ 'rooms' ];
		$working_hour = D( 'ShopTime' )->where( 'shop_id=' . $shop_id )->order( 'start_time,end_time,recurring_week' )->select();
		$where = array( "shop_id" => $shop_id, "recurring_week " => 0 );
		$special_working_hour = D( 'ShopTime' )->where( $where )->order( 'start_day,end_day' )->select();
		$shopInfo[ 'worktime' ] = generateShopWorkTime( $shopInfo[ 'work_time' ], $working_hour, $special_working_hour );
		$shopInfo[ 'telephone' ] = formatTelephone( $shopInfo[ 'telephone' ], 1, $startSalt, $endSalt, true );
		$shopInfo[ 'rating' ] = D( 'StatisticsShop' )->where( 'shop_id=' . $shop_id )->find();
		$shopInfo[ 'pictures' ] = D( 'ShopPicture' )->where( 'Shop_id=' . $shop_id )->count();
		$shopInfo[ 'is_mall' ] = D( 'ShopFields' )->where( 'parent_shop_id=' . $shop_id )->count() ? 1 : 0;

		/* input page */
		$result_per_page = C( 'RESULT_PER_PAGE' );
		$count = D( 'ReviewFields' )
			->table( 'review_fields a' )
			->where( array( 'a.shop_id' => $shop_id, 'a.is_audit' => '1' ) )
			->count( 'distinct a.id' );
		$totalpage = ceil( $count / $result_per_page );
		if ( $page == '' )
		{
			$page = 1;
		}
		if ( $page > $totalpage )
		{
			$page = $totalpage;
		}
		/* input page */

		$shopInfo[ 'review_count' ] = $count;
		$shopInfo[ 'reviews' ] = D( 'ReviewFields' )
			->field( 'a.id as review_id,a.title,a.description,b.rate_1 as rating,c.nick_name as user_name, if(c.face_path,CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'USERFACE_UPFILE_PATH' ) . 'face",c.face_path),CONCAT("' . C( 'OUTSTREET_DIR' ) . C( 'USERDEFAULTFACE_UPFILE_PATH' ) . '",c.face_path)) AS user_picture_url,FROM_UNIXTIME(a.create_time,"%d/%m") as create_time,FROM_UNIXTIME(a.create_time,"%w") as week_time,d.review_count as user_review_count' )
			->table( 'review_fields a' )
			->join( 'review_rate b ON a.id=b.review_id' )
			->join( 'user_fields c ON a.user_id=c.id' )
			->join( 'statistics_user d ON c.id=d.user_id' )
			->where( array( 'a.shop_id' => $shop_id, 'a.is_audit' => '1' ) )
			->limit( $result_per_page )->page( $page )
			->order( 'a.create_time desc' )
			->select();
		foreach ( $shopInfo[ 'reviews' ] as $key => $val )
		{
			$shopInfo[ 'reviews' ][ $key ][ 'week_time' ] = week2ch( $shopInfo[ 'reviews' ][ $key ][ 'week_time' ] );
			$shopInfo[ 'reviews' ][ $key ][ 'description' ] = strip_tags( $shopInfo[ 'reviews' ][ $key ][ 'description' ] );
			$shopInfo[ 'reviews' ][ $key ][ 'create_time' ].="(" . $shopInfo[ 'reviews' ][ $key ][ 'week_time' ] . ")";
			$shopInfo[ 'reviews' ][ $key ][ 'review_rate' ] = D( 'ReviewRate' )->where( 'review_id=' . $val[ 'review_id' ] )->find();
			unset( $shopInfo[ 'reviews' ][ $key ][ 'week_time' ] );
		}
		$this->assign( 'page_url', preg_replace( '/\/page\/\d+/is', '', $_SERVER[ 'REQUEST_URI' ] ) );
		$this->assign( 'result_per_page', $result_per_page );
		$this->assign( 'page', $page );
		$this->assign( 'result_count', $count );
		$this->assign( 'totalpage', $totalpage );
		foreach ( $shopInfo as $key => $val )
		{
			if ( !is_array( $val ) && $val == '' )
			{
				$shopInfo[ $key ] = '--';
			}
		}
		$this->assign( 'shopInfo', $shopInfo );
		$this->assign( 'title', $shopInfo[ 'name' ] );
		$this->display();
	}

	public function photo()
	{
		$page = (int) $_REQUEST[ 'page' ];
		$shop_id = $_REQUEST[ 'shop_id' ];
		$shopInfo = $this->shopbasicinfo( $shop_id );
		$shopInfo[ 'pictures_count' ] = D( 'ShopPicture' )->where( 'Shop_id=' . $shop_id )->count();
		/* input page */
		$result_per_page = C( 'RESULT_PER_PAGE' );
		$count = D( 'ShopPicture' )
			->where( 'Shop_id=' . $shop_id )
			->count( 'distinct id' );
		$totalpage = ceil( $count / $result_per_page );
		if ( $page == '' )
		{
			$page = 1;
		}
		if ( $page > $totalpage )
		{
			$page = $totalpage;
		}
		/* input page */

		$getPictures = D( 'ShopPicture' )
			->field( 'file_path' )
			->where( 'Shop_id=' . $shop_id )
			->limit( $result_per_page )->page( $page )
			->select();
		foreach ( $getPictures as $key => $val )
		{
			$shopInfo[ 'pictures' ][ $key ] = C( 'OUTSTREET_DIR' ) . C( 'SHOP_UPFILE_PATH' ) . 'thumb_' . $val[ 'file_path' ];
		}
		$this->assign( 'page_url', preg_replace( '/\/page\/\d+/is', '', $_SERVER[ 'REQUEST_URI' ] ) );
		$this->assign( 'result_per_page', $result_per_page );
		$this->assign( 'page', $page );
		$this->assign( 'totalpage', $totalpage );
		$this->assign( 'result_count', $count );
		$this->assign( 'shopInfo', $shopInfo );
		$this->assign( 'title', $shopInfo[ 'name' ] );
		$this->display();
	}

	public function mall()
	{
		$page = (int) $_REQUEST[ 'page' ];
		$shop_id = $_REQUEST[ 'shop_id' ];
		$shopInfo = $this->shopbasicinfo( $shop_id );
		/* input page */
		$result_per_page = C( 'RESULT_PER_PAGE' );
		$count = D( 'ShopFields' )
			->where( array( 'parent_shop_id' => $shop_id, 'is_audit' => '1' ) )
			->count( 'distinct id' );
		$totalpage = ceil( $count / $result_per_page );
		if ( $page == '' )
		{
			$page = 1;
		}
		if ( $page > $totalpage )
		{
			$page = $totalpage;
		}
		/* input page */
		$shopList = D( 'ShopFields' )
			->where( array( 'parent_shop_id' => $shop_id, 'is_audit' => '1' ) )
			->field( 'id as shop_id' )
			->limit( $result_per_page )->page( $page )
			->select();
		foreach ( $shopList as $key => $val )
		{
			$shopList[ $key ] = $this->shopbasicinfo( $val[ 'shop_id' ] );
		}
		$this->assign( 'page_url', preg_replace( '/\/page\/\d+/is', '', $_SERVER[ 'REQUEST_URI' ] ) );
		$this->assign( 'result_per_page', $result_per_page );
		$this->assign( 'page', $page );
		$this->assign( 'totalpage', $totalpage );
		$this->assign( 'result_count', $count );
		$this->assign( 'shopList', $shopList );
		$this->assign( 'shopInfo', $shopInfo );
		$this->assign( 'title', $shopInfo[ 'name' ] );
		$this->display();
	}

	public function brand()
	{
		$page = (int) $_REQUEST[ 'page' ];
		$shop_id = $_REQUEST[ 'shop_id' ];
		$shopInfo = $this->shopbasicinfo( $shop_id );
		/* input page */
		$result_per_page = C( 'RESULT_PER_PAGE' );
		$count = D( 'ShopFields' )
			->where( array( 'brand_id' => $shopInfo[ 'brand_id' ], 'is_audit' => '1' ) )
			->count( 'distinct id' );
		$totalpage = ceil( $count / $result_per_page );
		if ( $page == '' )
		{
			$page = 1;
		}
		if ( $page > $totalpage )
		{
			$page = $totalpage;
		}
		/* input page */
		$shopList = D( 'ShopFields' )
			->where( array( 'brand_id' => $shopInfo[ 'brand_id' ], 'is_audit' => '1' ) )
			->field( 'id as shop_id' )
			->limit( $result_per_page )->page( $page )
			->select();
		foreach ( $shopList as $key => $val )
		{
			$shopList[ $key ] = $this->shopbasicinfo( $val[ 'shop_id' ] );
		}
		$this->assign( 'page_url', preg_replace( '/\/page\/\d+/is', '', $_SERVER[ 'REQUEST_URI' ] ) );
		$this->assign( 'result_per_page', $result_per_page );
		$this->assign( 'page', $page );
		$this->assign( 'totalpage', $totalpage );
		$this->assign( 'result_count', $count );
		$this->assign( 'shopList', $shopList );
		$this->assign( 'shopInfo', $shopInfo );
		$this->assign( 'title', $shopInfo[ 'name' ] );
		$this->display();
	}

	public function map()
	{
		$shop_id = $_REQUEST[ 'shop_id' ];
		$shopInfo = $this->shopbasicinfo( $shop_id );
		$this->assign( 'shopInfo', $shopInfo );
		$this->assign( 'title', $shopInfo[ 'name' ] );
		$this->display();
	}

	private function shopbasicinfo( $shop_id )
	{
		$shopInfo = D( 'ShopFields' )
			->field( 'id as shop_id,name,eng_name,shop_status,parent_shop_id,address,rooms,main_location_id,sub_location_id,telephone,description,googlemap_lat,googlemap_lng,brand_id' )
			->find( $shop_id );
		if ( $shopInfo[ 'eng_name' ] != '' && $shopInfo[ 'eng_name' ] != $shopInfo[ 'name' ] )
		{
			$shopInfo[ 'name' ].=' ' . $shopInfo[ 'eng_name' ];
		}
		if ( $shopInfo[ 'shop_status' ] != 0 )
		{
			switch ( $shopInfo[ 'shop_status' ] )
			{
				case 1:
					$shopInfo[ 'name' ].=' (已搬址)';
					break;
				case 2:
					$shopInfo[ 'name' ].=' (已結業)';
					break;
				case 3:
					$shopInfo[ 'name' ].=' (裝修中)';
					break;
			}
		}
		$getCates = D( 'ShopCateMap' )->where( 'shop_id=' . $shop_id )->select();
		foreach ( $getCates as $key => $val )
		{
			$shopInfo[ 'category_names' ][ $val[ 'cate_id' ] ][ 'id' ] = $val[ 'cate_id' ];
			$shopInfo[ 'category_names' ][ $val[ 'cate_id' ] ][ 'name' ] = D( 'ShopCate' )->where( array( 'type' => array( 'neq', 'website' ), 'id' => $val[ 'cate_id' ] ) )->getField( 'name' );
		}
		$shopInfo[ 'mall_id' ] = $shopInfo[ 'parent_shop_id' ];
		$shopInfo[ 'main_location_name' ] = D( 'LocationMain' )->where( 'id=' . $shopInfo[ 'main_location_id' ] )->getField( 'name' );
		$shopInfo[ 'sub_location_name' ] = D( 'LocationSub' )->where( 'id=' . $shopInfo[ 'sub_location_id' ] )->getField( 'name' );
		if ( $shopInfo[ 'mall_id' ] != 0 )
		{
			$shopInfo[ 'address' ] = D( 'ShopFields' )->where( 'id=' . $shopInfo[ 'mall_id' ] )->getField( 'name' ) . " " . $shopInfo[ 'address' ];
		}
		$shopInfo[ 'address' ] = $shopInfo[ 'main_location_name' ] . " " . $shopInfo[ 'sub_location_name' ] . " " . $shopInfo[ 'address' ];
		if ( $shopInfo[ 'rooms' ] )
		{
			$shopInfo[ 'address' ] = $shopInfo[ 'address' ] . " " . $shopInfo[ 'rooms' ];
		}
		$shopInfo[ 'telephone' ] = formatTelephone( $shopInfo[ 'telephone' ], 1, $startSalt, $endSalt, true );
		$shopInfo[ 'rating' ] = D( 'StatisticsShop' )->where( 'shop_id=' . $shop_id )->find();
		foreach ( $shopInfo as $key => $val )
		{
			if ( !is_array( $val ) && $val == '' )
			{
				$shopInfo[ $key ] = '--';
			}
		}
		return $shopInfo;
	}

}
