<?php

class IndexAction extends BaseAction
{

	public function index()
	{
		$hotshopcatesList = D( 'ShopCate' )
			->field( 'id,name' )
			->where( 'ysm_score != 0' )
			->order( '( -LOG( 1.0 - RAND() ) / ( ysm_score + 0.1 ) )' )
			->group( 'name' )
			->limit( 20 )
			->select();

		$hoteventcatesList = D( 'EventCate' )
			->field( 'id,name' )
			->where( 'parent_id=65' )
			->select();

		$shopcatesList = generate_categoryList( 0, 'shop' );
		$shopcatesList = array_pop( $shopcatesList );
		$shopcatesLists = array();
		foreach ( $shopcatesList AS $cate )
		{
			$subcate = generate_categoryList( $cate[ 'id' ], 'shop' );
			$subcate = array_pop( $subcate );
			$shopcatesLists[ $cate[ 'id' ] ] = $subcate;
		}

		$eventcatesList = generate_categoryList( 0, 'event' );
		$eventcatesList = array_pop( $eventcatesList );

		$sublocationList = D( 'LocationSub' )
			->field( 'id,main_location_id,name,orders,type' )
			->order( 'orders asc' )
			->select();

		$where = array();
		$where[ 'is_audit' ] = 1;
		$where[ 'is_recommend' ] = array( gt, 0 );
		$recommendEventList = D( 'EventFields' )->relation( true )->where( $where )->order( 'is_recommend asc', 'id desc' )->limit( 3 )->select();

		$this->assign( 'recommendEventList', $recommendEventList );
		$this->assign( 'hotshopcatesList', $hotshopcatesList );
		$this->assign( 'hoteventcatesList', $hoteventcatesList );
		$this->assign( 'shopcatesList', $shopcatesList );
		$this->assign( 'shopcatesLists', $shopcatesLists );
		$this->assign( 'eventcatesList', $eventcatesList );
		$this->assign( 'sublocationList', $sublocationList );
		$this->assign( 'recommendEventList', $recommendEventList );
		$this->display();
	}

}
