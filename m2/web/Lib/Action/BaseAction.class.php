<?php

require_once COMMON_PATH . 'Mobile_Detect.php';

class BaseAction extends Action
{

	public $title = 'OutStreet 出街 - Mobile';

	public function _initialize()
	{
		$this->checkMobile();

		$this->page_url = preg_replace( '/\/page\/\d+/', '', $_SERVER[ 'REQUEST_URI' ] );

		$this->assign( 'title', $this->title );
	}

	public function checkMobile()
	{
		$detect = new Mobile_Detect();

		if ( !$detect->isMobile() || $detect->isTablet() )
		{
			$url = 'http://www.outstreet.com.hk';

			if ( strtolower( MODULE_NAME ) == 'shop' )
			{
				switch ( strtolower( ACTION_NAME ) )
				{
					case "search":
						$url .= '/shop/search';
						$main_location_id = (int) $_REQUEST[ 'main_location_id' ];
						$sub_location_ids = (array) $_REQUEST[ 'sub_location_ids' ];
						if ( $_REQUEST[ 'location_id' ] )
						{
							$location = explode( '_', $_REQUEST[ 'location_id' ] );
							if ( $location[ 0 ] == 'main' )
							{
								$main_location_id = $location[ 1 ];
							}
							else
							{
								$sub_location_ids[] = $location[ 1 ];
							}
						}
						if ( $main_location_id )
						{
							$url .= '/main_location_id/' . $main_location_id;
						}
						elseif ( $sub_location_ids )
						{
							$url .= '/sub_location_ids/' . implode( ',', $sub_location_ids );
						}
						if ( $_REQUEST[ 'category_id' ] )
						{
							$url .= '/shop_cate_ids/' . $_REQUEST[ 'category_id' ];
						}
						break;
					case "view":
						$url .= '/shop/view';
						if ( $_REQUEST[ 'shop_id' ] )
						{
							$url .= '/id/' . $_REQUEST[ 'shop_id' ];
						}
						break;
				}
			}
			else if ( strtolower( MODULE_NAME ) == 'event' )
			{
				switch ( strtolower( ACTION_NAME ) )
				{
					case "search":
						$url .= '/event/search';
						$main_location_id = (int) $_REQUEST[ 'main_location_id' ];
						$sub_location_ids = (array) $_REQUEST[ 'sub_location_ids' ];
						if ( $_REQUEST[ 'location_id' ] )
						{
							$location = explode( '_', $_REQUEST[ 'location_id' ] );
							if ( $location[ 0 ] == 'main' )
							{
								$main_location_id = $location[ 1 ];
							}
							else
							{
								$sub_location_ids[] = $location[ 1 ];
							}
						}
						if ( $main_location_id )
						{
							$url .= '/event_main_location_id/' . $main_location_id;
						}
						elseif ( $sub_location_ids )
						{
							$url .= '/event_sub_location_ids/' . implode( ',', $sub_location_ids );
						}
						if ( $_REQUEST[ 'category_id' ] )
						{
							$url .= '/event_cate_ids/' . $_REQUEST[ 'category_id' ];
						}
						break;
					case "view":
						$url .= '/event/view';
						if ( $_REQUEST[ 'event_id' ] )
						{
							$url .= '/id/' . $_REQUEST[ 'event_id' ];
						}
						break;
				}
			}

			header( 'location:' . $url );
		}
	}

}
