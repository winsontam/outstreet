<?php
import("Think.Core.Model.RelationModel");
class EventFieldsModel extends RelationModel{

	protected $_link = array(
		'sub_location'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'location_sub',
			'mapping_name'=>'sub_location',
			'foreign_key'=>'sub_location_id',
		),
		'main_location'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'location_main',
			'mapping_name'=>'main_location',
			'foreign_key'=>'main_location_id',
		),
		'cate'=>array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'event_cate',
			'mapping_name'=>'cate',
			'foreign_key'=>'cate_id',
		)
	);
	 
}

?>