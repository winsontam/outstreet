<?php

return array(
	//'配置项'=>'配置值
	'DB_TYPE'=>'mysql', //数据库类型
	'DB_HOST'=>'localhost', //服务器地址
	'DB_NAME'=>'outweb_main', //数据库名
	'DB_USER'=>'outweb_main', //用户名
	'DB_PWD'=>'out8989', //密码
	//'DB_PORT'=>3307, //端口
	'DB_PREFIX'=>'', //数据库表前缀

	'RESULT_PER_PAGE'=>5,
	'URL_DISPATCH_ON'=>true,
	'URL_MODE'=>0,	//URLREWRITE模式'
	'TEMPLATE_SUFFIX'=>'.html',	//模板文件後綴
	'URL_HTML_SUFFIX'=>'.html',	//偽靜態後綴
	'URL_CASE_INSENSITIVE'=>true,	//URL是否區分大小寫
	'APP_DEBUG'=>false,	//DEBUG模式是否開啟
	'OUTSTREET_DIR'=>'http://www.outstreet.com.hk',
	'SHOP_UPFILE_PATH'=> '/Public/uploadimages/shop/',
	'EVENT_UPFILE_PATH'=> '/Public/uploadimages/event/',
	'REVIEW_UPFILE_PATH'=> '/Public/uploadimages/review/',
	'EVENT_REVIEW_UPFILE_PATH'=> '/Public/uploadimages/event_review/'
);

?>